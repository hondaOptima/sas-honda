var table_autorizado = "";
var table_espera = "";
var table_comments = "";
var tipos;
var refacciones;
var talleres;
var deducibles;

var arr_defs_espera = [ {
                        "targets": -2,
                        "data": null,
                        "defaultContent": '<button type="button" class="btn btn-success btn-xs btn-autorizar">Autorizar</button>'
                    },{
                        "targets": -3,
                        "data": null,
                        "defaultContent": '<button type="button" class="btn btn-primary btn-xs btn-comentarios">Comentarios</button>'
                    },
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": '<button type="button" class="btn btn-primary btn-xs btn-editar">Editar</button>'
                    },
                    {
                        "targets": [1,2,10,13,16,18],
                        "visible": false
                    },
                    {
                        "responsivePriority": 1, "targets": [0,3,19,20,21,22]
                    },
                    {
                        "responsivePriority": 2, "targets": [4,5,6]
                    },
                    {
                        "responsivePriority": 3, "targets": [7,8]
                    }
];

var arr_defs_autorizados = [
                    {
                        "targets": [1,2,11,12,14,15,17,19,21],
                        "visible": false
                    },
                    {
                        "targets": -2,
                        "data": null,
                        "defaultContent": '<button type="button" class="btn btn-primary btn-xs btn-crear-cita">Cita</button>'
                    },
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": '<button type="button" class="btn btn-success btn-finalizar btn-xs">Finalizar</button>'
                    },
                    {
                        "responsivePriority": 1, "targets": [20,22,23,24,25]
                    },
                    {
                        "responsivePriority": 2, "targets": [5,6,7]
                    },
                    {
                        "responsivePriority": 3, "targets": [8,9]
                    }
];

var arr_defs_comments = [
                    {
                        "targets": [1,2],
                        "visible": false
                    },
                    {
                        "targets": 2,
                        "width": "50%"
                    },
                    {
                        "targets": [3,4],
                        "width": "25%"
                    }
];

function LoadDataTable( ) {
	'use strict';
	var EditableTable = {

		options: {
			addButton: '#addToTable',
			table: '#table-carroceria-autorizado',
			dialog: {
				wrapper: '#dialog',
				cancelButton: '#dialogCancel',
				confirmButton: '#dialogConfirm',
			}
		},

		initialize: function() {
			this
				.setVars()
				.build()
				.events();
		},

		setVars: function() {
			this.$table				= $( this.options.table );
			this.$addButton			= $( this.options.addButton );

			// dialog
			this.dialog				= {};
			this.dialog.$wrapper	= $( this.options.dialog.wrapper );
			this.dialog.$cancel		= $( this.options.dialog.cancelButton );
			this.dialog.$confirm	= $( this.options.dialog.confirmButton );

			return this;
		},

		build: function() {
			this.datatable = this.$table.DataTable({
				dom: "Bfrtip",
				sRowSelect: "single",
				"language": {
					"decimal":        "",
				    "emptyTable":     "Informaci&oacute;n no disponible",
				    "info":           "P&aacute;gina _START_ - _END_ de _TOTAL_ registros",
				    "infoEmpty":      "P&aacute;gina 0 al 0 - 0 registros",
				    "infoFiltered":   "(filtered from _MAX_ total registros)",
				    "infoPostFix":    "",
				    "thousands":      ",",
				    "lengthMenu":     "Ver _MENU_ registros",
				    "loadingRecords": "Cargando datos...",
				    "processing":     "Procesando...",
				    "search":         "Buscar:",
				    "zeroRecords":    "No se encontraron registros coincidentes",
				    "paginate": {
				        "first":      "Primero",
				        "last":       "Ultimo",
				        "next":       "Siguiente",
				        "previous":   "Anterior"
				    },
		        },

                "columnDefs": arr_defs_autorizados,
                responsive: true,
                "ajax": base_url + 'get_carroceria_autorizados',
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                    var opciones = ""; var elemento_seleccionado = undefined;
                    for(var i=0;i<refacciones.length;i++)
                    {
                        if(aData[19] == refacciones[i].id) elemento_seleccionado = refacciones[i];
                        opciones+='<li><a href="#" data-value="' + refacciones[i].id + '" data-color-class="' + refacciones[i].color + '">' + refacciones[i].descripcion + '</a></li>';
                    }

                    var str = '<div class="dropdown"><button class="btn ' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.color : "btn-default") + ' dropdown-toggle btn-xs" type="button"';
                    str+= ' data-toggle="dropdown" id="dde_r'+aData[1]+'" data-id="' + aData[1] + '" data-value=' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.id : '"0"') + ' data-campo="refaccion">';
                    str+= ((elemento_seleccionado != undefined) ? elemento_seleccionado.descripcion : "Seleccione una opción") + '<span class="caret"></span></button><ul class="dropdown-menu" aria-labelledby="dde_r'+ aData[1] +'">';
                    str += '<li><a href="#" data-value="0" data-color-class="btn-default">Selecciona una opción</a></li>';
                    str+= opciones;
                    str+='</ul></div>';
                    $('td:eq(12)', nRow).html(str);


                    opciones = ""; elemento_seleccionado = undefined;
                    for(i=0;i<talleres.length;i++)
                    {
                        if(aData[21] == talleres[i].id) elemento_seleccionado = talleres[i];
                        opciones+='<li><a href="#" data-value="' + talleres[i].id + '" data-color-class="' + talleres[i].color + '">' + talleres[i].descripcion + '</a></li>';
                    }

                    str = '<div class="dropdown"><button class="btn ' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.color : "btn-default") + ' dropdown-toggle btn-xs" type="button"';
                    str+= ' data-toggle="dropdown" id="dde_t'+aData[1]+'" data-id="' + aData[1] + '" data-value=' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.id : '"0"') + ' data-campo="taller">';
                    str+= ((elemento_seleccionado != undefined) ? elemento_seleccionado.descripcion : "Seleccione una opción") + '<span class="caret"></span></button><ul class="dropdown-menu" aria-labelledby="dde_t'+ aData[1] +'">';
                    str += '<li><a href="#" data-value="0" data-color-class="btn-default">Selecciona una opción</a></li>';
                    str+= opciones;
                    str+='</ul></div>';
                    $('td:eq(13)', nRow).html(str);

                    $("td:eq(8)", nRow).attr("title",aData[12]);
                    $("td:eq(9)", nRow).attr("title",aData[15]);


                    opciones = ""; elemento_seleccionado = undefined;
                    for(i=0;i<deducibles.length;i++)
                    {
                        if(parseInt(aData[23]) == parseInt(deducibles[i].id)) elemento_seleccionado = deducibles[i];
                        opciones+='<li><a href="#" data-value="' + deducibles[i].id + '" data-color-class="' + deducibles[i].color + '">' + deducibles[i].descripcion + '</a></li>';
                    }

                    str = '<div class="dropdown"><button class="btn ' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.color : "btn-default") + ' dropdown-toggle btn-xs" type="button"';
                    str+= ' data-toggle="dropdown" id="dec_'+aData[1]+'" data-id="' + aData[1] + '" data-value=' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.id : '""') + ' data-campo="deducible">';
                    str+= ((elemento_seleccionado != undefined) ? elemento_seleccionado.descripcion : "Seleccione una opción") + '<span class="caret"></span></button><ul class="dropdown-menu" aria-labelledby="dec_'+ aData[1] +'">';
                    str += '<li><a href="#" data-value="" data-color-class="btn-default">Selecciona una opción</a></li>';
                    str+= opciones;
                    str+='</ul></div>';
                    $('td:eq(14)', nRow).html(str);
                },
				"bSort": false,
			});
			window.dt = this.datatable;
			return this;
		},

		events: function() {
			var _self = this;

			this.$table

                .on('click', '.dropdown-menu li a', function( e ) {

                    var id = $(this).closest("ul").attr("aria-labelledby"); id = "#" + id;

                    if(($(id).data("value")).toString() != ($(this).data("value")).toString())
                    {
                        var id_carroceria = $(id).data("id");
                        var campo = $(id).data("campo");
                        var post_string = "id=" + id_carroceria + "&" + campo + "=" + $(this).data("value");
                        var color = $(this).data("color-class");
                        var value = $(this).data("value");
                        var selText = $(this).text();
                        $.ajax({
                            url: url_base + "update_carroceria",
                            data : post_string,
                            type : "POST",
                            dataType : "json",
                            success: function(json,textStatus, jqXHR) {
                                if(json.error)
                                {
                                    $.amaran({
                                        'theme'     :'colorful',
                                        'content'   : {
                                            bgcolor:color_error,
                                            color:'#fff',
                                            message:json.error
                                        },
                                        'position'  :'top left',
                                        'inEffect'  :'slideLeft',
                                        'outEffect' :'slideLeft'
                                    });
                                }
                                else
                                {
                                    var clases = ["btn-default","btn-success","btn-primary","btn-warning"];
                                    $(id).html(selText + ' <span class="caret"></span>');

                                    for(var i = 0; i < clases.length; i++)
                                        if($(id).hasClass(clases[i]))
                                            $(id).removeClass(clases[i]).addClass(color);
                                    $(id).data("value",value);

                                    $.amaran({
                                        'theme'     :'colorful',
                                        'content'   : {
                                            bgcolor:color_success,
                                            color:'#fff',
                                            message:'Registro actualizado como \"' + selText + '\"'
                                        },
                                        'position'  :'top left',
                                        'inEffect'  :'slideLeft',
                                        'outEffect' :'slideLeft'
                                    });
                                }
                            },
                            complete : function(jqXHR , textStatus) {

                            },
                            error : function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                            },
                            statusCode: {
                                404: function() {
                                    console.error( "page not found" );
                                },
                                500: function(){
                                    console.log("Internal server error");
                                }
                            }
                        });
                    }
                    else $.amaran({
                        'theme'     :'colorful',
                        'content'   : {
                            bgcolor:color_warning,
                            color:'#fff',
                            message: "Es el mismo valor"
                        },
                        'position'  :'top left',
                        'inEffect'  :'slideLeft',
                        'outEffect' :'slideLeft'
                    });

					e.preventDefault();
				})

                .on('click', '.btn-finalizar', function( e ) {
                    var id = $("#table-carroceria-autorizado").DataTable().row($(this).closest("tr")).data()[1];

                    swal({
                    	title: '¿Finalizar?',
                    	type: 'question',
                    	html:'',
                    	showCloseButton: true,
                    	showCancelButton: true,
                    	confirmButtonText:
                    	'Finalizar',
                    	cancelButtonText:
                    	'Cancelar'
                    }).then(function () {

                        $.ajax({
                            url: url_base + "update_carroceria",
                            data : { id : id, activo : "0" },
                            type : "POST",
                            dataType : "json",
                            success: function(json,textStatus, jqXHR) {
                                if(json.success)
                                {
                                    $("#table-carroceria-autorizado").DataTable().ajax.url(base_url + 'get_carroceria_autorizados').load();
                                    $("#table-carroceria-espera").DataTable().ajax.url(base_url + 'get_carroceria_espera' ).load();

                                    swal({
                                        title: '¡Finalizado correctamente!',
                                        text: 'Has finalizado el registro.',
                                        timer: 1000,
                                        type: 'success'
                                    }).then(
                                        function () {},
                                        function (dismiss) {
                                            if (dismiss === 'timer') {
                                            }
                                        }
                                    );
                                }
                                else if(json.error) swal('Error',json.error,'error');
                                else swal('Error','Error desconocido','error');
                            },
                            complete : function(jqXHR , textStatus) {

                            },
                            error : function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                            },
                            statusCode: {
                                404: function() {
                                    console.error( "page not found" );
                                },
                                500: function(){
                                    console.log("Internal server error");
                                }
                            }
                        });

                    },
                    	function(dismiss){
                    		if(dismiss === 'cancel') {

                    		}
                    	}
                    );

                    $("#txt_fecha_autorizada").datetimepicker({
                    	format: 'yyyy-mm-dd hh:ii',
                    	daysOfWeekDisabled: "0",
                    	todayBtn: true,
                    	autoclose : true
                    });

					e.preventDefault();
				})

			this.$addButton.on( 'click', function(e) {
				e.preventDefault();
				_self.rowAdd();
			});

			return this;
		},

		// ==========================================================================================
		// ROW FUNCTIONS
		// ==========================================================================================

	};

 	$(function() {
		EditableTable.initialize();
	});

    var TableEspera = {

		options: {
			addButton: '#addToTableEspera',
			table: '#table-carroceria-espera',
			dialog: {
				wrapper: '#dialogEspera',
				cancelButton: '#dialogCancelEspera',
				confirmButton: '#dialogConfirmEspera',
			}
		},

		initialize: function() {
			this
				.setVars()
				.build()
				.events();
		},

		setVars: function() {
			this.$table				= $( this.options.table );
			this.$addButton			= $( this.options.addButton );

			// dialog
			this.dialog				= {};
			this.dialog.$wrapper	= $( this.options.dialog.wrapper );
			this.dialog.$cancel		= $( this.options.dialog.cancelButton );
			this.dialog.$confirm	= $( this.options.dialog.confirmButton );

			return this;
		},

		build: function() {
			this.datatable = this.$table.DataTable({
				dom: "Bfrtip",
				sRowSelect: "single",
				"language": {
					"decimal":        "",
				    "emptyTable":     "Informaci&oacute;n no disponible",
				    "info":           "P&aacute;gina _START_ - _END_ de _TOTAL_ registros",
				    "infoEmpty":      "P&aacute;gina 0 al 0 - 0 registros",
				    "infoFiltered":   "(filtered from _MAX_ total registros)",
				    "infoPostFix":    "",
				    "thousands":      ",",
				    "lengthMenu":     "Ver _MENU_ registros",
				    "loadingRecords": "Cargando datos...",
				    "processing":     "Procesando...",
				    "search":         "Buscar:",
				    "zeroRecords":    "No se encontraron registros coincidentes",
				    "paginate": {
				        "first":      "Primero",
				        "last":       "Ultimo",
				        "next":       "Siguiente",
				        "previous":   "Anterior"
				    },
		        },

                "columnDefs": arr_defs_espera,
                responsive: true,
                "ajax": base_url + 'get_carroceria_espera',
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                    opciones = ""; elemento_seleccionado = undefined;
                    for(i=0;i<tipos.length;i++)
                    {
                        if(aData[2] == tipos[i].id) elemento_seleccionado = tipos[i];
                        opciones+='<li><a href="#" data-value="' + tipos[i].id + '" data-color-class="' + tipos[i].color + '">' + tipos[i].descripcion + '</a></li>';
                    }

                    str = '<div class="dropdown"><button class="btn ' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.color : "btn-default") + ' dropdown-toggle btn-xs" type="button"';
                    str+= ' data-toggle="dropdown" id="dde_'+aData[1]+'" data-id="' + aData[1] + '" data-value="' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.id : "0") + '" data-campo="tipo">';
                    str+= ((elemento_seleccionado != undefined) ? elemento_seleccionado.descripcion : "Seleccione una opción") + '<span class="caret"></span></button><ul class="dropdown-menu" aria-labelledby="dde_'+aData[1]+'">';
                    str += '<li><a href="#" data-value="0" data-color-class="btn-default">Selecciona una opción</a></li>';
                    str += opciones;
                    str+= '</ul></div>';
                    $('td:eq(1)', nRow).html(str);

                    var opciones = ""; var elemento_seleccionado = undefined;
                    for(var i=0;i<refacciones.length;i++)
                    {
                        if(aData[18] == refacciones[i].id) elemento_seleccionado = refacciones[i];
                        opciones+='<li><a href="#" data-value="' + refacciones[i].id + '" data-color-class="' + refacciones[i].color + '">' + refacciones[i].descripcion + '</a></li>';
                    }

                    var str = '<div class="dropdown"><button class="btn ' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.color : "btn-default") + ' dropdown-toggle btn-xs" type="button"';
                    str+= ' data-toggle="dropdown" id="dde_r'+aData[1]+'" data-id="' + aData[1] + '" data-value=' + ((elemento_seleccionado != undefined) ? elemento_seleccionado.id : '"0"') + ' data-campo="refaccion">';
                    str+= ((elemento_seleccionado != undefined) ? elemento_seleccionado.descripcion : "Seleccione una opción") + '<span class="caret"></span></button><ul class="dropdown-menu" aria-labelledby="dde_r'+ aData[1] +'">';
                    str += '<li><a href="#" data-value="0" data-color-class="btn-default">Selecciona una opción</a></li>';
                    str+= opciones;
                    str+='</ul></div>';
                    $('td:eq(13)', nRow).html(str);
                },
				"bSort": false,
			});
			window.dt = this.datatable;
			return this;
		},

		events: function() {
			var _self = this;

			this.$table

                .on('click', '.dropdown-menu li a', function( e ) {

                    var id = $(this).closest("ul").attr("aria-labelledby"); id = "#" + id;

                    if(($(id).data("value")).toString() != ($(this).data("value")).toString())
                    {
                        var id_carroceria = $(id).data("id");
                        var campo = $(id).data("campo");
                        var post_string = "id=" + id_carroceria + "&" + campo + "=" + $(this).data("value");
                        var color = $(this).data("color-class");
                        var value = $(this).data("value");
                        var selText = $(this).text();
                        $.ajax({
                            url: url_base + "update_carroceria",
                            data : post_string,
                            type : "POST",
                            dataType : "json",
                            success: function(json,textStatus, jqXHR) {
                                if(json.error)
                                {
                                    $.amaran({
                                        'theme'     :'colorful',
                                        'content'   : {
                                            bgcolor:color_error,
                                            color:'#fff',
                                            message:json.error
                                        },
                                        'position'  :'top left',
                                        'inEffect'  :'slideLeft',
                                        'outEffect' :'slideLeft'
                                    });
                                }
                                else
                                {
                                    var clases = ["btn-default","btn-success","btn-primary","btn-warning"];
                                    $(id).html(selText + ' <span class="caret"></span>');

                                    for(var i = 0; i < clases.length; i++)
                                        if($(id).hasClass(clases[i]))
                                            $(id).removeClass(clases[i]).addClass(color);
                                    $(id).data("value",value);

                                    $.amaran({
                                        'theme'     :'colorful',
                                        'content'   : {
                                            bgcolor:color_success,
                                            color:'#fff',
                                            message:'Registro actualizado como \"' + selText + '\"'
                                        },
                                        'position'  :'top left',
                                        'inEffect'  :'slideLeft',
                                        'outEffect' :'slideLeft'
                                    });
                                }
                            },
                            complete : function(jqXHR , textStatus) {

                            },
                            error : function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                            },
                            statusCode: {
                                404: function() {
                                    console.error( "page not found" );
                                },
                                500: function(){
                                    console.log("Internal server error");
                                }
                            }
                        });
                    }
                    else $.amaran({
                        'theme'     :'colorful',
                        'content'   : {
                            bgcolor:color_warning,
                            color:'#fff',
                            message: "Es el mismo valor"
                        },
                        'position'  :'top left',
                        'inEffect'  :'slideLeft',
                        'outEffect' :'slideLeft'
                    });

					e.preventDefault();
				})

                .on('click', '.btn-autorizar', function( e ) {

                    var id = $("#table-carroceria-espera").DataTable().row($(this).closest("tr")).data()[1];

                    swal({
                    	title: '¿Autorizar registro?',
                    	type: 'question',
                    	html:
                    		'<div class="row"><div class="col-md-12">'+
                    		'<div class="form-group">'+
                    		'<label for="txt_fecha_autorizada">Fecha recibido</label>'+
                    		'<input type="text" class="form-control datepicker" id="txt_fecha_autorizada" placeholder="Fecha Recibido">'+
                    		'</div>'+
                    		'</div></div>',
                    	showCloseButton: true,
                    	showCancelButton: true,
                    	confirmButtonText:
                    	'Autorizar',
                    	cancelButtonText:
                    	'Cancelar'
                    }).then(function () {
                    	var valor = $("#txt_fecha_autorizada").val();
                    	if(valor != "")
                    	{
                    		var dateFormat = 'YYYY-MM-DD hh:mm:ss';
                    		var valid = (moment(moment(valor).format(dateFormat),dateFormat,true).isValid());
                    		if(valid)
                            {
                                $.ajax({
                                    url: url_base + "update_carroceria",
                                    data : { id : id, autorizado : "1", fecha_autorizada : valor },
                                    type : "POST",
                                    dataType : "json",
                                    success: function(json,textStatus, jqXHR) {
                                        if(json.success)
                                        {
                                            $("#table-carroceria-autorizado").DataTable().ajax.url(base_url + 'get_carroceria_autorizados').load();
                                            $("#table-carroceria-espera").DataTable().ajax.url(base_url + 'get_carroceria_espera' ).load();

                                            swal({
                                                title: '¡Autorizado correctamente!',
                                                text: 'Has autorizado el registro.',
                                                timer: 1000,
                                                type: 'success'
                                            }).then(
                                                function () {},
                                                function (dismiss) {
                                                    if (dismiss === 'timer') {
                                                    }
                                                }
                                            );
                                        }
                                        else if(json.error) swal('Error',json.error,'error');
                                        else swal('Error','Error desconocido','error');
                                    },
                                    complete : function(jqXHR , textStatus) {

                                    },
                                    error : function(jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus);
                                    },
                                    statusCode: {
                                        404: function() {
                                            console.error( "page not found" );
                                        },
                                        500: function(){
                                            console.log("Internal server error");
                                        }
                                    }
                                });
                            }
                    		else swal('Error','El formato no es válido','error');
                    	}
                    	else swal('Error','No selecciono ninguna fecha','error');
                    },
                    	function(dismiss){
                    		if(dismiss === 'cancel') {
                    			swal(
                    				'Error',
                    				'No se autorizará el registro',
                    				'error'
                    			);
                    		}
                    	}
                    );

                    $("#txt_fecha_autorizada").datetimepicker({
                    	format: 'yyyy-mm-dd hh:ii',
                    	daysOfWeekDisabled: "0",
                    	todayBtn: true,
                    	autoclose : true
                    });

					e.preventDefault();
				})


                .on('click', '.btn-comentarios', function( e ) {
                    var data = $("#table-carroceria-espera").DataTable().row($(this).closest("tr")).data();
                    $("#table-comments").DataTable().ajax.url(base_url + 'get_comments/' + data[1] ).load();
                    $("#span_comment_name").html("Agregar Comentario: " + data[6]);
                    $("#modal-comment").data("id",data[1]);
                    $("#modal-comment").modal("show");
                })

                .on('click', '.btn-editar', function( e ) {
                    var id = $("#table-carroceria-espera").DataTable().row($(this).closest("tr")).data()[1];

                    $.ajax({
                        url: url_base + "get_carroceria",
                        data : { id : id },
                        type : "POST",
                        dataType : "json",
                        success: function(json,textStatus, jqXHR) {
                            if(json.error)
                            {
                                $.amaran({
                                    'theme'     :'colorful',
                                    'content'   : {
                                        bgcolor:color_error,
                                        color:'#fff',
                                        message:json.error
                                    },
                                    'position'  :'top left',
                                    'inEffect'  :'slideLeft',
                                    'outEffect' :'slideLeft'
                                });
                            }
                            else
                            {
                                $("#sel_tipo option").each(function() {
                                    if($(this).val() == json.tipo) $("#sel_tipo").val($(this).val());
                                });

                                if(json.fecha_recibido != "")
                                {
                                    var date = new Date(json.fecha_recibido);
                                    var day = date.getDate(); var month = date.getMonth()+1; var year = date.getFullYear(); var hour = date.getHours(); var minutes = date.getMinutes();
                                    $('#txt_fecha_recibido').val(year + '-' + ((month <= 9) ? "0" : "") + month + '-' + ((day <= 9) ? "0" : "") + day + " " + hour + ":"+minutes);
                                }

                                if(json.fecha_enviado != "")
                                {
                                    var date = new Date(json.fecha_enviado);
                                    var day = date.getDate(); var month = date.getMonth()+1; var year = date.getFullYear(); var hour = date.getHours(); var minutes = date.getMinutes();
                                    $('#txt_fecha_enviado').val(year + '-' + ((month <= 9) ? "0" : "") + month + '-' + ((day <= 9) ? "0" : "") + day + " " + hour + ":"+minutes);
                                }

                                if(json.fecha_enviado != "")
                                {
                                    var date = new Date(json.fecha_tentativa_entrega);
                                    var day = date.getDate(); var month = date.getMonth()+1; var year = date.getFullYear(); var hour = date.getHours(); var minutes = date.getMinutes();
                                    $('#txt_fecha_tentativa').val(year + '-' + ((month <= 9) ? "0" : "") + month + '-' + ((day <= 9) ? "0" : "") + day + " " + hour + ":" + minutes);
                                }

                                $("#txt_poliza").val(json.poliza);
                                $("#txt_siniestro").val(json.siniestro);

                                $("#sel_deducible option").each(function() {
                                    if($(this).val() == json.deducible) $("#sel_deducible").val($(this).val());
                                });
                            }
                        },
                        complete : function(jqXHR , textStatus) {

                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus);
                        },
                        statusCode: {
                            404: function() {
                                console.error( "page not found" );
                            },
                            500: function(){
                                console.log("Internal server error");
                            }
                        }
                    });

                    $('#myModal').data("id",id);
                    $('#myModal').modal('toggle');
                })

			this.$addButton.on( 'click', function(e) {
				e.preventDefault();
				_self.rowAdd();
			});

			return this;
		},

		// ==========================================================================================
		// ROW FUNCTIONS
		// ==========================================================================================

	};

 	$(function() {
		TableEspera.initialize();
	});


    var TableComentarios = {

		options: {
			addButton: '#addToTableComentarios',
			table: '#table-comments',
			dialog: {
				wrapper: '#dialogComentarios',
				cancelButton: '#dialogCancelComentarios',
				confirmButton: '#dialogConfirmComentarios',
			}
		},

		initialize: function() {
			this
				.setVars()
				.build()
				.events();
		},

		setVars: function() {
			this.$table				= $( this.options.table );
			this.$addButton			= $( this.options.addButton );

			// dialog
			this.dialog				= {};
			this.dialog.$wrapper	= $( this.options.dialog.wrapper );
			this.dialog.$cancel		= $( this.options.dialog.cancelButton );
			this.dialog.$confirm	= $( this.options.dialog.confirmButton );

			return this;
		},

		build: function() {
			this.datatable = this.$table.DataTable({
				dom: "Bfrtip",
				sRowSelect: "single",
				"language": {
					"decimal":        "",
				    "emptyTable":     "Informaci&oacute;n no disponible",
				    "info":           "P&aacute;gina _START_ - _END_ de _TOTAL_ registros",
				    "infoEmpty":      "P&aacute;gina 0 al 0 - 0 registros",
				    "infoFiltered":   "(filtered from _MAX_ total registros)",
				    "infoPostFix":    "",
				    "thousands":      ",",
				    "lengthMenu":     "Ver _MENU_ registros",
				    "loadingRecords": "Cargando datos...",
				    "processing":     "Procesando...",
				    "search":         "Buscar:",
				    "zeroRecords":    "No se encontraron registros coincidentes",
				    "paginate": {
				        "first":      "Primero",
				        "last":       "Ultimo",
				        "next":       "Siguiente",
				        "previous":   "Anterior"
				    },
		        },

                "columnDefs": arr_defs_comments,
                responsive: true,
                "ajax": base_url + 'get_comments',
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                },
				"bSort": false,
                "iDisplayLength": -1
			});
			window.dt = this.datatable;
			return this;
		},

		events: function() {
			var _self = this;

			this.$table

			this.$addButton.on( 'click', function(e) {
				e.preventDefault();
				_self.rowAdd();
			});
			return this;
		},

	};

 	$(function() {
		TableComentarios.initialize();
	});
}

$(document).ready(function() {

    $.ajax({
        url: url_base + "get_carroceria_data_status",
        dataType : "json",
        success: function(json,textStatus, jqXHR) {
            tipos = json.tipos;
            refacciones = json.refacciones;
            talleres = json.talleres;
            deducibles = json.deducibles;
            LoadDataTable();
        },
        complete : function(jqXHR , textStatus) {

        },
        error : function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        },
        statusCode: {
            404: function() {
                console.error( "page not found" );
            },
            500: function(){
                console.log("Internal server error");
            }
        }
    });

    $('#myModal').on('hidden.bs.modal', function () {
        reset_modal();
    });

    $('#modal-comment').on('hidden.bs.modal', function () {
        reset_modal_comment();
    });

    $(".select2").select2();

    $(".datepicker").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        daysOfWeekDisabled: "0",
        todayBtn: true,
        autoclose : true
    });

    $("#btn_guardar").click(function(e) {
        tipo = $("#sel_tipo").val();
        fecha_recibido = $("#txt_fecha_recibido").val();
        fecha_enviado = $("#txt_fecha_enviado").val();
        fecha_tentativa = $("#txt_fecha_tentativa").val();
        poliza = $("#txt_poliza").val();
        siniestro = $("#txt_siniestro").val();
        deducible = $("#sel_deducible").val();
        //taller = $("#sel_taller").val();
        id = $('#myModal').data("id");

        $.ajax({
            url: url_base + "update_carroceria",
            data : { id : id, tipo : tipo, fecha_recibido : fecha_recibido, fecha_enviado : fecha_enviado, poliza : poliza, siniestro : siniestro, deducible : deducible, fecha_tentativa : fecha_tentativa},
            type : "POST",
            dataType : "json",
            success: function(json,textStatus, jqXHR) {
                if(json.success)
                {
                    $("#table-carroceria-espera").DataTable().ajax.url( base_url + 'get_carroceria_espera' ).load();

                    swal({
                        title: '¡Se registro correctamente!',
                        text: 'Se ha actualizado correctamente el registro.',
                        timer: 1000,
                        type: 'success'
                    }).then(
                        function () {},
                        function (dismiss) {
                            if (dismiss === 'timer') {
                            }
                        }
                    );

                    $("#myModal").modal("toggle");
                }
                else if(json.error)
                {
                    $.amaran({
                        'theme'     :'colorful',
                        'content'   : {
                            bgcolor:color_error,
                            color:'#fff',
                            message:json.error
                        },
                        'position'  :'top left',
                        'inEffect'  :'slideLeft',
                        'outEffect' :'slideLeft'
                    });
                }
                else swal('Error','Error Desconocido','error');
            },
            complete : function(jqXHR , textStatus) {

            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            },
            statusCode: {
                404: function() {
                    console.error( "page not found" );
                },
                500: function(){
                    console.log("Internal server error");
                }
            }
        });
        e.preventDefault();
    });

    $("#btn_guardar_comentario").click(function(e) {
        comentario = $("#txt_comment").val();

        id = $('#modal-comment').data("id");
        $("#modal-comment").modal("toggle");

        $.ajax({
            url: url_base + "add_carroceria_comment",
            data : { id : id, comentario : comentario },
            type : "POST",
            dataType : "json",
            success: function(json,textStatus, jqXHR) {

                if(json.success)
                {
                    swal({
                        title: '¡Comentario agregado!',
                        text: 'Se ha registrado su comentario correctamente.',
                        timer: 1000,
                        type: 'success'
                    }).then(
                        function () {},
                        function (dismiss) {
                            if (dismiss === 'timer') {
                            }
                        }
                    );

                    $("#modal-comment").modal("hide");
                }
                else if(json.error)
                {
                    $.amaran({
                        'theme'     :'colorful',
                        'content'   : {
                            bgcolor:color_error,
                            color:'#fff',
                            message:json.error
                        },
                        'position'  :'top left',
                        'inEffect'  :'slideLeft',
                        'outEffect' :'slideLeft'
                    });
                }
                else swal('Error','Error Desconocido','error');
            },
            complete : function(jqXHR , textStatus) {

            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            },
            statusCode: {
                404: function() {
                    console.error( "page not found" );
                },
                500: function(){
                    console.log("Internal server error");
                }
            }
        });
        e.preventDefault();
    });

});

function cargar_elementos_funciones()
{
    $(".dropdown-menu li a").click(function() {
        id = $(this).closest("ul").attr("aria-labelledby"); id = "#" + id;

        if(($(id).data("value")).toString() != ($(this).data("value")).toString())
        {
            id_carroceria = $(id).data("id");
            campo = $(id).data("campo");
            post_string = "id=" + id_carroceria + "&" + campo + "=" + $(this).data("value");
            color = $(this).data("color-class");
            value = $(this).data("value");
            selText = $(this).text();
            $.ajax({
                url: url_base + "update_carroceria",
                data : post_string,
                type : "POST",
                dataType : "json",
                success: function(json,textStatus, jqXHR) {
                    if(json.error)
                    {
                        $.amaran({
                            'theme'     :'colorful',
                            'content'   : {
                                bgcolor:color_error,
                                color:'#fff',
                                message:json.error
                            },
                            'position'  :'top left',
                            'inEffect'  :'slideLeft',
                            'outEffect' :'slideLeft'
                        });
                    }
                    else
                    {
                        clases = ["btn-default","btn-success","btn-primary","btn-warning"];
                        $(id).html(selText + ' <span class="caret"></span>');

                        for(i = 0; i < clases.length; i++)
                            if($(id).hasClass(clases[i]))
                                $(id).removeClass(clases[i]).addClass(color);
                        $(id).data("value",value);

                        $.amaran({
                            'theme'     :'colorful',
                            'content'   : {
                                bgcolor:color_success,
                                color:'#fff',
                                message:'Registro actualizado como \"' + selText + '\"'
                            },
                            'position'  :'top left',
                            'inEffect'  :'slideLeft',
                            'outEffect' :'slideLeft'
                        });
                    }
                },
                complete : function(jqXHR , textStatus) {

                },
                error : function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                },
                statusCode: {
                    404: function() {
                        console.error( "page not found" );
                    },
                    500: function(){
                        console.log("Internal server error");
                    }
                }
            });
        }
        else $.amaran({
            'theme'     :'colorful',
            'content'   : {
                bgcolor:color_warning,
                color:'#fff',
                message: "Es el mismo valor"
            },
            'position'  :'top left',
            'inEffect'  :'slideLeft',
            'outEffect' :'slideLeft'
        });

    });

    $(".btn-crear-cita").click(function(){
        var win = window.open(base_url + "citas", '_blank');
        win.focus();
    });
}

function reset_modal()
{
    $("input[type='text']").val("");
    $("select").val("");
}
function reset_modal_comment()
{
    $("#txt_comment").val("");
    $("#span_comment_name").html("");
}
