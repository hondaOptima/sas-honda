<?php
	/*
	 * Script:    DataTables server-side script for PHP and MySQL
	 * Copyright: 2010 - Allan Jardine
	 * License:   GPL v2 or BSD (3-point)
	 */
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	 header('Content-Type: text/html; charset=UTF-8');
	 session_start();if ( !isset($_SESSION['username']) ) {}
	 require_once('../../conexion/conexion_datatable_crm.php');
	 $_SESSION['ciudad'];
	$nivel=$_SESSION['nivel'];
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	 ini_set('memory_limit', '-1');
	$aColumns = array('inv_cd','inv_vin','inv_auto', 'inv_status','inv_cilindros', 'inv_puertas','inv_pasajeros','inv_ID','inv_anio','inv_color','inv_tipo');
	

	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "inv_cd";
	
	/* DB table to use */
	$sTable = "inventario";
	
	/* Database connection information */
	;
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */
	
	/* 
	 * MySQL connection
	 */
	$gaSql['link'] =  mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
		die( 'Could not open connection to server' );
	
	mysql_select_db( $gaSql['db'], $gaSql['link'] ) or 
		die( 'Could not select database '. $gaSql['db'] );
	
	
	///funcion
	
	
	///fin funcion
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	 
	$sWhere = " where inv_en_piso=0 ";

	
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere .= " and (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$conversions = array(
    "æ" => "ae",
    "ñ" => "n",
	"Ñ" => "n",
	"í" => "i",
    "ó" => "o",
	"ú" => "u",
	"á" => "a",
    "é" => "e",
);

$varget= str_replace(array_keys($conversions), $conversions,$_GET['sSearch']);
			
			
			
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $varget)."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
		$sWhere;
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "";
			}
			else
			{
				$sWhere .= " AND  ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
		}
	}
	
	
	/*
	 * SQL queries
	 * Get data to display
	 */
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." 
		FROM   $sTable
		$sWhere
		$sOrder
		$sLimit
	";
	$rResult = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
	
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
		SELECT COUNT(".$sIndexColumn.")
		FROM   $sTable
	";
	$rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	


	
	while ( $aRow = mysql_fetch_array( $rResult ) )
	{
		$row = array();
		
		          

                    $row[]='<input type="checkbox" value="'.$aRow[ $aColumns[1] ].'" class="llego" name="1" id="inv'.$aRow[ $aColumns[1] ].'" checked="checked">';
					$row[]='<div style="cursor:pointer" class="selectVIN" id="'.$aRow[ $aColumns[1] ].'&'.$aRow[ $aColumns[8] ].'&'.$aRow[ $aColumns[2] ].'&'.$aRow[ $aColumns[9] ].'&'.$aRow[ $aColumns[10] ].'">'.$aRow[ $aColumns[1] ].'</div>';
					$row[]=$aRow[ $aColumns[0] ];
					$row[]=$aRow[ $aColumns[2] ];
					$row[]=$aRow[ $aColumns[4] ];
					$row[]=$aRow[ $aColumns[5] ];												
					
		
		
		//print_r($row);
		$output['aaData'][] = $row;
	
				   
	}
mysql_close($gaSql['link']);		
echo json_encode( $output );
?>