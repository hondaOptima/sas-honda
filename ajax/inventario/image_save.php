<?php
	header('Content-type: application/json; charset=UTF-8;');
	header('Access-Control-Allow-Origin: *');
	$response = array();
	if(!empty($_GET["dia_traslado"]) && !empty($_GET["hora"]))
	{
		$hoy = date("Y-m-d");
		$mañana = strtotime ('1 days',strtotime($hoy));
		$mañana = date('Y-m-d',$mañana);
		$hora_limite = "15:00:00";
		$hora_ahora = date("H:i:s");
		$fecha_enviada = date("Y-m-d", strtotime($_GET["dia_traslado"]));

		if(strtotime($fecha_enviada) == strtotime($hoy))
			$response["error"] = "No puedes registrar un traslado con la fecha de hoy";
		else
		{
			if(strtotime($fecha_enviada) < strtotime($hoy))
				$response["error"] = "No puedes registrar un traslado con fechas anteriores";
			else
			{
				if($fecha_enviada == $mañana && strtotime($hora_limite)<strtotime($hora_ahora))
					$response["error"] = "No se pueden registrar traslados para el día de mañana después de las 3:00pm";
				else
				{
					require_once('../../conexion/conexion_crm.php');
					require_once('../funciones/class.phpmailer-lite.php');
					require_once('../funciones/funciones.php');
					//$data = $_POST['imagedata'];
					$filename = 'j'.rand(10000,100000).'q'.rand(10000,100000).'bb'.rand(10000,100000).'le'.rand(10000,100000).'e.png';
					###############################DATOS DEL TRASLADO###############
					list($d,$m,$a)=explode('-',$_GET['dia_traslado']);
					$datos=array(
						'origen'=>$_GET['origen'],
						'destino'=>$_GET['destino'],
						'dia_tralado'=>$a.'-'.$m.'-'.$d,
						'hora'=>$_GET['hora'],
						'trasladista'=>$_GET['trasladista'],
						'vin'=>$_GET['vin'],
						'marca'=>$_GET['marca'],
						'modelo'=>$_GET['modelo'],
						'anio'=>$_GET['anio'],
						'motor'=>$_GET['motor'],
						'color'=>$_GET['color'],
						'torre'=>$_GET['torre'],
						'observaciones'=>$_GET['observaciones']
					);

					//actualizo los datos del vehiculo de autos_en_piso  le asigno un almacen de traslado le pongo torre cero
					//crm_updateAutoPiso($datos['vin']);
					//verificar Hora
					//resHora=crm_verificar_hora();
					//verificar que el numero de serie no exista en CRM
					$res = crm_verficar_vin_traslado($datos['vin']);
					if($res == 1)
					{
						$response["error"] = "Este vin ya ha sido trasladado";
					}
					else
					{
						crm_inserTraslado($datos,$filename);//insertar los datos del traslado en crm
						$lista = getListaCorreosAvisos($datos['origen']);//lista de correos
						enviarCorreo($lista,$datos);//Enviar correo de notificacion a la ciudad origen de auto.
						$response["success"] = "Se ha registrado el traslado correctamente";
					}
				}
			}
		}
	}
	else $response["error"] = "No se han recibido parámetros";
	echo json_encode($response);
?>
