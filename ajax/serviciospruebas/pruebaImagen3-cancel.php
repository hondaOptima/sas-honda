<?php
    /*Fecha de creación: 12 de septimebre del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: [fecha]
    Autor: [Autor]*/
    HEADER('Access-Control-Allow-Origin: *');

    if (isset($_POST["image"]) && isset($_POST["description"])) {

      $concat = "data:image/jpeg;base64,";//especifica que es una imagen
      $Base64Img = $_POST['image'];//imagen png codificada en base64
      $imgName = $_POST['description'];//nombre que llevará la imagen
      //$Base64Img = base64_decode($Base64Img, 9);//decodificar el base64 y nos daria un zip
      //$Base64Img = gzinflate($Base64Img);//descomprimimos el base64
      $texto = base64_url_decode($Base64Img);
      $Base64Img = $concat.base64_url_decode($Base64Img);//decodificar con caracteres de base64
      base64toImage($Base64Img,$imgName,$texto);
      $json = '{
          "Status": true,
          "Message": "Successful",
          "Object": {
                  "Image" : "",
                  "Description" : ""
          }
        }';
    }
    else {//en caso de que falte algún parámetro
      $json = '{
          "Status": false,
          "Message": "Missing Data",
          "Object": {
                  "Image" : "",
                  "Description" : ""
          }
        }';
    }
    echo $json;
    //método para convertir base64 en imagen
    function base64toImage($b64,$img,$texto)
    {
      //eliminamos data:image/png; y base64, de la cadena que tenemos
      list(, $b64) = explode(';', $b64);
      list(, $b64) = explode(',', $b64);
      file_put_contents($img, base64_decode($b64));//asignamos valor y nombre de nuestra imagen
      file_put_contents('texto.txt', $texto);
    }
    //método para cambiar los caracteres falsos de base64
    //Nota: se asignaron caracteres falsos para poder enviarse por URL
    function base64_url_decode($input) {
      return strtr($input, '-_.', '+/=');
    }
?>
