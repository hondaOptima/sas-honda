<?php
    /*Fecha de creación: 31 de agosto del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: 06 de septiembre del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: [fecha]
    Autor: [Autor]*/
    header('Access-Control-Allow-Origin: *');
    include('../../conexion/conexion_intelisis_mirror.php');
    //creación de json
    //$json = '{ "Juego" : [';
    /* Campos de tabla Juego:
    -> Juego
    -> Articulo
    -> Cantidad
    Donde: consultara los diferentes campos Juego
    */
    $query = 'select distinct Juego from Juego';
    $exec = mysql_query($query); //ejecutar consulta
    if(mysql_num_rows($exec)>0){ //si se encontraron datos en consulta
    //$f = 0;
    $json = '{
        "Status": true,
        "Message": "Successful",
        "Object" : [';
    while( $row = mysql_fetch_array( $exec,MYSQL_ASSOC ) ) {
        if($f == 1){$json.=',';}else{$f = 1;}
        //estructura json con los campos de la base de datos
        $json .= '{
              "Id": "'.$row['Juego'].'",
              "Name": "'.$row['Juego'].'",
              "Articles" : [';
              /*consultar todos los articulos de la inspección
              /* Campos de tabla Articulo:
              -> Articulo
              -> Rama
              -> Descripcion1
              -> Categoria
              -> Fabricante
              -> Unidad
              -> Tipo
              -> Estatus
              -> PrecioLista
              Nota: el $row[Cantidad] son la cantidad de un articulo en el Juego(inspeccion) anteriormente añadido al JSON
              */
              $queryArt = 'select a.*, j.Cantidad from Juego j join Articulo a ON a.Articulo = j.Articulo
                            where j.Juego = "'.$row['Juego'].'"';
              $execArt = mysql_query($queryArt); //ejecutar consulta
              if(mysql_num_rows($execArt)>0){ //si se encontraron datos en consulta
              $a = 0;
              while( $art = mysql_fetch_array( $execArt,MYSQL_ASSOC ) ) {
                  if($a == 1){$json.=',';}else{$a = 1;}
                  //estructura json con los campos de la base de datos
                  $json .= '{
                      "Id": "'.$art['Articulo'].'",
                      "Branch" : "'.$art['Rama'].'",
                      "Description" : "'.$art['Descripcion1'].'",
                      "Category": "'.$art['Categoria'].'",
                      "Maker" : "'.$art['Fabricante'].'",
                      "Unity" : "'.$art['Unidad'].'",
                      "Type": "'.$art['Tipo'].'",
                      "Status" : "'.$art['Estatus'].'",
                      "Price" : "'.$art['PrecioLista'].'",
                      "Quantity" : "'.$art['Cantidad'].'"
                  }';
                }
              }
              $json .='] }';
          }
        $json .='] }';
      }
    else {//si no se encontraron datos
          //Object debe enviarse vacío con sus respectivos campos
      $json = '{
            "Status":	false,
            "Message" : "Not found",
            "Object" : {
                      "Articles": "",
                      "Branch" : "",
                      "Description" : "",
                      "Category": "",
                      "Maker" : "",
                      "Unity" : "",
                      "Type": "",
                      "Status" : "",
                      "Price" : "",
                      "Quantity" : 0
            }
      }';
    }
    echo $json;
?>
