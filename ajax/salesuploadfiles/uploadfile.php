<?php
    /*Fecha de creación: 12 de septimebre del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: [fecha]
    Autor: [Autor]*/
    HEADER('Access-Control-Allow-Origin: *');
    include('../../conexion/conexion.php');

    if (isset($_POST["image"]) && isset($_POST["description"]) && isset($_POST["Vin"])) {
      
      $vin = $_POST["Vin"];
      $concat = "data:image/jpeg;base64,";//especifica que es una imagen
      $Base64Img = trim($_POST['image']);//imagen png codificada en base64
      $imgName = $_POST['description'];//nombre que llevará la imagen
      //$Base64Img = base64_decode(base64_url_decode($Base64Img));//decodificar base64 y nos da un zip
      //$texto = $Base64Img;
      $texto = gzdecode(base64_decode(base64_url_decode($Base64Img)));
      $Base64Img =  gzdecode(base64_decode(base64_url_decode($Base64Img)));//descomprimimos el base64
      $Base64Img = $concat.$Base64Img;//decodificar con caracteres de base64
      $basename = substr($imgName, strrpos($imgName, '/') + 1);
      $col = updateImages($basename);
      $route = base64toImage($Base64Img,$basename,$texto, $vin, $col);
      $json = '{
          "Status": true,
          "Message": "'.$route.'",
          "Object": {
                  "Image" : "",
                  "Description" : ""
          }
        }';
    }
    else {//en caso de que falte algún parámetro
      $json = '{
          "Status": false,
          "Message": "Missing Data",
          "Object": {
                  "Image" : "",
                  "Description" : ""
          }
        }';
    }
    echo $json;

    //método para convertir base64 en imagen
    function base64toImage($b64,$img,$texto, $vin, $col)
    {
      //eliminamos data:image/png; y base64, de la cadena que tenemos
      list(, $b64) = explode(';', $b64);
      list(, $b64) = explode(',', $b64);
      if(!is_dir("../../calendario/ajax/uploads/".$vin)){
        mkdir("../../calendario/ajax/uploads/".$vin);
      }
      if($col == "sei_cha_uno"){
        file_put_contents("../../signature/".$img, base64_decode($b64));
      }else{
        if($col == "sec_firma"){
          file_put_contents("../../firmas/".$img, base64_decode($b64));
        }else{
          file_put_contents("../../calendario/ajax/uploads/".$vin."/".$img, base64_decode($b64));
        }
      }
      //asignamos valor y nombre de nuestra imagen
      // file_put_contents('texto.txt', $texto);
      return "../../calendario/ajax/uploads/".$vin."/".$img;
    }
    //método para cambiar los caracteres falsos de base64
    //Nota: se asignaron caracteres falsos para poder enviarse por URL
    function base64_url_decode($input) {
      return strtr($input, '-_.', '+/=');
    }

    function gzdecode($data)
    {
       return gzinflate(substr($data,10,-8));
    }
?>
