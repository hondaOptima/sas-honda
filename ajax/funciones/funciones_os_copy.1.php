<?php

function insertOS($data)
{
	$insertServicio = "
	INSERT INTO
		serviceOrders
	VALUES (
	 NULL,
	 '".date('Y-m-d H:i:s')."',
	 '".$data['O']['InfoServiceModel']['PromiseTime']."',
	 NULL,
	 '".$data['O']['InfoServiceModel']['Tower']."',
	 '".($data['O']['ArticleSetModel']['Rugs'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Plugs'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Tool'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Reflector'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Equalizer'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Plug'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Windows'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Mirror'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Radio'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['SpareTire'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Extinguisher'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['HydraulicJack'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Carnet'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Garment'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Antenna'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Lighter'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Cleaners'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Cables'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['Stereo'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['BodyWork'] = false ? 1 : 2)."',
	 '".($data['O']['ArticleSetModel']['WindShield'] = false ? 1 : 2)."',
	 '".$data['O']['InfoServiceModel']['Fuel']."',
	 '".$data['O']['Observations']."',
	 '',
	 '".$data['V']['Id']."',
	 '".$data['S']['Id']."',
	 '1',
	 '".date('Y-m-d h:i:s')."',
	 '".$data['O']['InfoServiceModel']['Technician']['Id']."',
	 '".$data['V']['Km']."',
	 0,
	 '".$data['Id']."',
	 '".$data['C']['Id']."',
	 0,
	 1,
	 0,
	 0,
	 '',
	 '',
	 '".$data['O']['InfoServiceModel']['PaymentType']."',
	 0,
	 0,
	 '',
	 0,
	 '',
	 '',
	 0,
	 NULL
			)";
	//select last_inserID
	$id=insertOC($insertServicio);

	updateCita($data['Id']);
	insertCliente ($id,$data['C']);  //ok no se necesita
	insertAuto	($id,$data['V']);	//ok
		//insertServicio($id,$json);	//no se necesita
	insertImagenes($id, $data['V']['Vin']);   //ok las fotos reales se insertan aparte
	EnProceso($data['S']['Id'], $data['Id'],$id);
	Pizarron($id, $data['VehicleModel'],$data['ServiceSas']);
	insertMultipuntos($id);//ok
	insertLlantas ($id);   //ok
	insertFrenos  ($id);   //ok
	return $insertServicio." ".$id;
}


function insertOC($q)
{
	mysql_query($q);
	return mysql_insert_id();
}

function Pizarron($idOrden, $modelo, $servicio){
	$queryGetTabulado = "select * from services where ser_vehicleModel = ".$modelo." and ser_serviceType = ".$servicio;
	$execTabulado = mysql_query($queryGetTabulado);
	$objectTabulado = mysql_fetch_object($execTabulado);
	$query = "insert into pizarron values (null, ".$idOrden.", ".$objectTabulado->ser_approximate_duration.", '', 1,1,6, curdate())";
	mysql_query($query);
}

function EnProceso($asesor,$cita,$orden){
	$query = "insert into enproceso values (
		NULL,
		".$asesor.",
		".$cita.",
		".$orden.",
		2
	)";
	mysql_query($query);
}

function insertCliente($IDO,$a)
{
	$query= "
	INSERT INTO
		serviceOrder_clientInformation
	VALUES
		(
	   ".$IDO.",
	   '".$a['Name']." ".$a['LastName']."',
	   '".$a['Email']."',
	   '".$a['Phone']."',
	   '".$a['Phone']."',
	   '".$a['Street']."',
	   '',
	   '".$a['Colony']."',
	   '".$a['PostalCode']."',
	   '".$a['Municiple']."',
	   '".$a['State']."',
	   '".$a['RFC']."',
	   '',
	   '',
	   ''
	   )";
	mysql_query($query);
}


function updateCita($ID)
{
	$query= "
	update
		appointments
	SET
		app_status=1
	WHERE
		app_idAppointment=".$ID."
	   ";
	mysql_query($query);
}

function insertAuto($IDO,$a)
{
	$query = "
	INSERT INTO
		serviceOrder_vehicleInformation
	VALUES
		(
		".$IDO.",
		'".$a['Brand']."',
		'".$a['SubBrand']."',
		'',
		'".$a['Color']."',
		'".$a['Model']."',
		'".$a['Vin']."',
		'".$a['Capacity']."',
		'".$a['Km']."',
		'".$a['Plates']."',
		'".date('Y-m-d H:i:s')."',
		'',
		'',
		'".$a['Motor']."',
		'".$a['Transmision']."'
		)";
	mysql_query($query);
}

function insertServicio($ios,$orden,$vmodelo,$agente)
{
	$articulos = array();
	foreach($orden['ServiceModel'] as $serModel)
	{
		$insert = "
		INSERT INTO serviceOrders_service
		VALUES
		(NULL, ".$serModel['ServiceCatalog']['Id'].",'master',".$ios.",'publico',".$vmodelo.")";
		$ejecucion = mysql_query($insert);
		if(mysql_affected_rows($ejecucion) == 0)
			die('error al insertar');
		foreach($serModel['Articles'] as $art)
		{
			array_push($articulos, $art);
		}
	}


	return $intelisis = array
		(
		"agente" => $agente,
		"articulos" => $articulos
		);

}

function insertImagenes($IDO, $vin)
{
	$query=
	"
	insert INTO serviceOrders_images
	values(
	$IDO,
	'noimagen.png',
	'noimagen.png',
	'noimagen.png',
	'noimagen.png',
	'noimagen.png',
	'".$vin."',
	'noimagen.png',
	'noimagen.png',
	'noimagen.png',
	'',
	'',
	'',
	'')
	";
	mysql_query($query);
}

function insertMultipuntos($IDO)
{
	$query = "
	INSERT INTO multipuntos
	VALUES(
	'NULL',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'3',
	'',
	'',
	'',
	'".$IDO."')";
	mysql_query($query);
}

function insertLlantas($IDO)
{
	$query = "INSERT INTO llantas VALUES(
			'NULL',
			'3',
			'0',
			'0',
			'3',
			'0',
			'0',
			'3',
			'0',
			'0',
			'3',
			'0',
			'0',
			'3',
			'0',
			'0',
			'".$IDO."'

	)";
	mysql_query($cliente);
}

function insertFrenos($id,$json)
{
	$query = "INSERT INTO frenos VALUES(
	'NULL',
	'3',
	'0',
	'0',
	'3',
	'0',
	'0',
	'3',
	'0',
	'0',
	'3',
	'0',
	'0',
	'".$IDO."'
	)";
	mysql_query($query);
}
?>
