<?php

	$curl = curl_init();
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => 'https://hsas.gpoptima.net/cus_optima',
	));
	$resp = curl_exec($curl);
	curl_close($curl);

	$curl_crm = curl_init();
	curl_setopt_array($curl_crm, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => 'https://hsas.gpoptima.net/crm_optima',
	));
	$resp = curl_exec($curl_crm);
	curl_close($curl_crm);

?>