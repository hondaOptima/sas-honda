<?php
	//use files
	require_once('../../conexion/conexion_sas.php');
	require_once('../../conexion/exceptions.php');
	
	class Catalogo extends Conexion_sas
	{
		//attributes
		private $name;
		private $password;
		private $cph;
		
		//methods
		public function get_name() { return $this->name; }
		public function set_name($value) { $this->name = $value; }
		public function set_password($value) { $this->password = $value; }
		public function get_cph() { return $this->cph; }
	
		public function acceso()
		{
			//if no arguments received, create empty object
			if(func_num_args() == 0) 
			{
				$name = '';
				$password = '';
			}
			//if two argument received create object with data
			if(func_num_args() == 2)
			{
				//receive arguments into an array
				$args = func_get_args();
				//get arguments
				$name = $args[0];
				$password = $args[1];
				//open connection to MySql
				parent::open_connection();
				//query
				$query = "select api_user,api_pass from api where api_user = ? and api_pass = sha1(?);";
				//prepare command
				$command = parent::$connection->prepare($query);
				//link parameters
				$command->bind_param('ss',$name,$password);
				//execute command
				$command->execute();
				//link results to class attributes
				$command->bind_result($name, $password);
				//fetch data
				$found = $command->fetch();
				//close command
				mysqli_stmt_close($command);
				//close connection
				parent::close_connection();
				//if not found throw exception
				if(!$found) throw(new RecordNotFoundException());	
			}
		}	
		
		public function auto()
		{
			//if no arguments received, create empty object
			if(func_num_args() == 0) 
			{
				
				$motor = '';
				$version='';
				$modelo = '';
				$rines = '';
				$color = '';
				$placas='';
				$fecha_compra = '';
				$anio='';
				$transmision='';
				$rines='';
				$seguro='';
				$cliente='';
				$mot='';
				$tra='';																
			}
			//if two argument received create object with data
			if(func_num_args() == 1)
			{
				$ids=array();
				//receive arguments into an array
				$args = func_get_args();
				//get arguments
				$vin = $args[0];
				//open connection to MySql
				parent::open_connection();
				//query
			   $query = "
			   SELECT  
			   		`cus_version` , 
			    	`cus_color` ,  
					`cus_submarca` ,  
					`cus_placas` , 
					`cus_fecha_venta` ,
					`cus_modelo`,  
					`cus_name` ,
                    `cus_telephone` ,
                    `cus_email`,
					`cus_motor`,
					`cus_transmision`
				FROM  `customers` 
				WHERE  `cus_serie` =  '".$vin."' limit 1";
				//prepare command
				
				if($command = parent::$connection->prepare($query)){
				
				//link parameters
				$command->bind_param('s',$vin);
				//execute command
				$command->execute();
				//link results to class attributes
				$command->bind_result($version,$color,$m,$placas,$fecha_compra,$anio,$cliente,$tel,$email,$mot,$tra);
				//fill ids array

			
			while ($command->fetch()) array_push($ids,$version,$color,$m,$placas,$fecha_compra,$anio,$cliente,$tel,$email,$mot,$tra);
			//while ($command->fetch()) array_push($version,$color,$m,$placas,$fecha_compra,$anio,$cliente,$tel,$email,$mot,$tra);
			//close command
			mysqli_stmt_close($command);
			//close connection
			parent::close_connection();
			//fill object array
			return $ids;
			//return array
			//return $list;
				}
				else {
				    printf("Errormessage: %s\n", parent::$connection->error);
				}
				
			}
		}	

		public function cph($dato)
		{
			
			// $query = 'SELECT a FROM cph WHERE b ="'.$dato.'" ';
			$query = 'SELECT `COL 1` FROM `TABLE 101` WHERE `COL 2` = "'.$dato.'" limit 1';
			parent::open_connection();
			if($command= parent::$connection->prepare($query)){
			$command->bind_param('i', $dato);
			$command->execute();
			$command->bind_result($a);
			$encontrado = $command->fetch();
			mysqli_stmt_close($command);
			parent::close_connection();
			if($encontrado)
			{
				return substr($a,0,-3);
			}
			}
		}
		
		
		public function seminuevos()
		{
			$query = 'SELECT a FROM cph WHERE b ="'.$dato.'" ';
			parent::open_connection();
			if($command= parent::$connection->prepare($query)){
			$command->bind_param('i', $dato);
			$command->execute();
			$command->bind_result($version,$color,$modelo,$placas,$fecha_compra,$anio,$cliente);
				//fill ids array
			while ($command->fetch()) array_push($ids,$version,$color,$modelo,$placas,$fecha_compra,$anio,$cliente);
			
			mysqli_stmt_close($command);
			parent::close_connection();
			}
		}
		
		public function accesoSeminuevo()
		{
			//if no arguments received, create empty object
			if(func_num_args() == 0) 
			{
				$name = '';
				$password = '';
				return false;
			}
			//if two argument received create object with data
			if(func_num_args() == 2)
			{
				//receive arguments into an array
				$args = func_get_args();
				//get arguments
				$name = $args[0];
				$password = $args[1];
				//open connection to MySql
				parent::open_connection();
				//query
				$query = "select api_user,api_pass from api where api_user = ? and api_pass = sha1(?);";
				//prepare command
				$command = parent::$connection->prepare($query);
				//link parameters
				$command->bind_param('ss',$name,$password);
				//execute command
				$command->execute();
				//link results to class attributes
				$command->bind_result($name, $password);
				//fetch data
				$found = $command->fetch();
				//close command
				mysqli_stmt_close($command);
				//close connection
				parent::close_connection();
				//if not found throw exception
				if(!$found) return false; else return true;	
			}
		}
		function models($m) {
				$m = strtoupper($m);
				$word_found = "";
				$words = array("ACCORD", "CIVIC", "CITY", "HR-V", "FIT", "CRV",
				"ODYSSEY", "RIDGELINE", "PILOT");
				$count_words = count($words);
				for($i = 0; $i < $count_words; $i++){
				if (strpos($m, $words[$i]) !== false) {
				$word_found = $words[$i];
				$i = $count_words+1;
				}
			}
			
			return $word_found;
		}
		
		function modelExist($m) {
				$m = strtoupper($m);
				$word_found = "";
				$exist = 0;
				$words = array("ACCORD", "CIVIC", "CITY", "HR-V", "FIT", "CRV",
				"ODYSSEY", "RIDGELINE", "PILOT");
				$count_words = count($words);
				for($i = 0; $i < $count_words; $i++){
					if (strpos($m, $words[$i]) !== false) {
						$word_found = $words[$i];
						$exist = 1;
						$i = $count_words+1;
					}
				}
			
			return $exist;
		}
		
		function findVersion($text_in){

			$text_in = trim($text_in, " ");
			$word_found = "";
			$words = array("EX", "LX", "UNIQ", "EXL NAVI", "i-Style", "EXL ", 
			"COOL", "TOURING", "HIT", "TURBO", "EPIC", "FUN");
			$count_words = count($words);
			for($i = 0; $i < $count_words; $i++){
			if (strpos($text_in, $words[$i]) !== false) {
			$word_found = $words[$i];
			$i = $count_words+1;
			}
			}     
			return trim($word_found, " ");
		}
	}
?>


