<?php
	require_once('../../conexion/conexion_crm2.php');

    class Seminuevo extends Conexion_crm
    {
        private $vin = '';
        private $ciudad = '';
        private $modelo = '';
        private $anio = '';
		private $color = '';
		private $precio = '';
		private $km = '';
		private $garantia = '';
        
        public function getVin() { return $this->vin; }
     	public function getCiudad() { return $this->ciudad; }
		public function getModelo() { return $this->modelo; }
		public function getAnio() { return $this->anio; }
		public function getColor() { return $this->color; }
		public function getPrecio() { return $this->precio; }
        public function getKM() { return $this->km; }
		public function getGarantia() { return $this->garantia; }
        
        public function __construct()
        {
            $argumentos = func_get_args();
            if(func_num_args() == 0)
            {
                $this->vin = '';
                $this->ciudad = '';
                $this->modelo = '';
                $this->anio = '';
                $this->color = '';
				$this->precio = '';
				$this->km = '';
				$this->garantia = '';
            }
            if(func_num_args() == 1)
            {
                $instruccion = 'select inv_cd, inv_auto, inv_anio, inv_color, sem_precio, sem_km, sem_garantia from inventario i join seminuevos s on i.inv_vin=s.sem_vin where inv_vin=?';
                parent::open_connectioncrm();
                $comando = parent::$connection->prepare($instruccion);
                $comando->bind_param('s', $argumentos[0]);
                $comando->execute();
                $comando->bind_result($ciudad, $modelo, $anio, $color, $precio, $km, $garantia);
				$encontrado = $comando->fetch();
                mysqli_stmt_close($comando);
                parent::close_connectioncrm();
				if($encontrado)
				{
					$this->vin = $argumentos[0];
					$this->ciudad = $ciudad;
					$this->modelo =  $modelo;
					$this->anio = $anio;
					$this->color = $color;
					$this->precio = $precio;
					$this->km = $km;
					$this->garantia = $garantia;
				}
				else
				{
					$this->vin = '';
					$this->ciudad = '';
					$this->modelo = '';
					$this->anio = '';
					$this->color = '';
					$this->precio = '';
					$this->km = '';
					$this->garantia = '';
				}
				
			}
        }
    }
?>