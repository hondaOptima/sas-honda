<?php
    /*Fecha de creación: 14 de Octubre del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: [fecha]
    Autor: [Autor]*/
    header('Access-Control-Allow-Origin: *');
    
    echo getJsonStructure(true);


    function getJsonStructure($vari)
    {
        $json = '';
        if($vari)
        {
            $json = '{
                    "Status": true,
                    "Message": "Successful",
                    "Object" : null
                    }';   
        }
        else{
             $json = '{
                    "Status": false,
                    "Message": "Error",
                    "Object" : null
                    }';   
        }
       
        return $json;     
    } 

?>
