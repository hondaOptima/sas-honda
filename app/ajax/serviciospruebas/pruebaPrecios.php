<?php
    /*Fecha de creación: 14 de septiembre del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: [fecha]
    Autor: [Autor]*/
    header('Access-Control-Allow-Origin: *');
    include('../../conexion/conexion.php');
    //creación de json
    /* Campos de tabla services:
    -> ser_idService
    -> ser_price
    -> ser_refacciones
    -> ser_comercial
    -> ser_vehicleModel
    -> ser_serviceType
    -> ser_isActive
    -> ser_approximate_duration
    -> ser_tipo
    -> ser_actual
    Donde: consultara solo el precio en base a los parámetros
    */
    if (isset($_GET["model"]) && isset($_GET["service"])) { //parametros
      //$query = 'select * from services';
      $query = 'select distinct(ser_comercial) from services where ser_vehicleModel = '.$_GET["model"].' and ser_serviceType = '.$_GET["service"].' LIMIT 1';
      $exec = mysql_query($query); //ejecutar consulta
      if(mysql_num_rows($exec)>0){ //si se encontraron datos en consulta
        $json = '{
            "Status": true,
            "Message": "Successful",
            "Object" : ';
        while( $row = mysql_fetch_array( $exec,MYSQL_ASSOC ) ) {
            //estructura json con los campos de la base de datos
            $json .= '{
                  "Price": "'.$row['ser_comercial'].'"
            }
          }';
        }
      }
      else {//si no se encontraron datos
            //Object debe enviarse vacío con sus respectivos campos
        $json = '{
              "Status":	false,
              "Message" : "Not found",
              "Object" : {
                        "Price" : 0
              }
        }';
      }
    }
    else {
      $json = '{
            "Status":	false,
            "Message" : "Missing data",
            "Object" : {
                      "Price" : 0
            }
      }';
    }
    echo $json;
?>
