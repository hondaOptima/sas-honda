<?php
    /*Fecha de creación: 12 de septimebre del 2016
    Autor: José Rangel*/
    /*Fecha de modificación: [fecha]
    Autor: [Autor]*/
    HEADER('Access-Control-Allow-Origin: *');
    include('../../conexion/conexion.php');

    if (isset($_POST["image"]) && isset($_POST["description"]) && isset($_POST["Vin"])) {
      
      $vin = $_POST["Vin"];
      $concat = "data:image/jpeg;base64,";//especifica que es una imagen
      $Base64Img = trim($_POST['image']);//imagen png codificada en base64
      $imgName = $_POST['description'];//nombre que llevará la imagen
      //$Base64Img = base64_decode(base64_url_decode($Base64Img));//decodificar base64 y nos da un zip
      //$texto = $Base64Img;
      $texto = gzdecode(base64_decode(base64_url_decode($Base64Img)));
      $Base64Img =  gzdecode(base64_decode(base64_url_decode($Base64Img)));//descomprimimos el base64
      $Base64Img = $concat.$Base64Img;//decodificar con caracteres de base64
      $basename = substr($imgName, strrpos($imgName, '/') + 1);
      $col = updateImages($basename);
      $route = base64toImage($Base64Img,$basename,$texto, $vin, $col);
      $json = '{
          "Status": true,
          "Message": "'.$route.'",
          "Object": {
                  "Image" : "",
                  "Description" : ""
          }
        }';
    }
    else {//en caso de que falte algún parámetro
      $json = '{
          "Status": false,
          "Message": "Missing Data",
          "Object": {
                  "Image" : "",
                  "Description" : ""
          }
        }';
    }
    echo $json;

    function updateImages($image){
      $name =  trim(str_replace(range(0,9),"",$image));
      $name = substr($name, strpos($name, "-")); 
      $col = "";
      if($name == "-RightSide.jpg"){
        $col = "sei_inf_der";
      }else{
        if($name == "-LeftSide.jpg"){
          $col = "sei_inf_izq";
        }else{
          if($name == "-FrontSide.jpg"){
            $col = "sei_pos_der";
          }else{
            if($name == "-RearSide.jpg"){
              $col = "sei_pos_izq";
            }else{
              if($name == "-Dashboard.jpg"){
                $col = "sei_tablero";
              }else{
                if($name == "-OptionalOne.jpg"){
                  $col = "sei_extra_uno";
                }else{
                  if($name == "-OptionalTwo.jpg"){
                    $col = "sei_extra_dos";
                  }else{
                    if($name == "-OptionalTree.jpg"){
                      $col = "sei_extra_tres";
                    }else{
                      if($name == "-signature.jpg"){
                        $col = "sec_firma";
                      }else{
                        if($name == "-damage.jpg"){
                          $col = "sei_cha_uno";
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      $idCita = preg_replace("/[^0-9]/","",$image);
      $exec = mysql_query("select * from serviceOrders where seo_IDapp = ".$idCita." limit 1");
      $orden = mysql_fetch_object($exec);
      if($col == "sei_cha_uno"){
        $qu="update serviceOrders_images set sei_cha_uno='".$image."' where sei_ID=".$orden->seo_idServiceOrder."";
        mysql_query($qu);
      }else{
        if($col == "sec_firma"){
          $query = "update serviceOrder_clientInformation set sec_firma = '".$image."' where sec_idClientInformation = ".$orden->seo_idServiceOrder;
          mysql_query($query);
        }else{
          $query = "update serviceOrders_images set ".$col." = '".$image."' where sei_ID = ".$orden->seo_idServiceOrder;
          mysql_query($query);
        }
      }
      return $col;
    }

    //método para convertir base64 en imagen
    function base64toImage($b64,$img,$texto, $vin, $col)
    {
      //eliminamos data:image/png; y base64, de la cadena que tenemos
      list(, $b64) = explode(';', $b64);
      list(, $b64) = explode(',', $b64);
      if(!is_dir("../../calendario/ajax/uploads/".$vin)){
        mkdir("../../calendario/ajax/uploads/".$vin);
      }
      if($col == "sei_cha_uno"){
        file_put_contents("../../signature/".$img, base64_decode($b64));
      }else{
        if($col == "sec_firma"){
          file_put_contents("../../firmas/".$img, base64_decode($b64));
        }else{
          file_put_contents("../../calendario/ajax/uploads/".$vin."/".$img, base64_decode($b64));
        }
      }
      //asignamos valor y nombre de nuestra imagen
      // file_put_contents('texto.txt', $texto);
      return "../../calendario/ajax/uploads/".$vin."/".$img;
    }
    //método para cambiar los caracteres falsos de base64
    //Nota: se asignaron caracteres falsos para poder enviarse por URL
    function base64_url_decode($input) {
      return strtr($input, '-_.', '+/=');
    }

    function gzdecode($data)
    {
       return gzinflate(substr($data,10,-8));
    }
?>
