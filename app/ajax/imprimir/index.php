<?php 
require_once('../../conexion/conexion_crm.php');
require_once('../funciones/funciones.php');
require_once('../funciones/tpl_impresion.php');

$info=crmVenta($_GET['id']);//traer informacion
//print_r($info);

if($info['codInt']=='' || $info['transmision']=='')
{
  echo 'Asegúrese de de que tenga los siguientes datos:
  <br><br>
  * Aprobación de facturación<br>
  * Color Interior<br>
  * Transmisión Requerida<br><br>
  No es necesario que tenga la fecha del calendario de entrega<br>
  La fecha tomada es la del dia de hoy.
  ';
}
else
{
tpl_impresion($info);       //tabla de informacion
}
?>