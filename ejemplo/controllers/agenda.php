<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agenda extends CI_Controller {
	public $id,$cd,$nivel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Calendariomodel');
		$this->load->model('Contactomodel');
        $this->load->model('Vendedoresmodel');
		
        $this->load->helper('url');
		$this->load->library('email');
        $this->load->library('session');
	    $this->load->library('textmagic');
		$this->load->library('form_validation');
	    $this->load->library("curl");
		
	    session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}
		$this->id=$_SESSION['id'];
		$this->cd=$_SESSION['ciudad'];
		$this->nivel=$_SESSION['nivel'];
		$this->seguridad=$_SESSION['seguridad'];
    }
 
    function index()
    {   
	    $data['tipo_mensage'] = $this->Contactomodel->getTipoMensage();
		$data['flash_message'] = $this->session->flashdata('message');
        $data['id_vendedor']='no';

        if($this->seguridad=='C'){
		$data['todo_lista'] = $this->Contactomodel->getTodocontactoaxv($this->id);	
		$data['todo_lista_opciones'] = $this->Contactomodel->getTodocontactoOpciones($this->id);
		$data['todo_tareas_pendientes'] = $this->Contactomodel->getTareasPendientesUser($this->id);		
		$data['todo_tareas_pendientes_admin'] = $this->Contactomodel->getTareasPendientesAdmin($this->id);
		$data['view']='get-tarea.php';
		$data['viewb']='get-tarea-actividad.php';	
			}
		elseif($this->seguridad=='B' ){
		 $this->load->model('Vendedoresmodel');
         $data['todo_contactos'] = $this->Contactomodel->getTodocontactoax();
		 $data['todo_lista_opciones'] = $this->Vendedoresmodel->getTodoaNo($this->cd);
		 $data['todo_lista'] = $this->Vendedoresmodel->getTodoaNoT($this->cd);
		 $data['todo_tareas_pendientes'] = $this->Contactomodel->getTareasPendientes($this->cd);
		 $data['todo_tareas_pendientes_admin'] = $this->Contactomodel->getTareasPendientesAdminCd($this->cd);
		 $data['view']='get-tarea-admin.php';
	     $data['viewb']='get-tarea-actividad-admin.php';
		}
		
		elseif($this->seguridad=='A' ){
		 $this->load->model('Vendedoresmodel');
         $data['todo_contactos'] = $this->Contactomodel->getTodocontactoax();
		 $data['todo_lista_opciones'] = $this->Vendedoresmodel->getTodoaNoA($this->cd);
		 $data['todo_lista'] = $this->Vendedoresmodel->getTodoaNoT($this->cd);
		 $data['todo_tareas_pendientes'] = $this->Contactomodel->getTareasPendientes($this->cd);
		 $data['todo_tareas_pendientes_admin'] = $this->Contactomodel->getTareasPendientesAdminCd($this->cd);
		 $data['view']='get-tarea-admin.php';
	     $data['viewb']='get-tarea-actividad-admin.php';
		}
   
        $this->load->view('agenda/vistacitas', $data);

    }
	
	 function example()
    {   
	    $data['tipo_mensage'] = $this->Contactomodel->getTipoMensage();
		$data['flash_message'] = $this->session->flashdata('message');
        $data['id_vendedor']='no';

        if($this->nivel=='Vendedor'){
		$data['todo_lista'] = $this->Contactomodel->getTodocontactoaxv($this->id);	
		$data['todo_lista_opciones'] = $this->Contactomodel->getTodocontactoOpciones($this->id);
		$data['todo_tareas_pendientes'] = $this->Contactomodel->getTareasPendientesUser($this->id);		
		$data['todo_tareas_pendientes_admin'] = $this->Contactomodel->getTareasPendientesAdmin($this->id);
		$data['view']='get-tarea.php';
		$data['viewb']='get-tarea-actividad.php';	
			}
		else{
		 $this->load->model('Vendedoresmodel');
         $data['todo_contactos'] = $this->Contactomodel->getTodocontactoax();
		 $data['todo_lista_opciones'] = $this->Vendedoresmodel->getTodoaNo($this->cd);
		 $data['todo_lista'] = $this->Vendedoresmodel->getTodoaNoT($this->cd);
		 $data['todo_tareas_pendientes'] = $this->Contactomodel->getTareasPendientes($this->cd);
		 $data['todo_tareas_pendientes_admin'] = $this->Contactomodel->getTareasPendientesAdminCd($this->cd);
		 $data['view']='get-tarea-admin.php';
	     $data['viewb']='get-tarea-actividad-admin.php';
		}
		
 
        $this->load->view('agenda/vistacitas', $data);

    }
	
	
	function addtarea()
    {   
	
	  $this->form_validation->set_rules('titulo', 'Titulo', 'required');
	  if ($this->form_validation->run())
        {
		$mes=$this->Contactomodel->listames($this->input->post('fei'));
		$dia=$this->Contactomodel->listadia($this->input->post('fei'));
		$fei=$this->Contactomodel->listfecha($this->input->post('fei'));
		$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
		if($hora < 12){$ti="am";}else{$ti="pm";}
		$titulo=$this->input->post('titulo');
        $contacto=$this->input->post('contacto');
		$tmnsage=$this->input->post('tmensage');
        $ms=$mes;
		$onoff=$this->input->post('not');
			
			
		if($this->input->post('contacto')!='generales'){	
		$IDB=$this->Calendariomodel->getIdBitacora($contacto);	
		$todoa= array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$titulo,
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$IDB->bit_IDbitacora,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>$onoff,
					'tipo_mensage_tip_IDtipo_mensage'=>$tmnsage,
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
					
					$todoUA= array(
                    'con_ultima_actividad'=>''.date('Y-m-d').' '.$titulo.' '.$this->input->post('desc').'',
                    );
		$this->Contactomodel->inserttareaa($todoa);
		//$this->Contactomodel->UpdateUltimaActividad($todoUA,$contacto);
		
if($onoff=='on'){

/*		    				
if($tmnsage==1)
{//enviar email
$this->Contactomodel->Sendmensajeuno($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsUno($contacto,$ms,$dia,$hora,$min,$ti);		
}
		 
if($tmnsage==2){
//enviar email
$this->Contactomodel->Sendmensajedos($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsDos($contacto,$ms,$dia,$hora,$min,$ti);	
}

if($tmnsage==3){
//enviar email
$this->Contactomodel->Sendmensajetres($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsTres($contacto,$ms,$dia,$hora,$min,$ti);			
}

if($tmnsage==4){
$this->Contactomodel->Sendmensajecuatro($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsCuatro($contacto,$ms,$dia,$hora,$min,$ti);			
}
*/
		}
		
		}else{
		
		$td= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$titulo,
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>'generales',
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$this->id
                    );
					print_r($td);
		$this->Contactomodel->inserttareag($td);
		}
		$hoy=date('Y').'-'.date('m').'-'.date('d');
        if($fei==$hoy){
		$email=$_SESSION['username'];
		$desc=$this->input->post('desc');		
		$this->Contactomodel->Sendmensajegeneral($titulo,$contacto,$email,$fei,$hora,$min,$desc);
		}
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una Tarea nueva.</div>");            
        redirect('agenda/');
       } else{}
	  
	 }
	 
	 
	 
	 function addtareaPros()
    {   
	
	  $this->form_validation->set_rules('titulo', 'Titulo', 'required');
	  if ($this->form_validation->run())
        {
		$mes=$this->Contactomodel->listames($this->input->post('fei'));
		$dia=$this->Contactomodel->listadia($this->input->post('fei'));
		$fei=$this->Contactomodel->listfecha($this->input->post('fei'));
		$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
		if($hora < 12){$ti="am";}else{$ti="pm";}
		
		$titulo=$this->input->post('titulo');
        $contacto=$this->input->post('contacto');
	    $onoff=$this->input->post('not');
		$reg=$this->input->post('registrado');
		
		
		if($reg=='si'){	
		$IDB=$this->Calendariomodel->getIdBitacora($contacto);	
		              }
		else{
			
			//nombre folder		
$cadena=$this->input->post('nombre');
$string=$this->Contactomodel->limpiarEspacios($cadena);
$random=$this->Contactomodel->randomPass(3);
$folder=$this->Contactomodel->limpiarCaracteresEspeciales($string ).$random;
//asesor
$AGV=$this->input->post('vendedorPros');
       $todo = array(
                    'con_nombre'=>$this->input->post('nombre'),
					'con_apellido'=>'',
					'con_telefono_officina'=>'',
					'con_telefono_casa'=>$this->input->post('telefono'),
					'con_correo'=>$this->input->post('email'),
					'con_correo_b'=>'',
					'con_ocupacion'=>'',
					'fuente_fue_IDfuente'=>7,
					'huser_hus_IDhuser'=>$AGV,
					'con_status'=>'Prospecto',
					'con_titulo'=>'',
					'con_tipo'=>'',
					'con_modelo'=>'',
					'con_ano'=>'',
					'con_color'=>'',
					'con_telefono_off'=>'',
					'con_telefono_off2'=>'',
					'con_radio'=>'',
					'con_notas'=>$this->input->post('desc'),
					'con_folder'=>$folder,
					'publicidad_pub_IDpublicidad'=>6,
					'con_hora'=>$this->input->post('hora'),
					'con_version'=>'',
					'con_ultima_actividad'=>''.date('Y-m-d').' Alta en el Sistema',
					'con_cliente'=>'no',
					'con_pmanejo'=>'',
					'con_buscando_auto'=>'no'
                    );
// To create the nested structure, the $recursive parameter  to mkdir() must be specified.
$structure = './readpdf/web/expedientes/'.$folder;
if (!mkdir($structure, 0777, true)) { die('Failed to create folders...');}else{chmod($structure, 0777);}			
$IDC=$this->Contactomodel->addcontacto($todo);
$this->Contactomodel->addbitacora($IDC->IDC);
//ID bitacora
$IDB=$this->Contactomodel->vbitacora($IDC->IDC);
		
			}			  
		$todoa= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$titulo,
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$IDB->bit_IDbitacora,
					'tipo_actividad_tac_IDtipo_actividad'=>9,
					'act_notificar'=>$onoff,
					'tipo_mensage_tip_IDtipo_mensage'=>0,
					'fuente_IDfiuente'=>7,
					'act_confirmacion'=>''
                    );
					
		
				
		$this->Contactomodel->inserttareaa($todoa);
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una Tarea nueva.</div>");            
        redirect('agenda/');
       } else{}
	  
	 }
	 
	 
	 function addtarea2()
    {   
	
	  $this->form_validation->set_rules('titulo', 'Titulo', 'required');
	  if ($this->form_validation->run())
        {
		$mes=$this->Contactomodel->listames($this->input->post('fei'));
		$dia=$this->Contactomodel->listadia($this->input->post('fei'));
		$fei=$this->Contactomodel->listfecha($this->input->post('fei'));
		$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
		if($hora < 12){$ti="am";}else{$ti="pm";}
		$titulo=$this->input->post('titulo');
        $contacto=$this->input->post('contacto2');
		$tmnsage=$this->input->post('tmensage');
		$tipoCita=$this->input->post('fuente');
        $ms=$mes;
		$onoff=$this->input->post('not');
			
			
		if($this->input->post('contacto')!='generales'){	
		$IDB=$this->Calendariomodel->getIdBitacora($contacto);	
		$todoa= array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$titulo,
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$IDB->bit_IDbitacora,
					'tipo_actividad_tac_IDtipo_actividad'=>$tipoCita,
					'act_notificar'=>$onoff,
					'tipo_mensage_tip_IDtipo_mensage'=>$tmnsage,
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
					
					$todoUA= array(
                    'con_ultima_actividad'=>''.date('Y-m-d').' '.$titulo.' '.$this->input->post('desc').'',
                    );
		$this->Contactomodel->inserttareaa($todoa);
		//$this->Contactomodel->UpdateUltimaActividad($todoUA,$contacto);
		
if($onoff=='on'){

/*		    				
if($tmnsage==1)
{//enviar email
$this->Contactomodel->Sendmensajeuno($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsUno($contacto,$ms,$dia,$hora,$min,$ti);		
}
		 
if($tmnsage==2){
//enviar email
$this->Contactomodel->Sendmensajedos($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsDos($contacto,$ms,$dia,$hora,$min,$ti);	
}

if($tmnsage==3){
//enviar email
$this->Contactomodel->Sendmensajetres($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsTres($contacto,$ms,$dia,$hora,$min,$ti);			
}

if($tmnsage==4){
$this->Contactomodel->Sendmensajecuatro($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsCuatro($contacto,$ms,$dia,$hora,$min,$ti);			
}
*/
		}
		
		}else{
		
		$td= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$titulo,
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>'generales',
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$this->id
                    );
					print_r($td);
		$this->Contactomodel->inserttareag($td);
		}
		$hoy=date('Y').'-'.date('m').'-'.date('d');
        if($fei==$hoy){
		$email=$_SESSION['username'];
		$desc=$this->input->post('desc');		
		$this->Contactomodel->Sendmensajegeneral($titulo,$contacto,$email,$fei,$hora,$min,$desc);
		}
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una Tarea nueva.</div>");            
        redirect('agenda/');
       } else{}
	  
	 }
	 
	 
	 
	  function addtarea3()
    {   
	
	  $this->form_validation->set_rules('titulo', 'Titulo', 'required');
	  if ($this->form_validation->run())
        {
		
		$fei=$this->input->post('newdateop');
		list($di,$mss,$an)=explode('/',$fei);
		$fei=$an.'-'.$mss.'-'.$di;
		$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
		if($hora < 12){$ti="am";}else{$ti="pm";}
		$titulo=$this->input->post('titulo');
        $contacto=$this->input->post('contacto3');
		$tmnsage=$this->input->post('tmensage');
		$fuente=0;
      
		$onoff=$this->input->post('not');
			
			
		if($this->input->post('contacto')!='generales'){	
		$IDB=$this->Calendariomodel->getIdBitacora($contacto);	
		$todoa= array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$titulo,
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$IDB->bit_IDbitacora,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>$onoff,
					'tipo_mensage_tip_IDtipo_mensage'=>$tmnsage,
					'fuente_IDfiuente'=>$fuente,
					'act_confirmacion'=>''
                    );
					
					$todoUA= array(
                    'con_ultima_actividad'=>''.date('Y-m-d').' '.$titulo.' '.$this->input->post('desc').'',
                    );
		$this->Contactomodel->inserttareaa($todoa);
		//$this->Contactomodel->UpdateUltimaActividad($todoUA,$contacto);
		
if($onoff=='on'){

/*		    				
if($tmnsage==1)
{//enviar email
$this->Contactomodel->Sendmensajeuno($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsUno($contacto,$ms,$dia,$hora,$min,$ti);		
}
		 
if($tmnsage==2){
//enviar email
$this->Contactomodel->Sendmensajedos($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsDos($contacto,$ms,$dia,$hora,$min,$ti);	
}

if($tmnsage==3){
//enviar email
$this->Contactomodel->Sendmensajetres($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsTres($contacto,$ms,$dia,$hora,$min,$ti);			
}

if($tmnsage==4){
$this->Contactomodel->Sendmensajecuatro($contacto,$this->id,$this->cd,$ms,$dia,$hora,$min,$ti);
//enviar sms
$this->Contactomodel->SendSmsCuatro($contacto,$ms,$dia,$hora,$min,$ti);			
}
*/
		}
		
		}else{
		
		$td= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$titulo,
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>'generales',
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$this->id
                    );
					print_r($td);
		$this->Contactomodel->inserttareag($td);
		}
		$hoy=date('Y').'-'.date('m').'-'.date('d');
        if($fei==$hoy){
		$email=$_SESSION['username'];
		$desc=$this->input->post('desc');		
		$this->Contactomodel->Sendmensajegeneral($titulo,$contacto,$email,$fei,$hora,$min,$desc);
		}
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una Tarea nueva.</div>");            
        redirect('agenda/');
       } else{}
	  
	 }
	
	
	
	
	
	function addtareaadmin()
    {   
	
	session_start();
		if ( !isset($this->id) ) {
         redirect('login');
      }
	  $this->load->model('Vendedoresmodel');	
	  $this->load->model('Contactomodel');
	  $this->load->model('Eventosmodel');
	  $this->load->library("curl");
	  $this->load->library('form_validation');
		
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		
        if ($this->form_validation->run())
        {
		
		   $llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('-',$llegada);
			$mes=date('m', strtotime($mes));
			
			$fei=$ano.'-'.$mes.'-'.$dia;
			$time=$this->input->post('hora');
			list($hora,$min)=explode(":",$time);
			
		
		$idh=$this->id;
		
		$VLCON=$this->input->post('vendedor');
		$TCON=count($VLCON);
		
		if($TCON==1)
		{
		foreach($VLCON as $NVAR){$NVAR;}
		if($NVAR=='no')
		{//insertara a todos los vendedores
		
		//obtener lista de vendores de Tijuana o Mexicali
		$LSTDV=$this->Vendedoresmodel->ListadeVendedoresArray($this->cd);

		foreach($LSTDV as $LSTDVEACH){
			
			
			$td= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>'generales',
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$LSTDVEACH->hus_IDhuser
                    );
		//insert de en tareas generales	a vendedores y recepcionistas		
		$this->Contactomodel->inserttareag($td);
		
		//funcion email a cada uno de los vendedores en la lista.
		
		$this->Calendariomodel->SendEmailTareaVendedor($_SESSION['username'],$LSTDVEACH->hus_IDhuser,$fei,$hora,$min,$this->input->post('titulo'),$this->input->post('desc'));
		
		
		//Obtener info vendedor
		$huser=$this->Vendedoresmodel->getId($LSTDVEACH->hus_IDhuser);		
		$CAU=$huser[0]->hus_celular;
	    //funcion enviar sms a un vendedor
	    echo $CAU;		
		if($CAU)
		{
		if($hora < 12){$ty="am";}else{$ty="pm";}
		$this->Calendariomodel->SendSmsTareaVendedor($CAU,$fei,$hora,$min,$ty,$this->input->post('titulo'),$this->input->post('desc'));
		}
		
		
		
		echo 'enviado';
	    }
		
		
		}
		else
		{//insertar a un vendedor
		
		//array de datos a insertar en la tabla generales
		$td= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>'generales',
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$NVAR
                    );
					
		//insert de en tàrea generales			
		$this->Contactomodel->inserttareag($td);
		
		//funcion email a un vendedor
		$this->Calendariomodel->SendEmailTareaVendedor($_SESSION['username'],$NVAR,$fei,$hora,$min,$this->input->post('titulo'),$this->input->post('desc'));
				
		//Obtener info vendedor
		$huser=$this->Vendedoresmodel->getId($NVAR);		
		$CAU=$huser[0]->hus_celular;
	    //funcion enviar sms a un vendedor		
		if($CAU)
		{
		if($hora < 12){$ty="am";}else{$ty="pm";}
		$this->Calendariomodel->SendSmsTareaVendedor($CAU,$fei,$hora,$min,$ty,$this->input->post('titulo'),$this->input->post('desc'));
		}
		
		
		
		echo 'enviado';
		}
		}
		
		if($TCON>1)
		{
		//echo 'enviar a mas de uno , no a todos';
		
		foreach($VLCON as $NVAR){ $NVAR;
		
		//array de datos a insertar en la tabla generales
		$td= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>'generales',
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$NVAR
                    );
					
		//insert de en tàrea generales			
		$this->Contactomodel->inserttareag($td);
		
		//funcion email a un vendedor
		$this->Calendariomodel->SendEmailTareaVendedor($_SESSION['username'],$NVAR,$fei,$hora,$min,$this->input->post('titulo'),$this->input->post('desc'));
		
		
		//Obtener info vendedor
		$huser=$this->Vendedoresmodel->getId($NVAR);		
		$CAU=$huser[0]->hus_celular;
	    //funcion enviar sms a un vendedor		
		if($CAU)
		{
		if($hora < 12){$ty="am";}else{$ty="pm";}
		$this->Calendariomodel->SendSmsTareaVendedor($CAU,$fei,$hora,$min,$ty,$this->input->post('titulo'),$this->input->post('desc'));
		}
		
		
		echo 'enviado';
		
		}
		}
		
	   
	  	

        }else{}
		
    }
	
	
	

	
	
	
	
function editartareavendedor($ida,$idh,$ty)
    {
	$this->form_validation->set_rules('titulo', 'Titulo', 'required');
	$this->form_validation->set_rules('fei', 'Fecha de Inicio', 'required');
	$this->form_validation->set_rules('status', 'Estado', 'required');
			
if ($this->form_validation->run()){
$llegada=$this->input->post('fei');list($mes,$dia,$ano)=explode('/',$llegada);$fei=$ano.'-'.$mes.'-'.$dia;
$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
$act=$this->input->post('categoria');
			if($act=='generales')
			{
$todog= array( 'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fei,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>$this->input->post('categoria'),
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$idh,
                    );
$td=$todog;$this->Contactomodel->updategenerales($td,$ida);
          }else{		
$IDB=$this->Contactomodel->idBitacora($act);		
$todo = array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$IDB->bit_IDbitacora,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>$this->input->post('not'),
                    );
$this->Contactomodel->insertactividad($todo,$ida);
}
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");
redirect('agenda');
}
		
else{
		$data['idactv']=$ida;
		$data['idh']=$idh;
		$data['type']=$ty;
		$data['todo_contactos'] = $this->Contactomodel->getTodocontactoaxv($this->id);	
		if($ty=="act"){
		$data['todo_actividades'] = $this->Contactomodel->getActividadesview($ida);}
		else{
		$data['todo_generales'] = $this->Contactomodel->gettareasgenerales($ida);}
		
		if($ty=="act"){
			
		$data['tipo_mensage'] = $this->Contactomodel->getTipoMensage();	
        $this->load->view('agenda/editartareavendedoract', $data);
		}
		else{
		$this->load->view('agenda/editartareavendedorgen', $data);	
			
			}
		}
	
	}
	

function inserttarea($idh)
	{
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	$this->load->library('form_validation');
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		$this->form_validation->set_rules('fei', 'Fecha de Inicio', 'required');
		$this->form_validation->set_rules('fef', 'Fecha Fin', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required');
			
 
        if ($this->form_validation->run())
        {
		
		$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			
			
			$llegadab=$this->input->post('fef');
			list($mes,$dia,$ano)=explode('/',$llegadab);
			$fef=$ano.'-'.$mes.'-'.$dia;
			
			$time=$this->input->post('hora');
			list($hora,$min)=explode(":",$time);
			
		$todoa= array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fef,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'oportunidades_opo_IDoportunidad'=>$this->input->post('categoria'),
					'act_hora'=>$hora,
					'act_min'=>$min
                    );
					
					$todog= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fef,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>$this->input->post('categoria'),
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$idh,
					
                    );
					$this->load->model('Contactomodel');
			if($this->input->post('categoria')=='generales'){
				$td=$todog;$this->Contactomodel->inserttareag($td);}
		else{$td=$todoa; $this->Contactomodel->inserttareaa($td);}		
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una Tarea nueva.</div>");            
            redirect('agenda');
        }	
		
		
    }
	
	
	
	
function updatetarea($ida,$idh,$ty){   

$this->form_validation->set_rules('titulo', 'Titulo', 'required');
$this->form_validation->set_rules('fei', 'Fecha de Inicio', 'required');
$this->form_validation->set_rules('status', 'Estado', 'required');
			
if ($this->form_validation->run()){
$fei=$this->Contactomodel->fechaslash($this->input->post('fei'));
$fef=$fei;
$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
$act=$this->input->post('categoria');
if($act=='generales'){
			
$todog= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fef,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>$this->input->post('categoria'),
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$idh,
                    );
$td=$todog;$this->Contactomodel->inserttareagB($td,$ida);
}else{
$IDB=$this->Contactomodel->idBitacora($act);	
$todo = array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fef,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$IDB->bit_IDbitacora,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>$this->input->post('not'),
					'tipo_mensage_tip_IDtipo_mensage'=>$this->input->post('tmensage'),
                    );
$this->Contactomodel->updatetarea($todo,$ida);
}
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");redirect('agenda');
} else{
$data['idactv']=$ida;
$data['idh']=$idh;
$data['type']=$ty;
if($ty=="act"){
		$data['todo_actividades'] = $this->Contactomodel->getActividadesview($ida);}
		else{
		$data['todo_generales'] = $this->Contactomodel->gettareasgenerales($ida);}
 $this->load->view('agenda/editartareavendedoract', $data);
} }
	
	
function deletegeneralescal($ida)
    {
if ($this->Contactomodel->deletegenerales($ida)) {
$this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una Tarea </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('agenda');
}
	
function deletetareascal($ida)
    {
$this->load->model('Contactomodel');
        if ($this->Contactomodel->deletetareas($ida)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una Tarea </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('agenda');
 
    }
	
	
	
	
	
	
	
	 function editartareaadmin($ida,$idh,$ty)
    {
		session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
		$this->load->model('Contactomodel');
		$this->load->model('Vendedoresmodel');
		
		 $this->load->library('form_validation');
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		$this->form_validation->set_rules('fei', 'Fecha de Inicio', 'required');
		$this->form_validation->set_rules('fef', 'Fecha Fin', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required');
			
 
        if ($this->form_validation->run())
        {
		
		$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			
			
			$llegadab=$this->input->post('fef');
			list($mes,$dia,$ano)=explode('/',$llegadab);
			$fef=$ano.'-'.$mes.'-'.$dia;
			
			$time=$this->input->post('hora');
			list($hora,$min)=explode(":",$time);
			
			$act=$this->input->post('categoria');
			if($act=='generales')
			{
			
			$todog= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fef,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>$this->input->post('categoria'),
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$idh,
                    );
			
		$td=$todog;$this->Contactomodel->updategenerales($td,$ida);
		
			}
			else{
		$todo = array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fef,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'oportunidades_opo_IDoportunidad'=>$this->input->post('categoria'),
					'act_hora'=>$hora,
					'act_min'=>$min
					
                    );
		
		$this->Contactomodel->insertactividad($todo,$ida);
			}
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");            
            redirect('agenda');
		
		
		}
		
		
		else{
			
		$data['idactv']=$ida;
		$data['idh']=$idh;
		$data['type']=$ty;
		$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoa();
		
		
		
		if($ty=="act"){$data['todo_actividades'] = $this->Contactomodel->getActividadesview($ida);}
		else{$data['todo_generales'] = $this->Contactomodel->gettareasgenerales($ida);}
		
       
		
		if($ty=="act"){$this->load->view('agenda/editartareaadminact', $data);}
		else{$this->load->view('agenda/editartareaadmingen', $data);	}}
	
	}
	
	
	
	function editartareageneralesadmin($ida)
	
	{
		
		session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
		$this->load->model('Contactomodel');
		
		
		
		
		$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			
			
			
			$fef=$fei;
			
			$time=$this->input->post('hora');
			list($hora,$min)=explode(":",$time);
			
			$act=$this->input->post('categoria');
		
		 $idv=$this->input->post('vendedor');if($idv==""){$idv=$this->id;}
			
			$todog= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fef,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>$this->input->post('categoria'),
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$idv,
                    );
			
		$td=$todog;$this->Contactomodel->updategenerales($td,$ida);
		
			
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");            
            redirect('agenda');
		
		
	
		
		
  }
	
	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/contacto.php */


