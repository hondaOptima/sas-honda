<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

   public function __construct()
   {
      session_start();
      parent::__construct();
	  
        $this->load->model('admin_model');
	    $this->load->helper('url');
		$this->load->library('email');
        $this->load->library('session');
		$this->load->helper('form');
        $this->load->library('form_validation');
		
		
   }

	public function index()
   {
 
      if ( isset($_SESSION['username']) ) {
         redirect('login');
      }
	    $data['flash_message'] = $this->session->flashdata('message');
      $this->load->view('seguridad/login',$data);
	  // $this->load->view('users/mante',$data);
	  
	
   }
   
   	function inicio()
   {
      if ( isset($_SESSION['username']) ) {
         redirect('login');
      }
	  $data['flash_message'] = $this->session->flashdata('message');

      $this->load->library('form_validation');
      $this->form_validation->set_rules('email_address', 'Direccion de correo electrónico, ', 'valid_email|required');
      $this->form_validation->set_rules('password', 'Contraseña, ', 'required|min_length[4]');

//if ($this->input->post('email_address') == 'liusber_00@hotmail.com' ) {

     if ( $this->form_validation->run() !== false ) {
         // then validation passed. Get from db
         
         $this->form_validation->run();
         $res = $this->admin_model->verify_user($this->input->post('email_address'),$this->input->post('password'));

         if ( $res !== false ) {
			 
			 $this->load->model('Vendedoresmodel');
			$info= $this->Vendedoresmodel->getLogin($this->input->post('email_address'));
			 
			$_SESSION['username'] = $this->input->post('email_address');
			$_SESSION['id'] = $info->hus_IDhuser;
			$_SESSION['name'] = $info->hus_nombre;
			$_SESSION['apellido'] = $info->hus_apellido;
			$_SESSION['nivel'] = $info->hus_nivel;
			$_SESSION['seguridad'] = $info->hus_seguridad;
			$_SESSION['ciudad'] = $info->hus_ciudad;
			$_SESSION['tipocontactoadmin']='Proceso%20de%20Venta';
			$_SESSION['tipocontacto']='Prospecto';
			
			if($this->input->post('email_address')=='auxventas@hondaoptima.com' || $this->input->post('email_address')=='monica@hondaoptima.com' || $this->input->post('email_address')=='eruiz@hondaoptima.com' || $this->input->post('email_address')=='auxmex@hondaoptima.com' || $this->input->post('email_address')=='contamxli@hondaoptima.com' || $this->input->post('email_address')=='martha@hondaoptima.com' ){
				redirect('contacto/seguimiento');
				}
			elseif($this->input->post('email_address')=='josemaciel@hondaoptima.com' || $this->input->post('email_address')=='sgutierrez@hondaoptima.com'){redirect('reporte/dashboard');}
			else{redirect('contacto');}
         }

      }
 $this->session->set_flashdata('message', '<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>La Direccion de correo electr&oacute;nico o la contrase&ntilde;a introducidos no son correctos</div>');   
      redirect('login');
   }
   
   
   
    	
   
   
   
   
   
    function recuperar()
    {
		$data['flash_message'] = $this->session->flashdata('message','bien');
		
		
		
		$this->load->library('form_validation');
      $this->form_validation->set_rules('email_address', 'Direccion de correo electrónico,', 'valid_email|required');
     

      if ( $this->form_validation->run() !== false ) {
        
         $this->load->model('Vendedoresmodel');
         $mail=$this->input->post('email_address');
		 $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";$cad = "";
         for($i=0;$i<12;$i++) {$cad .= substr($str,rand(0,62),1);}$cad;
		 
			 if ($this->Vendedoresmodel->repassword($this->input->post('email_address'),$cad) !== false) {
				 
			$this->load->library('email');
			 $this->email->set_mailtype("html");
		    $this->email->from('casa@hondaoptima.com', 'Honda Optima');
            $this->email->to($mail);
            $this->email->subject('Recuperacion de Contraseña');
            $this->email->message('
			<table><tr><td>
			
			</td></tr></table>
			<br><br>
			Su nueva contraseña es: '.$cad.'
			<br><br><br>
			<hr/>
			<table><tr><td>
			
			</td></tr></table>
			
			');
			
            $this->email->send();
			
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Una nueva contraseña a  sido enviada a su cuenta de correo electrónico.</div>");	 
       
			redirect('login/recuperar');                      
        } else {
			
			
			
			$this->session->set_flashdata('message', '<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>Usuario o Contraseaña incorrecto !</div>'); 
			redirect('login/recuperar'); 
        }
			 
        
      }
		
		
		
        
        $this->load->view('seguridad/recovery',$data);
    }
   

   public function logout()
   {
      session_destroy();
      $this->load->view('users/login');
   }
}

