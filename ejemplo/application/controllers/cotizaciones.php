<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizaciones extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cotizacionesmodel');
		       $this->load->model('Contactomodel');
        
        
		$this->load->helper('url');
		$this->load->library('email');
        $this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");
		 $this->load->library("Pdf");
		session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
    }

function CrearCotizacion(){
		 $data['flash_message'] = $this->session->flashdata('message');
		 $segu=$_SESSION['seguridad'];
		 
		 if($segu=='A'){
		$data['lista_cotizacion'] = $this->Contactomodel->getListaCotizacionHuserCdAll();
		 }
		if($segu=='B'){
		$data['lista_cotizacion'] = $this->Contactomodel->getListaCotizacionHuserCd();	
		}
		if($segu=='C'){
		$data['lista_cotizacion'] = $this->Contactomodel->getListaCotizacionHuser();
		 }
		$this->load->view('contacto/lista_espera_cotizacion',$data);
		}
		
		function eliminarListaCotizacion($id){
		$data['lista_cotizacion'] = $this->Contactomodel->deleteListaCotizacion($id);	
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado la cotizacion.</div>"); 
		redirect('contacto/CrearCotizacion');
		}
		
			function CrearCotizacionHuser(){
		 $data['flash_message'] = $this->session->flashdata('message');
		 
		 $IDCOT=$this->Contactomodel->CreateCotizacionHuser();
         redirect('cotizaciones/LlenarCotizacionHuser/'.$IDCOT.'');
		}
		
		
		
		function LlenarCotizacionHuser($IDCOT){
		list($IDCOT,$IDLC)=explode('-',$IDCOT);	
		
		$data['flash_message'] = $this->session->flashdata('message');
		$data['idc']=0;
		$data['idcot']=$IDCOT;
		$data['idlc']=$IDLC;
		$data['lista_ver'] = $this->Cotizacionesmodel->getListaVersiones($IDCOT);
		$data['lista'] = $this->Cotizacionesmodel->getListaLC($IDLC);
		$data['uno_asesor'] = $this->Cotizacionesmodel->getAsesorT($_SESSION['id']);
		$data['uno_contacto'] = $this->Cotizacionesmodel->getcontactobitacora(0);
		$data['uno_cotizacion'] = $this->Cotizacionesmodel->getCotizacion($IDCOT);
		$data['lista_fichas']=$this->Cotizacionesmodel->getListaFichas();
		
		if(!empty($_GET['ficha'])){
		$data['detalle_fichas']=$this->Cotizacionesmodel->getDetalleFichas($_GET['ficha']);
			}
		 
		$this->load->view('cotizaciones/LlenarCotizacionHuser',$data);
		}
	
	
	function nuevacotizacion($idc){   
$IDCOT=$this->Contactomodel->CreateCotizacion($idc);
redirect('cotizaciones/cotizar/'.$idc.'/'.$IDCOT.'');
    }
	
	function cotizar($idc,$idcot){
$data['flash_message'] = $this->session->flashdata('message');		   
$this->form_validation->set_rules('titulo', 'Titulo', 'required');

if(!empty($_GET['ficha'])){
		$data['detalle_fichas']=$this->Cotizacionesmodel->getDetalleFichas($_GET['ficha']);
			}
		 

if ($this->form_validation->run()){
}
        else
        {
		$data['idc']=$idc;
		$data['idcot']=$idcot;
		$data['lista_ver'] = $this->Contactomodel->getListaVersiones($idcot);
		$data['uno_asesor'] = $this->Contactomodel->getAsesorT($_SESSION['id']);
		$data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);
		$data['uno_cotizacion'] = $this->Contactomodel->getCotizacion($idcot);
		$data['lista_fichas']=$this->Contactomodel->getListaFichas();
		$this->load->view('cotizaciones/NuevaCotizacion', $data);
        }
    }
	
	
	
	function viewcotizarpdfhuser($idcot,$idlcot){
if($_SESSION['ciudad']=='Tijuana'){$numh='(664) 900-9000';}
if($_SESSION['ciudad']=='Mexicali'){$numh='(686) 900-9000';}
if($_SESSION['ciudad']=='Ensenada'){$numh='(646) 900-9000';}

$versiones= $this->Contactomodel->getListaVersiones($idcot);
$asesor=$this->Contactomodel->getAsesorT($_SESSION['id']);
$cotizacion= $this->Contactomodel->getCotizacion($idcot);
$lista= $this->Contactomodel->getListaLC($idlcot);

if($cotizacion[0]->cot_IDficha==9){$vi='L';}	
else{$vi='';}	
    // create new PDF document
	 $pdf = new TCPDF($vi, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1);
$pdf->AddPage(); 
// set cell padding
$pdf->setCellPaddings(2, 2, 2, 2);

setlocale(LC_MONETARY, 'en_US');

$enganche=$versiones[0]->ver_precio * $versiones[0]->ver_por_enganche / 100;
$suma1=$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia+$versiones[0]->ver_extencion+$versiones[0]->ver_seguro;
$total1=$suma1-$versiones[0]->ver_bono;

$enganche2=$versiones[1]->ver_precio * $versiones[1]->ver_por_enganche / 100;
$suma2=$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia+$versiones[1]->ver_extencion+$versiones[1]->ver_seguro;
$total2=$suma2-$versiones[1]->ver_bono;

$enganche3=$versiones[2]->ver_precio * $versiones[2]->ver_por_enganche / 100;
$suma3=$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia+$versiones[2]->ver_extencion+$versiones[2]->ver_seguro;
$total3=$suma3-$versiones[2]->ver_bono;

if($versiones[0]->ver_bono==0 && $versiones[1]->ver_bono==0 && $versiones[2]->ver_bono==0){
$bonotable='';}
else{
$bonotable='<tr><td align="center"><b>BONO</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[0]->ver_bono).'</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[1]->ver_bono).'</b></td>
<td align="right"><b>'.money_format('%(#10n',$versiones[2]->ver_bono).'</b></td>
</tr>';		
	}
	
	if($cotizacion[0]->fmo_IDficha_modeo=='8'){$precio3='$1,750.00 / $2,425.00 (4WD)'; $ford='';}
else{$precio3=money_format('%(#10n',$cotizacion[0]->cot_precio3);
$ford='
<tr>
<td  align="left" style=" font-size:0.8em">20"000 (4WD)</td>
<td  align="right" style=" font-size:0.8em">$    2,310.00</td>
</tr>';
}

$num = preg_replace('/[^0-9]/', '', $asesor[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);



$html = '

<img src="'.base_url().'downloads/cotizador/'.$cotizacion[0]->fmo_pdf.'">
<table style="font-size:.8em" width="100%">
<tr>
<td width="80%">
'.$lista[0]->lsp_nombre.' '.$lista[0]->lsp_apellido.'
</td>
<td width="20%" align="right">Fecha:'.date('d-m-Y').'</td>
</tr>
</table>
<p style="font-size:.8em">
Gracias por su interés hacia el '.$cotizacion[0]->fmo_nombre.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que
tenemos a su disposición.
</p>

<table  style="font-size:.8em; " >
<tr>
<td width="75%">
<table border="1" cellpadding="2">
<tr align="center">
<td><b>Version</b></td>
<td><b>'.$versiones[0]->ver_nombre.'</b></td>
<td><b>'.$versiones[1]->ver_nombre.'</b></td>
<td><b>'.$versiones[2]->ver_nombre.'</b></td>
</tr>
<tr >
<td align="left"> Precio del Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_precio).'</td>
</tr>
<tr align="center">
<td align="left"><b> FINANCIERA</b></td>
<td>'.$versiones[0]->ver_financiera.'</td>
<td>'.$versiones[1]->ver_financiera.'</td>
<td>'.$versiones[2]->ver_financiera.'</td>
</tr>
<tr >
<td><b> ENGANCHE</b></td>
<td >
<table  cellpadding="2">
<tr>
<td align="right"> % '.$versiones[0]->ver_por_enganche.'</td>
</tr><tr>
<td  align="right">'.money_format('%(#10n',$enganche).'</td></tr></table></td>
<td>
<table cellpadding="2" ><tr>
<td align="right"> % '.$versiones[1]->ver_por_enganche.'</td>
</tr><tr>
<td  align="right">'.money_format('%(#10n',$enganche2).'</td></tr></table></td>
<td >
<table cellpadding="2" ><tr>
<td align="right"> % '.$versiones[2]->ver_por_enganche.'</td>
</tr><tr>
<td  align="right">'.money_format('%(#10n',$enganche3).'</td></tr></table></td>
</tr>
<tr >
<td > Seguro Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_auto).'</td>
</tr>
<tr>
<td> Seguro Vida</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_vida).'</td>
</tr>
<tr >
<td> Com.Apertura</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_apertura).'</td>
</tr>
<tr >
<td> Placas y Tenencia</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td> Inversión Inicial</td>
<td align="right">'.money_format('%(#10n',$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td> Extensión Garantía</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_extencion).'</td>
</tr>
<tr >
<td> Seguro R.C USA</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro).'</td>
</tr>
'.$bonotable.'
<tr >
<td> Total</td>
<td align="right">'.money_format('%(#10n',$total1).'</td>
<td align="right">'.money_format('%(#10n',$total2).'</td>
<td align="right">'.money_format('%(#10n',$total3).'</td>
</tr>
<tr  bgcolor="#CCCCCC">
<td> Mensualidades de:</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_mensualidad).'</td>
</tr>

<tr  >
<td><b> PLAZO</b></td>
<td align="center">'.$versiones[0]->ver_plazo.'</td>
<td align="center">'.$versiones[1]->ver_plazo.'</td>
<td align="center">'.$versiones[2]->ver_plazo.'</td>
</tr>
</table>
</td>
<td width="25%">
<table>
<tr><td>Notas</td></tr>
<tr><td>
<table border="1" cellpadding="3" ><tr>
<td height="143px;" style="padding-left:10px"> '.$cotizacion[0]->cot_nota.'
</td></tr></table>
</td></tr>
</table>
</td>
</tr>
</table>
<br><br>
<table ><tr>
<td width="45%">
<table border="1" cellpadding="2" >
<tr>
<td align="center" colspan="2" style="color:red; font-size:0.8em">Su auto tiene Garantía de fábrica por 3 años o 60"000
Km.</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#CCCCCC" style=" font-size:0.8em">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">12 MESES o 20 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex4).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">24 MESES o 40 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex5).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">36 MESES o 60 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex6).'</td>
</tr>
<tr bgcolor="#CCCCCC">
<td  align="center" style=" font-size:0.8em">SERVICIO Km.</td>
<td  align="center" style=" font-size:0.8em">PRECIO*</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">5"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio1).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">10"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio2).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 </td>
<td  align="right" style=" font-size:0.8em">'.$precio3.'</td></tr>
'.$ford.'
</table>
<p align="center"   style=" font-size:0.8em">
*Precios con IVA incluido. Posibilidad de variar sin previo aviso</p>
</td>
<td width="25%">
<table align="right"  style=" font-size:0.8em" cellpadding="2">
<tr><td>Su Asesor de Ventas:</td></tr>
<tr><td>Mail:</td></tr>
<tr><td>Celular:</td></tr>
<tr><td>Radio Personal:</td></tr>
<tr><td>Radio Honda:</td></tr>
<tr><td>Teléfono Honda:</td></tr>
</table>

</td>
<td width="30%">
<table  cellpadding="2"align="left" border="1"  style=" font-size:0.8em">
<tr><td> '.$asesor[0]->hus_nombre.' '.$asesor[0]->hus_apellido.'</td></tr>
<tr><td> '.$asesor[0]->hus_correo.'</td></tr>
<tr><td> '.$celular.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_personal.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_honda.'</td></tr>
<tr><td> '.$numh.'</td></tr>
</table>
<br>
<p align="right">
<a style="color:red; font-size:.8em" href="http://www.hondaoptima.com">hondaoptima.com</a><br>
<a style="color:blue; font-size:.8em" href="http://www.facebook.com/hondaoptima">www.facebook.com/hondaoptima</a></p>
</td>


</tr></table>

';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('downloads/cotizaciones/cotizacion'.$idcot.'.pdf', 'I');    

		}		
		
		
		
		function cotizarpdf($idcot){
$versiones= $this->Contactomodel->getListaVersiones($idcot);
$asesor=$this->Contactomodel->getAsesorT($_SESSION['id']);
$cotizacion= $this->Contactomodel->getCotizacion($idcot);
if($_SESSION['ciudad']=='Tijuana'){$numh='(664) 900-9000';}
if($_SESSION['ciudad']=='Mexicali'){$numh='(686) 900-9000';}
if($_SESSION['ciudad']=='Ensenada'){$numh='(646) 900-9000';}

if($cotizacion[0]->cot_IDficha==9){$vi='L';}	
else{$vi='';}	
		
    // create new PDF document
	 $pdf = new TCPDF($vi, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 
setlocale(LC_MONETARY, 'en_US');

$enganche=$versiones[0]->ver_precio * $versiones[0]->ver_por_enganche / 100;
$suma1=$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia+$versiones[0]->ver_extencion+$versiones[0]->ver_seguro;
$total1=$suma1-$versiones[0]->ver_bono;

$enganche2=$versiones[1]->ver_precio * $versiones[1]->ver_por_enganche / 100;
$suma2=$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia+$versiones[1]->ver_extencion+$versiones[1]->ver_seguro;
$total2=$suma2-$versiones[1]->ver_bono;

$enganche3=$versiones[2]->ver_precio * $versiones[2]->ver_por_enganche / 100;
$suma3=$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia+$versiones[2]->ver_extencion+$versiones[2]->ver_seguro;
$total3=$suma3-$versiones[2]->ver_bono;

if($versiones[0]->ver_bono==0 && $versiones[1]->ver_bono==0 && $versiones[2]->ver_bono==0){
$bonotable='';}
else{
$bonotable='<tr><td align="center"><b>BONO</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[0]->ver_bono).'</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[1]->ver_bono).'</b></td>
<td align="right"><b>'.money_format('%(#10n',$versiones[2]->ver_bono).'</b></td>
</tr>';		
	}
	
	if($cotizacion[0]->fmo_IDficha_modeo=='8'){$precio3='$1,750.00 / $2,425.00 (4WD)'; $ford='';}
else{$precio3=money_format('%(#10n',$cotizacion[0]->cot_precio3);
$ford='
<tr>
<td  align="left" style=" font-size:0.8em">20"000 (4WD)</td>
<td  align="right" style=" font-size:0.8em">$    2,310.00</td>
</tr>';
}

$num = preg_replace('/[^0-9]/', '', $asesor[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);



$html = '
<img src="'.base_url().'downloads/cotizador/'.$cotizacion[0]->fmo_pdf.'">
<table style="font-size:.8em" width="100%">
<tr>
<td width="80%">
'.$cotizacion[0]->con_titulo.' '.$cotizacion[0]->con_nombre.' '.$cotizacion[0]->con_apellido.'
</td>
<td width="20%" align="right">Fecha:'.date('d-m-Y').'</td>
</tr>
</table>
<p style="font-size:.8em">
Gracias por su interés hacia el '.$cotizacion[0]->fmo_nombre.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que
tenemos a su disposición.
</p>

<table style="font-size:.8em">
<tr>
<td width="70%">
<table border="1" >
<tr align="center">
<td><b>Version</b></td>
<td><b>'.$versiones[0]->ver_nombre.'</b></td>
<td><b>'.$versiones[1]->ver_nombre.'</b></td>
<td><b>'.$versiones[2]->ver_nombre.'</b></td>
</tr>
<tr >
<td align="center">Precio del Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_precio).'</td>
</tr>
<tr align="center">
<td><b>FINANCIERA</b></td>
<td>'.$versiones[0]->ver_financiera.'</td>
<td>'.$versiones[1]->ver_financiera.'</td>
<td>'.$versiones[2]->ver_financiera.'</td>
</tr>
<tr >
<td><b>ENGANCHE</b></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[0]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche).'</td></tr></table></td>
<td>
<table border="1"><tr>
<td width="30%"> % '.$versiones[1]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche2).'</td></tr></table></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[2]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche3).'</td></tr></table></td>
</tr>
<tr >
<td>Seguro Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_auto).'</td>
</tr>
<tr>
<td>Seguro Vida</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_vida).'</td>
</tr>
<tr >
<td>Com.Apertura</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_apertura).'</td>
</tr>
<tr >
<td>Placas y Tenencia</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Inversión Inicial</td>
<td align="right">'.money_format('%(#10n',$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Extensión Garantía</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_extencion).'</td>
</tr>
<tr >
<td>Seguro R.C USA</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro).'</td>
</tr>
'.$bonotable.'
<tr >
<td>Total</td>
<td align="right">'.money_format('%(#10n',$total1).'</td>
<td align="right">'.money_format('%(#10n',$total2).'</td>
<td align="right">'.money_format('%(#10n',$total3).'</td>
</tr>
<tr  bgcolor="#CCCCCC">
<td>Mensualidades de:</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_mensualidad).'</td>
</tr>

<tr  >
<td><b>PLAZO</b></td>
<td align="center">'.$versiones[0]->ver_plazo.'</td>
<td align="center">'.$versiones[1]->ver_plazo.'</td>
<td align="center">'.$versiones[2]->ver_plazo.'</td>
</tr>
</table>
</td>
<td width="30%">
<table>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td>Notas</td></tr>
<tr><td>
<table border="1" ><tr>
<td height="143px;">
'.$cotizacion[0]->cot_nota.'
</td></tr></table>
</td></tr>
</table>
</td>
</tr>
</table>
<br><br>
<table ><tr>
<td width="45%">
<table border="1" >
<tr>
<td align="center" colspan="2" style="color:red; font-size:0.8em">Su auto tiene Garantía de fábrica por 3 años o 60"000
Km.</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#CCCCCC" style=" font-size:0.8em">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>

<tr>
<td  align="left" style=" font-size:0.8em">12 MESES o 20 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex4).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">24 MESES o 40 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex5).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">36 MESES o 60 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex6).'</td>
</tr>
<tr bgcolor="#CCCCCC">
<td  align="center" style=" font-size:0.8em">SERVICIO Km.</td>
<td  align="center" style=" font-size:0.8em">PRECIO*</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">5"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio1).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">10"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio2).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 </td>
<td  align="right" style=" font-size:0.8em">'.$precio3.'</td></tr>
'.$ford.'
</table>
<p align="center"   style=" font-size:0.8em">
*Precios con IVA incluido. Posibilidad de variar sin previo aviso</p>
</td>
<td width="25%">
<table align="right"  style=" font-size:0.8em">
<tr><td>Su Asesor de Ventas:</td></tr>
<tr><td>Mail:</td></tr>
<tr><td>Celular:</td></tr>
<tr><td>Radio Personal:</td></tr>
<tr><td>Radio Honda:</td></tr>
<tr><td>Teléfono Honda:</td></tr>
</table>

</td>
<td width="30%">
<table align="left" border="1"  style=" font-size:0.8em">
<tr><td> '.$asesor[0]->hus_nombre.' '.$asesor[0]->hus_apellido.'</td></tr>
<tr><td> '.$asesor[0]->hus_correo.'</td></tr>
<tr><td> '.$celular.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_personal.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_honda.'</td></tr>
<tr><td> '.$numh.'</td></tr>
</table>
<br>
<p align="right">
<a style="color:red; font-size:.8em" href="http://www.hondaoptima.com">hondaoptima.com</a><br>
<a style="color:blue; font-size:.8em" href="http://www.facebook.com/hondaoptima">www.facebook.com/hondaoptima</a></p>
</td>


</tr></table>

';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('downloads/cotizaciones/cotizacion'.$idcot.'.pdf', 'F');  

$datos='Llamada de seguimiento de la cotizacion del:'.$cotizacion[0]->fmo_nombre.'';
$idbit=$this->Contactomodel->idBitacora($cotizacion[0]->cot_IDcontacto);
$this->Contactomodel->AddTareaCotizacionContacto($datos,$idbit->bit_IDbitacora);
 
$res=$this->Contactomodel->EmailCotizacion($cotizacion[0]->cot_email,$cotizacion[0]->fmo_nombre,$cotizacion[0]->con_titulo,$cotizacion[0]->con_nombre,$cotizacion[0]->con_apellido,$idcot);


if($res==1){

$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha enviado una nueva cotizacion.</div>"); 	
 redirect('contacto/bitacora/'.$cotizacion[0]->con_IDcontacto.'');
}
		}		
		
		function viewcotizarpdf($idcot){

$versiones= $this->Contactomodel->getListaVersiones($idcot);
$asesor=$this->Contactomodel->getAsesorT($_SESSION['id']);
$cotizacion= $this->Contactomodel->getCotizacion($idcot);

if($_SESSION['ciudad']=='Tijuana'){$numh='(664) 900-9000';}
if($_SESSION['ciudad']=='Mexicali'){$numh='(686) 900-9000';}
if($_SESSION['ciudad']=='Ensenada'){$numh='(646) 900-9000';}

if($cotizacion[0]->cot_IDficha==9){$vi='L';}	
else{$vi='';}		
    // create new PDF document
	 $pdf = new TCPDF($vi, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 
setlocale(LC_MONETARY, 'en_US');

$enganche=$versiones[0]->ver_precio * $versiones[0]->ver_por_enganche / 100;
$suma1=$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia+$versiones[0]->ver_extencion+$versiones[0]->ver_seguro;
$total1=$suma1-$versiones[0]->ver_bono;

$enganche2=$versiones[1]->ver_precio * $versiones[1]->ver_por_enganche / 100;
$suma2=$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia+$versiones[1]->ver_extencion+$versiones[1]->ver_seguro;
$total2=$suma2-$versiones[1]->ver_bono;

$enganche3=$versiones[2]->ver_precio * $versiones[2]->ver_por_enganche / 100;
$suma3=$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia+$versiones[2]->ver_extencion+$versiones[2]->ver_seguro;
$total3=$suma3-$versiones[2]->ver_bono;

if($cotizacion[0]->fmo_IDficha_modeo=='8'){$precio3='$1,750.00 / $2,425.00 (4WD)'; $ford='';}
else{$precio3=money_format('%(#10n',$cotizacion[0]->cot_precio3);
$ford='
<tr>
<td  align="left" style=" font-size:0.8em">20"000 (4WD)</td>
<td  align="right" style=" font-size:0.8em">$    2,310.00</td>
</tr>';
}


if($versiones[0]->ver_bono==0 && $versiones[1]->ver_bono==0 && $versiones[2]->ver_bono==0){
$bonotable='';}
else{
$bonotable='<tr><td align="center"><b>BONO</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[0]->ver_bono).'</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[1]->ver_bono).'</b></td>
<td align="right"><b>'.money_format('%(#10n',$versiones[2]->ver_bono).'</b></td>
</tr>';		
	}

$num = preg_replace('/[^0-9]/', '', $asesor[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);



$html = '
<img src="'.base_url().'downloads/cotizador/'.$cotizacion[0]->fmo_pdf.'">
<table style="font-size:.8em" width="100%">
<tr>
<td width="80%">
'.$cotizacion[0]->con_titulo.' '.$cotizacion[0]->con_nombre.' '.$cotizacion[0]->con_apellido.'
</td>
<td width="20%" align="right">Fecha:'.date('d-m-Y').'</td>
</tr>
</table>
<p style="font-size:.8em">
Gracias por su interés hacia el '.$cotizacion[0]->fmo_nombre.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que
tenemos a su disposición.
</p>

<table style="font-size:.8em">
<tr>
<td width="70%">
<table border="1" >
<tr align="center">
<td><b>Version</b></td>
<td><b>'.$versiones[0]->ver_nombre.'</b></td>
<td><b>'.$versiones[1]->ver_nombre.'</b></td>
<td><b>'.$versiones[2]->ver_nombre.'</b></td>
</tr>
<tr >
<td align="center">Precio del Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_precio).'</td>
</tr>
<tr align="center">
<td><b>FINANCIERA</b></td>
<td>'.$versiones[0]->ver_financiera.'</td>
<td>'.$versiones[1]->ver_financiera.'</td>
<td>'.$versiones[2]->ver_financiera.'</td>
</tr>
<tr >
<td><b>ENGANCHE</b></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[0]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche).'</td></tr></table></td>
<td>
<table border="1"><tr>
<td width="30%"> % '.$versiones[1]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche2).'</td></tr></table></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[2]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche3).'</td></tr></table></td>
</tr>
<tr >
<td>Seguro Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_auto).'</td>
</tr>
<tr>
<td>Seguro Vida</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_vida).'</td>
</tr>
<tr >
<td>Com.Apertura</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_apertura).'</td>
</tr>
<tr >
<td>Placas y Tenencia</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Inversión Inicial</td>
<td align="right">'.money_format('%(#10n',$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Extensión Garantía</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_extencion).'</td>
</tr>
<tr >
<td>Seguro R.C USA</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro).'</td>
</tr>
'.$bonotable.'
<tr >
<td>Total</td>
<td align="right">'.money_format('%(#10n',$total1).'</td>
<td align="right">'.money_format('%(#10n',$total2).'</td>
<td align="right">'.money_format('%(#10n',$total3).'</td>
</tr>
<tr  bgcolor="#CCCCCC">
<td>Mensualidades de:</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_mensualidad).'</td>
</tr>

<tr  >
<td><b>PLAZO</b></td>
<td align="center">'.$versiones[0]->ver_plazo.'</td>
<td align="center">'.$versiones[1]->ver_plazo.'</td>
<td align="center">'.$versiones[2]->ver_plazo.'</td>
</tr>
</table>
</td>
<td width="30%">
<table>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td>Notas</td></tr>
<tr><td>
<table border="1" ><tr>
<td height="143px;">
'.$cotizacion[0]->cot_nota.'
</td></tr></table>
</td></tr>
</table>
</td>
</tr>
</table>
<br><br>
<table ><tr>
<td width="45%">
<table border="1" >
<tr>
<td align="center" colspan="2" style="color:red; font-size:0.8em">Su auto tiene Garantía de fábrica por 3 años o 60"000
Km.</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#CCCCCC" style=" font-size:0.8em">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>

<tr>
<td  align="left" style=" font-size:0.8em">12 MESES o 20 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex4).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">24 MESES o 40 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex5).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">36 MESES o 60 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex6).'</td>
</tr>
<tr bgcolor="#CCCCCC">
<td  align="center" style=" font-size:0.8em">SERVICIO Km.</td>
<td  align="center" style=" font-size:0.8em">PRECIO*</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">5"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio1).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">10"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio2).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 </td>
<td  align="right" style=" font-size:0.8em">'.$precio3.'</td></tr>
'.$ford.'
</table>
<p align="center"   style=" font-size:0.8em">
*Precios con IVA incluido. Posibilidad de variar sin previo aviso</p>
</td>
<td width="25%">
<table align="right"  style=" font-size:0.8em">
<tr><td>Su Asesor de Ventas:</td></tr>
<tr><td>Mail:</td></tr>
<tr><td>Celular:</td></tr>
<tr><td>Radio Personal:</td></tr>
<tr><td>Radio Honda:</td></tr>
<tr><td>Teléfono Honda:</td></tr>
</table>

</td>
<td width="30%">
<table align="left" border="1"  style=" font-size:0.8em">
<tr><td> '.$asesor[0]->hus_nombre.' '.$asesor[0]->hus_apellido.'</td></tr>
<tr><td> '.$asesor[0]->hus_correo.'</td></tr>
<tr><td> '.$celular.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_personal.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_honda.'</td></tr>
<tr><td> '.$numh.'</td></tr>
</table>
<br>
<p align="right">
<a style="color:red; font-size:.8em" href="http://www.hondaoptima.com">hondaoptima.com</a><br>
<a style="color:blue; font-size:.8em" href="http://www.facebook.com/hondaoptima">www.facebook.com/hondaoptima</a></p>
</td>


</tr></table>

';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('downloads/cotizaciones/cotizacion'.$idcot.'.pdf', 'I');    

		}		
	
		
			
	function cotizarpdfHuser($idcot,$idlcot){
if($_SESSION['ciudad']=='Tijuana'){$numh='(664) 900-9000';}
if($_SESSION['ciudad']=='Mexicali'){$numh='(686) 900-9000';}
if($_SESSION['ciudad']=='Ensenada'){$numh='(646) 900-9000';}

$versiones= $this->Contactomodel->getListaVersiones($idcot);
$asesor=$this->Contactomodel->getAsesorT($_SESSION['id']);
$cotizacion= $this->Contactomodel->getCotizacion($idcot);
$lista= $this->Contactomodel->getListaLC($idlcot);

if($cotizacion[0]->cot_IDficha==9){$vi='L';}	
else{$vi='';}		
    // create new PDF document
	 $pdf = new TCPDF($vi, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 
setlocale(LC_MONETARY, 'en_US');

$enganche=$versiones[0]->ver_precio * $versiones[0]->ver_por_enganche / 100;
$suma1=$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia+$versiones[0]->ver_extencion+$versiones[0]->ver_seguro;
$total1=$suma1-$versiones[0]->ver_bono;

$enganche2=$versiones[1]->ver_precio * $versiones[1]->ver_por_enganche / 100;
$suma2=$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia+$versiones[1]->ver_extencion+$versiones[1]->ver_seguro;
$total2=$suma2-$versiones[1]->ver_bono;

$enganche3=$versiones[2]->ver_precio * $versiones[2]->ver_por_enganche / 100;
$suma3=$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia+$versiones[2]->ver_extencion+$versiones[2]->ver_seguro;
$total3=$suma3-$versiones[2]->ver_bono;

if($versiones[0]->ver_bono==0 && $versiones[1]->ver_bono==0 && $versiones[2]->ver_bono==0){
$bonotable='';}
else{
$bonotable='<tr><td align="center"><b>BONO</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[0]->ver_bono).'</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[1]->ver_bono).'</b></td>
<td align="right"><b>'.money_format('%(#10n',$versiones[2]->ver_bono).'</b></td>
</tr>';		
	}
	
	
	if($cotizacion[0]->fmo_IDficha_modeo=='8'){$precio3='$1,750.00 / $2,425.00 (4WD)'; $ford='';}
else{$precio3=money_format('%(#10n',$cotizacion[0]->cot_precio3);
$ford='
<tr>
<td  align="left" style=" font-size:0.8em">20"000 (4WD)</td>
<td  align="right" style=" font-size:0.8em">$    2,310.00</td>
</tr>';
}

$num = preg_replace('/[^0-9]/', '', $asesor[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);



$html = '
<img src="'.base_url().'downloads/cotizador/'.$cotizacion[0]->fmo_pdf.'">
<table style="font-size:.8em" width="100%">
<tr>
<td width="80%">
'.$lista[0]->lsp_nombre.' '.$lista[0]->lsp_apellido.'
</td>
<td width="20%" align="right">Fecha:'.date('d-m-Y').'</td>
</tr>
</table>
<p style="font-size:.8em">
Gracias por su interés hacia el '.$cotizacion[0]->fmo_nombre.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que
tenemos a su disposición.
</p>

<table style="font-size:.8em">
<tr>
<td width="70%">
<table border="1" >
<tr align="center">
<td><b>Version</b></td>
<td><b>'.$versiones[0]->ver_nombre.'</b></td>
<td><b>'.$versiones[1]->ver_nombre.'</b></td>
<td><b>'.$versiones[2]->ver_nombre.'</b></td>
</tr>
<tr >
<td align="center">Precio del Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_precio).'</td>
</tr>
<tr align="center">
<td><b>FINANCIERA</b></td>
<td>'.$versiones[0]->ver_financiera.'</td>
<td>'.$versiones[1]->ver_financiera.'</td>
<td>'.$versiones[2]->ver_financiera.'</td>
</tr>
<tr >
<td><b>ENGANCHE</b></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[0]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche).'</td></tr></table></td>
<td>
<table border="1"><tr>
<td width="30%"> % '.$versiones[1]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche2).'</td></tr></table></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[2]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche3).'</td></tr></table></td>
</tr>
<tr >
<td>Seguro Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_auto).'</td>
</tr>
<tr>
<td>Seguro Vida</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_vida).'</td>
</tr>
<tr >
<td>Com.Apertura</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_apertura).'</td>
</tr>
<tr >
<td>Placas y Tenencia</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Inversión Inicial</td>
<td align="right">'.money_format('%(#10n',$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Extensión Garantía</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_extencion).'</td>
</tr>
<tr >
<td>Seguro R.C USA</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro).'</td>
</tr>
'.$bonotable.'
<tr >
<td>Total</td>
<td align="right">'.money_format('%(#10n',$total1).'</td>
<td align="right">'.money_format('%(#10n',$total2).'</td>
<td align="right">'.money_format('%(#10n',$total3).'</td>
</tr>
<tr  bgcolor="#CCCCCC">
<td>Mensualidades de:</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_mensualidad).'</td>
</tr>

<tr  >
<td><b>PLAZO</b></td>
<td align="center">'.$versiones[0]->ver_plazo.'</td>
<td align="center">'.$versiones[1]->ver_plazo.'</td>
<td align="center">'.$versiones[2]->ver_plazo.'</td>
</tr>
</table>
</td>
<td width="30%">
<table>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td>Notas</td></tr>
<tr><td>
<table border="1" ><tr>
<td height="143px;">
'.$cotizacion[0]->cot_nota.'
</td></tr></table>
</td></tr>
</table>
</td>
</tr>
</table>
<br><br>
<table ><tr>
<td width="45%">
<table border="1" >
<tr>
<td align="center" colspan="2" style="color:red; font-size:0.8em">Su auto tiene Garantía de fábrica por 3 años o 60"000
Km.</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#CCCCCC" style=" font-size:0.8em">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">12 MESES o 20 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex4).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">24 MESES o 40 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex5).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">36 MESES o 60 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex6).'</td>
</tr>
<tr bgcolor="#CCCCCC">
<td  align="center" style=" font-size:0.8em">SERVICIO Km.</td>
<td  align="center" style=" font-size:0.8em">PRECIO*</td>
</tr>

<tr>
<td  align="left" style=" font-size:0.8em">5"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio1).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">10"000</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precio2).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 </td>
<td  align="right" style=" font-size:0.8em">'.$precio3.'</td>
</tr>
'.$ford.'
</table>
<p align="center"   style=" font-size:0.8em">
*Precios con IVA incluido. Posibilidad de variar sin previo aviso</p>
</td>
<td width="25%">
<table align="right"  style=" font-size:0.8em">
<tr><td>Su Asesor de Ventas:</td></tr>
<tr><td>Mail:</td></tr>
<tr><td>Celular:</td></tr>
<tr><td>Radio Personal:</td></tr>
<tr><td>Radio Honda:</td></tr>
<tr><td>Teléfono Honda:</td></tr>
</table>

</td>
<td width="30%">
<table align="left" border="1"  style=" font-size:0.8em">
<tr><td> '.$asesor[0]->hus_nombre.' '.$asesor[0]->hus_apellido.'</td></tr>
<tr><td> '.$asesor[0]->hus_correo.'</td></tr>
<tr><td> '.$celular.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_personal.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_honda.'</td></tr>
<tr><td> '.$numh.'</td></tr>
</table>
<br>
<p align="right">
<a style="color:red; font-size:.8em" href="http://www.hondaoptima.com">hondaoptima.com</a><br>
<a style="color:blue; font-size:.8em" href="http://www.facebook.com/hondaoptima">www.facebook.com/hondaoptima</a></p>
</td>


</tr></table>

';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);  
$pdf->Output('downloads/cotizaciones/cotizacion'.$idcot.'.pdf', 'F');

$datos='
Nombre:'.$lista[0]->lsp_nombre.' '.$lista[0]->lsp_apellido.'<br>
Tel&eacute;fono:'.$lista[0]->lsp_email.'<br>
Email:'.$cotizacion[0]->cot_email.'<br>
Auto:'.$cotizacion[0]->fmo_nombre.'
';    
$this->Contactomodel->AddTareaCotizacion($datos);

$res=$this->Contactomodel->EmailCotizacion($cotizacion[0]->cot_email,$cotizacion[0]->fmo_nombre,'',$lista[0]->lsp_nombre,$lista[0]->lsp_apellido,$idcot);


if($res==1){

$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha enviado una nueva cotizacion.</div>"); 	
 redirect('cotizaciones/CrearCotizacion/');
}
		}		
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/contizaciones.php */


