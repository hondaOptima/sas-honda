<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporte extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Reportesmodel');
		$this->load->model('Vendedoresmodel');
		$this->load->model('Inventariomodel');
        $this->load->model('Homemodel');
		$this->load->helper('url');
		$this->load->library('email');
        $this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");
		session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
    }
 
    function index()
    {
	$ciud=$_SESSION['ciudad'];
	if(empty($_GET['finicio'])){$data['finicio']=date('d-m-Y');}else{$data['finicio']=$_GET['finicio'];}
	if(empty($_GET['ffin'])){$data['ffin']=date('d-m-Y');}else{$data['ffin']=$_GET['ffin'];}
	if(empty($_GET['vendedor'])){$data['vendedor']='0';}else{$data['vendedor']=$_GET['vendedor'];}
  
 if(empty($_GET['finicio'])){
	$data['todo_no_realizadas'] ='';
    $data['todo_realizadas'] ='';
	 }else{   
$data['todo_no_realizadas'] = $this->Vendedoresmodel->getTareasNoRealizadas($data['finicio'],$data['ffin'],$data['vendedor'],$ciud);
$data['todo_realizadas'] = $this->Vendedoresmodel->getTareasRealizadas($data['finicio'],$data['ffin'],$data['vendedor'],$ciud);
 }
	 
	$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($ciud);	
	$this->load->view('reportes/vista',$data);
	
          
        }
		
		
	function consulta(){
		
	if(empty($_GET['usuarios'])){}else{redirect('reporte/usuarios/');}
	if(empty($_GET['contacto'])){}else{redirect('reporte/contacto/');}
	if(empty($_GET['factura'])){}else{redirect('reporte/facturascheck/');}	
	if(empty($_GET['cliente'])){}else{redirect('reporte/clientecheck/');}	
		
		}
			function facturascheck(){
		
		$this->load->view('reportes/facturascheck');
		}
		
		
		function estadisticasbateo(){
			
			if(!empty($_GET['finicio'])){$finicio=$_GET['finicio'];}else{$finicio='01-'.date('m').'-'.'2014';}
            if(!empty($_GET['ffin'])){$ffin=$_GET['ffin'];}else{$ffin='31-'.date('m').'-'.'2014';}
			if(!empty($_GET['ciudad'])){$cd=$_GET['ciudad'];}else{$cd='todas';}
			$data['cid']=array('todas'=>'Todas','Tijuana'=>'Tijuana','Mexicali'=>'Mexicali','Ensenada'=>'Ensenada');
			$data['cd']=$cd;
			$data['finicio']=$finicio;
			$data['ffin']=$ffin;
			$data['asesoresTij']=$this->Reportesmodel->listaAsesoresExclude('Tijuana');
			$data['asesoresMex']=$this->Reportesmodel->listaAsesoresExclude('Mexicali');
			$data['asesoresEns']=$this->Reportesmodel->listaAsesoresExclude('Ensenada');
			
			$data['A2014']=$this->Reportesmodel->Entradaspiso();
			$data['V2014']=$this->Reportesmodel->VentasPiso();
			$data['VT2014']=$this->Reportesmodel->VentasPisoTotal();
			
			$data['R2014']=$this->Reportesmodel->EntradaspisoRango($finicio,$ffin);
			$data['VR2014']=$this->Reportesmodel->VentasPisoRango($finicio,$ffin);
			$data['VRT2014']=$this->Reportesmodel->VentasPisoRangoTotal($finicio,$ffin);
			
		
		$this->load->view('reportes/estadisticas/bateo',$data);
		}
		
		function facturas(){
			
if(!empty($_GET['colors'])){			
$result = $_GET['colors'];
$cadena='';
 foreach($result AS $key=>$values)
  {
     $cadena.=''.$values."-";
  }
$data['cadena']=$cadena;
}
else{
$data['cadena']=$_GET['cadena'];	
	}
			
		if(empty($_GET['cid'])){$cid='0';}else{$cid=$_GET['cid'];}
		$data['cid']=$cid;

		$data['asesores']= $this->Reportesmodel->listaAsesores($cid);
		$data['oanios']= $this->Reportesmodel->listaAnios();
		$this->load->view('reportes/facturas',$data);
		}
		
		function usuarios(){
		
		if(empty($_GET['cid'])){$cid='0';}
		else{
			$cid=$_GET['cid'];
			}
		$data['cid']=$cid;
		if(empty($_GET['nivel'])){
		 $nivel=0;
		 $data['nivel']=0;
		 }else{
			 $nivel=$_GET['nivel'];
			 $data['nivel']=$_GET['nivel'];
			 
			 }
		
		if(empty($_GET['status'])){
			$status=0;
			$data['status']='0';
			}else{
				$status=$_GET['status'];
				$data['status']=$_GET['status'];
				}
		
		$data['datax']= $this->Reportesmodel->ReportesGetUsuarios($cid,$nivel,$status);
		$this->load->view('reportes/usuarios',$data);
		}
		
		function contacto(){
		
		$data['ofuente']= $this->Reportesmodel->getfuenteSelect();
		$data['opublicidad']= $this->Reportesmodel->getpublicidadSelect();
		$data['oanios']= $this->Reportesmodel->getaniosSelect();
		
		$this->load->view('reportes/contacto',$data);
		}	
		
		
		 function setup()
    {
	$data['tij']= $this->Reportesmodel->planVentas('1','2014');
	$data['mex']= $this->Reportesmodel->planVentas('2','2014');
	$data['ens']= $this->Reportesmodel->planVentas('3','2014');
	$this->load->view('reportes/setup',$data);
	}
		
		 function crearplanventas()
    {
		$data['flash_message'] = $this->session->flashdata('message');
		$this->form_validation->set_rules('agencia', 'agencia', 'required');
	 if ($this->form_validation->run())
        {
			$ene=$this->input->post('ene');
			$feb=$this->input->post('feb');
			$mar=$this->input->post('mar');
			$abr=$this->input->post('abr');
			$may=$this->input->post('may');
			$jun=$this->input->post('jun');
			$jul=$this->input->post('jul');
			$ago=$this->input->post('ago');
			$sep=$this->input->post('sep');
			$oct=$this->input->post('oct');
			$nov=$this->input->post('nov');
			$dic=$this->input->post('dic');
			$i=0;
			$x=0;
			$anio=$this->input->post('anio');
			$age=$this->input->post('agencia');
			$autos= $this->Reportesmodel->listaModelosPlanVenta();
			for($x; $x<count($autos); $x++){
			for($i; $i<count($ene); $i++){
			 $todo = array(
                    'plv_anio'=>$anio,
					'plv_ID_agencia'=>$age,
					'plv_ID_modelo'=>$autos[$i]->mopv_ID,
					'plv_ene'=>$ene[$i],
					'plv_feb'=>$feb[$i],
					'plv_mar'=>$mar[$i],
					'plv_abri'=>$abr[$i],
					'plv_may'=>$may[$i],
					'plv_jun'=>$jun[$i],
					'plv_jul'=>$jul[$i],
					'plv_ago'=>$ago[$i],
					'plv_sep'=>$sep[$i],
					'plv_oct'=>$oct[$i],
					'plv_nov'=>$nov[$i],
					'plv_dic'=>$dic[$i]
                    );	
				
		$this->Reportesmodel->insertPlanVentas($todo);	
			}}
			
			$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado un nuevo Plan de Ventas.</div>");            
redirect('reporte/setup');
		}
		else{
	$data['modelos'] = $this->Reportesmodel->listaModelosPlanVenta();
	$this->load->view('reportes/formplanventas',$data);
		}
          
        }
		
		 function editarplanventas($id)
    {
		$data['flash_message'] = $this->session->flashdata('message');
		$this->form_validation->set_rules('ene', 'ene', 'required');
	 if ($this->form_validation->run())
        {
			$ene=$this->input->post('ene');
			$feb=$this->input->post('feb');
			$mar=$this->input->post('mar');
			$abr=$this->input->post('abr');
			$may=$this->input->post('may');
			$jun=$this->input->post('jun');
			$jul=$this->input->post('jul');
			$ago=$this->input->post('ago');
			$sep=$this->input->post('sep');
			$oct=$this->input->post('oct');
			$nov=$this->input->post('nov');
			$dic=$this->input->post('dic');
			
			 $todo = array(
					'plv_ene'=>$ene,
					'plv_feb'=>$feb,
					'plv_mar'=>$mar,
					'plv_abri'=>$abr,
					'plv_may'=>$may,
					'plv_jun'=>$jun,
					'plv_jul'=>$jul,
					'plv_ago'=>$ago,
					'plv_sep'=>$sep,
					'plv_oct'=>$oct,
					'plv_nov'=>$nov,
					'plv_dic'=>$dic
                    );	
				
		$this->Reportesmodel->updatePlanVentas($todo,$id);	
		
			
			$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Editado la Informacion con Exito!.</div>");            
redirect('reporte/setup');
		}
		else{
	$data['id'] =$id;
	$data['planVentas'] = $this->Reportesmodel->getPlandeVentas($id);
	$this->load->view('reportes/editplanventas',$data);
		}
          
        }
		
		function ventasmes(){
			
$ciud=$_SESSION['ciudad'];
	if(empty($_GET['finicio'])){$data['finicio']=date('d-m-Y');}else{$data['finicio']=$_GET['finicio'];}
	if(empty($_GET['ffin'])){$data['ffin']=date('d-m-Y');}else{$data['ffin']=$_GET['ffin'];}
	if(empty($_GET['vendedor'])){$data['vendedor']='0';}else{$data['vendedor']=$_GET['vendedor'];}
	if(empty($_GET['ciudad'])){$data['ciudad']='0'; $ciudad=0;}else{$data['ciudad']=$_GET['ciudad']; $ciudad=$_GET['ciudad'];}
	if(empty($_GET['tp'])){$tp='no';}else{$tp=$_GET['tp'];}
	
	if($_SESSION['username']=='josemaciel@hondaoptima.com' || $_SESSION['username']=='sgutierrez@hondaoptima.com') { $ciud=$ciudad;}
    
$ventas= $this->Vendedoresmodel->getVentas($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,$tp);
$data['todo_ventas_sin_fecha_de_entrega'] = $this->Vendedoresmodel->getVentasSinFecha($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,$tp);
$facturados= $this->Vendedoresmodel->getVentasFacturados($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,$tp);
$data['todo_ventas']=array_merge_recursive($ventas,$facturados);
$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($ciud);
$this->load->view('reportes/ventasmes',$data);
 }
 
 
 function ventas2(){
			
$ciud=$_SESSION['ciudad'];
	if(empty($_GET['finicio'])){$data['finicio']=date('d-m-Y');}else{$data['finicio']=$_GET['finicio'];}
	if(empty($_GET['ffin'])){$data['ffin']=date('d-m-Y');}else{$data['ffin']=$_GET['ffin'];}
	if(empty($_GET['vendedor'])){$data['vendedor']='0';}else{$data['vendedor']=$_GET['vendedor'];}
	if(empty($_GET['ciudad'])){$data['ciudad']='0'; $ciudad=0;}else{$data['ciudad']=$_GET['ciudad']; $ciudad=$_GET['ciudad'];}
	if(empty($_GET['tp'])){$tp='no';}else{$tp=$_GET['tp'];}
	
	if($_SESSION['username']=='josemaciel@hondaoptima.com') { $ciud=$ciudad;}
    
$ventas= $this->Vendedoresmodel->getVentas($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,$tp);
$data['todo_ventas_sin_fecha_de_entrega'] = $this->Vendedoresmodel->getVentasSinFecha($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,$tp);
$facturados= $this->Vendedoresmodel->getVentasFacturados($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,$tp);
$data['todo_ventas']=array_merge_recursive($ventas,$facturados);
$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($ciud);
$this->load->view('reportes/ventas2',$data);
 }
		
		function inventario()
    {
	$ciud=$_SESSION['ciudad'];
	if(empty($_GET['finicio'])){$data['finicio']=date('d-m-Y');}else{$data['finicio']=$_GET['finicio'];}
	if(empty($_GET['ffin'])){$data['ffin']=date('d-m-Y');}else{$data['ffin']=$_GET['ffin'];}
	if(empty($_GET['vendedor'])){$data['vendedor']='0';}else{$data['vendedor']=$_GET['vendedor'];}
    
	$data['todo_ventas'] = $this->Vendedoresmodel->getVentas($data['finicio'],$data['ffin'],$data['vendedor'],$ciud,'no');
	$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($ciud);	
	$this->load->view('reportes/inventario',$data);
	
          
        }
		
		function ventas()
    {
	
	$data['id_vendedor']='no';
	$busqueda='';
	
	if(empty($_GET['con'])){$con='no';}else{$con=$_GET['con'];}
	if(empty($_GET['huser'])){$hus='no';}else{$hus=$_GET['huser'];}	
	if(empty($_GET['cid'])){$cid='no';}else{$cid=$_GET['cid'];}	
	if(empty($_GET['stac'])){$stac='no';}else{$stac=$_GET['stac'];}	
	
$cd=array(0=>"hus_ciudad");	
$sc=array(0=>"con_status");	
	
	if($con!='no' && $hus!='no'){ 
	
$busqueda=array_merge($cd,$hus,$sc,$con);

}else{
	if($hus!='no'){$busqueda=$hus;}
	if($con!='no'){$busqueda=$con;}
		
		}
		if($hus!='no' || $con!='no'){
		
	$data['todo_consulta'] = $this->Reportesmodel->getDisenadorConsulta($cid,$hus,$stac,$con);
	$data['busqueda']=$busqueda;
	
		}
		else{$data['todo_consulta']=''; $data['busqueda']='';}
	//$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($ciud);	
	$this->load->view('reportes/disenador',$data);
	
          
        }
	function piso(){
	
if (strpos($_GET['fuente'],'/') !== false) {
	list($fue,$nm)=explode('/',$_GET['fuente']);
}else{
$fue=$_GET['fuente'];
}
if($fue=='Internet'){$fue=='Correo Electronico';}

		if(empty($_GET['finicio'])){$mesi='01-05-2014';}else{$mesi=$_GET['finicio'];}
		if(empty($_GET['ffin'])){$mesf='31-05-2014';}else{$mesf=$_GET['ffin'];}
		if(empty($_GET['asesor'])){$ase='no';}else{$ase=$_GET['asesor'];}
		if(empty($_GET['status'])){$sta='0';}else{$sta=$_GET['status'];}
		if(empty($_GET['actividad'])){$act='0';}else{$act=$_GET['actividad'];}
		

		
		$data['sucursal']=$_GET['sucursal'];
		$data['fue']=trim($fue);
		$data['mesi']=$mesi;
		$data['mesf']=$mesf;
		$data['ase']=$ase;
		$data['sta']=$sta;
		$data['act']=$act;		
		
		    $data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($_GET['sucursal']);		
			$this->load->view('reportes/piso',$data);
			}
			
			function citas(){
	
if (empty($_GET['fuente'])) {
	$fue='0';
}else{
$fue=$_GET['fuente'];
}

if(empty($_GET['actividad'])){$act='0';}else{$act=$_GET['actividad'];}
		if(empty($_GET['finicio'])){$mesi='01-03-2014';}else{$mesi=$_GET['finicio'];}
		if(empty($_GET['ffin'])){$mesf='31-03-2014';}else{$mesf=$_GET['ffin'];}
		if(empty($_GET['asesor'])){$ase='no';}else{$ase=$_GET['asesor'];}
		if(empty($_GET['status'])){$sta='0';}else{$sta=$_GET['status'];}
		

		
		$data['sucursal']=$_GET['sucursal'];
		$data['fue']=trim($fue);
		$data['mesi']=$mesi;
		$data['mesf']=$mesf;
		$data['ase']=$ase;
		$data['sta']=$sta;		
		$data['act']=$act;	
		    $data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($_GET['sucursal']);		
			
			$this->load->view('reportes/cita',$data);
			
			
			}

		function dashboard()
    {
		
		$mesi='01-05-2014';
		$mesf='31-05-2014';
	    //ventas
		$data['totalVentasMes']=$this->Vendedoresmodel->totalventas($mesi,$mesf,'no','nuevo');
		$data['totalVentasAnual']=$this->Vendedoresmodel->totalventas('01-01-2014',$mesf,'no','nuevo');
		//Plan de Ventas
		$data['planMes']=$this->Reportesmodel->plv_mont('plv_may',date('Y'));
		$data['planAnual']=$this->Reportesmodel->plv_year(date('Y'));
		//Entradas en Piso
		//$data['pisoMes']=$this->Reportesmodel->getFuenteEnsenada($mesi,$mesf);
		//Citas jquery
		//Ventas del mes por vendedor jquery
		//Acumulado Anual por vendedor jquery
		//Status Contactos jquery
        $this->load->view('reportes/dashboardFinal',$data);
	
          
        }
		
		
		
		 function ventaspormodelo()
    {
	$ventas=$this->Vendedoresmodel->getVentas('01-03-2014','31-03-2014','no',$_GET['cd'],'nuevo');
	$facturados=$this->Vendedoresmodel->getVentasFacturados('01-03-2014','31-03-2014','no',$_GET['cd'],'nuevo');
	$data['todo']=array_merge_recursive($ventas,$facturados);	
	$data['cd']=$_GET['cd'];
	$this->load->view('reportes/ventaspormodelo',$data);
	}
	
	 function ventaspormodeloA()
    {
	$data['tij']= $this->Reportesmodel->planVentas('1','2014');
	$data['mex']= $this->Reportesmodel->planVentas('2','2014');
	$data['ens']= $this->Reportesmodel->planVentas('3','2014');
	$ventas=$this->Vendedoresmodel->getVentas('01-01-2014','31-12-2014','no',$_GET['cd'],'nuevo');
	$facturados=$this->Vendedoresmodel->getVentasFacturados('01-01-2014','31-12-2014','no',$_GET['cd'],'nuevo');
	$data['todo']=array_merge_recursive($ventas,$facturados);	
	$data['cd']=$_GET['cd'];
	$this->load->view('reportes/ventaspormodeloanio',$data);
	}

	
	function precios()
    {
	 $data['flash_message'] = $this->session->flashdata('message');	
	$data['precios'] = $this->Reportesmodel->getPrecios();	
	$this->load->view('reportes/precios',$data);
	}
	function deleteprecio($id)
    {
	
	$this->Reportesmodel->deletePrecios($id);
	
	$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Eliminado la version con exito!.</div>"); 	
	redirect('reporte/precios');
	}
	
	function preciosEdit($id)
    {
		$this->form_validation->set_rules('nombre', 'nombre', 'trim');
		if ($this->form_validation->run()){
			$todo = array(
                    'pr_nombre'=>$this->input->post('nombre'),
					'pr_precio'=>$this->input->post('precio'),
					'pr_rebate'=>$this->input->post('rebate')
                    );
		$this->Reportesmodel->updateVersion($todo,$id);
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Su informaci&oacute;n fue actualizada correctamente.</div>");         redirect('reporte/precios');
			
			
		}
		else{
	$data['id']=$id;		
	$data['version'] = $this->Reportesmodel->getPreciosEdit($id);	
	$this->load->view('reportes/preciosEdit',$data);
	}}
	
	
	function intellisis()
    {
	 $data['flash_message'] = $this->session->flashdata('message');	
	$this->load->view('reportes/intellisis',$data);
	}
	
	
	function preciosAdd()
    {   
        $this->form_validation->set_rules('modelo', 'Modelo', 'required');
		if ($this->form_validation->run())
        {
			
			
		 $coni=$this->input->post('nombre');
	     $coni=count($coni);
		 $coni=$coni-1;
		 $nombre=$this->input->post('nombre');
		 $precio=$this->input->post('precio');
		 $rebate=$this->input->post('rebate');
		 for($i=0; $i<=$coni; $i++){
			 
			$todob = array(
                    
					'pr_precio'=>$precio[$i],
					'pr_rebate'=>$rebate[$i],
					'pr_IDtipo_precio'=>$this->input->post('modelo'),
					'pr_nombre'=>$nombre[$i]
                    );
			 
			 
			 
			$this->Reportesmodel->addprecios($todob);
			}  
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado una nueva version a la lista de precios.</div>");            
            redirect('reporte/precios');
        }
		
		 else
        {$data['modelos'] = $this->Reportesmodel->getPreciosList();
            $this->load->view('reportes/preciosAdd', $data);
        }
   
    }
	
	
	function example(){
		 $this->load->view('reportes/example');
		}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/reporte.php */


