<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Contactomodel');
        $this->load->model('Inventariomodel');
		$this->load->model('Vendedoresmodel');
		$this->load->model('Homemodel');
		$this->load->model('Calendariomodel');
        
		$this->load->helper('url');
		$this->load->library('email');
        $this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");
		 $this->load->library("Pdf");
		session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
    }

 function index()
    {
	$data['flash_message'] = $this->session->flashdata('message');
	$ciud=$_SESSION['ciudad'];
	$idUs=$_SESSION['id'];
	$nivel=$_SESSION['nivel'];
	$segu=$_SESSION['seguridad'];
	$fini='01-'.date('m').'-2014';
	$ffin='30-05-2014';

$data['lista']=$this->Contactomodel->Arraylista();
$data['arrclass']=$this->Contactomodel->ArrayClass();
$data['arrtitle']=$this->Contactomodel->ArrayTitle();
$data['arrmes']=$this->Contactomodel->ArrayMes();
	if(empty($_SESSION['ciudades'])){
			$data['idcd']='0';}
		    else{
		$data['idcd']=$_SESSION['ciudades'];	
				}
	   
		if($segu=='A'){
		$data['tipcon']='tipocontactoadmin';	
		
        
		$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($data['idcd']);
		//vista Gerente uno
		if(!empty($_SESSION['tipocontactoadmin']) && empty($_SESSION['coucheo']))
		{   
			  if(empty($_SESSION['ciudades']) || $_SESSION['ciudades']=='0'){
		    $data['total'] = $this->Homemodel->ContactosTotalA();
$todopv=0;
$todt= $this->Contactomodel->getTodocontactoacouadmin('Tijuana','Proceso%20de%20Venta');
$todm= $this->Contactomodel->getTodocontactoacouadmin('Mexicali','Proceso%20de%20Venta');
$tode= $this->Contactomodel->getTodocontactoacouadmin('Ensenada','Proceso%20de%20Venta');
$todopva=array_merge_recursive($todt,$todm,$tode);
foreach($todopva as $todo){
$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);
if(empty($INFOAUTO)){}
else{
$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto,$INFOAUTO->aau_IdFk);
	}
if(empty($INFO) || $INFO->prcv_prcv_status_credito_contado!='aprobado')
{
$todopv++;	
}
}
$data['todopv']=$todopv;
			
			  }else{
			 $data['total'] = $this->Homemodel->ContactosTotalB($data['idcd']); 
$todopv=0;
$todopva= $this->Contactomodel->getTodocontactoacouadmin($data['idcd'],'Proceso%20de%20Venta');
foreach($todopva as $todo){
$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);
if(empty($INFOAUTO)){}
else{
$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto,$INFOAUTO->aau_IdFk);
	}
if(empty($INFO) || $INFO->prcv_prcv_status_credito_contado!='aprobado')
{
$todopv++;	
}
}
			
			$data['todopv']=$todopv;			 
			 
			  
				  }
			$data['id_vendedor']='no';
			$data['ajax']='no';
			$data['sin']=0;
			if($_SESSION['tipocontactoadmin']=='Ventas%20del%20Mes'){
			
			if(!empty($_SESSION['ciudades'])){
				 $entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,'no',$_SESSION['ciudades'],'no');
		    $facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,'no',$_SESSION['ciudades'],'no');
		    $data['ventasMes'] = count($entregado) + count($facturados) ;
		    $data['tipoconadmin']='Ventas del Mes';
		    $data['todo_contacto'] =array_merge_recursive($entregado,$facturados);
				
				}
			else{
				
		    $entregado=$this->Vendedoresmodel->getVentasA($fini,$ffin,'no',$ciud,'no');
		    $facturados=$this->Vendedoresmodel->getVentasFacturadosA($fini,$ffin,'no',$ciud,'no');
		    $data['ventasMes'] = count($entregado) + count($facturados) ;
		    $data['tipoconadmin']='Ventas del Mes';
		    $data['todo_contacto'] =array_merge_recursive($entregado,$facturados);
			}
		    }else{
				if(empty($_SESSION['ciudades']) || $_SESSION['ciudades']=='0'){
		$data['todo_contacto'] = $this->Contactomodel->getTodocontactoacoudosA($_SESSION['tipocontactoadmin']);	
				}
				else{
				$data['todo_contacto'] = $this->Contactomodel->getTodocontactoacouadmin($_SESSION['ciudades'],$_SESSION['tipocontactoadmin']);		
					}
		$data['ventasMes']=$this->Vendedoresmodel->totalventas($fini,$ffin,'no','nuevo');
		$data['tipoconadmin']=$_SESSION['tipocontactoadmin'];
			}
			}
		//vista Gerente dos
		if(!empty($_SESSION['tipocontactoadmin']) && !empty($_SESSION['coucheo']))
		{
		$idUsx=$_SESSION['coucheo'];
		$data['ajax']='no';
		$data['id_vendedor']=$_SESSION['coucheo'];
		$data['sin'] = $this->Homemodel->ContactosUserSinAct($idUsx);
		$data['total'] = $this->Homemodel->ContactosTotal($idUsx);
		$todopv=0;
$todopva= $this->Contactomodel->getTodocontactoacoudos($_SESSION['coucheo'],'Proceso%20de%20Venta');
foreach($todopva as $todo){
$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);
if(empty($INFOAUTO)){}
else{
$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto,$INFOAUTO->aau_IdFk);
	}
if(empty($INFO) || $INFO->prcv_prcv_status_credito_contado!='aprobado')
{
$todopv++;	
}
}
			
			$data['todopv']=$todopv;
		
		$entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,$idUsx,$ciud,'no');
		$facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,$idUsx,$ciud,'no');
		$data['ventasMes'] = count($entregado) + count($facturados);
		    if($_SESSION['tipocontactoadmin']=='Ventas%20del%20Mes' ){
		
		$data['tipoconadmin']='Ventas del Mes';
		$data['todo_contacto'] =array_merge_recursive($entregado,$facturados);
			}
			elseif($_SESSION['tipocontactoadmin']=='Sin%20Actividad' ){
		$entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,$idUsx,$ciud,'no');
		$facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,$idUsx,$ciud,'no');
		$data['ventasMes'] = count($entregado) + count($facturados);
		$data['tipoconadmin']='Sin Actividad';
		}		
			else{
				
		$data['tipoconadmin']=$_SESSION['tipocontactoadmin'];
		$data['todo_contacto'] = $this->Contactomodel->getTodocontactoacoudos($_SESSION['coucheo'],$_SESSION['tipocontactoadmin']);
		
		
			}
		}
		
		}
		if($segu=='B'){
		$data['tipcon']='tipocontactoadmin';	
		$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoaNoC($ciud);
       
		//vista Gerente uno
		if(!empty($_SESSION['tipocontactoadmin']) && empty($_SESSION['coucheo']))
		{   
		    $data['total'] = $this->Homemodel->ContactosTotalB($ciud);
			$data['id_vendedor']='no';
			$data['ajax']='no';
			$data['sin']=0;
			$todopv=0;
$todopva= $this->Contactomodel->getTodocontactoacouadmin($ciud,'Proceso%20de%20Venta');
foreach($todopva as $todo){
$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);
if(empty($INFOAUTO)){}
else{
$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto,$INFOAUTO->aau_IdFk);
	}
if(empty($INFO) || $INFO->prcv_prcv_status_credito_contado!='aprobado')
{
$todopv++;	
}
}
			
			$data['todopv']=$todopv;
			if($_SESSION['tipocontactoadmin']=='Ventas%20del%20Mes'){
		    $entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,'no',$ciud,'no');
		    $facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,'no',$ciud,'no');
		    $data['ventasMes'] = count($entregado) + count($facturados) ;
		    $data['tipoconadmin']='Ventas del Mes';
		    $data['todo_contacto'] =array_merge_recursive($entregado,$facturados);
			
		    }else{
				
			$data['ventasMes']=$this->Vendedoresmodel->totalventas($fini,$ffin,'no','nuevo');
			$data['todo_contacto'] = $this->Contactomodel->getTodocontactoacouadmin($ciud,$_SESSION['tipocontactoadmin']);
			$data['tipoconadmin']=$_SESSION['tipocontactoadmin'];
			}
			}
		//vista Gerente dos
		if(!empty($_SESSION['tipocontactoadmin']) && !empty($_SESSION['coucheo']))
		{
			
		$idUsx=$_SESSION['coucheo'];
		$data['ajax']='no';
		$data['id_vendedor']=$_SESSION['coucheo'];
		$data['sin'] = $this->Homemodel->ContactosUserSinAct($idUsx);
		$data['total'] = $this->Homemodel->ContactosTotal($idUsx);
		$todopv=0;
$todopva= $this->Contactomodel->getTodocontactoacoudos($_SESSION['coucheo'],'Proceso%20de%20Venta');
foreach($todopva as $todo){
$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);
if(empty($INFOAUTO)){}
else{
$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto,$INFOAUTO->aau_IdFk);
	}
if(empty($INFO) || $INFO->prcv_prcv_status_credito_contado!='aprobado')
{
$todopv++;	
}
}
			
			$data['todopv']=$todopv;
		
		    if($_SESSION['tipocontactoadmin']=='Ventas%20del%20Mes' ){
		$entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,$idUsx,$ciud,'no');
		$facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,$idUsx,$ciud,'no');
		$data['ventasMes'] = count($entregado) + count($facturados);
		$data['tipoconadmin']='Ventas del Mes';
		$data['todo_contacto'] =array_merge_recursive($entregado,$facturados);
			}
			elseif($_SESSION['tipocontactoadmin']=='Sin%20Actividad' ){
		$data['ventasMes']=$this->Vendedoresmodel->totalventas($fini,$ffin,'no','nuevo');
		$data['tipoconadmin']='Sin Actividad';
		}		
			else{
				
		$data['tipoconadmin']=$_SESSION['tipocontactoadmin'];
		$data['todo_contacto'] = $this->Contactomodel->getTodocontactoacoudos($_SESSION['coucheo'],$_SESSION['tipocontactoadmin']);
		$entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,$idUsx,$ciud,'no');
		$facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,$idUsx,$ciud,'no');
		$data['ventasMes'] = count($entregado) + count($facturados);
		
			}
		}
		
		}
		if($segu=='C')
        {
		//vista Asesor
		/*$data['total'] = $this->Homemodel->ContactosTotal($idUs);
		$data['sin'] = $this->Homemodel->ContactosUserSinAct($idUs);
		$data['tipcon']='tipocontacto';
		$data['ajax']='no';
		
		$entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,$idUs,$ciud,'no');
		$facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,$idUs,$ciud,'no');
		$data['ventasMes'] = count($entregado) + count($facturados);
		
         if(empty($_SESSION['tipocontacto'])){ 
		 $data['tipoconadmin']='Prospectos';
		  
		  }else{ 
		
		$data['tipoconadmin']=$_SESSION['tipocontacto'];
		$data['todo_contacto'] = $this->Contactomodel->getTodocontactoVTC($_SESSION['id'],$_SESSION['tipocontacto']);}*/
		$prsp='Prospecto';
	$cali='Caliente';
	$proc='Proceso de Venta';
	$vent='Caida';
	$clie='Cliente';
		$data['prospectos'] = $this->Homemodel->ContactosUser($idUs,$prsp);
		$data['calientes'] = $this->Homemodel->ContactosUser($idUs,$cali);
		$data['proceso'] = $this->Homemodel->ContactosUser($idUs,$proc);
		$data['caidas'] = $this->Homemodel->ContactosUser($idUs,$vent);
		$data['cliente'] = $this->Homemodel->ContactosUser($idUs,$clie);
		$data['todos'] = $this->Homemodel->ContactosUserT($idUs);
		$data['sin'] = $this->Homemodel->ContactosUserSinAct($idUs);
		$data['tipcon']='tipocontacto';
		
		$entregado=$this->Vendedoresmodel->getVentas($fini,$ffin,$idUs,$ciud,'no');
		$facturados=$this->Vendedoresmodel->getVentasFacturados($fini,$ffin,$idUs,$ciud,'no');
		$data['ventasMes'] = count($entregado) + count($facturados);
		
         if(empty($_SESSION['tipocontacto'])){ 
		 $data['tipoconadmin']='Prospectos';
		 
		 }
		else{
		$data['tipoconadmin']=$_SESSION['tipocontacto'];
		$data['todo_contacto'] = $this->Contactomodel->getTodocontactoVTC($_SESSION['id'],$_SESSION['tipocontacto']);
		}
          $data['ajax']='no';
	
        }
if($segu=='A' || $segu=='B'){		
$this->load->view('contacto/listaContactos/vista', $data);}
if($segu=='C'){
$this->load->view('contacto/vista', $data);
	}
}
	




function listaajax(){                      
	$this->load->view('contacto/listaajax');
	}
function coucheo($id){
if($id=='no'){
unset($_SESSION['tipocontactoadmin']);
$_SESSION['tipocontactoadmin'] ='Caliente';

$_SESSION['coucheo']; 

unset($_SESSION['coucheo']);
}
else{

unset($_SESSION['tipocontactoadmin']);	
$_SESSION['tipocontactoadmin'] ='Prospecto';	

$_SESSION['coucheo'] = $id;
	}
redirect('contacto');
	}
	
	function ventasMes(){
if(!empty($_SESSION['tipocontactoadmin'])){		
unset($_SESSION['tipocontactoadmin']);}
if(!empty($_SESSION['coucheo'])){
unset($_SESSION['coucheo']);}
redirect('contacto');
	}

function tipocontactoadmin($id){
unset($_SESSION['tipocontactoadmin']);
$_SESSION['tipocontactoadmin'] = $id;
redirect('contacto');
	}
	
	function clearciudad($id){
if(!empty($_SESSION['coucheo'])){
unset($_SESSION['coucheo']);}
$_SESSION['ciudades'] = $id;
redirect('contacto');
	}


function tipocontacto($id){
unset($_SESSION['tipocontacto']);
$_SESSION['tipocontacto'] = $id;
redirect('contacto');	
}

function session(){
if($_SESSION['ciudad']==$_GET['cd']){
//redirijir	
redirect('contacto/tipocontactoadmin/'.$_GET['tp'].'');
	}
else{
unset($_SESSION['ciudad']);	
$_SESSION['ciudad'] = $_GET['cd'];	
//redirijir
redirect('contacto/tipocontactoadmin/'.$_GET['tp'].'');
	}	
	
	}	
	
	function add()
    {   
	    $data['flash_message'] = $this->session->flashdata('message');
        if(isset($_GET['TPO'])){$TPO=$_GET['TPO'];
		$DATO=$this->Contactomodel->getInfoLlamada($TPO);
		
		$data['asesor']=$DATO[0]->hus_IDhuser;
		$data['contacto']=$DATO[0]->lle_cliente_interes;
		$data['telefono']=$DATO[0]->lle_telefono;
		$data['correo']=$DATO[0]->lle_correo;
		$data['auto']=$DATO[0]->lle_autointeres;
		
		}
		if(isset($_GET['TPOLC'])){$TPOLC=$_GET['TPOLC'];
		list($IDLC,$IDCIN)=explode('-',$TPOLC);
		$DATO=$this->Contactomodel->getInfoCotizacion($IDLC);
		
		$data['TPOLC']=$TPOLC;
		$data['asesor']=$DATO[0]->hus_IDhuser;
		$data['contacto']=$DATO[0]->lsp_nombre.' '.$DATO[0]->lsp_apellido;
		$data['telefono']=$DATO[0]->lsp_email;
		$data['correo']=$DATO[0]->cot_email;
		
		}
		$this->form_validation->set_rules('asesor', 'Asesor', 'trim|required');
	    $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'trim|required');
		if($this->input->post('toficina')=='' && $this->input->post('tcasa')==''){
		$this->form_validation->set_rules('toficina', 'Telefono de Casa', 'trim|required|numeric');
		$this->form_validation->set_rules('tcasa', 'Celular', 'trim|required|min_length[9]|max_length[10]|numeric');}
		else{
	    if($this->input->post('toficina')){$this->form_validation->set_rules('toficina', 'Telefono de Casa', 'trim|numeric');}
		if($this->input->post('tcasa')){$this->form_validation->set_rules('tcasa', 'Celular','trim|required|min_length[10]|max_length[10]|numeric');}
		}
		$this->form_validation->set_rules('toficina2', 'Telefono de Oficina ', 'trim|numeric');
		$this->form_validation->set_rules('toficina3', 'Telefono de Oficina 2', 'trim|numeric');
		$this->form_validation->set_rules('radio', 'Radio ', 'trim');
		$this->form_validation->set_rules('correo', 'Correo Electronico', 'trim|required');
		$this->form_validation->set_rules('correob', 'Correo Electronico Alternativo', 'trim');
		$this->form_validation->set_rules('fuente', 'Fuente', 'trim|required');
		$this->form_validation->set_rules('status', 'Tipo de Contacto', 'trim|required');
		$this->form_validation->set_rules('tipoa', 'Tipo de Auto', 'trim|required');
		$this->form_validation->set_rules('ano', 'Año', 'trim|required');
		$this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');
		$this->form_validation->set_rules('color', 'Color', 'trim');
		$this->form_validation->set_rules('publicidad', 'Publicidad', 'trim|required');
		$this->form_validation->set_rules('notas', 'Notas', 'trim');
		$this->form_validation->set_rules('version', 'Version', 'trim');
		$this->form_validation->set_rules('empresa', 'Empresa', 'trim');
		$this->form_validation->set_rules('pmanejo', 'Prueba de Manejo', 'trim|required');
		
        if ($this->form_validation->run())
        {
			
//nombre folder		
$cadena=$this->input->post('nombre').$this->input->post('apellido');
$string=$this->Contactomodel->limpiarEspacios($cadena);
$random=$this->Contactomodel->randomPass(3);
$folder=$this->Contactomodel->limpiarCaracteresEspeciales($string ).$random;
//asesor
$AGV=$this->input->post('asesor');
       $todo = array(
                    'con_nombre'=>$this->input->post('nombre'),
					'con_apellido'=>$this->input->post('apellido'),
					'con_telefono_officina'=>$this->input->post('toficina'),
					'con_telefono_casa'=>$this->input->post('tcasa'),
					'con_correo'=>$this->input->post('correo'),
					'con_correo_b'=>$this->input->post('correob'),
					'con_ocupacion'=>$this->input->post('empresa'),
					'fuente_fue_IDfuente'=>$this->input->post('fuente'),
					'huser_hus_IDhuser'=>$AGV,
					'con_status'=>$this->input->post('status'),
					'con_titulo'=>$this->input->post('titulo'),
					'con_tipo'=>$this->input->post('tipoa'),
					'con_modelo'=>$this->input->post('modelo'),
					'con_ano'=>$this->input->post('ano'),
					'con_color'=>$this->input->post('color'),
					'con_telefono_off'=>$this->input->post('toficina2'),
					'con_telefono_off2'=>$this->input->post('toficina3'),
					'con_radio'=>$this->input->post('radio'),
					'con_notas'=>$this->input->post('notas'),
					'con_folder'=>$folder,
					'publicidad_pub_IDpublicidad'=>$this->input->post('publicidad'),
					'con_hora'=>$this->input->post('hora'),
					'con_version'=>$this->input->post('version'),
					'con_ultima_actividad'=>''.date('Y-m-d').' Alta en el Sistema',
					'con_cliente'=>'no',
					'con_pmanejo'=>$this->input->post('pmanejo'),
					'con_buscando_auto'=>'no'
                    );
// To create the nested structure, the $recursive parameter  to mkdir() must be specified.
$structure = './readpdf/web/expedientes/'.$folder;
if (!mkdir($structure, 0777, true)) { die('Failed to create folders...');}else{chmod($structure, 0777);}			
$IDC=$this->Contactomodel->addcontacto($todo);
$this->Contactomodel->addbitacora($IDC->IDC);
 //Enviar Email de Bienvenida.
$this->Contactomodel->SendemailBienvenida($AGV,$this->input->post('correo'),$this->input->post('titulo'),$this->input->post('nombre'),$this->input->post('apellido'),$_SESSION['ciudad']);
//Agregar 2 tareas 24 y 72 horas.
$idbita=$this->Contactomodel->vbitacora($IDC->IDC);
$this->Contactomodel->addactividadnewcontact($idbita->bit_IDbitacora);
 //Enviar sms de Bienvenida 
$CAU=$this->input->post('tcasa');
if($CAU){$this->Contactomodel->SmsBienvenida($CAU);}
//mensaje
//Editar informacion cotizacion
if(isset($_GET['TPOLC'])){$this->Contactomodel->UpdateCotizaciontoDataBase($_GET['TPOLC'],$IDC->IDC);}
//enviar mensaje de extito	
$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado nuevo Contacto.</div>");            
redirect('contacto');
        }
        else
        {
if($_SESSION['ciudad']=='Tijuana'){
$data['todo_asesor'] = $this->Contactomodel->getAsesor();}
else{
if($_SESSION['ciudad']=='Ensenada'){
$data['todo_asesor'] = $this->Contactomodel->getAsesore();	
	}
else{	
$data['todo_asesor'] = $this->Contactomodel->getAsesorm();
}
}
$data['todo_fuente'] = $this->Contactomodel->getfuente();
$data['todo_publicidad'] = $this->Contactomodel->getpublicidad();
$this->load->view('contacto/add', $data);
        }
    }
	
	
		
		function ViewTransferir($idc){
			
			 $data['flash_message'] = $this->session->flashdata('message');
			$this->form_validation->set_rules('vendedor', 'Asesor de venta', 'required|trim');
		
        if ($this->form_validation->run())
        {
			$this->Contactomodel->transfer($idc,$this->input->post('vendedor'));
			
			//enviar mensaje de extito	
$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha transferido un contacto con exito!.</div>");            
redirect('contacto');
		}
		else{	
			$data['idc']=$idc;
			$data['uno_contacto'] = $this->Contactomodel->getcontacto($idc);	
			$data['todo_vendedor'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
			$this->load->view('contacto/ViewTransferir',$data);
			}
		}
	
	
	function update($id)
    {   
        
	    $this->form_validation->set_rules('hora', 'Hora', 'trim|required');
	    $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'trim|required');
		if($this->input->post('toficina')=='' && $this->input->post('tcasa')==''){
		$this->form_validation->set_rules('toficina', 'Telefono de Casa', 'trim|required');
		$this->form_validation->set_rules('tcasa', 'Celular', 'trim|required|min_length[9]|max_length[10]|numeric');}
		else{
	    if($this->input->post('toficina')){$this->form_validation->set_rules('toficina', 'Telefono de Casa', 'trim');}
		if($this->input->post('tcasa')){$this->form_validation->set_rules('tcasa', 'Celular','trim|required|min_length[10]|max_length[10]|numeric');}
		}
		$this->form_validation->set_rules('toficina2', 'Telefono de Oficina ', 'trim|numeric');
		$this->form_validation->set_rules('toficina3', 'Telefono de Oficina 2', 'trim|numeric');
		$this->form_validation->set_rules('radio', 'Radio ', 'trim');
		$this->form_validation->set_rules('correo', 'Correo Electronico', 'trim|required');
		$this->form_validation->set_rules('correob', 'Correo Electronico Alternativo', 'trim');
		$this->form_validation->set_rules('fuente', 'Fuente', 'trim|required');
		$this->form_validation->set_rules('status', 'Tipo de Contacto', 'trim|required');
		$this->form_validation->set_rules('tipoa', 'Tipo de Auto', 'trim|required');
		$this->form_validation->set_rules('ano', 'Año', 'trim|required|numeric');
		$this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');
		$this->form_validation->set_rules('color', 'Color', 'trim');
		$this->form_validation->set_rules('publicidad', 'Publicidad', 'trim|required');
		$this->form_validation->set_rules('notas', 'Notas', 'trim');
		$this->form_validation->set_rules('version', 'Version', 'trim');
		$this->form_validation->set_rules('empresa', 'Empresa', 'trim');
		$this->form_validation->set_rules('pmanejo', 'Prueba de Manejo', 'trim|required');
		
		if ($this->form_validation->run()){
		$todo = array(
                    'con_nombre'=>$this->input->post('nombre'),
					'con_apellido'=>$this->input->post('apellido'),
					'con_telefono_officina'=>$this->input->post('toficina'),
					'con_telefono_casa'=>$this->input->post('tcasa'),
					'con_correo'=>$this->input->post('correo'),
					'con_correo_b'=>$this->input->post('correob'),
                    'con_ocupacion'=>$this->input->post('empresa'),
					'fuente_fue_IDfuente'=>$this->input->post('fuente'),
					'con_status'=>$this->input->post('status'),
					'con_titulo'=>$this->input->post('titulo'),
					'con_tipo'=>$this->input->post('tipoa'),
					'con_modelo'=>$this->input->post('modelo'),
					'con_ano'=>$this->input->post('ano'),
					'con_color'=>$this->input->post('color'),
					'con_telefono_off'=>$this->input->post('toficina2'),
					'con_telefono_off2'=>$this->input->post('toficina3'),
					'con_radio'=>$this->input->post('radio'),
					'con_notas'=>$this->input->post('notas'),
					'publicidad_pub_IDpublicidad'=>$this->input->post('publicidad'),
					'con_hora'=>$this->input->post('hora'),
					'con_version'=>$this->input->post('version'),
					'con_pmanejo'=>$this->input->post('pmanejo')
                    );
		$this->Contactomodel->updatetodo($todo,$id);
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Su informaci&oacute;n fue actualizada correctamente.</div>");         redirect('contacto');
        }
        else
        {
		$data['uno_contacto'] = $this->Contactomodel->getcontacto($id);	
	    $data['todo_fuente'] = $this->Contactomodel->getfuente();
		$data['todo_publicidad'] = $this->Contactomodel->getpublicidad();
		if(!empty($_GET['edit'])){
			$data['idc']=$id;
			 $this->load->view('contacto/addedit', $data);
			}else{
	    $this->load->view('contacto/edit', $data);
        }}
    }
	
	
	
	
	
	
	
function delete($id)
    {
$data= $this->Contactomodel->getT($id);
if ($this->Contactomodel->delete($id)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado a:  $data->con_nombre. </div>");} else {
$this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('contacto');
}
	
	
	
	function facturar($id){
	
	$todob=array(
			'dat_fecha_facturacion'=>''.date('Y-m-d').''
			);
			//update fecha de facturacion
			$this->Contactomodel->updatefechafacturacion($todob,$id);
			
			$data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Fecha de facturacion agregada</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
		
		}
		
		function example(){
			$data['datos']=$this->Contactomodel->getFuenteEnsenada();
			$this->load->view('contacto/example',$data);
			}
	
	function bitacora($id)
    {   
	
	
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	   $ven=$_SESSION['name'].' '.$_SESSION['apellido'];
	   if($this->Contactomodel->vbitacora($id)){
	 $idb=$this->Contactomodel->vbitacora($id);
	 $data['todo_actividades'] = $this->Contactomodel->getActividades($idb->bit_IDbitacora);
	 $data['registro'] = $this->Contactomodel->getRegistro($idb->bit_IDbitacora);
	
	   }
	   else{
		  $this->Contactomodel->addbitacora($id);
		
	      }
$this->load->model('Inventariomodel');
	   $data['fe1']=date('m').'/'.date('d').'/'.date('Y');
		$data['fe2']=date('m').'/'.date('d').'/'.date('Y');
		
		$data['testado']='testados';
		$data['tact']='tactiviades';

	$data['ordenes_de_compra'] = $this->Contactomodel->getOrdenesdeCompra($id);	
	 $data['todo_hoja_deunarazon'] = $this->Contactomodel->getHojaRazon($id);
	 $data['llamada_entrante'] = $this->Contactomodel->getLlamadaEntrante($id);
	 $data['guia_telefonica']=$this->Contactomodel->getGuiaTelefonica($id);
	 $data['guia_telefonicaInternet']=$this->Contactomodel->getGuiaTelefonicaInternet($id);
	 
	 if(empty($_SESSION['coucheo'])){ $data['id_vendedor']='';}
		else{
		$data['id_vendedor']=$_SESSION['coucheo'];
		
			
		}
		 

        $data['lista_cotizacion']= $this->Contactomodel->getCotizacionLista($id);		 
	    $data['todo_credito'] = $this->Contactomodel->getsolicitudcredito($id);	
	    $data['todo_generales'] = $this->Contactomodel->getgenerales($_SESSION['id']);
	    $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($id);	
		$this->load->view('contacto/bitacora', $data);
       
    }






function bitacorabus($id,$fe1,$fe2,$esta,$act)
    {   
	
	
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	   $ven=$_SESSION['name'].' '.$_SESSION['apellido'];
	   if($this->Contactomodel->vbitacora($id)){
		$idb=$this->Contactomodel->vbitacora($id);
	
	 $data['todo_actividades'] = $this->Contactomodel->getActividadesbus($idb->bit_IDbitacora,$fe1,$fe2,$esta,$act);
	 
	 
	 $data['registro'] = $this->Contactomodel->getRegistro($idb->bit_IDbitacora);
	
	   }
	   else{
		  $this->Contactomodel->addbitacora($id);
		
	      }
	   $data['ordenes_de_compra'] = $this->Contactomodel->getOrdenesdeCompra($id);	
	    $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($id);	
		 $data['llamada_entrante'] = $this->Contactomodel->getLlamadaEntrante($id);
		 $data['guia_telefonica']=$this->Contactomodel->getGuiaTelefonica($id);
		 
		  if(empty($_SESSION['coucheo'])){ $data['id_vendedor']='';}
		else{
		$data['id_vendedor']=$_SESSION['coucheo'];
		
			
		}
		 
		 
		  $data['guia_telefonicaInternet']=$this->Contactomodel->getGuiaTelefonicaInternet($id);
		 
		list($a,$m,$d)=explode('-',$fe1); $fe1=$m.'/'.$d.'/'.$a;
		list($a2,$m2,$d2)=explode('-',$fe2); $fe2=$m2.'/'.$d2.'/'.$a2;
		
		$data['fe1']=$fe1;
		$data['fe2']=$fe2;
		
		$data['testado']=$esta;
		$data['tact']=$act;
		$data['todo_hoja_deunarazon'] = $this->Contactomodel->getHojaRazon($id);
		$this->load->view('contacto/bitacora', $data);
       
    }
	
	
	
	function vistatarea($id,$ido,$idt)
    {   
	
	session_start();
		if ( !isset($_SESSION['id']) ) {
         redirect('admin');
      }
	  
	   
	    $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($id);	
	    $data['todo_oportunidades'] = $this->Contactomodel->getOportunidadesV($ido);
		 $data['todot_oportunidades'] = $this->Contactomodel->getOportunidadesAdd($id);
		$data['todo_tareas'] = $this->Contactomodel->gettareas($idt);	
		$this->load->view('contacto/vistatarea', $data);
       
    }
	
	function editLlamadaEntrante($idll,$idc)
	 {   
	
	session_start();
		if ( !isset($_SESSION['id']) ) {
         redirect('admin');
      }
	  
	   	$data['uno_llamadasentrantes'] = $this->Hppmodel->getLlamadasEntrantesidedit($id);
	  $this->load->view('hpp/editLlamadaEntrante',$data);
       
    }
	
		
	function vistaHojaRazon($id,$ido){   

$this->form_validation->set_rules('cliente', 'Cliente', '');
if ($this->form_validation->run())
        {
        	
				$todo =  array('hoj_fecha'=>$this->input->post('fechaini'),
				'hoj_cliente'=>$this->input->post('cliente'),
				'hoj_unidadano'=>$this->input->post('unidaano'),
				'hoj_unidadtipo'=>$this->input->post('unidatipo'),
				'hoj_telefonomejor'=>$this->input->post('telefonomejor'),
				'hoj_telefonosiguiente'=>$this->input->post('telefonosiguinete'),
				'hoj_telefono'=>$this->input->post('telefonootro'),
				'hoj_emailmejor'=>$this->input->post('correomejor'),
				'hoj_emailsiguiente'=>$this->input->post('correosiguiente'),
				'hoj_pendiente'=>$this->input->post('preguntapen'),
				'hoj_nombrevehi'=>$this->input->post('nomveiculo'),
				'hoj_comparado'=>$this->input->post('comparado'),
				'hoj_sorprendio'=>$this->input->post('sorprendio'),
				'hoj_unicarazon'=>$this->input->post('unicarazon'),
				'hoj_comentgerente'=>$this->input->post('comentario'),
				'hoj_fechafin'=>$this->input->post('fechafin'),
				'hoj_hora'=>$this->input->post('hora'),
				'hoj_min'=>$this->input->post('minutos'),
				'hoj_confirmado'=>$this->input->post('confirmado'));
		
		$this->Contactomodel->updateHojaRazon($todo,$ido);
		
		
		
		
		   
		   $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");            
            redirect('contacto/bitacora/'.$id.'');
        	
			
			
		}
	  else{
	   
	    $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($id);	
	    $data['todo_hojaRazon'] = $this->Contactomodel->getAllHojaRazon($ido);
        $this->load->view('hpp/show_una_razon_edit', $data);
	  }
    }
	
	function vistatareagenerales($id,$idt)
    {   
	
	session_start();
		if ( !isset($_SESSION['id']) ) {
         redirect('admin');
      }
	  
	   
	    $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($id);	

		 $data['todot_oportunidades'] = $this->Contactomodel->getOportunidadesAdd($id);
		$data['todo_tareas_generales'] = $this->Contactomodel->gettareasgenerales($idt);	
		$this->load->view('contacto/vistatareagenerales', $data);
       
    }
	
function updatetarea($ida,$idc,$idb){   
$this->form_validation->set_rules('titulo', 'Titulo', 'required');
if ($this->form_validation->run()){
$fei=$this->Contactomodel->fechaslash($this->input->post('fei'));
$time=$this->input->post('hora');list($hora,$min)=explode(":",$time);
$act=$this->input->post('categoria');
$todo = array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$idb,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					
                    );
$this->Contactomodel->updatetarea($todo,$ida);
 $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");
redirect('contacto/bitacora/'.$idc.'');
        }
        else
        {
		$data['tipo_mensage'] = $this->Contactomodel->getTipoMensage();
		$data['idb']=$idb;
		$data['idc']=$idc;
		$data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);	
		$data['todo_tareas'] = $this->Contactomodel->gettareas($ida);	
		$this->load->view('contacto/vistatarea', $data);
        }
    }
	

	
	function updatecomentario($ida,$idc,$idb){   
$this->form_validation->set_rules('titulo', 'Titulo', 'required');
 if ($this->form_validation->run()){
$todo = array(
              'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc')
              );
$this->Contactomodel->updatetarea($todo,$ida);
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>"); redirect('contacto/bitacora/'.$idc.'');
        }
        else{
		$data['idb']=$idb;
		$data['idc']=$idc;
		$data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);	
		$data['todo_tareas'] = $this->Contactomodel->gettareas($ida);	
		$this->load->view('contacto/vistatareacomentario', $data);
        }
    }
	
	
	function updatetareagenerales($ida,$idc)
    {   
	
	session_start();
		if ( !isset($_SESSION['id']) ) {
         redirect('admin');
      }
	    
	    $this->load->library('form_validation');
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		$this->form_validation->set_rules('fei', 'Fecha de Inicio', 'required');
		$this->form_validation->set_rules('fef', 'Fecha Fin', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required');
			
 
        if ($this->form_validation->run())
        {
		
		$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			
			
			$llegadab=$this->input->post('fef');
			list($mes,$dia,$ano)=explode('/',$llegadab);
			$fef=$ano.'-'.$mes.'-'.$dia;
			
			$time=$this->input->post('hora');
			list($hora,$min)=explode(":",$time);
			
			$act=$this->input->post('categoria');
			if($act=='generales')
			{
			
			$todog= array(
                    'gen_status'=>$this->input->post('status'),
					'gen_fecha_inicio'=>$fei,
					'gen_fecha_fin'=>$fef,
					'gen_titulo'=>$this->input->post('titulo'),
					'gen_descripcion'=>$this->input->post('desc'),
					'gen_categoria'=>$this->input->post('categoria'),
					'gen_hora'=>$hora,
					'gen_min'=>$min,
					'huser_hus_IDhuser'=>$_SESSION['id'],
                    );
			
		$td=$todog;$this->Contactomodel->updategenerales($td,$ida);
		
			}
			else{
		$todo = array(
                    'act_status'=>$this->input->post('status'),
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fef,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'oportunidades_opo_IDoportunidad'=>$this->input->post('categoria'),
					'act_hora'=>$hora,
					'act_min'=>$min
					
                    );
		
		$this->Contactomodel->insertactividad($todo,$ida);
			}
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la Informacion con exito.</div>");            
            redirect('contacto/bitacora/'.$idc.'');
        }
        else
        {
		 $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($id);	

		 $data['todot_oportunidades'] = $this->Contactomodel->getOportunidadesAdd($id);
		$data['todo_tareas_generales'] = $this->Contactomodel->gettareasgenerales($ida);	
		$this->load->view('contacto/vistatareagenerales', $data);
			 
		
        }
    }
	
	
	
	
	
	function addtareacomentario($idc,$idb){   
$fei=date('Y').'-'.date('m').'-'.date('d');
if($this->input->post('titulo')=='' && $this->input->post('desc')==''){
$this->session->set_flashdata('message', " <br><div class='alert alert-error'>
<button class='close' data-dismiss='alert' type='button'>×</button>Es Necesario que escriba un titulo y una descripcion al agregar un comentario.</div>");            
redirect('contacto/bitacora/'.$idc.'');
}
else{
		$todoa= array(
                    'act_status'=>'realizada',
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>'09',
					'act_min'=>'00',
					'bitacora_bit_IDbitacora'=>$idb,
					'tipo_actividad_tac_IDtipo_actividad'=>4,
					'act_notificar'=>'0',
					'tipo_mensage_tip_IDtipo_mensage'=>'0',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
					
					$this->Contactomodel->inserttareaa($todoa);		
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado un Comentario.</div>");            
            redirect('contacto/bitacora/'.$idc.'');
			}
    }
	
	
	function addtarea($idc,$idb)
    {   
$this->load->library('form_validation');
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		
			
 
        if ($this->form_validation->run())
        {
		
		$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			$time=$this->input->post('hora');
			list($hora,$min)=explode(":",$time);
			
		$todoa= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>$this->input->post('titulo'),
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$idb,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'0',
					'tipo_mensage_tip_IDtipo_mensage'=>'0',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
					
					$this->Contactomodel->inserttareaa($todoa);
					
					
					//notificacion para agente de ventas
			$hoy=date('Y').'-'.date('m').'-'.date('d');
		///////////Enviar Email
		
			$titulo=$this->input->post('titulo');
			$contacto=$this->Contactomodel->getNameContacto($idc);
			$this->load->library('email');
			
		 $huser=$this->Contactomodel->getHuserCon($idc);
		 $huser[0]->hus_IDhuser;
	$this->Calendariomodel->SendEmailTareaVendedor($_SESSION['username'],$huser[0]->hus_IDhuser,$fei,$hora,$min,$this->input->post('titulo'),$this->input->post('desc'));
			
		//Obtener info vendedor
		$huser=$this->Vendedoresmodel->getId($huser[0]->hus_IDhuser);		
		$CAU=$huser[0]->hus_celular;
	    //funcion enviar sms a un vendedor		
		if($CAU)
		{
		if($hora < 12){$ty="am";}else{$ty="pm";}
		$this->Calendariomodel->SendSmsTareaVendedor($CAU,$fei,$hora,$min,$ty,$this->input->post('titulo'),$this->input->post('desc'));
		}
						
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una Tarea nueva.</div>");            
            redirect('contacto/bitacora/'.$idc.'');
        }
        else
        {
	  
	
        $data['idb']=$idb;
	    $data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);	
		$this->load->view('contacto/addtarea', $data);
		}
    }
	
	
	function addComentarioCoucheo($idc,$idb)
    {   
$this->load->library('form_validation');
$this->form_validation->set_rules('desc', 'Descripcion', 'required');
if ($this->form_validation->run()){
		    $llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			$hora=date('H');
			$min=date('i');
			
		$todoa= array(
                    'act_status'=>'realizada',
					'act_fecha_inicio'=>$fei,
					'act_fecha_fin'=>$fei,
					'act_titulo'=>'Coucheo',
					'act_descripcion'=>$this->input->post('desc'),
					'act_hora'=>$hora,
					'act_min'=>$min,
					'bitacora_bit_IDbitacora'=>$idb,
					'tipo_actividad_tac_IDtipo_actividad'=>7,
					'act_notificar'=>'0',
					'tipo_mensage_tip_IDtipo_mensage'=>'0',
					'fuente_IDfiuente'=>0
                    );
					
					$this->Contactomodel->inserttareaa($todoa);
				$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Registrado su Coucheo con Exito.</div>");            
            redirect('contacto/bitacora/'.$idc.'');
        }
        else
        {
	  
		}
    }
	
	


function deletehojarazon($idc,$ido)
    {

        if ($this->Contactomodel->deletehojarazon($ido)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una Hoja de Razon. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('contacto/bitacora/'.$idc.'');
 
    }
	
	function deleteCotizacion($id,$idc){
		      if ($this->Contactomodel->deleteCotizacion($id)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado con exito. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('contacto/bitacora/'.$idc.'');
		}
	
	
	
	function deletegenerales($idc,$ida)
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('admin');
      }
        if ($this->Contactomodel->deletegenerales($ida)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una Tarea </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('contacto/bitacora/'.$idc.'');
 
    }
	
function deletetareas($idc,$ida){
if ($this->Contactomodel->deletetareas($ida)) {
$this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una Tarea </div>");                        
} else {
$this->session->set_flashdata('message', "No se encontraron datos. Error."); }
redirect('contacto/bitacora/'.$idc.'');
}
	
	
		
function tareasgenerales()
	{ 
	
	session_start();
		if ( !isset($_SESSION['username']) ) {redirect('admin');}
	
	  $this->load->model('Contactomodel');
		$data['todo_generales'] = $this->Contactomodel->getgenerales($_SESSION['id']);
	    $this->load->view('contacto/tareasgenerales',$data);
	}	
	
	 function ordenesdecompra($idc,$msg)
    {

		
		$data['flash_message'] = $this->session->flashdata('message');
		$data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);
		
		if($msg=='crear'){
			
			$LastID=$this->Contactomodel->IdOrdendeCompra($idc);
			
			$LastID->UID;
			
			$this->Contactomodel->InsertSet($LastID->UID,$idc);
			 $IDOC=$LastID->UID +1;
			$data['IDOC']=$IDOC;
			
		}
		redirect('contacto/ordenesdecompraedit/'.$idc.'/'.$IDOC.'/');
		
    }
	
	
	 function ordenesdecompraedit($idc,$ido){
		$data['flash_message'] = $this->session->flashdata('message');
		$data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);
		$data['IDOC']=$ido;
		$data['todo_ordendecompra'] = $this->Contactomodel->getAllDataOrdendeCompra($ido);
		$data['procesodeventa']=$this->Contactomodel->getAllProcesodeventa($ido);
		$data['asesordeVenta']=$this->Contactomodel->getAllDataOrdendeCompraUser($ido);
		$data['auto_apartados'] = $this->Contactomodel->getAutosApartados($idc);
		$data['candado']=$this->Contactomodel->getCandado($ido);
		$data['detalle_proceso']=$this->Contactomodel->Getprocesodeventadd($ido);
		$data['candado']=$this->Contactomodel->getCandado($ido);
		$this->load->view('contacto/ordenesdecompraedit', $data);
        }
	
	 function insertauto($ida,$ido,$idc){
	 $data['flash_message'] = $this->session->flashdata('message');
	 $dataauto = $this->Contactomodel->getDataAuto($ida);
	 $todoa= array(
                    'data_auto'=>$dataauto[0]->aau_modelo.' '.$dataauto[0]->aau_color_exterior,
					'data_carroceria'=>$dataauto[0]->aau_puertas.' Puertas',
					'data_capacidad'=>$dataauto[0]->aau_pasajeros.' Pasajeros',
                    'data_marca'=>'Honda',
					'data_modelo'=>$dataauto[0]->aau_modelo,
					'data_ano'=>$dataauto[0]->aau_ano,
                    'data_color'=>$dataauto[0]->aau_color_exterior,
                    'data_motor'=>$dataauto[0]->aau_cilindros.' Cilindros',
					'data_no_motor'=>$dataauto[0]->aau_no_motor,  
					'data_vin'=>$dataauto[0]->aau_IdFk,
                    );
					
		/*$todoa= array(
                    'datco_nuevo'=>$demo,
					'datco_snuevo'=>$nuevo,
					'datco_nuevox'=>$semi
                    );*/			
					
					$this->Contactomodel->updateDatosAuto($todoa,$ido);
					/*$this->Contactomodel->updateDatosAutoTipo($todob,$ido);*/
	redirect('contacto/ordenesdecompraedit/'.$idc.'/'.$ido.'');
	
	}
	
		 function seguimiento()
    {
			  
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	  if($_SESSION['nivel']=='Administrador'){
		if($_SESSION['username']=='monica@hondaoptima.com' || $_SESSION['username']=='contaens@hondaoptima.com') {
			$data['todo_procesodeventa']=$this->Contactomodel->Getprocesodeventaconta();
			$this->load->view('contacto/seguimiento',$data);
			}
		else{
	  if($_SESSION['username']=='josemaciel@hondaoptima.com' || $_SESSION['username']=='sgutierrez@hondaoptima.com') {
		  
            $data['Tijuana']=$this->Contactomodel->Getprocesodeventa('Tijuana');
			$data['Mexicali']=$this->Contactomodel->Getprocesodeventa('Mexicali');
			$data['Ensenada']=$this->Contactomodel->Getprocesodeventa('Ensenada');
			if(empty($_GET['age']) || $_GET['age']=='all'){
			$data['todo_procesodeventa']=$this->Contactomodel->GetprocesodeventaAdmin();
				}
				else{
			$data['todo_procesodeventa']=$this->Contactomodel->Getprocesodeventa($_GET['age']);		
					}		  
			$this->load->view('contacto/seguimientoA',$data);
			}
			else{		 
	  $data['todo_procesodeventa']=$this->Contactomodel->Getprocesodeventa($_SESSION['ciudad']);
	  $this->load->view('contacto/seguimiento',$data);
	  }}
	  }else{
		  
		  $data['todo_procesodeventa']=$this->Contactomodel->GetprocesodeventaHuser($_SESSION['id']);
		  $this->load->view('contacto/seguimiento',$data);
		  
		  }
	
        
 
    }
	
	
			 function entregados()
    {
			  
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	  if($_SESSION['nivel']=='Administrador'){
		if($_SESSION['username']=='monica@hondaoptima.com' || $_SESSION['username']=='contaens@hondaoptima.com') {
			$data['todo_procesodeventa']=$this->Contactomodel->GetprocesodeventacontaEntregados();
			}
		else{
	  if($_SESSION['username']=='josemaciel@hondaoptima.com') {
			$data['todo_procesodeventa']=$this->Contactomodel->GetprocesodeventaAdmin();
			}
			else{		 
	  $data['todo_procesodeventa']=$this->Contactomodel->Getprocesodeventa($_SESSION['ciudad']);
	  }}
	  }else{
		  
		  $data['todo_procesodeventa']=$this->Contactomodel->GetprocesodeventaHuser($_SESSION['id']);
		  }
	
        $this->load->view('contacto/entregados',$data);
 
    }
	
	
	function pruebaemail(){
		//$this->Contactomodel->sendEmailClienteTj(2538);
		//$this->Contactomodel->sendEmailClienteEs(8684);
		
		$this->Contactomodel->sendEmailClienteMx(8722);
		
		}
	
	 function convertirventa($id,$idb,$idc,$vin)
    {$data['flash_message'] = $this->session->flashdata('message');
	  $fecha=date('Y').'-'.date('m').'-'.date('d');
	  $todo=array('prcv_status'=>'venta');
	  $todoseg=array('aau_status'=>'Entregado');
	  $todob=array(
	   'ven_fecha'=>$fecha,
	   'ven_status'=>'on',
	   'ven_fk_IDventa'=>$id
	   );
	 $todocontacto=array('con_status'=>'Cliente','con_cliente'=>'si');
	 
if($_SESSION['ciudad']=='Tijuana'){
$email='mercadotecnia@hondaoptima.com';
$this->Contactomodel->sendEmailClienteTj($id);
}
if($_SESSION['ciudad']=='Mexicali'){
$email='lucero@hondaoptima.com';
$this->Contactomodel->sendEmailClienteMx($id);
}
if($_SESSION['ciudad']=='Ensenada'){
$email='andream@hondaoptima.com';
$this->Contactomodel->sendEmailClienteEs($id);
}
	   
	$this->Contactomodel->updateseguimiento($todo,$id);
	$this->Contactomodel->insertventa($todob);
	$this->Contactomodel->addactividad($idb);
	$this->Contactomodel->updatetodo($todocontacto,$idc);
	$this->Contactomodel->updateApartadoA($todoseg,$vin);
	
	$this->Contactomodel->sendemailmercaMkt($email,$id);
	

	
    redirect('contacto/seguimiento');
 
    }
	
	
	

		 function insertpedido(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$ido=$this->input->post('ido');
		$idc=$this->input->post('idc');
		$idh=$this->input->post('idh');
		$idproceso=$this->input->post('idproceso');
		$res=$this->Contactomodel->login($user,$pass);
		if($res){
			$todo=array(
			'prcv_status_asesor'=>'aprobado'
			);
		$this->Contactomodel->insertseguimiento($todo,$idproceso);
		
		  $this->Contactomodel->enviaraviso($idh,$idc,$_SESSION['ciudad']);
		 $this->Contactomodel->smsenviaraviso($idc,$idh);
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Su aviso de pedido se encuentra en proceso de venta</div>");  
			
			redirect('contacto/bitacora/'.$idc.'/');
			}
			else{redirect('contacto/ordenesdecompraedit/'.$idc.'/'.$ido.'');}
		
					
                  
			
		}else{
			
			redirect('contacto/ordenesdecompraedit/'.$idc.'/'.$ido.'');
			}
	
      
 
    }	
	
	
	
	
		 function eliminarpedido()
    {
		
	    $this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
	  
	  if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$idp=$this->input->post('idd');
		$idc=$this->input->post('idc');
		$res=$this->Contactomodel->login($user,$pass);
		if($res){
			
		//$this->Contactomodel->deleteavisodepedido($idp);
		
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Su aviso de Pedido fue cancelado</div>");  
			
			redirect('contacto/bitacora/'.$idc.'/');
			}
			else{redirect('contacto/ordenesdecompraedit/'.$idc.'/'.$ido.'');}
		
					
                  
			
		}else{
			
			redirect('contacto/ordenesdecompraedit/'.$idc.'/'.$ido.'');
			}
	
      
 
    }	
	
	
	
	
	
	
	
	
	
	function aprobargerente(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$idh=$this->input->post('asesor');
		$nombre=$this->input->post('nombre');
		$ciudadase=$this->input->post('ciudadase');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_status_gerente'=>'aprobado'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			
			//enviar email
			 $this->Contactomodel->smsenviaravisofactura($idh,$nombre,$ciudadase);
			//enviar sms
			 $this->Contactomodel->enviaravisofactura($idh,$nombre,$ciudadase);
			
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de pedido aprobado por el gerente</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');
				}
		
					
                  
			
		}else{
			$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
			redirect('contacto/detalleseguimiento/'.$id.'');
			}
	
      
 
    }	
	
	function updateProcesodeVenta($id){
		$todob=array(
			'prcv_status'=>'proceso',
			'prcv_prcv_status_credito_contado'=>'proceso',
			'prcv_status_gerente'=>'proceso',
			'prcv_status_fi'=>'proceso',
			'prcv_status_contabilidad'=>'proceso',
			'prcv_director'=>'proceso',
			'prcv_director_gral'=>'proceso',
			'prcv_status_asesor'=>'proceso',
			);
		//update fecha de facturacion
			$this->Contactomodel->insertseguimiento($todob,$id);
			
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Facturacion Cancelada</div>");  
			
			redirect('contacto/seguimiento');
		}
	
	function aprobarfac(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$idh=$this->input->post('asesor');
		$fecha=$this->input->post('fecha');
		$nombre=$this->input->post('nombre');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_prcv_status_credito_contado'=>'aprobado'
			);
			
				
			
			$this->Contactomodel->updateseguimiento($todo,$id);
			
			//enviar email
			$this->Contactomodel->smsenviaravisoasesor($idh,$nombre);
			//enviar sms
			$this->Contactomodel->enviaravisoasesor($idh,$nombre);
			
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de pedido aprobado por Facturaci&oacute;n</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
}else{
	$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
	redirect('contacto/detalleseguimiento/'.$id.'');}}	



	function aprobarasedos(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$idh=$this->input->post('asesor');
		$nombre=$this->input->post('nombre');
		$res=$this->Contactomodel->loginasesor($user,$pass);
		if($res){
			$todo=array(
			'prcv_director_gral'=>'aprobado'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			
			//enviar email
			$this->Contactomodel->smsenviaravisoconta($idh,$nombre);
			//enviar sms
			$this->Contactomodel->enviaravisoconta($idh,$nombre);
			
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de pedido aprobado por Asesor</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
}else{
	$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
	redirect('contacto/detalleseguimiento/'.$id.'');}}	


	
	function desaprobarasedos(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_director_gral'=>'proceso'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido cancelado por Asesor</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
}else{
	$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
	redirect('contacto/detalleseguimiento/'.$id.'');
			} }	

	function desaprobarfac(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_prcv_status_credito_contado'=>'proceso'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido cancelado por Facturaci&oacute;n</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{

			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
}else{
	$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
	redirect('contacto/detalleseguimiento/'.$id.'');
			} }		
	
	function desaprobargerente(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_status_gerente'=>'proceso'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido cancelado por el gerente</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
}else{
	
	$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
	redirect('contacto/detalleseguimiento/'.$id.'');
			} }	
	
	
		function aprobarfi(){
$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
 if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$idh=$this->input->post('asesor');
		$nombre=$this->input->post('nombre');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
	
		if($res){
		
			$todo=array(
			'prcv_status_fi'=>'aprobado'
			);
		
			
			$this->Contactomodel->updateseguimiento($todo,$id);
			
			//enviar email
			$this->Contactomodel->smsenviaravisogerente($idh,$nombre);
			//enviar sms
			$this->Contactomodel->enviaravisogerente($idh,$nombre);
			
			
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido aprobado por F&I</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
		
					
                  
			
		}else{
			$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
			redirect('contacto/detalleseguimiento/'.$id.'');
			}
	
      
 
    }	
	
	
	function desaprobarfi()
    {

			$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
	  
	  if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_status_fi'=>'proceso'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>F&I aprobaci&oacute;n cancelada</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
		
					
                  
			
		}else{
			$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
			redirect('contacto/detalleseguimiento/'.$id.'');
			}
	
      
 
    }
	
	
	
	
	function aprobarcont()
    {
		if($_SESSION['username']=='monica@hondaoptima.com'){
			$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'trim');
			}
		else{
			$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
			}
		if ($this->form_validation->run())
        {
		if($_SESSION['username']=='monica@hondaoptima.com'){}
		else{
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		}
		$id=$this->input->post('id');
		$idc=$this->input->post('idc');
		$tipo=$this->input->post('tipo');
		if($_SESSION['username']=='monica@hondaoptima.com'){
		$res=1;	
			}
		else{
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		}
		if($res){
		$todo=array(
			'prcv_status_contabilidad'=>'aprobado'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			//$this->Contactomodel->enviaravisoventa($_SESSION['id'],$idc,$_SESSION['ciudad']);
			$data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido aprobado por Contabilidad</div>");  
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
				$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido aprobado por Contabilidad</div>");  
				redirect('contacto/detalleseguimiento/'.$id.'');}
		    }else{
				$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>");  
			redirect('contacto/detalleseguimiento/'.$id.'');
			}
 }	
	
	function desaprobarcont()
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('admin');
      }
	    $this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Correo Electr&oacute;nico', 'required');
	  
	  if ($this->form_validation->run())
        {
		$user=$this->input->post('email');
		$pass=$this->input->post('pass');
		$id=$this->input->post('id');
		$tipo=$this->input->post('tipo');
		$res=$this->Contactomodel->logingerente($user,$pass,$tipo);
		if($res){
			$todo=array(
			'prcv_status_contabilidad'=>'proceso'
			);
			$this->Contactomodel->updateseguimiento($todo,$id);
			 $data['flash_message'] = $this->session->flashdata('message');
			 $this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Aviso de Pedido en proceso por Contabilidad</div>");  
			
			redirect('contacto/detalleseguimiento/'.$id.'/');
			}
			else{
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
				redirect('contacto/detalleseguimiento/'.$id.'');}
		
					
                  
			
		}else{
			$id=$this->input->post('id');
			$this->session->set_flashdata('message', " <br><div class='alert alert-danger'><button class='close' data-dismiss='alert' type='button'>×</button>Usuario o Contraseña, inconrecta.</div>"); 
			redirect('contacto/detalleseguimiento/'.$id.'');
			}
	
      
 
    }
	
	
	
	
	
	
	
	
	function detalleseguimiento($idd)
    {
		
	  $data['flash_message'] = $this->session->flashdata('message');
	  $data['candado']=$this->Contactomodel->Getcontabilidad1($idd);
	  $data['gerencia']=$this->Contactomodel->Getcontabilidad2($idd);
	  $data['casesor']=$this->Contactomodel->Getcontabilidad3($idd);
	  $data['cmerca']=$this->Contactomodel->Getcontabilidad4($idd);
	  $data['ccontacto']=$this->Contactomodel->Getcontabilidad5($idd);
	  $data['idd']=$idd;
	  $CD=$this->Contactomodel->getAllDataOrdendeCompraUser($idd);
	  $data['lista_fechas']=$this->Contactomodel->Getfechaentrega($CD[0]->hus_ciudad);
	  $data['info_venta']=$this->Contactomodel->getAllDataOrdendeCompraUser($idd);
	  $data['info_entrega']=$this->Contactomodel->getAllDataEntregaDetalle($idd);
	  $data['detalle_procesodeventa']=$this->Contactomodel->Getprocesodeventadetalle($idd);
	  
	  if($CD[0]->hus_ciudad=='Tijuana'){
		$data['fandi']='Isela Ruiz';
		$data['facturacion']='Ivone de los Palos';
		$data['contabilidad']='Monica';
		$data['DatFandi']='6641249766/eruiz@hondaoptima.com'; 
		$data['DatGeren']='6646834480/frivera@hondaoptima.com';
		$data['DatFactu']='6643140506/auxventas@hondaoptima.com';
		$data['DatContab']='6642717852/monica@hondaoptima.com'; 
		$data['gerente']='Fernando Rivera'; 
	    }
	if($CD[0]->hus_ciudad=='Mexicali'){
		$data['fandi']='Martha Ofelia Tob&oacute;n Mu&ntilde;oz'; 
		$data['contabilidad']='Karla Monges'; 
		$data['facturacion']='Alicia Leon';
		$data['DatFandi']='6861434505/creditomxli@hondaoptima.com'; 
		$data['DatGeren']='6861838343/oscarv@hondaoptima.com';
		$data['DatFactu']='6861060692/auxmex@hondaoptima.com';
		$data['DatContab']='6862276020/contamxli@hondaoptima.com'; 
		$data['gerente']='Oscar Vega';
		}
		if($CD[0]->hus_ciudad=='Ensenada'){
		$data['fandi']='Erika Granados Amador'; 
		$data['contabilidad']='Monica'; 
		$data['facturacion']='Adriana Karina Martinez Galvez';
		$data['DatFandi']='6461380326/egranados@hondaoptima.com'; 
		$data['DatGeren']='6462274470/afierro@hondaoptima.com';
		$data['DatFactu']='6461288180/amartinez@hondaoptima.com';
		$data['DatContab']='6642274470/contaens@hondaoptima.com'; 
		$data['gerente']='Alberto Manuel Fierro';
		}
	  $this->load->view('contacto/detalleseguimiento',$data);
 
    }
	
	function fechaentrega($idd,$ido)
    {
	
	  $data['flash_message'] = $this->session->flashdata('message');
	  $this->load->library('form_validation');
      $this->form_validation->set_rules('fei', 'Fecha de Entrega', '');
		
 
        if ($this->form_validation->run())
        {
			$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
		$todo= array(
					'fen_date'=>$fei,
					'fen_hora'=>$this->input->post('hora'),
					'fen_comentario'=>$this->input->post('notas'),
					'fen_Id_Ordendecompra'=>$this->input->post('ido'),
					'fen_status'=>'Agregada' );
					$todob= array('dat_fecha_entrega'=>$fei,'dat_hora'=>$this->input->post('hora'));
					$todoc= array('prcv_fechaentrega'=>$fei);
				$this->Contactomodel->insertfechaentrega($todo);
				$this->Contactomodel->editfechaentregaseguimiento($todob,$this->input->post('ido'));
				$this->Contactomodel->editfechaentregaseguimientodos($todoc,$this->input->post('ido'));
				
			
				if($_SESSION['ciudad']=='Tijuana'){
					
					$this->Contactomodel->sendemailmerca('mercadotecnia@hondaoptima.com',$ido,$fei);
					}
				$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado una nueva fecha de Entrega</div>");            
           redirect('contacto/detalleseguimiento/'.$ido.'');
		}else{
	  
	  
redirect('contacto/detalleseguimiento/'.$ido.'');
 
    }
	}
	
		function fechaentregaedit($idd,$ido,$idf){
	    $data['flash_message'] = $this->session->flashdata('message');
        $this->form_validation->set_rules('fei', 'Fecha de Entrega', '');
		
		if ($this->form_validation->run()) {
			$llegada=$this->input->post('fei');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fei=$ano.'-'.$mes.'-'.$dia;
			
			$todo= array('fen_date'=>$fei,
					'fen_hora'=>$this->input->post('hora'),
					'fen_comentario'=>$this->input->post('notas'));
		    $todob= array('dat_fecha_entrega'=>$fei,'dat_hora'=>$this->input->post('hora'));
		    $todoc= array('prcv_fechaentrega'=>$fei);			
				
			$this->Contactomodel->editfechaentrega($todo,$idf);
			$this->Contactomodel->editfechaentregaseguimiento($todob,$this->input->post('ido'));
			$this->Contactomodel->editfechaentregaseguimientodos($todoc,$this->input->post('ido'));
			$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizada la informacion de entrega</div>");            
          redirect('contacto/detalleseguimiento/'.$idd.'');
		}else{}
	}
	
	
function savecredito($idc){
$fecha=date('Y').'-'.date('m').'-'.date('d');
		$todoa= array(
                    'scr_identificacion'=>$this->input->post('ide'),
					'scr_estadocouenta'=>$this->input->post('esta'),
					'src_com_domicilio'=>$this->input->post('domi'),
					'src_fecha'=>$fecha,
					'src_con_IDcontacto'=>$idc,
					'src_status'=>'Pendiente',
					'src_comentario'=>'');
					
					$this->Contactomodel->insertcredito($todoa);
					$this->Contactomodel->emailcredito($idc,$_SESSION['id']);
					$this->Contactomodel->smscredito($idc,$_SESSION['id']);		
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha enviado una notificacion de credito a F&I</div>");            
            redirect('contacto/bitacora/'.$idc.'');
			
    }
	function updatecredito($idc){
		$this->Contactomodel->deletecredito($idc);
$fecha=date('Y').'-'.date('m').'-'.date('d');
		$todoa= array(
                    'scr_identificacion'=>$this->input->post('ide'),
					'scr_estadocouenta'=>$this->input->post('esta'),
					'src_com_domicilio'=>$this->input->post('domi'),
					'src_fecha'=>$fecha,
					'src_con_IDcontacto'=>$idc,
					'src_status'=>'Pendiente',
					'src_comentario'=>'');
					
					$this->Contactomodel->insertcredito($todoa);
					$this->Contactomodel->emailcredito($idc,$_SESSION['id']);		
					
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha enviado una notificacion de credito a F&I</div>");            
            redirect('contacto/bitacora/'.$idc.'');
			
    }
	
	
	function pdf($id){
	//consultar info
	$info=$this->Contactomodel->getAllDataOrdendeCompra($id);
	
	if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}

$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
//echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Hoja de entrega');
    $pdf->SetSubject('Hoja de entrega');
    $pdf->SetKeywords('Hoja de entrega, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           Hoja de entrega', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
    // Set some content to print
//$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
$html = '								
 <p>
 <span style="color:black;font-size:1em;font-weight:bold;">Lugar y fecha:</span><span style="text-decoration:none;background-color:#ffcc99;color:black;"> '.$_SESSION['ciudad'].' Baja California </span><span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;">'.date('d').'  de</span><span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;">'.$meses[date('n')-1].'</span><span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.date('Y').' </span>
 </p>
 <p>Recibi(mos) de Optima Automotriz, S.A. de C.V.  El vehiculo y los documentos que a 							
continuacion se detallan:</p>  
<table width="100%">
<tr>
<td>
<b>Marca</b>
</td><td>
<span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.$info[0]->data_marca.' </span>
</td><td>
<b>Modelo</b>
</td><td>
<span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.$info[0]->data_modelo.'	</span>
</td><td>
<b>Version</b>
</td><td>
<span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.$info[0]->data_version.' </span>
</td><td>
<b>Año</b>
</td><td>
<span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.$info[0]->data_ano.'	</span>
</td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td>
<b>Color Ext.</b>
</td><td>
<span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.$info[0]->data_color.'</span>
</td><td>
<b>No. MOTOR</b>
</td><td>
<span style="text-decoration:none;background-color:#ffcc99;color:black;margin-left:5px;"> '.$info[0]->data_no_motor.'</span>
</td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td width="13%" ><b>No. Serie</b></td>
<td width="22%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->data_vin.'</span></td>
<td width="10%"><b>Nuevo</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datco_snuevo.'</span></td>
<td width="15%"><b>Seminuevo</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datco_nuevox.'</span></td>
<td width="10%"><b>Demo</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datco_nuevo.'</span></td>
</tr>
</table>
<br><br>
<b>Documentos Recibidos:</b>
<br><br>
<table width="100%">
<tr>
<td width="20%" ><b>Fact. Original</b></td>
<td width="5%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_facoriginal.'</span></td>
<td width="15%"><b>Copia Fact</b></td>
<td width="5%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_coipafact.'</span></td>
<td width="15%"><b>Carta Fact.</b></td>
<td width="5%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_cartafact.'</span></td>
<td width="25%"><b>Fact. Tramite placas</b></td>
<td width="5%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_facplacas.'</span></td>
</tr>
</table>
<br><br>
<b>Manuales e Instructivos:</b>
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Manual de Servico de Garantia</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_manualgarantia.'</span></td>
<td width="40%"><b>Manual de Usuario y Sist. SRS</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_manual_deusuario.'</span></td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Manual de Asistencia Honda en el camino</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_manualdeasistemcia.'</span></td>
<td width="40%"><b>Contrato de Adhesion Profeco</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_profeco.'</span></td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Poliza de Seguro Automotriz</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_poliza.'</span></td>
<td width="40%"><b>Manual de Seguro Automotriz</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_msautomotriz.'</span></td>
</tr>
</table>
<br><br>
<b>Llaves y Herramientas:</b>
<br><br>
Llaves del vehiculo:
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Llave Principal</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_llaveprincipal.'</span></td>
<td width="40%"><b>Llave de Valet</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_llavevalet.'</span></td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td width="60%" ><b>Llaves con control remoto y apertura de puertas</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_llacontrolrepuerta.'</span></td>
<td width="20%"><b>Cod. Radio</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_codradio.'</span></td>
</tr>
</table>
<br><br>
Accesorios de Auxilio Vial :
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Extintor</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_extintor.'</span></td>
<td width="40%"><b>Llanta de refacción</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_llantarefa.'</span></td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Cables de Corriente</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_cablecorr.'</span></td>
<td width="40%"><b>Gato Hidraulico y su Herramienta</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_gatohydraulico.'</span></td>
</tr>
</table>
<br><br>
<table width="100%">
<tr>
<td width="40%" ><b>Triangulos de Seguridad</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_triangulodeseguridad.'</span></td>
<td width="40%"><b>Paquete de Herramientas</b></td>
<td width="10%"><span style="text-decoration:none;background-color:#ffcc99;color:black;">'.$info[0]->datdr_paqherra.'</span></td>
</tr>
</table>
<br><br>
<b>Observaciones:</b>
<br>





  
    <p style="text-align:justify; font-size:0.8em">
		Asi mismo, hago (hacemos) constar que estoy (estamos) de acuardo con los datos								
asentados en los documentos recibidos como son: Facturas, Polizas de Seguro,								
Garantias, y Manuales y que he(mos) revisado toda la informacion contenida en estos 								
documentos sin encontrar error alguno.								
Hago (hacemos) constar tambien que estoy (estamos) informando (s) y de acuerdo con 								
los terminos de la garantia del vehiculo y sus accesorios y que estoy (estamos) recibiendo								
dicho vehiculo a mi (nuestra) entera satisfaccion, tanto en su funcionamiento mecanico como el								
estado fisico del exterior e interior del mismo, por lo que desde este momento, asumo								
(asumimos) toda la responsabilidad inherente a la propiedad del mismo.								
Ademas, he(mos) sido informado (s) que cualquier desperfecto en los sistemas de audio								
y de entretenimiento (video) de dicha unidad, originados por el uso indebido de formatos								
electronicos no especificados como Discos Compactos o DVD s regrabados, de los								
mencionados Piratas o de baja calidad, pueden en su caso generar la perdida de								
garantia de los componentes mencionados (Caja de discos y sistema de entretenimiento).
	</p>
   <p style="text-align:center">
   <b>Recibi(mos)</b>
   <br><br><br>
   <b>'.$cliente.'</b>
   
   </p>
   
   ';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('Hoja_de_entrega.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		

		
		
		
		
		
		
		
		
		function viewcotizarpdfhuser($idcot,$idlcot){

$versiones= $this->Contactomodel->getListaVersiones($idcot);
$asesor=$this->Contactomodel->getAsesorT($_SESSION['id']);
$cotizacion= $this->Contactomodel->getCotizacion($idcot);
$lista= $this->Contactomodel->getListaLC($idlcot);

if($cotizacion[0]->cot_IDficha==9){$vi='L';}	
else{$vi='';}	
    // create new PDF document
	 $pdf = new TCPDF($vi, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1);
$pdf->AddPage(); 
// set cell padding
$pdf->setCellPaddings(2, 2, 2, 2);

setlocale(LC_MONETARY, 'en_US');

$enganche=$versiones[0]->ver_precio * $versiones[0]->ver_por_enganche / 100;
$suma1=$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia+$versiones[0]->ver_extencion+$versiones[0]->ver_seguro;
$total1=$suma1-$versiones[0]->ver_bono;

$enganche2=$versiones[1]->ver_precio * $versiones[1]->ver_por_enganche / 100;
$suma2=$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia+$versiones[1]->ver_extencion+$versiones[1]->ver_seguro;
$total2=$suma2-$versiones[1]->ver_bono;

$enganche3=$versiones[2]->ver_precio * $versiones[2]->ver_por_enganche / 100;
$suma3=$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia+$versiones[2]->ver_extencion+$versiones[2]->ver_seguro;
$total3=$suma3-$versiones[2]->ver_bono;

if($versiones[0]->ver_bono==0 && $versiones[1]->ver_bono==0 && $versiones[2]->ver_bono==0){
$bonotable='';}
else{
$bonotable='<tr><td align="center"><b>BONO</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[0]->ver_bono).'</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[1]->ver_bono).'</b></td>
<td align="right"><b>'.money_format('%(#10n',$versiones[2]->ver_bono).'</b></td>
</tr>';		
	}

$num = preg_replace('/[^0-9]/', '', $asesor[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);



$html = '

<img src="'.base_url().'downloads/cotizador/'.$cotizacion[0]->fmo_pdf.'">
<table style="font-size:.8em" width="100%">
<tr>
<td width="80%">
'.$lista[0]->lsp_nombre.' '.$lista[0]->lsp_apellido.'
</td>
<td width="20%" align="right">Fecha:'.date('d-m-Y').'</td>
</tr>
</table>
<p style="font-size:.8em">
Gracias por su interés hacia el '.$cotizacion[0]->fmo_nombre.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que
tenemos a su disposición.
</p>

<table  style="font-size:.8em; " >
<tr>
<td width="75%">
<table border="1" cellpadding="2">
<tr align="center">
<td><b>Version</b></td>
<td><b>'.$versiones[0]->ver_nombre.'</b></td>
<td><b>'.$versiones[1]->ver_nombre.'</b></td>
<td><b>'.$versiones[2]->ver_nombre.'</b></td>
</tr>
<tr >
<td align="left"> Precio del Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_precio).'</td>
</tr>
<tr align="center">
<td align="left"><b> FINANCIERA</b></td>
<td>'.$versiones[0]->ver_financiera.'</td>
<td>'.$versiones[1]->ver_financiera.'</td>
<td>'.$versiones[2]->ver_financiera.'</td>
</tr>
<tr >
<td><b> ENGANCHE</b></td>
<td >
<table  cellpadding="2">
<tr>
<td align="right"> % '.$versiones[0]->ver_por_enganche.'</td>
</tr><tr>
<td  align="right">'.money_format('%(#10n',$enganche).'</td></tr></table></td>
<td>
<table cellpadding="2" ><tr>
<td align="right"> % '.$versiones[1]->ver_por_enganche.'</td>
</tr><tr>
<td  align="right">'.money_format('%(#10n',$enganche2).'</td></tr></table></td>
<td >
<table cellpadding="2" ><tr>
<td align="right"> % '.$versiones[2]->ver_por_enganche.'</td>
</tr><tr>
<td  align="right">'.money_format('%(#10n',$enganche3).'</td></tr></table></td>
</tr>
<tr >
<td > Seguro Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_auto).'</td>
</tr>
<tr>
<td> Seguro Vida</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_vida).'</td>
</tr>
<tr >
<td> Com.Apertura</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_apertura).'</td>
</tr>
<tr >
<td> Placas y Tenencia</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td> Inversión Inicial</td>
<td align="right">'.money_format('%(#10n',$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td> Extensión Garantía</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_extencion).'</td>
</tr>
<tr >
<td> Seguro R.C USA</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro).'</td>
</tr>
'.$bonotable.'
<tr >
<td> Total</td>
<td align="right">'.money_format('%(#10n',$total1).'</td>
<td align="right">'.money_format('%(#10n',$total2).'</td>
<td align="right">'.money_format('%(#10n',$total3).'</td>
</tr>
<tr  bgcolor="#CCCCCC">
<td> Mensualidades de:</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_mensualidad).'</td>
</tr>

<tr  >
<td><b> PLAZO</b></td>
<td align="center">'.$versiones[0]->ver_plazo.'</td>
<td align="center">'.$versiones[1]->ver_plazo.'</td>
<td align="center">'.$versiones[2]->ver_plazo.'</td>
</tr>
</table>
</td>
<td width="25%">
<table>
<tr><td>Notas</td></tr>
<tr><td>
<table border="1" cellpadding="3" ><tr>
<td height="143px;" style="padding-left:10px"> '.$cotizacion[0]->cot_nota.'
</td></tr></table>
</td></tr>
</table>
</td>
</tr>
</table>
<br><br>
<table ><tr>
<td width="45%">
<table border="1" cellpadding="2" >
<tr>
<td align="center" colspan="2" style="color:red; font-size:0.8em">Su auto tiene Garantía de fábrica por 3 años o 60"000
Km.</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#CCCCCC" style=" font-size:0.8em">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">12 MESES o 20 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex4).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">24 MESES o 40 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex5).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">36 MESES o 60 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex6).'</td>
</tr>
<tr bgcolor="#CCCCCC">
<td  align="center" style=" font-size:0.8em">SERVICIO Km.</td>
<td  align="center" style=" font-size:0.8em">PRECIO*</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">5"000</td>
<td  align="right" style=" font-size:0.8em">$    825.00</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">10"000</td>
<td  align="right" style=" font-size:0.8em">$    1,145.00</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 </td>
<td  align="right" style=" font-size:0.8em">$    1,670.00</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 (4WD)</td>
<td  align="right" style=" font-size:0.8em">$    2,310.00</td>
</tr>
</table>
<p align="center"   style=" font-size:0.8em">
*Precios con IVA incluido. Posibilidad de variar sin previo aviso</p>
</td>
<td width="25%">
<table align="right"  style=" font-size:0.8em" cellpadding="2">
<tr><td>Su Asesor de Ventas:</td></tr>
<tr><td>Mail:</td></tr>
<tr><td>Celular:</td></tr>
<tr><td>Radio Personal:</td></tr>
<tr><td>Radio Honda:</td></tr>
<tr><td>Teléfono Honda:</td></tr>
</table>

</td>
<td width="30%">
<table  cellpadding="2"align="left" border="1"  style=" font-size:0.8em">
<tr><td> '.$asesor[0]->hus_nombre.' '.$asesor[0]->hus_apellido.'</td></tr>
<tr><td> '.$asesor[0]->hus_correo.'</td></tr>
<tr><td> '.$celular.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_personal.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_honda.'</td></tr>
<tr><td> '.$asesor[0]->hus_telefono.'</td></tr>
</table>
<br>
<p align="right">
<a style="color:red; font-size:.8em" href="http://www.hondaoptima.com">hondaoptima.com</a><br>
<a style="color:blue; font-size:.8em" href="http://www.facebook.com/hondaoptima">www.facebook.com/hondaoptima</a></p>
</td>


</tr></table>

';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('downloads/cotizaciones/cotizacion'.$idcot.'.pdf', 'I');    

		}		
		
		
		
		
		
		function viewcotizarpdf($idcot){

$versiones= $this->Contactomodel->getListaVersiones($idcot);
$asesor=$this->Contactomodel->getAsesorT($_SESSION['id']);
$cotizacion= $this->Contactomodel->getCotizacion($idcot);

if($cotizacion[0]->cot_IDficha==9){$vi='L';}	
else{$vi='';}		
    // create new PDF document
	 $pdf = new TCPDF($vi, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 
setlocale(LC_MONETARY, 'en_US');

$enganche=$versiones[0]->ver_precio * $versiones[0]->ver_por_enganche / 100;
$suma1=$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia+$versiones[0]->ver_extencion+$versiones[0]->ver_seguro;
$total1=$suma1-$versiones[0]->ver_bono;

$enganche2=$versiones[1]->ver_precio * $versiones[1]->ver_por_enganche / 100;
$suma2=$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia+$versiones[1]->ver_extencion+$versiones[1]->ver_seguro;
$total2=$suma2-$versiones[1]->ver_bono;

$enganche3=$versiones[2]->ver_precio * $versiones[2]->ver_por_enganche / 100;
$suma3=$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia+$versiones[2]->ver_extencion+$versiones[2]->ver_seguro;
$total3=$suma3-$versiones[2]->ver_bono;

if($versiones[0]->ver_bono==0 && $versiones[1]->ver_bono==0 && $versiones[2]->ver_bono==0){
$bonotable='';}
else{
$bonotable='<tr><td align="center"><b>BONO</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[0]->ver_bono).'</b></td>
<td align="right"><b> '.money_format('%(#10n',$versiones[1]->ver_bono).'</b></td>
<td align="right"><b>'.money_format('%(#10n',$versiones[2]->ver_bono).'</b></td>
</tr>';		
	}

$num = preg_replace('/[^0-9]/', '', $asesor[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);



$html = '
<img src="'.base_url().'downloads/cotizador/'.$cotizacion[0]->fmo_pdf.'">
<table style="font-size:.8em" width="100%">
<tr>
<td width="80%">
'.$cotizacion[0]->con_titulo.' '.$cotizacion[0]->con_nombre.' '.$cotizacion[0]->con_apellido.'
</td>
<td width="20%" align="right">Fecha:'.date('d-m-Y').'</td>
</tr>
</table>
<p style="font-size:.8em">
Gracias por su interés hacia el '.$cotizacion[0]->fmo_nombre.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que
tenemos a su disposición.
</p>

<table style="font-size:.8em">
<tr>
<td width="70%">
<table border="1" >
<tr align="center">
<td><b>Version</b></td>
<td><b>'.$versiones[0]->ver_nombre.'</b></td>
<td><b>'.$versiones[1]->ver_nombre.'</b></td>
<td><b>'.$versiones[2]->ver_nombre.'</b></td>
</tr>
<tr >
<td align="center">Precio del Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_precio).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_precio).'</td>
</tr>
<tr align="center">
<td><b>FINANCIERA</b></td>
<td>'.$versiones[0]->ver_financiera.'</td>
<td>'.$versiones[1]->ver_financiera.'</td>
<td>'.$versiones[2]->ver_financiera.'</td>
</tr>
<tr >
<td><b>ENGANCHE</b></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[0]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche).'</td></tr></table></td>
<td>
<table border="1"><tr>
<td width="30%"> % '.$versiones[1]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche2).'</td></tr></table></td>
<td >
<table border="1"><tr>
<td width="30%"> % '.$versiones[2]->ver_por_enganche.'</td>
<td width="70%" align="right">'.money_format('%(#10n',$enganche3).'</td></tr></table></td>
</tr>
<tr >
<td>Seguro Auto</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_auto).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_auto).'</td>
</tr>
<tr>
<td>Seguro Vida</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro_vida).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro_vida).'</td>
</tr>
<tr >
<td>Com.Apertura</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_apertura).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_apertura).'</td>
</tr>
<tr >
<td>Placas y Tenencia</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Inversión Inicial</td>
<td align="right">'.money_format('%(#10n',$enganche+$versiones[0]->ver_seguro_auto+$versiones[0]->ver_seguro_vida+$versiones[0]->ver_apertura+$versiones[0]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche2+$versiones[1]->ver_seguro_auto+$versiones[1]->ver_seguro_vida+$versiones[1]->ver_apertura+$versiones[1]->ver_placas_tenecia).'</td>
<td align="right">'.money_format('%(#10n',$enganche3+$versiones[2]->ver_seguro_auto+$versiones[2]->ver_seguro_vida+$versiones[2]->ver_apertura+$versiones[2]->ver_placas_tenecia).'</td>
</tr>
<tr >
<td>Extensión Garantía</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_extencion).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_extencion).'</td>
</tr>
<tr >
<td>Seguro R.C USA</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_seguro).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_seguro).'</td>
</tr>
'.$bonotable.'
<tr >
<td>Total</td>
<td align="right">'.money_format('%(#10n',$total1).'</td>
<td align="right">'.money_format('%(#10n',$total2).'</td>
<td align="right">'.money_format('%(#10n',$total3).'</td>
</tr>
<tr  bgcolor="#CCCCCC">
<td>Mensualidades de:</td>
<td align="right">'.money_format('%(#10n',$versiones[0]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[1]->ver_mensualidad).'</td>
<td align="right">'.money_format('%(#10n',$versiones[2]->ver_mensualidad).'</td>
</tr>

<tr  >
<td><b>PLAZO</b></td>
<td align="center">'.$versiones[0]->ver_plazo.'</td>
<td align="center">'.$versiones[1]->ver_plazo.'</td>
<td align="center">'.$versiones[2]->ver_plazo.'</td>
</tr>
</table>
</td>
<td width="30%">
<table>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td>Notas</td></tr>
<tr><td>
<table border="1" ><tr>
<td height="143px;">
'.$cotizacion[0]->cot_nota.'
</td></tr></table>
</td></tr>
</table>
</td>
</tr>
</table>
<br><br>
<table ><tr>
<td width="45%">
<table border="1" >
<tr>
<td align="center" colspan="2" style="color:red; font-size:0.8em">Su auto tiene Garantía de fábrica por 3 años o 60"000
Km.</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#CCCCCC" style=" font-size:0.8em">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>

<tr>
<td  align="left" style=" font-size:0.8em">12 MESES o 20 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex4).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">24 MESES o 40 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex5).'</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">36 MESES o 60 MIL KM</td>
<td  align="right" style=" font-size:0.8em">'.money_format('%(#10n',$cotizacion[0]->cot_precioex6).'</td>
</tr>
<tr bgcolor="#CCCCCC">
<td  align="center" style=" font-size:0.8em">SERVICIO Km.</td>
<td  align="center" style=" font-size:0.8em">PRECIO*</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">5"000</td>
<td  align="right" style=" font-size:0.8em">$    825.00</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">10"000</td>
<td  align="right" style=" font-size:0.8em">$    1,145.00</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 </td>
<td  align="right" style=" font-size:0.8em">$    1,670.00</td>
</tr>
<tr>
<td  align="left" style=" font-size:0.8em">20"000 (4WD)</td>
<td  align="right" style=" font-size:0.8em">$    2,310.00</td>
</tr>
</table>
<p align="center"   style=" font-size:0.8em">
*Precios con IVA incluido. Posibilidad de variar sin previo aviso</p>
</td>
<td width="25%">
<table align="right"  style=" font-size:0.8em">
<tr><td>Su Asesor de Ventas:</td></tr>
<tr><td>Mail:</td></tr>
<tr><td>Celular:</td></tr>
<tr><td>Radio Personal:</td></tr>
<tr><td>Radio Honda:</td></tr>
<tr><td>Teléfono Honda:</td></tr>
</table>

</td>
<td width="30%">
<table align="left" border="1"  style=" font-size:0.8em">
<tr><td> '.$asesor[0]->hus_nombre.' '.$asesor[0]->hus_apellido.'</td></tr>
<tr><td> '.$asesor[0]->hus_correo.'</td></tr>
<tr><td> '.$celular.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_personal.'</td></tr>
<tr><td> '.$asesor[0]->hus_radio_honda.'</td></tr>
<tr><td> '.$asesor[0]->hus_telefono.'</td></tr>
</table>
<br>
<p align="right">
<a style="color:red; font-size:.8em" href="http://www.hondaoptima.com">hondaoptima.com</a><br>
<a style="color:blue; font-size:.8em" href="http://www.facebook.com/hondaoptima">www.facebook.com/hondaoptima</a></p>
</td>


</tr></table>

';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('downloads/cotizaciones/cotizacion'.$idcot.'.pdf', 'I');    

		}		
		
		
		function viewordendecomprapdf($ido){
setlocale(LC_MONETARY, 'en_US');			
$OD= $this->Contactomodel->getOrdendeCompraPdf($ido);
$PAGOS= $this->Contactomodel->getPagosOrden($ido);
if($OD[0]->hus_ciudad=='Tijuana'){$address='Av. Padre Kino No. 4300 Zona Rio, C.P. 22320';
//$fsemi='Hector Revilla Ortiz';
$ffactura='Ivone de los Palos';
$fconta='Monica Negrete';
$fgerente='LIC. Fernando Rivera';
$fcredito='Isela Ruiz';
}
if($OD[0]->hus_ciudad=='Mexicali'){$address='Calz. Justo SIerra 1233, Fracc. Los Pinos, C.P. 21230 ';
$ffactura='Alicia Leon';
$fconta='Karla Monges';
$fgerente='Ing. Oscar Vega';
$fcredito='Martha Ofelia Tobón Muñoz';
}
if($OD[0]->hus_ciudad=='Ensenada'){$address='Av. Balboa 146 Esq. Lopez Mateos, Fracc. Granados, C.P. 22840 ';
$ffactura='Adriana Karina Martinez Galvez';
$fconta='Monica Negrete';
$fgerente='Lic. Alberto Manuel Fierro';
$fcredito='Erika Granados Amador';

}
//facturara a
if($OD[0]->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.' '.$OD[0]->datc_apellidopaterno.' '.$OD[0]->datc_apellidomaterno;}
else{$facturaNombre=$OD[0]->datc_moral;}
//mensualidad
if(empty($OD[0]->dats_mensualidad)){$mensualidad=0;}else{$mensualidad=$OD[0]->dats_mensualidad;}
$mensualidad;
//taza
if(empty($OD[0]->dats_taza)){$taza=0;}else{$taza=$OD[0]->dats_taza;}
// nuevo demo semi
if($OD[0]->datco_snuevo=='si'){$nuevo='SI';}else{$nuevo='NO';}
if($OD[0]->datco_nuevo=='si'){$demo='SI';}else{$demo='NO';}
if($OD[0]->datco_nuevox=='si'){$seminuevo='SI';}else{$seminuevo='NO';}
//lista pagos
$listapagos='';
$totalpagos=0;
foreach ($PAGOS as $tpagos){
$listapagos.='<tr>
<td><table border="1"><tr><td>'.$tpagos->nop_tipo.'</td></tr></table> '.$tpagos->nop_fecha.'</td>
<td>'.$tpagos->nop_folio.'</td>
<td>'.money_format('%(#10n',$tpagos->nop_cantidad).'</td>
</tr>';
$totalpagos+=$tpagos->nop_cantidad;
}

list($dan,$dms,$ddi)=explode('-',$OD[0]->dor_fecha);
 if($dan=='2013'){$once=0.11; $iva=1.11;}else{$once=0.16;$iva=1.16;}
//precio venta
  if($OD[0]->datco_nuevox=='si'){$prev=$OD[0]->dats_valorfactura-$OD[0]->avi_ivaonce; $toprev=round($prev,2); }
  //nuevo
  if($OD[0]->datco_snuevo=='si'){  $prev=$OD[0]->dats_valorfactura /$iva;$toprev=round($prev,2); }
 //demo
  if($OD[0]->datco_nuevo=='si'){ $prev=$OD[0]->dats_valorfactura / $iva;$toprev=round($prev,2); }  
	
	
//iva
 if($OD[0]->datco_snuevo=='si'){ $valorty=$toprev * $once;  $miva=round($valorty,2);  } 
   //demo
 if($OD[0]->datco_nuevo=='si'){ $valorty=$toprev * $once; $miva=round($valorty,2);} 
 //seminuevo
 if($OD[0]->datco_nuevox=='si'){ $miva=$OD[0]->avi_ivaonce;}	
//titulo iva
if($OD[0]->datco_nuevox=='si'){$titiva='IVA:';}else{$titiva='IVA:';}

//total enganche
$totalenganche=$OD[0]->dats_monto + $OD[0]->avi_gastosadicionales + $OD[0]->avi_recomprausado;
//banco
$banco=$OD[0]->dats_valorfactura-$OD[0]->dats_monto;

//tipo de financiado
if($OD[0]->dats_financiadoanual=='si'){$fanual='SI';}else{$fanual='NO';}
if($OD[0]->dats_financiadomultianual=='si'){$fmulti='SI';}else{$fmulti='NO';}
if($OD[0]->dats_contadoanual=='si'){$fcontado='SI';}else{$fcontado='NO';}
// semestral
if($OD[0]->dats_contadomultianual=='si'){$cmulti='SI';}else{$cmulti='NO';}
if($OD[0]->dats_contadosemestral=='si'){$csem='SI';}else{$csem='NO';}
//seguro
if($OD[0]->dats_tipocaic=='si'){$caic='SI';}else{$caic='NO';}
if($OD[0]->dats_tiipoaig=='si'){$aig='SI';}else{$aig='NO';}
if($OD[0]->dats_tipognp=='si'){$gnp='SI';}else{$gnp='NO';}
// create new PDF document
$pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Cotizacion');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 


$html = '
<table width="100%" style="font-size:0.8em"><tr>
<td width="50%" align="left">
<img width="150px" height="50px" src="http://hondaoptima.com/ventas/img/logo.png">
</td>
<td width="50%" align="right">
<table>
<tr><td><b>OPTIMA AUTOMOTRIZ S.A. DE C.V.</b></td></tr>
<tr><td>'.$address.'</td></tr>
<tr><td>'.$OD[0]->hus_ciudad.' B.C. RFC:OAU-990325-G88</td></tr>
<tr><td>TEL:900-9000</td></tr>
</table>
</td>
</tr></table>
<table width="100%" style="font-size:0.7em; font-weight:bold;"><tr>
<td width="21%" align="center">
<table border="1"><tr><td>NO.DE CLIENTE</td></tr><tr>
<td bgcolor="#ffcc99">0</td></tr>
</table>
</td>
<td width="1%" align="center"></td>
<td width="25%" align="center">
<table border="1"><tr><td>FECHA DE ENTREGA</td></tr><tr>
<td bgcolor="#ffcc99">'.$OD[0]->dat_fecha_entrega.'</td></tr>
</table>
<table><tr><td style="font-size:0.8em; color:blue" align="center">Fecha</td></tr></table>
</td>
<td width="1%" align="center"></td>
<td width="21%" align="center">
<table border="1"><tr><td>HORA DE ENTREGA</td></tr><tr>
<td bgcolor="#ffcc99">'.$OD[0]->dat_hora.'</td></tr></table>
<table><tr><td style="font-size:0.8em; color:blue" align="center">Hora</td></tr></table>
</td>
<td width="1%" align="center"></td>
<td width="30%" align="center">
<table border="1"><tr><td>FECHA DE FACTURACION</td></tr><tr>
<td bgcolor="#ffcc99">'.$OD[0]->dat_fecha_facturacion.'</td></tr></table>
<table><tr><td style="font-size:0.8em; color:blue" align="center">Año-Mes-Dia</td></tr></table>
</td>
</tr></table>
<br><br>
<table width="100%" style="font-size:0.7em; font-weight:bold;"><tr>
<td width="23%">FACTURAR A:</td>
<td width="77%" bgcolor="#ffcc99" align="center"> '.$facturaNombre.'</td>
</tr>
<tr>
<td></td>
<td style="font-size:0.8em; color:blue" align="center">Nombre Completo y/o Razon Social</td>
</tr>
</table>
<table width="100%" style="font-size:0.7em; font-weight:bold;"><tr>
<td width="23%">DOMICILIO:</td>
<td width="77%" bgcolor="#ffcc99" align="center">'.$OD[0]->dfa_calle.' '.$OD[0]->dfa_colonia.' '.$OD[0]->dfa_codigopostal.'</td>
</tr>
<tr>
<td></td>
<td style="font-size:0.8em; color:blue" align="center">Calle, No. Int. / Ext, Colonia o Fraccionamiento y Codigo Postal</td>
</tr>
</table>

<table width="100%" style="font-size:0.7em; font-weight:bold;"><tr>
<td width="21%" align="left">
CIUDAD / EDO:
</td>
<td width="2%" align="center"></td>
<td width="25%" bgcolor="#ffcc99" align="center">
'.$OD[0]->dfa_ciudad.' '.$OD[0]->dfa_estado.'
</td>
<td width="1%" align="center"></td>
<td width="21%" align="center">
R.F.C. / CURP
</td>
<td width="1%" align="center"></td>
<td width="29%" bgcolor="#ffcc99" align="center">
'.$OD[0]->datc_rfc.'
</td>
</tr></table>
<table ><tr><td></td></tr></table>
<table width="100%" style="font-size:0.7em; font-weight:bold;"><tr>
<td width="21%" align="left">
TEL / CEL / RADIO:
</td>
<td width="2%" align="center"></td>
<td width="25%" bgcolor="#ffcc99" align="center">
'.$OD[0]->datc_tcasa.' '.$OD[0]->datc_tcelular.'
</td>
<td width="1%" align="center"></td>
<td width="21%" align="center">
EMAIL:
</td>
<td width="1%" align="center"></td>
<td style="font-size:0.9em" width="29%" bgcolor="#ffcc99" align="center">
'.$OD[0]->datc_email.'
</td>
</tr></table>
<table><tr><td></td></tr></table>
<table width="100%" align="center" style="font-size:.7em; font-weight:bold" border="1">
<tr>
<td width="30%">MARCA, MODELO, VERSION Y AÑO</td>
<td width="25%">COLOR EXT</td>
<td width="20%">NO. MOTOR</td>
<td width="25%">NO. SERIE(VIN)</td>
</tr>
<tr bgcolor="#ffcc99">
<td width="30%">'.$OD[0]->data_marca.' '.$OD[0]->data_modelo.' '.$OD[0]->data_version.' '.$OD[0]->data_ano.'</td>
<td width="25%">'.$OD[0]->data_color.'</td>
<td width="20%">'.$OD[0]->data_no_motor.'</td>
<td width="25%">'.$OD[0]->data_vin.'</td>
</tr>
</table>
<table><tr><td></td></tr></table>
<table width="100%" align="center" style="font-size:.7em; font-weight:bold" border="1">
<tr>
<td width="15%" style="font-size:0.7em">TIPO DE OPERACION</td>
<td width="15%">FINANCIERA</td>
<td width="25%">TIPO MONEDA</td>
<td width="20%">MENSUALIDAD</td>
<td width="13%">PLAZO</td>
<td width="12%">TASA</td>
</tr>
<tr bgcolor="#ffcc99">
<td width="15%" style="font-size:0.7em">'.$OD[0]->dats_compra.'</td>
<td width="15%">'.$OD[0]->dats_financiera.'</td>
<td width="25%">'.$OD[0]->dats_tipomoneda.'</td>
<td width="20%">'.money_format('%(#10n',$mensualidad).'</td>
<td width="13%">'.$OD[0]->dats_plazo.'</td>
<td width="12%">'.$taza.'</td>
</tr>
</table>
<table width="100%" align="center" style="font-size:.6em; color:blue" >
<tr>
<td width="15%" style="font-size:0.7em">Financiamiento / Contado</td>
<td width="15%">Bancos</td>
<td width="25%">M.N. / Dls.</td>
<td width="20%">Cantidad a Pagar</td>
<td width="13%">Mensualidades</td>
<td width="12%">Porcentaje</td>
</tr>
</table>
<table><tr><td></td></tr></table>
<table width="100%" align="center" style="font-size:.7em; font-weight:bold" >
<tr>
<td width="49%">
<table border="1"><tr><td>CONDICIONES GENERALES</td></tr></table>
</td>
<td width="2%"></td>
<td width="49%">
<table border="1"><tr><td>DESGLOSE DE LA OPERACION</td></tr></table>
</td>
</tr>
</table>

<table width="100%"  style="font-size:.7em; font-weight:bold" >
<tr>
<td width="49%">
<table width="100%"><tr>
<td width="40%">
<table width="100%"><tr>
<td width="20%" bgcolor="#ffcc99" align="center" ><table border="1"><tr><td>'.$nuevo.'</td></tr></table></td>
<td width="80%"> HONDA NUEVO</td>
</tr>
<tr>
<td width="20%" bgcolor="#ffcc99" align="center" ><table border="1"><tr><td>'.$demo.'</td></tr></table></td>
<td> HONDA DEMO</td>
</tr>
<tr>
<td width="20%" bgcolor="#ffcc99" align="center" ><table border="1"><tr><td>'.$seminuevo.'</td></tr></table></td>
<td> SEMINUEVO</td></tr>
</table>
</td>
<td width="60%" align="center">
<table border="1" ><tr><td colspan="2">Garantias:(auto, ext., certificado)</td></tr>
<tr><td>'.$OD[0]->datco_garantiaanos.'</td><td>'.$OD[0]->datco_garacntiakm.'</td></tr>
</table>
<table style="font-weight:none; color:blue;"><tr><td>Años</td><td>Kilometraje</td></tr></table>
</td>
</tr>
<tr>
<td colspan="2" align="center">
<table ><tr><td>DATOS DE LA RECOMPRA</td></tr></table>
</td></tr>

<tr>
<td colspan="2" align="center">
<table border="1" ><tr>
<td>MARCA</td>
<td>MODELO</td>
<td>VERSION</td>
</tr></table>
<table border="1" bgcolor="#ffcc99" ><tr>
<td>'.$OD[0]->datco_marca.'</td>
<td>'.$OD[0]->datco_modelo.'</td>
<td>'.$OD[0]->datco_version.'</td>
</tr></table>
</td></tr>
</table>
<table border="1" width="100%" height="3px" ><tr>
<td></td>
</tr></table>
<table border="1" align="center" ><tr>
<td>AÑO</td>
<td>NO. DE COMPRA</td>
<td>NO. DE SERIE</td>
</tr></table>
<table border="1" bgcolor="#ffcc99" align="center" ><tr>
<td>'.$OD[0]->datco_ano.'</td>
<td>'.$OD[0]->datco_nocompra.'</td>
<td>'.$OD[0]->datco_noserie.'</td>
</tr></table>
<table  align="center" ><tr>
<td></td>
<td>Aut./ Manual</td>
<td>No. de Factura</td>
</tr></table>
<table border="1" align="center" width="100%" height="3px" ><tr>
<td>DETALLE DE PAGOS (Anexar Soportes)</td>
</tr></table>
<table border="1" align="center" ><tr>
<td>T.PAGO/FECHA</td>
<td>FOLIO / DTCO</td>
<td>IMPORTE</td>
</tr>
'.$listapagos.'
<tr>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td colspan="2">TOTAL PAGOS</td>
<td>'.money_format('%(#10n',$totalpagos).'</td>
</tr>
</table>
</td>
<td width="2%"></td>
<td width="49%">
<table border="1"><tr><td>
<table>
<tr>
<td width="70%"> Precio de Venta Auto</td>
<td width="30%" align="right">'.money_format('%(#10n',$toprev).'</td>
</tr>
<tr>
<td width="70%"> Accesorios</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_acessorios).'</td>
</tr>
<tr>
<td width="70%"> '.$titiva.'</td>
<td width="30%" align="right">'.money_format('%(#10n',$miva).'</td>
</tr>
</table>
<table border="1">
<tr>
<td width="70%"> VALOR FACTURA</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->dats_valorfactura).'</td>
</tr>
</table>
<table>
<tr>
<td width="70%"> Gastos Adicionales*</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_gastosadicionales).'</td>
</tr>
<tr>
<td width="70%"> Recompra Usado</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_recomprausado).'</td>
</tr>
<tr>
<td width="70%"> Enganche o Complemento</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->dats_monto).'</td>
</tr>
</table>
<table border="1">
<tr>
<td width="70%"> TOTAL ENGANCHE / INV INICIAL</td>
<td width="30%" align="right">'.money_format('%(#10n',$totalenganche).'</td>
</tr>
<tr>
<td width="70%"> Saldo a Pagar por el Banco</td>
<td width="30%" align="right">'.money_format('%(#10n',$banco).'</td>
</tr>
<tr>
<td width="70%"> TOTAL DE LA OPERACION</td>
<td width="30%" align="right">'.money_format('%(#10n',$totalenganche+$banco).'</td>
</tr>
<tr>
<td width="100%" colspan="2" align="right">*Detalle de Gastos Adicionales</td>
</tr>
</table>
<table>
<tr>
<td width="70%"> Seguro Auto Nacional</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_seguronac).'</td>
</tr>
<tr>
<td width="70%"> Seguro Resp. Civil(USA)</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_seguroes).'</td>
</tr>
<tr>
<td width="70%"> Seguro de Vida</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_segurovida).'</td>
</tr>
<tr>
<td width="70%"> Comisión por Apertura</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_comisionapertura).'</td>
</tr>
<tr>
<td width="70%"> Placas y Tenecia</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_placastenecia).'</td>
</tr>
<tr>
<td width="70%"> Trámites</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_tramites).'</td>
</tr>
<tr>
<td width="70%"> Interes</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_intereses).'</td>
</tr>
<tr>
<td width="70%"> Carrocería</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_carroceria).'</td>
</tr>
<tr>
<td width="70%"> Accesorios(Anexar Pedido)</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_accesoriosanexarpedido).'</td>
</tr>
<tr>
<td width="70%"> Seguro de Desempleo</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_segurodesempleo).'</td>
</tr>
<tr>
<td width="70%"> Ext./ Certif. de Garantia</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_certifigarantia).'</td>
</tr>
</table>
<table border="1">
<tr>
<td width="70%"> TOTAL GASTOS ADICIONALES</td>
<td width="30%" align="right">'.money_format('%(#10n',$OD[0]->avi_gastosadicionales).'</td>
</tr>
</table>

</td></tr></table>
</td>
</tr>
</table>
<table><tr><td></td></tr></table>
<table border="1" style="font-size:0.7em" ><tr><td>
<table width="100%"><tr>
<td width="20%">
<table><tr><td></td></tr>
<tr><td>TIPO DE SEGURO NAC.</td></tr>
<tr><td></td></tr>
</table>
</td>
<td width="27%">
<table width="100%">
<tr>
<td width="10%" align="center"><table border="1"><tr><td>'.$fanual.'</td></tr></table></td>
<td> Financiado Anual</td>
</tr>
<tr>
<td width="10%" align="center"><table border="1"><tr><td>'.$fmulti.'</td></tr></table></td>
<td style="font-size:.8em"> Financiado Multianual</td>
</tr>
<tr>
<td width="10%" align="center"><table border="1"><tr><td>'.$fcontado.'</td></tr></table></td>
<td> Contado Anual</td>
</tr>
</table>
</td>
<td width="53%">
<table width="100%">
<tr>
<td width="7%" align="center"><table border="1"><tr><td>'.$cmulti.'</td></tr></table></td>
<td width="45%"> Contado Multianual</td>
<td width="26%"></td>
<td width="15%">CAIC</td>
<td width="8%" align="center"><table border="1"><tr><td>'.$caic.'</td></tr></table></td>
</tr>
<tr>
<td width="7%" align="center"><table border="1"><tr><td></td></tr></table></td>
<td width="45%"></td>
<td width="26%">TIPO SEG. USA</td>
<td width="15%">AIG</td>
<td width="8%" align="center"><table border="1"><tr><td>'.$aig.'</td></tr></table></td>
</tr>
<tr>
<td width="7%" align="center"><table border="1"><tr><td>'.$csem.'</td></tr></table></td>
<td width="45%"> Contado Semestral</td>
<td width="26%"></td>
<td width="15%">GNP</td>
<td width="8%" align="center"><table border="1"><tr><td>'.$gnp.'</td></tr></table></td>
</tr>
</table>
</td>
</tr></table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table width="100%" style="font-size:0.7em"><tr>
<td width="70%">
<table border="1"><tr>
<td>
<table><tr>
<td align="center"><b>SALDO A FAVOR O FALTANTE</b></td></tr>
<tr>
<td align="center">'.money_format('%(#10n',$totalpagos - $totalenganche - $banco).'</td>
</tr>
</table>
</td>
<td><b>OBSERVACIONES:</b>
'.$OD[0]->avi_observaciones.'
</td>
</tr></table>
</td>
<td width="30%">
<table border="1" style="font-size:0.8em">
<tr>
<td>DESGLOZAR IVA</td>
<td>TIPO DE CAMBIO</td>
</tr>
<tr>
<td>(SI) / NO</td>
<td></td>
</tr>
<tr>
<td>Encerrar en circulo</td>
<td>De Honda Optima</td>
</tr>
</table>
</td>
</tr></table>
<br><br><br>
<table width="100%" align="center">
<tr style="font-size:.8em">
<td width="33%">'.$facturaNombre.'</td>
<td width="33%">'.$OD[0]->hus_nombre.' '.$OD[0]->hus_apellido.'</td>
<td width="33%">'.$fcredito.'</td>
</tr><tr style="font-size:.7em">
<td width="33%">Cliente</td>
<td width="33%">Asesor de Ventas</td>
<td width="33%">Credito / Seguros</td>
</tr><tr>
<td width="33%"></td>
<td width="33%"></td>
<td width="33%"></td>
</tr>
<tr>
<td width="33%"></td>
<td width="33%"></td>
<td width="33%"></td>
</tr>
<tr style="font-size:.8em">
<td width="33%">'.$ffactura.'</td>
<td width="33%">'.$fgerente.'</td>
<td width="33%">'.$fconta.'</td>
</tr><tr style="font-size:.7em">
<td width="33%">Facturación</td>
<td width="33%">Gerente de Ventas</td>
<td width="33%">Contabilidad</td>
</tr></table>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('ordendecompra.pdf', 'I');    

		}		
		

function codigoRadioForm(){
	$this->load->view('contacto/codigoRadioForm');
	}
		function codigoRadio(){

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Codigo de radio');
    $pdf->SetTitle('Codigo de radio');
    $pdf->SetSubject('Codigo de radio');
    $pdf->SetKeywords('Codigo de radio, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           Codigo de radio', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
    // Set some content to print
	$codigo=$_GET['codigo'];
    $html = '
	<p >
<h2>CODIGO DE RADIO</h2>
</p>
<br><br>

<table  width="100%" cellpadding="5" style="border:1px solid #333333; margin-left:400px;">
<tr>
<td width="34%"  ></td>
<td width="27%" style="text-align:right">
<img 	src="'.base_url().'img/logosmall.png">
</td>
</tr>
<tr>
<td width="34%"  ><b>Anti-Theft Radio</b></td>
<td width="27%">Identification Card</td>
</tr>
<tr>
<td width="34%" ><b>Radio anti-robo</b></td>
<td width="27%">Tarjeta de identificacion</td>
</tr>
<tr>
<td width="34%" ><b>Radio antivol</b></td>
<td width="27%">Carte d" identification</td>
</tr>
<tr>
<td width="34%" ><b>Codigo AntiRoubo</b></td>
<td width="27%">Cartâo de identificacâo do</td>
</tr>
<tr>
<td width="34%" >Code Number/ Serial Number</td>
<td width="27%"></td>
</tr>
<tr>
<td width="34%" >Número clave/Número de serie</td>
<td width="27%"><b>'.$codigo.'</b></td>
</tr>
<tr>
<td width="34%" >Numéro de code/Numero de serie</td>
<td width="27%"></td>
</tr>
<tr>
<td width="34%" >Número do codigo/Numero de serie</td>
<td width="27%"></td>
</tr>
</table>
<br><br>
<p>
En caso de no tener codigo de radio imprimirlo y pegarlo 
<br>
en una tarjeta de presentacion y ponerlo en el estuche.
</p>
	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('Codigo_radio.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
function ctaAutorizacionForm($id){
	$data['info']=$this->Contactomodel->getAllDataOrdendeCompra($id);
	$data['id']=$id;
	$this->load->view('contacto/ctaAutorizacionForm', $data);
	}


		function ctaAutorizacion($id){

$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
//echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Cuenta Autorizacion Cliente');
    $pdf->SetTitle('Cuenta Autorizacion Cliente');
    $pdf->SetSubject('Cuenta Autorizacion Cliente');
    $pdf->SetKeywords('Cuenta Autorizacion Cliente, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           Cuenta Autorizacion Cliente', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 	//consultar info
	$info=$this->Contactomodel->getAllDataOrdendeCompra($id);
	if(empty($info[0]->datc_moral_nombre)){$nombre=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
	else{
		$nombre=$info[0]->datc_moral_nombre;
		}
		
//get
$gnombre=$_GET['nombre'];
$gparenteso=$_GET['parentesco'];		
		
    // Set some content to print
setlocale(LC_TIME, 'spanish');
    $html = '
	<p style=" text-align:center">
<h2>'.$info[0]->hus_ciudad.', B.C. a '.date('d').' de '.$meses[date('n')-1].' del '.date('Y').'</h2>
</p>
<br><br>
<p>
Atención,<br>
A Quien Corresponda:<br>
Optima Automotriz S.A. de C.V
</p>
<br><br><br><br>
<table width="100%"><tr><td>
Por medio del presente autorizo a el, la Sr(a):
</td>
<td>
'.$gnombre.'
<p style="font-size:9px">Nombre de quien recibe la unidad nueva, demo o seminueva</p>
</td>
</tr></table>
<p>
<table><tr><td>
     Para que en mi nombre firme los  documentos de entrega de la unidad adquirida
	 </td></tr>
<tr ><td >	 
	 debido a que no puedo ir personalmente, el parentesco que nos une es:'.$gparenteso.'
</td>
</tr>
<tr height="50px;">
<td align="right">

<p style="font-size:9px">amigo, padre, madre, hermano</p>
</td>
</tr>
</table>
<br><br>
<b>Los datos son:</b>
<table width="100%">
<tr>
<td width="40%" >Marca, version y año :</td><td>'.$info[0]->data_marca.' '.$info[0]->data_modelo.' '.$info[0]->data_version.'</td>
</tr>
<tr>
<td width="40%" >Color :</td><td> '.$info[0]->data_color.' </td>
</tr>
<tr>
<td width="40%" >Serie :</td><td> '.$info[0]->data_vin.'</td>
</tr>
<tr>
<td width="40%" >Motor:</td><td> '.$info[0]->data_no_motor.'</td>
</tr>
</table>
<br><br>

<p>
     Agradezco las facilidades otorgadas a mi representante y sin mas por el momento <br>
	 me despido quedando a sus ordenes para cualquier duda o aclaración al respecto.
</p>
<br><br>
Atentamente,
<br><br><br><br><br><br>
<p align="center">
<b>'.$nombre.'</b><br>
Cliente<br>
<b>Tel:</b>664-2874104
</p>
</p>

	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('Cta_Autorizacion.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}		
		
		
		
		
		function vinpdf($id){
			
			

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('VIN');
    $pdf->SetTitle('VIN');
    $pdf->SetSubject('VIN');
    $pdf->SetKeywords('VIN, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           VIN', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal')); 
   
   $info=$this->Contactomodel->getAllDataOrdendeCompra($id);
	if(empty($info[0]->datc_moral_nombre)){$nombre=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
	else{
		$nombre=$info[0]->datc_moral_nombre;
		}   
 
    // Set some content to print
setlocale(LC_TIME, 'spanish');
    $html = '

<br><br><br><br>
<table width="55%" cellpadding="5" align="center" 	 style="border:1px solid #333333">
<tr>
<td colspan="2" >
<h2>Asistencia Honda</h2>
</td></tr>
<tr>
<td width="50%">
'.$info[0]->data_vin.'
</td>
<td width="50%">
<b># SERIE / VIN:</b>
</td>
</tr>


<tr>
<td width="50%">
'.$info[0]->data_marca.'
</td>
<td width="50%">
<b>MARCA:</b>
</td>
</tr>


<tr>
<td width="50%">
'.$info[0]->data_ano.'
</td>
<td width="50%">
<b>AÑO:</b>
</td>
</tr>

<tr>
<td width="50%">
'.$info[0]->data_modelo.'
</td>
<td width="50%">
<b>MODELO:</b>
</td>
</tr>


<tr>
<td width="50%">
'.$info[0]->data_color.'
</td>
<td width="50%">
<b>COLOR:</b>
</td>
</tr>
</table>
<p>No. de serie para pegar en la Tarjeta de Asistencia Honda.</p>
<br><br><br>
<table width="55%" cellpadding="5" align="center" 	 style="border:1px solid #333333">
<tr>
<td >
<h2>Manual de Consulta</h2>
</td>
</tr>
<tr>
<td >
<h1>'.$info[0]->data_vin.'</h1>
</td>
</tr>
</table>
<br><br>
<table width="55%" cellpadding="5" align="center" 	 style="border:1px solid #333333">
<tr>
<td >
<h2>Manual de Consulta</h2>
</td>
</tr>
<tr>
<td >
<h1>'.$info[0]->data_vin.'</h1>
</td>
</tr>
</table>
<p>No. de serie para pegar en manual de Asistencia Honda</p>
	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('vin.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}	
		
		
		
		
		
				function reportedanos($id){

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Reporte Daños');
    $pdf->SetTitle('Reporte Daños');
    $pdf->SetSubject('Reporte Daños');
    $pdf->SetKeywords('Reporte Daños, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           Reporte de Daños y/o Faltantes', '               Nuevos, Seminuevos, y Demos', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
   
    $info=$this->Contactomodel->getAllDataOrdendeCompra($id);
	if(empty($info[0]->datc_moral_nombre)){$nombre=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
	else{
		$nombre=$info[0]->datc_moral_nombre;
		}      
		
		
		$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
//echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
 
    // Set some content to print
setlocale(LC_TIME, 'spanish');
    $html = '
<table width="100%" >
<tr>
<td >
<b>Unidad para:</b>
</td>
<td >
Venta
</td>
<td  bgcolor="#CCCC66">
</td>
<td >
Exhibicion
</td>
<td  bgcolor="#CCCC66">
</td>
<td >
Traslado
</td>
<td  bgcolor="#CCCC66">
</td>
</tr>
</table>
<br><br>
<table width="100%" >
<tr>
<td >
<b>Unidad nueva:</b>
</td>
<td bgcolor="#CC9966">'.$info[0]->datco_snuevo.'
</td>
<td >
Seminuevo
</td>
<td bgcolor="#CC9966">
'.$info[0]->datco_nuevox.'
</td>
<td >
Demo
</td>
<td bgcolor="#CC9966">
'.$info[0]->datco_nuevo.'
</td>
</tr>
</table>
<br><br>
<p align="center">DATOS AUTO NUEVO</p>
<br><br>
<table width="100%" >
<tr>
<td width="10%" bgcolor="#CC9966">
'.$info[0]->data_marca.'
</td>
<td width="15%" bgcolor="#CC9966">
'.$info[0]->data_modelo.'
</td>
<td width="10%" bgcolor="#CC9966">
'.$info[0]->data_version.'
</td>
<td width="5%" bgcolor="#CC9966">
'.$info[0]->data_ano.'
</td>
<td width="20%" bgcolor="#CC9966">
'.$info[0]->data_color.'
</td>
<td width="20%" bgcolor="#CC9966">
'.$info[0]->data_no_motor.'
</td>
<td width="20%" bgcolor="#CC9966">
'.$info[0]->data_vin.'
</td>
</tr>
<tr>
<td width="10%" align="center">
Marca
</td>
<td width="15%" align="center">
Modelo
</td>
<td width="10%" align="center">
Version
</td>
<td width="5%" align="center">
Año
</td>
<td width="20%" align="center">
Color Ext.
</td>
<td width="20%" align="center">
No.Motor
</td>
<td width="20%" align="center">
Serie Completa
</td>
</tr>
</table>
<br><br>
<table width="100%" >
<tr>
<td  bgcolor="#CCCC66" width="10%">
</td>
<td width="22%">
DAÑOS EXTERIORES
</td>
<td  bgcolor="#CCCC66" width="10%">
</td>
<td width="22%">
DAÑOS INTERIORES
</td>
<td  bgcolor="#CCCC66" width="16%">
</td>
<td width="16%">
FALTANTE
</td>
</tr>
</table>
<br><br>
Favor de describir el daño y/o faltante y el lugar exacto del mismo:
<br><br><br><br><br><br><br>
<table width="100%" >
<tr>
<td  width="30%">
Fecha de arrivo a la Agencia:
</td>
<td width="4%" >
</td>
<td width="8%" bgcolor="#CCCC66">
</td>
<td width="8%" bgcolor="#CCCC66">
</td>
<td width="8%" bgcolor="#CCCC66">
</td>
<td width="38%" bgcolor="#CC9966">'.date('d').' de '.$meses[date('n')-1].' del '.date('Y').'</td>
</tr>
<tr>
<td  width="30%" align="center">
</td>
<td width="4%" align="center">
</td>
<td width="8%" align="center">Dia
</td>
<td width="8%" align="center">Mes
</td>
<td width="8%" align="center">Año
</td>
<td width="38%" align="center">Fecha del Reporte DD/MM/AA</td>
</tr>
</table>
<br><br><br>
<table>
<tr>
<td  width="50%" >
<b>Reporte Generado</b>
</td>
<td width="50%" align="center">
<b>Personal que recibe el reporte</b>
</td>
</tr>
</table>
<br><br><br>
<table width="100%">
<tr>
<td  width="40%" >
'.$info[0]->hus_nombre.' '.$info[0]->hus_apellido.'
</td>
<td width="35%" align="center">
Guillermo Ortiz / Ivon Velazquez
</td>
<td width="25%" align="center">
Francisco Echegollen
</td>
</tr>
</table>
<br><br><br>
OBSERVACIONES:
<br><br><br><br>
<img src="'.base_url().'img/danos.jpg" align="center">

	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('ReporteDanos.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}	
		
		
		
				function pedidoaccesorios($id){

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Pedido Accesorios');
    $pdf->SetTitle('Pedido Accesorios');
    $pdf->SetSubject('Pedido Accesorios');
    $pdf->SetKeywords('Pedido Accesorios, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '  SOLICITUD DE INSTALACION DE ACCESORIOS GENUINOS HONDA', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '10'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
    $info=$this->Contactomodel->getAllDataOrdendeCompra($id);
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	
	if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}
 
//echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
	
    // Set some content to print
setlocale(LC_TIME, 'spanish');
    $html = '
<p align="center">
<h1>Favor de Instalar los accesorios relacionados en la siguiente Unidad:</h1>
</p>	
<table width="100%" >
<tr>
<td width="20%" >
<b>Unidad: </b>
</td>
<td width="10%"></td>
<td align="center"  bgcolor="#CCCC66" width="20%">
'.$info[0]->datco_snuevo.'
</td>
<td width="5%"></td>
<td align="center"  bgcolor="#CCCC66" width="20%">
'.$info[0]->datco_nuevox.'
</td>
<td width="5%"></td>
<td align="center"  bgcolor="#CCCC66" width="20%">
'.$info[0]->datco_nuevo.'
</td>
</tr>
<tr>
<td >

</td>
<td width="10%"></td>
<td  width="20%" align="center">
Nuevo
</td>
<td width="5%"></td>
<td  width="20%" align="center">
Demo
</td>
<td width="5%"></td>
<td   width="20%" align="center">
Seminuevo
</td>
</tr>
</table>
<br><br>

<table width="100%" >
<tr>
<td  width="20%" bgcolor="#CCCC66">
'.$info[0]->data_modelo.'
</td>
<td width="10%"></td>
<td  bgcolor="#CCCC66" width="20%">
'.$info[0]->data_color.'
</td>
<td width="5%"></td>
<td  bgcolor="#CCCC66" width="20%">
'.$info[0]->data_vin.'
</td>
<td width="5%"></td>
<td  bgcolor="#CCCC66" width="20%">
'.$info[0]->data_ano.'
</td>
</tr>
<tr>
<td align="center">
Modelo
</td>
<td width="10%"></td>
<td  width="20%" align="center">
Color
</td>
<td width="5%"></td>
<td  width="20%" align="center">
No.Serie
</td>
<td width="5%"></td>
<td   width="20%" align="center">
Año
</td>
</tr>
</table>
<br><br>
<table width="100%" >
<tr>
<td  width="50%" >
Incluir en la Factura del Auto (Pago con Aviso):
</td>
<td width="5%" bgcolor="#CCCC66">
'.$info[0]->dats_accfactura.'
</td>
<td   width="20%">
Fecha Pedido:
</td>
<td width="25%" bgcolor="#CCCC66">
'.date('d').' de '.$meses[date('n')-1].' del '.date('Y').'
</td>
</tr>
</table>
<br><br>
<table width="100%" >
<tr>
<td  width="50%" >
Facturar por Mostrador (Refacciones):
</td>
<td width="5%" bgcolor="#CCCC66"></td>
<td   width="20%">
Asesor de Ventas:
</td>
<td width="25%" bgcolor="#CCCC66">'.$info[0]->hus_nombre.' '.$info[0]->hus_apellido.'</td>
</tr>
</table>
<br><br>
<p align="center"><h2>DATOS FACTURACION</h2></p>
<br><br>
<table width="100%" >
<tr>
<td  width="50%" >
Nombre o Razon Social:
</td>
<td width="50%" bgcolor="#CCCC66">'.$cliente.'</td>
</tr>
</table>
<br><br>
<table width="100%" >
<tr>
<td  width="15%" >Domicilio:</td>
<td width="25%" bgcolor="#CCCC66">'.$info[0]->dfa_calle.' '.$info[0]->dfa_colonia.'</td>
<td  width="15%" >Ciudad:</td>
<td width="20%" bgcolor="#CCCC66">'.$info[0]->dfa_ciudad.'</td>
<td  width="15%" >Estado:</td>
<td width="10%" bgcolor="#CCCC66">'.$info[0]->dfa_estado.'</td>
</tr>
</table>
<br><br>
<table width="100%" >
<tr>
<td  width="20%" >R.F.C:</td>
<td width="30%" bgcolor="#CCCC66">'.$info[0]->datc_rfc.'</td>
<td  width="20%" >Fecha de entrega:</td>
<td width="30%" bgcolor="#CCCC66">'.$info[0]->dat_fecha_entrega.'</td>
</tr>
</table>
<br><br>
<table width="100%" border="1" align="center" >
<tr>
<td  width="5%" >#</td>
<td width="5%" >QTY</td>
<td  width="10%" >#PARTE</td>
<td width="30%">DESCRIPCION</td>
<td width="20%">PRECIO</td>
<td width="30%">IMPORTE</td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr><tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="5%" >1</td>
<td width="5%" ></td>
<td  width="10%" ></td>
<td width="30%"></td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="50%" colspan="5" >
***Precios sujetos a cambio sin previo aviso
</td>
<td width="20%"><b>Total</b></td>
<td width="30%"></td>
</tr>
<tr>
<td  width="50%" colspan="5" >
***Precios incluyen el I.V.A.
</td>
<td width="20%"></td>
<td width="30%"></td>
</tr>
</table>
<br><br>

	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('PedidoAccesorios.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		
		
						function facturacontado($id){
	$info=$this->Contactomodel->getAllDataOrdendeCompra($id);						
							
if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}							

if($_SESSION['ciudad']=='Tijuana'){$direccion='Av. Padre Kino 4300, Zona Rio, C.P. 22320'; $gerente='Lic. Fernando Rivera';}
if($_SESSION['ciudad']=='Mexicali'){$direccion='Calz. Justo Sierra 1233, Los Pinos, C.P. 21230'; $gerente='Ing. Oscar Vega';}
if($_SESSION['ciudad']=='Ensenada'){$direccion='Av. Balboa 146 esq. López Mateos Fracc. Granados, C.P. 22840';$gerente='Lic. Alberto Fierro';}

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Factura contado');
    $pdf->SetTitle('Factura contado');
    $pdf->SetSubject('Factura contado');
    $pdf->SetKeywords('Factura contado, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '  CARTA FACTURA', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '10'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
	
	 $meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>"Noviembre",'12'=>"Diciembre");
 $nms=date('m');
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
    // Set some content to print
setlocale(LC_TIME, 'spanish');
    $html = '
<table width="100%" >
<tr>
<td width="50%" align="right" >
<b>'.$_SESSION['ciudad'].' B.C., a: </b>
</td>
<td width="50%" align="left">'.date('d').' '.$meses[$nms].' del '.date('Y').'</td>
</tr>
</table>
<br><br>
<b>A quien Corresponda:</b>
<br><br>
<p>
Por este conducto hacemos constar que con esta fecha,  hemos vendido un automovil bajo<br><br>
las siguientes características y condiciones:
</p>
<br><br>
<b style="text-decoration:underline">Datos del comprador</b>
<br><br>
<table width="100%" >
<tr>
<td width="30%"  >
Nombre / Razon Social:
</td>
<td width="70%" >'.$cliente.'</td>
</tr>
<tr>
<td width="30%" >
Domicilio de Entrega:
</td>
<td width="70%" >'.$direccion.'</td>
</tr>
</table>
<br><br>
<b>Datos del Automóvil</b>
<br><br>
<table width="100%" >
<tr>
<td width="30%"  >
Marca y Modelo:
</td>
<td width="70%" >'.$info[0]->data_marca.' '.$info[0]->data_modelo.'</td>
</tr>
<tr>
<td width="30%" >
Tipo de Carrocería :
</td>
<td width="70%" >'.$info[0]->data_carroceria.'</td>
</tr>
<tr>
<td width="30%" >
Año :
</td>
<td width="70%" >'.$info[0]->data_ano.'</td>
</tr>
<tr>
<td width="30%" >
Tipo de Transmisión y Motor :
</td>
<td width="70%" >'.$info[0]->data_motor.'</td>
</tr>
<tr>
<td width="30%" >
Capacidad de Pasajeros :
</td>
<td width="70%" >'.$info[0]->data_capacidad.'</td>
</tr>
<tr>
<td width="30%" >
Color Exterior / Interior :
</td>
<td width="70%" >'.$info[0]->data_color.'</td>
</tr>
<tr>
<td width="30%" >
Numero de Serie :
</td>
<td width="70%" >'.$info[0]->data_vin.'</td>
</tr>
<tr>
<td width="30%" >
Numero de Motor :
</td>
<td width="70%" >'.$info[0]->data_no_motor.'</td>
</tr>
<tr>
<td width="30%" >
Pedimento # :
</td>
<td width="70%" ></td>
</tr>
</table>
<br><br>
<p>
<b>Optima Automotriz</b>,  agradecera las  facilidades prestadas  al comprador para los tramites
<br><br>
que apliquen para la circulacion legal de esta unidad.
</p>
<br><br>
<b>Datos de la Factura</b>
<br><br>
<table align="center"><tr>
<td width="30%">Folio</td>
<td width="30%">Fecha</td>
<td width="40%">Emitida por</td>
</tr>
<tr>
<td width="30%" style=" background-color:#ffcc99;">'.$info[0]->dats_factura.'</td>
<td width="30%" style=" background-color:#ffcc99;">'.$info[0]->dat_fecha_facturacion.'</td>
<td width="40%" style="background-color:#ffcc99;">Optima Automotriz, S.A. de C. V.</td>
</tr>
</table>
<br><br>
<p align="center"><b>Atentamente,</b>
<br><br><br>
'.$gerente.'<br><br>
Gte. de Ventas
<br><br><br><br><br>
<br><br><br><br><br>
<a href="www.hondaoptima.com" style="color:red" >www.hondaoptima.com</a>
</p><br><br>
<p align="center">
Esta Carta Factura solo es valida si se anexa copia fotostatica de factura con firma y sello en original de
<br><br>
Optima Automotriz, S.A. de C.V.</p>	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('FacturaContado.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}			
		
		
		
							function facturacredito($id){
								
									$info=$this->Contactomodel->getAllDataOrdendeCompra($id);						
							
if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}							

if($_SESSION['ciudad']=='Tijuana'){$direccion='Av. Padre Kino 4300, Zona Rio, C.P. 22320'; $gerente='Lic. Fernando Rivera';}
if($_SESSION['ciudad']=='Mexicali'){$direccion='Calz. Justo Sierra 1233, Los Pinos, C.P. 21230'; $gerente='Ing. Oscar Vega';}
if($_SESSION['ciudad']=='Ensenada'){$direccion='Av. Balboa 146 esq. López Mateos Fracc. Granados, C.P. 22840';$gerente='Lic. Alberto Fierro';}

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Factura credito');
    $pdf->SetTitle('Factura credito');
    $pdf->SetSubject('Factura credito');
    $pdf->SetKeywords('Factura credito, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '  CARTA FACTURA', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '10'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
 $meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>"Noviembre",'12'=>"Diciembre");
 $nms=date('m');
    // Set some content to print
setlocale(LC_TIME, 'spanish');
    $html = '
<table width="100%" >
<tr>
<td width="50%" align="right" >
<b>'.$info[0]->hus_ciudad.' B.C., a: </b>
</td>
<td width="50%" align="left">'.date('d').' '.$meses[$nms].' '.date('Y').'</td>
</tr>
</table>
<br><br>
<b>A quien Corresponda:</b>
<br><br>
<p>
Por este conducto hacemos constar que con esta fecha,  hemos vendido un automovil bajo
<br><br>
las siguientes características y condiciones:
</p>
<br><br>
<b style="text-decoration:underline">Datos del comprador</b>
<br><br>
<table width="100%" >
<tr>
<td width="30%"  >
Nombre / Razon Social:
</td>
<td width="70%" >'.$cliente.'</td>
</tr>
<tr>
<td width="30%" >
Domicilio de Entrega:
</td>
<td width="70%" >'.$direccion.'</td>
</tr>
</table>
<br><br>
<b>Datos del Automóvil</b>
<br><br>
<table width="100%" >
<tr>
<td width="30%"  >
Marca y Modelo:
</td>
<td width="70%" >'.$info[0]->data_marca.' '.$info[0]->data_modelo.'</td>
</tr>
<tr>
<td width="30%" >
Tipo de Carrocería :
</td>
<td width="70%" >'.$info[0]->data_carroceria.'</td>
</tr>
<tr>
<td width="30%" >
Año :
</td>
<td width="70%" >'.$info[0]->data_ano.'</td>
</tr>
<tr>
<td width="30%" >
Tipo de Transmisión y Motor :
</td>
<td width="70%" >'.$info[0]->data_motor.'</td>
</tr>
<tr>
<td width="30%" >
Capacidad de Pasajeros :
</td>
<td width="70%" >'.$info[0]->data_capacidad.'</td>
</tr>
<tr>
<td width="30%" >
Color Exterior / Interior :
</td>
<td width="70%" >'.$info[0]->data_color.'</td>
</tr>
<tr>
<td width="30%" >
Numero de Serie :
</td>
<td width="70%" >'.$info[0]->data_vin.'</td>
</tr>
<tr>
<td width="30%" >
Numero de Motor :
</td>
<td width="70%" >'.$info[0]->data_no_motor.'</td>
</tr>
<tr>
<td width="30%" >
Pedimento # :
</td>
<td width="70%" >'.$info[0]->dat_pedimento.'</td>
</tr>
</table>
<br><br>
<p>
La factura original que ampara esta operacion, fue endosada por el comprador 
<br><br>
y entregada en garantia a favor del banco que otorga el financiamiento para lar
<br><br>
compra, por lo que Optima Automotriz, agradecera las facilidades prestadas al
<br><br>
comprador para los tramites que apliquen para la circulacion legal de esta unidad.
</p>
<br><br>
<b>Datos de la Factura</b>
<br><br>
<table align="center"><tr>
<td width="30%">Folio</td>
<td width="30%">Fecha</td>
<td width="40%">Emitida por</td>
</tr>
<tr>
<td width="30%" style=" background-color:#ffcc99;">'.$info[0]->dats_factura.'</td>
<td width="30%" style=" background-color:#ffcc99;">'.$info[0]->dat_fecha_facturacion.'</td>
<td width="40%" style="background-color:#ffcc99;">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
</tr>
</table>
<br><br>
<p align="center"><b>Atentamente,</b>
<br><br><br>
'.$gerente.'<br><br>
Gte. de Ventas
<br><br><br><br><br>
<a href="www.hondaoptima.com" style="color:red" >www.hondaoptima.com</a>
</p><br><br>
<p align="center">
Esta Carta Factura solo es valida si se anexa copia fotostatica de factura con firma y sello en original de
<br><br>
Optima Automotriz, S.A. de C.V.</p>	';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('FacturaCredito.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}	
		
		
		
		
		function adhesionNuevos(){

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Contrato Adhesion Nuevos');
    $pdf->SetSubject('Contrato Adhesion Nuevos');
    $pdf->SetKeywords('Contrato Adhesion Nuevos, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           Contrato Adhesion Nuevos', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
    // Set some content to print
    $html = '
<table width="100%" style="font-size:8px">
<tr>
<td width="20%" ><b>Denominación</b></td>
<td width="47%">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td width="25%"><b>Factura-Folio</b></td>
<td width="3%"><span style="text-decoration:none;background-color:#ffcc99;color:black;"> 0</span></td>
</tr>

<tr>
<td width="20%" ><b>Domicilio:</b></td>
<td width="47%">AV. PADRE KINO No. 4300 ZONA RIO C.P. 22320</td>
<td width="25%"></td>
<td width="3%"></td>
</tr>

<tr>
<td width="20%" ><b>R.F.C.</b></td>
<td width="47%">OAU-990325-G88</td>
<td width="25%"><b>Fecha:</b></td>
<td width="3%"></td>
</tr>

<tr>
<td width="20%" ><b>Horario de atencion</b></td>
<td width="47%">De 8:00am a 8:00pm de Lunes a Viernes</td>
<td colspan="2" width="28%" style="text-decoration:none;background-color:#ffcc99;color:black;">Sabado 31 de Marzo del 2012</td>
</tr>

<tr>
<td width="20%" ><b>Fax</b></td>
<td width="47%">(664) 683-4481</td>
<td width="25%"><b>Localidad:</b></td>
<td width="3%"></td>
</tr>

<tr>
<td width="20%"><b>E. Mail</b></td>
<td width="47%">auxventas@hondaoptima.com</td>
<td width="25%">Tijuana, Baja California</td>
<td  width="3%"></td>
</tr>

</table>
<br>
<b>DATOS DEL CLIENTE( CONSUMIDOR ): </b>
<br>
<table width="100%" style="font-size:8px">
<tr>
<td width="15%" ><b>Nombre</b></td>
<td   width="35%">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td width="15%"><b>R.F.C</b></td>
<td  width="30%"></td>
</tr>
<tr>
<td width="15%" ><b>Domicilio</b></td>
<td  width="35%">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td width="15%"><b>Telefono</b></td>
<td  width="30%"></td>
</tr>
<tr>
<td width="15%" ><b>Ciudad</b></td>
<td width="35%">Tijuana. <b>Delegacion o Municipio</b></td>
<td width="15%"></td>
<td width="30%"><b>Estado:</b></td>
</tr>
</table>
<br><br>
<b>CARACTERÍSTICAS DEL VEHÍCULO:</b>
<br>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>Marca: </b></td>
<td   width="15%"></td>
<td width="35%"><b>Número Ident. Vehicular:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Sub-marca o Tipo: </b></td>
<td  width="15%"></td>
<td width="35%"><b>Capacidad</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Version o Modelo:</b></td>
<td width="15%"></td>
<td width="35%"></td>
<td width="25%"><b></b></td>
</tr>
<tr>
<td width="25%" ><b>Color:</b></td>
<td width="15%"></td>
<td width="35%"><b>Fecha de entrega : </b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Año-modelo:</b></td>
<td width="15%"></td>
<td width="35%"><b>Lugar de entrega del Vehículo: </b></td>
<td width="25%">HONDA OPTIMA AUTOMOTRIZ </td>
</tr>
</table>
<br><br>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>MONTO DE LA OPERACIÓN: </b></td>
<td   width="15%"></td>
<td width="35%"><b>FORMA DE PAGO:</b></td>
<td  width="25%">CONTADO</td>
</tr>
<tr>
<td width="25%" ><b>Precio del vehículo: </b></td>
<td  width="15%"></td>
<td width="35%"><b>Contado:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Equipo y accesorios adicionales (Ver análisis)</b></td>
<td width="15%"></td>
<td width="35%"><b>Enganche unidad o Enganche toma auto:</b></td>
<td width="25%"><b></b></td>
</tr>
<tr>
<td width="25%" ><b>Otros cargos:</b></td>
<td width="15%"></td>
<td width="35%"><b>Crédito concedido por el Banco o Agencia:</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Parcial:</b></td>
<td width="15%"></td>
<td width="35%"><b>Tasa mensual de intereses ordinarios %</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Impuesto al Valor Agregado:</b></td>
<td width="15%"></td>
<td width="35%"><b>Tasa mensual de intereses moratorios %</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Sub-total:</b></td>
<td width="15%"></td>
<td width="35%"><b>Núm. de Pagarés:    0</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Intereses (incluye IVA)</b></td>
<td width="15%"></td>
<td width="35%"><b>Importe de Pagarés: $    0</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b></b></td>
<td width="15%"></td>
<td width="35%"><b>Vencimiento los días: </b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Monto de la operación:</b></td>
<td width="15%"></td>
<td width="35%"><b>Lugar de Pago:     HONDA OPTIMA AUTOMOTRIZ</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%"></td>
<td width="35%"><b>Monto de la operación:</b></td>
<td width="25%"></td>
</tr>
</table>
<br><br>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>EQUIPO Y ACCESORIOS ADICIONALES: </b></td>
<td   width="15%"></td>
<td width="35%"><b>DESCRIPCIÓN UNIDAD USADA:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td  width="15%">$</td>
<td width="35%">Número de Identificación Vehicular:</td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Marca:</td>
<td width="25%"><b></b></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Submarca:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Tipo o versión:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Color:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Año-modelo:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" >Total equipo y accesorios adicionales:</td>
<td width="15%"></td>
<td width="35%">Valor de la Unidad:</td>
<td width="25%"></td>
</tr>
</table>
<br>
<b>CONDICIONES DEL CONTRATO DE COMPRA-VENTA DE VEHÍCULO NUEVO</b>
<p style="font-size:8px">
1. En virtud de este contrato (*), el Distribuidor (Proveedor) vende al Cliente (Consumidor) el vehículo cuyas características se detallan en este documento.						
2. El vehículo cuenta con el equipo opcional y accesorios adicionales solicitados y autorizados por el Cliente.<br>						
3. El precio de la compraventa será cubierto en la forma y términos expresados en este contrato, incluyendo en su caso, los equipos y accesorios adicionales.<br>4. En caso de que el Cliente entregue un vehículo usado a cuenta del precio, entregará también la documentación correspondiente, segun relacion anexa, declarando de manera expresa que dicha documentacion es legitima. Los impuestos anteriores no pagados asi como sus accesorios y gastos de tramite de baja, seran por cuenta del cliente, asi mismo, el cliente manifiesta que el vehiculo esta libre de gravamen y no tiene probelma judicial y / o administrativo alguno, por lo que en este acto libera al distribuidor de adeudos o conflictos que por cualquier motivo pudiera presentar dicho vehiculo hasta la fecha de su entrega.<br>5. El distribuidor entrega junto con el vehiculo: (I) la carta-factura; (II) el manual del usuario; y la poliza de garantia otorgada por el fabricante y /o importador en la que						
se establece los terminos y condiciones para su cumplimiento, debidamente sellada y firmada. El distribuidor entregara al cliente la factura original dentro de un plazo de						
15 dias contados a partir de la fecha en la que el cliente hubiese liquidado el monto de la compra venta.<br>						
6. En caso de que dentro del periodo de garantia, el cliente acuda ante cualquier dustribuidor autorizado para solicitar la reparacion del vehiculo conforme a la garantia						
otorgada por el fabricante y/o importador, y el distribuidor autorizado no cuente con las refaciones necesarias para la reparacion del vehiculo en un plazo maximo de						
60 dias naturales contados a partir de la fecha en la que el cliente alla presenatdo el vehiculo para su reparacion, quien alla otorgado la garantia asumira ante el cliente						
los costos por el incumplimiento en los terminos establecidos en la garantia, en la NOM-160-SCFI-2003, y de acuerdo con las politicas y procedimientos de garantia						
convenidos entre el fabricante o el importador con el distribuidor.<br>						
7. Son causas de recisión del presente contrato: (I) Incumplimiento de entrega del vehículo por parte del distribuidor.-  El Cliente le notificará por escrito el incumplimiento de dicha obligación y el Distribuidor devolverá la cantidad que por cualquier concepto hubiese recibido del Cliente con motivo de esta compraventa, en un plazo no mayor de 5 días hábiles a partir de la fecha en que fue notificado dicho incumplimiento. (II) Incumplimiento de pago por parte del cliente.- en el evento que el vehiculo alla sido entregado al cliente, y este incumpla con su obligacin de pago, el Distribuidor podra ejercitar las acciones que en derecho proceda. En caso de que el vehiculo no haya sido entregado, y el cliente incumpla con su obligacion de pago, el distribuidor le notificará por escrito en forma feaciente su incumplimiento y devolvera al cliente la cantidad que por cualquier concepto hubiese recibido con motivo de esta compraventa, en un plazo no mayor de 5 dias habiles contados  a partir de la fecha de la notificacion del incumplimiento el distribuidor podra disponer del vehiculo a partir de la fecha de notificacion del incumplimiento, sin autorizacion judicial previa.<br>						
8. Las partes están de acuerdo en someterse a la competencia de la Procuraduría Federal del Consumidor en la vía administrativa para resolver cualquier controversia que se suscite sobre la interpretación o cumplimiento de los términos y condiciones del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-160-SCFI-2003 y cualquier otra disposición aplicable, sin perjuicio del derecho que tienen las partes de someterse a la jurisdiccion de los tribunales competentes del domicilio del Distribuidor, renunciando las partes expresamente a cualquier otra jurisdicción que pudiera corresponderles por razón de sus domicilios futuros.<br>						
9. El Cliente y el Distribuidor aceptan la realización de la presente compraventa, en los términos establecidos en este contrato, y sabedores de su alcance legal, lo   firman por duplicado.						
</p>

   
   </p>
<table align="center">
<tr>
<td><b>EL DISTRIBUIDOR</b></td>
<td><b>EL CLIENTE</b></td>
</tr>
<tr>
<td>OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td>RODOLFO RAMOS RUELAS</td>
</tr>
</table>
<p style="font-size:8px;">
(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número, de registro en tramite, el dia 19 de diciembre de 2008.<br>* LFPC.- Ley Federal de Proteccion al Consumidor
</p>
   
   ';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('ContratoNuevos.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		
		
		
		function adhesionSeminuevos($id){
			$info=$this->Contactomodel->getAllDataOrdendeCompra($id);

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Contrato Adhesion Seminuevos');
    $pdf->SetSubject('Contrato Adhesion Seminuevos');
    $pdf->SetKeywords('Contrato Adhesion Seminuevos, PDF, example, test, guide');   
 
    // set default header data
    $pdf->SetHeaderData('logo.png', '40', '           Contrato Adhesion Seminuevos', '', array(0,64,5), array(0,0,0));
    $pdf->setFooterData(array(0,64,0), array(0,0,0)); 
 
    // set header and footer fonts
	
    $pdf->setHeaderFont(Array('', '', '15'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
 
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
 
    // ---------------------------------------------------------    
 
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
 
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 9, '', true);   
 
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
 
    // set text shadow effect
   // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
 
    // Set some content to print
    $html = '
<table width="100%" style="font-size:8px">
<tr>
<td width="20%" ><b>Denominación</b></td>
<td width="47%">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td width="25%"><b>Factura-Folio</b></td>
<td width="3%"><span style="text-decoration:none;background-color:#ffcc99;color:black;"> 0</span></td>
</tr>

<tr>
<td width="20%" ><b>Domicilio:</b></td>
<td width="47%">AV. PADRE KINO No. 4300 ZONA RIO C.P. 22320</td>
<td width="25%"></td>
<td width="3%"></td>
</tr>

<tr>
<td width="20%" ><b>R.F.C.</b></td>
<td width="47%">OAU-990325-G88</td>
<td width="25%"><b>Fecha:</b></td>
<td width="3%"></td>
</tr>

<tr>
<td width="20%" ><b>Horario de atencion</b></td>
<td width="47%">De 8:00am a 8:00pm de Lunes a Viernes</td>
<td colspan="2" width="28%" style="text-decoration:none;background-color:#ffcc99;color:black;">Sabado 31 de Marzo del 2012</td>
</tr>

<tr>
<td width="20%" ><b>Fax</b></td>
<td width="47%">(664) 683-4481</td>
<td width="25%"><b>Localidad:</b></td>
<td width="3%"></td>
</tr>

<tr>
<td width="20%"><b>E. Mail</b></td>
<td width="47%">auxventas@hondaoptima.com</td>
<td width="25%">Tijuana, Baja California</td>
<td  width="3%"></td>
</tr>

</table>
<br>
<b>DATOS DEL CLIENTE( CONSUMIDOR ): </b>
<br>
<table width="100%" style="font-size:8px">
<tr>
<td width="15%" ><b>Nombre</b></td>
<td   width="35%">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td width="15%"><b>R.F.C</b></td>
<td  width="30%"></td>
</tr>
<tr>
<td width="15%" ><b>Domicilio</b></td>
<td  width="35%">OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td width="15%"><b>Telefono</b></td>
<td  width="30%"></td>
</tr>
<tr>
<td width="15%" ><b>Ciudad</b></td>
<td width="35%">Tijuana. <b>Delegacion o Municipio</b></td>
<td width="15%"></td>
<td width="30%"><b>Estado:</b></td>
</tr>
</table>
<br>
<b>CARACTERÍSTICAS DEL VEHÍCULO:</b>
<br>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>Marca: </b></td>
<td   width="15%"></td>
<td width="35%"><b>Número Ident. Vehicular:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Sub-marca o Tipo: </b></td>
<td  width="15%"></td>
<td width="35%"><b>Capacidad</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Version o Modelo:</b></td>
<td width="15%"></td>
<td width="35%"><b>Numero de placas:</b></td>
<td width="25%"><b></b></td>
</tr>
<tr>
<td width="25%" ><b>Color:</b></td>
<td width="15%"></td>
<td width="35%"><b>Fecha de entrega : </b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Año-modelo:</b></td>
<td width="15%"></td>
<td width="35%"><b>Lugar de entrega del Vehículo: </b></td>
<td width="25%">HONDA OPTIMA AUTOMOTRIZ </td>
</tr>
</table>
<br><br>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>MONTO DE LA OPERACIÓN: </b></td>
<td   width="15%"></td>
<td width="35%"><b>FORMA DE PAGO:</b></td>
<td  width="25%">CONTADO</td>
</tr>
<tr>
<td width="25%" ><b>Precio del vehículo: </b></td>
<td  width="15%"></td>
<td width="35%"><b>Contado:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Equipo y accesorios adicionales (Ver análisis)</b></td>
<td width="15%"></td>
<td width="35%"><b>Enganche unidad o Enganche toma auto:</b></td>
<td width="25%"><b></b></td>
</tr>
<tr>
<td width="25%" ><b>Otros cargos:</b></td>
<td width="15%"></td>
<td width="35%"><b>Crédito concedido por el Banco o Agencia:</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Parcial:</b></td>
<td width="15%"></td>
<td width="35%"><b>Tasa mensual de intereses ordinarios %</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Impuesto al Valor Agregado:</b></td>
<td width="15%"></td>
<td width="35%"><b>Tasa mensual de intereses moratorios %</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Sub-total:</b></td>
<td width="15%"></td>
<td width="35%"><b>Núm. de Pagarés:    0</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Intereses (incluye IVA)</b></td>
<td width="15%"></td>
<td width="35%"><b>Importe de Pagarés: $    0</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b></b></td>
<td width="15%"></td>
<td width="35%"><b>Vencimiento los días: </b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Monto de la operación:</b></td>
<td width="15%"></td>
<td width="35%"><b>Lugar de Pago:     HONDA OPTIMA AUTOMOTRIZ</b></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%"></td>
<td width="35%"><b>Monto de la operación:</b></td>
<td width="25%"></td>
</tr>
</table>
<p style="color:red; font-size:8px;">El Cliente tiene derecho a liquidar	
anticipadamente el credito con la 	
consiguiente reduccion de intereses	
del credito no causados.	
</p>
<br><br>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>EQUIPO Y ACCESORIOS ADICIONALES: </b></td>
<td   width="15%"></td>
<td width="35%"><b>DESCRIPCIÓN UNIDAD USADA:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td  width="15%">$</td>
<td width="35%">Número de Identificación Vehicular:</td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Marca:</td>
<td width="25%"><b></b></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Submarca:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Tipo o versión:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Color:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Año-modelo:</td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" >Total equipo y accesorios adicionales:</td>
<td width="15%"></td>
<td width="35%">Valor de la Unidad:</td>
<td width="25%"></td>
</tr>
</table>
<br>
<b>CONDICIONES DEL CONTRATO DE COMPRA-VENTA DE VEHÍCULO SEMINUEVO</b>
<p style="font-size:6px">
1.  En virtud de este contrato (*), el Distribuidor (Proveedor) como legitimo propietario vende al Cliente (Consumidor) el vehículo usado cuyas características se 						
    detallan en este documento, quien lo recibe despues de haber efectuado una revision de forma detallada.<br>						
2.  El vehículo usado cuenta con el equipo opcional y accesorios adicionales solicitados y autorizados por el Cliente, detallados en el presente contrato.<br> 						
3.  El precio de la compraventa será cubierto en la forma y términos expresados en este contrato, incluyendo, en su caso, los equipos y accesorios adicionales. En						
     caso de que la compraventa sea a credito concedido por la empresa, hasta en tanto el precio total del vehiculo y sus accesorios no hayan sido cubiertos en su 						
     totalidad, el Distribuidor conservara el dominio del vehiculo y la factura que solo sera entregada hasta concluir las obligaciones de pago por parte del Cliente.<br>						
4. El Cliente acepta que por tratarse de un vehiculo usado, lo adquiere en el estado de uso en el que se encuentra, el cual le fue facilitado para su revision de forma						
    detallada y cuenta con el siguiente equipo: Exteriores: Limpiadores (plumas), (  ) Unidades de luces, (  ) Antena, (  ) Espejos laterales, (  ) Cristales, (  ) Tapones de						
    ruedas, (  ) Molduras completas, (  ) Tapon de Gasolina, (  ) Claxon, (  ) Interiores: Instrumentos de Tablero, (  ) Calefaccion, (  ) Aire Acondicionado, (  ) Radio, (  )						
    Bocinas, (  ) Encendedor, (  ) Espejo retrovisor, (  ) Ceniceros, (  ) Cinturones de Seguridad, (  ) Tapetes, (  ) Manijas y/o controles interiores, (  ) Equipo adicional, (  )						
    Accesorios, (  ) Otros, (  )						
CONDICIONES.<br>						
5.  En caso de que el Cliente entregue un vehículo usado a cuenta del precio, entregará también la documentación correspondiente, consiste en: Factura, (  ) Tarjeta     de circulacion, (  ) Documentos oficiales que acrediten su legal estancia en el pais, (  ) Comprobante de pago de tenencias, (  ) Comprobante de verificacion según relación anexa, declarando de manera expresa que dicha documentación es legítima. Los impuestos anteriores no pagados así como sus accesorios y gastos de trámite de "baja", serán por cuenta del Cliente. Asimismo, el Cliente manifiesta que el vehículo está libre de gravamen y no tiene problema judicial y/o administrativo alguno, por lo que en este acto libera al Distribuidor de adeudos o conflictos que por cualquier motivo pudiera presentar  dicho vehículo hasta la fecha  de su entrega.<br>						
6. El vehiculo usado se vende, sin garantia (  ) con garantia por un plazo de_______, (Art. 77 de la LFPC no podra ser inferior a 60 dias) contados a partir de la entrega 						
del vehiculo usado, excluyendose la correspondiente a partes electricas y debera hacerse valida en el domicilio, telefonos y horarios de atencion señalados en el 						
rubro del presente contrato, siempre y cuando no se haya efectuado una reparacion por un tercer. Asimismo, el Distribuidor sera el responsable por las 						
descomposturas, daños o perdidaas parciales o totales imputables a el, mientras el vehiculo se encuentre bajo su resguardo para llevar a cabo el cumplimiento de la						
garantia; (  ) garantia vigente otorgada por el fabricante y/o importador y debera hacerse valida en los terminos y condiciones establecidos en la poliza de garantia						
correspondiente.<br>						
7. El Distribuidor entrega junto con el vehículo: usado los siguientes documentos: Factura Emitida por el  Distribuidor; (  ) Documentos oficiales que acrediten su legal estancia en el pais,  (  ) )Constancia de cambio de propietario, (  ) Comprobante de pago de tenecias, (  ) Comprobante de verificacion ambiental, (  ) Comprobante de pago de multsa y recargos. Los tramites y gastos de tramite de cambio de propietario, asi como los correspondientes a la investigacion del credito tanto del cliente como del fiador, en su caso, seran por cuenta del Cliente. El Cliente recibe el vehiculo usado descrito en el presente contrato, por lo que en este acto libera al distribuidor de adeudos o conflictos que por cualquier motivo pudiera generar dicho vehiculo a partir de la fecha de entrega en el entendido de que con anterioridad a esta fecha, el Distribuidor asume los adeudos o conflictos que por cualquier motivo pudiera generar dicho vehiculo, obligandose a responder por el saneamiento para el caso de eviccion.<br>						
8. Son causas de rescisión del presente contrato: (i) Que el distribuidor Incumpla con la entrega del vehículo en las condiciones pactadas en el presente contarto por causas imputable a el. - El Cliente le notificará por escrito el incumplimiento de dicha obligación y el Distribuidor devolverá las cantidad que por cualquier concepto hubiese recibido del Cliente con motivo de esta compraventa, en un plazo no mayor de 5 días hábiles a partir de la fecha en que fue notificado dicho incumplimiento, mas la cantidad por concepto de pena convencional equivalente al_____% del precio total de la venta del vehiculo, en el que se incluye el IVA.  (ii) Que el cliente incumpla con su obligacion de pago. En el evento que el Cliente incumpla con el pago de dos o mas abonos pactados, el Distribuidor le notificará por escrito su incumplimiento y podra exigirle la rescision o cumplimiento del contrato por mora, mas la pena convencional del _____% del precio total de la venta de vehiculo, en el que se incluye el IVA, en terminos de lo establecido en los articulos 70 y 71 de la LFPC. Las penas convencionales deberan ser equitativas y de la misma magnitud para las partes.<br>						
9. Las partes están de acuerdo en someterse a la competencia de la Procuraduría Federal del Consumidor en la vía administrativa para resolver cualquier controversia que se suscite sobre la interpretación o cumplimiento de los términos y condiciones del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-160-SCFI-2005 y cualquier otra disposición aplicable, sin perjuicio del derecho que tienen las partes de someterse a la jurisdicción de los Tribunales competentes del domicilio del Distribuidor, renunciando las partes expresamente a cualquier otra jurisdicción que pudiera corresponderles por razón de sus domicilios futuros.<br>						
10. El Cliente y el Distribuidor aceptan la realización de la presente compraventa, en los términos establecidos en este contrato, y sabedores de su contenido legal, lo   firman por duplicado.						
						
</p>

   
   </p>
<table align="center">
<tr>
<td><b>EL DISTRIBUIDOR</b></td>
<td><b>EL CLIENTE</b></td>
</tr>
<tr>
<td>OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td>RODOLFO RAMOS RUELAS</td>
</tr>
</table>
<p style="font-size:8px;">
(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número, de registro en tramite, el dia 19 de diciembre de 2008.<br>* LFPC.- Ley Federal de Proteccion al Consumidor
</p>
   
   ';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('ContratoSeminuevo.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		
		function adhesionNuevosDos($id){
			$info=$this->Contactomodel->getAllDataOrdendeCompra($id);
			$PAGOS= $this->Contactomodel->getPagosOrden($id);
			$PAGOSBAN= $this->Contactomodel->getPagosOrdenBan($id);
if(empty($PAGOSBAN[0]->nop_cantidad)){$PAGOSBANCO=0;}else{$PAGOSBANCO=$PAGOSBAN[0]->nop_cantidad;}			
$PAGOSAUT= $this->Contactomodel->getPagosOrdenAut($id);
if(empty($PAGOSAUT[0]->nop_cantidad)){$PAGOAUTO=0;}else{$PAGOAUTO=$PAGOSAUT[0]->nop_cantidad;}
$noautopagos=0;
$nobancopagos=0;			
foreach($PAGOS as $listapagos){
	$var=$listapagos->nop_tipo;
	if($var=='auto'){$noautopagos=$listapagos->nop_catidad;}
	if($var=='banco'){$nobancopagos=$listapagos->nop_catidad;}
	}
	
	
if($info[0]->hus_ciudad=='Tijuana'){$domicilio='AV. PADRE KINO#4300, ZONA RIO, TIJUANA, B.C.'; $telefono='(664) 683 4480'; $email='hondaoptima@hondaoptima.com'; $fax='(664) 683 4481';
$leyenda='(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número de registro 6881-2009, con fecha 2009/11/17.';
}			
if($info[0]->hus_ciudad=='Mexicali'){$domicilio='JUSTO SIERRA #1233, LOS PINOS, MEXICALI, B.C.';$telefono='(686) 568 4686';  $email='hondaoptima@hondaoptima.com';$fax='(686) 568 4687';
$leyenda='(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número de registro 6880-2009,  el dia 17 de Noviembre de 2009.';
}			
if($info[0]->hus_ciudad=='Ensenada'){$domicilio='AV. BALBOA 146, FRACC. GRANADOS, ENSENADA, B.C';$telefono='(646) 900 9000';  $email='hondaoptimaensenada@hondaoptima.com';$fax='(646) 900 9000';
$leyenda='(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número de registro 6881-2009, con fecha 2009/11/17.';
}			

    // create new PDF document
	 $pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Adhesion Seminuevos');
    $pdf->SetSubject('Adhesion Seminuevos');
    $pdf->SetKeywords('Adhesion Seminuevos, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage();

if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}

$info[0]->dats_valorfactura;

//precio venta
  if($info[0]->datco_nuevox=='si'){$prev=$info[0]->dats_valorfactura; $toprev=round($prev,2); }
  //nuevo
   if($info[0]->datco_snuevo=='si'){$iva=1.11;  $prev=$info[0]->dats_valorfactura /$iva;$toprev=round($prev,2); }
 //demo
    if($info[0]->datco_nuevo=='si'){$iva=1.11; $prev=$info[0]->dats_valorfactura / $iva;$toprev=round($prev,2); }  
	
	
//iva
 if($info[0]->datco_snuevo=='si'){ $valorty=$toprev * 0.11;  $miva=round($valorty,2);  } 
   //demo
 if($info[0]->datco_nuevo=='si'){ $valorty=$toprev * 0.11; $miva=round($valorty,2);} 
 //seminuevo
 if($info[0]->datco_nuevox=='si'){ $miva=0;}
 
 setlocale(LC_MONETARY, 'en_US');	

    $html = '
<table width="100%"><tr>
<td width="18%">

<img src="'.base_url().'img/logosilver.jpg">
</td>
<td width="55%">

<table width="100%" style="font-size:.7em">
<tr>
<td width="25%" ><b>Denominación</b></td>
<td width="75%"><b>HONDA OPTIMA</b></td>
</tr>
<tr>
<td width="25%" ><b>Domicilio:</b></td>
<td width="75%">'.$domicilio.'</td>
</tr>
<tr>
<td width="25%" ><b>R.F.C.</b></td>
<td width="75%">OAU 990325 G88</td>
</tr>
<tr>
<td width="25%"  style="font-size:.7em"><b>Telefono(s) y</b></td>
<td width="75%">'.$telefono.'</td>
</tr>
<tr>
<td width="25%"  style="font-size:.7em"><b>Horario de atencion</b></td>
<td width="75%">De 8:00am a 8:00pm de Lunes a Viernes</td>
</tr>
<tr>
<td width="25%" ><b>Fax</b></td>
<td width="75%">'.$fax.'</td>
</tr>
<tr>
<td width="25%"><b>E. Mail</b></td>
<td width="75%">'.$email.'</td>
</tr>
</table>
</td>
<td width="27%">
<table style="font-size:.7em" border="1"><tr><td>
<table>
<tr>
<td> Factura-Folio</td>
<td>'.$info[0]->dats_factura.'</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<tr>
<td> Fecha</td>
<td>'.$info[0]->dat_fecha_facturacion.'</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<tr>
<td> Localidad:</td>
<td align="center">'.$info[0]->hus_ciudad.'</td>
</tr>
<tr>
<td></td>
<td align="center">Baja California</td>
</tr>
</table>

</td></tr></table>
</td>
</tr></table>	
<table border="1"><tr><td>

<table><tr>
<td style="font-size:.7em" >
 DATOS DEL CLIENTE (CONSUMIDOR):</td></tr>
<tr><td>
<table width="100%" style="font-size:8px">
<tr>
<td width="15%" ><b> Nombre</b></td>
<td   width="30%">'.$cliente.'</td>
<td width="20%"><b>R.F.C</b></td>
<td  width="30%">'.$info[0]->datc_rfc.'</td>
</tr>
<tr>
<td width="15%" > <b>Domicilio</b></td>
<td  width="30%">'.$info[0]->datc_calle.'</td>
<td width="20%"><b>Colonia</b></td>
<td  width="30%">'.$info[0]->datc_colonia.'</td>
</tr>
<tr>
<td width="15%" > <b>Codigo Postal</b></td>
<td  width="30%"> '.$info[0]->datc_cp.'</td>
<td width="20%"><b>Delegacion o Municipio</b></td>
<td  width="30%">'.$info[0]->datc_ciudad.'</td>
</tr>
<tr>
<td width="15%" > <b>Estado</b></td>
<td width="30%">'.$info[0]->datc_estado.'</td>
<td width="20%"><b>Telefonos</b></td>
<td width="30%">'.$info[0]->datc_tcasa.'</td>
</tr>
</table>
</td></tr></table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table border="1"><tr><td>

<table><tr>
<td style="font-size:.7em" >
 CARACTERÍSTICAS DEL VEHÍCULO:</td>
 
 <td style="font-size:.7em" ><b></b></td>
 <td></td>
 </tr>
<tr>
<td colspan="3" >
<table width="100%" style="font-size:8px">
<tr>
<td width="15%" > <b>Marca: </b></td>
<td   width="30%">'.$info[0]->data_marca.'</td>
<td width="30%"><b>Catalogo:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="15%" > <b>Sub-marca: </b></td>
<td  width="30%" style="font-size:.9em">'.$info[0]->data_modelo.'</td>
<td width="30%"><b>Número Ident. Vehicular:</b></td>
<td  width="25%">'.$info[0]->data_vin.'</td>
</tr>
<tr>
<td width="15%" > <b>Tipo o Version:</b></td>
<td width="30%">'.$info[0]->data_version.'</td>
<td width="30%"><b>Capacidad:</b></td>
<td width="25%">'.$info[0]->data_capacidad.'</td>
</tr>
<tr>
<td width="15%" > <b>Color:</b></td>
<td width="30%">'.$info[0]->data_color.'</td>
<td width="30%"><b>Fecha de entrega : </b></td>
<td width="25%">'.$info[0]->dat_fecha_entrega.'</td>
</tr>
<tr>
<td width="15%" > <b>Año-modelo:</b></td>
<td width="30%">'.$info[0]->data_ano.'</td>
<td width="30%"><b>Lugar de entrega del Vehículo: </b></td>
<td style="color:red" width="25%">HONDA OPTIMA AUTOMOTRIZ </td>
</tr>
</table>
</td></tr></table>
</td></tr></table>

<table><tr><td></td></tr></table>
<table border="1"><tr><td>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>MONTO DE LA OPERACIÓN: </b></td>
<td   width="15%"></td>
<td width="35%"><b>FORMA DE PAGO:</b></td>
<td  width="25%"><b>CONTADO</b></td>
</tr>
<tr>
<td width="25%" > Precio del vehículo: </td>
<td  width="15%">'.money_format('%(#10n',$toprev).'</td>
<td width="35%">Contado:</td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" > Equipo y accesorios adicionales (Ver análisis)</td>
<td width="15%"></td>
<td width="35%">Enganche unidad o Enganche toma auto:</td>
<td width="25%">'.money_format('%(#10n',$info[0]->dats_monto).'</td>
</tr>
<tr>
<td width="25%" > Otros cargos:</td>
<td width="15%"> '.money_format('%(#10n',$info[0]->avi_gastosadicionales).'</td>
<td width="35%"></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" > Impuesto al Valor Agregado:</td>
<td width="15%">'.money_format('%(#10n',$miva).'</td>
<td width="35%"></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Monto de la operación:</b></td>
<td width="15%">'.money_format('%(#10n',$toprev+$miva).'</td>
<td width="35%"></td>
<td width="25%"></td>
</tr>
</table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table border="1"><tr><td>
<table width="100%" style="font-size:8px">
<tr>
<td width="40%" colspan="2" > <b>EQUIPO Y ACCESORIOS ADICIONALES: </b></td>
<td   width="15%"></td>
<td width="35%"><b>DESCRIPCIÓN UNIDAD USADA:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td  width="15%">'.money_format('%(#10n',$info[0]->avi_accesoriosanexarpedido).'</td>
<td width="35%">Número de Identificación Vehicular:</td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Marca:</td>
<td width="25%">'.$info[0]->datco_marca.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Submarca:</td>
<td width="25%">'.$info[0]->datco_modelo.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Tipo o versión:</td>
<td width="25%">'.$info[0]->datco_version.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Color:</td>
<td width="25%">'.$info[0]->datco_color.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Año-modelo:</td>
<td width="25%">'.$info[0]->datco_ano.'</td>
</tr>
<tr>
<td width="25%" style="font-size:.9em" > Total equipo y accesorios adicionales:</td>
<td width="15%"></td>
<td width="35%" align="right"><b>Valor de la Unidad:</b></td>
<td width="25%">'.$PAGOAUTO.'</td>
</tr>
</table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table border="1"><tr><td>
<table><tr>
<td style="font-size:.8em" >
 <b>CONDICIONES DEL CONTRATO DE COMPRA-VENTA DE VEHÍCULO NUEVO AL CONTADO</b>
</td></tr>
<tr><td>
<p style="font-size:6px">
1. En virtud de este contrato (*), el Distribuidor vende al Cliente (Consumidor) el vehículo cuyas características se detallan en este documento.<br>						
2. El vehículo cuenta con el equipo opcional y accesorios adicionales solicitados y autorizados por el Cliente. <br>						
3. El precio de la compraventa será cubierto en la fecha de firma del presente contrato, incluyendo, en su caso, los equipos y accesorios adicionales.<br>
4. En caso de que el Cliente entregue un vehículo usado a cuenta del precio, entregará también la documentación correspondiente, según relación anexa, declarando de manera expresa que dicha documentación es legítima. Los impuestos anteriores no pagados así como sus accesorios y gastos de trámite de "baja", serán por cuenta del Cliente. Asimismo, el Cliente manifiesta que el vehículo está libre de gravamen y no tiene problema judicial y/o administrativo alguno, por lo que en este acto libera al Distribuidor de adeudos o conflictos que por cualquier motivo pudiera presentar  dicho vehículo hasta la fecha  de su entrega.
<br>
5. El Distribuidor entrega junto con el vehículo: (i) la Carta-Factura; (ii) el Manual del Usuario; y (iii) la Póliza de Garantía otorgada por el fabricante y/o importador en la que se establecen los términos y condiciones para su cumplimiento, debidamente sellada y firmada. El Distribuidor entregará al Cliente la Factura Original dentro de un plazo de 15 días contados a partir de la fecha en la que el Cliente hubiese liquidado el monto de la compraventa.<br>						
6. En caso de que dentro del periodo de garantía, el Cliente acuda ante cualquier distribuidor autorizado para solicitar la reparación del vehículo conforme a la garantía otorgada por el fabricante y/o importador, y el distribuidor autorizado no cuente con las refacciones necesarias para la reparación del vehículo en un plazo máximo de 60 días naturales contados a partir de la fecha en la que el Cliente haya presentado el vehículo para su reparación, quien haya otorgado la garantía asumirá ante el Cliente los costos por el incumplimiento en los términos establecidos en la garantía, en la NOM-160-SCFI-2003, y de acuerdo con las políticas y procedimientos de garantía convenidos entre el fabricante o el importador con el Distribuidor.<br>						
7. Son causas de rescisión del presente contrato: (i) Incumplimiento de entrega del vehículo por parte del Distribuidor.- El Cliente le notificará por escrito el incumplimiento de dicha obligación y el Distribuidor devolverá la cantidad que por cualquier concepto hubiese recibido del Cliente con motivo de esta compraventa, en un plazo no mayor de 5 días hábiles a partir de la fecha en que fue notificado dicho incumplimiento.<br>						
8. Las partes están de acuerdo en someterse a la competencia de la Procuraduría Federal del Consumidor en la vía administrativa para resolver cualquier controversia que se suscite sobre la interpretación o cumplimiento de los términos y condiciones del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-160-SCFI-2003, Prácticas Comerciales- Elementos Normativos para la Comercialización de Vehículos Nuevos y cualquier otra disposición aplicable, sin perjuicio del derecho que tienen las partes de someterse a la jurisdicción de los Tribunales competentes del domicilio del Distribuidor, renunciando las partes expresamente a cualquier otra jurisdicción que pudiera corresponderles por razón de sus domicilios futuros.<br>						
9. El Cliente y el Distribuidor aceptan la realización de la presente compraventa, en los términos establecidos en este contrato, y sabedores de su alcance legal, lo firman por duplicado.						
</p>					
</td></tr></table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table align="center" style="font-size:.7em">

<tr>
<td><b>EL DISTRIBUIDOR</b></td>
<td><b>EL CLIENTE</b></td>
</tr>
<tr>
<td>OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td>'.$cliente.'</td>
</tr>
<tr style="font-size:.8em">
<td></td>
<td></td>
</tr>
<tr style="font-size:.8em">
<td>(Nombre y Firma)</td>
<td>(Nombre y Firma)</td>
</tr>

</table>
<br><br><br>
<p style="font-size:.6em;">'.$leyenda.'</p> 
   ';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('ContratoSeminuevo.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		
		
		
		function adhesionSeminuevosDos($id){
			$info=$this->Contactomodel->getAllDataOrdendeCompra($id);
			
if($info[0]->hus_ciudad=='Tijuana'){$domicilio='AV. PADRE KINO#4300, ZONA RIO, TIJUANA, B.C.'; $telefono='(664) 683 4480'; $email='hondaoptima@hondaoptima.com'; $fax='(664) 683 4481';
$leyenda='(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número 134-2011 con fecha 07 de enero de 2011. ';
}			
if($info[0]->hus_ciudad=='Mexicali'){$domicilio='JUSTO SIERRA #1233, LOS PINOS, MEXICALI, B.C.';$telefono='(686) 568 4686';  $email='hondaoptima@hondaoptima.com';$fax='(686) 568 4687';
$leyenda='(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número de registro 133-2011, de fecha 7 de Enero de 2011 ';
}			
if($info[0]->hus_ciudad=='Ensenada'){$domicilio='AV. BALBOA 146, FRACC. GRANADOS, ENSENADA, B.C';$telefono='(646) 900 9000';  $email='hondaoptimaensenada@hondaoptima.com';$fax='(664) 683 4481';
$leyenda='(*) El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo el número 134-2011 con fecha 07 de enero de 2011. ';
}				
			
$PAGOS= $this->Contactomodel->getPagosOrden($id);
$PAGOSBAN= $this->Contactomodel->getPagosOrdenBan($id);
if(empty($PAGOSBAN[0]->nop_cantidad)){$PAGOSBANCO=0;}else{$PAGOSBANCO=$PAGOSBAN[0]->nop_cantidad;}			
$PAGOSAUT= $this->Contactomodel->getPagosOrdenAut($id);
if(empty($PAGOSAUT[0]->nop_cantidad)){$PAGOAUTO=0;}else{$PAGOAUTO=$PAGOSAUT[0]->nop_cantidad;}
			
$noautopagos=0;
$nobancopagos=0;			
foreach($PAGOS as $listapagos){
	$var=$listapagos->nop_tipo;
	if($var=='auto'){$noautopagos=$listapagos->nop_catidad;}
	if($var=='banco'){$nobancopagos=$listapagos->nop_catidad;}
	
	}			

    // create new PDF document
	 $pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Adhesion Seminuevos');
    $pdf->SetSubject('Adhesion Seminuevos');
    $pdf->SetKeywords('Adhesion Seminuevos, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage();

if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}

$info[0]->dats_valorfactura;

//precio venta
  if($info[0]->datco_nuevox=='si'){$prev=$info[0]->dats_valorfactura; $toprev=round($prev,2); }
  //nuevo
   if($info[0]->datco_snuevo=='si'){$iva=1.11;  $prev=$info[0]->dats_valorfactura /$iva;$toprev=round($prev,2); }
 //demo
    if($info[0]->datco_nuevo=='si'){$iva=1.11; $prev=$info[0]->dats_valorfactura / $iva;$toprev=round($prev,2); }  
	
	
//iva
 if($info[0]->datco_snuevo=='si'){ $valorty=$toprev * 0.11;  $miva=round($valorty,2);  } 
   //demo
 if($info[0]->datco_nuevo=='si'){ $valorty=$toprev * 0.11; $miva=round($valorty,2);} 
 //seminuevo
 if($info[0]->datco_nuevox=='si'){ $miva=0;}
 
 setlocale(LC_MONETARY, 'en_US');	

    $html = '
<table width="100%"><tr>
<td width="18%">

<img src="'.base_url().'img/logosilver.jpg">
</td>
<td width="55%">

<table width="100%" style="font-size:.7em">
<tr>
<td width="25%" ><b>Denominación</b></td>
<td width="75%"><b>HONDA OPTIMA</b></td>
</tr>
<tr>
<td width="25%" ><b>Domicilio:</b></td>
<td width="75%">'.$domicilio.'</td>
</tr>
<tr>
<td width="25%" ><b>R.F.C.</b></td>
<td width="75%">OAU-990325-G88</td>
</tr>
<tr>
<td width="25%" ><b>Telefonos</b></td>
<td width="75%">'.$telefono.'</td>
</tr>
<tr>
<td width="25%"  style="font-size:.7em"><b>Horario de atencion</b></td>
<td width="75%">De 8:00am a 8:00pm </td>
</tr>
<tr>
<td width="25%" ><b>Fax</b></td>
<td width="75%">'.$fax.'</td>
</tr>
<tr>
<td width="25%"><b>E. Mail</b></td>
<td width="75%">'.$email.'</td>
</tr>
</table>
</td>
<td width="27%">
<table style="font-size:.7em" border="1"><tr><td>
<table>
<tr>
<td> Factura-Folio</td>
<td>'.$info[0]->dats_factura.'</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<tr>
<td> Fecha</td>
<td>'.$info[0]->dat_fecha_facturacion.'</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<tr>
<td> Localidad:</td>
<td align="center">'.$info[0]->hus_ciudad.'</td>
</tr>
<tr>
<td></td>
<td align="center">Baja California</td>
</tr>
</table>

</td></tr></table>
</td></tr></table>	
<table border="1"><tr><td>

<table><tr>
<td style="font-size:.7em" >
 DATOS DEL CLIENTE (CONSUMIDOR):</td></tr>
<tr><td>
<table width="100%" style="font-size:8px">
<tr>
<td width="15%" ><b> Nombre</b></td>
<td   width="30%">'.$cliente.'</td>
<td width="20%"><b>R.F.C</b></td>
<td  width="30%">'.$info[0]->datc_rfc.'</td>
</tr>
<tr>
<td width="15%" > <b>Domicilio</b></td>
<td  width="30%">'.$info[0]->datc_calle.'</td>
<td width="20%"><b>Colonia</b></td>
<td  width="30%">'.$info[0]->datc_colonia.'</td>
</tr>
<tr>
<td width="15%" > <b>Codigo Postal</b></td>
<td  width="30%"> '.$info[0]->datc_cp.'</td>
<td width="20%"><b>Delegacion o Municipio</b></td>
<td  width="30%">'.$info[0]->datc_ciudad.'</td>
</tr>
<tr>
<td width="15%" > <b>Estado</b></td>
<td width="30%">'.$info[0]->datc_estado.'</td>
<td width="20%"><b>Telefonos</b></td>
<td width="30%">'.$info[0]->datc_tcasa.'</td>
</tr>
</table>
</td></tr></table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table border="1"><tr><td>

<table><tr style="font-size:.7em">
<td  >
CARACTERÍSTICAS DEL VEHÍCULO USADO:</td>
 

 <td> <b>Numero de Km. recorridos:</b>'.$info[0]->data_km.'</td>
 </tr>
<tr>
<td colspan="2" >
<table width="100%" style="font-size:8px">
<tr>
<td width="15%" > <b>Marca: </b></td>
<td   width="30%">'.$info[0]->data_marca.'</td>
<td width="30%"><b>Número Ident. Vehicular:</b></td>
<td  width="25%">'.$info[0]->data_vin.'</td>
</tr>
<tr>
<td width="15%" > <b>Sub-marca o Tipo: </b></td>
<td  width="30%" style="font-size:.9em">'.$info[0]->data_modelo.'</td>
<td width="30%"><b>Capacidad</b></td>
<td  width="25%">'.$info[0]->data_capacidad.'</td>
</tr>
<tr>
<td width="15%" > <b>Tipo o Version:</b></td>
<td width="30%">'.$info[0]->data_version.'</td>
<td width="30%"><b>Numero de placas:</b></td>
<td width="25%">'.$info[0]->data_placas.'</td>
</tr>
<tr>
<td width="15%" > <b>Color:</b></td>
<td width="30%">'.$info[0]->data_color.'</td>
<td width="30%"><b>Fecha de entrega : </b></td>
<td width="25%">'.$info[0]->dat_fecha_entrega.'</td>
</tr>
<tr>
<td width="15%" > <b>Año-modelo:</b></td>
<td width="30%">'.$info[0]->data_ano.'</td>
<td width="30%"><b>Lugar de entrega del Vehículo: </b></td>
<td style="color:red" width="25%">HONDA OPTIMA AUTOMOTRIZ </td>
</tr>
</table>
</td></tr></table>
</td></tr></table>

<table><tr><td></td></tr></table>
<table border="1"><tr><td>
<table width="100%" style="font-size:8px">
<tr>
<td width="25%" ><b>MONTO DE LA OPERACIÓN: </b></td>
<td   width="15%"></td>
<td width="35%"><b>FORMA DE PAGO:</b></td>
<td  width="25%"><b>CONTADO</b></td>
</tr>
<tr>
<td width="25%" > Precio del vehículo: </td>
<td  width="15%">'.money_format('%(#10n',$toprev).'</td>
<td width="35%">Contado:</td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" > Equipo y accesorios adicionales (Ver análisis)</td>
<td width="15%"></td>
<td width="35%">Enganche o unidad usada a cuenta (Ver descripción):</td>
<td width="25%">'.money_format('%(#10n',$info[0]->dats_monto).'</td>
</tr>
<tr>
<td width="25%" > Otros cargos:</td>
<td width="15%"> '.money_format('%(#10n',$info[0]->avi_gastosadicionales).'</td>
<td width="35%"></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" > Impuesto al Valor Agregado:</td>
<td width="15%">'.money_format('%(#10n',$miva).'</td>
<td width="35%"></td>
<td width="25%"></td>
</tr>
<tr>
<td width="25%" ><b>Monto de la operación:</b></td>
<td width="15%">'.money_format('%(#10n',$toprev+$miva).'</td>
<td width="35%"></td>
<td width="25%"></td>
</tr>
</table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table border="1"><tr><td>
<table width="100%" style="font-size:8px">
<tr>
<td width="40%" colspan="2" > <b>EQUIPO Y ACCESORIOS ADICIONALES: </b></td>
<td   width="15%"></td>
<td width="35%"><b>DESCRIPCIÓN UNIDAD USADA A CUENTA:</b></td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td  width="15%">'.money_format('%(#10n',$info[0]->avi_accesoriosanexarpedido).'</td>
<td width="35%">Número de Identificación Vehicular:</td>
<td  width="25%"></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Marca:</td>
<td width="25%">'.$info[0]->datco_marca.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Submarca:</td>
<td width="25%">'.$info[0]->datco_modelo.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Tipo o versión:</td>
<td width="25%">'.$info[0]->datco_version.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Color:</td>
<td width="25%">'.$info[0]->datco_color.'</td>
</tr>
<tr>
<td width="25%" ></td>
<td width="15%">$</td>
<td width="35%">Año-modelo:</td>
<td width="25%">'.$info[0]->datco_ano.'</td>
</tr>
<tr>
<td width="25%" style="font-size:.9em" > Total equipo y accesorios adicionales:</td>
<td width="15%"></td>
<td width="35%" align="right"><b>Valor de la Unidad:</b></td>
<td width="25%">'.$PAGOAUTO.'</td>
</tr>
</table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table border="1"><tr><td>
<table><tr>
<td style="font-size:.8em" >
 <b>CONDICIONES DEL CONTRATO DE COMPRA-VENTA DE VEHÍCULO USADO AL CONTADO</b>
</td></tr>
<tr><td>
<p style="font-size:6px; margin-left:2px;">
1.	En virtud de este contrato (*), el Distribuidor (Proveedor) como legítimo propietario vende al Cliente (Consumidor) el vehículo usado cuyas características se detallan en este documento, quien lo recibe después de haber efectuado una revisión de forma detallada.<br>
2.	El vehículo usado cuenta con el equipo opcional y accesorios adicionales solicitados y autorizados por el Cliente, detallados en el presente contrato.<br>3.	El precio de la compraventa será cubierto en la fecha de firma del presente contrato, incluyendo, en su caso, los equipos y accesorios adicionales. <br>
4.	El Cliente acepta que por tratarse de un vehículo usado, lo adquiere en el estado de uso en el que se encuentra, el cual le fue facilitado para su revisión de forma detallada y cuenta con el siguiente equipo:  Exteriores: (  ) Limpiadores (plumas); (  ) Unidades de luces; (  ) Antena; (  ) Espejos laterales; (  ) Cristales;    (  ) Tapones de ruedas; (  ) Molduras completas; (  ) Tapón de gasolina; (  ) claxon; Interiores: (  ) Instrumentos de tablero; (  ) Calefacción; (  ) Aire acondicionado;  (  ) Radio/Tipo; (  ) Bocinas; (  ) Encendedor; (  ) Espejo retrovisor; (  ) ceniceros; (  ) Cinturones de seguridad; (  ) Tapetes; (  ) Manijas y/o controles interiores;      (  ) Equipo adicional; (  ) Accesorios; (  ) Otros. 
El vehículo se encuentra en las siguientes condiciones generales: Aspectos mecánicos__________________;  aspectos de carrocería____________.<br>
5.	En caso de que el Cliente entregue un vehículo usado a cuenta del precio, entrega también la documentación correspondiente, consistente en: (  ) Factura; (  ) Tarjeta de circulación; (  ) Documentos oficiales que acrediten su legal estancia en el país; (  ) Comprobante de pago de tenencias; (  ) Comprobante de verificación ambiental; (  ) Comprobantes de pago multas y recargos, declarando de manera expresa que dicha documentación es legítima. Los impuestos anteriores no pagados así como sus accesorios serán por cuenta y responsabilidad del Cliente. Asimismo, el Cliente manifiesta que el vehículo está libre de gravamen y no tiene problema judicial y/o administrativo alguno, por lo que en este acto libera al Distribuidor de adeudos o conflictos que por cualquier motivo pudiera generar dicho vehículo previo a la celebración del presente contrato.<br>
6.	El vehículo usado se vende (   ) sin garantía; (   ) con garantía por un plazo de _______, (Art. 77 de la LFPC* no podrá ser inferior a 60 días) contados a partir de la entrega del vehículo usado, excluyéndose la correspondiente a partes eléctricas y deberá hacerse válida en el domicilio, teléfonos y horarios de atención señalados en el rubro del presente contrato,  siempre y cuando no se haya efectuado una reparación por un tercero. Asimismo, el Distribuidor será el responsable por las descomposturas, daños o perdidas parciales o totales imputables a él, mientras el vehículo se encuentre bajo su resguardo para llevar a cabo el cumplimiento de la garantía; (  ) garantía vigente otorgada por el fabricante y/o Importador y deberá hacerse válida en los términos y condiciones establecidos en la póliza de garantía correspondiente.<br>
7.	El Distribuidor entrega junto con el vehículo usado los siguientes documentos: (   ) Factura emitida por el Distribuidor; (  ) Documentos oficiales que acrediten su legal estancia en el país; (  ) Constancia de cambio de propietario; (  ) Comprobante de pago de tenencias; ( ) Comprobante de verificación ambiental; ( ) Comprobantes de pago multas y recargos. Los trámites y gastos de trámite de "cambio de propietario", así como los correspondientes a la investigación de crédito tanto del Cliente como del fiador, en su caso, serán por cuenta del Cliente. El Cliente recibe el vehículo usado descrito en el presente contrato, por lo que en este acto libera al Distribuidor de adeudos o conflictos que por cualquier motivo pudiera generar dicho vehículo a partir de la fecha de su entrega, en el entendido de que con anterioridad a esta fecha, el Distribuidor asume los adeudos o conflictos que por cualquier motivo pudiera generar dicho vehículo, obligándose a responder por el saneamiento para el caso de evicción.<br>
8.	Son causas de rescisión del presente contrato: (i) Que el Distribuidor incumpla con la entrega del vehículo en las condiciones pactadas en el presente contrato por causas imputable a él.- El Cliente le notificará por escrito el incumplimiento de dicha obligación y el Distribuidor devolverá las cantidades que por cualquier concepto hubiese recibido del Cliente con motivo de esta compraventa, en un plazo no mayor de 5 días hábiles a partir de la fecha en que fue notificado dicho incumplimiento, más la cantidad por concepto de pena convencional equivalente al __% del precio total de venta del vehículo, en el que se incluye el IVA.<br>  
9 Las partes están de acuerdo en someterse a la competencia de la Procuraduría Federal del Consumidor en la vía administrativa para resolver cualquier controversia que se suscite sobre la interpretación o cumplimiento de los términos y condiciones del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-122-SCFI-2005, Prácticas Comerciales-Elementos Normativos para la Comercialización y/o Consignación de Vehículos Usados y cualquier otra disposición aplicable, sin perjuicio del derecho que tienen las partes de someterse a la jurisdicción de los Tribunales competentes del domicilio del Distribuidor, renunciando las partes expresamente a cualquier otra jurisdicción que pudiera corresponderles por razón de sus domicilios futuros.<br>
10. El Cliente y el Distribuidor aceptan la realización de la presente compraventa en los términos establecidos en este contrato, y sabedores de su contenido legal, lo firman por Duplicado. 
</p>					
</td></tr></table>
</td></tr></table>
<table><tr><td></td></tr></table>
<table align="center" style="font-size:.7em">

<tr>
<td><b>EL DISTRIBUIDOR</b></td>
<td><b>EL CLIENTE</b></td>
</tr>
<tr>
<td>OPTIMA AUTOMOTRIZ S.A. DE C.V.</td>
<td>'.$cliente.'</td>
</tr>
<tr  style="font-size:.8em">
<td ></td>
<td></td>
</tr>
<tr  style="font-size:.8em">
<td >(Nombre y firma)</td>
<td>(Nombre y firma)</td>
</tr>
</table>
<br><br>
<p style="font-size:.6em;">'.$leyenda.'</p>
   
   ';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('ContratoNuevo.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		
		
		
	
			
		function altaPlacas($id){
$info=$this->Contactomodel->getAllDataOrdendeCompra($id);
if($info[0]->hus_ciudad=='Tijuana'){$recaudador='Gonzalo Garcia Lopez';}			
if($info[0]->hus_ciudad=='Mexicali'){$recaudador='Favor de enviar nombre de recaudador a los Administradores del Sistema';}			
if($info[0]->hus_ciudad=='Ensenada'){$recaudador='Lic. Alejandro Flores Nuñez';}				
				
// create new PDF document
$pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Adhesion Seminuevos');
    $pdf->SetSubject('Adhesion Seminuevos');
    $pdf->SetKeywords('Adhesion Seminuevos, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage();

if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}

$info[0]->dats_valorfactura;

//precio venta
  if($info[0]->datco_nuevox=='si'){$prev=$info[0]->dats_valorfactura; $toprev=round($prev,2); }
  //nuevo
   if($info[0]->datco_snuevo=='si'){$iva=1.11;  $prev=$info[0]->dats_valorfactura /$iva;$toprev=round($prev,2); }
 //demo
    if($info[0]->datco_nuevo=='si'){$iva=1.11; $prev=$info[0]->dats_valorfactura / $iva;$toprev=round($prev,2); }  
	
	
//iva
 if($info[0]->datco_snuevo=='si'){ $valorty=$toprev * 0.11;  $miva=round($valorty,2);  } 
   //demo
 if($info[0]->datco_nuevo=='si'){ $valorty=$toprev * 0.11; $miva=round($valorty,2);} 
 //seminuevo
 if($info[0]->datco_nuevox=='si'){ $miva=0;}
 
 setlocale(LC_MONETARY, 'en_US');	
 
 $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    $html = '
<table width="100%"><tr>
<td width="18%">

<img src="'.base_url().'img/logosilver.jpg">
</td>
<td width="35%">


</td>
<td width="47%">
<br><br><br>
'.$info[0]->hus_ciudad.', B.C. a '.date('d').' de '.$meses[date('n')-1].' del '.date('Y').'</td></tr></table>	
<br><br><br>
Tramite Alta de Placas
<br><br>
Atención,<br>
'.$recaudador.'<br>
Recaudador Aux. De Rentas Via Rapida.
<br><br><br>

Por medio del presente autorizo a Optima Automotriz S.A. de C.V.<br><br>
Para que realice el tramite de alta del vehiculo a mi nombre.,<br><br>
Los datos del vehiculo son:<span style=" background-color:#ffcc99">'.$info[0]->data_auto.'</span><br><br>
Color:<span style=" background-color:#ffcc99">'.$info[0]->data_color.'</span> Serie:<span style=" background-color:#ffcc99"> '.$info[0]->data_vin.'</span><br><br>
No. de Motor:<span style=" background-color:#ffcc99">'.$info[0]->data_no_motor.'</span><br><br>
El cual fue adquirido en la agencia el dia:'.$info[0]->dat_fecha_entrega.'
<br><br><br><br>
<center>Sin mas por el momento me despido quedado a sus ordenes<br>
para cualquier duda o aclaración al respecto.</center>
<br><br><br><br>
Atentamente:
<br><br>
'.$cliente.'<br>
Tel.'.$info[0]->datc_tcasa.'
';
 
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
 
    // ---------------------------------------------------------    
 
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output('ContratoNuevo.pdf', 'I');    
 
    //============================================================+
    // END OF FILE
    //============================================================+
		
		}
		
		
		
		function avisoprivacidad($ido){
setlocale(LC_MONETARY, 'en_US');			
$info= $this->Contactomodel->getOrdendeCompraPdf($ido);
if($info[0]->datc_persona_fisica_moral=='fisica'){$cliente=$info[0]->datc_primernombre.' '.$info[0]->datc_segundonombre.' '.$info[0]->datc_apellidopaterno.' '.$info[0]->datc_apellidomaterno;}
if($info[0]->datc_persona_fisica_moral=='moral'){$cliente=$info[0]->datc_moral;}

// create new PDF document
$pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('Aviso Privacidad');
    $pdf->SetSubject('Cotizacion');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 


$html = '
<img src="'.base_url().'img/privacidad-1.jpg"><br>
<img src="'.base_url().'img/privacidad-2.jpg"><br><br><br>
<table width="100%"><tr><td>
<center>                                         <b>'.$cliente.'<br>Nombre y firma del Titular</b></center>
</td></tr></table>
';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('avisoprivacidad.pdf', 'I');    

		}		
	
	function fisicaldineroform($ido){
		$data['OD']= $this->Contactomodel->getOrdendeCompraPdf($ido);
		$data['id']=$ido;
		$this->load->view('contacto/fisicaldineroform', $data);
		}
		
		function moralldineroform($ido){
		$data['OD']= $this->Contactomodel->getOrdendeCompraPdf($ido);
		$data['id']=$ido;
		$this->load->view('contacto/moralldineroform', $data);
		}

function fisicaldinero($ido){
setlocale(LC_MONETARY, 'en_US');			
$OD= $this->Contactomodel->getOrdendeCompraPdf($ido);
// create new PDF document
$pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('');
    $pdf->SetSubject('');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 
$html = '
<span style="font-size:10px">Este formato cumple con lo establecido por la LFPIORPI</span><br>
<table  width="100%" align="center" bgcolor="#e41937" style="color:white"><tr><td>
Formato de Identificación del cliente persona física
</td></tr></table>
<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td style=" height:18px;">
Por este medio proporciono los datos y documentos requeridos con la única finalidad de identificarme
</td></tr></table>
<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr bgcolor="#dddddd" style="font-size:9px;" >
<td width="10%" style=" height:18px;" >
Fecha:
</td>
<td width="90%">'.date('d / m / Y').'</td></tr></table>
<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="4"></td></tr>
<tr bgcolor="#e7e7e7" style="font-size:9px;">
<td width="25%" style="font-size:9px;height:18px;" >Nombre Completo del Cliente</td>
<td width="25%">'.$OD[0]->datc_apellidopaterno.'</td>
<td width="25%">'.$OD[0]->datc_apellidomaterno.'</td>
<td width="25%">'.$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.'</td>
</tr>
<tr bgcolor="#e7e7e7" style="font-size:9px;">
<td width="25%">(SIN ABREVIATURA)</td>
<td width="25%">Apellido Paterno</td>
<td width="25%">Apellido Materno</td>
<td width="25%">Nombre(s)</td>
</tr>
</table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="3"></td></tr>
<tr  style="font-size:9px;">
<td width="25%" bgcolor="#f3f3f3" style="font-size:9px; height:18px;">Fecha de Nacimiento:'.$OD[0]->datc_fechanac.'</td>
<td width="37.5%" bgcolor="#dddddd">Pais de Nacimiento:'.$_GET['pais'].'</td>
<td width="37.5%" bgcolor="#f3f3f3">Nacionalidad:'.$_GET['nacionalidad'].'</td>
</tr>
<tr  style="font-size:9px;">
<td width="25%"  bgcolor="#dddddd"  style="height:18px;">R.F.C. :'.$_GET['rfc'].'</td>
<td colspan="2" width="75%"  bgcolor="#e7e7e7">Curp: '.$_GET['curp'].'</td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr bgcolor="#f3f3f3" style="font-size:9px;" >
<td width="30%"  style="height:18px;">
Actividad, Ocupación o Profesión:'.$_GET['actividad'].'
</td>
<td width="70%"></td></tr></table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" >
<td width="60%" bgcolor="#dddddd">
Tipo de Documento de Identificación Oficial<br>
emitida por autoridad local y federal: '.$_GET['identificacion'].'
</td>
<td width="40%" bgcolor="#e7e7e7">
Número<br> de Folio: '.$_GET['folio'].'
</td></tr></table>

<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="4"></td></tr>
<tr bgcolor="#e7e7e7" style="font-size:9px;" >
<td width="34%" style="font-size:9px; height:18px;" align="left">Domicilio Particular:</td>
<td width="33%" align="center">'.$_GET['calle'].'</td>
<td width="33%" align="center">'.$_GET['numeroint'].'</td>
</tr>
<tr align="center" bgcolor="#e7e7e7" style="font-size:9px;">
<td colspan="2" width="67%">Calle, Avenida ó vía y número exterior</td>
<td width="33%">Número interior</td>
</tr>
</table>


<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="3"></td></tr>
<tr  style="font-size:9px;">
<td width="25%" bgcolor="#f3f3f3" style="font-size:9px; height:18px;">Colonia: '.$_GET['colonia'].'</td>
<td width="37.5%" bgcolor="#dddddd">Delegación/Municipio/Demarcacion Politica: '.$_GET['delegacion'].'</td>
<td width="37.5%" bgcolor="#f3f3f3">País:'.$_GET['pais'].'</td>
</tr>
<tr  style="font-size:9px;">
<td width="25%"  bgcolor="#dddddd" style="height:18px;">Ciudad/Población: '.$_GET['ciudad'].'</td>
<td width="37.5%"  bgcolor="#e7e7e7">Entidad Federativa/Estado: '.$_GET['estado'].'</td>
<td width="37.5%"  bgcolor="#e7e7e7">C.P.: '.$_GET['cp'].'</td>
</tr>
</table>

<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="4"></td></tr>
<tr align="left" bgcolor="#e7e7e7" style="font-size:9px;">
<td width="34%"  style="height:18px;">Tel&eacute;fono1: '.$_GET['telefono'].'</td>
<td width="33%">Celular: '.$_GET['celular'].'</td>
<td width="33%">'.$_GET['ext'].'</td>
</tr>
<tr align="center" bgcolor="#e7e7e7" style="font-size:9px;">
<td width="34%">(Incluir Clave Lada/ Clave Internacional)</td>
<td width="33%">(Incluir Clave Lada/ Clave Internacional)</td>
<td width="33%">Extensión</td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr bgcolor="#dddddd" style="font-size:9px;" >
<td width="20%" style="height:18px;" >
Correo Electrónico: '.$_GET['email'].'
</td>
<td width="80%"></td></tr></table>
<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;"><td>
Agrego a la presente copia simple legible de los siguientes documentos previamente cotejados
</td></tr></table>
<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cidenti'].'</td>
<td width="90%" bgcolor="#f3f3f3"> Identificación Oficial</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['ccurp'].'</td>
<td width="90%" bgcolor="#f3f3f3"> CURP</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cdomi'].'</td>
<td width="90%" bgcolor="#f3f3f3"> Comprobante de domicilio</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['crfc'].'</td>
<td width="90%" bgcolor="#f3f3f3"> RFC</td></tr>
</table>
<table  width="100%" align="center"  >
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td width="60%" style="font-size:9px;">
Manifiesto que he tenido a la vista los documentos originales para cotejo: '.$_GET['funcionario'].'
</td>
<td width="40%">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td width="60%"></td>
<td width="40%" style="font-size:9px;">
Nombre y firma del funcionario de la agencia
</td>
</tr>
</table>
<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#dddddd" style="font-size:10px;"><td>
Refencias Personales y  Comerciales ( solo en caso de que los documentos originales tengan tachaduras o enmendaduras deber&aacute;n agregarce al expediente dos referencias personales y dos bancarias o comerciales que incluyan los siguientes datos, debidamente suscrita por quien otorga la refencia ).
</td></tr></table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
1.Nombre completo: '.$_GET['unobnom'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
1.Nombre completo: '.$_GET['unopnom'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
Dirección: '.$_GET['unobdire'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
Dirección: '.$_GET['unopdire'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
Tel&eacute;fono: '.$_GET['unobtele'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
Tel&eacute;fono: '.$_GET['unoptele'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
1.Nombre completo: '.$_GET['dosbnom'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
1.Nombre completo: '.$_GET['dospnom'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
Dirección: '.$_GET['dosbdire'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
Dirección: '.$_GET['dospdire'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
Tel&eacute;fono: '.$_GET['dosbtele'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
Tel&eacute;fono: '.$_GET['dosptele'].'
</td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cacto'].'</td>
<td width="90%" bgcolor="#f3f3f3"> El acto u operaci&oacute;n celebrada con la presente empresa automotriz sera&aacute; para beneficio propio y no tengo conocimiento de la existencia de algun proveedor de recursos, dueño o beneficiario controlador</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cpersona'].'</td>
<td width="90%" bgcolor="#f3f3f3"> La persona o grupo de personas que ejercer&aacute;n los derechos de uso, goce, disfrute, aprovechamiento o disposicion del vehículo objeto de la operacion son distintas al cliente (Dueño Beneficiario)(Requisitar Información al reverso)</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['crecursos'].'</td>
<td width="90%" bgcolor="#f3f3f3"> Los recursos para el acto u operación son aportados por persona distinta al cliente("Proveedor de recursos") (Requisistar informacion ala reverso)</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cmani'].'</td>
<td width="90%" bgcolor="#f3f3f3"> Manifiesto bajo protesta de decir la verdad que no cuento con RFC debido a que:</td></tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" >
<td width="70%"bgcolor="#f3f3f3" style="height:18px;" >
Razón Social de la Distribuidora: '.$_GET['razondis'].'
</td>
<td rowspan="2" bgcolor="#ffffff" width="30%" align="center">
<img src="'.base_url().'img/topleft.jpg">
</td></tr>
<tr  style="font-size:9px;" >
<td width="70%" align="center" bgcolor="#e7e7e7" >
Declaro bajo protesta decir la verdad que todos y cada uno de los datos proporcionados son verdaderos<br>
'.$_GET['declaro'].'
 <br>

*Datos y/o documentos obligatorios
</td></tr>
</table>
<span style="font-size:9px;">Formato de Identificaci&oacute;n Persona Física(Anverso)</span>
';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('personafisica.pdf', 'I');    

		}	
		
		
		
		
		function moralldinero($ido){
setlocale(LC_MONETARY, 'en_US');			
$OD= $this->Contactomodel->getOrdendeCompraPdf($ido);
// create new PDF document
$pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Optima Automotriz');
    $pdf->SetTitle('');
    $pdf->SetSubject('');
    $pdf->SetKeywords('Cotizacion, PDF, example, test, guide');   
	// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(PDF_MARGIN_LEFT, '1', PDF_MARGIN_RIGHT,'1');
$pdf->SetFooterMargin(0);    
$pdf->SetAutoPageBreak(TRUE, 1); 
$pdf->AddPage(); 
$html = '
<span style="font-size:10px">Este formato cumple con lo establecido por la LFPIORPI</span><br>
<table  width="100%" align="center" bgcolor="#e41937" style="color:white"><tr><td>
Formato de Identificación del cliente persona moral
</td></tr></table>
<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td style=" height:18px;">
Por este medio proporciono los datos y documentos requeridos con la única finalidad de identificarme
</td></tr></table>
<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="3"></td></tr>
<tr bgcolor="#dddddd" style="font-size:9px;" >
<td width="10%" style=" height:18px;" >
<b>Fecha:</b>
</td>
<td width="10%">'.date('d/m/Y').'</td>
<td bgcolor="#f3f3f3" width="80%"><b>Razón Social de la Distribuidora:</b>'.$OD[0]->datc_moral.'</td>

</tr></table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="3"></td></tr>
<tr bgcolor="#dddddd" style="font-size:9px;" >
<td width="34%" style=" height:18px;" bgcolor="#f3f3f3" ><b>Fecha de Costitución:</b>'.$_GET['fechacon'].'</td>
<td width="34%"><b>Pais de Nacimiento:'.$_GET['paisnac'].'</b></td>
<td bgcolor="#f3f3f3" width="32%"><b>Nacionalidad:'.$_GET['naci'].'</b></td>
</tr></table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="3"></td></tr>
<tr bgcolor="#dddddd" style="font-size:9px;" >
<td width="10%" style=" height:18px;" >
<b>R.F.C.:</b>
</td>
<td width="10%">'.$OD[0]->datc_rfc.'</td>
<td bgcolor="#f3f3f3" width="80%"><b>Actividad, Giro Mercantil.:</b>'.$_GET['activi'].'</td>
</tr></table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="3"></td></tr>
<tr  style="font-size:9px;" >
<td bgcolor="#f3f3f3" width="100%"><b>Objeto Social que desempeñe o Giro.:</b> '.$_GET['objetosoc'].'</td>
</tr></table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="3"></td></tr>
<tr  style="font-size:9px;" >
<td bgcolor="#dddddd" width="100%"><b>Fecha de Inscripción en el<br>registro público de la propiedad:</b> '.$_GET['fechain'].'</td>
</tr></table>

<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:2px;"><td colspan="4"></td></tr>
<tr bgcolor="#e7e7e7" style="font-size:9px;" >
<td width="34%" style="font-size:9px; height:18px;" align="left"><b>Domicilio Particular:</b></td>
<td width="33%" align="center">'.$_GET['calle'].'</td>
<td width="33%" align="center">'.$_GET['numeroint'].'</td>
</tr>
<tr align="center" bgcolor="#e7e7e7" style="font-size:9px;">
<td colspan="2" width="67%"><b>Calle, Avenida ó vía y número exterior</b></td>
<td width="33%"><b>Número interior</b></td>
</tr>
</table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td colspan="3"></td></tr>
<tr  style="font-size:9px;">
<td width="25%" bgcolor="#f3f3f3" style="font-size:9px; height:18px;"><b>Colonia</b>: '.$_GET['colonia'].'</td>
<td width="37.5%" bgcolor="#dddddd"><b>Delegación/Municipio/Demarcacion Politica:</b> '.$OD[0]->datc_municipio.'</td>
<td width="37.5%" bgcolor="#f3f3f3"><b>País:</b>'.$_GET['pais'].'</td>
</tr>
<tr  style="font-size:9px;">
<td width="25%"  bgcolor="#dddddd" style="height:18px;"><b>Ciudad/Población:</b> '.$OD[0]->datc_ciudad.'</td>
<td width="37.5%"  bgcolor="#e7e7e7"><b>Entidad Federativa/Estado:</b> '.$OD[0]->datc_estado.'</td>
<td width="37.5%"  bgcolor="#e7e7e7"><b>C.P.: '.$OD[0]->datc_cp.'</b></td>
</tr>
</table>


<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:2px;"><td colspan="4"></td></tr>
<tr align="left" bgcolor="#e7e7e7" style="font-size:9px;">
<td width="34%"  style="height:18px;"><b>Tel&eacute;fono del Domicilio:</b> '.$_GET['telefono'].'</td>
<td width="33%">'.$_GET['extencion'].'</td>
<td width="33%"><b>Correo Electronico</b></td>
</tr>
<tr align="center" bgcolor="#e7e7e7" style="font-size:9px;">
<td width="34%"><b>(Incluir Clave Lada/ Clave Internacional)</b></td>
<td width="33%"><b>Extencion</b></td>
<td width="33%">'.$OD[0]->datc_email.'</td>
</tr>
</table>


<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td colspan="4"></td></tr>
<tr bgcolor="#e7e7e7" style="font-size:9px;">
<td width="25%" style="font-size:9px;height:18px;" ><b>Nombre Completo del Apoderado Legal <br> o representante</b></td>
<td width="25%">'.$_GET['apaterno'].'</td>
<td width="25%">'.$_GET['amaterno'].'</td>
<td width="25%">'.$_GET['nombres'].'</td>
</tr>
<tr bgcolor="#e7e7e7" style="font-size:9px;">
<td width="25%"><b>(SIN ABREVIATURA)</b></td>
<td width="25%"><b>Apellido Paterno</b></td>
<td width="25%"><b>Apellido Materno</b></td>
<td width="25%"><b>Nombre(s)</b></td>
</tr>
</table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td colspan="3"></td></tr>
<tr  style="font-size:9px;">
<td width="34%"  bgcolor="#dddddd"  style="height:18px;"><b>R.F.C. :</b>'.$_GET['rfc2'].'</td>
<td colspan="2" width="34%"  bgcolor="#e7e7e7"><b>Curp:</b> '.$_GET['curp'].'</td>
<td width="32%" bgcolor="#f3f3f3" style="font-size:9px; height:18px;"><b>Fecha de Nacimiento:</b>'.$OD[0]->datc_fechanac.'</td>
</tr>
</table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td colspan="3"></td></tr>
<tr  style="font-size:9px;">
<td width="45%"  bgcolor="#dddddd"  style="height:18px;"><b>Tipo de documento de indentificacion oficial del apoderado<br>o representate emitida por autoridad local o federal.:</b>'.$_GET['tipodoc'].'</td>
<td colspan="25" width="34%"  bgcolor="#e7e7e7"><b>Número de Folio:</b> '.$_GET['numfolio'].'</td>
<td width="30%" bgcolor="#f3f3f3" style="font-size:9px; height:18px;"><b>Autoridad Emisora del ID.:</b>'.$_GET['autoemi'].'</td>
</tr>
</table>


<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;"><td>
Agrego a la presente copia simple legible de los siguientes documentos previamente cotejados
</td></tr></table>
<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" ><td width="5%"bgcolor="#dddddd" >'.$_GET['cacta'].'</td>
<td width="45%" bgcolor="#f3f3f3"> Acta constitutiva Inscrita en el registro público de la propiedad</td>

<td width="5%"bgcolor="#dddddd" >'.$_GET['ccomprobante'].'</td>
<td width="45%" bgcolor="#f3f3f3"> Comprobante de Domicilio con antig&uuml;edad no mayor a 3 Meses</td>
</tr>
<tr  style="font-size:9px;" ><td width="5%"bgcolor="#dddddd" >'.$_GET['cpoder'].'</td>
<td width="45%" bgcolor="#f3f3f3"> C&eacute;dula de Identificación fiscal expedida por el SAT</td>
<td width="5%"bgcolor="#dddddd" >'.$_GET['cacta2'].'</td>
<td width="45%" bgcolor="#f3f3f3"> Poder del Representante Legal</td>
</tr>
<tr  style="font-size:9px;" ><td width="5%"bgcolor="#dddddd" >'.$_GET['cidelegal'].'</td>
<td width="45%" bgcolor="#f3f3f3"> Identificación del representate legal</td>
<td width="5%"bgcolor="#dddddd" >'.$_GET['cdomilegal'].'</td>
<td width="45%" bgcolor="#f3f3f3"> Comprobante de Domicilio del representate legal</td>
</tr>
<tr  style="font-size:9px;" ><td width="5%"bgcolor="#dddddd" >'.$_GET['crfc'].'</td>
<td width="45%" bgcolor="#f3f3f3"> RFC del Representante Legal</td>
<td width="5%"bgcolor="#dddddd" ></td>
<td width="45%" bgcolor="#f3f3f3"></td>
</tr>
</table>
<table  width="100%" align="center"  >
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td width="60%" style="font-size:9px;">
Manifiesto que he tenido a la vista los documentos originales para cotejo:
</td>
<td width="40%"> '.$_GET['funcionario'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td width="60%"></td>
<td width="40%" style="font-size:9px;">
Nombre y firma del funcionario de la agencia
</td>
</tr>
</table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td></td></tr>
<tr bgcolor="#dddddd" style="font-size:10px;"><td>
Refencias Personales y  Comerciales ( solo en caso de que los documentos originales tengan tachaduras o enmendaduras deber&aacute;n agregarce al expediente dos referencias personales y dos bancarias o comerciales que incluyan los siguientes datos, debidamente suscrita por quien otorga la refencia ).
</td></tr></table>

<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:2px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
<b>1.Nombre completo:</b> '.$_GET['unobnom'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
<b>1.Nombre completo:</b> '.$_GET['unopnom'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
<b>Dirección:</b> '.$_GET['unobdire'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
<b>Dirección:</b> '.$_GET['unopdire'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
<b>Tel&eacute;fono:</b> '.$_GET['unobtele'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
<b>Tel&eacute;fono:</b> '.$_GET['unoptele'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
<b>1.Nombre completo:</b> '.$_GET['dosbnom'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
<b>1.Nombre completo:</b> '.$_GET['dospnom'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
<b>Dirección:</b> '.$_GET['dosbdire'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
<b>Dirección:</b> '.$_GET['dospdire'].'
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:10px;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:9px; height:18px;">
<b>Tel&eacute;fono:</b> '.$_GET['dosbtele'].'
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:9px;">
<b>Tel&eacute;fono:</b> '.$_GET['dosptele'].'
</td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cacto'].'</td>
<td width="90%" bgcolor="#f3f3f3"> El acto u operaci&oacute;n celebrada con la presente empresa automotriz ser&aacute; para beneficio propio y no tengo conocimiento de la existencia de algun proveedor de recursos, dueño o beneficiario controlador la unidad ser&aacute; propiedad de la empresa que represento</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['cpersona'].'</td>
<td width="90%" bgcolor="#f3f3f3"> La persona o grupo de personas que ejercer&aacute;n los derechos de uso, goce, disfrute, aprovechamiento o disposicion del vehículo objeto de la operacion son distintas al cliente (Dueño Beneficiario)(Requisitar Información al reverso)</td></tr>
<tr  style="font-size:9px;" ><td width="10%"bgcolor="#dddddd" >'.$_GET['crecursos'].'</td>
<td width="90%" bgcolor="#f3f3f3"> Los recursos para el acto u operación son aportados por persona distinta al cliente("Proveedor de recursos") (Requisistar informacion ala reverso)</td></tr>

</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:2px;">
<td colspan="2"></td></tr>
<tr  style="font-size:9px;" >
<td width="35%" align="center" bgcolor="#e7e7e7" >
Nombre o razón Social del Cliente
<br>
'.$_GET['declaro'].'
 <br>

*Datos y/o documentos obligatorios
</td>

<td width="35%" align="center" bgcolor="#e7e7e7" >
Nombre y firma del apoderado legal
<br>
'.$_GET['declaro'].'
 <br>

*Datos y/o documentos obligatorios
</td>

<td  bgcolor="#ffffff" width="30%" align="center">
<img src="'.base_url().'img/topleft.jpg">
</td></tr>
</table>
<span style="font-size:9px;">Formato de Identificaci&oacute;n Persona Moral(Anverso)</span>
';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
$pdf->Output('personamoral.pdf', 'I');    

		}	
		
			
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/contacto.php */


