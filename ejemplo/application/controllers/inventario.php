<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventario extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Inventariomodel');
		 // load url helper
        $this->load->helper('url');
		$this->load->library('email');
		
		// load session library
        $this->load->library('session');
    }
 
    function index()
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}
		
		$data['flash_message'] = $this->session->flashdata('message');

function readCSV($csvFile){
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {
		$line_of_text[] = fgetcsv($file_handle, 1024);
	}
	fclose($file_handle);
	return $line_of_text;
}


$csvarray=$this->Inventariomodel->getCsvFile();
$csvarray[0]->csv_nombre;
$csvFile =base_url().'inventario/'.$csvarray[0]->csv_nombre;
$csv = readCSV($csvFile);


		if(!isset($_GET['ubicacion'])){$ubi='Piso';$data['chu'] ="";}
		else{$ubi=$_GET['ubicacion'];$data['chu'] ="checked='checked'";}
		
		if(!isset($_GET['ubicacion2'])){$ubi2='0';$data['chu2'] ="";}
		else{$ubi2=$_GET['ubicacion2'];$data['chu'] ="checked='checked'";}
		
		if(!isset($_GET['ubicacion3'])){$ubi3='Asignados';$data['chu3'] ="";}
		else{$ubi3=$_GET['ubicacion3'];$data['chu3'] ="checked='checked'";}
		
		if(!isset($_GET['sucursal'])){$suc='0';}else{$suc=$_GET['sucursal'];}
		if(!isset($_GET['modelo'])){$mod='0';}else{$mod=$_GET['modelo'];}
		if($ubi=='0' && $ubi2=='0' && $ubi3=='0'){}
		$data['suc']=$suc;
		$data['mod']=$mod;
		
		if($ubi!='0'){$data['todo_piso'] = $csv;}else{$data['todo_piso'] ='0';}
		if($ubi2!='0'){$data['todo_embarque'] = $this->Inventariomodel->getAsignacionPedido($suc,$mod);}else{$data['todo_embarque'] ='0';}
		if($ubi3!='0'){$data['todo_asignacion'] = $this->Inventariomodel->getAsignacionPedido($suc,$mod);}else{$data['todo_asignacion'] ='0';}
		
		
        $this->load->view('inventario/show', $data);
    }
	
	function indexPrueba()
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}
		
		$data['flash_message'] = $this->session->flashdata('message');

function readCSV($csvFile){
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {
		$line_of_text[] = fgetcsv($file_handle, 1024);
	}
	fclose($file_handle);
	return $line_of_text;
}


$csvarray=$this->Inventariomodel->getCsvFile();
$csvarray[0]->csv_nombre;
$csvFile =base_url().'inventario/'.$csvarray[0]->csv_nombre;
$csv = readCSV($csvFile);


		if(!isset($_GET['ubicacion'])){$ubi='Piso';$data['chu'] ="";}
		else{$ubi=$_GET['ubicacion'];$data['chu'] ="checked='checked'";}
		
		if(!isset($_GET['ubicacion2'])){$ubi2='0';$data['chu2'] ="";}
		else{$ubi2=$_GET['ubicacion2'];$data['chu'] ="checked='checked'";}
		
		if(!isset($_GET['ubicacion3'])){$ubi3='Asignados';$data['chu3'] ="";}
		else{$ubi3=$_GET['ubicacion3'];$data['chu3'] ="checked='checked'";}
		
		if(!isset($_GET['sucursal'])){$suc='0';}else{$suc=$_GET['sucursal'];}
		if(!isset($_GET['modelo'])){$mod='0';}else{$mod=$_GET['modelo'];}
		if($ubi=='0' && $ubi2=='0' && $ubi3=='0'){}
		$data['suc']=$suc;
		$data['mod']=$mod;
		
		if($ubi!='0'){$data['todo_piso'] = $csv;}else{$data['todo_piso'] ='0';}
		if($ubi2!='0'){$data['todo_embarque'] = $this->Inventariomodel->getAsignacionPedido($suc,$mod);}else{$data['todo_embarque'] ='0';}
		if($ubi3!='0'){$data['todo_asignacion'] = $this->Inventariomodel->getAsignacionPedido($suc,$mod);}else{$data['todo_asignacion'] ='0';}
		
		
        $this->load->view('inventario/show2', $data);
    }
	
	
	
	
	
	function add()
    {   
	
	session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('sucursal', 'Sucursal', 'required');
		$this->form_validation->set_rules('noinventario', 'Numero de Inventario', 'required');
		$this->form_validation->set_rules('ubicacion', 'Ubicacion', 'required');
		$this->form_validation->set_rules('almacen', 'Almacen', 'required');
		$this->form_validation->set_rules('llegada', 'Fecha de llegada', 'required');
		$this->form_validation->set_rules('vin', 'VIN', 'required');
		$this->form_validation->set_rules('ano', 'Año', 'required');
		$this->form_validation->set_rules('modelo', 'Modelo', 'required');
		$this->form_validation->set_rules('color', 'Color', 'required');
		$this->form_validation->set_rules('tunidad', 'Tipo de unidad', 'required');
		
		
 
        if ($this->form_validation->run())
        {
			$llegada=$this->input->post('llegada');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fecha=$ano.'-'.$mes.'-'.$dia;
			
			
			
        $todo = array(
                    'aut_sucursal'=>$this->input->post('sucursal'),
					'aut_no_inventario'=>$this->input->post('noinventario'),
					'aut_ano'=>$this->input->post('ano'),
					'aut_vin'=>$this->input->post('vin'),
					'aut_motor'=>$this->input->post('motor'),
					'aut_tipo_unidad'=>$this->input->post('tunidad'),
                    'aut_ubicacion'=>$this->input->post('ubicacion'),
					'aut_importe'=>$this->input->post('importe'),
					'aut_iva'=>$this->input->post('iva'),
					'aut_llegada'=>$fecha,
					'aut_foto'=>'auto.jpg',
					'aut_status'=>'Disponible',
					'version_ver_IDversion'=>'0',
					'modelos_mod_IDmodelos'=>$this->input->post('modelo'),
					'almacen_alm_IDalmacen'=>$this->input->post('almacen'),
					'color_col_IDcolor'=>$this->input->post('color'),
					'fichatecnica_fte_IDfichatecnica'=>$this->input->post('ftecnica')
                    );
				

						
           $this->Inventariomodel->addauto($todo);
		   
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado nuevo Auto al Inventario.</div>");            
            redirect('inventario');
        }
        else
        {
			 
			$data['todo_almacen'] = $this->Inventariomodel->getAlmacen();
			$data['todo_modelo'] = $this->Inventariomodel->getModelo();
			$data['todo_color'] = $this->Inventariomodel->getColor();
			$data['fichatecnica'] = $this->Inventariomodel->getFicha();
            $this->load->view('inventario/add', $data);
        }
    }
	
	
	
	
	function addasignacion()
    {   
	
	session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('grupo1', 'Sucursal', 'required');
		$this->form_validation->set_rules('mes', 'Mes', 'required');
		
		if ($this->form_validation->run())
        {
			
			$fecha=date('Y').'-'.date('m').'-'.date('d');
			$todo = array(
                    'asp_fecha'=>$fecha,
					'asp_mes'=>$this->input->post('mes'),
					'huser_hus_IDhuser'=>$_SESSION['id'],
					'asp_sucursal'=>$this->input->post('grupo1')
                    );
				
         $IDA=$this->Inventariomodel->addasigpedido($todo);
		 
		 
		 
		 $coni=$this->input->post('ano');
	     $coni=count($coni);
		 $coni=$coni-1;
		 $ano=$this->input->post('ano');
		 $mod=$this->input->post('modelo');
		 $col=$this->input->post('color');
		  $vco=$this->input->post('vco');
		 
		 for($i=0; $i<=$coni; $i++){
			 
			$todob = array(
                    'das_ano'=>$ano[$i],
					'das_modelo'=>$mod[$i],
					'das_color'=>$col[$i],
					'asignacion_pedido_asp_IDasignacion_pedido'=>$IDA->AID,
					'fichatecnica_fte_IDfichatecnica'=>'0',
					'das_status'=>'disponible',
					'das_vco'=>$vco[$i]
                    ); 
			 
			 
			 
			$this->Inventariomodel->adddetalleasig($todob);
			}  
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado una nueva Asignacion de Autos.</div>");            
            redirect('inventario');
        }
		
		 else
        {
			 
			
		
			
			$data['todo_ficha'] = $this->Inventariomodel->getFicha();
            $this->load->view('inventario/add', $data);
        }
   
    }
	


function transfer($id)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('vin', 'Vin', 'required');
		
		if ($this->form_validation->run())
        {
		
		
			 
			$todob = array(
                    'tra_fecha'=>date('Y-m-d'),
					'tra_vin'=>$this->input->post('vin'),
					'tra_IDhuser'=>$_SESSION['id']
                    ); 
			 $this->Inventariomodel->inserttransfer($todob);
			 
			 $todoc = array('das_status'=>'Transferido'); 
			 $this->Inventariomodel->editdetalleasig($todoc,$this->input->post('idasp'));
			 
			 if($this->input->post('status')=='Reservado')
			 {
				 
			  function readCSV($csvFile){
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {
		$line_of_text[] = fgetcsv($file_handle, 1024);
	}
	fclose($file_handle);
	return $line_of_text;
}


$csvarray=$this->Inventariomodel->getCsvFile();

// Set path to CSV file
$csvFile = 'http://hondaoptima.com/ventas/inventario/'.$csvarray[0]->csv_nombre;



$csv = readCSV($csvFile);
foreach($csv as $todo){
	
	 if ($todo[7] !==$this->input->post('vin')) {}else{
						    $ano=$todo[3]; 
							$mod=$todo[5];
							$col=$todo[6];
							$almacen=$todo[0];
							$ninventario=$todo[1];
							$fechallegada=$todo[2];
							$nmotor=$todo[8];
							$tipounidad=$todo[9];
							$diasinventario=$todo[10];
							$cilindros=$todo[13];
							$puertas=$todo[14];
							$pasajeros=$todo[15]; /**/
							
						   }
	
	}	 
				list($dia,$mes,$ano)=explode('/',$fechallegada); 
				 
			
			$todod = array(
                    'aau_IdFk'=>$this->input->post('vin'),
					'aau_asig_intell'=>$this->input->post('vin'),
					'aau_alamacen'=>$almacen,
					'aau_no_inventario'=>$ninventario,
					'aau_fecha_llegada'=>$ano.'-'.$mes.'-'.$dia,
					'aau_ano'=>$ano,
					'aau_modelo'=>$mod,
					'aau_color_exterior'=>$col,
					'aau_no_motor'=>$nmotor,
					'aau_tipo_unidad'=>$tipounidad,
					'aau_dias_inv'=>$diasinventario,
					'aau_cilindros'=>$cilindros,
					'aau_puertas'=>$puertas,
					'aau_pasajeros'=>$pasajeros
                    ); 	 
				 
			$this->Inventariomodel->editasignauto($todod,$this->input->post('idasp'));
			
			}
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha transferido el auto correctamente.</div>");            
            redirect('inventario');
        }
		
		 else
        {
			$data['idasp']=$id;
			$data['uno_asp'] = $this->Inventariomodel->getAsp($id);
			$data['todo_ficha'] = $this->Inventariomodel->getFicha();
            $this->load->view('inventario/transfer', $data);
        }
   
    }
	
	
	function adddata($id)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	$data['flash_message'] = $this->session->flashdata('message');
	
 $this->load->library('form_validation');
		$this->form_validation->set_rules('vin', 'Vin', 'required');
		$this->form_validation->set_rules('precio', 'precio', 'numeric');
		if ($this->form_validation->run())
        {
			
		$res=$this->Inventariomodel->seminuevosStatus($id);
		
		if($res){
		$todod = array(
                    'sem_vin'=>$this->input->post('vin'),
					'sem_km'=>$this->input->post('km'),
					'sem_precio'=>$this->input->post('precio'),
					'sem_garantia'=>$this->input->post('garantia'),
					'sem_status'=>'ok'
                    ); 	 
				 
			$this->Inventariomodel->editSeminuevos($todod,$id);	
			
			}
		else{	
			
			$todod = array(
                    'sem_vin'=>$this->input->post('vin'),
					'sem_km'=>$this->input->post('km'),
					'sem_precio'=>$this->input->post('precio'),
					'sem_garantia'=>$this->input->post('garantia'),
					'sem_status'=>'ok'
                    ); 	 
				 
			$this->Inventariomodel->insertSeminuevos($todod);
		}
		
		
		
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Informaci&oacute;n modificada.</div>");            
            redirect('inventario/adddata/'.$id.'');
		}
		
		 
else{
$data['id']=$id;
$data['uno_data'] = $this->Inventariomodel->getDataSeminuevo($id);
$data['lista_img'] = $this->Inventariomodel->getImageSeminuevo($id);
$this->load->view('inventario/adddata', $data);	
	}
		

   
    }


	function removeimg($id,$idi)
    { 
	
	if ($this->Inventariomodel->deleteimg($idi)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado la Imagen. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
       redirect('inventario/adddata/'.$id.'');
	
	}
	
	
	
	function apartar($id,$ty)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('contacto', 'Contacto', 'required');
		$this->form_validation->set_rules('vendedor', 'Vendedor', 'required');
		
		if ($this->form_validation->run())
        {
		$fecha=date('Y').'-'.date('m').'-'.date('d');
		$idc=$this->input->post('contacto');
			 
			$todob = array(
                    'aau_fecha'=>$fecha,
					'aau_pago'=>$this->input->post('cantidad'),
					'huser_hus_IDhuser'=>$this->input->post('vendedor'),
					'aau_status'=>'Apartado',
					'contacto_con_IDcontacto'=>$idc,
					'aau_IdFk'=>$id,
					'aau_asig_intell'=>$ty,
					'aau_folio'=>$this->input->post('recibo'),
					'aau_alamacen'=>'',
					'aau_no_inventario'=>'',
					'aau_fecha_llegada'=>'',
					'aau_ano'=>$this->input->post('ano'),
					'aau_modelo'=>$this->input->post('mod'),
					'aau_color_exterior'=>$this->input->post('color'),
					'aau_no_motor'=>'',
					'aau_tipo_unidad'=>'',
					'aau_dias_inv'=>$ty,
					'aau_agente_apartado'=>'',
					'aau_cilindros'=>'',
					'aau_puertas'=>'',
					'aau_pasajeros'=>'',
					'aau_ficha_tecnica'=>$this->input->post('ficha') 
                    ); 
					
			 $this->load->model('Contactomodel');
		
			 
			 
			 $IDAS=$this->Inventariomodel->saveapartado($todob,$ty,$id,$idc);
			 
			  if($idc==12219 || $idc==12220 || $idc==12223 || $idc==13760 || $idc==13805 || $idc==13806){}
			 else{$IDB=$this->Contactomodel->idBitacora($idc);
			 $this->Inventariomodel->actualizaractividades($IDAS->AID,$IDB->bit_IDbitacora);
			 }
	
			 
			 
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha apartado un auto con exito.</div>");            
            redirect('inventario');
        }
		
		 else
        {
			 $this->load->model('Vendedoresmodel');
			 $this->load->model('Contactomodel');
			 $data['todo_vendedor'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
			 $data['vin']=$id;
		if(empty($_GET['asesor'])){
				$data['lista_contactos']=array();
				$data['asesor']='';
				}
			else{
			$data['lista_contactos'] = $this->Contactomodel->getContactosApartar($_GET['asesor']);
			$data['asesor']=$_GET['asesor'];
				}
			
			
			$data['idasp']=$id;
			$data['ty']=$ty;
			$data['uno_asp'] = $this->Inventariomodel->getAsp($id);
            $this->load->view('inventario/apartar', $data);
        }
   
    }



function apartaredit($ida,$id)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('contacto', 'Contacto', 'required');
		$this->form_validation->set_rules('vendedor', 'Vendedor', 'required');
		
		if ($this->form_validation->run())
        {
		$fecha=date('Y').'-'.date('m').'-'.date('d');
		$idc=$this->input->post('contacto');
			 
			$todob = array(
                    'aau_fecha'=>$fecha,
					'aau_pago'=>$this->input->post('cantidad'),
					'huser_hus_IDhuser'=>$this->input->post('vendedor'),
					'aau_status'=>'Apartado',
					'contacto_con_IDcontacto'=>$idc,
					'aau_IdFk'=>$id,
					'aau_asig_intell'=>$ty,
					'aau_folio'=>$this->input->post('recibo'),
					'aau_alamacen'=>'',
					'aau_no_inventario'=>'',
					'aau_fecha_llegada'=>'',
					'aau_ano'=>$this->input->post('ano'),
					'aau_modelo'=>$this->input->post('mod'),
					'aau_color_exterior'=>$this->input->post('color'),
					'aau_no_motor'=>'',
					'aau_tipo_unidad'=>'',
					'aau_dias_inv'=>$ty,
					'aau_agente_apartado'=>'',
					'aau_cilindros'=>'',
					'aau_puertas'=>'',
					'aau_pasajeros'=>'',
					'aau_ficha_tecnica'=>$this->input->post('ficha') 
                    ); 
					
			 $this->load->model('Contactomodel');
			 
			 
			 
			 $IDAS=$this->Inventariomodel->saveapartado($todob,$ty,$id,$idc);
			 
			  if($idc==12219 || $idc==12220 || $idc==12223 || $idc==13760 || $idc==13805 || $idc==13806){}
			 else{
			 //$IDB=$this->Contactomodel->idBitacora($idc);
			 //$this->Inventariomodel->actualizaractividades($IDAS->AID,$IDB->bit_IDbitacora);
			 }
	
			 
			 
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha apartado un auto con exito.</div>");            
            redirect('inventario');
        }
		
		 else
        {
			 $this->load->model('Vendedoresmodel');
			 $this->load->model('Contactomodel');
			 $data['todo_vendedor'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
			 $data['vin']=$id;
		if(empty($_GET['asesor'])){
				$data['lista_contactos']=array();
				$data['asesor']='';
				}
			else{
			$data['lista_contactos'] = $this->Contactomodel->getContactosApartar($_GET['asesor']);
			$data['asesor']=$_GET['asesor'];
				}
			
			
			$data['idasp']=$id;
			$data['ty']=$ty;
			$data['uno_asp'] = $this->Inventariomodel->getAsp($id);
            $this->load->view('inventario/apartar', $data);
        }
   
    }




function apartarcsv($id,$ty)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('contacto', 'Contacto', 'required');
		$this->form_validation->set_rules('vendedor', 'Vendedor', 'required');
		
		if ($this->form_validation->run())
        {
		$fecha=date('Y').'-'.date('m').'-'.date('d');
		$idc=$this->input->post('contacto');
			 $fechalle=$this->input->post('fechallegada');
			 list($dia,$mes,$ano)=explode('/',$fechalle);
			$todob = array(
                    'aau_fecha'=>$fecha,
					'aau_pago'=>$this->input->post('cantidad'),
					'huser_hus_IDhuser'=>$this->input->post('vendedor'),
					'aau_status'=>'Apartado',
					'contacto_con_IDcontacto'=>$idc,
					'aau_IdFk'=>$id,
					'aau_asig_intell'=>$ty,
					'aau_folio'=>$this->input->post('recibo'),
					'aau_alamacen'=>$this->input->post('almacen'),
					'aau_no_inventario'=>$this->input->post('ninventario'),
					'aau_fecha_llegada'=>$ano.'-'.$mes.'-'.$dia,
					'aau_ano'=>$this->input->post('ano'),
					'aau_modelo'=>$this->input->post('mod'),
					'aau_color_exterior'=>$this->input->post('color'),
					'aau_no_motor'=>$this->input->post('nmotor'),
					'aau_tipo_unidad'=>$this->input->post('tipounidad'),
					'aau_dias_inv'=>$this->input->post('diasinventario'),
					'aau_agente_apartado'=>'',
					'aau_cilindros'=>$this->input->post('cilindros'),
					'aau_puertas'=>$this->input->post('puertas'),
					'aau_pasajeros'=>$this->input->post('pasajeros'),
					'aau_ficha_tecnica'=>''
					
                    ); 
					
			 $this->load->model('Contactomodel');
			 
			 $IDAS=$this->Inventariomodel->saveapartado($todob,$ty,$id,$idc);
			 $IDB=$this->Contactomodel->idBitacora($idc);
			 if($idc==12219 || $idc==12220 || $idc==12223 || $idc==13760 || $idc==13805 || $idc==13806){}
			 else{
			 $this->Inventariomodel->actualizaractividades($IDAS->AID,$IDB->bit_IDbitacora);
			 }
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha apartado un auto con exito.</div>");            
            redirect('inventario');
        }
		
		 else
        {
        	
function readCSV($csvFile)
{
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {$line_of_text[] = fgetcsv($file_handle, 1024);}
	fclose($file_handle);
	return $line_of_text;
}
$csvarray=$this->Inventariomodel->getCsvFile();

// Set path to CSV file
$csvFile = 'http://hondaoptima.com/ventas/inventario/'.$csvarray[0]->csv_nombre;
$csv = readCSV($csvFile);
$this->load->model('Vendedoresmodel');
			$this->load->model('Contactomodel');
			$data['todo_vendedor'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
			$data['ty']=$ty;
			$data['vin']=$id;
			$data['listcsv']=$csv;
			if(empty($_GET['asesor'])){
				$data['lista_contactos']=array();
				$data['asesor']='';
				}
			else{
			$data['lista_contactos'] = $this->Contactomodel->getContactosApartar($_GET['asesor']);
			$data['asesor']=$_GET['asesor'];
				}
			
            $this->load->view('inventario/apartarcsv', $data);
        }
   
    }
	
	
	function apartarcsvedit($id,$ty,$ida,$idh)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('contacto', 'Contacto', 'required');
		$this->form_validation->set_rules('vendedor', 'Vendedor', 'required');
		
		if ($this->form_validation->run())
        {
		$fecha=date('Y').'-'.date('m').'-'.date('d');
		$idc=$this->input->post('contacto');
		
			$todob = array(
					'huser_hus_IDhuser'=>$this->input->post('vendedor'),
					'contacto_con_IDcontacto'=>$idc,
                    ); 
					
			 $this->load->model('Contactomodel');
			 
			 $IDAS=$this->Inventariomodel->updateapartado($todob,$ida,$idc);
			 
			 $IDB=$this->Contactomodel->idBitacora($idc);
			 if($idc==12219 || $idc==12220 || $idc==12223 || $idc==13760 || $idc==13805 || $idc==13806){}
			 else{
			 $this->Inventariomodel->actualizaractividades($ida,$IDB->bit_IDbitacora);
			 }
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha apartado un auto con exito.</div>");            
            redirect('inventario/asignarauto');
        }
		
		 else
        {
        	
function readCSV($csvFile)
{
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {$line_of_text[] = fgetcsv($file_handle, 1024);}
	fclose($file_handle);
	return $line_of_text;
}
$csvarray=$this->Inventariomodel->getCsvFile();

// Set path to CSV file
$csvFile = 'http://hondaoptima.com/ventas/inventario/'.$csvarray[0]->csv_nombre;
$csv = readCSV($csvFile);
$this->load->model('Vendedoresmodel');
			$this->load->model('Contactomodel');
			$data['todo_vendedor'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
			$datax=$data['data_asig'] = $this->Vendedoresmodel->getTodoAsig($ida);
			$data['data_asig'] = $this->Vendedoresmodel->getTodoAsig($ida);
			$data['ty']=$ty;
			$data['vin']=$id;
			$data['listcsv']=$csv;
			$data['ida']=$ida;
			$data['idh']=$idh;
			if(empty($_GET['asesor'])){
				$data['lista_contactos']=array();
				$data['asesor']=$idh;
				$data['lista_contactos'] = $this->Contactomodel->getContactosApartar($idh);
				$data['idcon']=$datax[0]->con_IDcontacto;
				}
			else{
				
			$data['lista_contactos'] = $this->Contactomodel->getContactosApartar($_GET['asesor']);
			$data['asesor']=$_GET['asesor'];
			$data['idcon']='';
				}
			
            $this->load->view('inventario/apartarcsvedit', $data);
        }
   
    }
	
	
	function updateasigedit($id)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('ano', 'Ano', 'required');
		$this->form_validation->set_rules('mod', 'Modelo', 'required');
		
		if ($this->form_validation->run())
        {
		
		
			 
			$todob = array(
                    'das_ano'=>$this->input->post('ano'),
					'das_modelo'=>$this->input->post('mod'),
					'das_color'=>$this->input->post('color'),
					'fichatecnica_fte_IDfichatecnica'=>$this->input->post('ficha'),
					'das_status'=>'disponible' 
                    ); 
			 
			 $this->Inventariomodel->editdetalleasig($todob,$id);
			
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la informacion con exito.</div>");            
            redirect('inventario');
        }
		
		 else
        {
			$data['idasp']=$id;
			$data['uno_asp'] = $this->Inventariomodel->getAsp($id);
			$data['todo_ficha'] = $this->Inventariomodel->getFicha();
            $this->load->view('inventario/editasig', $data);
        }
   
    }
	
	
  
   function update($id)
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	  
	  
        $data['uno_inventario'] = $this->Inventariomodel->getAuto($id);
		$data['todo_almacen'] = $this->Inventariomodel->getAlmacen();
	    $data['todo_modelo'] = $this->Inventariomodel->getModelo();
		$data['todo_color'] = $this->Inventariomodel->getColor();
        $data['todo_ficha'] = $this->Inventariomodel->getFicha();
        $this->load->view('inventario/edit', $data);
 
    }
	
	
	
	
	 function listaprospectos()
    {
		$this->load->library('form_validation');
	    $this->load->library("curl");
		$this->form_validation->set_rules('idv', 'Selecciona un Agente de Ventas', 'required');
		
        $this->load->model('Contactomodel');
		$arr=$lpro[]= $this->Inventariomodel->getListaProspectos($this->input->post('idv'));
		$uno='';
		$uno.='<select name="contacto" style="width: 250px;">';	
		$uno.='<option value="">Seleccione una opcion</option>';
		foreach($arr as $arrlist){
				
		$uno.='<option value="'.$arrlist->con_IDcontacto.'">'.$arrlist->con_nombre.'  '.$arrlist->con_apellido.'</option>';
			
		}
$uno.='</select>';	
		echo $uno;
		
    }
	
	
	 function ventas()
    {
		
		session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}
		 $this->load->model('Vendedoresmodel');
		$data['todo_ventas'] = $this->Vendedoresmodel->getVentas('01-01-2013',date('d-m-Y'),'no');
		
		$this->load->view('inventario/ventas',$data);
		
    }
	
	
	

	
	function vistaauto($id)
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	  
	    $in='interior';$ex='exterior';
 
        $data['foto_in'] = $this->Inventariomodel->getFotoinex($id,$in);
        $data['foto_ex'] = $this->Inventariomodel->getFotoinex($id,$ex);
        $data['uno_inventario'] = $this->Inventariomodel->getAutoid($id);
		
        $this->load->view('inventario/vistaauto', $data);
 
    }
	
	
		function updatetodo($id)
    {   
	
	session_start();if ( !isset($_SESSION['username']) ) {redirect('login');}
	$this->load->library('form_validation');
 
     
         $this->form_validation->set_rules('sucursal', 'Sucursal', 'required');
		$this->form_validation->set_rules('noinventario', 'Numero de Inventario', 'required');
		$this->form_validation->set_rules('ubicacion', 'Ubicacion', 'required');
		$this->form_validation->set_rules('almacen', 'Almacen', 'required');
		$this->form_validation->set_rules('llegada', 'Fecha de llegada', 'required');
		$this->form_validation->set_rules('vin', 'VIN', 'required');
		$this->form_validation->set_rules('ano', 'Año', 'required');
		$this->form_validation->set_rules('modelo', 'Modelo', 'required');
		$this->form_validation->set_rules('color', 'Color', 'required');
		$this->form_validation->set_rules('tunidad', 'Tipo de unidad', 'required');
		
		
       
 
 
        if ($this->form_validation->run())
        {
			$llegada=$this->input->post('llegada');
			list($mes,$dia,$ano)=explode('/',$llegada);
			$fecha=$ano.'-'.$mes.'-'.$dia;
			
			$vin=$this->input->post('vin');
			mkdir('./upload/'.$vin,0777);
	
         $todo = array(
                    'aut_sucursal'=>$this->input->post('sucursal'),
					'aut_no_inventario'=>$this->input->post('noinventario'),
					'aut_ano'=>$this->input->post('ano'),
					'aut_vin'=>$this->input->post('vin'),
					'aut_motor'=>$this->input->post('motor'),
					'aut_tipo_unidad'=>$this->input->post('tunidad'),
                    'aut_ubicacion'=>$this->input->post('ubicacion'),
					'aut_importe'=>$this->input->post('importe'),
					'aut_iva'=>$this->input->post('iva'),
					'aut_llegada'=>$fecha,
				    'version_ver_IDversion'=>'0',
					'modelos_mod_IDmodelos'=>$this->input->post('modelo'),
					'almacen_alm_IDalmacen'=>$this->input->post('almacen'),
					'color_col_IDcolor'=>$this->input->post('color'),
					'fichatecnica_fte_IDfichatecnica'=>$this->input->post('ftecnica')
					
                    );
				
             $this->Inventariomodel->updatetodo($todo,$id);
		     
			 
			 
		       $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la infomacion con exito.</div>");            
            redirect('inventario');
        }
        else
        {
        $data['uno_inventario'] = $this->Inventariomodel->getAuto($id);
		$data['todo_almacen'] = $this->Inventariomodel->getAlmacen();
	    $data['todo_modelo'] = $this->Inventariomodel->getModelo();
		$data['todo_color'] = $this->Inventariomodel->getColor();
        $data['todo_ficha'] = $this->Inventariomodel->getFicha();
        $this->load->view('inventario/edit', $data);
        }
    }
	
	
	function eliminarAsignado()
	{
		$ts=$this->input->post('idasig');
		foreach($ts as $bb){
			$this->Inventariomodel->delete($bb);
			}
		
 $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado Autos del Inventario Asigando!. </div>");
 redirect('inventario');
}
	function delete($id)
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	  

	  
        $data= $this->Inventariomodel->get($id);
 
        if ($this->Inventariomodel->delete($id)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado a:  $data->das_modelo. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
       redirect('inventario');
 
    }
	
	
	 function pictures($id)
    {
		session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	  
	   $data['flash_message'] = $this->session->flashdata('message');


$in='interior';
$ex='exterior';
 $data['uno_inventario'] = $this->Inventariomodel->getAuto($id);
 $data['foto_in'] = $this->Inventariomodel->getFotoinex($id,$in);
 $data['foto_ex'] = $this->Inventariomodel->getFotoinex($id,$ex);
 
 $this->load->view('inventario/picture', $data);
       
 
    }
	
	function doUpload($id)
{

session_start();
		if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }

	  
$data['flash_message'] = $this->session->flashdata('message');	  

$this->load->library('form_validation');
$this->form_validation->set_rules('direccion', 'Direccion', 'required');

 if ($this->form_validation->run())
        {

$direccion=$this->input->post('direccion');

$tmp_new_name = $_FILES["userfile"]["name"];
$path_parts = pathinfo($tmp_new_name);
$new_name =time() .'.'. $path_parts['extension'];
 		 
$config['upload_path'] = './upload/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['file_name'] =$new_name;
$config['max_size'] = '10000';
$config['max_width'] = '1920';
$config['max_height'] = '1280';

$nombre=$new_name;
$this->load->library('upload', $config);

if(!$this->upload->do_upload())
{
echo $this->upload->display_errors();
}
else {

 
	
	
if($direccion=='perfil'){
$todo = array('aut_foto'=>$nombre);	
$this->Inventariomodel->updatefoto($todo,$id);
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una foto de perfil con exito.</div>");            
 redirect('inventario/pictures/'.$id.'');
	
}

if($direccion=='interior'){
$todo = array('fat_nombre'=>$nombre,'fat_seccion'=>$direccion,'auto_aut_IDauto_vin'=>$id);
$this->Inventariomodel->insertfotoseccion($todo);
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una foto Interior con exito.</div>");            
 redirect('inventario/pictures/'.$id.'');
	
}

if($direccion=='exterior'){
$todo = array('fat_nombre'=>$nombre,'fat_seccion'=>$direccion,'auto_aut_IDauto_vin'=>$id);
$this->Inventariomodel->insertfotoseccion($todo);
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una foto de Exterior con exito.</div>");            
 redirect('inventario/pictures/'.$id.'');
	
}







}
		}

else {

 $in='interior';
 $ex='exterior';
 $data['uno_inventario'] = $this->Inventariomodel->getAuto($id);
 $data['foto_in'] = $this->Inventariomodel->getFotoinex($id,$in);
 $data['foto_ex'] = $this->Inventariomodel->getFotoinex($id,$ex);
 
 $this->load->view('inventario/picture', $data);

}


} 
	
	
	

function deletepicture($id,$idf,$foto)
{



 
 unlink(realpath('./upload/'.$foto.'')); 
 
        if ($this->Inventariomodel->deletefoto($idf)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado a:  $foto. </div>");

                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
			 
        }
    
 redirect('inventario/pictures/'.$id.'');

}
	
	
	
	function asignarauto()
    {
		
		session_start();
		if ( !isset($_SESSION['username']) ) { redirect('login');}
		$data['flash_message'] = $this->session->flashdata('message');
	
			$data['todo_apartados'] = $this->Inventariomodel->getTodoApartados();
			$this->load->view('inventario/asignarauto',$data);
      
}



function updateasig($id)
    {
		
		session_start();
		if ( !isset($_SESSION['username']) ) { redirect('login');}
		$data['flash_message'] = $this->session->flashdata('message');
	
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('vendedor', 'Vendedor', 'required');
		$this->form_validation->set_rules('auto', 'Auto', 'required');
		
        if ($this->form_validation->run())
        {
			
			
		
			$todo = array(
					'aau_pago'=>$this->input->post('pago'),
					'huser_hus_IDhuser'=>$this->input->post('vendedor'),
					'auto_aut_IDauto_vin'=>$this->input->post('auto')
                    );
				

						
            $this->Inventariomodel->updateasigancion($todo,$id);
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Actualizado la informacion.</div>");            
            redirect('inventario/asignarauto');
        }
        else
        {
			 $this->load->model('Vendedoresmodel');
			 $this->load->model('Contactomodel');
			  $data['idg'] = $id;
             $data['todo_vendedor'] = $this->Vendedoresmodel->getTodoa();
			  $data['todo_contacto'] = $this->Contactomodel->getTodocontactoax();
			 $data['uno_apartado'] = $this->Inventariomodel->getApartado($id);
			$this->load->view('inventario/editasignarauto',$data);
        }
}

	
	
	function deleteasig($idas,$idac,$idcar,$idcon)
{
 $this->load->model('Contactomodel');
$this->Inventariomodel->deleteasig($idas);
//$this->Inventariomodel->deleteAct($idac);
$this->Inventariomodel->UpdateASp($idcar);
//$this->Inventariomodel->UpdateCOn($idcon);

 if($idcon==12219 || $idcon==12220 || $idcon==12223 || $idcon==13760 || $idcon==13805 || $idcon==13806){}
			 else{
$IDB=$this->Contactomodel->idBitacora($idcon);

$this->Inventariomodel->insertAct($IDB->bit_IDbitacora);
			 }


 
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una asignacion. </div>");
  
 redirect('inventario/asignarauto/');

}
function deleteasigintel($idas,$idac,$idcon)
{
 $this->load->model('Contactomodel');
$this->Inventariomodel->deleteasig($idas);
if($idcon==12219 || $idcon==12220 || $idcon==12223 || $idcon==13760 || $idcon==13805 || $idcon==13806){}else{
$this->Inventariomodel->deleteAct($idac);
}
//$this->Inventariomodel->UpdateASp($idcar);
$this->Inventariomodel->UpdateCOn($idcon);
$IDB=$this->Contactomodel->idBitacora($idcon);
if($idcon==12219 || $idcon==12220 || $idcon==12223 || $idcon==13760 || $idcon==13805 || $idcon==13806){}else{
$this->Inventariomodel->insertAct($IDB->bit_IDbitacora);
}
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado una asignacion. </div>");
  
 redirect('inventario/asignarauto/');

}




function busquedaAutos(){
session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}	
$data['lista_Autos'] = $this->Inventariomodel->getBusquedaAuto();		
$this->load->view('inventario/busquedaAutos',$data);
 			
			}

	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */


