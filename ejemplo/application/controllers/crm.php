<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crm extends CI_Controller {

   function __construct()
   {
      session_start();
      parent::__construct();
      if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
	     $this->load->helper('url');
		 $this->load->model('Homemodel');
		 $this->load->model('Contactomodel');
   }
	
	public function index()
	{
	$this->load->library("curl");
	$prsp='Prospecto';
	$cali='Caliente';
	$proc='Proceso de Venta';
	$vent='Caida';
	$ciud=$_SESSION['ciudad'];
	$idUs=$_SESSION['id'];
	
	if($_SESSION['nivel']=='Administrador')
	    {
		$data['prospectos'] = $this->Homemodel->ContactosAdmin($ciud,$prsp);
		$data['calientes'] = $this->Homemodel->ContactosAdmin($ciud,$cali);
		$data['proceso'] = $this->Homemodel->ContactosAdmin($ciud,$proc);
		$data['caidas'] = $this->Homemodel->ContactosAdmin($ciud,$vent);
		$data['tipcon']='tipocontactoadmin';
		$data['todo_tareas_admin'] = $this->Homemodel->getTareasA($ciud);
		
		}
	else{
		$data['prospectos'] = $this->Homemodel->ContactosUser($idUs,$prsp);
		$data['calientes'] = $this->Homemodel->ContactosUser($idUs,$cali);
		$data['proceso'] = $this->Homemodel->ContactosUser($idUs,$proc);
		$data['caidas'] = $this->Homemodel->ContactosUser($idUs,$vent);
		$data['tipcon']='tipocontacto';
		$data['todo_tareas_admin'] = $this->Homemodel->getTareas($idUs);
		
		}
		$data['arrmes']=array("01"=>"Enero","02"=>"Febrero","03"=>"Marzo","04"=>"Abril","05"=>"Mayo","06"=>"Junio","07"=>"Julio","08"=>"Agosto","09"=>"Septiembre","10"=>"Octubre","11"=>"Noviembre","12"=>"Diciembre");
		
	$this->load->view('crm',$data);
	}
	
	
	
	
	function ciudadotra($id){

	if($id=='Tijuana'){$_SESSION['ciudad'];  unset($_SESSION['ciudad']); $_SESSION['ciudad'] ='Tijuana';}
	if($id=='Mexicali'){$_SESSION['ciudad'];  unset($_SESSION['ciudad']); $_SESSION['ciudad'] ='Mexicali';}
	
	redirect('crm');
	
}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
