<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
       $this->load->model('Eventosmodel');

session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}
        $this->load->helper('url');
		$this->load->library('email');
        $this->load->library('session');
	
    }
 
    function index()
    {
		$data['flash_message'] = $this->session->flashdata('message');
		
		if($_SESSION['nivel']=='Administrador'){
		$data['todo_eventos'] =$this->Eventosmodel->getEventos($_SESSION['ciudad']);
		}
		else{
		$data['todo_eventos'] =$this->Eventosmodel->getEventosV($_SESSION['id']);	
			}
		 $this->load->view('eventos/show', $data);
    }
	
	
	
		
	function add()
    {   
	
	 $this->load->library('form_validation');
     $this->form_validation->set_rules('titulo', 'Titulo', 'required');
	
		if ($this->form_validation->run())
        {
$fecha=$this->Eventosmodel->listfecha($this->input->post('ini'));
$fechab=$this->Eventosmodel->listfecha($this->input->post('fin'));
$nombre='evento.jpg';
$nombre2='evento.jpg';
$this->load->library('upload');
// this is for form field 1 which is an image....
if($_FILES["file_1"]["name"]){
$tmp_new_name = $_FILES["file_1"]["name"];
$path_parts = pathinfo($tmp_new_name);
$new_name =time() .'.'. $path_parts['extension'];
 		 
$config['upload_path'] = './upload/eventos/';
$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
$config['file_name'] =$new_name;
$config['max_size'] = '1000000';
$config['max_width'] = '2000';
$config['max_height'] = '2000';
$this->upload->initialize($config); 
$this->upload->do_upload('file_1');
		}
// this is for form field 2 which is a pdf
if($_FILES["file_2"]["name"]){
$tmp_new_name = $_FILES["file_2"]["name"];
$path_parts = pathinfo($tmp_new_name);
$new_name2 =time() .'.'. $path_parts['extension'];
 		 
$config['upload_path'] = './upload/eventos/';
$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
$config['file_name'] =$new_name2;
$config['max_size'] = '1000000';
$config['max_width'] = '2000';
$config['max_height'] = '2000';
$this->upload->initialize($config); 
$this->upload->do_upload('file_2');
}
if($_FILES["file_1"]["name"]){$nombre1=$new_name;}else{$nombre1='evento.jpg';}
if($_FILES["file_2"]["name"]){$nombre2=$new_name2;}else{$nombre2='evento.jpg';}
   
   
   

   
        $todo = array(
                    'eve_titulo'=>$this->input->post('titulo'),
					'eve_fecha_inicio'=>$fecha,
					'eve_fecha_fin'=>$fechab,
					'eve_descripcion'=>$this->input->post('desc'),
					'eve_foto'=>$nombre,
					'eve_fotoDos'=>$nombre2,
					'eve_status'=>$this->input->post('status')
                    );
				
           $EID=$this->Eventosmodel->addEventos($todo);
		   $this->Eventosmodel->addNotificaciones($EID->EID);
		   
		  
		  
		  //lista de contactos 
		  	  if($_SESSION['username']=='josemaciel@hondaoptima.com'){
		  $arrayasesores=$this->Eventosmodel->asesoresAll();
		  }else{
		  $arrayasesores=$this->Eventosmodel->asesores();}
		  
	
		 
		foreach($arrayasesores as $row){
		$correo=$row->hus_correo;
		$celular=$row->hus_celular;
		
		//enviar email
		$this->Eventosmodel->emailasesores($correo,$this->input->post('titulo'),$this->input->post('desc'),$fecha,$nombre,$nombre2);
		//enviar mensaje de texto.
		//$this->Eventosmodel->smsasesores($celular,$this->input->post('titulo'),$fecha);
		
		} 
	

		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Evento agregado con exito.</div>");            
            redirect('eventos');
        }
        else
        {
            $this->load->view('eventos/add');
        }
		 
	
    }
	
	
	
	
	
	function update($id)
    {   
$this->load->library('form_validation');
$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		
if ($this->form_validation->run())
        {
$fecha=$this->Eventosmodel->listfecha($this->input->post('ini'));
$fechab=$this->Eventosmodel->listfecha($this->input->post('fin'));
		
if($_FILES["userfile"]["name"]){
$tmp_new_name = $_FILES["userfile"]["name"];
$path_parts = pathinfo($tmp_new_name);
$new_name =time() .'.'. $path_parts['extension'];
 		 
$config['upload_path'] = './upload/eventos/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['file_name'] =$new_name;
$config['max_size'] = '10000';
$config['max_width'] = '1920';
$config['max_height'] = '1280';

$nombre=$new_name;
$this->load->library('upload', $config);
}else{}
		
		if($_FILES["userfile"]["name"]){
                $todo = array(
                    'eve_titulo'=>$this->input->post('titulo'),
					'eve_fecha_inicio'=>$fecha,
					'eve_fecha_fin'=>$fechab,
					'eve_descripcion'=>$this->input->post('desc'),
					'eve_foto'=>$nombre,
					'eve_status'=>$this->input->post('status')
                    );
				}else{$todo = array(
                    'eve_titulo'=>$this->input->post('titulo'),
					'eve_fecha_inicio'=>$fecha,
					'eve_fecha_fin'=>$fechab,
					'eve_descripcion'=>$this->input->post('desc'),
					'eve_status'=>$this->input->post('status')
                    );}

						
           $this->Eventosmodel->updateEventos($todo,$id);
		   
		   
		   if($_FILES["userfile"]["name"]){
		   if(!$this->upload->do_upload()){echo $this->upload->display_errors();}
		   }else{}
		   
		  
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Informaci&oacute;n actualizada.</div>");            
            redirect('eventos');
        }
        else
        {
			$data['uno_eventos'] =$this->Eventosmodel->getEventosUno($id);
            $this->load->view('eventos/edit',$data);
        }
		 
	
    }
	
	
	
		function delete($id)
    {
		
        if ($this->Eventosmodel->delete($id)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado un Evento. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('eventos');
 
    }
	
	
	
		function vistaevento($id)
    {   
	$this->Eventosmodel->openEvento($id,$_SESSION['id']);
			$data['uno_eventos'] =$this->Eventosmodel->getEventosUno($id);
            $this->load->view('eventos/vistaevento',$data);
     }
		 

	
	
	
	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/contacto.php */


