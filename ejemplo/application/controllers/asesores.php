<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asesores extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vendedoresmodel');
		$this->load->model('Contactomodel');
		 // load url helper
        $this->load->helper('url');
		$this->load->library('email');
		
		// load session librarysferi
        $this->load->library('session');
		session_start();
		if ( !isset($_SESSION['username']) ) {redirect('login');}
    }
 
    function index()
    {
		
		
		$data['flash_message'] = $this->session->flashdata('message');
		
        $data['todo_vendedores'] = $this->Vendedoresmodel->getTodo($_SESSION['ciudad']);
        
        $this->load->view('asesores/show', $data);
    }
	
	  function citas()
    {
		
		
		$data['flash_message'] = $this->session->flashdata('message');
		
        $data['todo_vendedores'] = $this->Vendedoresmodel->citashoy($_SESSION['ciudad']);
        
        $this->load->view('asesores/citas', $data);
    }
	
	
	 function viewbaja($idh,$cd)
    {
		
		
		$data['flash_message'] = $this->session->flashdata('message');
		
		$data['uno_vendedor'] = $this->Vendedoresmodel->getId($idh);
        $data['ncontactos'] = $this->Vendedoresmodel->getContactosHuser($idh);
		$data['nventas'] = $this->Vendedoresmodel->getVentasHuser($idh,$cd);
        
        $this->load->view('asesores/viewbaja', $data);
    }
	
	 function baja($idh,$cd)
    {
	 $contactos = $this->Vendedoresmodel->getContactosHuserNombre($idh);
	 $ventas= $this->Vendedoresmodel->getVentasHuserNombre($idh,$cd);
	 if($cd=='Tijuana'){$gerente=28;}
	 if($cd=='Mexicali'){$gerente=29;}
	 if($cd=='Ensenada'){$gerente=510;}
	 
	
	
	 
	 foreach($contactos as $con){
	 $todo = array(
                    'ct_fecha'=>''.date('Y-m-d H:i:s').'',
					'ct_huser_before'=>''.$idh.'',
					'ct_huser_now'=>''.$gerente.'',
					'ct_IDcontacto'=>''.$con->con_IDcontacto.'',
                    );
	  $this->Vendedoresmodel->addBaja($todo);
	  $this->Contactomodel->transfer($con->con_IDcontacto,$gerente);
	  $this->Vendedoresmodel->delete($idh);
		}
	
	  $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Contactos Transferidos al Gerente de Ventas de ".$cd." y Ventas Transferidas a Casa </div>");
	redirect('asesores');

	
    }
	
	
	function add()
    {   
	
        $this->load->library('form_validation');
 
      
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
		$this->form_validation->set_rules('telefono', 'Telefono', 'required');
		$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('antiguedad', 'Antiguedad', 'required');
		$this->form_validation->set_rules('nivel', 'Nivel', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('contrasena', 'Contrasena', 'required');
		$this->form_validation->set_rules('ciudad', 'Ciudad', 'required');
		
		
		
 
        if ($this->form_validation->run())
        {
			//$date=$this->input->post('antiguedad');
			//list($mes,$dia,$ano)=explode('/',$date);
			$fecha=date('Y').'-'.date('m').'-'.date('d');
			
			$toemail=$this->input->post('correo');
			$nombre=$this->input->post('nombre');
			$apellido=$this->input->post('apellido');
			$correo=$this->input->post('correo');
			$con=$this->input->post('contrasena');
			
        $todo = array(
                    'hus_nombre'=>$this->input->post('nombre'),
					'hus_apellido'=>$this->input->post('apellido'),
					'hus_telefono'=>$this->input->post('telefono'),
					'hus_correo'=>$this->input->post('correo'),
					'hus_antiguedad'=>"$fecha",
					'hus_nivel'=>$this->input->post('nivel'),
                    'hus_status'=>$this->input->post('status'),
					'hus_radio_honda'=>$this->input->post('radiohonda'),
					'hus_radio_personal'=>$this->input->post('radiopersonal'),
					'hus_celular'=>$this->input->post('celular'),
					'hus_contrasena'=>md5($this->input->post('contrasena')),
					'hus_ciudad'=>$this->input->post('ciudad'),
					'hus_foto'=>'usuarios.jpg'
                    );
				

						
           $this->Vendedoresmodel->addVendedores($todo);
		   
		    $this->load->library('email');
			$this->email->set_mailtype("html");
		    $this->email->from('casa@hondaoptima.com', 'Honda Optima');
            $this->email->to($toemail);
            $this->email->subject('Bienvenido a CRM Honda');
            $this->email->message('
			<table><tr><td>
		
			</td></tr></table>
			<br>
			<table><tr><td>Bienvenido:</td><td>'.$nombre.' '.$apellido.'</td></tr></table>
			<br>
			<table><tr><td>Le damos la mas cordial bienvenido al equipo de venta Honda optima.</td></tr></table>
		    <table><tr><td>Usted podr&aacute; ingresar a este sistema en l&iacute;nea, siguiendo la direcci&oacute;n y los datos que <br>se muestran a continuaci&oacute;n:</td></tr></table>
			<br><br>
			<table><tr><td><b>Direcci&oacute;n:</b></td><td>
			</td>
			</tr>
			<tr><td><a href="hondaoptima.com/ventas/">www.hondaoptima.com/ventas/</a></td></tr>
			<tr><td><b>Correo electr&oacute;nico:</b></td><td>
			'.$correo.'</td>
			</tr>
			<tr><td><b>Contrase&ntilde;a:</b></td><td>
			'.$con.'</td>
			</tr>
			
			</table>
			
			<br>
			<br>
			<hr/>
			<table><tr><td>
			
			</td></tr></table>
			
			');
			
            $this->email->send();
		   
 
            $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado nuevo Vendedor.</div>");            
            redirect('asesores');
        }
        else
        {
            $this->load->view('asesores/add');
        }
		 
	
    }
	
	
	
	function updatetodo($id)
    {   
	
	
	$this->load->library('form_validation');
 
     
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
	
		$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('antiguedad', 'Antiguedad', 'required');
		$this->form_validation->set_rules('nivel', 'Nivel', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('ciudad', 'Ciudad', 'required');
		if ($this->form_validation->run())
        {
			$date=$this->input->post('antiguedad');
			list($mes,$dia,$ano)=explode('/',$date);
			$fecha=$ano.'-'.$mes.'-'.$dia;
			
			
	
        $todo = array(
                    'hus_nombre'=>$this->input->post('nombre'),
					'hus_apellido'=>$this->input->post('apellido'),
					'hus_telefono'=>$this->input->post('telefono'),
					'hus_correo'=>$this->input->post('correo'),
					'hus_antiguedad'=>"$fecha",
					'hus_nivel'=>$this->input->post('nivel'),
                    'hus_status'=>$this->input->post('status'),
					'hus_radio_honda'=>$this->input->post('radiohonda'),
					'hus_radio_personal'=>$this->input->post('radiopersonal'),
					'hus_celular'=>$this->input->post('celular'),
					
					'hus_ciudad'=>$this->input->post('ciudad')
                    );
				
             $this->Vendedoresmodel->updatetodo($todo,$id);
		   
		       $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la infomacion con exito.</div>");            
            redirect('asesores');
        }
        else
        {
            $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
        
            $this->load->view('asesores/editar', $data);
        }
    }
	
	
	
	
	
	
	function updatetodoperfil($id)
    {   
	
     $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', '');
		$this->form_validation->set_rules('apellido', 'Apellido', '');
		$this->form_validation->set_rules('telefono', 'Telefono', '');
		$this->form_validation->set_rules('correo', 'Correo', '');
		$this->form_validation->set_rules('radiohonda', 'Radiohonda', '');
		$this->form_validation->set_rules('radiopersonal', 'Radiopersonal', '');
		$this->form_validation->set_rules('celular', 'Celular', '');
		
		
		
		
       
 
 
        if ($this->form_validation->run())
        {
			
			
			
	
        $todo = array(
                    'hus_nombre'=>$this->input->post('nombre'),
					'hus_apellido'=>$this->input->post('apellido'),
					'hus_telefono'=>$this->input->post('telefono'),
					'hus_correo'=>$this->input->post('correo'),
				    'hus_radio_honda'=>$this->input->post('radiohonda'),
					'hus_radio_personal'=>$this->input->post('radiopersonal'),
					'hus_celular'=>$this->input->post('celular')
					
					
                    );
				
             $this->Vendedoresmodel->updatetodo($todo,$id);
		   
		       $this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la infomacion con exito.</div>");  
          
           redirect('asesores/configuracion/'.$id.'');
        }
        else
        {
            $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
        
            redirect('asesores/configuracion/'.$id.'');
        }
    }
	
	
	
	
	
	
	
		
	
	
	 function configuracion($id)
    {
		
	  
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	  
	   $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
        
       $this->load->view('asesores/configuracion', $data);
 
    }
	
	
	 function editvendedor($id)
    {
		
	  
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	  
	   $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
        
       $this->load->view('asesores/editvendedor', $data);
 
    }
	
	
	function changepass($id)
   {
	
	 
	 
	 


      $this->load->library('form_validation');
      $this->form_validation->set_rules('password', 'Contraseña, ', 'required|min_length[4]');
      $this->form_validation->set_rules('npassword', 'Nueva Contraseña, ', 'required|min_length[4]');

      if ( $this->form_validation->run() !== false ) {
         // then validation passed. Get from db
        $npass=$this->input->post('npassword');
		 $this->load->model('Vendedoresmodel');
			 $res= $this->Vendedoresmodel->change_pass($this->input->post('password'),$this->input->post('npassword'),$id);

         if ( $res !== false ) {
			 
			 
			$this->load->library('email');
			$this->email->set_mailtype("html");
		    $this->email->from('casa@hondaoptima.com', 'Honda Optima');
            $this->email->to($_SESSION['username']);
            $this->email->subject('Cambio de Contraseña | CRM Honda');
            $this->email->message('
			<table><tr><td>
			</td></tr></table>
			<br>
			<br>
			<table><tr><td>Su nueva contraseña es:</td><td>'.$npass.'</td></tr></table>
			<br>
			
			
			
			<br>
			<br>
			<hr/>
			<table><tr><td>
			</td></tr></table>
			
			');
			
            $this->email->send();
			 
			 
			 
			 
			 
			 
			
			 
			 
			  $this->session->set_flashdata('message', '<div class="alert alert-success">
<button class="close" data-dismiss="alert" type="button">×</button>La  contraseña fue cambiada correctamente.</div>');   
             $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
      redirect('asesores/configuracion/'.$id);
			 
           
         }

      }
 $this->session->set_flashdata('message', '<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>La  contraseña introducida no es correcta.</div>');   
$data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
  redirect('asesores/configuracion/'.$id);
	  
	  
   }
	
	
	
	
	
	
	function transferir()
    {
		
	  ini_set('memory_limit', '256M');
	  $data['flash_message'] = $this->session->flashdata('message');
	   $this->load->model('vendedoresmodel');
	   $this->load->model('Contactomodel');
         $data['todo_vendedores'] = $this->Vendedoresmodel->getTodosel();
		//$data['todo_contacto'] = $this->Contactomodel->getTodocontactoav($_SESSION['ciudad']);
       $this->load->view('asesores/transferir',$data);
 
    }
	
	
	function transfer()
	{
		
		$ts=$this->input->post('idc');
		$id=$this->input->post('vendedor');
		 $this->load->model('Contactomodel');
		foreach($ts as $bb){
			$this->Contactomodel->transfer($bb,$id);
			}
		
		 $this->session->set_flashdata('message', "<br><div class='alert alert-info'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha transferido contactos con exito ! </div>");
redirect('asesores/transferir');
}
	
	
	
	
	 function delete($id)
    {
	
	  
	  
        $data= $this->Vendedoresmodel->get($id);
 
        if ($this->Vendedoresmodel->delete($id)) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha eliminado a  $data->hus_nombre. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('asesores');
 
    }
	
	
	 function update($id)
    {
		
	  
        $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
        
        $this->load->view('asesores/edit', $data);
 
    }
	
	
	 function picture($id)
    {
		
	  $data['flash_message'] = $this->session->flashdata('message');
	  
	   
        $data['uno_vendedor'] = $this->Vendedoresmodel->getId($id);
        
        $this->load->view('asesores/picture', $data);
 
    }
	
	
	function doUpload($id)
{

$data['flash_message'] = $this->session->flashdata('message');	  
$tmp_new_name = $_FILES["userfile"]["name"];
$path_parts = pathinfo($tmp_new_name);
$new_name =time() .'.'. $path_parts['extension'];
 		 
$config['upload_path'] = './upload/usuarios/';
$config['allowed_types'] = 'gif|jpg|jpeg|png';
$config['file_name'] =$new_name;
$config['max_size'] = '10000';
$config['max_width'] = '1920';
$config['max_height'] = '1280';
$nombre=$new_name;
$this->load->library('upload', $config);
if(!$this->upload->do_upload()){echo $this->upload->display_errors();}
else {
$todo = array('hus_foto'=>$nombre);	
$this->Vendedoresmodel->updatefoto($todo,$id);
$this->session->set_flashdata('message', " <br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha Agregado una foto de perfil con exito.</div>");            
 redirect('asesores/configuracion/'.$id.'');
}} 
	
	
	
	
 
    function complete($id)
    {
       $data= $this->Vendedoresmodel->get($id);
 
        if ($this->Vendedoresmodel->setComplete($id)) {
            $this->session->set_flashdata('message', "Ha configurado correctamente a $data->hus_nombre .");                        
        } else {
            $this->session->set_flashdata('message', "No data found. You access wrong to do list.");  
        }
        redirect('vendedores');
    }
	
	
	
	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */


