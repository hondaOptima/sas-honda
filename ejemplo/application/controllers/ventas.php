<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crm extends CI_Controller {

   function __construct()
   {
      session_start();
      parent::__construct();
      if ( !isset($_SESSION['username']) ) {
         redirect('admin');
      }
	     $this->load->helper('url');
		 $this->load->model('Homemodel');
		 /*$this->load->model('Contactomodel');
		 $this->load->model('Vendedoresmodel');
		 $this->load->model('Inventariomodel'); */
   }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	$this->load->library("curl");
	
	
 
	
	if($_SESSION['nivel']=='Administrador')
	{$data['noticias'] = $this->Homemodel->getNoticiasA();}
	else{$data['noticias'] = $this->Homemodel->getNoticias($_SESSION['id']);}
	
	if($_SESSION['nivel']=='Administrador')
	{$data['generales'] = $this->Homemodel->getGeneralesA();}
	else{$data['generales'] = $this->Homemodel->getGenerales($_SESSION['id']);}
	
	if($_SESSION['nivel']=='Administrador')
	{
	
	$data['contactos'] = $this->Homemodel->getContactosA($_SESSION['ciudad']);
		
	}
	else{$data['contactos'] = $this->Homemodel->getContactos($_SESSION['id']);}
	
	
	if($_SESSION['nivel']=='Administrador')
	{$data['todo_tareas_admin'] = $this->Homemodel->getTareasA($_SESSION['ciudad']);}
	else{$data['todo_tareas_vendedor'] = $this->Homemodel->getTareas($_SESSION['id']);}
	
	if($_SESSION['nivel']=='Administrador')
	{$data['ventas'] = $this->Homemodel->getVentaA($_SESSION['ciudad']);}
	else{$data['ventas'] = $this->Homemodel->getVentas($_SESSION['id']);}
	

	
	//$data['generales'] = $this->Homemodel->getGenerales($_SESSION['id']);
	//$data['contactos'] = $this->Homemodel->getContactos($_SESSION['id']);
	//$data['ventas'] = $this->Homemodel->getVentas($_SESSION['id']);
	


	$this->load->view('crm',$data);
	}
	
	function ciudadotra($id){

	if($id=='Tijuana'){$_SESSION['ciudad'];  unset($_SESSION['ciudad']); $_SESSION['ciudad'] ='Tijuana';}
	if($id=='Mexicali'){$_SESSION['ciudad'];  unset($_SESSION['ciudad']); $_SESSION['ciudad'] ='Mexicali';}
	
	redirect('crm');
	
}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
