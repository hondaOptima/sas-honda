<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hpp extends CI_Controller {

 public  function __construct()
   {
   	parent::__construct();
   	 $this->load->model('Hppmodel');
	 $this->load->model('Reportesmodel');
	  $this->load->library('session');
      session_start();
      
      if ( !isset($_SESSION['username']) ) {
         redirect('login');
      }
   }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	
	 */
	public function index()
	{
			$data['flash_message'] = $this->session->flashdata('message');

		function readCSV($csvFile){
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {
		$line_of_text[] = fgetcsv($file_handle, 1024);
	}
	fclose($file_handle);
	return $line_of_text;
}


$csvFile =base_url().'intellisis/uno.csv';
$csv = readCSV($csvFile);
$data['todo_piso'] = $csv;

$this->load->view('hpp/intellisis',$data);
		
	}
	
	/*public function index()
	{
		$this->load->view('hpp/pdf');
		
	}*/
	
	function guiaReferidos()
	{
	$this->load->view('hpp/guia-referidos');
	}
	
	
	
	function llamadaentrante()
    {
    	   
	      $todo = array(
                    'lle_fecha'=>date('Y').'-'.date('m').'-'.date('d'),
					'lle_hora'=>date('H:i'),
					'lle_nombre_cliente'=>$this->input->post('cliente'),
					'lle_telefono'=>$this->input->post('telefono'),
					'lle_tipo_llamada'=>$this->input->post('grupoll'),
					'lle_asesor_deventa'=>$this->input->post('asesor'),
					'lle_nota'=>$this->input->post('nota'),
					'lle_autointeres'=>$this->input->post('autointeres'),
					'lle_status'=>'pendiente',
					'lle_cliente_interes'=>$this->input->post('lle_cliente_interes'),
					'lle_contacto'=>'',
					'lle_correo'=>$this->input->post('lle_correo'),
                    );
					
      
	  
      $this->Hppmodel->insertLlamada($todo);
	  
	  echo'exito';



    }


function llamada()
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	
	$this->load->model('Contactomodel');
	$this->load->model('Vendedoresmodel');
	$this->load->library('form_validation');
		
		$this->form_validation->set_rules('asesor', 'Asesor', 'required');
        $this->form_validation->set_rules('autointeres', 'Nombre', 'required');
		$this->form_validation->set_rules('cliente', 'Cliente', 'required'); 
	 
	if($this->form_validation->run()){
    $DC=$this->input->post('tipollamada');	
	if($DC=='nuevo'){$status='pendiente';$con='';}else{$status='atendida';$con=$DC;}
	
	$todo = array(
                    'lle_fecha'=>date('Y').'-'.date('m').'-'.date('d'),
					'lle_hora'=>date('H:i'),
					'lle_nombre_cliente'=>$this->input->post('cliente'),
					'lle_telefono'=>$this->input->post('telefono'),
					'lle_tipo_llamada'=>$this->input->post('grupoll'),
					'lle_asesor_deventa'=>$this->input->post('asesor'),
					'lle_nota'=>$this->input->post('nota'),
					'lle_autointeres'=>$this->input->post('autointeres'),
					'lle_status'=>$status,
					'lle_cliente_interes'=>$this->input->post('lle_cliente_interes'),
					'lle_contacto'=>$con,
					'lle_correo'=>$this->input->post('lle_correo'),
                    );	
		
    $this->Hppmodel->insertLlamada($todo);

		
		
		
		
		
		
		
		$this->session->set_flashdata('message', " <br><div class='alert alert-success'><button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha agregado una nueva llamada entrante.</div>");            
            redirect('hpp/llamada/');
		}
	else{
		$data['asesores_de_venta'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
	$this->load->view('hpp/create-llamada',$data);
		}
	
}

function listaguiatelefonica()
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	/*
	$this->load->model('Contactomodel');
	$this->load->model('Vendedoresmodel');
	$this->load->library('form_validation');
		
		$this->form_validation->set_rules('asesor', 'Asesor', 'required');
        $this->form_validation->set_rules('autointeres', 'Nombre', 'required');
		$this->form_validation->set_rules('cliente', 'Cliente', 'required'); 
	 
	if($this->form_validation->run()){

		}
	else{
		$data['asesores_de_venta'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
	$this->load->view('hpp/guia-telefonica',$data);
		}
		*/
		$this->load->view('hpp/lista-guia-telefonica',$data);
	
}

function guiatelefonica()
    {   
	$data['flash_message'] = $this->session->flashdata('message');

	$this->load->model('Contactomodel');
	$this->load->model('Vendedoresmodel');
	$this->load->library('form_validation');
		
		$this->form_validation->set_rules('asesor', 'Asesor', 'required');
        $this->form_validation->set_rules('autointeres', 'Nombre', 'required');
		$this->form_validation->set_rules('cliente', 'Cliente', 'required'); 
	 
	if($this->form_validation->run()){

		}
	else{
		$data['asesores_de_venta'] = $this->Vendedoresmodel->getTodoaCiudad($_SESSION['ciudad']);
	$this->load->view('hpp/guia-telefonica',$data);
		}
		
	
}

function buscarcontacto()
{
$name=$_POST['valor'];		
$this->load->model('Contactomodel');
$arr=$lpro[] = $this->Contactomodel->getTodocontactoahpp($_SESSION['ciudad'],$name);
echo '<table class=" table-striped table-bordered bootstrap-datatable ">
						  <thead>
							  <tr>
								  <th>Contacto</th>
								  <th width="30px;">Correo Electr&oacute;nico</th>
								  <th>Tel&eacute;fono</th>
								  <th>Acci&oacute;n</th>
							  </tr>
						  </thead>   
						  <tbody>';
foreach($arr as $tod)
	
	{
echo '<tr><td>'.$tod->con_nombre.' '.$tod->con_apellido.'</td><td>'.$tod->con_correo.'</td><td>'.$tod->con_telefono_casa.'</td>
<td class="center"><a class="btn btn-small addexistente" id="'.$tod->con_nombre.' '.$tod->con_apellido.'['.$tod->con_telefono_casa.'['.$tod->con_correo.'['.$tod->con_IDcontacto.' " href="#">Agregar</a></td>
</tr>';
}
echo'</tbody></table>';





}
	
	

function centrodellamadas()
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	
	$this->load->model('Contactomodel');
	
	
$data['todo_llamadasentrantes'] = $this->Hppmodel->getLlamadasEntrantes();

	
	$this->load->view('hpp/show-CentrodeLlamadas',$data);
}	
	
	
	
	
	function centrollamadaupdate($id)
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	$this->load->library('form_validation');
	$this->form_validation->set_rules('asesor', 'Asesor', '');
		
	if($this->form_validation->run())
	{
		    $todo = array(
                    'lle_nombre_cliente'=>$this->input->post('cliente'),
					'lle_telefono'=>$this->input->post('telefono'),
					'lle_tipo_llamada'=>$this->input->post('grupoll'),
					'lle_asesor_deventa'=>$this->input->post('asesor'),
					'lle_nota'=>$this->input->post('nota'),
					'lle_autointeres'=>$this->input->post('autointeres'),
         
					);
					
					 $data= $this->Hppmodel->UpdateLlamada($todo,$id);
 
        if ($data) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la informaci&oacute;n. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('hpp/centrodellamadas/');
		
		
	}
	
else{
	 $this->load->model('Vendedoresmodel');
	$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoVendedores();
	$data['uno_llamadasentrantes'] = $this->Hppmodel->getLlamadasEntrantesidedit($id);
	$this->load->view('hpp/show-CentrodeLlamadasUpdate',$data);
	}
}

function centrollamadaupdateB($id,$idc)
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	$this->load->library('form_validation');
	$this->form_validation->set_rules('asesor', 'Asesor', '');
		
	if($this->form_validation->run())
	{
		    $todo = array(
                    'lle_nombre_cliente'=>$this->input->post('cliente'),
					'lle_telefono'=>$this->input->post('telefono'),
					'lle_tipo_llamada'=>$this->input->post('grupoll'),
					'lle_asesor_deventa'=>$this->input->post('asesor'),
					'lle_nota'=>$this->input->post('nota'),
					'lle_autointeres'=>$this->input->post('autointeres'),
                    'lle_cliente_interes'=>$this->input->post('autointeres'),
                    'lle_cliente_interes'=>$this->input->post('lle_cliente_interes'),
                    'lle_correo'=>$this->input->post('lle_correo'),
					);
					
					 $data= $this->Hppmodel->UpdateLlamada($todo,$id);
 
        if ($data) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la informaci&oacute;n. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('contacto/bitacora/'.$idc);
		
		
	}
	
else{
	 $this->load->model('Vendedoresmodel');
	$data['todo_vendedores'] = $this->Vendedoresmodel->getTodoVendedores();
	$data['uno_llamadasentrantes'] = $this->Hppmodel->getLlamadasEntrantesidedit($id);
	$this->load->view('hpp/show-CentrodeLlamadasUpdateB',$data);
	}
}


function llamadaentranteform($id,$idc)
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	$this->load->library('form_validation');
	$this->form_validation->set_rules('asesor', 'Asesor', '');
		
	if($this->form_validation->run())
	{
		    $todo = array(
                    'lle_nombre_cliente'=>$this->input->post('cliente'),
					'lle_telefono'=>$this->input->post('telefono'),
					'lle_tipo_llamada'=>$this->input->post('grupoll'),
					'lle_asesor_deventa'=>$this->input->post('asesor'),
					'lle_nota'=>$this->input->post('nota'),
					'lle_autointeres'=>$this->input->post('autointeres'),
         
					);
					
					 $data= $this->Hppmodel->UpdateLlamada($todo,$id);
 
        if ($data) {
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la informaci&oacute;n. </div>");                        
        } else {
            $this->session->set_flashdata('message', "No se encontraron datos. Error."); 
        }
        redirect('hpp/centrodellamadas/');
		
		
	}
	
else{
	 $this->load->model('Vendedoresmodel');
	$data['idlla']=$id;
	$data['idc']=$idc;
	
	
	$data['todo_vendedores'] = $this->Vendedoresmodel->getId($_SESSION['id']);
	$data['uno_llamadasentrantes'] = $this->Hppmodel->getLlamadasEntrantesidedit($id);
	$this->load->view('hpp/llamadaentranteform',$data);
	}
}

function guiatelefonicaedit($id,$idc)
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	$data['uno_guiatelefonica'] = $this->Hppmodel->getTodoGuiaTelefonica($id);
	$this->load->view('hpp/guia-telefonica-edit',$data);
	
}





	
	function guiastelefonicas($id)
    {   
	$data['flash_message'] = $this->session->flashdata('message');
	
	$data['todo_guias_telefonicas'] = $this->Hppmodel->getGuiasTelefonicas($id);
	$this->load->view('hpp/show_guiatelefonica',$data);



    }
	
	function guiastelefonicasinternet($id,$idc)
	{
		 $this->load->model('Contactomodel');
	$data['flash_message'] = $this->session->flashdata('message');
	$this->load->library('form_validation');
	
	$this->form_validation->set_rules('eleccion', 'Eleccion', '');
	
	
	if($this->form_validation->run())
	{
		    $todo = array(
                    'gin_eleccion'=>$this->input->post('eleccion'),
					'gin_ambos'=>$this->input->post('ambos'),
					'gin_opcion1'=>$this->input->post('opcion1'),
					'gin_opcion2'=>$this->input->post('opcion2'),
					'gin_opcion3'=>$this->input->post('opcion3'),
					'gin_opcion4'=>$this->input->post('opcion4'),
					'gin_claro'=>$this->input->post('clarooscuro'),
					'gin_numero'=>$this->input->post('sunumero'),
					'gin_apellido'=>$this->input->post('apellido'),
					'gin_nombre'=>$this->input->post('nombre'),
					'gin_hoymanana'=>$this->input->post('hoymanana'),
					'gin_hora'=>$this->input->post('hora'),
					'gin_posible'=>$this->input->post('posiblealas'),
					'gin_numero2'=>$this->input->post('minumero'),
					'gin_citalas'=>$this->input->post('cita'),
					'gin_localizarlo'=>$this->input->post('localizarlo'),
					'gin_notas'=>$this->input->post('notas'),
					'gin_porque'=>$this->input->post('porque'),
         
					);
					
					 $data= $this->Hppmodel->Updateguiatel($todo,$id);
 
        
            $this->session->set_flashdata('message', "<br><div class='alert alert-success'>
<button class='close' data-dismiss='alert' type='button'>×</button>Hecho. Ha actualizado la informaci&oacute;n. </div>");        redirect('contacto/bitacora/'.$idc);                
        }
	
	else{
	
	$data['uno_contacto'] = $this->Contactomodel->getcontactobitacora($idc);
	$data['guiainternet'] = $this->Hppmodel->getGuiasTelefonicasInternet($id);
	$this->load->view('hpp/show_guiainternet',$data);
	}
	}
	
	function unarazon($id)
	{
		
	$data['flash_message'] = $this->session->flashdata('message');
	
	$data['una_razon'] = $this->Hppmodel->getUnaRazon($id);
	$this->load->view('hpp/show_una_razon',$data);
	}
	
	function deleteguiaInternet($idc,$idg)
	{
		$this->Hppmodel->deleteguiaInternet($idg);
	$data['flash_message'] = $this->session->flashdata('message');
	$this->session->set_flashdata('message', "<br><div class='alert alert-warning'>
<button class='close' data-dismiss='alert' type='button'>×</button>Guía Telefónica Internet , Eliminada </div>");        
redirect('contacto/bitacora/'.$idc);
	}
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
