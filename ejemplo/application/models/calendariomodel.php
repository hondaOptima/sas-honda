<?php
class Calendariomodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
 
 
	function getTodoOportunidades($idh)
	{
	  $result = $this->db->query('Select opo_IDoportunidad,t_o_nombre,mod_nombre,col_nombre,con_nombre,con_apellido,bit_IDbitacora
		
		from oportunidades,t_oportunidades,bitacora,contacto,auto,modelos,color,huser
		where hus_IDhuser='.$idh.'
		and huser_hus_IDhuser=hus_IDhuser
		and contacto_con_IDcontacto=con_IDcontacto
		and bit_IDbitacora=bitacora_bit_IDbitacora
		and t_oportunidades_t_o_IDt_oportunidades=t_o_IDt_oportunidades
		and opo_auto_vin=aut_IDauto_vin
		and modelos_mod_IDmodelos=mod_IDmodelos
		and color_col_IDcolor=col_IDcolor
		');		
        
		
		
    $return = array();
	 $return['generales'] = 'Actividades Generales';
    if($result->num_rows() > 0){
           
        foreach($result->result_array() as $row){
			  $return['general'] = 'General';
            $return[$row['opo_IDoportunidad']] = $row['t_o_nombre'].' [ '.$row['con_nombre'].' '.$row['con_apellido'].' ] '.' [ '.$row['mod_nombre'].' '.$row['col_nombre'].']';
        }
    }
    return $return;	
	
	
	}
	
	function getIdBitacora($idc)
	{

	
	 $query=$this->db->query('select bit_IDbitacora from bitacora where contacto_con_IDcontacto='.$idc.' ');
	 $result = $query->result();
     return $result[0];
		
    }
	
	
	
		function SendEmailTareaVendedor($idhem,$NVAR,$fei,$hora,$min,$titulo,$desc)
	
	{
		
		
	       $this->load->model('Contactomodel');
		
		
			//obtener datos de contacto para utilizar en email
		    $huser=$this->Contactomodel->getHuser($NVAR);
			if($hora < 12){$ty="am";}else{$ty="pm";}
			
			
	        //preparacion y envio del correo
			$this->email->set_mailtype("html");
		    $this->email->from($idhem, 'CRM HondaOptima.com');
            $this->email->to($huser[0]->hus_correo);
           $this->email->subject('Notificación[Honda Optima] - Tarea asignada por su Gerente de Ventas');
            $this->email->message('
		
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<br>
                     <div>
                     	Hola '.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'.
						<br><br>
						Se te ha asignado la siguiente tarea:	
						<br><br>
						<table>
						<tr><td><b>Fecha:</b></td><td>'.$fei.'</td></tr>
						<tr><td><b>Hora:</b></td><td>'.$hora.':'.$min.' '.$ty.'</td></tr>
						<tr><td><b>Titulo:</b></td><td>'.$titulo.'</td></tr>
						</table>
						<table><tr><td><b>Descripcion:</b></td></tr>
						<tr><td>'.$desc.'</td></tr>
						</table>
							<br><br><br><br><br><br>
                     	</div>
                     	<br>
                     	</td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
		
		
	}
	
	
	function SendSmsTareaVendedor($CAU,$fei,$hora,$min,$ty,$titulo,$desc){
	$txtsms= substr('Tarea asignada;Fecha:'.$fei.'; Hora:'.$hora.':'.$min.' '.$ty.'; Asunto:'.$titulo.';'.$desc, 0, 160);
	
	
	
	$CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
       //print_r($response);
	
	
	}
	
	
}
?>