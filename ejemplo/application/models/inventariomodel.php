<?php
class Inventariomodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
	
	
	 function getCsvFile()
   {
	 $query = $this->db->query('SELECT csv_IDcsv, csv_nombre
FROM csvintellisis
ORDER BY csv_IDcsv DESC
LIMIT 1  ');		
        return $query->result();      
	   
	}
	function getBusquedaAuto(){
		 $query = $this->db->query('SELECT * FROM autos_en_busqueda  ');		
        return $query->result();  
		}
	
 
  function getTodo()
    {
        $query = $this->db->query('Select aut_IDauto_vin,aut_foto,aut_vin,alm_nombre,mod_nombre,col_nombre,aut_ubicacion
		from almacen,color,modelos,fichatecnica
		where almacen_alm_IDalmacen=alm_IDalmacen
		and modelos_mod_IDmodelos=mod_IDmodelos
		and color_col_IDcolor=col_IDcolor	
		and fichatecnica_fte_IDfichatecnica=fte_IDfichatecnica 
		and aut_IDauto_vin > 0
		and (aut_status="Disponible" or aut_status="Reservado")
		');		
        return $query->result();
    }
	
	function statusapartado($id)
    {
        $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_seguro,datos_compra,datos_cliente
		 where dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and data_vin="'.$id.'"
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto
		 and prcv_status_asesor="aprobado" 	
		 and datos_dat_IDdatos=dat_IDdatos
		 and data_=datos_auto_data_
		 and dats_IDdatos_seguro=datos_seguro_dats_IDdatos_seguro
		 and datco_IDdatos_compra=datos_compra_datco_IDdatos_compra
		 and datc_IDdatos_clientes=datos_cliente_datc_IDdatos_clientes
		 and prcv_status!="dupli"
		 '); 
       $result = $query->result();
	   if(empty($result)){}else{
        return $result[0];}
    }
   
   function addasigpedido($todo)
   {
	 $this->db->insert('asignacion_pedido', $todo);
	 $query=$this->db->query('select LAST_INSERT_ID() as AID from asignacion_pedido');
	 $result = $query->result();
     return $result[0];
   }
   
   
   function getApartado($id)
    {
   $query = $this->db->query('
		Select *
		from asigancion_auto,detalle_asp,huser,contacto,fichatecnica
		where
		aau_IdFK=das_IDdetalle_asp
		and asigancion_auto.huser_hus_IDhuser=hus_IDhuser
		and contacto_con_IDcontacto=con_IDcontacto
		and fichatecnica_fte_IDfichatecnica=fte_IDfichatecnica
		and aau_IDasignacion_auto='.$id.'
		');		
        return $query->result();

   }
  function getListaProspectos($idv)
  {
  	
	 $query = $this->db->query('
		Select *
		from contacto,huser
		where hus_IDhuser='.$idv.' and hus_IDhuser=huser_hus_IDhuser and con_status IN ("Prospecto","Caliente","Proceso de Venta","Cliente")  	');		
        return $query->result();
	
	
  }


   
 function deleteAct($id)
 {
 $this->db->delete('actvidades', array('act_IDactividades' => $id)); 	 
	} 

function UpdateASp($id)	
{
 $query = $this->db->query('
update detalle_asp set das_status="Disponible" where das_IDdetalle_asp='.$id.'
		');	
}


function insertAct($IDB)
	{
		$fechab=date('Y').'-'.date('m').'-'.date('d');
		$this->db->query("INSERT INTO actvidades
VALUES ('NULL','realizada','$fechab','$fechab','Cambio a Caliente','Cancelacion de Apartado','09','00',$IDB,'2',0,0,0,'')");
		
	}


function UpdateCOn($id)	
{
 $query = $this->db->query('
update contacto set con_status="Caliente" where con_IDcontacto='.$id.'
		');	
}	
	 
   
   function getActividadDelete($id)
{
 $qu='
		Select act_IDactividades as IDA
		from actvidades
		where
		act_descripcion='.$id.'
		
		';
 $query = $this->db->query($qu);		
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
}
	
	
	function inapartado($vin)
	{
		$query = $this->db->query('
		Select con_nombre,con_apellido,con_titulo,hus_nombre,hus_apellido,aau_fecha
		from asigancion_auto,contacto,huser
		where
		aau_IdFk="'.$vin.'"
		and aau_status="Apartado"
		and contacto_con_IDcontacto=con_IDcontacto
		and asigancion_auto.huser_hus_IDhuser=hus_IDhuser
		');		
        return $query->result();
	
	}
	
	function checkinfoSemi($vin)
	{
		$query = $this->db->query('
		Select * from seminuevos where sem_vin="'.$vin.'"
		');		
        return $query->result();
	
	}
	
   function getTodoApartados()
   {
   $query = $this->db->query('
		Select *
		from asigancion_auto,huser,contacto
		where
		 asigancion_auto.huser_hus_IDhuser=hus_IDhuser
		and contacto_con_IDcontacto=con_IDcontacto
		and aau_status!="Entregado"
		');		
        return $query->result();

   }
   
   function getReservacion($id)
   {
	 $query = $this->db->query('
		Select *
		from asigancion_auto,detalle_asp,huser,contacto
		where
		aau_IdFK=das_IDdetalle_asp
		and asigancion_auto.huser_hus_IDhuser=hus_IDhuser
		and contacto_con_IDcontacto=con_IDcontacto
		and aau_IdFK='.$id.'
		');		
        return $query->result();      
	   
	}
   
   function editdetalleasig($todo,$id)
   {
	   $this->db->update('detalle_asp', $todo, array('das_IDdetalle_asp' => $id));
            return true;
	   
	}
   
   function getAsignacionPedido($suc,$mod)
   {
	 

	   if($suc=='0'){$var1="";}else{$var1=" and asp_sucursal='".$suc."' ";}
	   if($mod=='0'){$var2="";}else{$var2="and  das_modelo like '%".$mod."%' ";}
	  
	  $qu='Select * from asignacion_pedido,detalle_asp,fichatecnica where
	   asignacion_pedido_asp_IDasignacion_pedido=asp_IDasignacion_pedido
	   and das_status IN("disponible","Disponible","Reservado") 
	   and fichatecnica_fte_IDfichatecnica=fte_IDfichatecnica '.$var1.' '.$var2.' '; 
	 $query = $this->db->query($qu);		
       return $query->result();  
	   
	 }
   
   
   function adddetalleasig($todo)
   {
	$this->db->insert('detalle_asp', $todo);
   }
   function inserttransfer($todo)
   {
	   $this->db->insert('transfervin', $todo);
    }
	
	
   function getTodoa()
    {
    $result = $this->db->query('Select aut_IDauto_vin,aut_foto,aut_vin,alm_nombre,mod_nombre,col_nombre,aut_ubicacion
		from auto,almacen,color,modelos,fichatecnica
		where almacen_alm_IDalmacen=alm_IDalmacen
		and modelos_mod_IDmodelos=mod_IDmodelos
		and color_col_IDcolor=col_IDcolor
		
		and fichatecnica_fte_IDfichatecnica=fte_IDfichatecnica 
		and aut_status!="eliminado"');		
   

    $return = array();
    if($result->num_rows() > 0){
         
        foreach($result->result_array() as $row){
            $return[$row['aut_IDauto_vin']] = $row['aut_vin'];
        }
    }
    return $return;
	
	 }
	 function getAsigDataCon($idauto,$idven)
	 {
	 $query = $this->db->query('
		Select con_nombre,con_apellido
		from huser,contacto,bitacora,oportunidades,actvidades
		where
		hus_IDhuser=huser_hus_IDhuser
		and con_IDcontacto=contacto_con_IDcontacto
		and bit_IDbitacora=bitacora_bit_IDbitacora
		and opo_IDoportunidad=oportunidades_opo_IDoportunidad
		and t_oportunidades_t_o_IDt_oportunidades="1"
		and act_titulo="Negociacion y Cierre"
		and act_status="proceso"
		and opo_status="Abierta"
		and hus_IDhuser='.$idven.'
		and opo_auto_vin='.$idauto.'
		
		');		
        return $query->result();   
		 
	}
   
   function getAsigData($id)
   {
	 $query = $this->db->query('
		Select aau_IDasignacion_auto,aau_fecha,aau_pago,hus_nombre,hus_apellido,auto_aut_IDauto_vin,hus_ciudad,huser_hus_IDhuser
		from asigancion_auto,huser
		where 
		huser_hus_IDhuser=hus_IDhuser
        and auto_aut_IDauto_vin='.$id.'
		');		
        return $query->result();   
	   
	}
    function getTodoasig()
    {
        $query = $this->db->query('
		Select *
		from asigancion_auto,huser,contacto
		where 
		asigancion_auto.huser_hus_IDhuser=hus_IDhuser
		and contacto_con_IDcontacto=con_IDcontacto
		
		and aau_status="En Proceso"
		');		
        return $query->result();
    }
	
	 function getUnoasig($id)
    {
        $query = $this->db->query('
		Select *
		from asigancion_auto
		where aau_IDasignacion_auto='.$id.' ');		
        return $query->result();
    }
  
 
	 
	
	 
	   function getModeloName()
    {
    $result = $this->db->get_where('modelos');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['mod_nombre']] = $row['mod_nombre'];
        }
    }
    return $return;
	
	 }
	 
	
	 
	 function getColorName()
    {
    $result = $this->db->get_where('color');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['col_nombre']] = $row['col_nombre'];
        }
    }
    return $return;
	
	 }
	 
	 function getFicha()
    {
    $result = $this->db->get_where('fichatecnica');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['fte_IDfichatecnica']] = $row['fte_nombre'];
        }
    }
    return $return;
	
	 }
	 
   
   function addAuto($todo)
    {
         $this->db->insert('auto', $todo); 
    }
	
	
	 function seminuevosStatus($id)
    {
    	
$query = $this->db->query('select sem_vin from seminuevos where sem_vin="'.$id.'"'); 
  return  $result = $query->result();

	
	 }
	
	
	
   function getAsp($id)
   {
   $query = $this->db->query('select * from detalle_asp,fichatecnica where fichatecnica_fte_IDfichatecnica=fte_IDfichatecnica and   das_IDdetalle_asp='.$id.'');		
        return $query->result();
	   
   }
   
   function getDataSeminuevo($id)
   {
   $query = $this->db->query('select * from seminuevos where sem_vin="'.$id.'"');		
        return $query->result();
	   
   }
   
   function getImageSeminuevo($id)
   {
   $query = $this->db->query('select * from fotos_seminuevos where fse_vin="'.$id.'" and fse_status="on" ');		
        return $query->result();
	   
   }
   
   function getBitApartado($id)
   {
   $query = $this->db->query('select * from asigancion_auto  where  aau_IDasignacion_auto='.$id.'');		
        return $query->result();
	   
   }
   
   
    function addasigancion($todo){  $this->db->insert('asigancion_auto', $todo); }
   
   
   
   
   function saveapartado($todo,$ty,$id,$idc)
   {
	   $this->db->insert('asigancion_auto', $todo); 
	   
	   
	   if($ty=='asig'){ $this->db->query('update detalle_asp set das_status="Reservado" where das_IDdetalle_asp='.$id.' ');}
		
		$this->db->query('update contacto set con_status="Proceso de Venta" where con_IDcontacto='.$idc.' ');
		
		 $query=$this->db->query('select LAST_INSERT_ID() as AID from asigancion_auto');
	 $result = $query->result();
     return $result[0];
	   
   }
   
    function updateapartado($todo,$id,$idc)
   {
	   $this->db->update('asigancion_auto', $todo, array('aau_IDasignacion_auto' => $id));
	   
		
		$this->db->query('update contacto set con_status="Proceso de Venta" where con_IDcontacto='.$idc.' ');
		
	   
   }
   
   
    function editSeminuevos($todo,$id)
   {
   $this->db->update('seminuevos', $todo, array('sem_vin' => $id));   
   }
   
   
  
	
	function actualizaractividades($IDAS,$IDB)
	{
		$fechab=date('Y').'-'.date('m').'-'.date('d');
		$this->db->query("INSERT INTO actvidades
VALUES ('NULL','realizada','$fechab','$fechab','Proceso de venta','".$IDAS."','09','0','$IDB','3','0','0','0','')");
		
	}
	
	
	
	
   
    function getAuto($id)
    {
        $query = $this->db->query('select * from auto where  aut_IDauto_vin='.$id.'');		
        return $query->result();
    }
	
	function getnombres($id)

	{
		 $te='select hus_nombre,hus_apellido,con_titulo,con_nombre,con_apellido 
		 from asigancion_auto,huser,contacto 
		 where asigancion_auto.huser_hus_IDhuser and contacto_con_IDcontacto=con_IDcontacto
		 and aau_IdFk='.$id.' ';
		 $query = $this->db->query($te);		
        return $query->result();
	}
    
	/*
	 function getAutoid($id)
    {
        $query = $this->db->query('
	Select aut_IDauto_vin,aut_sucursal,aut_no_inventario,aut_ano,mod_nombre,aut_vin,aut_motor,aut_tipo_unidad,aut_ubicacion,aut_importe,aut_iva,aut_llegada,aut_foto,col_nombre,alm_nombre,fichatecnica_fte_IDfichatecnica,fte_nombre
		from auto,almacen,color,modelos,fichatecnica
		where almacen_alm_IDalmacen=alm_IDalmacen
		and modelos_mod_IDmodelos=mod_IDmodelos
		and color_col_IDcolor=col_IDcolor
		
		and fichatecnica_fte_IDfichatecnica=fte_IDfichatecnica 
		and (aut_status!="Vendido" or aut_status!="Eliminado")
		and aut_IDauto_vin='.$id.'
		');		
        return $query->result();
    }
	 * 
	 * 
	 */
	
	function updateasigancion($todo,$id)
	{
	
         $this->db->update('asigancion_auto', $todo, array('aau_IDasignacion_auto' => $id));
            return true;	
		
	}
	
	function editasignauto($todo,$id)
	{
	
         $this->db->update('asigancion_auto', $todo, array('aau_IdFk' => $id));
            return true;	
		
	}
	
	function updatetodo($todo,$id)
    {
         $this->db->update('auto', $todo, array('aut_IDauto_vin' => $id));
            return true;
    }
	
	function updatefoto($todo,$id)
    {
         $this->db->update('auto', $todo, array('aut_IDauto_vin' => $id));
            return true;
    }
	
	 function get($id) 
    {
        $query = $this->db->query('select das_modelo from detalle_asp where das_IDdetalle_asp='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
    }
	
	 function deleteasig($id)
    {
       
            $this->db->delete('asigancion_auto', array('aau_IDasignacion_auto' => $id)); 
            return true;
          
    }
	
	 function deleteimg($id)
    {
       
           $qr='update fotos_seminuevos set fse_status="off" where fse_IDfotos_seminuevos='.$id.' ';
            $this->db->query($qr); 
            return true;
          
    }
	
	
	 function delete($id)
    {
        $qr='update detalle_asp set das_status="eliminado" where das_IDdetalle_asp='.$id.' ';
            $this->db->query($qr); 
            return true;
           
    }
	
	 function deletefoto($id)
    {
        $query = $this->db->get_where('fotosauto', array('fat_IDfotosauto'=>$id));  
 
        if ($query->num_rows()==0) {
            return false;
        }
        else {
            $this->db->delete('fotosauto', array('fat_IDfotosauto' => $id)); 
            return true;
        }    
    }
	
	
	function insertSeminuevos($todo)
    {
         $this->db->insert('seminuevos', $todo); 
    }
	
	 function insertfotoseccion($todo)
    {
         $this->db->insert('fotosauto', $todo); 
    }
	
	 function getFotoinex($id,$inex)
    {
         $query = $this->db->query('select * from fotosauto where auto_aut_IDauto_vin="'.$id.'" and fat_seccion="'.$inex.'"');		
        return $query->result();
    }
	
	
}
?>