<?php
class Homemodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
 
   function getNoticias($id)
    {
        $query = $this->db->query("SELECT * FROM notificaciones,eventos where eventos_eve_IDeventos=eve_IDeventos and   eve_status='activa' and huser_hus_IDhuser=".$id." and not_status='cerrada' ");		
        return $query->result();
    }
	
	function ContactosAdmin($cd,$sta){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser,bitacora 
where 
huser_hus_IDhuser=hus_IDhuser 
and contacto_con_IDcontacto=con_IDcontacto
and hus_ciudad='".$cd."' 
and con_status='".$sta."'   ");
$result = $query->result();
return $result[0];		
   }
   
   function ContactosAdminSinA($cd,$sta){
$query = $this->db->query("
SELECT *
from contactos_admin_sin_actividad
where 
hus_ciudad='".$cd."' and 
con_status='".$sta."' 
  ");
return $query->result();
	
   }
   
   function ContactosAdminT($cd){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser,bitacora 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad='".$cd."' and 
con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida') 
and
contacto_con_IDcontacto=con_IDcontacto  ");
$result = $query->result();
return $result[0];		
   }
   function ContactosTotal($id){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status='Prospecto' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status='Caliente' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status='Caida' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status='Cliente' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida')   

");		
$result = $query->result();
return $result;		   
	   }
	   
	   
	    function ContactosTotalB($id){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad='".$id."' and
con_status='Prospecto' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad='".$id."' and
con_status='Caliente' 


union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad='".$id."' and
con_status='Caida' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad='".$id."' and
con_status='Cliente' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad='".$id."' and
con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida')   

");		
$result = $query->result();
return $result;		   
	   }
	   
	    function ContactosTotalA(){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad IN ('Tijuana','Mexicali','Ensenada')
and con_status='Prospecto' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad IN ('Tijuana','Mexicali','Ensenada') 
and con_status='Caliente' 


union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad IN ('Tijuana','Mexicali','Ensenada')
and con_status='Caida' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad IN ('Tijuana','Mexicali','Ensenada')
and con_status='Cliente' 

union all

SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_ciudad IN ('Tijuana','Mexicali','Ensenada')
and con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida')   

");		
$result = $query->result();
return $result;		   
	   }
		
	function ContactosUser($id,$sta){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status='".$sta."' ");		
$result = $query->result();
return $result[0];	
		}
		
		function ContactosUserT($id){
$query = $this->db->query("
SELECT count(*) as num
from contacto,huser 
where 
huser_hus_IDhuser=hus_IDhuser and 
hus_IDhuser='".$id."' and
con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida')     ");		
$result = $query->result();
return $result[0];	
		}
		
		/*function ContactosUserSinAct($id){
$query = $this->db->query("
SELECT count(*) as num from lista_contactos_tipo where
hus_IDhuser='".$id."' and
con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida')     ");		
$result = $query->result();
return $result[0];	
		}*/
		function siono($idb){
	$result=$this->db->query("SELECT * 
		FROM  bitacora,actvidades 
		where  
		bitacora_bit_IDbitacora=bit_IDbitacora 
		and bit_IDbitacora=".$idb."
		and act_status='pendiente'");
		

//$return = array();
    if($result->num_rows() > 0){
       return 'si';
    }
	else {
    return 'no';
	}
		
	}
				function ContactosUserSinAct($id){


$result = $this->db->query("
SELECT bit_IDbitacora  from lista_contactos_tipo where
hus_IDhuser='".$id."' and
con_status IN  ('Prospecto','Cliente','Caliente','Proceso de Venta','Caida')     ");		

$i=0;
$return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
           $res=$this->Homemodel->siono($row['bit_IDbitacora']);
        if($res=='no'){$i++;}
		}
    }
    return $i;
		}
		
		
	
	 function getNoticiasA()
    {
        $query = $this->db->query("SELECT * FROM eventos where eve_status!='eliminado'");		
        return $query->result();
    }
	
	function lastActv($idb)
	{
        $query = $this->db->query("
		SELECT * 
		FROM  bitacora,actvidades 
		where  
		bitacora_bit_IDbitacora=bit_IDbitacora 
		and bit_IDbitacora=".$idb."
		and act_status='pendiente' ");		
        return $query->result();
    }
	
	function SigActv($idb)
	{
        $query = $this->db->query("
		SELECT * 
		FROM  bitacora,actvidades 
		where  
		bitacora_bit_IDbitacora=bit_IDbitacora 
		and bit_IDbitacora=".$idb."
		and act_fecha_inicio >='".date('Y')."-".date('m')."-".date('d')."'
		and act_status='pendiente' ");		
        return $query->result();
    }
	
	function lastActvCoucheo($idb)
	{
        $query = $this->db->query("
		SELECT * 
		FROM  bitacora,actvidades 
		where  
		bitacora_bit_IDbitacora=bit_IDbitacora 
		and bit_IDbitacora=".$idb."
		and tipo_actividad_tac_IDtipo_actividad=7
		ORDER BY  `actvidades`.`act_fecha_inicio` DESC  ");		
        $query->result();
		$result = $query->result();
		if(empty($result)){}else{
        return $result[0];}
    }
	
	 function ProcesodeVentaHome($id,$vin){
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_seguro,datos_compra,datos_cliente
		 where dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and bitacora.contacto_con_IDcontacto='.$id.'
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto
		 and prcv_status_asesor="aprobado" 	
		 and datos_dat_IDdatos=dat_IDdatos
		 and data_=datos_auto_data_
		 and data_vin="'.$vin.'"
		 and dats_IDdatos_seguro=datos_seguro_dats_IDdatos_seguro
		 and datco_IDdatos_compra=datos_compra_datco_IDdatos_compra
		 and datc_IDdatos_clientes=datos_cliente_datc_IDdatos_clientes
		 and prcv_status!="dupli"
		 '); 
       $result = $query->result();
	   if(empty($result)){}else{
        return $result[0];}
		 
		 }
		 
		 function ProcesodeVentaHomeCliente($id){
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_seguro
		 where prcv_status="venta"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and bitacora.contacto_con_IDcontacto='.$id.'
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto
		 and datos_dat_IDdatos=dat_IDdatos
		 and data_=datos_auto_data_
		 and dats_IDdatos_seguro=datos_seguro_dats_IDdatos_seguro
		 '); 
       $result = $query->result();
	   if(empty($result)){}else{
        return $result[0];}
		 
		 }
		 
		  function AutoApartado($id){
		 $query = $this->db->query('
		 select * from asigancion_auto 
		 where contacto_con_IDcontacto='.$id.'
		 and aau_status="Apartado" '); 
       $result = $query->result();
	   if(empty($result)){}else{
        return $result[0];}
		 
		 }
		 
		  function AutoApartadoVenta($id){
		 $query = $this->db->query('
		 select * from asigancion_auto 
		 where contacto_con_IDcontacto='.$id.'
		'); 
       $result = $query->result();
	   if(empty($result)){}else{
        return $result[0];}
		 
		 }
	
	function getTareasA($ciudad)
	{
        $query = $this->db->query("SELECT hus_nombre,hus_apellido,con_nombre,con_apellido,bit_IDbitacora,con_modelo,con_ano,con_color,con_tipo,con_IDcontacto,con_status 
FROM huser,contacto,bitacora 
where huser_hus_IDhuser=hus_IDhuser 
and contacto_con_IDcontacto=con_IDcontacto
and  con_status='Prospecto'
and hus_ciudad='".$ciudad."' ORDER BY bit_fecha  DESC ");		
        return $query->result();
    }
	function getTareas($id)
	{
        $query = $this->db->query("SELECT hus_nombre,hus_apellido,con_nombre,con_apellido,bit_IDbitacora,con_modelo,con_ano,con_color,con_tipo,con_IDcontacto,con_status  FROM huser,contacto,bitacora 
where huser_hus_IDhuser=hus_IDhuser 
and contacto_con_IDcontacto=con_IDcontacto 
and  con_status='Prospecto'
and hus_IDhuser=".$id."  ORDER BY bit_fecha  DESC  ");		
        return $query->result();
    }
	
	
	function getVentas($id)
	{
		function getMonthDays($Month, $Year){if( is_callable("cal_days_in_month")){return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);}
else{return date("d",mktime(0,0,0,$Month+1,0,$Year));}}
$totd=getMonthDays(date('m'),date('Y'));

$uno=date('Y').'-'.date('m').'-'.'01';
$dos=date('Y').'-'.date('m').'-'.$totd;

        $query = $this->db->query("SELECT ven_IDventa from venta,procesodeventa,dordendencomprat,contacto,huser where  
		ven_IDventa=prcv_IDprocesodeventa
		and prcv_IDordencompra=dor_IDdordencomprat
		and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		and  	huser_hus_IDhuser=hus_IDhuser
		 and hus_IDhuser=".$id."
		 and ven_status='on'
		 and DATE(ven_fecha) BETWEEN  '".$uno."' and '".$dos."'   ");		
        return $query->result();
    }
	
	function getVentaA($ciudad)
	{
		function getMonthDays($Month, $Year){if( is_callable("cal_days_in_month")){return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);}
else{return date("d",mktime(0,0,0,$Month+1,0,$Year));}}
$totd=getMonthDays(date('m'),date('Y'));

$uno=date('Y').'-'.date('m').'-'.'01';
$dos=date('Y').'-'.date('m').'-'.$totd;


 $qu="SELECT ven_IDventa from venta,procesodeventa,dordendencomprat,contacto,huser where  
		ven_fk_IDventa=prcv_IDprocesodeventa
		and prcv_IDordencompra=dor_IDdordencomprat
		and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		and  	huser_hus_IDhuser=hus_IDhuser
		and hus_ciudad='".$ciudad."'
		 and ven_status='on' 
		 and DATE(ven_fecha) BETWEEN  '".$uno."' and '".$dos."'  ";
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	
	
	 function getGenerales($id)
    {
        $query = $this->db->query("SELECT * FROM generales where gen_status='pendiente' and huser_hus_IDhuser=".$id."");		
        return $query->result();
    }
	
	 function getGeneralesA()
    {
        $query = $this->db->query("SELECT * FROM generales where gen_status='pendiente'");		
        return $query->result();
    }
	
	 function getContactos($id)
    {
        $query = $this->db->query("SELECT * FROM contacto where con_status='Prospecto' and huser_hus_IDhuser=".$id."  ORDER BY `contacto`.`con_IDcontacto` DESC ");		
        return $query->result();
    }
	
	function getContactosA($ciudad)
    {
		if($ciudad=='off'){ $var="";}else{ $var="and hus_ciudad='".$ciudad."'";}
        $query = $this->db->query("SELECT * FROM contacto,huser where  	huser_hus_IDhuser=hus_IDhuser ".$var." and  con_status='Prospecto' ORDER BY `contacto`.`con_IDcontacto` DESC ");		
        return $query->result();
    }
	
	function getTareasP($ido)
   {
	  
	 $query=$this->db->query(' select count(*) as numero from actvidades where act_status="pendiente" and bitacora_bit_IDbitacora='.$ido.''); 
	 $result = $query->result();
        return $result[0];
   }
   
   function getRegistro($id)
	{
	$qu='Select * from bitacora where  bit_IDbitacora='.$id.'  ';
	$query = $this->db->query($qu);		
        return $query->result();
		
    }
	
	 function getBitApartado($id)
   {
   $query = $this->db->query('select * from asigancion_auto  where  aau_IDasignacion_auto='.$id.'');		
        return $query->result();
	   
   }
   
	
	/*
	function getVentasA()
    {
		
function getMonthDays($Month, $Year){if( is_callable("cal_days_in_month")){return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);}
else{return date("d",mktime(0,0,0,$Month+1,0,$Year));}}
$totd=getMonthDays(date('m'),date('Y'));

$uno=date('Y').'-'.date('m').'-'.'01';
$dos=date('Y').'-'.date('m').'-'.$totd;

$query="SELECT * FROM ventas where DATE(ven_fecha) BETWEEN  '".$uno."' and '".$dos."'

and ven_status='Realizada' ";
 $query = $this->db->query($query);		
    return $query->result();
	
}


function getVentas($id)
    {
		
function getMonthDays($Month, $Year){if( is_callable("cal_days_in_month")){return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);}
else{return date("d",mktime(0,0,0,$Month+1,0,$Year));}}
$totd=getMonthDays(date('m'),date('Y'));

$uno=date('Y').'-'.date('m').'-'.'01';
$dos=date('Y').'-'.date('m').'-'.$totd;

$query="SELECT * FROM ventas where DATE(ven_fecha) BETWEEN  '".$uno."' and '".$dos."'

and ven_status='Realizada' and huser_hus_IDhuser=".$id." ";
 $query = $this->db->query($query);		
    return $query->result();
	
}
*/


}
?>