<?php
class Contactomodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
	
	function listfecha($date){
	
		list($mes,$dia,$ano)=explode('-',$date);
		$mes=date('m', strtotime($mes));
			return $fecha=$ano.'-'.$mes.'-'.$dia;
		}
		function fechaslash($date){
	list($mes,$dia,$ano)=explode('/',$date);
			return $fecha=$ano.'-'.$mes.'-'.$dia;
		}
		function listadia($date){
	
		list($mes,$dia,$ano)=explode('-',$date);
		$mes=date('m', strtotime($mes));
			return $dia;
		}
		
		function listames($date){
		
		list($mes,$dia,$ano)=explode('-',$date);
			$mes=date('m', strtotime($mes));
if($mes=='01'){return $ms="Enero";}
if($mes=='02'){return $ms="Febrero";}
if($mes=='03'){return $ms="Marzo";}
if($mes=='04'){return $ms="Abril";}
if($mes=='05'){return $ms="Mayo";}
if($mes=='06'){return $ms="Junio";}
if($mes=='07'){return $ms="Julio";}
if($mes=='08'){return $ms="Agosto";}
if($mes=='09'){return $ms="Septiembre";}
if($mes=='10'){return $ms="Octubre";}
if($mes=='11'){return $ms="Noviembre";}
if($mes=='12'){return $ms="Diciembre";}
		}

function Arraylista()
{
$array=
array('Prospecto'=>'Prospecto','Caliente'=>'Caliente','Cliente'=>'Clientes','Proceso%20de%20Venta'=>'Proceso de venta','Caida'=>'Ventas Ca&iacute;das');
return $array;	
}
function ArrayClass(){
	$array=array('elusive-icon-file-edit','elusive-icon-bookmark','elusive-icon-facebook','elusive-icon-slideshare','elusive-icon-glass','elusive-envelope-alt','icon-home','elusive-icon-twitter','elusive-icon-search"');
	return $array;
}

function ArrayTitle(){
	$array=array('Pagina Web','Eflyer','Facebook','Referido','Evento','Email','Piso','Twitter','Prospeccion');
	return $array;
	}
function ArrayMes(){
	
	$array=array("00"=>"Ene","01"=>"Ene","02"=>"Feb","03"=>"Mar","04"=>"Abr","05"=>"May","06"=>"Jun","07"=>"Jul","08"=>"Ago","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dic");
	return $array;
	}	
	function limpiarEspacios($cadena){ $cadena = str_replace(' ', '', $cadena);
    return $cadena;}
	
function limpiarCaracteresEspeciales($string ){
$string = htmlentities($string);$string = preg_replace('/\&(.)[^;]*;/', '\\1', $string);return $string;} 

function randomPass($caracteres){
	$random_pass = substr(md5(rand()),0,$caracteres);
	return $random_pass;
	}
	
function SmsBienvenida($CAU){
     $txtsms="Queremos que tu experiencia en Honda Optima sea perfecta, gracias por visitarnos. Honda The Power of Dreams ";
	 $CAT='52'.$CAU;
	 $phone = array($CAT);
     $response = $this->textmagic->send($txtsms, $phone);
	}	
	
	
	 function getCandado($id)
    {
		$qu='SELECT ctb_desc
FROM `contabilidad` , dordendencomprat, procesodeventa
WHERE prcv_IDordencompra = dor_IDdordencomprat
AND ctb_fk_IDordendecompra = prcv_IDprocesodeventa
AND dor_IDdordencomprat ='.$id.'
AND ctb_desc = "candado"';
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	 function getTareasPendientes($cd)
    {
		$qu="SELECT con_nombre,act_IDactividades,con_correo,act_status,bit_IDbitacora,hus_correo,con_IDcontacto,hus_IDhuser,act_fecha_inicio,act_titulo,hus_ciudad,con_apellido,hus_nombre,hus_apellido,act_descripcion 
		
FROM `actvidades`,bitacora,contacto,huser WHERE act_status='pendiente'
and bitacora_bit_IDbitacora=bit_IDbitacora
and contacto_con_IDcontacto=con_IDcontacto
and huser_hus_IDhuser=hus_IDhuser
and hus_ciudad='".$cd."'
and act_fecha_inicio <= '".date('Y').'-'.date('m').'-'.date('d')."'
ORDER BY `actvidades`.`act_fecha_fin` ASC 
";
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	function getTareasPendientesUser($id)
    {
		$qu="SELECT con_nombre,act_IDactividades,con_correo,act_status,bit_IDbitacora,hus_correo,con_IDcontacto,hus_IDhuser,act_fecha_inicio,act_titulo,hus_ciudad,con_apellido,act_descripcion 
FROM `actvidades`,bitacora,contacto,huser WHERE act_status='pendiente'
and bitacora_bit_IDbitacora=bit_IDbitacora
and contacto_con_IDcontacto=con_IDcontacto
and huser_hus_IDhuser=hus_IDhuser
and hus_IDhuser='".$id."'
and act_fecha_inicio <= '".date('Y').'-'.date('m').'-'.date('d')."'
ORDER BY `actvidades`.`act_fecha_fin` ASC ";
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	function getTareasPendientesAdmin($id)
    {
		$qu="
		SELECT * 
		FROM generales,huser 
		where
		gen_status='pendiente' 
		and huser_hus_IDhuser=hus_IDhuser 
		and hus_IDhuser=".$id."
        and gen_fecha_inicio <= '".date('Y').'-'.date('m').'-'.date('d')."'
		ORDER BY `generales`.`gen_fecha_fin` DESC ";
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	function getTareasPendientesAdminCd($cd)
    {
		$qu="
		SELECT * 
		FROM generales,huser 
		where 
		gen_status='pendiente'
		and huser_hus_IDhuser=hus_IDhuser 
		and hus_ciudad='".$cd."'
        and gen_fecha_inicio <= '".date('Y').'-'.date('m').'-'.date('d')."'
		ORDER BY `generales`.`gen_fecha_fin` DESC ";
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	
	
	function Getcontabilidad1($id)
	{
		$qu='Select * from contabilidad where  	ctb_fk_IDordendecompra='.$id.' and ctb_desc="candado" ';
        $query = $this->db->query($qu);		
        return $query->result();
    }
	
	function Getcontabilidad2($id)
	{
        $query = $this->db->query('Select * from contabilidad where  	ctb_fk_IDordendecompra='.$id.' and ctb_desc="gerencia" ');		
        return $query->result();
    }function Getcontabilidad3($id)
	{
        $query = $this->db->query('Select * from contabilidad where  	ctb_fk_IDordendecompra='.$id.' and ctb_desc="asesor" ');		
        return $query->result();
    }function Getcontabilidad4($id)
	{
        $query = $this->db->query('Select * from contabilidad where 	ctb_fk_IDordendecompra='.$id.' and ctb_desc="merca" ');		
        return $query->result();
    }function Getcontabilidad5($id)
	{
        $query = $this->db->query('Select * from contabilidad where  	ctb_fk_IDordendecompra='.$id.' and ctb_desc="contacto" ');		
        return $query->result();
    }
	
	
  function getAllDataOrdendeCompra($id)
    {
		 $qry='Select * from dordendencomprat,datos,datos_auto,datos_cliente,datos_seguro,datos_documentos_recibidos,datos_compra,dirfacturacion,contacto,huser,avisodepedido
 where
dor_IDdordencomprat='.$id.'
and datos_dat_IDdatos=dat_IDdatos
and datos_auto_data_=data_
and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
and datos_seguro_dats_IDdatos_seguro=dats_IDdatos_seguro
and datos_documentos_recibidos_datdr_datos_documentos_recibidos=datdr_datos_documentos_recibidos
and datos_compra_datco_IDdatos_compra=datco_IDdatos_compra
and  	dfa_IDdirfacturacion=datos_dirfacturacion
and contacto_con_IDcontacto=con_IDcontacto
and huser_hus_IDhuser=hus_IDhuser 
and idavisodepedido=avisodepedido_idavisodepedido  ';
        $query = $this->db->query($qry);		
        return $query->result();
    }
	
	 function getAllProcesodeventa($id)
    {
		 $qry='Select * from procesodeventa
 where prcv_IDprocesodeventa='.$id.'
  ';
        $query = $this->db->query($qry);		
        return $query->result();
    }
	 function getAllDataOrdendeCompraUser($id)
    {
		$qry='Select * from procesodeventa,dordendencomprat,datos_auto,contacto,huser where
         contacto_con_IDcontacto=con_IDcontacto
		 and huser_hus_IDhuser=hus_IDhuser
		 and datos_auto_data_=data_
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and prcv_IDprocesodeventa='.$id.'

  ';
        $query = $this->db->query($qry);		
        return $query->result();
    }
	
	function getAllDataEntregaDetalle($ido)
	{
	$qry='Select * from fechaentrega where fen_Id_Ordendecompra='.$ido.'

  ';
        $query = $this->db->query($qry);		
        return $query->result();	
	}
 
  function getTodocontactoa($cd)
    {$query = $this->db->query(' SELECT * FROM  `listaProspectos`  where  hus_ciudad="'.$cd.'" ');		
     return $query->result();
    }
	
	function getTodocontactoacou($cd)
    {
        $query = $this->db->query('Select 
        
		hus_nombre,
        hus_apellido,
        bit_IDbitacora,
        con_nombre,
        con_apellido,
        con_modelo,
        con_ano,
        con_color,
        con_tipo,
        con_IDcontacto,
        con_status,
        publicidad_pub_IDpublicidad,
        con_titulo,
        con_telefono_officina,
        con_telefono_casa,
        con_telefono_off,
        con_telefono_off2,
        bit_fecha,
        con_correo,
		con_ocupacion
        
        from contacto,huser,bitacora where  con_status IN  ("Prospecto") and hus_IDhuser=huser_hus_IDhuser and hus_IDhuser="'.$cd.'" and contacto_con_IDcontacto=con_IDcontacto  ORDER BY `bit_fecha` DESC ');		
        return $query->result();
    }
	
	function getTodocontactoacouadmin($cd,$sta)
    {
		$dosd=$sta;
		if($dosd=='Prospecto'){$dosd='con_status IN  ("Prospecto")';}
		if($dosd=='Cliente'){$dosd='con_status IN  ("Cliente")';}
		if($dosd=='Caliente'){$dosd='con_status IN  ("Caliente")';}
		if($dosd=='Caida'){$dosd='con_status IN  ("Caida")';}
		if($dosd=='Proceso%20de%20Venta'){$dosd='con_status IN  ("Proceso de Venta")';}
		if($dosd=='Todos'){$dosd='con_status IN  ("Cliente","Prospecto","Caliente","Proceso de Venta","Caida")';}
        $query = $this->db->query('Select 
		hus_nombre,
        hus_apellido,
        bit_IDbitacora,
        con_nombre,
        con_apellido,
        con_modelo,
        con_ano,
        con_color,
        con_tipo,
        con_IDcontacto,
        con_status,
        publicidad_pub_IDpublicidad,
        con_titulo,
        con_telefono_officina,
        con_telefono_casa,
        con_telefono_off,
        con_telefono_off2,
        bit_fecha,
        con_correo,
		con_ocupacion
		 from 
		 contacto,huser,bitacora
		  where '.$dosd.' and hus_ciudad="'.$cd.'"
		  
		  and hus_IDhuser=huser_hus_IDhuser  and contacto_con_IDcontacto=con_IDcontacto  ORDER BY `bit_fecha` DESC  ');		
        return $query->result();
    }
	
	function getTodocontactoacoudosA($sta)
    {
		$dosd=$sta;
		if($dosd=='Prospecto'){$dosd='con_status IN  ("Prospecto")';}
		if($dosd=='Cliente'){$dosd='con_status IN  ("Cliente")';}
		if($dosd=='Caliente'){$dosd='con_status IN  ("Caliente")';}
		if($dosd=='Caida'){$dosd='con_status IN  ("Caida")';}
		if($dosd=='Proceso%20de%20Venta'){$dosd='con_status IN  ("Proceso de Venta")';}
		if($dosd=='Todos'){$dosd='con_status IN  ("Cliente","Prospecto","Caliente","Proceso de Venta","Caida")';}
	$data='Select 
	
	    hus_nombre,
        hus_apellido,
        bit_IDbitacora,
        con_nombre,
        con_apellido,
        con_modelo,
        con_ano,
        con_color,
        con_tipo,
        con_IDcontacto,
        con_status,
        publicidad_pub_IDpublicidad,
        con_titulo,
        con_telefono_officina,
        con_telefono_casa,
        con_telefono_off,
        con_telefono_off2,
        bit_fecha,
        con_correo,
		con_ocupacion
	
	 from contacto,huser,bitacora where '.$dosd.' and  hus_IDhuser=huser_hus_IDhuser and hus_ciudad IN ("Tijuana","Mexicali","Ensenada")  and contacto_con_IDcontacto=con_IDcontacto group by(con_IDcontacto) ORDER BY `bit_fecha` DESC' ;
        $query = $this->db->query($data);		
        return $query->result();
    }
	
	function getTodocontactoacoudos($cd,$sta)
    {
		$dosd=$sta;
		if($dosd=='Prospecto'){$dosd='con_status IN  ("Prospecto")';}
		if($dosd=='Cliente'){$dosd='con_status IN  ("Cliente")';}
		if($dosd=='Caliente'){$dosd='con_status IN  ("Caliente")';}
		if($dosd=='Caida'){$dosd='con_status IN  ("Caida")';}
		if($dosd=='Proceso%20de%20Venta'){$dosd='con_status IN  ("Proceso de Venta")';}
		if($dosd=='Todos'){$dosd='con_status IN  ("Cliente","Prospecto","Caliente","Proceso de Venta","Caida")';}
	$data='Select 
	    hus_nombre,
        hus_apellido,
        bit_IDbitacora,
        con_nombre,
        con_apellido,
        con_modelo,
        con_ano,
        con_color,
        con_tipo,
        con_IDcontacto,
        con_status,
        publicidad_pub_IDpublicidad,
        con_titulo,
        con_telefono_officina,
        con_telefono_casa,
        con_telefono_off,
        con_telefono_off2,
        bit_fecha,
        con_correo,
		con_ocupacion
	
	 from contacto,huser,bitacora where '.$dosd.' and  
	 hus_IDhuser=huser_hus_IDhuser and hus_IDhuser="'.$cd.'"  and 
	 contacto_con_IDcontacto=con_IDcontacto group by(con_IDcontacto) ORDER BY `bit_fecha` DESC' ;
        $query = $this->db->query($data);		
        return $query->result();
    }
	
	
	function getLlamadaEntrante($id)
	 {
        $query = $this->db->query('Select * from llamada_entrante where lle_status= "atendida" and lle_contacto='.$id.'   ');		
        return $query->result();
    }
	
	
	 
	 function getGuiaTelefonica($id)
	 {
        $query = $this->db->query('Select get_hora, 	gte_fecha, 	gte_IDguia_telefonica,get_nota from guia_telefonica  where contacto_con_IDcontacto='.$id.'  ');		
        return $query->result();
    }
	
	 function getGuiaTelefonicaInternet($id)
	 {
        $query = $this->db->query('Select * from guiainternet where contacto_con_IDcontacto='.$id.'  ');		
        return $query->result();
    }


	
	function getHojaRazon($id)
    {
        $query = $this->db->query('Select hoj_IDhojaunarazon,hoj_fechafin,hoj_unidadtipo,hoj_hora,hoj_min from hojaunarazon where  contacto_con_IDcontacto='.$id.' ');		
        return $query->result();
    }
	
	function getAllHojaRazon($id)
	
	{
		 $query = $this->db->query('Select * from hojaunarazon where  hoj_IDhojaunarazon='.$id.' ');		
        return $query->result();
		
	}
	
	function getInfoLlamada($id)
	{
		 $query = $this->db->query('Select * from llamada_entrante,huser where  lle_IDllamada_entrante='.$id.' 
		 and lle_asesor_deventa=hus_IDhuser');		
        return $query->result();
		
	}
  
  function getTipoMensage()
  {
  	  $result = $this->db->query('Select * from tipo_mensage ');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			 
            $return[$row['tip_IDtipo_mensage']] = $row['tip_nombre'];
        }
    }
    return $return;
	
	
  }

  function getTodocontactoahpp($cd,$name)
    {
$qu='Select * from contacto,huser where  con_status != "eliminado" and hus_IDhuser=huser_hus_IDhuser and hus_ciudad="'.$cd.'" and con_nombre like "%'.$name.'%" ';
$query = $this->db->query($qu);		
return $query->result();
	}
	
	
	function getTodoagentes($cd)
	
	 {
        $result = $this->db->query('Select * from huser where  con_status != "eliminado" and hus_IDhuser=huser_hus_IDhuser ');		
         $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			 	$return[''] = 'Seleccione una opcion';
			 $return['0'] = 'Nuevo Contacto';
            $return[$row['con_IDcontacto']] = $row['con_nombre'].''.$row['con_apellido'];
        }
    }
    return $return;
		
		
		
    }
	
		function getContactosApartar($id)
	
	 {
        $result = $this->db->query('Select * from contacto where  con_status != "eliminado" and huser_hus_IDhuser='.$id.' ');		
         $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			 	$return['0'] = 'Seleccione un contacto';
            $return[$row['con_IDcontacto']] =  $row['con_titulo'].' '.$row['con_nombre'].' '.$row['con_apellido'];
        }
    }
    return $return;
		
		
		
    }


  
  function getNameContacto($id)
    {
        $query = $this->db->query('
Select con_nombre,con_apellido,con_correo,con_titulo,con_modelo,con_telefono_casa,con_status,fuente_fue_IDfuente 
from contacto where  con_IDcontacto='.$id.' ');		
        return $query->result();
    }

 function getHuser($id)
    {
        $query = $this->db->query('Select * from huser where  hus_IDhuser='.$id.' ');		
        return $query->result();
    }
	function getCotizacionLista($idc){
		$query = $this->db->query('SELECT * 
FROM   cotizador,contacto,ficha_modelo
WHERE   con_IDcontacto=cot_IDcontacto	
and con_IDcontacto='.$idc.'
and cot_IDficha=fmo_IDficha_modeo ');		
        return $query->result();
		
		}
		
		
	
	function getHuserCon($id)
    {
        $query = $this->db->query('Select * from huser,contacto where  con_IDcontacto='.$id.' and huser_hus_IDhuser=hus_IDhuser ');		
        return $query->result();
    }
  	
	function getOrdenesdeCompra($id)
	  {
	 $query='Select data_auto,data_vin,dor_fecha,dor_IDdordencomprat from dordendencomprat,datos_auto where  contacto_con_IDcontacto='.$id.' and datos_auto_data_=data_ ' ;
        $query = $this->db->query($query);		
        return $query->result();
    }
	  
	  function IdOrdendeCompra($id)
	{
		$qy='select MAX(dat_IDdatos) as UID from datos ';
	 $query=$this->db->query($qy);
	 $result = $query->result();
	 
	   
	    return $result[0];
			
	    
    }

function CreateCotizacionHuser(){
$datos_cotizacion = array('cot_IDcotizador'=>null,'cot_IDcontacto'=>0,'cot_email'=>'','cot_IDficha'=>0,'cot_precioex1'=>'','cot_precioex2'=>'','cot_precioex3'=>'','cot_precioex4'=>0,'cot_precioex5'=>0,'cot_precioex6'=>0,'cot_servicio1'=>0,'cot_servicio2'=>0,'cot_servicio3'=>0,'cot_precio1'=>0,'cot_precio2'=>0,'cot_precio3'=>0,'cot_fecha'=>''.date('Y-m-d').'','cot_nota'=>'');

$this->db->insert('cotizador', $datos_cotizacion);
$query=$this->db->query('select LAST_INSERT_ID() as IDC from contacto');
$result = $query->result();
$IDCOT=$result[0]->IDC;

for($i=0; $i<=2; $i++){
$datos= array('ver_IDversion'=>null,'ver_nombre'=>'','ver_precio'=>'','ver_financiera'=>'','ver_enganche'=>'','ver_seguro_auto'=>'','ver_seguro_vida'=>'','ver_apertura'=>'','ver_placas_tenecia'=>'','ver_extencion'=>'','ver_seguro'=>'','ver_bono'=>'','ver_total'=>'','ver_mensualidad'=>'','ver_plazo'=>'','ver_cotIDcotizador'=>$IDCOT,'ver_por_enganche'=>'');

$this->db->insert('versio_cotizacion', $datos);	
}

$datos_lista_espera = array('lsp_IDlista_espera_cotizacion'=>null,'lsp_nombre'=>'','lsp_apellido'=>'','lsp_email'=>'','lsp_IDcotizacion'=>$IDCOT,'lsp_fecha'=>''.date('Y-m-d').'','lsp_IDhuser'=>$_SESSION['id'],'lsp_status'=>'listado');

$this->db->insert('lista_espera_cotizacion', $datos_lista_espera);
$query=$this->db->query('select LAST_INSERT_ID() as IDLC from lista_espera_cotizacion');
$result = $query->result();
$IDLC=$result[0]->IDLC;	


return $IDCOT.'-'.$IDLC;
}
function getListaLC($id)
{
	  $query = $this->db->query('Select * from lista_espera_cotizacion where  lsp_IDlista_espera_cotizacion='.$id.' ');		
        return $query->result();
}

function getListaCotizacionHuser(){
	$query = $this->db->query('Select * from lista_espera_cotizacion,cotizador,huser,ficha_modelo where  lsp_IDcotizacion=cot_IDcotizador and fmo_IDficha_modeo=cot_IDficha and lsp_IDhuser=hus_IDhuser and hus_IDhuser='.$_SESSION['id'].'  and lsp_status="listado" ');		
        return $query->result();
	
	}

function getInfoCotizacion($id){
	$query = $this->db->query('Select * from lista_espera_cotizacion,cotizador,huser,ficha_modelo where  lsp_IDcotizacion=cot_IDcotizador and fmo_IDficha_modeo=cot_IDficha and lsp_IDhuser=hus_IDhuser and lsp_IDlista_espera_cotizacion='.$id.' ');		
        return $query->result();
	
	}
	
	function getListaCotizacionHuserCdAll(){
	$query = $this->db->query('Select * from lista_espera_cotizacion,cotizador,huser,ficha_modelo where  lsp_IDcotizacion=cot_IDcotizador and fmo_IDficha_modeo=cot_IDficha and lsp_IDhuser=hus_IDhuser 
and hus_ciudad IN ("Tijuana","Mexicali","Ensenada")
and lsp_status="listado"
order by (cot_fecha) DESC ');		
        return $query->result();
	
	}
	
	function getListaCotizacionHuserCd(){
	$query = $this->db->query('Select * from lista_espera_cotizacion,cotizador,huser,ficha_modelo where  lsp_IDcotizacion=cot_IDcotizador and fmo_IDficha_modeo=cot_IDficha and lsp_IDhuser=hus_IDhuser and hus_ciudad="'.$_SESSION['ciudad'].'"  and lsp_status="listado"
order by (cot_fecha) DESC	
	 ');		
        return $query->result();
	
	}
	
function CreateCotizacion($idc){
$datos_cotizacion = array('cot_IDcotizador'=>null,'cot_IDcontacto'=>$idc,'cot_email'=>'','cot_IDficha'=>0,'cot_precioex1'=>'','cot_precioex2'=>'','cot_precioex3'=>'','cot_precioex4'=>0,'cot_precioex5'=>0,'cot_precioex6'=>0,'cot_servicio1'=>0,'cot_servicio2'=>0,'cot_servicio3'=>0,'cot_precio1'=>0,'cot_precio2'=>0,'cot_precio3'=>0,'cot_fecha'=>''.date('Y-m-d').'','cot_nota'=>'');

$this->db->insert('cotizador', $datos_cotizacion);
$query=$this->db->query('select LAST_INSERT_ID() as IDC from contacto');
$result = $query->result();
$IDCOT=$result[0]->IDC;

for($i=0; $i<=2; $i++){
$datos= array('ver_IDversion'=>null,'ver_nombre'=>'','ver_precio'=>'','ver_financiera'=>'','ver_enganche'=>'','ver_seguro_auto'=>'','ver_seguro_vida'=>'','ver_apertura'=>'','ver_placas_tenecia'=>'','ver_extencion'=>'','ver_seguro'=>'','ver_bono'=>'','ver_total'=>'','ver_mensualidad'=>'','ver_plazo'=>'','ver_cotIDcotizador'=>$IDCOT,'ver_por_enganche'=>'');

$this->db->insert('versio_cotizacion', $datos);	
}

return $IDCOT;
}	
	

function InsertSet($id,$idc)
	{
			
	$con=$this->Contactomodel->getNameContacto($idc);
	$idf=$con[0]->fuente_fue_IDfuente;
		
	$id=$id+1;
	$fe=date('Y-m-d');
	
	$datos=array('dat_IDdatos'=>$id,
	'dat_fecha_facturacion'=>'',
	'dat_fecha_entrega'=>'',
	'dat_hora'=>'',
	'dat_garantia'=>'',
	'dat_pedimento'=>''
	);
	
$datos_cliente =array('datc_IDdatos_clientes'=>$id,'datc_nombrecompleto'=>null,'datc_primernombre'=>null,'datc_segundonombre'=>null,'datc_apellidopaterno'=>null,'datc_apellidomaterno'=>null,'datc_direccion'=>null,'datc_calle'=>null,'datc_colonia'=>null,'datc_ciudad'=>null,'datc_estado'=>null,'datc_municipio'=>null,'datc_cp'=>null,'datc_rfc'=>null,'datc_tcasa'=>null,'datc_tcelular'=>null,'datc_oficinia'=>null,'datc_fax'=>null,'datc_radio'=>null,'datc_email'=>null,'datc_fechanac'=>'0000-00-00','datc_aniversario'=>'0000-00-00','datc_persona_fisica_moral'=>'','datc_moral'=>'','datc_parentesco'=>'','datc_mismadireccion'=>'','datc_moral_nombre'=>'','datc_moral_telefono'=>'','datc_moral_telefono2'=>'');
	
$datos_auto = array('data_'=>$id,'data_auto'=>null,'data_carroceria'=>null,'data_capacidad'=>null,'data_placas'=>null,'data_km'=>null,'data_marca'=>null,'data_modelo'=>null,'data_version'=>null,'data_ano'=>null,'data_color'=>null,'data_motor'=>null,'data_no_motor'=>null,'data_llaves'=>null,'data_importacion'=>null,'data_vin'=>null,'data_codigoderadio'=>null);
	
$datos_seguro = array('dats_IDdatos_seguro'=>$id,'dats_compra'=>null,'dats_financiera'=>null,'dats_financiadoanual'=>null,'dats_financiadomultianual'=>null,'dats_contadoanual'=>null,'dats_contadomultianual'=>null,'dats_financiadosemestral'=>null,'dats_contadosemestral'=>null,'dats_factura'=>null,'dats_tipomoneda'=>null,'dats_valorfactura'=>null,'dats_cantidadletra'=>null,'dats_enganche'=>null,'dats_monto'=>null,'dats_tipocambio'=>null,'dats_taza'=>null,'dats_plazo'=>null,'dats_mensualidad'=>null,'dats_pagaresnum'=>null,'dats_pagarescant'=>null,'dats_pagaresvencimiento'=>null,'dats_tipocaic'=>null,'dats_tipognp'=>null,'dats_tiipoaig'=>null,'dats_accfactura'=>null,'dats_mosrefacc'=>null);
		
$datos_documentos_recibidos = array('datdr_datos_documentos_recibidos'=>$id,'datdr_facoriginal'=>null,'datdr_coipafact'=>null,'datdr_facaccesorios'=>null,'datdr_facplacas'=>null,'datdr_cartafact'=>null,'datdr_manualgarantia'=>null,'datdr_manual_deusuario'=>null,'datdr_poliza'=>null,'datdr_msautomotriz'=>null,'datdr_manualdeasistemcia'=>null,'datdr_profeco'=>null,'datdr_llaveprincipal'=>null,'datdr_llavevalet'=>null,'datdr_codradio'=>null,'datdr_llantarefa'=>null,'datdr_llacontrolrepuerta'=>null,'datdr_extintor'=>null,'datdr_cablecorr'=>null,'datdr_gatohydraulico'=>null,'datdr_triangulodeseguridad'=>null,'datdr_paqherra'=>null);

$datos_compra = array('datco_IDdatos_compra'=>$id,'datco_marca'=>null,'datco_modelo'=>null,'datco_version'=>null,'datco_ano'=>null,'datco_color'=>null,'datco_nocompra'=>null,'datco_noserie'=>null,'datco_garantiaanos'=>null,'datco_garacntiakm'=>null,'datco_nuevo'=>null,'datco_snuevo'=>null,'datco_nuevox'=>null);
			
$avisodepedido = array('idavisodepedido' => $id,'avi_acessorios' => NULL,'avi_gastosadicionales' => NULL,'avi_recomprausado' => NULL,'avi_totalenganche' => NULL,'avi_saldoapagarbanco' => NULL,'avi_totaloperacion' => NULL,'avi_seguronac' => NULL,'avi_seguroes' => NULL,'avi_segurovida' => NULL,'avi_comisionapertura' => NULL,'avi_placastenecia' => NULL,'avi_tramites' => NULL,'avi_intereses' => NULL,'avi_carroceria' => NULL,'avi_accesoriosanexarpedido' => NULL,'avi_segurodesempleo' => NULL,'avi_certifigarantia' => NULL,'avi_observaciones' => NULL,'avi_desglosariva' => NULL,'avi_tipodecambo' => NULL,'avi_gseminuevo' => NULL,'avi_asesorventa' => NULL,'avi_ggeneral' => NULL,'avi_contabilida' => NULL,'avi_gventas' => NULL,'avi_credito' => NULL,'avi_director' => NULL,'avi_ivaonce' => '');

$dirfacturacion=array('dfa_IDdirfacturacion'=>$id,'dfa_calle' => '','dfa_colonia' => '','dfa_ciudad' =>'','dfa_estado' =>'','dfa_municipio' => '','dfa_codigopostal' => '');

$dordendencomprat = array('dor_IDdordencomprat'=>$id,'dor_nombre'=>$idf,'datos_dat_IDdatos'=>$id,'datos_auto_data_'=>$id,'datos_cliente_datc_IDdatos_clientes'=>$id,'datos_seguro_dats_IDdatos_seguro'=>$id,'datos_documentos_recibidos_datdr_datos_documentos_recibidos'=>$id,'datos_compra_datco_IDdatos_compra'=>$id,'contacto_con_IDcontacto'=>$idc,'dor_fecha'=>''.$fe.'','avisodepedido_idavisodepedido'=>$id,'datos_dirfacturacion'=>$id,'datos_hus_IDhuser'=>$_SESSION['id']);


$procesodeventa=array(
            'prcv_IDprocesodeventa'=>$id,
			'prcv_fecha'=>date('Y-m-d'),
			'prcv_IDordencompra'=>$id,
			'prcv_status'=>'proceso',
			'prcv_prcv_status_credito_contado'=>'proceso',
			'prcv_status_gerente'=>'proceso',
			'prcv_status_fi'=>'proceso',
			'prcv_status_contabilidad'=>'proceso',
			'prcv_director'=>'proceso',
			'prcv_director_gral'=>'proceso',
			'prcv_status_asesor'=>'proceso'
			);
	
	
	 $this->db->insert('datos', $datos); 
	 $this->db->insert('datos_cliente', $datos_cliente); 
	 $this->db->insert('datos_auto', $datos_auto); 
	 $this->db->insert('datos_seguro ', $datos_seguro); 
	 $this->db->insert('datos_documentos_recibidos', $datos_documentos_recibidos); 
	 $this->db->insert('datos_compra', $datos_compra); 
	  $this->db->insert('avisodepedido', $avisodepedido); 
	  $this->db->insert('dirfacturacion', $dirfacturacion);
	  $this->db->insert('procesodeventa', $procesodeventa);
	  $this->db->insert('dordendencomprat', $dordendencomprat); 
	 
		
    }
	
	function getDataPdf()
    {
        $result = $this->db->query('SELECT *
FROM `table 248`');		
        return $result->result();
		}
	function getTodocontactoax()
    {
        $result = $this->db->query('Select * from contacto where con_status != "eliminado" ');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			 $return['general'] = 'Seleccione una opcion';
            $return[$row['con_IDcontacto']] = ucwords(strtolower($row['con_nombre'].' '.$row['con_apellido']));
        }
    }
    return $return;
		}


function getTodocontactoaxx()
    {
        $result = $this->db->query('Select * from contacto where con_status != "eliminado" ');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			 $return[''] = 'Seleccione una opcion';
            $return[$row['con_IDcontacto']] = $row['con_nombre'].' '.$row['con_apellido'];
        }
    }
    return $return;
		}
		
		
		function getAutosApartados($idc){
			
			  $query = $this->db->query('Select * from asigancion_auto where contacto_con_IDcontacto='.$idc.' and aau_status="Apartado" ');		
        return $query->result();
			
			}
			
			function getDataAuto($ida){
			
			  $query = $this->db->query('Select * from asigancion_auto where aau_IDasignacion_auto='.$ida.' and aau_status="Apartado" ');		
        return $query->result();
			
			}
	
	
	function insertVenta($todo)
	{$this->db->insert('venta', $todo);}
	
	function insertcredito($todo)
	{$this->db->insert('solicitudcredito', $todo);}
	
	function UpdateCotizaciontoDataBase($idglobal,$idcon){
		list($idl,$idc)=explode('-',$idglobal);
$query = $this->db->query('update lista_espera_cotizacion set lsp_status="transferido" where lsp_IDlista_espera_cotizacion='.$idl.'');
$query = $this->db->query('update cotizador set cot_IDcontacto='.$idcon.' where cot_IDcotizador='.$idc.'');	
		}
		
		function deleteListaCotizacion($idglobal){
		list($idl,$idc)=explode('-',$idglobal);
$query = $this->db->query('delete from lista_espera_cotizacion  where lsp_IDlista_espera_cotizacion='.$idl.'');
$query = $this->db->query('delete from cotizador  where cot_IDcotizador='.$idc.'');	
		}
	
	function updateVenta($ido,$idh,$ida)
	{
	$query = $this->db->query('update ventas set ven_status="Cancelada" where oportunidades_opo_IDoportunidad='.$ido.' and huser_hus_IDhuser='.$idh.' ');
	//echo 'update asigancion_auto set aau_status="En Proceso" where auto_aut_IDauto_vin='.$ida.'';
	 $this->db->query('update asigancion_auto set aau_status="En Proceso" where auto_aut_IDauto_vin='.$ida.'');
	 $this->db->query('update auto set aut_status="Reservado" where aut_IDauto_vin='.$ida.'');
	
	}
	
		
		function getTodocontactoaxv($id)
    {
        $result = $this->db->query('Select * from contacto where con_status != "eliminado" and huser_hus_IDhuser='.$id.'');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			$return['generales']='Seleccione una opcion';
			$return[$row['con_IDcontacto']] = ucwords(strtolower($row['con_nombre'].' '.$row['con_apellido']));
        }
    }
	else{
		$return['generales']='Seleccione una opcion';
		}
    return $return;
		}
		
		
		function getListaFichas()
		{
        $result = $this->db->query('Select * from ficha_modelo ');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			$return['0']='Seleccione una opcion';
			$return[$row['fmo_IDficha_modeo']] = $row['fmo_nombre'];
        }
    }
	else{
		$return['generales']='Seleccione una opcion';
		}
    return $return;
		}
	
	function getTodocontactoOpciones($id)
    {
        $result = $this->db->query('Select * from contacto where con_status != "eliminado" and huser_hus_IDhuser='.$id.'');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			$return['todas']='Todos Las Actividades';
			$return['tgenerales']='Solo Tareas Generales';
			$return[$row['con_IDcontacto']] = $row['con_nombre'].' '.$row['con_apellido'];
        }
    }
	else{
		$return['todas']='Seleccione una opcion';
		}
    return $return;
		}
	
	
	
	function transfer($bb,$id)
    {
        $query = $this->db->query('update contacto set huser_hus_IDhuser='.$id.' where con_IDcontacto='.$bb.'');		
       
    }
	 function getTodocontactoav($cd)
    {
        $query = $this->db->query('Select * from contacto,huser where con_status != "eliminado" and hus_IDhuser=huser_hus_IDhuser and hus_ciudad="'.$cd.'" ORDER BY `hus_nombre` ASC ');		
        return $query->result();
    }
	
	function getTodocontactoV($id)
    {
        $query = $this->db->query('Select * from contacto,huser,bitacora where  con_status IN  ("Prospecto") and huser_hus_IDhuser='.$id.' and hus_IDhuser=huser_hus_IDhuser and contacto_con_IDcontacto=con_IDcontacto group by(con_IDcontacto) ORDER BY `con_nombre` ASC ');		
        return $query->result();
    }
	
	function getTodocontactoVV($id)
    {
        $query = $this->db->query('Select * from contacto,huser,bitacora where  con_status != "eliminado"  and huser_hus_IDhuser='.$id.' and hus_IDhuser=huser_hus_IDhuser and contacto_con_IDcontacto=con_IDcontacto group by(con_IDcontacto) ORDER BY `con_nombre` ASC ');		
        return $query->result();
    }
	
	function getTodocontactoVTC($id,$sta)
    {
    
    	$dosd=$sta;
		if($dosd=='Prospecto'){$dosd='con_status IN  ("Prospecto")';}
		if($dosd=='Cliente'){$dosd='con_status IN  ("Cliente")';}
		if($dosd=='Caliente'){$dosd='con_status IN  ("Caliente")';}
		if($dosd=='Caida'){$dosd='con_status IN  ("Caida")';}
		if($dosd=='Proceso%20de%20Venta'){$dosd='con_status IN  ("Proceso de Venta")';}
		if($dosd=='Todos'){$dosd='con_status IN  ("Cliente","Prospecto","Caliente","Proceso de Venta","Caida")';}
		
		 $qry='Select * from contacto,huser,bitacora 
		 where '.$dosd.' 
		 and hus_IDhuser='.$id.' 
		 and hus_IDhuser=huser_hus_IDhuser 
		 and contacto_con_IDcontacto=con_IDcontacto
		  group by(con_IDcontacto)
		 ORDER BY `con_IDcontacto` DESC ';
        $query = $this->db->query($qry);		
        return $query->result();
    }
	
	
	function getTodocontactoVTCCD($id,$TC)
    {
    
    	if($TC=='Cliente'){$DES='Cliente';}
		if($TC=='Proceso%20de%20Venta'){$DES='Proceso de Venta';}
		if($TC=='Caliente'){$DES='Caliente';}
		if($TC=='Caida'){$DES='Caida';}
		if($TC=='Prospecto'){$DES='Prospecto';}
		if($TC=='Todos'){$DES='';}
		 $qry='Select * from contacto,huser,bitacora where con_status != "eliminado" and  con_status= "'.$DES.'" and hus_ciudad="'.$id.'" and hus_IDhuser=huser_hus_IDhuser and contacto_con_IDcontacto=con_IDcontacto group by(con_IDcontacto)  group by(con_IDcontacto)
		 ORDER BY `con_IDcontacto` DESC ';
        $query = $this->db->query($qry);		
        return $query->result();
    }
	
	
	function getgenerales($id)
    {
        $query = $this->db->query('Select * from generales where huser_hus_IDhuser='.$id.' ');		
        return $query->result();
    }
	function gettareas($id)
    {
        $query = $this->db->query('Select * from actvidades where act_IDactividades='.$id.' ');		
        return $query->result();
    }
	
    
	function getcontacto($id)
	{
	$query = $this->db->query('Select * from contacto where con_IDcontacto='.$id.' ');		
        return $query->result();
		
	}
	
	function getcontactobitacora($id)
	{
	
	$query = $this->db->query('Select * from contacto,huser,fuente,bitacora where fuente_fue_IDfuente=fue_IDfuente  and  con_IDcontacto='.$id.' and huser_hus_IDhuser=hus_IDhuser and contacto_con_IDcontacto=con_IDcontacto ');		
        return $query->result();
		
    }
	
	function getActividades($id)
	{
	$qu='Select * from actvidades,bitacora,tipo_actividad where bit_IDbitacora=bitacora_bit_IDbitacora and  bitacora_bit_IDbitacora='.$id.' and tac_IDtipo_actividad=tipo_actividad_tac_IDtipo_actividad     ORDER BY `actvidades`.`act_IDactividades` DESC ';
	$query = $this->db->query($qu);		
        return $query->result();
		
    }
	
	function getActividadesbus($id,$fe1,$fe2,$esta,$act)
	{
	
	if($esta!='testados'){ $uno="and act_status='$esta'";}else{$uno='';}
if($act!='tactividades'){ $dos="and tac_IDtipo_actividad='$act'";}else{$dos='';}
		
	 $qu='Select * from actvidades,bitacora,tipo_actividad where bit_IDbitacora=bitacora_bit_IDbitacora and  bitacora_bit_IDbitacora='.$id.' and tac_IDtipo_actividad=tipo_actividad_tac_IDtipo_actividad  and 
	
	act_fecha_inicio between "'.$fe1.'" and "'.$fe2.'" '.$uno.'  '.$dos.'
	
	    ORDER BY `actvidades`.`act_IDactividades` DESC ';
	$query = $this->db->query($qu);		
        return $query->result();
		
    }
	
	function getRegistro($id)
	{
	$qu='Select * from bitacora where  bit_IDbitacora='.$id.'  ';
	$query = $this->db->query($qu);		
        return $query->result();
		
    }
	
	function getActividadesview($id)
	{
	
	$query = $this->db->query('Select * from actvidades where act_IDactividades='.$id.' ');		
        return $query->result();
		
    }
	
	function gettareasgenerales($idt)
	{
	
	$query = $this->db->query('Select * from generales where gen_IDgenerales='.$idt.'  ');		
        return $query->result();
		
	}
	
	
	 function getOportunidades($idb)
    {
        $query = $this->db->query('Select opo_IDoportunidad,opo_fecha,bitacora_bit_IDbitacora,t_oportunidades_t_o_IDt_oportunidades,opo_status,opo_user_historia,opo_auto_vin,t_o_nombre,mod_nombre 
from oportunidades,t_oportunidades,auto,modelos
where bitacora_bit_IDbitacora='.$idb.' 
and t_oportunidades_t_o_IDt_oportunidades=t_o_IDt_oportunidades
and opo_auto_vin=aut_IDauto_vin
and modelos_mod_IDmodelos=mod_IDmodelos		
		');		
        return $query->result();
    }
	
	
	 function getOportunidadesV($idb)
    {
        $query = $this->db->query('Select opo_IDoportunidad,opo_fecha,bitacora_bit_IDbitacora,t_oportunidades_t_o_IDt_oportunidades,opo_status,opo_user_historia,opo_auto_vin,t_o_nombre,mod_nombre 
from oportunidades,t_oportunidades,auto,modelos
where opo_IDoportunidad='.$idb.' 
and t_oportunidades_t_o_IDt_oportunidades=t_o_IDt_oportunidades
and opo_auto_vin=aut_IDauto_vin
and modelos_mod_IDmodelos=mod_IDmodelos		
		');		
        return $query->result();
    }
	
	function getsolicitudcredito($id)
	    {
        $query = $this->db->query('Select *
from solicitudcredito
where src_con_IDcontacto='.$id.'
		');		
        return $query->result();
    }
	
	
	function getOportunidadesAdd($idc)
    {
        $result = $this->db->query('Select opo_IDoportunidad,t_o_nombre,mod_nombre,col_nombre
		
		from oportunidades,t_oportunidades,bitacora,contacto,auto,modelos,color
		where con_IDcontacto='.$idc.'
		and contacto_con_IDcontacto=con_IDcontacto
		and bit_IDbitacora=bitacora_bit_IDbitacora
		and t_oportunidades_t_o_IDt_oportunidades=t_o_IDt_oportunidades
		and opo_auto_vin=aut_IDauto_vin
		and modelos_mod_IDmodelos=mod_IDmodelos
		and color_col_IDcolor=col_IDcolor
		');		
        
		
		
    $return = array();
    if($result->num_rows() > 0){
            $return['generales'] = 'Actividades Generales';
        foreach($result->result_array() as $row){
            $return[$row['opo_IDoportunidad']] = $row['t_o_nombre'].' - '.$row['mod_nombre'].' '.$row['col_nombre'];
        }
    }
    return $return;
		
		
		
    }
	
	
	function gettoportunidades()
    {
        $result = $this->db->query('Select * from t_oportunidades');		
    $return = array();
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
            $return[$row['t_o_IDt_oportunidades']] = $row['t_o_nombre'];
        }
    }
    return $return;
		
		
		
    }
	
	
	function getOportunidadesUno($id)
	{
	  $query = $this->db->query('Select * from oportunidades where opo_IDoportunidad='.$id.'');		
      return $query->result();
		
	}
	
	
	
	function getT($id)
	{
	$query = $this->db->query('select con_nombre from contacto where con_IDcontacto='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
	}

function getNamePublicidad($id)
	{
	$query = $this->db->query('select pub_nombre from publicidad where pub_IDpublicidad='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
	}
	
	
	function get($id)
	{
	$query = $this->db->query('select * from contacto where con_IDcontacto='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result;	
	}
	
	function idBitacora($id)
	{
	$query = $this->db->query('select bit_IDbitacora from contacto,bitacora where contacto_con_IDcontacto=con_IDcontacto and con_IDcontacto='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
	}
	
	function getIdContacto($id)
	{
	$query = $this->db->query('select con_IDcontacto from contacto,bitacora where contacto_con_IDcontacto=con_IDcontacto and bit_IDbitacora='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
	}
	
	function getIdContactoName($id)
	{
	$query = $this->db->query('select con_nombre,con_apellido from contacto,bitacora where contacto_con_IDcontacto=con_IDcontacto and bit_IDbitacora='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result;
	}

	
	function vbitacora($id)
	{
	$query = $this->db->query('select bit_IDbitacora from bitacora where contacto_con_IDcontacto='.$id.'');  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];	
	}
	
	
	function addactividad($id)
	{
		



$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+1 days", $fechaComparacion); //Le restamos 15 dias
$undia=date("Y-m-d", $calculo);
$todoa= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$undia,
					'act_fecha_fin'=>$undia,
					'act_titulo'=>'Llamada Telefonica',
					'act_descripcion'=>'Llamada Telefonica , Alguna duda ?, todo ok ?, hacer breve cuestionario',
					'act_hora'=>9,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
					

$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+31 days", $fechaComparacion); //Le restamos 15 dias
$unmes=date("Y-m-d", $calculo);					
$todob= array(
                    'act_status'=>'realizada',
					'act_fecha_inicio'=>$unmes,
					'act_fecha_fin'=>$unmes,
					'act_titulo'=>'Carta de agradecimiento',
					'act_descripcion'=>'Carta de agradecimiento de gerentes, de servicio y del asesor de ventas',
					'act_hora'=>10,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );	
					
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+92 days", $fechaComparacion); //Le restamos 15 dias
$tresmeses=date("Y-m-d", $calculo);						
					
					$todoc= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$tresmeses,
					'act_fecha_fin'=>$tresmeses,
					'act_titulo'=>'Llamada telefonica',
					'act_descripcion'=>'Todo ok ?, Kilometraje ?',
					'act_hora'=>11,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );	
					
					
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+186 days", $fechaComparacion); //Le restamos 15 dias
$seismeses=date("Y-m-d", $calculo);
					
							$todod= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$seismeses,
					'act_fecha_fin'=>$seismeses,
					'act_titulo'=>'Llamada telefonica',
					'act_descripcion'=>'Gracias Por traer su auto a servicio !',
					'act_hora'=>9,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );	
					
					
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+365 days", $fechaComparacion); //Le restamos 15 dias
$unano=date("Y-m-d", $calculo);					
					
							$todoe= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$unano,
					'act_fecha_fin'=>$unano,
					'act_titulo'=>'Llamada telefonica',
					'act_descripcion'=>'Invitacion a un evento',
					'act_hora'=>12,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );		
					
					
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+31 days", $fechaComparacion); //Le restamos 15 dias
$unmesr=date("Y-m-d", $calculo);						
					
						$todof= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$unmesr,
					'act_fecha_fin'=>$unmesr,
					'act_titulo'=>'Visita Regalo',
					'act_descripcion'=>'Visitar a su cliente para entregar regalo y solicitar recomendaciones.',
					'act_hora'=>13,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );					

					
					$datafin=date('Y').'-'.date('m').'-'.date('d');
						$todog= array(
                    'act_status'=>'realizada',
					'act_fecha_inicio'=>$datafin,
					'act_fecha_fin'=>$datafin,
					'act_titulo'=>'Auto entregado',
					'act_descripcion'=>'Auto entregado, ahora es un cliente.',
					'act_hora'=>13,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );					

 $this->db->insert('actvidades', $todoa);
  $this->db->insert('actvidades', $todob);
   $this->db->insert('actvidades', $todoc);
    $this->db->insert('actvidades', $todod);
	 $this->db->insert('actvidades', $todoe);
	  $this->db->insert('actvidades', $todof);
	  $this->db->insert('actvidades', $todog);

	 
		
    }
	
	
	
	
	
	
	
	function addbitacora($id)
	{
	$fe=date('d/m/Y');list($di,$ms,$an)=explode('/',$fe);$fecha=$an.'-'.$ms.'-'.$di;
	$todo=array('bit_fecha'=>$fecha,
	'contacto_con_IDcontacto'=>$id,
	);
	 $this->db->insert('bitacora', $todo);
		
    }
	
	
	
   function getfuente()
    {
    $query = $this->db->get_where('fuente');		
   

 $result = $query->result();
     return $result;
	 
	 /*
    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['fue_IDfuente']] = $row['fue_nombre'];
        } 
    }
    return $return;*/
	
	 }




 function getCotizacion($id){
   $query = $this->db->query('SELECT * 
FROM  `cotizador`,ficha_modelo,contacto  where cot_IDcotizador='.$id.' and fmo_IDficha_modeo=cot_IDficha and cot_IDcontacto=con_IDcontacto'); 
   $result = $query->result();
   return $result;}
	
   function getAsesor(){
   $query = $this->db->query('select * from huser where hus_ciudad="Tijuana"'); 
   $result = $query->result();
   return $result;}
   
   function getAsesore(){
   $query = $this->db->query('select * from huser where hus_ciudad="Ensenada"'); 
   $result = $query->result();
   return $result;}
   
   
   
   function getAsesorT($id){
   $query = $this->db->query('select * from huser where hus_IDhuser='.$id.''); 
   $result = $query->result();
   return $result;}
   
   function getListaVersiones($id){
	$query = $this->db->query('select * from versio_cotizacion where ver_cotIDcotizador='.$id.''); 
   $result = $query->result();
   return $result;}
	
	
	
	 function getAsesorm()
    {
    	
		 $query = $this->db->query('select * from huser where hus_ciudad="Mexicali"'); 

   

 $result = $query->result();
     return $result;
	 
	 /*
    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['fue_IDfuente']] = $row['fue_nombre'];
        } 
    }
    return $return;*/
	
	 }


 function login($user,$pass)
    {
    	
$query = $this->db->query('select hus_IDhuser from huser where hus_correo="'.$user.'" and hus_contrasena=md5("'.$pass.'")'); 
  return  $result = $query->result();

	
	 }
	 
	 function Getfechaentrega($cd){
		 $query = $this->db->query('
		select * from  fechaentrega,dordendencomprat,contacto,huser,datos_auto
		 where fen_Id_Ordendecompra=dor_IDdordencomprat
		 and contacto_con_IDcontacto=con_IDcontacto
		 and huser_hus_IDhuser=hus_IDhuser
		 and hus_ciudad="'.$cd.'"
		 and datos_auto_data_=data_
		 '); 
  return  $result = $query->result();
		 
		 }
	 
	 function logingerente($user,$pass,$tipo)
    {
    	
$query = $this->db->query('select hus_IDhuser from huser where hus_correo="'.$user.'" and hus_contrasena=md5("'.$pass.'") and hus_nivel="'.$tipo.'"'); 
  return  $result = $query->result();

	
	 }
	 
	  function loginasesor($user,$pass)
    {
    	
$query = $this->db->query('select hus_IDhuser from huser where hus_correo="'.$user.'" and hus_contrasena=md5("'.$pass.'") '); 
  return  $result = $query->result();

	
	 }
	 
	 
	 function Getprocesodeventa($cd){
		 
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_cliente
		 where prcv_status="proce"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and hus_ciudad="'.$cd.'"
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto 
		  and prcv_status_asesor="aprobado"	
		   and datos_dat_IDdatos=dat_IDdatos
		   and data_=datos_auto_data_
		   and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
		 '); 
  return  $result = $query->result();
		 
		 }
		 
		  function Getprocesodeventaconta(){
		 
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_cliente
		 where prcv_status="proce"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and hus_ciudad IN ("Tijuana","Ensenada")
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto 
		  and prcv_status_asesor="aprobado"	
		  and datos_dat_IDdatos=dat_IDdatos
		  and data_=datos_auto_data_
		  and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
		 '); 
  return  $result = $query->result();
		 
		 }
		 
		 function GetprocesodeventacontaEntregados(){
		 
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto
		 where prcv_status="venta"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and hus_ciudad IN ("Tijuana","Ensenada")
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto 
		  and prcv_status_asesor="aprobado"	
		  and datos_dat_IDdatos=dat_IDdatos
		  and data_=datos_auto_data_
		 '); 
  return  $result = $query->result();
		 
		 }
		 
		  function GetprocesodeventaAdmin(){
		 
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_cliente 
		 where prcv_status="proce"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and hus_ciudad IN ("Tijuana","Ensenada","Mexicali")
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto 
		  and prcv_status_asesor="aprobado"	
		  and datos_dat_IDdatos=dat_IDdatos
		  and data_=datos_auto_data_
		  and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
		 '); 
  return  $result = $query->result();
		 
		 }
		  function GetprocesodeventaAdminEntregados(){
		 
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto 
		 where prcv_status="venta"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and hus_ciudad IN ("Tijuana","Ensenada","Mexicali")
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto 
		  and prcv_status_asesor="aprobado"	
		  and datos_dat_IDdatos=dat_IDdatos
		  and data_=datos_auto_data_
		 '); 
  return  $result = $query->result();
		 
		 }
		 
		 
		 
		  function GetprocesodeventaHuser($cd){
		 $query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,bitacora,datos,datos_auto,datos_cliente
		 where prcv_status="proce"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and hus_IDhuser="'.$cd.'"
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto
		 and prcv_status_asesor="aprobado" 	
		 and datos_dat_IDdatos=dat_IDdatos
		 and data_=datos_auto_data_
		 and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
		 '); 
  return  $result = $query->result();
		 
		 }
		 
		  function Getprocesodeventadd($ido){
		
		$qu='
		 select prcv_IDordencompra from procesodeventa,dordendencomprat,contacto,huser,bitacora
		 where prcv_status="proce"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and prcv_IDordencompra="'.$ido.'"
		 and bitacora.contacto_con_IDcontacto=con_IDcontacto 	
		 ';	  
			  
		  $query = $this->db->query($qu); 
  return  $result = $query->result();
		 
		 }
		 
function getOrdendeCompraPdf($id){
$query = $this->db->query('
Select * from dordendencomprat,datos,datos_auto,datos_cliente,datos_seguro,datos_documentos_recibidos,datos_compra,avisodepedido,dirfacturacion,contacto,huser

 where
dor_IDdordencomprat='.$id.'
and datos_dat_IDdatos=dat_IDdatos
and datos_auto_data_=data_
and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
and datos_seguro_dats_IDdatos_seguro=dats_IDdatos_seguro
and datos_documentos_recibidos_datdr_datos_documentos_recibidos=datdr_datos_documentos_recibidos
and datos_compra_datco_IDdatos_compra=datco_IDdatos_compra 
and idavisodepedido=avisodepedido_idavisodepedido 
and datos_dirfacturacion=dfa_IDdirfacturacion
and contacto_con_IDcontacto=con_IDcontacto
and huser_hus_IDhuser=hus_IDhuser
		 
		 '); 
  return  $result = $query->result();	
	
	}	
	
	function getPagosOrden($id){
		$query = $this->db->query('
		Select * from nopagos where avisodepedido_idavisodepedido='.$id.'
		 '); 
  return  $result = $query->result();
		}
		
		function getPagosOrdenBan($id){
		$query = $this->db->query('
		Select * from nopagos where avisodepedido_idavisodepedido='.$id.' and nop_tipo="Banco"
		 '); 
  return  $result = $query->result();
		}
		
		function getPagosOrdenAut($id){
		$query = $this->db->query('
		Select * from nopagos where avisodepedido_idavisodepedido='.$id.'  and nop_tipo="Auto"
		 '); 
  return  $result = $query->result();
		}	 
		 



function Getprocesodeventadetalle($dd){
		$query = $this->db->query('
		 select * from procesodeventa,dordendencomprat,contacto,huser,datos_auto,datos_compra,datos
		 where prcv_status="proce"
		 and dor_IDdordencomprat=prcv_IDordencompra
		 and contacto_con_IDcontacto=con_IDcontacto
		 and hus_IDhuser=huser_hus_IDhuser
		 and prcv_IDprocesodeventa='.$dd.'
		 and datos_auto_data_=data_
		 and datco_IDdatos_compra=datos_compra_datco_IDdatos_compra
		 and dat_IDdatos=datos_dat_IDdatos
		 
		 '); 
  return  $result = $query->result();
		 
		 }


function getpublicidad()
    {
    $query = $this->db->get_where('publicidad');		
    $result = $query->result();
     return $result;

    /*$return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['pub_IDpublicidad']] = $row['pub_nombre'];
        }
    }
    return $return; */
	
	 }
	 
	 
	  function getestado()
    {
    $result = $this->db->get_where('estados');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione una opcion';
        foreach($result->result_array() as $row){
            $return[$row['est_IDestado']] = $row['est_nombre'];
        }
    }
    return $return;
	
	 }
   
   
   
   
   function addContacto($todo)
    {
		
     $this->db->insert('contacto', $todo); 
	 $query=$this->db->query('select LAST_INSERT_ID() as IDC from contacto');
	 $result = $query->result();
     return $result[0];
    }
	
	function insertseguimiento($todo,$id)
    {
	   $this->db->update('procesodeventa', $todo, array('prcv_IDprocesodeventa' => $id));
            return true;
	
    }
	
	function updatefechafacturacion($todo,$id){
		 $this->db->update('datos', $todo, array('dat_IDdatos' => $id));
            return true;
		}
	
	function insertfechaentrega($todo)
	    {
		
     $this->db->insert('fechaentrega', $todo); 
	
    }
	function inserttareag($td)
    {
		
       $this->db->insert('generales', $td); 
    }
	
	function inserttareagB($td,$id)
    {
		 $this->db->delete('actvidades', array('act_IDactividades' => $id));
         $this->db->insert('generales', $td); 
    }
	
	function deleteavisodepedido($id)
    {
		 $this->db->delete('procesodeventa', array('prcv_IDordencompra' => $id));
         
    }
	function deleteCotizacion($id){
		 $this->db->delete('cotizador', array('cot_IDcotizador' => $id));
		}
	function deletecredito($id)
	{
		 $this->db->delete('solicitudcredito', array('scr_IDsolicitudcredito' => $id));
         
    }
	function insertactividad($td,$id)
    {
		 $this->db->delete('generales', array('gen_IDgenerales' => $id));
         $this->db->insert('actvidades', $td); 
    }
	
	
	function inserttareaa($td)
    {
		
         $this->db->insert('actvidades', $td); 
    }
 
 
  function updateseguimiento($todo,$id){
	 $this->db->update('procesodeventa', $todo, array('prcv_IDprocesodeventa' => $id));
            return true;
	 }
	 
	
	 
	 
	 function editfechaentregaseguimiento($todo,$id){
	  $this->db->update('datos', $todo, array('dat_IDdatos' => $id));
            return true;	 
		 
		 }
		 
		 
		 
		 function UpdateUltimaActividad($todo,$id){
	  $this->db->update('contacto', $todo, array('con_IDcontacto' => $id));
            return true;	 
		 
		 }
		 
		 
		 function editfechaentregaseguimientodos($todo,$id){
	  $this->db->update('procesodeventa', $todo, array('prcv_IDprocesodeventa' => $id));
            return true;	 
		 
		 }
	 
 function editfechaentrega($todo,$id){
	  $this->db->update('fechaentrega', $todo, array('fen_IDfechaentrega' => $id));
            return true;
	 
	 }
 
 
 function updateDatosAuto($todo,$id){
	 $this->db->update('datos_auto', $todo, array('data_' => $id));
            return true;
	 }
	
	function updateDatosAutoTipo($todo,$id){
	 $this->db->update('datos_compra', $todo, array('datco_IDdatos_compra' => $id));
            return true;
	 } 
	 
 function  updateHojaRazon($todo,$id)
 {
 	
	$this->db->update('hojaunarazon', $todo, array('hoj_IDhojaunarazon' => $id));
            return true;
	
 }
 
 function updatetodo($todo,$id)
    {
         $this->db->update('contacto', $todo, array('con_IDcontacto' => $id));
            return true;
    }
	
	 function updateApartadoA($todo,$id)
    {
         $this->db->update('asigancion_auto', $todo, array('aau_IdFk' => $id));
            return true;
    }
	
	function updatellamadaentr($todo,$id)
	{
		 $this->db->update('llamada_entrante', $todo, array('lle_IDllamada_entrante' => $id));
         return true;
		
	}
	
	
	 function updateoportunidad($todo,$ido)
    {
         $this->db->update('oportunidades', $todo, array('opo_IDoportunidad' => $ido));
            return true;
    }
	
	
	function updatetarea($todo,$ida)
    {
         $this->db->update('actvidades', $todo, array('act_IDactividades' => $ida));
            return true;
    }
	
	function updategenerales($td,$ida)
    {
         $this->db->update('generales', $td, array('gen_IDgenerales' => $ida));
            return true;
    }
	
	function delete($id)
    {
        
            $this->db->query('update contacto set con_status="eliminado" where con_IDcontacto='.$id.''); 
            return true;
          
    }
	function deletehojarazon($ido)
	 {
        
           $this->db->delete('hojaunarazon', array('hoj_IDhojaunarazon' => $ido));
            return true;
          
    }
	
	
	
	function deletegenerales($id)
    {
        
            $this->db->delete('generales', array('gen_IDgenerales' => $id));
            return true;
       
    }
	
	
	function deletetareas($id)
    {
        
            $this->db->delete('actvidades', array('act_IDactividades' => $id));
            return true;
       
    }
	
function AddTareaCotizacionContacto($datos,$idb){

$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+1 days", $fechaComparacion); //Le restamos 15 dias
$undia=date("Y-m-d", $calculo);						
					
						$todo= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$undia,
					'act_fecha_fin'=>$undia,
					'act_titulo'=>'Cotizacion Llamada de seguiemiento 24 hrs.',
					'act_descripcion'=>''.$datos.'',
					'act_hora'=>12,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$idb,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+1 days", $fechaComparacion); //Le restamos 15 dias
$tresdia=date("Y-m-d", $calculo);						
					$todob= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$tresdia,
					'act_fecha_fin'=>$tresdia,
					'act_titulo'=>'Cotizacion Llamada de seguiemiento 72 hrs.',
					'act_descripcion'=>''.$datos.'',
					'act_hora'=>12,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$idb,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );										

 $this->db->insert('actvidades', $todo);
 $this->db->insert('actvidades', $todob); 	
	
	
	}	
	

function AddTareaCotizacion($datos){
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+1 days", $fechaComparacion); //un dia 24 horas
$undia=date("Y-m-d", $calculo);					
$todo= array(
                    'gen_status'=>'pendiente',
					'gen_fecha_inicio'=>$undia,
					'gen_fecha_fin'=>$undia,
					'gen_titulo'=>'Cotizacion - Llamada de Seguimiento 24 hrs',
					'gen_descripcion'=>''.$datos.'',
					'gen_categoria'=>'generales',
					'gen_hora'=>12,
					'gen_min'=>30,
					'huser_hus_IDhuser'=>$_SESSION['id']
                    );	
					
					$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+3 days", $fechaComparacion); //3 dia 72 horas
$tresdias=date("Y-m-d", $calculo);					
$todob= array(
                    'gen_status'=>'pendiente',
					'gen_fecha_inicio'=>$tresdias,
					'gen_fecha_fin'=>$tresdias,
					'gen_titulo'=>'Cotizacion - Llamada de Seguimiento 72 hrs',
					'gen_descripcion'=>''.$datos.'',
					'gen_categoria'=>'generales',
					'gen_hora'=>12,
					'gen_min'=>30,
					'huser_hus_IDhuser'=>$_SESSION['id']
                    );	
					
 $this->db->insert('generales', $todo);
  $this->db->insert('generales', $todob);	
	}
	

function addactividadnewcontact($id){

$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+1 days", $fechaComparacion); //un dia 24 horas
$undia=date("Y-m-d", $calculo);	
$todoa= array(
                    'act_status'=>'pendiente',
					'act_fecha_inicio'=>$undia,
					'act_fecha_fin'=>$undia,
					'act_titulo'=>'Llamada Telefonica 24 horas',
					'act_descripcion'=>'Utilizar formato de una razon en el sistema de prospeccion',
					'act_hora'=>9,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                    );
$fechaComparacion = strtotime("".date('d')." ".strftime('%b')." ".date('Y')."");
$calculo= strtotime("+3 days", $fechaComparacion); //3 dia 72 horas
$tresdias=date("Y-m-d", $calculo);				
$todob= array(
                    'act_status'=>'realizada',
					'act_fecha_inicio'=>$tresdias,
					'act_fecha_fin'=>$tresdias,
					'act_titulo'=>'Llamada Telefonica de 72 horas',
					'act_descripcion'=>'Captura en el sistema de prospeccion',
					'act_hora'=>10,
					'act_min'=>30,
					'bitacora_bit_IDbitacora'=>$id,
					'tipo_actividad_tac_IDtipo_actividad'=>1,
					'act_notificar'=>'on',
					'tipo_mensage_tip_IDtipo_mensage'=>'',
					'fuente_IDfiuente'=>0,
					'act_confirmacion'=>''
                   );	
			
 $this->db->insert('actvidades', $todoa);
 $this->db->insert('actvidades', $todob);
 }	
	

function EmailCotizacion($email,$auto,$tit,$nom,$ape,$idcot){
	
	

$huser=$this->Contactomodel->getHuser($_SESSION['id']);	
$num = preg_replace('/[^0-9]/', '', $huser[0]->hus_celular);
$len = strlen($num);
if($len == 7)
$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
elseif($len == 10)
$celular= preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);
 //preparacion y envio del correo
			$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'Honda Optima');
            $this->email->to($email);
			$this->email->cc($huser[0]->hus_correo);
			if($_SESSION['ciudad']=='Mexicali'){
				$this->email->cc('recepcionmxli@hondaoptima.com');
				$this->email->bcc('oscarv@hondaoptima.com');
				$this->email->bcc('liusber_00@hotmail.com');
				}
			 $this->email->subject('Cotizacion: '.$auto.'');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<br>
                     	
                     	<br><br>
                     	<div >
'.$tit.' '.$nom.' '.$ape.'
<br><br>
Gracias por su interés hacia el '.$auto.', sin duda está Usted tomando una decisión inteligente. Permítame presentarle a continuación la cotización de las versiones que tenemos a su disposición en el archivo adjunto en este correo electrónico.
                     		
                     	</div>
                     	<div>
                     		<br>
                     
                     	
                     	
                     	
                     	<br>
		    <table>
		    <tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
		   <tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$celular.'</td></tr>
			<tr><td><b>Radio Personal</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      
  
      

      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
			
			');
			
			$this->email->attach('downloads/cotizaciones/cotizacion'.$idcot.'.pdf');
			$this->email->attach('downloads/cotizaciones/HondaOptima.pdf');
			/*$this->email->attach('downloads/cotizaciones/HojaInformativaCPHversion2013.jpg');
			$this->email->attach('downloads/cotizaciones/HojaInformativaGarantiaHDMversion2013.jpg');
			$this->email->attach('downloads/cotizaciones/HojaInformativaHistoriaHondaversion2013.jpg');
			$this->email->attach('downloads/cotizaciones/HojaInformativaSeguroGNPversion2013.jpg');
			*/
			$this->email->send();	
			
			return 1;
	
	
	}	
	
	function SendemailBienvenida($NVAR,$correopara,$titulo,$nombre,$apellido,$ciudad)
	
	{
		 function nombremes($mes){
 setlocale(LC_TIME, 'spanish');  
 $nombre=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
 return $nombre;
} 
		 


	//obtener datos de contacto para utilizar en email
	  $huser=$this->Contactomodel->getHuser($NVAR);
	  
	
			
	        //preparacion y envio del correo
			$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'CRM HondaOptima.com');
            $this->email->to($correopara);
            $this->email->subject('Bienvenido a Honda Óptima');
            $this->email->message('
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<br>
                     	<table width="100%" ><tr><td width="50%"><div style="text-align: justify;"><b>'.$titulo.' '.$nombre.' '.$apellido.' </b></div></td>
                     	 <td width="50%" style="text-align:right">
                     	 <div>'.$ciudad.' B.C. '.ucwords(nombremes(date('n'))).' 2013<div></td>
                     	 </tr></table>
                     	<br><br>
                     	<div >
                     	Bienvenido(a) a Honda Óptima donde trabajamos para rebasar tus expectativas y ofrecerte el mejor servicio.
 <br><br>
                       El poder de los sueños ahora está en tus manos .
                     		
                     	</div>
                     	<div>
                     		<br>
                     
                     	
                     	
                     	
                     	<br>
		    <table>
		    <tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
		   <tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$huser[0]->hus_celular.'</td></tr>
			<tr><td><b>Radio Personal</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      
  
      

      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
			
			');
			$this->email->send();
		
	}
	
	
	function Sendmensajeuno($con,$idh,$cid,$ms,$dia,$hora,$min,$ti)
	{
		
			$contacto=$this->Contactomodel->getNameContacto($con);
			$huser=$this->Vendedoresmodel->getId($idh);
			
			
				            if($contacto[0]->con_modelo=='ACCORD'){ $text='Estás por dar el primer paso a un viaje de sofisticación. ';}
                            if($contacto[0]->con_modelo=='FIT'){ $text='Llegó el momento de conducirte con estilo.';}
                            if($contacto[0]->con_modelo=='CIVIC'){ $text='Da el primer paso con el favorito de todos.';}
                            if($contacto[0]->con_modelo=='PILOT'){ $text='Comienza tu aventura con la potencia de una PILOT.  ';}
                            if($contacto[0]->con_modelo=='CRV'){ $text='Cada vez más cerca de la SUV de tus sueños.';}
                            if($contacto[0]->con_modelo=='ODYSSEY'){ $text='Imagina todos los viajes en familia que harás. ';}
                     		if($contacto[0]->con_modelo=='CITY'){ $text='Estilo y equipamiento en un solo auto. ';}
                            if($contacto[0]->con_modelo=='CRZ'){ $text='Energía híbrida y buena vibra. ';}
		
		   	$this->email->set_mailtype("html");
		   $this->email->from($huser[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to($contacto[0]->con_correo);
			$this->email->cc($huser[0]->hus_correo);
            $this->email->subject('Notificación[Honda Optima] - Cita para ver vehículo');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     	<table width="100%" ><tr><td width="50%"><div style="text-align: justify;"><b>Estimado(a) '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'.</b></div></td> <td width="50%" style="text-align:right"><div>'.$cid.'  B.C. '.$ms.' 2013<div></td></tr></table>
                     	<br>
                     
                     	<div>
                     	
                     		'.$text.'.
							<br><br>
							Te esperamos el día '.$dia.' de '.$ms.'  a las '.$hora.':'.$min.' '.$ti.'  para conocer el '.$contacto[0]->con_modelo.'.
                     		<br>
                     		<br>
						Descubre las miles de posibilidades que Honda tiene para ti.
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
						<table>
			<tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
			
			<tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$huser[0]->hus_celular.'</td></tr>
			<tr><td><b>Radio Personal:</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda:</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono:</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
			
			</table>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
       
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: center"></td>
         </tr>
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
				
		
		
		
	}

function Sendmensajedos($con,$idh,$cid,$ms,$dia,$hora,$min,$ti)
{
	
	$contacto=$this->Contactomodel->getNameContacto($con);
	$huser=$this->Vendedoresmodel->getId($idh);
				
				            if($contacto[0]->con_modelo=='ACCORD'){ $text='Ven y conoce el auto más sofisticado. ';}
                            if($contacto[0]->con_modelo=='FIT'){ $text='Ven y maneja el compacto que lo tiene TODO. ';}
                            if($contacto[0]->con_modelo=='CIVIC'){ $text='Ven y descubre por qué a todos les encanta este auto.';}
                            if($contacto[0]->con_modelo=='PILOT'){ $text='Ven y conoce el auto con la tecnología automotriz más avanzada. ';}
                            if($contacto[0]->con_modelo=='CRV'){ $text='Sabemos que te encantará. Ven y conoce la SUV de tus sueños. ';}
                            if($contacto[0]->con_modelo=='ODYSSEY'){ $text='Ven y conoce un auto con equipamiento único.';}
                     		if($contacto[0]->con_modelo=='CITY'){ $text='Como este auto no hay otro. Compruébalo.';}
                            if($contacto[0]->con_modelo=='CRZ'){ $text='Despierta tu instinto deportivo. Ven y conócelo. ';}
							$fei;
							
							
				
				$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to($contacto[0]->con_correo);
				$this->email->cc($huser[0]->hus_correo);
            $this->email->subject('Notificación[Honda Optima] - Prueba de Demostración');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     	<table width="100%" ><tr><td width="50%"><div style="text-align: justify;"><b>Estimado(a) '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.' .</b></div></td> <td width="50%" style="text-align:right"><div>'.$cid.'  B.C. '.$ms.' 2013<div></td></tr></table>
                     	<br>
                     
                     	<div>
                     	<br>
                     	Llegó el momento de dar el siguiente paso.
                     	<br><br>
                     	
                     		'.$text.'.
							<br><br>
							Tienes una cita el '.$dia.' de '.$ms.'  a las '.$hora.':'.$min.' '.$ti.'  para la demostración del  '.$contacto[0]->con_modelo.'.
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
						<table>
			<tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
			
			<tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$huser[0]->hus_celular.'</td></tr>
			<tr><td><b>Radio Personal:</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda:</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono:</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
			
			</table>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
             
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: center"></td>
         </tr>
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
				
	
	
	
}
	
	function Sendmensajetres($con,$idh,$cid,$ms,$dia,$hora,$min,$ti)
	{
		
	$contacto=$this->Contactomodel->getNameContacto($con);
	$huser=$this->Vendedoresmodel->getId($idh);
				
			$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to($contacto[0]->con_correo);
			$this->email->cc($huser[0]->hus_correo);
            $this->email->subject('Notificación[Honda Optima] - Evaluación de su unidad ');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     	
                     		<table width="100%" ><tr><td width="50%"><div style="text-align: justify;"><b>Estimado(a) '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.' .</b></div></td> <td width="50%" style="text-align:right"><div>'.$cid.'  B.C. '.$ms.' 2013<div></td></tr></table>
                     	<br>
                     
                     	<div>
                     	
                     	 <br><br>
                     	
                     		Queremos que tu experiencia sea perfecta. Ven a Honda Óptima este '.$dia.' de '.$ms.' a las '.$hora.':'.$min.' '.$ti.' con tu auto nacional y conoce la mejor opción para que estrenes YA <b>tu nuevo '.$contacto[0]->con_modelo.'</b>.
<br><br>


							<br><br>
					
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
						<table>
			<tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
			
			<tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$huser[0]->hus_celular.'</td></tr>
			<tr><td><b>Radio Personal:</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda:</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono:</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
			
			</table>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}
	
	function Sendmensajecuatro($con,$idh,$cid,$ms,$dia,$hora,$min,$ti)
	{
		$contacto=$this->Contactomodel->getNameContacto($con);
	    $huser=$this->Vendedoresmodel->getId($idh);
	
	
	
	
			                if($contacto[0]->con_modelo=='ACCORD'){ $text='Condúcete con elegancia y sofisticación en tu nuevo ACCORD. ';}
                            if($contacto[0]->con_modelo=='FIT'){ $text='Llegó  el momento de que todos te vean en tu FIT. ';}
                            if($contacto[0]->con_modelo=='CIVIC'){ $text='Estás listo para recorrer el mundo con tu CIVIC.';}
                            if($contacto[0]->con_modelo=='PILOT'){ $text='Con tu Pilot ahora tienes el poder de abrir cualquier camino.';}
                            if($contacto[0]->con_modelo=='CRV'){ $text='Despierta! La SUV de tus sueños es tuya. ';}
                            if($contacto[0]->con_modelo=='ODYSSEY'){ $text=' Dale la bienvenida al nuevo miembro de la familia ODYSSEY. ';}
                     		if($contacto[0]->con_modelo=='CITY'){ $text='Tu historia comienza con el CITY.';}
                            if($contacto[0]->con_modelo=='CRZ'){ $text='Ecología y estilo  que ahora van contigo CRZ.   ';}
							$fei;
							
							
				
				$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to($contacto[0]->con_correo);
				$this->email->cc($huser[0]->hus_correo);
            $this->email->subject('Notificacion[Honda Optima] - ¡La espera ha terminado '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'! ');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     	<div style="text-align:right; width:100%">'.$cid.'  B.C. '.$ms.' 2013</div>
                     		<table width="100%" ><tr><td width="50%"><div style="text-align: justify;"><b>¡La espera ha terminado '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'! </b></div></td> <td width="20%" ></td></tr></table>
                     	<br>
                     
                     	<div>
                     	
						Este '.$dia.' de '.$ms.' a las '.$hora.':'.$min.' '.$ti.' te esperamos en Honda Óptima con las llaves de tu auto nuevo.
						<br><br>
						'.$text.'
                     	
                     		

							<br><br>
					
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
						<table>
			<tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
			
			<tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$huser[0]->hus_celular.'</td></tr>
			<tr><td><b>Radio Personal:</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda:</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono:</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
			
			</table>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff<a href="../config">config</a>">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
	}

function Sendmensajegeneral($titulo,$ncon,$email,$fei,$hora,$min,$desc){
           $contacto=$this->Contactomodel->getNameContacto($ncon);	
            $this->load->library('email');
			$this->email->set_mailtype("html");
		    $this->email->from($email, 'CRM Hondaoptima.com');
            $this->email->to($email);
            $this->email->subject('Notificacion - '.$titulo.'');
            $this->email->message('
			<table><tr><td>
			
			</td></tr></table>
			<br>
			<table>
			<tr><td><b>Recordatorio de</b></td><td>'.$titulo.'</td></tr>
			</table>
		<br>
			<table>
			<tr><td><b>Contacto:</b></td><td>'.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'</td></tr>
			<tr><td><b>Fecha:</b></td><td>'.$fei.'</td></tr>
			<tr><td><b>Hora:</b></td><td>'.$hora.':'.$min.'</td></tr>
			</table>
			
			<table>
			<tr><td><b>Descripci&oacute;n:</b></td></tr>
			<tr><td>'.$desc.'</td></tr>
			
			</table>
			
			
			<hr/>
			<table><tr><td>

			</td></tr></table>
			
			');
			
            $this->email->send();
}
	
	function SendSmsUno($ncon,$ms,$dia,$hora,$min,$ti)
	
{
	 $contacto=$this->Contactomodel->getNameContacto($ncon);
	 $contacto[0]->con_telefono_casa;
	                        if($contacto[0]->con_modelo=='ACCORD'){ $text='Estas por dar un viaje de sofisticacion,';}
                            if($contacto[0]->con_modelo=='FIT'){ $text='Conduce con estilo,';}
                            if($contacto[0]->con_modelo=='CIVIC'){ $text='Conduce el favorito de todos,';}
                            if($contacto[0]->con_modelo=='PILOT'){ $text='Comienza tu aventura con una Pilot,';}
                            if($contacto[0]->con_modelo=='CRV'){ $text='Cada vez mas cerca de la SUV de tus sueños,';}
                            if($contacto[0]->con_modelo=='ODYSSEY'){ $text='Imagina los viajes en familia que haras, ';}
                     		if($contacto[0]->con_modelo=='CITY'){ $text='Estilo y equipamiento en un solo auto,';}
                            if($contacto[0]->con_modelo=='CRZ'){ $text='Energía híbrida y buena vibra,';}
 

	if($contacto[0]->con_telefono_casa)
	{
	$CAU=$contacto[0]->con_telefono_casa;			
	$txtsms= substr('Estimado '.$contacto[0]->con_nombre.', '.$text.' Te esperamos el dia '.$dia.' de '.$ms.'  a las '.$hora.':'.$min.' '.$ti.' para conocer el '.$contacto[0]->con_modelo.' ', 0, 180);
	 $txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
       print_r($response);
	
	//$this->curl->simple_post("http://api.quiubas.com:9501/api", array("action"=>"sendmessage","username"=>"HITEK","password"=>"9723H20","recipient"=>"52".$CAU."","messagedata"=>"".$txtsms.""));
	}



}



function SendSmsDos($ncon,$ms,$dia,$hora,$min,$ti)
	
{
	 $contacto=$this->Contactomodel->getNameContacto($ncon);
	 $contacto[0]->con_telefono_casa;
	                        if($contacto[0]->con_modelo=='ACCORD'){ $text='Ven y conoce el auto mas sofisticado,';}
                            if($contacto[0]->con_modelo=='FIT'){    $text='Ven y maneja el compacto que lo tiene TODO,';}
                            if($contacto[0]->con_modelo=='CIVIC'){  $text='Ven y descubre por que a todos les encanta este auto,';}
                            if($contacto[0]->con_modelo=='PILOT'){  $text='Ven y conoce el auto con la tecnologia automotriz mas avanzada,';}
                            if($contacto[0]->con_modelo=='CRV'){    $text='Sabemos que te encantara. Ven y conoce la SUV de tus suenos,';}
                            if($contacto[0]->con_modelo=='ODYSSEY'){$text='Ven y conoce un auto con equipamiento unico,';}
                     		if($contacto[0]->con_modelo=='CITY'){   $text='Como este auto no hay otro. Compruebalo,';}
                            if($contacto[0]->con_modelo=='CRZ'){    $text='Despierta tu instinto deportivo. Ven y conocelo,';}
 

	if($contacto[0]->con_telefono_casa)
	{
	$CAU=$contacto[0]->con_telefono_casa;			
	$txtsms= substr('Hola '.$contacto[0]->con_nombre.', Llego el momento de dar el siguiente paso. Tienes una cita el '.$dia.' de '.$ms.'  a las '.$hora.':'.$min.' '.$ti.' para la demostracion del '.$contacto[0]->con_modelo.' ', 0, 180);
	 $txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
      // print_r($response);

	}



}



function SendSmsTres($ncon,$ms,$dia,$hora,$min,$ti)
	
{
	 $contacto=$this->Contactomodel->getNameContacto($ncon);
	 $contacto[0]->con_telefono_casa;
	                        

	if($contacto[0]->con_telefono_casa)
	{
	$CAU=$contacto[0]->con_telefono_casa;			
	$txtsms= substr('Te esperamos en  Honda Optima este '.$dia.' de '.$ms.'  a las '.$hora.':'.$min.' '.$ti.' con tu auto nacional y conoce la mejor opcion para que estrenes un '.$contacto[0]->con_modelo.'  ', 0, 180);
	 $txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
       print_r($response);
	

	}



}


function SendSmsCuatro($ncon,$ms,$dia,$hora,$min,$ti)
	
{
	 $contacto=$this->Contactomodel->getNameContacto($ncon);
	 $contacto[0]->con_telefono_casa;
	                        if($contacto[0]->con_modelo=='ACCORD'){ $text='Conducete con elegancia y sofisticacion en tu nuevo Accord';}
                            if($contacto[0]->con_modelo=='FIT'){ $text='Llego  el momento de que todos te vean en tu Fit';}
                            if($contacto[0]->con_modelo=='CIVIC'){ $text='Estás listo para recorrer el mundo con tu Civic';}
                            if($contacto[0]->con_modelo=='PILOT'){ $text='Con tu Pilot ahora tienes el poder de abrir cualquier camino';}
                            if($contacto[0]->con_modelo=='CRV'){ $text='¡Despierta! La SUV de tus suenos es tuya';}
                            if($contacto[0]->con_modelo=='ODYSSEY'){ $text='Dale la bienvenida al nuevo miembro de la familia';}
                     		if($contacto[0]->con_modelo=='CITY'){ $text='Tu historia comienza con el City';}
                            if($contacto[0]->con_modelo=='CRZ'){ $text='Ecologia y estilo  que ahora van contigo';}
 

	if($contacto[0]->con_telefono_casa)
	{
	$CAU=$contacto[0]->con_telefono_casa;			
	$txtsms= substr('¡La espera ha terminado '.$contacto[0]->con_nombre.'!, Este '.$dia.' de '.$ms.'  a las '.$hora.':'.$min.' '.$ti.' te esperamos en Honda Optima con las llaves de tu auto nuevo.', 0, 180);
	 $txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
      

	}



}

function smscredito($con,$idh)
	{
		
	$contacto=$this->Contactomodel->getNameContacto($con);
	$huser=$this->Vendedoresmodel->getId($idh);
	if($_SESSION['ciudad']=='Tijuana'){
		//enviar a f&i 
	$CAU='6641249766';
	}
	elseif($_SESSION['ciudad']=='Mexicali'){
		//enviar a f&i
	$CAU='6861434505';	
		}			
	$txtsms= substr('¡Nueva solicitud de credito Contacto:'.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.', Asesor:'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'', 0, 180);
	 $txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
	}


	
	
	
function sendTextMagic()
    {
        // set text
        $text = 'Hi, I am appleboy';
        // set phone (string or array)
        $phone = array('xxxxxxx');
        $response = $this->textmagic->send($text, $phone);
        print_r($response);
    }
	
	
	
function smsenviaraviso($con,$idh)
	{
		
	$contacto=$this->Contactomodel->getNameContacto($con);
	$huser=$this->Vendedoresmodel->getId($idh);
	if($_SESSION['ciudad']=='Tijuana'){
		//enviar f&i
	$CAU='6641249766';
	}
	elseif($_SESSION['ciudad']=='Mexicali'){
	//enviar f&i
	$CAU='6861434505';	
		}
		elseif($_SESSION['ciudad']=='Ensenada'){
	//enviar f&i
	$CAU='6461380326';	
		}			
	$txtsms= substr('¡Nueva orden de compra por revisar:'.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.', Asesor:'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'', 0, 180);
	$txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
	}
	
	
	function smsenviaravisogerente($ase,$nombre)
	{
		$huser=$this->Vendedoresmodel->getId($ase);
		
	if($_SESSION['ciudad']=='Tijuana'){
	$CAU='6643140506';
	}
	elseif($_SESSION['ciudad']=='Mexicali'){
	$CAU='6861192283';	
		}
		elseif($_SESSION['ciudad']=='Ensenada'){
	$CAU='6462274470';	
		}			
	$txtsms= substr('¡F and I Aprobo orden de compra de :'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'; Contacto:'.$nombre.'', 0, 180);
	$txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
	}
	
	function smsenviaravisoconta($ase,$nombre)
	{
		$huser=$this->Vendedoresmodel->getId($ase);
		
	if($_SESSION['ciudad']=='Tijuana'){
		//numero contabilidad
	$CAU='6642717852';
	}
	elseif($_SESSION['ciudad']=='Mexicali'){
		//numero contabilidad
	$CAU='6862276020';	
		}	
	elseif($_SESSION['ciudad']=='Ensenada'){
		//numero contabilidad
	$CAU='6642274470';	
		}		
				
	$txtsms= substr('¡Asesor Aprobo orden de compra de :'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.' ; '.$nombre.'', 0, 180);
	$txtsms;
	
	if($_SESSION['ciudad']=='Mexicali'){
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
	}
	
	
	   if($_SESSION['ciudad']=='Ensenada' || $_SESSION['ciudad']=='Tijuana'){
		   
		    $CAT='526642274470';
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
		   
		   $CAT='526642717852';
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
		   }
	}
	
	function smsenviaravisoasesor($ase,$nombre)
	{
		$huser=$this->Vendedoresmodel->getId($ase);
				
	$txtsms= substr('¡Facturacion Aprobo orden de compra; Contacto: '.$nombre.' ', 0, 180);
	$txtsms;
	
	   $CAT='52'.$huser[0]->hus_celular;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
	}	
	
	function smsenviaravisofactura($ase,$nombre,$cd)
	{
		$huser=$this->Vendedoresmodel->getId($ase);
		
	if($cd=='Tijuana'){
		//numero de ivone
	$CAU='6643140506';
	}
	elseif($cd=='Mexicali'){
		//numero facturacion mexicali alicia leon
	$CAU='6861060692';	
		}
	elseif($cd=='Ensenada'){
		//numero facturacion Ensenada Adriana Karina
	$CAU='6461288180';	
		}				
	$txtsms= substr('¡Gerente Aprobo orden de compra de :'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'; Contacto:'.$nombre.'', 0, 180);
	$txtsms;
	
	   $CAT='52'.$CAU;
	   $phone = array($CAT);
       $response = $this->textmagic->send($txtsms, $phone);
	}	
	
	
	function enviaraviso($idh,$idc,$cd)
{
	
	$contacto=$this->Contactomodel->getNameContacto($idc);
	$huser=$this->Vendedoresmodel->getId($idh);
				
	if($cd=='Tijuana'){
$emag='frivera@hondaoptima.com';
$emacon='monica@hondaoptima.com'; 
$emafi='eruiz@hondaoptima.com'; 
$emamecar='aimontoya@hondaoptima.com';}	
	if($cd=='Mexicali'){
$emag='oscarv@hondaoptima.com';
$emacon='contamxli@hondaoptima.com'; 
$emafi='creditomxli@hondaoptima.com'; 
$emamecar='lucero@hondaoptima.com';}
	if($cd=='Ensenada'){
$emag='oscarv@hondaoptima.com';
$emacon='contamxli@hondaoptima.com'; 
$emafi='egranados@hondaoptima.com'; 
$emamecar='lucero@hondaoptima.com';}			          
				
			$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to($emafi,'liusber_00@hotmail.com');
			$this->email->cc($huser[0]->hus_correo);
            $this->email->subject('Notificación[Honda Optima] - Nueva orden de compra para seguimiento');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>		

     <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
               
                     	<br>
                     
                     	<div>
                     	<br>
                     	Se ha generado una nueva orden de compra para seguimiento
                     	<br><br>
                     	Asesor de venta: '.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'
						<br><br>
						Contacto: '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'
                     	
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
						<table>
			<tr><td rowspan="8"><img src="'.base_url().'upload/usuarios/'.$huser[0]->hus_foto.'"" width="150px" height="150px;"></td></tr>
			
			<tr><td><b>Su Asesor de ventas:</b></td><td>'.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'</td></tr>
			<tr><td><b>Correo:</b></td><td>'.$huser[0]->hus_correo.'</td></tr>
			<tr><td><b>Celular:</b></td><td>'.$huser[0]->hus_celular.'</td></tr>
			<tr><td><b>Radio Personal:</b></td><td>'.$huser[0]->hus_radio_personal.'</td></tr>
			<tr><td><b>Radio Honda:</b></td><td>'.$huser[0]->hus_radio_honda.'</td></tr>
			<tr><td><b>Telefono:</b></td><td>'.$huser[0]->hus_telefono.'</td></tr>
			
			</table>
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
       
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
					
}



function enviaravisogerente($ase,$nombre)
{
	$huser=$this->Vendedoresmodel->getId($ase);
if($_SESSION['ciudad']=='Tijuana'){
$emag='frivera@hondaoptima.com';
$emacon='monica@hondaoptima.com'; 
$emafi='eruiz@hondaoptima.com'; 
$emamecar='aimontoya@hondaoptima.com';}	
	if($_SESSION['ciudad']=='Mexicali'){
$emag='oscarv@hondaoptima.com';
$emacon='karlam@hondaoptima.com'; 
$emafi='creditomxli@hondaoptima.com'; 
$emamecar='lucero@hondaoptima.com';}

if($_SESSION['ciudad']=='Ensenada'){
$emag='afierro@hondaoptima.com';
$emacon=''; 
$emafi=''; 
$emamecar='';}			          
				
			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'CRM Hondaoptima.com');
            $this->email->to($emag,'liusber_00@hotmail.com');
			$this->email->cc($_SESSION['username']);
            $this->email->subject('Notificación[Honda Optima] - Aprobación de Orden de compra');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
               
                     	<br>
                     
                     	<div>
                     	<br>
                     	F&I Aprob&oacute; orden de compra.
                     	<br><br>
                     	Asesor de venta: '.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'
						
                     		<br>
						Contacto:'.$nombre.'
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
					
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
       
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
					
}


function enviaravisoconta($ase,$nombre)
{
	$huser=$this->Vendedoresmodel->getId($ase);
if($_SESSION['ciudad']=='Tijuana'){
$emag='frivera@hondaoptima.com';
$emacon='monica@hondaoptima.com'; 
$emafi='eruiz@hondaoptima.com'; 
$emamecar='aimontoya@hondaoptima.com';
$cc='contaens@hondaoptima.com';
}	
	if($_SESSION['ciudad']=='Mexicali'){
$emag='oscarv@hondaoptima.com';
$emacon='karlam@hondaoptima.com'; 
$emafi='creditomxli@hondaoptima.com'; 
$emamecar='lucero@hondaoptima.com';
$cc=''.$_SESSION['username'].'';}

if($_SESSION['ciudad']=='Ensenada'){
$emag='oscarv@hondaoptima.com';
$emacon='contaens@hondaoptima.com'; 
$emafi='creditomxli@hondaoptima.com'; 
$emamecar='lucero@hondaoptima.com';
$cc='monica@hondaoptima.com';}			          
				
			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'CRM Hondaoptima.com');
            $this->email->to($emacon,'liusber_00@hotmail.com');
			$this->email->cc($cc);
            $this->email->subject('Notificación[Honda Optima] - Aprobación de Orden de compra');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
               
                     	<br>
                     
                     	<div>
                     	<br>
                     	Asesor Aprob&oacute; orden de compra.
                     	<br><br>
                     	Asesor de venta: '.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'
						<br><br>
                     	Asesor de venta: '.$nombre.'
						
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
					
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
       
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
					
}



function enviaravisoasesor($ase,$nombre)
{
	$huser=$this->Vendedoresmodel->getId($ase);

			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'CRM Hondaoptima.com');
            $this->email->to($huser[0]->hus_correo,'liusber_00@hotmail.com');
			$this->email->cc($_SESSION['username']);
            $this->email->subject('Notificación[Honda Optima] - Aprobación de Orden de compra');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
               
                     	<br>
                     
                     	<div>
                     	<br>
                     	F&I Aprob&oacute; la orden de compra.
						
                     		<br>
                     		<br>
						Contacto:'.$nombre.'
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
					
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
       
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
					
}





function enviaravisofactura($ase,$nombre,$cd)
{
	$huser=$this->Vendedoresmodel->getId($ase);
if($cd=='Tijuana'){
$emag='frivera@hondaoptima.com';
$emacon='monica@hondaoptima.com'; 
$emafi='eruiz@hondaoptima.com'; 
$emamecar='aimontoya@hondaoptima.com';
$fac='auxventas@hondaoptima.com';
}
	
	if($cd=='Mexicali'){
$emag='oscarv@hondaoptima.com';
$emacon='karlam@hondaoptima.com'; 
$emafi='creditomxli@hondaoptima.com'; 
$emamecar='lucero@hondaoptima.com';
$fac='auxmex@hondaoptima.com';
}	

if($cd=='Ensenada'){
$emag='';
$emacon=''; 
$emafi=''; 
$emamecar='';
$fac='amartinez@hondaoptima.com';
}			          
				
			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'CRM Hondaoptima.com');
            $this->email->to($fac);
			//$this->email->cc($_SESSION['username']);
            $this->email->subject('Notificación[Honda Optima] - Aprobación de Orden de compra');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
               
                     	<br>
                     
                     	<div>
                     	<br>
                     	Gerente Aprob&oacute; orden de compra.
                     	<br><br>
                     	Asesor de venta: '.$huser[0]->hus_nombre.' '.$huser[0]->hus_apellido.'
						
                     		<br>
                     		<br>
						Contacto:'.$nombre.'
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>
                     	
					
                     	
                     	 </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
       
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
					
}



	function emailcredito($con,$idh)
	{
		
	$contacto=$this->Contactomodel->getNameContacto($con);
	$huser=$this->Vendedoresmodel->getId($idh);
	//eruiz@hondaoptima.com			
			$this->email->set_mailtype("html");
		    $this->email->from($huser[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to('eruiz@hondaoptima.com	');
			$this->email->cc($huser[0]->hus_correo);
            $this->email->subject('Notificación[Honda Optima] - Credito de: '.$contacto[0]->con_titulo.' '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     
                     	<br>
                     
                     	<div>
                     	
                     	 <br><br>
                     Nueva solicitud de credito de '.$contacto[0]->con_nombre.' '.$contacto[0]->con_apellido.'<br>
					 Estado del contacto:'.$contacto[0]->con_status.'
<br><br>


							<br><br>
					
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>

            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}
	
	
	function sendemailmerca($email,$ido,$fecha)
	{
		
	//$contacto=$this->Contactomodel->getNameContacto($con);
	$OD=$this->Contactomodel->getOrdendeCompraPdf($ido);
	if($OD[0]->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.' '.$OD[0]->datc_apellidopaterno.' '.$OD[0]->datc_apellidomaterno;}
else{$facturaNombre=$OD[0]->datc_moral;}


if($OD[0]->datco_snuevo=='si'){$ta='Nuevo';}
elseif($OD[0]->datco_nuevo=='si'){$ta='Demo';}
elseif($OD[0]->datco_nuevox=='si'){$ta='Seminuevo';}
	//$huser=$this->Vendedoresmodel->getId($idh);
	//eruiz@hondaoptima.com			
			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'CRM Hondaoptima.com');
            $this->email->to(''.$email.'');
			$this->email->cc('liusber_00@hotmail.com');
            $this->email->subject('Notificación[Honda Optima] - Proxima Entrega de Auto');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     
                     	<br>
                     
                     	<div>
                     	
                     	 <br><br>
<b>Nombre del Cliente:</b>'.$facturaNombre.'
<br><br>
<b>Modelo y Año del Carro:</b>'.$OD[0]->data_marca.' '.$OD[0]->data_modelo.' '.$OD[0]->data_version.' '.$OD[0]->data_ano.'
<br><br>
<b>Correo electrónico del cliente:</b>'.$OD[0]->datc_email.'
<br><br>
<b>Tipo de Auto:</b>'.$ta.'
<br><br> 
<b>Nombre del Asesor:</b>'.$OD[0]->hus_nombre.' '.$OD[0]->hus_apellido.'  
<br><br> 
<b>Fecha de Entrega:</b>'.$fecha.'
<br><br>
<b>Fecha de Envio</b>'.date("Y-m-d H:i:s").'                  
<br><br>


							<br><br>
					
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>

            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}
	
	function sendemailmercaMkt($email,$ido)
	{
		
	//$contacto=$this->Contactomodel->getNameContacto($con);
	$OD=$this->Contactomodel->getOrdendeCompraPdf($ido);
	if($OD[0]->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.' '.$OD[0]->datc_apellidopaterno.' '.$OD[0]->datc_apellidomaterno;}
else{$facturaNombre=$OD[0]->datc_moral;}


if($OD[0]->datco_snuevo=='si'){$ta='Nuevo';}
elseif($OD[0]->datco_nuevo=='si'){$ta='Demo';}
elseif($OD[0]->datco_nuevox=='si'){$ta='Seminuevo';}
	//$huser=$this->Vendedoresmodel->getId($idh);
	//eruiz@hondaoptima.com			
			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'CRM Hondaoptima.com');
            $this->email->to(''.$email.'');
			$this->email->cc('aimontoya@hondaoptima.com');
            $this->email->subject('Notificación[Honda Optima] - Auto Entregado');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     
                     	<br>
                     
                     	<div>
                     	
                     	 <br><br>
<b>Nombre del Cliente:</b>'.$facturaNombre.'
<br><br>
<b>Modelo y Año del Carro:</b>'.$OD[0]->data_marca.' '.$OD[0]->data_modelo.' '.$OD[0]->data_version.' '.$OD[0]->data_ano.'
<br><br>
<b>Correo electrónico del cliente:</b>'.$OD[0]->datc_email.'
<br><br>
<b>Tipo de Auto:</b>'.$ta.'
<br><br> 
<b>Nombre del Asesor:</b>'.$OD[0]->hus_nombre.' '.$OD[0]->hus_apellido.'
                 
<br><br>


							<br><br>
					
                     		<br>
                     		<br>
						
                     	<br><br><br><br><br><br>
                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>

            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}
	
	function sendEmailClienteTj($ido)
	{
		
	//$contacto=$this->Contactomodel->getNameContacto($con);
	$OD=$this->Contactomodel->getOrdendeCompraPdf($ido);
	if($OD[0]->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.' '.$OD[0]->datc_apellidopaterno.' '.$OD[0]->datc_apellidomaterno;}
else{$facturaNombre=$OD[0]->con_titulo.' '.$OD[0]->con_nombre.' '.$OD[0]->con_apellido;}
$meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>"Noviembre",'12'=>"Diciembre");
	
	$emailCliente=''.$OD[0]->datc_email.'';
			if(empty($emailCliente)){$emailCliente='hondaoptima@hondaoptima.com';}
	
			$this->email->set_mailtype("html");
		    $this->email->from($OD[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to(''.$emailCliente.'');
			$this->email->cc(''.$OD[0]->hus_correo.'');
			$this->email->cc('aimontoya@hondaoptima.com');
			$this->email->bcc('liusber_00@hotmail.com');			
            $this->email->subject('Bienvenido a la familia Honda');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:black;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="630" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     
                     	<br>
                     
                     	<div>
<table align="right" style="color:black"><tr><td>                     	
Tijuana B.C. '.$meses[date('m')].' de '.date('Y').'
</td></tr></table>
 <br>
<b>'.mb_strtoupper($facturaNombre).'</b>
<br><br>
<p style="text-align:justify">
En Honda Optima la atención al cliente y la calidad de nuestros productos y servicios son  nuestros compromisos. Queremos aprovechar esta oportunidad para felicitarlo y darle la bienvenida a la familia Honda, es por eso que a continuación le recordamos los servicios que tenemos para Usted y su automóvil. 
<br><br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Servicio para su automóvil - Nuestro departamento de Servicio tiene el compromiso de ofrecer atención personalizada a precios muy accesibles y  responder a sus necesidades como Usted lo merece. Por ello, utilizamos partes originales Honda y personal altamente calificado, conservando así el valor de su inversión. Cada vez que su automóvil requiera un servicio o alguna atención, le sugerimos comunicarse a la extensión 119,120 ó 116 ID.152*132171*5 para brindarle la atención adecuada.  Nuestro horario de servicio es de 8:00 a 18:00 hrs. de Lunes a Viernes y Sábados de 8:00 a 13:30 hrs.
</td></tr></table>

<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Refacciones y Accesorios Originales Honda y Acura. Ext. 107
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Extensión de Garantía Original Defensa a Defensa para  autos nuevos de 1 a 3 años adicionales a la Garantía Original de Honda de 3 años. Para mayor información estamos a sus órdenes en nuestra concesionaria en la extensión 120.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Certificado de Garantía para autos seminuevos en tren motriz de 1 a 2 años. Aplicable a automóviles con menos de 100 mil Km. y con un máximo de 5 años de antigüedad al año modelo actual. Si desea más información, lo atenderemos con gusto en la extensión 120. 
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Seguro Nacional y Responsabilidad Civil en Estados Unidos con precios especiales para Clientes Honda. Para mayor información estamos a sus órdenes en la extensión 108.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Nosotros valoramos su preferencia. Cuando traiga su auto a servicio, nos encargamos de llevarlo a  Usted a su casa u oficina.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Club de Privilegios Honda, un concepto innovador único en la industria automotriz, con el que acumula puntos para canjearlos por los exclusivos beneficios. Reciba más información marcando a la Ext. 132  y/o accesando a la página: <a href="http://www.clubprivilegioshonda.com.mx">www.clubprivilegioshonda.com.mx</a> 
</td></tr></table>
<br><br>

<p style="text-align:justify">
En Honda Optima contamos con los recursos técnicos y humanos para ofrecer un servicio de Calidad a clientes tan valiosos como Usted.
</p>
<br>
<center>
<b>Felicidades y Gracias por Su Confianza.</b>
<br><br>
<b>Asesor de Ventas</b>
<br>
<b>'.$OD[0]->hus_nombre.' '.$OD[0]->hus_apellido.'</b>
</center>


                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>

            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}
	
	
	function sendEmailClienteEs($ido)
	{
		
	//$contacto=$this->Contactomodel->getNameContacto($con);
	$OD=$this->Contactomodel->getOrdendeCompraPdf($ido);
	if($OD[0]->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.' '.$OD[0]->datc_apellidopaterno.' '.$OD[0]->datc_apellidomaterno;}
else{$facturaNombre=$OD[0]->con_titulo.' '.$OD[0]->con_nombre.' '.$OD[0]->con_apellido;}
$meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>"Noviembre",'12'=>"Diciembre");
	
            $emailCliente=''.$OD[0]->datc_email.'';
			if(empty($emailCliente)){$emailCliente='hondaoptima@hondaoptima.com';}
	
			$this->email->set_mailtype("html");
		    $this->email->from($OD[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to(''.$emailCliente.'');
			$this->email->cc('aimontoya@hondaoptima.com');
			$this->email->bcc('liusber_00@hotmail.com');
			$this->email->bcc('andream@hondaoptima.com');			
            $this->email->subject('Bienvenido a la familia Honda');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:black;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="630" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     
                     	<br>
                     
                     	<div>
<table align="right" style="color:black"><tr><td>                     	
Ensenada B.C. '.$meses[date('m')].' de '.date('Y').'
</td></tr></table>
 <br>
<b>'.mb_strtoupper($facturaNombre).'</b>
<br><br>
<p style="text-align:justify">
En Honda Optima la atención al cliente y la calidad de nuestros productos y servicios son  nuestros compromisos. Queremos aprovechar esta oportunidad para felicitarlo y darle la bienvenida a la familia Honda, es por eso que a continuación le recordamos los servicios que tenemos para Usted y su automóvil. 
<br><br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Servicio para su automóvil - Nuestro departamento de Servicio tiene el compromiso de ofrecer atención personalizada a precios muy accesibles y  responder a sus necesidades como Usted lo merece. Por ello, utilizamos partes originales Honda y personal altamente calificado, conservando así el valor de su inversión. Cada vez que su automóvil requiera un servicio o alguna atención, le sugerimos comunicarse a la extensión 111,112 ó 113 para brindarle la atención adecuada.  Nuestro horario de servicio es de 8:00 a 18:00 hrs. de Lunes a Viernes y Sábados de 8:00 a 13:30 hrs.
</td></tr></table>

<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
Refacciones y Accesorios Originales Honda y Acura. Ext. 109
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
 	Extensión de Garantía Original Defensa a Defensa para  autos nuevos de 1 a 3 años adicionales a la Garantía Original de Honda de 3 años. Para mayor información estamos a sus órdenes en nuestra concesionaria en la extensión 113.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
 Certificado de Garantía para autos seminuevos en tren motriz de 1 a 2 años. Aplicable a automóviles con menos de 100 mil Km. y con un máximo de 5 años de antigüedad al año modelo actual. Si desea más información, lo atenderemos con gusto en la extensión 113. 
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
 	Seguro Nacional y Responsabilidad Civil en Estados Unidos con precios especiales para Clientes Honda. Para mayor información estamos a sus órdenes en la extensión 104.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
<img ver src="http://www.hondaoptima.com/ventas/img/vineta.jpg"></td>
<td style="text-align:justify; color:black">
 Nosotros valoramos su preferencia. Cuando traiga su auto a servicio, nos encargamos de llevarlo a  Usted a su casa u oficina.
</td></tr></table>
<br><br>

<p style="text-align:justify">
En Honda Optima contamos con los recursos técnicos y humanos para ofrecer un servicio de Calidad a clientes tan valiosos como Usted.
</p>
<br>
<center>
<b>Felicidades y Gracias por Su Confianza.</b>
<br><br>
<b>Asesor de Ventas</b>
<br>
<b>'.$OD[0]->hus_nombre.' '.$OD[0]->hus_apellido.'</b>
</center>


                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>

            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}
	
	
	
	function sendEmailClienteMx($ido)
	{
		
	//$contacto=$this->Contactomodel->getNameContacto($con);
	$OD=$this->Contactomodel->getOrdendeCompraPdf($ido);
	if($OD[0]->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre.' '.$OD[0]->datc_apellidopaterno.' '.$OD[0]->datc_apellidomaterno;}
else{$facturaNombre=$OD[0]->con_titulo.' '.$OD[0]->con_nombre.' '.$OD[0]->con_apellido;}
$meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>"Noviembre",'12'=>"Diciembre");
	
	 $emailCliente=''.$OD[0]->datc_email.'';
			if(empty($emailCliente)){$emailCliente='hondaoptima@hondaoptima.com';}
			$this->email->set_mailtype("html");
		    $this->email->from($OD[0]->hus_correo, 'CRM Hondaoptima.com');
            $this->email->to($emailCliente);
			$this->email->cc($OD[0]->hus_correo);
			$this->email->cc('lflores@hondaoptima.com');
			$this->email->cc('lucero@hondaoptima.com');		
			$this->email->bcc('liusber_00@hotmail.com');			
            $this->email->subject('Bienvenido (a) a la Familia Honda Optima');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
  <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border:1px solid white;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid white;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:black;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="white">
                  <tr>
                     <td valign="top" align="center">
                         <img style="border:3 px solid white" src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
						 
                     </td>
                  </tr>
               </table>
      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="630" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	
                     
                     	<br>
                     
                     	<div>
 <br>
<b>Apreciable Cliente:'.mb_strtoupper($facturaNombre).'</b>
<br><br>
<p style="text-align:justify">
En Honda Optima la atención al cliente y la calidad de nuestros productos y servicios son nuestros
compromisos. Queremos aprovechar esta oportunidad para felicitarlo y darle la bienvenida a la familia
Honda, es por eso que a continuación le recordamos los servicios que tenemos para Usted y su automóvil.
<br><br>
<table ><tr><td style="vertical-align:top;">  
*</td>
<td style="text-align:justify; color:black">
Servicio para su automóvil – Nuestro departamento de Servicio tiene el compromiso de ofrecer atención
personalizada a precios muy accesibles y responder a sus necesidades como Usted lo merece. Por ello,
utilizamos partes originales Honda y personal altamente calificado, conservando así el valor de su inversión.
Cada vez que su automóvil requiera un servicio o alguna atención, le sugerimos comunicarse a la extensión
109,111 o 113/ID 152*132171*15 para brindarle la atención adecuada. Nuestro horario de servicio es de 7:30
a 18:00 hrs. De lunes a viernes y sábados de 8:00 a 13:30 hrs.
</td></tr></table>

<br>
<table ><tr><td style="vertical-align:top;">  
*</td>
<td style="text-align:justify; color:black">
Refacciones y Accesorios Originales Honda y Acura Ext. 118
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">  
*</td>
<td style="text-align:justify; color:black">
 	Extensión de Garantía original defensa a defensa para autos nuevos de 1 a 3 años adicionales a la
Garantía Original de Honda de 3 años. Para mayor información estamos a sus órdenes en nuestra
concesionaria a la extensión 109.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">*</td>
<td style="text-align:justify; color:black">
Certificado de garantía para autos seminuevos en tren motriz de 1 a 2 años. Aplicable a automóviles con
menos de 100 mil KM y con un máximo de 5 años de antigüedad al año modelo actual. Si desea más
información, lo atenderemos con gusto en la extensión 109.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">*</td>
<td style="text-align:justify; color:black">
Seguro Nacional y Responsabilidad Civil en Estados Unidos con precios especiales para Clientes Honda.
Para mayor información estamos a sus órdenes en la extensión 106.
</td></tr></table>


<br>
<table ><tr><td style="vertical-align:top;">*</td>
<td style="text-align:justify; color:black">
Nosotros valoramos su preferencia. Cuando traiga su auto a servicio, nos encargamos de llevarlo a Usted
a su casa u oficina.
</td></tr></table>

<br>
<table ><tr><td style="vertical-align:top;">*</td>
<td style="text-align:justify; color:black">
Club de privilegios Honda, un concepto mayor a innovador único en la industria automotriz, con el que
acumula puntos para canjearlos por los exclusivos beneficios. Reciba más información marcando a la Ext.
120 y/o accesando a la página:<a href="http://www.clubdeprivilegios.com.mx">www.clubdeprivilegios.com.mx</a><br>
En Honda Optima contamos con los recursos técnicos y humanos para ofrecer un servicio de Calidad a
clientes tan valiosos como Usted.
</td></tr></table>
<br><br>

<br>
<center>
<b>Felicidades y Gracias por Su Confianza.</b>

</center>


                     		
                   
                     	</div>
                     	
                     	
                    
                     	<br>

            </td>
         </tr>
      </table>
      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
		 <tr>
            <td valign="top" align="center">
              <img src="http://www.hondaoptima.com/ventas/img/privilegios.jpg">
            </td>
         </tr>
         
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
		
			
			');
			$this->email->send();
				
		
	}





	
}
?>