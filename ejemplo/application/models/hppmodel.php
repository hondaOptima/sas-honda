<?php
class Hppmodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
 

	
	
	function insertLlamada($todo)
	{
     $this->db->insert('llamada_entrante', $todo);
	
	
	}
	
	
	function getLlamadasEntrantes()
	{
    $query = $this->db->query('select * from llamada_entrante where  lle_status ="pendiente"	');		
    return $query->result();
	
	
	}
	
	function getLlamadasEntrantesid($id)
	{
    $query = $this->db->query('select * from llamada_entrante where lle_asesor_deventa='.$id.' and lle_status ="pendiente"		');		
    return $query->result();
	
	
	}
	
	function getTodoGuiaTelefonica($id)
	{
    $query = $this->db->query('select * from guia_telefonica,contacto,huser where 
	contacto_con_IDcontacto=con_IDcontacto
	and huser_hus_IDhuser=hus_IDhuser
	and  gte_IDguia_telefonica='.$id.'		');		
    return $query->result();
	
	
	}
	
	function UpdateLlamada($todo,$id)
	{
		
	$this->db->update('llamada_entrante', $todo, array('lle_IDllamada_entrante' => $id));
            return true;
		
	}
	function Updateguiatel($todo,$id)
	{
	$this->db->update('guiainternet', $todo, array('gin_IDguiainternet' => $id));
            return true;	
		}
	function getLlamadasEntrantesidedit($id)
	{
    $query = $this->db->query('select * from llamada_entrante where lle_IDllamada_entrante='.$id.'	');		
    return $query->result();
	
	
	}
	
	
	function getGuiasTelefonicas($id)
	{
		$query = $this->db->query('select * from guia_telefonica where huser_hus_IDhuser='.$id.'	');		
    return $query->result();
		}
		
		
		function getGuiasTelefonicasInternet($id)
	{
		$query = $this->db->query('select * from guiainternet,contacto,huser where gin_IDguiainternet='.$id.'
		and contacto_con_IDcontacto=con_IDcontacto
		and huser_hus_IDhuser=hus_IDhuser	');		
    return $query->result();
		}
		
		
	function getUnaRazon($id)
	{
	$query = $this->db->query('select * from hojaunarazon where huserID='.$id.'	');		
    return $query->result();	
		
	}
	
	function deleteguiaInternet($id)
    {
		 $this->db->delete('guiainternet', array('gin_IDguiainternet' => $id));

    }
	
	
	
}
?>