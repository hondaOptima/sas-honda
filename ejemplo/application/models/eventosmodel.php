<?php
class Eventosmodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
 
 function listfecha($date){
		
		list($mes,$dia,$ano)=explode('/',$date);
			return $fecha=$ano.'-'.$mes.'-'.$dia;
		}
   
	function addEventos($todo)
    {
         $this->db->insert('eventos', $todo); 
		 
		  $query=$this->db->query('select LAST_INSERT_ID() as EID from eventos');
	 $result = $query->result();
     return $result[0];
    }
	  function getEventos($cd)
    {
    $query=$this->db->query('select * from eventos where eve_status !="eliminado"  and eve_fecha_fin>="'.date('Y-m-d').'" ');		
    return $query->result();
	}
	
	function asesores()
	{
	$query=$this->db->query('select * from huser where hus_status !="Ex Empleado" and hus_ciudad="'.$_SESSION['ciudad'].'" ');		
    return $query->result();
	}
	function asesoresAll()
	{
	$query=$this->db->query('select * from huser where hus_status !="Ex Empleado"  ');		
    return $query->result();
	}
	 
	  function getEventosV($id)
    {
    $query=$this->db->query('select * from eventos,notificaciones where eventos_eve_IDeventos=eve_IDeventos and   eve_status="activa" and huser_hus_IDhuser='.$id.' and eve_fecha_fin>="'.date('Y-m-d').'"');		
   

      return $query->result();
	
	 }
	 
	 
	  function getEventosUno($id)
    {
    $query=$this->db->query('select * from eventos where eve_status !="eliminado" and eve_IDeventos='.$id.' ');		
   

      return $query->result();
	
	 }
	
	
	function updateEventos($todo,$id)
    {
         $this->db->update('eventos', $todo, array('eve_IDeventos' => $id));
            return true;
    }
	
	
	function delete($id)
    {
		$todo = array('eve_status'=>'eliminado');
         $this->db->update('eventos', $todo, array('eve_IDeventos' => $id));
            return true;
    }
	
	function openEvento($id,$idh)
    {
		
         $this->db->query('update notificaciones set not_status="abierta" where eventos_eve_IDeventos='.$id.' and huser_hus_IDhuser='.$idh.'');
            return true;
    }
	
	
	function addNotificaciones($id){
		
		
		$result = $this->db->query('Select * from huser where hus_status !="Ex Empleado"');	
		
		
		
    if($result->num_rows() > 0){
        foreach($result->result_array() as $row){
			
			$todo=array('not_fecha'=>date('Y').'-'.date('m').'-'.date('d'),
	'not_status'=>'cerrada',
	'huser_hus_IDhuser'=>$row['hus_IDhuser'],
	'eventos_eve_IDeventos'=>$id,
	);
	
	
	 $this->db->insert('notificaciones', $todo);
			
			
			
        }
    }
	
	}
	
		function addNotificacionesUno($id,$idh){
		
		$todo=array('not_fecha'=>date('Y').'-'.date('m').'-'.date('d'),
	'not_status'=>'cerrada',
	'huser_hus_IDhuser'=>$idh,
	'eventos_eve_IDeventos'=>$id,
	);
	$this->db->insert('notificaciones', $todo);
		}
		
		
		
function emailasesores($email,$titulo,$des,$fecha,$filex,$filex2){

//preparacion y envio del correo
			$this->email->set_mailtype("html");
		    $this->email->from('hondaoptima@hondaoptima.com', 'Honda Optima');
            $this->email->to($email);
			$this->email->cc('liusber_00@hotmail.com');
			$this->email->subject('Noticia: '.$titulo.'');
            $this->email->message('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;} 
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; } 
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}
	
	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">
              
               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<br>
                     	
                     	<br><br>
                     	<div >
Fecha:'.$fecha.'<br>
Titulo:'.$titulo.'
<br><br>
'.$des.'         		
                     	</div>
                     	<div>
                     		<br>
                     
                     	
                     	
                     	
                     	<br>
            </td>
         </tr>
      </table>
      
  
      

      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>
			
			');
			if($filex!='evento.jpg'){$this->email->attach('upload/eventos/'.$filex.'');}
			if($filex2!='evento.jpg'){$this->email->attach('upload/eventos/'.$filex2.'');}
			
			////$this->email->attach('downloads/cotizaciones/HondaOptima.pdf');
			/*$this->email->attach('downloads/cotizaciones/HojaInformativaCPHversion2013.jpg');
			$this->email->attach('downloads/cotizaciones/HojaInformativaGarantiaHDMversion2013.jpg');
			$this->email->attach('downloads/cotizaciones/HojaInformativaHistoriaHondaversion2013.jpg');
			$this->email->attach('downloads/cotizaciones/HojaInformativaSeguroGNPversion2013.jpg');
			*/
			$this->email->send();	
		
	}	
		
	
		
		
		

}
?>