<?php
class Vendedoresmodel extends CI_Model {
 
    public function __construct()
    {
        // model constructor
        parent::__construct();
    }
 
 
 
 function getContactosHuser($idh){
   $query = $this->db->query('select count(con_IDcontacto) as ncon from contacto where  huser_hus_IDhuser='.$idh.' and con_status!="eliminado"');		
   
    return $query->result();	 
	 
	 }
	 
	 function getContactosHuserNombre($idh){
   $query = $this->db->query('select con_IDcontacto from contacto where  huser_hus_IDhuser='.$idh.' and con_status!="eliminado"');		
   
    return $query->result();	 
	 
	 }
	 
	
	 
	 function getVentasHuser($id,$cd){
   $query = $this->db->query('
   
   Select count(dor_IDdordencomprat) as total
from ventas_facturado 
where
 hus_ciudad="'.$cd.'"
and datco_snuevo="si"
and hus_IDhuser="'.$id.'"
union all
Select count(dor_IDdordencomprat) as total 
from  ventas_entrega
where
 hus_ciudad="'.$cd.'"
and datco_snuevo="si"
and hus_IDhuser="'.$id.'"
   ');		
   
    return $query->result();	 
	 
	 }
	 
	 
	 
	 function getVentasHuserNombre($id,$cd){
   $query = $this->db->query('
   
   Select dor_IDdordencomprat
from ventas_facturado 
where
 hus_ciudad="'.$cd.'"
and datco_snuevo="si"
and hus_IDhuser="'.$id.'"
union all
Select dor_IDdordencomprat 
from  ventas_entrega
where
 hus_ciudad="'.$cd.'"
and datco_snuevo="si"
and hus_IDhuser="'.$id.'"
   ');		
   
    return $query->result();	 
	 
	 }
 
    function totalventas($finicio,$ffin,$idv,$tp){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='0' || $idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
		
if($tp=='no'){$andtp='';}
		elseif($tp=='nuevo'){$andtp='and datco_snuevo="si"';}	
		elseif($tp=='demo'){$andtp='and datco_nuevo="si"';}
		elseif($tp=='semi'){$andtp='and datco_nuevox="si"';}	
	
	 $qry='
Select count(dor_IDdordencomprat) as total
from ventas_facturado 
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
and hus_ciudad="Tijuana"
'.$andtp.'
union all
Select count(dor_IDdordencomprat) as total 
from  ventas_entrega
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
and hus_ciudad="Tijuana"
'.$andtp.'
union all
Select count(dor_IDdordencomprat) as total
from ventas_facturado 
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
and hus_ciudad="Mexicali"
'.$andtp.'
union all
Select count(dor_IDdordencomprat) as total 
from  ventas_entrega
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
and hus_ciudad="Mexicali"
'.$andtp.'
union all
Select count(dor_IDdordencomprat) as total
from ventas_facturado 
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
and hus_ciudad="Ensenada"
'.$andtp.'
union all
Select count(dor_IDdordencomprat) as total 
from  ventas_entrega
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
and hus_ciudad="Ensenada"
'.$andtp.'
	 ';
	$query = $this->db->query($qry);		
    return $query->result();
		}
 
    function getTodo($cd)
    {
         $query = $this->db->query('select * from huser where hus_status !="Ex Empleado" and hus_ciudad="'.$cd.'"  ');		
   
       return $query->result();
    }
	
	  function citashoy($cd)
    {
         $query = $this->db->query('select * 
	from huser,contacto,fuente,bitacora,actvidades 
	where 
	hus_IDhuser=huser_hus_IDhuser and 
	con_IDcontacto=contacto_con_IDcontacto and 
	fuente_fue_IDfuente=fue_IDfuente and 
	bit_IDbitacora=bitacora_bit_IDbitacora and
	tipo_actividad_tac_IDtipo_actividad=8 and 
act_fecha_inicio="'.date('Y-m-12').'"
and hus_ciudad="'.$cd.'"
and act_status="realizada"
and act_confirmacion="ok"  ');		
   
       return $query->result();
    }
	
	function getTareasRealizadas($finicio,$ffin,$idv,$ciu){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
	$qry='
	select * 
	from huser,contacto,bitacora,actvidades 
	where hus_IDhuser=huser_hus_IDhuser
	and con_IDcontacto=contacto_con_IDcontacto
	and bit_IDbitacora=bitacora_bit_IDbitacora
	'.$and.'
	and act_fecha_inicio BETWEEN "'.$finicio.'" and "'.$ffin.'"
	and hus_ciudad="'.$ciu.'"
	and act_status="realizada"
	 ';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		function getTareasNoRealizadas($finicio,$ffin,$idv,$ciu){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
	
	if($idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
	$qry='
	select * 
	from huser,contacto,bitacora,actvidades 
	where hus_IDhuser=huser_hus_IDhuser
	and con_IDcontacto=contacto_con_IDcontacto
	and bit_IDbitacora=bitacora_bit_IDbitacora
	'.$and.'
	and act_fecha_inicio BETWEEN "'.$finicio.'" and "'.$ffin.'"
	and hus_ciudad="'.$ciu.'"
	and act_status="pendiente"
	 ';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		function getVentas($finicio,$ffin,$idv,$ciu,$tp){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='0' || $idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
		
if($tp=='no'){$andtp='';}
		elseif($tp=='nuevo'){$andtp='and datco_snuevo="si"';}	
		elseif($tp=='demo'){$andtp='and datco_nuevo="si"';}
		elseif($tp=='semi'){$andtp='and datco_nuevox="si"';}	
		
		if($_SESSION['username']=='josemaciel@hondaoptima.com') {
			
		if($ciu=='0'){$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';}
		else{$ciudad='and hus_ciudad="'.$ciu.'"';}	
		
		}
		elseif($_SESSION['username']=='monica@hondaoptima.com') {$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';}
		else{$ciudad='and hus_ciudad="'.$ciu.'"';}
		
	 $qry='
Select * 
from ventas_facturado 
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
'.$ciudad.'
'.$andtp.'

	 ';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		function getVentasA($finicio,$ffin,$idv,$ciu,$tp){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='0' || $idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
		
if($tp=='no'){$andtp='';}
		elseif($tp=='nuevo'){$andtp='and datco_snuevo="si"';}	
		elseif($tp=='demo'){$andtp='and datco_nuevo="si"';}
		elseif($tp=='semi'){$andtp='and datco_nuevox="si"';}	
		
		$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';
		
	 $qry='
Select * 
from ventas_facturado 
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
'.$ciudad.'
'.$andtp.'

	 ';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		
	function vmaset($id,$cd)
	{
$eno=$this->getVentas('01-03-2014','31-03-2014',$id,$cd,'nuevo');
$fac=$this->getVentasFacturados('01-03-2014','31-03-2014',$id,$cd,'nuevo');
if($eno){$ceno=count($eno);}else{$ceno=0;}
if($fac){$cfac=count($fac);}else{$cfac=0;}
$ase= $ceno + $cfac;
return $ase;
}	



function vmasetA($id,$cd)
	{
$eno=$this->getVentas('01-01-2014','31-12-2014',$id,$cd,'nuevo');
$fac=$this->getVentasFacturados('01-01-2014','31-12-2014',$id,$cd,'nuevo');
$ase= count($eno) + count($fac);
return $ase;
}	
		
				function getVentasSinFecha($finicio,$ffin,$idv,$ciu,$tp){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
		
		if($tp=='no'){$andtp='';}
		elseif($tp=='nuevo'){$andtp='and datco_snuevo="si"';}	
		elseif($tp=='demo'){$andtp='and datco_nuevo="si"';}
		elseif($tp=='semi'){$andtp='and datco_nuevox="si"';}	
		
		if($_SESSION['username']=='josemaciel@hondaoptima.com' || $_SESSION['username']=='sgutierrez@hondaoptima.com') {$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';}
		elseif($_SESSION['username']=='monica@hondaoptima.com') {$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';}
		else{$ciudad='and hus_ciudad="'.$ciu.'"';}
		
	$qry='
Select * 
from dordendencomprat,datos,datos_auto,datos_cliente,datos_seguro,datos_documentos_recibidos,datos_compra,avisodepedido,dirfacturacion,contacto,huser,asigancion_auto,procesodeventa
where
datos_dat_IDdatos=dat_IDdatos
and datos_auto_data_=data_
and datos_cliente_datc_IDdatos_clientes=datc_IDdatos_clientes
and datos_seguro_dats_IDdatos_seguro=dats_IDdatos_seguro
and datos_documentos_recibidos_datdr_datos_documentos_recibidos=datdr_datos_documentos_recibidos
and datos_compra_datco_IDdatos_compra=datco_IDdatos_compra 
and idavisodepedido=avisodepedido_idavisodepedido 
and datos_dirfacturacion=dfa_IDdirfacturacion
and dordendencomprat.contacto_con_IDcontacto=con_IDcontacto
and contacto.huser_hus_IDhuser=hus_IDhuser
'.$and.'
'.$ciudad.'
'.$andtp.'
and aau_IdFk=data_vin
and prcv_IDordencompra=dor_IDdordencomprat
and prcv_status="venta"
	 ';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		
		function getVentasFacturados($finicio,$ffin,$idv,$ciu,$tp){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='0' || $idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
		
		if($tp=='no'){$andtp='';}
		elseif($tp=='nuevo'){$andtp='and datco_snuevo="si"';}	
		elseif($tp=='demo'){$andtp='and datco_nuevo="si"';}
		elseif($tp=='semi'){$andtp='and datco_nuevox="si"';}	
		
		
		
		if($_SESSION['username']=='josemaciel@hondaoptima.com' || $_SESSION['username']=='sgutierrez@hondaoptima.com') {
		if($ciu=='0'){$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';}
		else{$ciudad='and hus_ciudad="'.$ciu.'"';}	
		}
		else{$ciudad='and hus_ciudad="'.$ciu.'"';}
 $qry='
Select * 
from  ventas_entrega
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
'.$ciudad.'
'.$andtp.'
';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		
		function getVentasFacturadosA($finicio,$ffin,$idv,$ciu,$tp){
	list($idia,$imes,$ianio)=explode('-',$finicio);
	$finicio=$ianio.'-'.$imes.'-'.$idia;
	list($fdia,$fmes,$fanio)=explode('-',$ffin);
	$ffin=$fanio.'-'.$fmes.'-'.$fdia;
		if($idv=='0' || $idv=='no'){
     $and='';		
	}else{
	$and='and hus_IDhuser='.$idv.'';	
		}
		
		if($tp=='no'){$andtp='';}
		elseif($tp=='nuevo'){$andtp='and datco_snuevo="si"';}	
		elseif($tp=='demo'){$andtp='and datco_nuevo="si"';}
		elseif($tp=='semi'){$andtp='and datco_nuevox="si"';}	
		
		$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';
 $qry='
Select * 
from  ventas_entrega
where
dat_fecha_facturacion BETWEEN "'.$finicio.'" and "'.$ffin.'"
'.$and.'
'.$ciudad.'
'.$andtp.'
';
	$query = $this->db->query($qry);		
    return $query->result();
		}
		
		
	  function getTodoa()
    {
        $result = $this->db->query('select * from huser where hus_status !="Ex Empleado"  ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione un vendedor';
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	

	
	
	  function getTodoaCiudad($cd)
    {
        $result = $this->db->query('select * from huser where hus_status !="Ex Empleado"   ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione un vendedor';
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	
	  function getTodoVendedores()
    {
        $query = $this->db->query('select * from huser where hus_status !="Ex Empleado" ');		
   
       return $query->result();
	
	 }
	 
	  function getTodoaNo($cd)
    {
        $result = $this->db->query('select * from huser where hus_status !="Ex Empleado" and hus_ciudad="'.$cd.'" ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return['no'] = 'Solo Citas';
		     $return['atgenerales'] = 'Solo Tareas Generales';
	
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	 
	  function getTodoaNoA($cd)
    {
        $result = $this->db->query('select * from huser where hus_status !="Ex Empleado" and hus_ciudad IN ("Tijuana","Mexicali","Ensenada") ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return['no'] = 'Solo Citas';
		     $return['atgenerales'] = 'Solo Tareas Generales';
	
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }


function getTodoaNoT($cd)
    {
        $result = $this->db->query('select * from huser where hus_status !="Ex Empleado" and hus_ciudad="'.$cd.'" and hus_nivel IN ("Vendedor","Recepcion")   ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return['no'] = 'Tarea para todos los Asesores';
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	 
	 function getTodoaNoTPros($cd)
    {
        $result = $this->db->query('select * from huser where hus_status !="Ex Empleado" and hus_ciudad="'.$cd.'" and hus_nivel IN ("Vendedor")   ');		
   

    $return = array();
    if($result->num_rows() > 0){
            
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	
	
	
	
	
	function getTodoaNoC($cd)
    {
		if($cd=='0') {$ciudad=' and hus_ciudad IN("Tijuana","Mexicali","Ensenada")';}
		else{$ciudad='and hus_ciudad="'.$cd.'"';}
		
        $result = $this->db->query('select hus_IDhuser,hus_nombre,hus_apellido from huser where hus_status !="Ex Empleado" '.$ciudad.' and hus_nivel IN ("Vendedor","Recepcion")   ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return['no'] = 'Todos los Asesores';
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	
	
	
	
	 function ListadeVendedoresArray($cd)
    {
          $query = $this->db->query('select hus_IDhuser,hus_correo from huser where hus_status !="Ex Empleado" and hus_ciudad="'.$cd.'" and hus_nivel IN ("Vendedor","Recepcion") ');		
   
       return $query->result();
	
	 }
	
	function getTodoAsig($id)
	{
		$qu='select * from asigancion_auto,contacto,huser where aau_IDasignacion_auto='.$id.' and contacto_con_IDcontacto=con_IDcontacto and contacto.huser_hus_IDhuser=hus_IDhuser ';
	$query = $this->db->query($qu);
	return $query->result();		
	}
	
	
	
	
	
	  function getTodosel()
    {

		
		 $result = $this->db->query('select hus_IDhuser,hus_nombre,hus_apellido from huser where hus_status !="Ex Empleado"  ');		
   

    $return = array();
    if($result->num_rows() > 0){
            $return[''] = 'Seleccione un vendedor';
        foreach($result->result_array() as $row){
            $return[$row['hus_IDhuser']] = $row['hus_nombre'].' '.$row['hus_apellido'];
        }
    }
    return $return;
	
	 }
	 function getId($id)
    {
        $query = $this->db->get_where('huser', array('hus_IDhuser'=>$id));		
        return $query->result();
    }
	
	function addVendedores($todo)
    {
         $this->db->insert('huser', $todo); 
    }
	
	function addBaja($todo)
    {
         $this->db->insert('contactos_transferidos', $todo); 
    }
	
	
	function updatetodo($todo,$id)
    {
    	 $todo;
         $this->db->update('huser', $todo, array('hus_IDhuser' => $id));
            return true;
    }
	 
	 function UpdateVentas($todo,$id)
	   {
    	
         $this->db->update('dordendencomprat', $todo, array('dor_IDdordencomprat' => $id));
            return true;
      }
	
	
	function getLogin($email)
    {
        
		$qu ="select * from huser where hus_correo='".$email."' and hus_status='Empleado'";  
		$query = $this->db->query($qu);		
       $result = $query->result();
        return $result[0];
    }
	
	
	function repassword($email,$cad)
    {

		
        
		 $query = $this->db->get_where('huser', array('hus_correo'=>$email));  
 
        if ($query->num_rows()==0) {
            return false;
        }
        else {
            $this->db->update('huser', array('hus_contrasena' =>md5($cad)), array('hus_correo' => $email));
            return true;
        }   
		
		
		
    }
	
	 function get($id) 
    {
        $query = $this->db->get_where('huser', array('hus_IDhuser'=>$id));  
        if ($query->num_rows()==0) {
            return false;
        }
        $result = $query->result();
        return $result[0];
    }
 
    function delete($id)
    {
        
            $this->db->query('update huser set hus_status="Ex Empleado" where hus_IDhuser='.$id.''); 
            return true;
           
    }
 
    function setComplete($id)
    {
        $query = $this->db->get_where('huser', array('hus_IDhuser'=>$id));  
 
        if ($query->num_rows()==0) {
            return false;
        }
        else {
            $this->db->update('huser', array('hus_status' =>'Ex Empl'), array('hus_IDhuser' => $id));
            return true;
        }   
    }
	
	
	 function change_pass($password,$npassword,$id)
    {
        $query = $this->db->get_where('huser', array('hus_contrasena'=>md5($password)));  
 
        if ($query->num_rows()==0) {
            return false;
        }
        else {
            $this->db->update('huser', array('hus_contrasena' =>md5($npassword)), array('hus_IDhuser' => $id));
            return true;
        }   
    }
	
	
    	function updatefoto($todo,$id)
    {
         $this->db->update('huser', $todo, array('hus_IDhuser' => $id));
            return true;
    }
   
   function getTareasP($ido)
   {
	  
	 $query=$this->db->query(' select count(*) as numero from actvidades where act_status="pendiente" and bitacora_bit_IDbitacora='.$ido.''); 
	 $result = $query->result();
        return $result[0];
   }
   
   function getVendedoresAdmin()
   {
	    $query = $this->db->query('select * 
		from huser,contacto,bitacora,oportunidades,t_oportunidades,actvidades 
		where hus_IDhuser=huser_hus_IDhuser
		and con_IDcontacto=contacto_con_IDcontacto
		and bit_IDbitacora=bitacora_bit_IDbitacora
		and opo_IDoportunidad=oportunidades_opo_IDoportunidad
		and t_o_IDt_oportunidades=t_oportunidades_t_o_IDt_oportunidades
		and hus_status="Empleado"
		and act_status="pendiente"
		group by(opo_IDoportunidad)
		');		
        return $query->result();
	   
   }
   
   
   function getVendedoresTareas($id)
   {
	    $query = $this->db->query('select * 
		from huser,contacto,bitacora,oportunidades,t_oportunidades,actvidades 
		where hus_IDhuser=huser_hus_IDhuser
		and con_IDcontacto=contacto_con_IDcontacto
		and bit_IDbitacora=bitacora_bit_IDbitacora
		and opo_IDoportunidad=oportunidades_opo_IDoportunidad
		and t_o_IDt_oportunidades=t_oportunidades_t_o_IDt_oportunidades
		and hus_status="Empleado"
		and act_status="pendiente"
		and hus_IDhuser='.$id.'
		group by(opo_IDoportunidad)
		');		
        return $query->result();
	   
   }
}
?>