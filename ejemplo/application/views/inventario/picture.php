<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Vendedores"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "inventario"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/inventario">Inventario</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="#">Fotos</a>
					</li>
                    
				</ul>
				
			</div>
		  <?php if($uno_inventario): ?>
                          <?php foreach($uno_inventario as $todo): ?>
                           <?php
						    $ida=$todo->aut_IDauto_vin; 
						    $vin=$todo->aut_vin;
							$foto=$todo->aut_foto;
							?>
						   <?php endforeach ?>
	      <?php else: ?>None<?php endif ?>
			
            <?php echo $flash_message ?>
            
             
            
           
            
            <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Subir Fotos</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							
						</div>
					</div>
                    
					<div class="box-content">
                    
                     <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
						<?php echo form_open_multipart('inventario/doUpload/'.$ida.''); ?>
                        
<div class="control-group">
								
								<div class="controls">
                                
                                <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'perfil'  => 'Perfil',
								 'interior'  => 'Interior',
								 'exterior'  => 'Exterior',
								);
								 echo form_dropdown('direccion', $options, '','style="width:246px;" id="selectErroraa" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>                        

<input type="file" name="userfile" id="userfile" />
<input type="submit" value ="Upload" />

<?php echo form_close(); ?>

					</div>
				</div><!--/span-->

			</div>
			
			
			<hr>
			
            
            
             <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Perfil</h2>
						<div class="box-icon">
						
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							
						</div>
					</div>
					<div class="box-content">
                    
                    
						<?php
                        if($foto)
						{
						echo'<div class="masonry-gallery">
														<div id="image-1" class="masonry-thumb">
								<a style="background:url('.base_url().'upload/'.$foto.')" title="Sample Image 1" href="'.base_url().'upload/'.$foto.'"><img class="grayscale" src="'.base_url().'upload/'.$foto.'" alt=""></a>
							</div>';	
						}
						
						?>
                        






					</div>
				</div><!--/span-->

			</div>
	

 
  <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Fotos Interior</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
						
						</div>
					</div>
					<div class="box-content">
                       <div class="masonry-gallery">
                       <?php if($foto_in):?><?php foreach($foto_in as $todo): ?>
                       <?php
					   $idin=$todo->fat_IDfotosauto; 
				       $fotoin=$todo->fat_nombre;
				 echo'>
					  <div id="image-1" class="masonry-thumb">
				      <a style="background:url('.base_url().'upload/'.$fotoin.')" title="Sample Image 1" href="'.base_url().'upload/'.$fotoin.'">
					  <img class="grayscale" src="'.base_url().'upload/'.$fotoin.'" alt=""></a>';?>
                       <?php echo anchor('inventario/deletepicture/'.$ida.'/'.$idin.'/'.$fotoin.'', 'eliminar',
					   array('onClick' => "return confirm('Esta seguro que decea eliminar esta Imagen:$fotoin ?')",'style="color:black; font-size:.9em;"'	)); ?>
					<?php
					  echo'</div>';	
					  ?>
                     
                      <?php endforeach ?>
                      <?php else: ?>Vacio<?php endif ?>
                      
					</div> <br></div>
				</div><!--/span-->

			</div>
 
 
 
 
 
  <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Fotos exterior</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							
						</div>
					</div>
					<div class="box-content">
						
                         <div class="masonry-gallery">
                       <?php if($foto_ex):?><?php foreach($foto_ex as $todo): ?>
                       <?php
					   $idin=$todo->fat_IDfotosauto; 
				       $fotoex=$todo->fat_nombre;
				 echo'
					  <div id="image-1" class="masonry-thumb">
				      <a style="background:url('.base_url().'upload/'.$fotoex.')" title="Sample Image 1" href="'.base_url().'upload/'.$fotoex.'">
					  <img class="grayscale" src="'.base_url().'upload/'.$fotoex.'" alt=""></a>';?>
                      
					   <?php echo anchor('inventario/deletepicture/'.$ida.'/'.$idin.'/'.$fotoex.'', 'eliminar',
					   array('onClick' => "return confirm('Esta seguro que decea eliminar esta Imagen:$fotoex ?')",'style="color:black; font-size:.9em;"'	)); ?>
                       
					  <?php
					  echo '</div>';	
					  ?>
                      <?php endforeach ?>
                      <?php else: ?>Vacio<?php endif ?>
                      
					</div>
                      
                      <br>
                      
                        
                        
					</div>
				</div><!--/span-->

			</div>
 

		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
	

</body>
</html>
