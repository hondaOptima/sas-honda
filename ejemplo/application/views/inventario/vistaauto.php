<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Vendedores"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "inventario"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				
 <ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/inventario">Inventario</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="#">Vista Auto</a>
					</li>
                    
				</ul>
				
			</div>
		
			
			
			
			<hr>
			
	
<div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Informacion General</h2>
						<div class="box-icon">
						
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
					
						</div>
					</div>
					<div class="box-content">
                    
                    
                     <?php if($uno_inventario): ?>
                          <?php foreach($uno_inventario as $todo): ?>
                           <?php
						    $ida=$todo->aut_IDauto_vin; 
						    $suc=$todo->aut_sucursal; 
							$nin=$todo->aut_no_inventario;
							$ano=$todo->aut_ano; 
							$vin=$todo->aut_vin;
							$mot=$todo->aut_motor; 
							$tun=$todo->aut_tipo_unidad;
							$ubi=$todo->aut_ubicacion; 
							$imp=$todo->aut_importe;
							$iva=$todo->aut_iva; 
							$lle=$todo->aut_llegada;
							$mod=$todo->mod_nombre;
							$alm=$todo->alm_nombre;
							$col=$todo->col_nombre; 
							$fte=$todo->fichatecnica_fte_IDfichatecnica; 
							$fot=$todo->aut_foto;
							$fname=$todo->fte_nombre;
							
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                    
                    <style>
                    .ffa{font-size:.9em; font-weight:bold}
					 .ff{font-size:.9em; }
                    </style>
                  
                  <table><tr><td rowspan="8">
                  <img class="grayscale" width="300"  alt="" src="<?php echo base_url(); ?>upload/<?php echo $fot;?>">
                  
                  </td></tr>
                  
                  <tr>
                  <td width="150px"  class="ffa" >Sucursal:</td>
                  <td width="150px" class="ff" ><?php echo $suc;?></td>
                  <td width="150px" class="ffa" >No. Inventario:</td>
                  <td width="150px" class="ff" ><?php echo $nin;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px" class="ffa" >Ubicacion:</td>
                  <td width="150px"class="ff" ><?php echo $ubi;?></td>
                  <td width="150px" class="ffa" >Almacen:</td>
                  <td width="150px" class="ff" ><?php echo $alm;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px"class="ffa" >LLegada:</td>
                  <td width="150px"  class="ff"><?php list($an,$mes,$dia)=explode('-',$lle); echo $dia.'/'.$mes.'/'.$an;?></td>
                  <td width="150px" class="ffa" >Vin:</td>
                  <td width="150px" class="ff" ><?php echo $vin;?></td>
                  </tr>
                  
                  <tr>
                  <td width="150px" class="ffa" >Motor:</td>
                  <td width="150px" class="ff" ><?php echo $mot;?></td>
                  <td width="150px" class="ffa" >Año:</td>
                  <td width="150px" class="ff" ><?php echo $ano;?></td>
                  </tr>
                  
                   <tr>
                  <td width="150px" class="ffa" >Modelo:</td>
                  <td width="150px" class="ff"><?php echo $mod;?></td>
                  <td width="150px" class="ffa" >Color:</td>
                  <td width="150px" class="ff"><?php echo $col;?></td>
                  </tr>
                  
                   <tr>
                  <td width="150px" class="ffa" >Tipo de Unidad:</td>
                  <td width="150px"class="ff" ><?php echo $tun;?></td>
                  <td width="150px" class="ffa" >Importe:</td>
                  <td width="150px" class="ff"><?php echo $imp;?></td>
                  </tr>
                  
                   <tr>
                  <td width="150px" class="ffa" >Iva:</td>
                  <td width="150px" class="ff"><?php echo $iva;?></td>
                  <td width="150px" class="ffa" >Total:</td>
                  <td width="150px" class="ff"><?php echo $tot=$iva+$imp;?></td>
                  </tr>
                  
                  
                  </table>
                                    
                                    
                                    
								  </div>
								 
								</div>
							  </div>
                              
                               
                              
                            
           <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Fotos Interior</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
						
						</div>
					</div>
					<div class="box-content" >
                       <div class="masonry-gallery">
                       <?php if($foto_in):?><?php foreach($foto_in as $todo): ?>
                       <?php
					   $idin=$todo->fat_IDfotosauto; 
				       $fotoin=$todo->fat_nombre;
				 echo'
					  <div id="image-1" class="masonry-thumb">
				      <a style="background:url('.base_url().'upload/'.$fotoin.')" title="Sample Image 1" href="'.base_url().'upload/'.$fotoin.'">
					  <img class="grayscale" src="'.base_url().'upload/'.$fotoin.'" alt=""></a>';?>
                       
					<?php
					  echo'</div>';	
					  ?>
                     
                      <?php endforeach ?>
                      <?php else: ?>Vacio<?php endif ?>
                      
					</div> <br></div>
				</div><!--/span-->

			</div>
           
           
           
           
           
           <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Fotos exterior</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							
						</div>
					</div>
					<div class="box-content">
						
                         <div class="masonry-gallery">
                       <?php if($foto_ex):?><?php foreach($foto_ex as $todo): ?>
                       <?php
					   $idin=$todo->fat_IDfotosauto; 
				       $fotoex=$todo->fat_nombre;
				 echo'
					  <div id="image-1" class="masonry-thumb">
				      <a style="background:url('.base_url().'upload/'.$fotoex.')" title="Sample Image 1" href="'.base_url().'upload/'.$fotoex.'">
					  <img class="grayscale" src="'.base_url().'upload/'.$fotoex.'" alt=""></a>
					  </div>';	
					  ?>
                      <?php endforeach ?>
                      <?php else: ?>Vacio<?php endif ?>
                      
					</div>
                      
                      <br>
                      
                        
                        
					</div>
				</div><!--/span-->

			</div>
           
           
           
                              
                              
                              
                            
                             
                         <div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Ficha Tecnica</h2>
						<div class="box-icon">
						
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
					
						</div>
					</div>
					<div class="box-content">
                    
                       <embed src="<?php echo base_url(); ?>pdf/<?php echo $fname;?>.pdf "  width="100%" height="1000px">
                              
                              
                                    
								  </div>
								 
								</div>
							  </div>   
                            
                            
 
 
 
 
 
 
 
 

		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
	

</body>
</html>
