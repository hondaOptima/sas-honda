<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Inventario"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
				<?php $data["clase"] = "home"; $this->load->view('commons/menu-left',$data); ?>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/inventario">Inventario</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="#">Editar Auto</a>
					</li>
                    
				</ul>
				
			</div>
		
			
			
			
			<hr>
			<a href="javascript:window.history.go(-1);"><button class="btn btn-small btn-success">Regresar</button></a>
	
<div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Formulario Asignacion</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                    
                    

                    
                    <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>


 <?php if($uno_asp): ?>
                          <?php foreach($uno_asp as $todo): ?>
                           <?php
						    $idasp=$todo->das_IDdetalle_asp; 
						    $ano=$todo->das_ano; 
							$mod=$todo->das_modelo;
							$col=$todo->das_color; 
						    $ficha=$todo->fichatecnica_fte_IDfichatecnica;
						    $status=$todo->das_status;
							
						   ?>
						   <?php endforeach ?><?php else: ?>None<?php endif ?>
                           
                          

	<?php echo form_open('inventario/transfer/'.$idasp.'','class="form-horizontal"'); ?>
						  <fieldset>


     
	


       
       <input name="status" type="hidden" value="<?php echo $status;?>">
       <input name="idasp" type="hidden" value="<?php echo $idasp;?>">
       <?php  if($status=='Reservado'){
		    $nombre=$this->Inventariomodel->getnombres($idasp);
		   
		   ?>
       
       
       
       <div class="control-group">
								<label class="control-label" for="focusedInput">Agente de Ventas</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'agente',
              'id'          => 'agente',
              'value'       => $nombre[0]->hus_nombre.' '.$nombre[0]->hus_apellido,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div> 
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contacto</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'contacto',
              'id'          => 'contacto',
              'value'       => $nombre[0]->con_titulo.' '.$nombre[0]->con_nombre.' '.$nombre[0]->con_apellido,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>  
                              
                              
                <?php }?>              
                              
                               
       
        <div class="control-group">
								<label class="control-label" for="focusedInput">Vin</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'vin',
              'id'          => 'vin',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   
       
          <div class="control-group">
								<label class="control-label" for="focusedInput">Año</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'ano',
              'id'          => 'ano',
              'value'       => $ano,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  'readonly'       => 'readonly',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


   <div class="control-group">
								<label class="control-label" for="focusedInput">Modelo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'mod',
              'id'          => 'mod',
              'value'       => $mod,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  
			  'readonly'       => 'readonly',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Color</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'color',
              'id'          => 'color',
              'value'       => $col,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  
			  'readonly'       => 'readonly',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   

 
                                                
   
<table class="table table-striped"><tr><td>
							 <?php echo form_submit('submit', 'Transferir a Inventario en Piso','class="btn btn-primary"'); ?>
							  </td></tr></table>
							</div>                       
                                                          
	
		
	
     </fieldset>
						<?php echo form_close(); ?>
	
          
							
						
					

					</div>
				</div>
 
 
 </div>
 
 
 
 

 
 
 

		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->


		<script>
		
$(document).ready(function(){ 


$('#chall').live('click',function(){
var chk=$('#chall').attr('checked');
if(chk==true){
// Seleccionar un checkbox
$('input:checkbox').attr('checked', true);
}
else{
// Deseleccionar un checkbox
$('input:checkbox').attr('checked', false);
}
});





		$("#add").click(function() {
var num=$('input[name=suma]').val();
if(num==0){$('input[name=suma]').val('1'); var mass=1; }
if(num>0){var nums=$('input[name=suma]').val(); var mass=parseInt(nums); var sma=(mass + 1);
$('input[name=suma]').val(sma)}
/* Opción 1 */
var n = $('tr:last td', $("#tabins")).length;
var tds = '<tr  style="border:1px solid #CCC" height="30px" id="rowDetalle_'+mass+'" >';
tds += '<td><input type="checkbox" value="rowDetalle_'+mass+'" name="Dependiente[]"></td>';
tds += '<td><input type="text" style="width:32px" name="ano[]"></td>';
tds += '<td><input type="text" name="modelo[]"></td>';
tds += '<td><input type="text" name="color[]"></td>';
tds += '<td><select name="fichatecnica[]"><option value="">No definida</option><option  value="1">accord-crosstour-2011</option><option  value="2">civic-coupe-2012</option><option  value="3">civic-hybrid-2012</option><option  value="4">civic-sedan-2012</option><option  value="5">civic-si-2012</option><option  value="6">cr-v-2012</option><option  value="7">ridgeline-2012</option><option  value="8"> 	accord-coupe-2013</option><option  value="9">accord-crosstour-2013</option><option  value="10">accord-sedan-2013</option><option  value="11">civic-coupe-2013</option><option  value="12">civic-hybrid-2013</option><option  value="13">civic-sedan-2013</option><option  value="14">civic-si-2013</option><option  value="15">cr-v-2013</option><option  value="16">honda-city-2013</option><option  value="17"> 	honda-fit-2013</option><option  value="18"></option><option  value="1"></option><option  value="1"></option></select></td>';
tds += '</tr>';
$("#tabins").append(tds);
}); 
}); 


function remove()
{
$(":checkbox:checked").each(
function() {
var ch= $(this).val();

respuesta = confirm("Eliminar Linea? ");
if (respuesta){
$('#'+ch).remove();
var t=0;
}
}
);
} 
		</script>		                              
             
                             
                            
                            
	


</body>
</html>
