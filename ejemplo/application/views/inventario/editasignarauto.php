<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Inventario"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "inventario"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/inventario">Inventario</a><span class="divider">/</span>
					</li>
                     <li>
						<a  href="javascript:window.history.go(-1);">Autos Apartados</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="#">Editar Apartado</a>
					</li>
                    
				</ul>
				
			</div>
		  
			
            <?php echo $flash_message ?>
             <?php if($uno_apartado): ?>
                          <?php foreach($uno_apartado as $todo): ?>
                           <?php
						    $ven=$todo->huser_hus_IDhuser; 
						    $con=$todo->con_IDcontacto; 
							$pag=$todo->aau_pago;
							$recibo=$todo->aau_folio; 
							$ano=$todo->das_ano;
							$mod=$todo->das_modelo;
							$col=$todo->das_color;
							$ficha=$todo->fte_nombre;
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                     
                     
                     
                     
             
            
           	<hr>
            
            <div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Editar Apartado</h2>
						<div class="box-icon">
						
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
						
						</div>
					</div>
					<div class="box-content">
                    
                    

                    
                    <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
							<?php echo form_open('inventario/updateasig/'.$idg,'class="form-horizontal"'); ?>
						  <fieldset>
                          
                            <div class="control-group">
								<label class="control-label" for="selectErroraad">Vendedor</label>
								<div class="controls">
                                
                                <?php
                               
								 echo form_dropdown('vendedor', $todo_vendedor,$ven,'style="width:246px;" id="selectErroraad" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>    
                              
                              
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraav">Contacto</label>
								<div class="controls">
                                
                                <?php
                               
								 echo form_dropdown('contacto', $todo_contacto,$con,'style="width:246px;" id="selectErroraav" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>  
                              
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">No. Recibo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'recibo',
              'id'          => 'recibo',
              'value'       => $recibo,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Cantidad</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on">$</span>
                                    <?php
									
									
									$data = array(
              'name'        => 'cantidad',
              'id'          => 'cantidad',
              'value'       => $pag,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>      
       
       
       
          <div class="control-group">
								<label class="control-label" for="focusedInput">Año</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'ano',
              'id'          => 'ano',
              'value'       => $ano,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


   <div class="control-group">
								<label class="control-label" for="focusedInput">Modelo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'mod',
              'id'          => 'mod',
              'value'       => $mod,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Color</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'color',
              'id'          => 'color',
              'value'       => $col,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


   <div class="control-group">
								<label class="control-label" for="focusedInput">Ficha Tecnica</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'ficha',
              'id'          => 'ficha',
              'value'       => $ficha,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


 
                                          
   
<table class="table table-striped"><tr><td>
							 <?php echo form_submit('submit', 'Guardar Informacion','class="btn btn-primary"'); ?>
							  </td></tr></table>
						  </fieldset>
						<?php echo form_close(); ?>

					</div>
				</div>
 
 
 </div>
			
			
		
			
            
            
             
	

 
 
 
 
 
 
 
 

		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
	

</body>
</html>
