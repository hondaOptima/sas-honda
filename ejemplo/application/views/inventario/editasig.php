<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="inv"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/inventario/?ubicacion=Piso&ubicacion3=Asignados&sucursal=0">Inventario</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                  
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    <div class="row-fluid sortable">
                
                
              
              
                
                
                <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						 <div class="box_a_heading">
                                            <h3>	Editar auto apartado</h3>
                                         
                                        </div>
					</div>
					<div class="box-content">
                    
                    
<br><br>
                    
                    <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
<?php echo form_open('inventario/updateasigedit/'.$idasp.'','class="form-horizontal"'); ?>
						  <fieldset>


           
	

 <?php if($uno_asp): ?>
                          <?php foreach($uno_asp as $todo): ?>
                           <?php
						    $idasp=$todo->das_IDdetalle_asp; 
						    $ano=$todo->das_ano; 
							$mod=$todo->das_modelo;
							$col=$todo->das_color; 
						    $ficha=$todo->fichatecnica_fte_IDfichatecnica; 
							
							
						   ?>
						   <?php endforeach ?><?php else: ?>None<?php endif ?>
       
       
       
       
       
       
          <div class="control-group">
								<label class="control-label" for="focusedInput">Año</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'ano',
              'id'          => 'ano',
              'value'       => $ano,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


   <div class="control-group">
								<label class="control-label" for="focusedInput">Modelo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'mod',
              'id'          => 'mod',
              'value'       => $mod,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Color</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'color',
              'id'          => 'color',
              'value'       => $col,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   

 
                              <div class="control-group">
								<label class="control-label" for="selectErroraa">Ficha</label>
								<div class="controls">
                                
                                <?php
                               
								 echo form_dropdown('ficha', $todo_ficha,$ficha,'style="width:246px;" id="selectErroraa" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>                        
   
<table class="table table-striped"><tr><td>
							 <?php echo form_submit('submit', 'Actualizar Información','class="btn btn-primary"'); ?>
							  </td></tr></table>
							</div>                       
                                                          
	
		
	
     </fieldset>
						<?php echo form_close(); ?>
	

	
          
							
						
					

					</div>
				</div><!--/span-->
			
		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/span-->
            	</div>
                                    
                                    
                                    
                                    
                                    
                                     
                                    
                                    
                                    
                                    
                                      
                                      
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 
  <?php $this->load->view('globales/js'); ?> 
  
	             </body>
</html>