<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Inventario"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
					<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "inventario"; $this->load->view('commons/menu-left',$data); ?>
			<!-- end: Main Menu -->
			
			
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a href="#">Inventario</a>
					</li>
				</ul>
				
			</div>
		
			
			
			
			<hr>
      
      <?php  $this->load->view('commons/control-inventario'); ?>
			
		
           
           
            
            
 
 
<?php 

echo $flash_message ?>


		
			
            
            
            	<div class="row-fluid sortable">
                
                
                <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-tasks"></i><span class="break"></span>Busqueda.</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							
						</div>
					</div>
					<div class="box-content">
            
            
          <form  name="form1" method="get" action="<?php echo base_url(); ?>index.php/inventario/form/">
						  <fieldset>
             <div class="control-group">
             <label class="control-label">Ubicacion</label>
		
								<div class="controls">
								  <label class="inline" style="display: inline-block;padding-top: 5px;
    vertical-align: middle;">
									<input type="checkbox" id="inlineCheckbox1" name="ubicacion"  value="Piso">Piso
								  </label>
								  <label class="checkbox inline">
									<input type="checkbox" id="inlineCheckbox2" name="ubicacion2"  value="Embarque"> Embarque
								  </label>
								  <label class="checkbox inline">
									<input type="checkbox" id="inlineCheckbox3" name="ubicacion3" value="Asignados"> Asignados
								  </label>
                                  
                               
							
								</div>
                                 </div>
                                     <div class="control-group">
								<label class="control-label" for="selectError">Sucursal</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'Tijuana'  => 'Tijuana',
                                 'Mexicali'    => 'Mexicali',
								 'Ensenada'    => 'Ensenada',
								 );
								 echo form_dropdown('sucursal', $options, '','style="width:286px;" id="selectError" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                                  <div class="control-group">
								<label class="control-label" for="selectErroraact">Modelo</label>
								<div class="controls">
                               
            <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'Nuevo'  => 'Nuevo',
								 'Seminuevo'  => 'Seminuevo',);
								 echo form_dropdown('tunidad', $options, '','style="width:286px;" id="selectErroraact" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                                
							 
                              
                              
                               <div class="form-actions">
								<button type="submit" class="btn btn-primary" style="margin-left:200px;">Buscar</button>
								
							  </div>
							</fieldset>
						</form>
                              
                              
                              
                              </div>
				</div><!--/span-->
			
		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/span-->
            	</div><!--/fluid-row-->
               
				<!--/span-->
			
   
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
                
                	</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	

		

</body>
</html>
