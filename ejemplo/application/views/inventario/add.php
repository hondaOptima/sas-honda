<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="inv"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/inventario/?ubicacion=Piso&ubicacion3=Asignados&sucursal=0">Inventario</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                  
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    <div class="row-fluid sortable">
                
                
              
              
                
                
                <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						 <div class="box_a_heading">
                                            <h3>Agregar inventario futuro</h3>
                                         
                                        </div>
					</div>
					<div class="box-content">
                    
                    

                    
                    <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>

	<?php echo form_open('inventario/addasignacion/','class="form-horizontal"'); ?>
						  <fieldset>

<table><tr>
<td>Sucursal:</td>
<td> <div class="control-group">
          
		
								<div class="controls">
								  <label class="inline" style="display: inline-block;padding-top: 5px;
    vertical-align: middle;">
									<input type="radio" name="grupo1" id="inlineCheckbox1" value="Tijuana">Tijuana
								  </label>
								  <label class="checkbox inline">
									<input type="radio" name="grupo1"  id="inlineCheckbox2" value="Mexicali">Mexicali
								  </label>
								  <label class="checkbox inline">
									<input type="radio" name="grupo1"  id="inlineCheckbox3" value="Ensenada">Ensenada
								  </label>
                                  
                               
							
								</div>
                                 </div></td>
                              
</tr><tr>
<td>Mes:</td>
<td>
 <div class="control-group">
								
								<div class="controls">
                                
                                <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'Enero'  => 'Enero',
                                 'Febrero'    => 'Febrero',
								 'Marzo'    => 'Marzo',
								 'Abril'    => 'Abril',
								 'Mayo'    => 'Mayo',
								 'Junio'    => 'junio',
								 'Julio'    => 'Julio',
								 'Agosto'    => 'Agosto',
								 'Septiembre'    => 'Septiembre',
								 'Octubre'    => 'Octubre',
								 'Noviembre'    => 'Noviembre','Diciembre'    => 'Diciembre',
								 );
								 echo form_dropdown('mes', $options, '','style="width:286px;" id="selectErrorxx" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>


</td>                              
</tr></table>
                    
    <style>
    .addxx{
		font-family:Arial, Helvetica, sans-serif; color:#000; 
		cursor: pointer;
    text-decoration: underline; font-size:.9em;
		}
		
		 .addxx:hover{ font-style:italic;
		}
		
		
		.remove{ margin-left:8px;font-size:.9em;
		font-family:Arial, Helvetica, sans-serif; color:#000; 
		cursor: pointer;
    text-decoration: underline;
		}
		
		.remove:hover{ text-decoration:line-through;
		}
    
    </style>                
	
       <table><tr><td>
       <div id="add" class="addxx" >Agregar Linea</div>
       </td>
       <td>
       <div  class="removelinea" >Eliminar Linea</div>	
       </td>
       </tr></table>				
                          
                       <table id="tabins" style="border:1px solid #CCC; width:525px" >
<thead  >
<tr style="border:1px solid #CCC">
<th ></th>
<th >A&ntilde;o</th>
<th >Modelo</th>
<th >Color</th>
<th >Vco</th>

</tr>
</thead>
<tbody >
<tr class="odd">

</tr>
</tbody>
</table>

<input type="hidden" value="1" name="suma">
                  
               
   
<table class="table table-striped"><tr><td>
							 <?php echo form_submit('submit', 'Guardar Información','class="btn btn-primary"'); ?>
							  </td></tr></table>
							</div>                       
                                                          
	
		
	
     </fieldset>
						<?php echo form_close(); ?>
	
          
							
						
					

					</div>
				</div><!--/span-->
			
		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/span-->
            	</div>
                                    
                                    
                                    
                                    
                                    
                                     
                                    
                                    
                                    
                                    
                                      
                                      
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 
  <?php $this->load->view('globales/js'); ?> 
  
  	<script>



	function remove()
{


$(":checkbox:checked").each(
function() {
var ch= $(this).val();

respuesta = confirm("Eliminar Linea? ");
if (respuesta){
$('#'+ch).remove();
var t=0;
}
}
); 



} 
$(document).ready(function(){ 



$('.removelinea').live('click',function(){
	
	remove();	
	
});


$('#chall').live('click',function(){
var chk=$('#chall').attr('checked');
if(chk==true){
// Seleccionar un checkbox
$('input:checkbox').attr('checked', true);
}
else{
// Deseleccionar un checkbox
$('input:checkbox').attr('checked', false);
}
});





		$("#add").click(function() {
var num=$('input[name=suma]').val();
if(num==0){$('input[name=suma]').val('1'); var mass=1; }
if(num>0){var nums=$('input[name=suma]').val(); var mass=parseInt(nums); var sma=(mass + 1);
$('input[name=suma]').val(sma)}
/* Opción 1 */
var n = $('tr:last td', $("#tabins")).length;
var tds = '<tr  style="border:1px solid #CCC" height="30px" id="rowDetalle_'+mass+'" >';
tds += '<td><input type="checkbox" value="rowDetalle_'+mass+'" name="Dependiente[]"></td>';
tds += '<td><input type="text" style="width:32px" name="ano[]"></td>';
tds += '<td><input type="text" name="modelo[]"></td>';
tds += '<td><input type="text" name="color[]"></td>';
tds += '<td><input type="text" name="vco[]"></td>';
tds += '</tr>';
$("#tabins").append(tds);
}); 
}); 
</script>		            
 
    </body>
</html>