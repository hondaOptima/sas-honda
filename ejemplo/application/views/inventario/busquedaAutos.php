<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="inv"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
       <?php $this->load->view('contacto/auto/ModalAuto'); ?>       

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.go(-1)">Auto en busqueda</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                  
                <div class="row-fluid">
                
<div class="span12"><div class="box_a"><div class="row-fluid sortable">
                     <div class="box_a_heading">
                                            <h3>Autos en busqueda Tijuana</h3>
                                         
                                        </div>
                                        <div class="box_a_content">

<?php if($lista_Autos): ?>
   <table id="example3"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr>
                              <?php if($_SESSION['nivel']=='Administrador'){?> 
                                  <th>Encontrado</th>
                                  <?php } ?>
                                  <th>Fecha</th>
                                  <th data-class="expand">Tipo</th>
                                  <th  data-hide="expand">Modelo</th> 
                                  <th data-hide="phone,tablet" >A&ntilde;o</th>
								  <th data-hide="phone,tablet" >Color</th>
								  <th  data-hide="phone,tablet" >Version</th> 
								 <th  data-hide="phone" >Asesor</th>
                                 <th  data-hide="phone" >Contacto</th>
                                 <th data-hide="small" >Status</th>
                                  </tr>
						  </thead>   
						  <tbody>
                          <?php foreach($lista_Autos as $todo): ?>

<?php if($todo->hus_ciudad=='Tijuana'){?>
<tr>
<?php if($_SESSION['nivel']=='Administrador'){?>                           
<td><div id="<?php echo $todo->con_IDcontacto;?>" class="btn btn-small ModalAuto">Ver</div></td>
<?php } ?>
<td><?php echo $todo->bus_fecha;?></td>
<td><?php echo ucwords(strtolower($todo->con_tipo));?></td>
<td><?php echo ucwords(strtolower($todo->con_modelo));?></td>
<td><?php echo $todo->con_ano;?></td>
<td><?php echo ucwords(strtolower($todo->con_color));?></td>
<td><?php echo ucwords(strtolower($todo->con_version));?></td>
<td><?php echo ucwords(strtolower($todo->hus_nombre.' '.$todo->hus_apellido));?></td>
<td><?php echo ucwords(strtolower($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido));?></td>

<td><?php echo ucwords(strtolower($todo->con_status));?></td>
</tr>
<?php } ?>
 <?php endforeach ?><?php else: ?>None<?php endif ?>
                           </tbody>
                           </table>

 </div> </div>
 </div> </div>
 </div>
  <div class="row-fluid">
 
 <div class="span12"><div class="box_a"><div class="row-fluid sortable">
                     <div class="box_a_heading">
                                            <h3>Autos en busqueda Mexicali</h3>
                                         
                                        </div>
                                        <div class="box_a_content">

<?php if($lista_Autos): ?>
   <table id="example4"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr>
                              <?php if($_SESSION['nivel']=='Administrador'){?> 
                                  <th>Encontrado</th>
                                  <?php } ?>
                                  <th>Fecha</th>
                                  <th data-class="expand">Tipo</th>
                                  <th  data-hide="expand">Modelo</th> 
                                  <th data-hide="phone,tablet" >A&ntilde;o</th>
								  <th data-hide="phone,tablet" >Color</th>
								  <th  data-hide="phone,tablet" >Version</th> 
								 <th  data-hide="phone" >Asesor</th>
                                 <th  data-hide="phone" >Contacto</th>
                                 <th data-hide="small" >Status</th>
                                  </tr>
						  </thead>   
						  <tbody>
                          <?php foreach($lista_Autos as $todo): ?>
                         
<?php if($todo->hus_ciudad=='Mexicali'){?><tr>                           
<?php if($_SESSION['nivel']=='Administrador'){?> 
                          
<td><div id="<?php echo $todo->con_IDcontacto;?>" class="btn btn-small ModalAuto">Ver</div></td>
<?php } ?>
<td><?php echo $todo->bus_fecha;?></td>
<td><?php echo ucwords(strtolower($todo->con_tipo));?></td>
<td><?php echo ucwords(strtolower($todo->con_modelo));?></td>
<td><?php echo $todo->con_ano;?></td>
<td><?php echo ucwords(strtolower($todo->con_color));?></td>
<td><?php echo ucwords(strtolower($todo->con_version));?></td>
<td><?php echo ucwords(strtolower($todo->hus_nombre.' '.$todo->hus_apellido));?></td>
<td><?php echo ucwords(strtolower($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido));?></td>
<td><?php echo ucwords(strtolower($todo->con_status));?></td>
</tr><?php } ?>

 <?php endforeach ?><?php else: ?>None<?php endif ?>
                           </tbody>
                           </table>

 </div> </div>
 </div> </div>
 </div>
 
  <div class="row-fluid">
 <div class="span12"><div class="box_a"><div class="row-fluid sortable">
                     <div class="box_a_heading">
                                            <h3>Autos en busqueda Ensenada</h3>
                                         
                                        </div>
                                        <div class="box_a_content">

<?php if($lista_Autos): ?>
   <table id="example5"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr>
                              <?php if($_SESSION['nivel']=='Administrador'){?> 
                                  <th>Encontrado</th>
                                  <?php } ?>
                                  <th>Fecha</th>
                                  <th data-class="expand">Tipo</th>
                                  <th  data-hide="expand">Modelo</th> 
                                  <th data-hide="phone,tablet" >A&ntilde;o</th>
								  <th data-hide="phone,tablet" >Color</th>
								  <th  data-hide="phone,tablet" >Version</th> 
								 <th  data-hide="phone" >Asesor</th>
                                 <th  data-hide="phone" >Contacto</th>
                                 <th data-hide="small" >Status</th>
                                  </tr>
						  </thead>   
						  <tbody>
                          <?php foreach($lista_Autos as $todo): ?>
                         
<?php if($todo->hus_ciudad=='Ensenada'){?>     <tr>                      
<?php if($_SESSION['nivel']=='Administrador'){?>                           
<td><div id="<?php echo $todo->con_IDcontacto;?>" class="btn btn-small ModalAuto">Ver</div></td>
<?php } ?>
<td><?php echo $todo->bus_fecha;?></td>
<td><?php echo ucwords(strtolower($todo->con_tipo));?></td>
<td><?php echo ucwords(strtolower($todo->con_modelo));?></td>
<td><?php echo $todo->con_ano;?></td>
<td><?php echo ucwords(strtolower($todo->con_color));?></td>
<td><?php echo ucwords(strtolower($todo->con_version));?></td>
<td><?php echo ucwords(strtolower($todo->hus_nombre.' '.$todo->hus_apellido));?></td>
<td><?php echo ucwords(strtolower($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido));?></td>
<td><?php echo ucwords(strtolower($todo->con_status));?></td>
</tr><?php } ?>

 <?php endforeach ?><?php else: ?>None<?php endif ?>
                           </tbody>
                           </table>

 </div> </div>
 </div> </div>
 </div>
 
 
 
 
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 
  <script>
  	$(document).ready(function() {
		$('#example3').dataTable( {
		     "iDisplayLength": 50,
					 "bAutoWidth": true,
					 	"bProcessing": true,
						"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example3_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	$('#example4').dataTable( {
		     "iDisplayLength": 50,
					 "bAutoWidth": true,
					 	"bProcessing": true,
						"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example4_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
	$('#example5').dataTable( {
		     "iDisplayLength": 50,
					 "bAutoWidth": true,
					 	"bProcessing": true,
						"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example5_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
	
	
	$('.ModalAuto').live('click',function(){
$('#ModalAuto').modal('show');
var idt=$(this).attr('id');

$.ajax({
   type: 'POST',
   url: '<?php echo base_url().'querys/auto/checkAuto.php'; ?>',
   data: 'idt='+idt,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.informacion').html(data);



   }
});	
});
	
	
	
	$('.checkAuto').live('click',function(){
$('.respuesta').html('Conectando... !');	
var mcCbxCheck = $(this);
var id=$(this).attr('id');
if(mcCbxCheck.is(':checked')) {
$.post("<?php echo base_url(); ?>querys/auto/updateAuto.php",{id:id,valor:'si'}, function(data) { $('.respuesta').html('Auto en busqueda !');
window.location="<?php echo base_url();?>index.php/inventario/busquedaAutos";
});
    }
    else{
$.post("<?php echo base_url(); ?>querys/auto/updateAuto.php",{id:id,valor:'no'}, function(data) { $('.respuesta').html('Busqueda cancelada !');
window.location="<?php echo base_url();?>index.php/inventario/busquedaAutos";

 });
    }
	
});
	 } );
  </script>
  
  
 </body>
</html>