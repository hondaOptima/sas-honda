<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Vendedores"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "inventario"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/inventario">Inventario</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="#">Agregar Nuevo Vendedor</a>
					</li>
                    
				</ul>
				
			</div>
		
			
			
			
			<hr>
			
	
<div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span>Formulario Auto</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                    
                    
                     <?php if($uno_inventario): ?>
                          <?php foreach($uno_inventario as $todo): ?>
                           <?php
						    $ida=$todo->aut_IDauto_vin; 
						    $suc=$todo->aut_sucursal; 
							$nin=$todo->aut_no_inventario;
							$ano=$todo->aut_ano; 
							$vin=$todo->aut_vin;
							$mot=$todo->aut_motor; 
							$tun=$todo->aut_tipo_unidad;
							$ubi=$todo->aut_ubicacion; 
							$imp=$todo->aut_importe;
							$iva=$todo->aut_iva; 
							$lle=$todo->aut_llegada;
							$mod=$todo->modelos_mod_IDmodelos;
							$alm=$todo->almacen_alm_IDalmacen;
							$col=$todo->color_col_IDcolor; 
							$fte=$todo->fichatecnica_fte_IDfichatecnica; 
							
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                    
                    
                    <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
							<?php echo form_open('inventario/updatetodo/'.$ida.'','class="form-horizontal"'); ?>
						  <fieldset>
                          
                          <div class="control-group">
								<label class="control-label" for="selectError">Sucursal</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'Tijuana'  => 'Tijuana',
                                 'Mexicali'    => 'Mexicali',
								 'Ensenada'    => 'Ensenada',
								 );
								 echo form_dropdown('sucursal', $options, $suc,'style="width:246px;" id="selectError" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                              
							<div class="control-group">
								<label class="control-label" for="focusedInput">No.Inventario</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'noinventario',
              'id'          => 'noinventario',
              'value'       => $nin,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                    
                                  
                                   
								  </div>
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="selectErrorau">Ubicacion</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'Enbarque'  => 'Enbarque',
								 'Traslado'  => 'Traslado',
								 'Piso'  => 'Piso',
                                );
								 echo form_dropdown('ubicacion', $options,$ubi,'style="width:246px;" id="selectErrorau" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                              
                              
                             <div class="control-group">
								<label class="control-label" for="selectErrora">Almacen</label>
								<div class="controls">
                                
								<?php if($todo_almacen): ?>
                                <?php                          
								 echo form_dropdown('almacen', $todo_almacen,$alm,'style="width:246px;" id="selectErrora" data-rel="chosen"');
								?>
                                <?php else: ?>None<?php endif ?>
                                
								  
								</div>
							  </div>
                              
                             <div class="control-group">
							  <label class="control-label" for="date01">Llegada</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 
								 list($ano,$mes,$dia)=explode('-',$lle);
								 $fecha=$mes.'/'.$dia.'/'.$ano;	
								 
								 $datai= array(
              'name'        => 'llegada',
              'id'          => 'llegada',
			  'class'          => 'input-xlarge datepicker',
              'value'       => $fecha,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                              
                               <div class="control-group">
								<label class="control-label" for="focusedInput">VIN</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'vin',
              'id'          => 'vin',
              'value'       => $vin,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="focusedInput">Motor</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$datam= array(
              'name'        => 'motor',
              'id'          => 'motor',
              'value'       => $mot,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($datam);?>
                              
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="selectErroraa">Año</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 '2006'  => '2006',
								 '2007'  => '2007',
								 '2008'  => '2008',
								 '2009'  => '2009',
								 '2010'  => '2010',
                                 '2011'    => '2011',
								 '2012'    => '2012',
								 '2013'    => '2013',
								 '2014'    => '2014',
								 '2015'    => '2015',);
								 echo form_dropdown('ano', $options,$ano,'style="width:246px;" id="selectErroraa" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="selectErroraam">Modelo</label>
								<div class="controls">
                               
                               
							    <?php if($todo_modelo): ?>
                                <?php
                                echo form_dropdown('modelo', $todo_modelo,$mod,'style="width:246px;" id="selectErroraam" data-rel="chosen"');
								?>
                                <?php else: ?>None<?php endif ?>
								  
								</div>
							  </div>
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraamf">Ficha Tecnica</label>
								<div class="controls">
                               
                               
							    <?php if($todo_ficha): ?>
                                <?php
                                echo form_dropdown('ftecnica', $todo_ficha,$fte,'style="width:246px;" id="selectErroraamf" data-rel="chosen"');
								?>
                                <?php else: ?>None<?php endif ?>
								  
								</div>
							  </div>
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraac">Color</label>
								<div class="controls">
                               <?php if($todo_color): ?>
                               <?php
                                echo form_dropdown('color', $todo_color,$col,'style="width:246px;" id="selectErroraac" data-rel="chosen"');
								?>
                                 <?php else: ?>None<?php endif ?>
								  
								</div>
							  </div>
                            
                           
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraact">Tipo Unidad</label>
								<div class="controls">
                               
            <?php
                                $options = array(
								''  => 'Seleccione una opcion',
                                 'Nuevo'  => 'Nuevo',
								 'Seminuevo'  => 'Seminuevo',);
								 echo form_dropdown('tunidad', $options,$tun,'style="width:246px;" id="selectErroraact" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                              
                              
                              
                              
                              						
                            
                            <div class="control-group">
								<label class="control-label" for="prependedInput">Importe</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">$</span>
                                    <?php
									
									
									$data = array(
              'name'        => 'importe',
              'id'          => 'importe',
              'value'       => $imp,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                    
                                    
                                    
								  </div>
								 
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="prependedInput">Iva</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">$</span>
                                    <?php
									
									
									$data = array(
              'name'        => 'iva',
              'id'          => 'iva',
              'value'       => $iva,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                      
                                   
                                    
								  </div>
								 
								</div>
							  </div>
                            
                              
                            
                              
                              
                              
                            
                             
                            
                            
                            
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>

                    
                    
                    
                    

					</div>
				</div>
 
 
 </div>
 
 
 
 
 
 
 
 
 

		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
	

</body>
</html>
