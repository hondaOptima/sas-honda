<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="inv"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.go(-1)">Inventario</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                  
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    <div class="row-fluid sortable">
                <?php echo $flash_message ?>  
                     <div class="box_a_heading">
                                            <h3>Autos apartados</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                            <table id="foo_example" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                              <th data-class="expand">Ciudad</th>
                              <th data-class="expand">Almacen</th>
                               <th data-class="expand">Vendedor</th>
								  <th data-hide="phone">Contacto</th>
								  <th data-hide="phone">Vin</th>
                                  <th >Auto</th>
                                  <th data-hide="phone,tablet">Estado</th> 
                                  <th data-hide="phone,tablet">Dias Apartado.</th>
                                  <th data-hide="phone,tablet">Primer Pago</th>
								 <?php if($_SESSION['nivel']=='Administrador'){ ?><th>Acciones</th><?php } ?>
                                 
                                
							  </tr>
						  </thead>   
						  <tbody>

<?php foreach($todo_apartados as $todo): ?>
                          
                          
							
							<tr>
								<td class="center"><?php echo $todo->hus_ciudad ?></td>
                                <td><?php if(empty($todo->aau_alamacen)){}else{
									list($tn,$cd)=explode(" ",$todo->aau_alamacen);$str=substr($tn,0,1); 
if($cd=='')
{
list($tn,$cd,$ty)=explode(" ",$todo->aau_alamacen);echo $str.'-'.$ty; 	
}
else{echo $str.'-'.$cd;}
									
									}?></td>
                                <td class="center"><?php echo ucwords(strtolower($todo->hus_nombre.' '.$todo->hus_apellido)); ?></td>
								
                                <td class="center"><?php echo ucwords(strtolower($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido)); ?></td>
                                <td class="center"><?php echo $todo->aau_IdFk;?></td>
<td class="center"><?php echo ucwords(strtolower($todo->aau_modelo.' '.$todo->aau_ano.' '.$todo->aau_color_exterior)); ?></td>
                              
                                <td class="center"><?php
								$INFO=$this->Inventariomodel->statusapartado($todo->aau_IdFk);
								if(empty($INFO)){echo'Apartado';}else{ 
if($INFO->prcv_director_gral=='aprobado'){echo 'Facturado';}
elseif($INFO->prcv_prcv_status_credito_contado=='aprobado'){echo 'Facturado';}	
elseif($INFO->prcv_status_gerente=='aprobado'){echo 'Facturado';}
elseif($INFO->prcv_status_fi=='aprobado'){echo 'Facturado';}	
								} ?></td>
                                <td class="center">
                                
                          <?php $fecha1=$todo->aau_fecha;?>
                          <?php
//defino fecha 1
list($ano1,$mes1,$dia1)=explode('-',$fecha1);


//defino fecha 2
$ano2 = date('Y');
$mes2 = date('m');
$dia2 = date('d');

//calculo timestam de las dos fechas
$timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
$timestamp2 = mktime(4,12,0,$mes2,$dia2,$ano2);

//resto a una fecha la otra
$segundos_diferencia = $timestamp1 - $timestamp2;
//echo $segundos_diferencia;

//convierto segundos en días
$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

//obtengo el valor absoulto de los días (quito el posible signo negativo)
$dias_diferencia = abs($dias_diferencia);

//quito los decimales a los días de diferencia
$dias_diferencia = floor($dias_diferencia);
if($dias_diferencia=='0'){$dias_diferencia=1;}
if($dias_diferencia<=9){$dias_diferencia='0'.$dias_diferencia;}
echo $dias_diferencia;
?> 
                          
                          
                          
                          
                            
                                </td>
                                
                                <td class="center"><?php echo $todo->aau_pago ?></td>
                                <?php if($_SESSION['nivel']=='Administrador'){ ?>
								<td class="center">
                                
                                <?php 
								// 4 echo $todo->aau_IDasignacion_auto;
								$IDA=$this->Inventariomodel->getActividadDelete($todo->aau_IDasignacion_auto);
								//echo $IDA->IDA;
								?>
                              
    
                                    
									<?php 
									
									
									
									if($todo->aau_asig_intell=='intel'){
										
if($todo->contacto_con_IDcontacto==12219 || $todo->contacto_con_IDcontacto==12220 || $todo->contacto_con_IDcontacto==12223 || $todo->contacto_con_IDcontacto==13760 || $todo->contacto_con_IDcontacto==13805 || $todo->contacto_con_IDcontacto==13806){$IDAA=0;}else{$IDAA=$IDA->IDA;}
										echo anchor("inventario/deleteasigintel/$todo->aau_IDasignacion_auto/$IDAA/$todo->contacto_con_IDcontacto", '<i class="icon-trash icon-red"></i>',array('onClick' => "return confirm('Esta seguro que desea eliminar esta asignacion ?')",'class'=>"btn btn-mini"));
										
										
										
										
										
										}
									else{
									echo anchor("inventario/deleteasig/$todo->aau_IDasignacion_auto/$IDAA/$todo->aau_IdFk/$todo->contacto_con_IDcontacto", '<i class="icon-trash icon-red"></i>',array('onClick' => "return confirm('Esta seguro que desea eliminar esta asignacion ?')",'class'=>"btn btn-mini"));
									}
									echo anchor("inventario/apartarcsvedit/$todo->aau_IdFk/csv/$todo->aau_IDasignacion_auto/$todo->huser_hus_IDhuser", '<i class="icon-random icon-red"></i>',array('onClick' => "return confirm('Esta seguro que desea transferir este auto?')",'class'=>"btn btn-mini"));
										
									
									 ?> 
                              
                                  
										
									
								</td><?php } ?>
								
							</tr>
							
						
                            
                            <?php endforeach ?>
</tbody>

</table>

 </div>
 
 
                       </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 
  <?php $this->load->view('globales/js'); ?> 
  
    <script type='text/javascript'>

	$(document).ready(function() {
	$('#foo_example').dataTable( {
		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
    
	
	
} );
</script>


    </body>
</html>