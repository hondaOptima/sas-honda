<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="inv"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/inventario/?ubicacion=Piso&ubicacion3=Asignados&sucursal=0">Inventario</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                  
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    <div class="row-fluid sortable">
                
                
              
              
                 <?php echo $flash_message ?>  
                
                <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						 <div class="box_a_heading">
                                            <h3>Datos Seminuevos</h3>
                                         
                                        </div>
					</div>
					<div class="box-content">
                    <br><br>
               <?php if($uno_data): ?>
                          <?php foreach($uno_data as $todo): ?>
                           <?php
                         
                         $vin=$todo->sem_vin;
						 $km=$todo->sem_km;
						 $pre=$todo->sem_precio;
						 $gar=$todo->sem_garantia;
						 
							
							
						   ?>
						   <?php endforeach ?><?php else: ?><?php endif ?>                   

                    
                    <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>

	<?php echo form_open('inventario/adddata/'.$id.'/','class="form-horizontal"'); ?>
						  <fieldset>


           
	      
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Vin</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'vin',
              'id'          => 'vin',
              'value'       => ''.$id.'',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Precio</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on">$</span>
                                    <?php
									
									if(empty($pre)){$pre='';}
									$data = array(
              'name'        => 'precio',
              'id'          => 'precio',
              'value'       => ''.$pre.'',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>      
       
       
       
          <div class="control-group">
								<label class="control-label" for="focusedInput">Garantía</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									if(empty($gar)){$gar='';}
									
									$data = array(
              'name'        => 'garantia',
              'id'          => 'garantia',
              'value'       => $gar,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			 
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   
                              
                              
                               <div class="control-group">
								<label class="control-label" for="focusedInput">Km</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									if(empty($km)){$km='';}
									
									$data = array(
              'name'        => 'km',
              'id'          => 'km',
              'value'       => $km,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			 
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   

   
<table class="table table-striped"><tr><td>
							 <?php echo form_submit('submit', 'Guardar Información','class="btn btn-primary"'); ?>
							  </td></tr></table>
							</div>                       
                                                          
	
		
	
     </fieldset>
						<?php echo form_close(); ?>
	
          
							
						
					

					</div>
				</div><!--/span-->
			
		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/span-->
            	</div>
                                    
                                    
                                    
                                    
            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Imagenes</h3>
                                            <ul class="gallery_layout inline pull-right">
<li  title="Upload"><div class="btn btn-small btn-success btn-settingUpload" style="margin-top:-7px;">Upload Image</div></li>
<li class="gallery_grid active_layout" title="Grid"><i class="elusive-icon-th-large"></i></li>
<li class="gallery_list" title="List"><i class="elusive-icon-align-justify"></i></li>
                                            </ul>
                                        </div>
                                        <div class="box_a_content">
<div id="gallery" >
<ul>
<?php
if($lista_img){
	
?>
<?php foreach($lista_img as $todo): ?>
<li class="mix cat_all category_b" data-timestamp="1370210400" data-name="image_001.jpg">
<a class="holder img-polaroid" data-img-src="<?php echo base_url();?>upload/seminuevos/<?php echo $todo->fse_foto;?>" href="<?php echo base_url();?>upload/seminuevos/<?php echo $todo->fse_foto;?>" title="image_001.jpg (03-06-2013)">
<img src="<?php echo base_url();?>upload/seminuevos/<?php echo $todo->fse_foto;?>" alt="">
</a>
<div class="img_more_details">
<p class="img_title"><?php echo $todo->fse_foto;?></p>
<p class="img_other_info"><span class="muted">Fecha:</span><?php echo $todo->fse_date;?></p>
</div>
<span class="img_actions">
<a href="<?php echo base_url();?>index.php/inventario/removeimg/<?php echo $id;?>/<?php echo $todo->fse_IDfotos_seminuevos;?>">
<i class="elusive-icon-remove" ></i>
</a>
</span>
</li>

 <?php endforeach ?>
<?php } ?>
</ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                          
                                     
                                    
                                    
                                    
                                    
                                      
                                      
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <?php 
$datax['idc']=$id;
$this->load->view('inventario/formUploadfile',$datax); 
?>  
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 
  <?php $this->load->view('globales/js'); ?> 
	         
             
<script>
$(document).ready(function()
{
$('#FileUploader').on('submit', function(e)
    {
		
        e.preventDefault();
        $('#uploadButton').attr('disabled', ''); // disable upload button
        //show uploading message
        
        $("#output").html('<div style="padding:10px"><img src="<?php echo base_url();?>img/ajax-loader.gif" alt="Please Wait"/> <span>Uploading...</span></div>');
		
        $(this).ajaxSubmit({target: '#output', success:  afterSuccess});
        
        	 
        
    });
	
	});
	
	
	function afterSuccess()
{
    $('#FileUploader').resetForm();  // reset form
    $('#uploadButton').removeAttr('disabled'); //enable submit button
  
}


</script>             
              
  
 
    </body>
</html>