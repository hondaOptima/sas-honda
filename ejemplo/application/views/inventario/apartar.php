<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="inv"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/inventario/?ubicacion=Piso&ubicacion3=Asignados&sucursal=0">Inventario</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                  
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    <div class="row-fluid sortable">
                
                
              
              
                
                
                <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						 <div class="box_a_heading">
                                            <h3>Agregar inventario futuro</h3>
                                         
                                        </div>
					</div>
					<div class="box-content">
                    
                    <br><br>

                    

 <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>

	<?php echo form_open('inventario/apartar/'.$idasp.'/'.$ty.'','class="form-horizontal"'); ?>
						  <fieldset>


           
	

 <?php if($uno_asp): ?>
                          <?php foreach($uno_asp as $todo): ?>
                           <?php
						    $idasp=$todo->das_IDdetalle_asp; 
						    $ano=$todo->das_ano; 
							$mod=$todo->das_modelo;
							$col=$todo->das_color; 
						    $ficha=$todo->fte_nombre; 
							
							
						   ?>
						   <?php endforeach ?><?php else: ?>None<?php endif ?>
       
       
    <div class="control-group">
								<label class="control-label" for="selectErroraad">Vendedor</label>
								<div class="controls">
                                
                               <?php
                               
								 echo form_dropdown('vendedor', $todo_vendedor,$asesor,'style="width:246px;" id="s2_single" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>    
                              
                              
                              
                                 
                               <div class="control-group">
								<label class="control-label" for="selectErroraav">Contacto</label>
								<div class="controls">
								
								<div class="listaprospecto">	
								<?php
                               
								 echo form_dropdown('contacto', $lista_contactos,'','style="width:246px;" id="s23_single" data-rel="chosen"');
								?>
								  
								</div>
							  </div>  
                              <br>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">No. Recibo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'recibo',
              'id'          => 'recibo',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Cantidad</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on">$</span>
                                    <?php
									
									
									$data = array(
              'name'        => 'cantidad',
              'id'          => 'cantidad',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>      
       
       
       
          <div class="control-group">
								<label class="control-label" for="focusedInput">Año</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'ano',
              'id'          => 'ano',
              'value'       => $ano,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


   <div class="control-group">
								<label class="control-label" for="focusedInput">Modelo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'mod',
              'id'          => 'mod',
              'value'       => $mod,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Color</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'color',
              'id'          => 'color',
              'value'       => $col,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


   <div class="control-group">
								<label class="control-label" for="focusedInput">Ficha Tecnica</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'ficha',
              'id'          => 'ficha',
              'value'       => $ficha,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			   'readonly'=>'readonly'
            );
									  echo form_input($data);?>
                                   
                                    
								  </div>
								</div>
							  </div>   


 
                                          
   
<table class="table table-striped"><tr><td>
							 <?php echo form_submit('submit', 'Guardar Información','class="btn btn-primary"'); ?>
							  </td></tr></table>
							</div>                       
                                                          
	
		
	
     </fieldset>
						<?php echo form_close(); ?>
	
          
							
						
					

					</div>
				</div><!--/span-->
			
		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/span-->
            	</div>
                                    
                                    
                                    
                                    
                                    
                                     
                                    
                                    
                                    
                                    
                                      
                                      
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 
  <?php $this->load->view('globales/js'); ?> 
  
  
  	<script>
		
$(document).ready(function(){ 


$('#s2_single').live('click',function(){	
	
var idv=$('select[name=vendedor]').val();

document.location.href='<?php echo base_url();?>index.php/inventario/apartar/<?php echo $vin;?>/<?php echo $ty;?>?asesor='+idv+'';

//$.post("< echo base_url(); ?>index.php/inventario/listaprospectos/",{idv:idv}, function(datax) {$('.listaprospecto').html(datax);});

});




$('#chall').live('click',function(){
var chk=$('#chall').attr('checked');
if(chk==true){
// Seleccionar un checkbox
$('input:checkbox').attr('checked', true);
}
else{
// Deseleccionar un checkbox
$('input:checkbox').attr('checked', false);
}
});





		$("#add").click(function() {
var num=$('input[name=suma]').val();
if(num==0){$('input[name=suma]').val('1'); var mass=1; }
if(num>0){var nums=$('input[name=suma]').val(); var mass=parseInt(nums); var sma=(mass + 1);
$('input[name=suma]').val(sma)}
/* Opción 1 */
var n = $('tr:last td', $("#tabins")).length;
var tds = '<tr  style="border:1px solid #CCC" height="30px" id="rowDetalle_'+mass+'" >';
tds += '<td><input type="checkbox" value="rowDetalle_'+mass+'" name="Dependiente[]"></td>';
tds += '<td><input type="text" style="width:32px" name="ano[]"></td>';
tds += '<td><input type="text" name="modelo[]"></td>';
tds += '<td><input type="text" name="color[]"></td>';
tds += '<td><select name="fichatecnica[]"><option value="">No definida</option><option  value="1">accord-crosstour-2011</option><option  value="2">civic-coupe-2012</option><option  value="3">civic-hybrid-2012</option><option  value="4">civic-sedan-2012</option><option  value="5">civic-si-2012</option><option  value="6">cr-v-2012</option><option  value="7">ridgeline-2012</option><option  value="8"> 	accord-coupe-2013</option><option  value="9">accord-crosstour-2013</option><option  value="10">accord-sedan-2013</option><option  value="11">civic-coupe-2013</option><option  value="12">civic-hybrid-2013</option><option  value="13">civic-sedan-2013</option><option  value="14">civic-si-2013</option><option  value="15">cr-v-2013</option><option  value="16">honda-city-2013</option><option  value="17"> 	honda-fit-2013</option><option  value="18"></option><option  value="1"></option><option  value="1"></option></select></td>';
tds += '</tr>';
$("#tabins").append(tds);
}); 
}); 


function remove()
{
$(":checkbox:checked").each(
function() {
var ch= $(this).val();

respuesta = confirm("Eliminar Linea? ");
if (respuesta){
$('#'+ch).remove();
var t=0;
}
}
);
} 
		</script>		
  	            
 
    </body>
</html>