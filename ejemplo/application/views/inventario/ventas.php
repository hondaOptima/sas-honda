<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="ventas"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Ventas</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Ventas</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                            <table id="dt_table_tools" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                               <th>Almacen</th>
                             <th >Factura</th>  
                                  <th >Fecha de Factura</th>
								  <th >Modelo</th>
								  <th>Color</th>
								  <th>Vin</th>
								  <th>Asesor</th> 
								 <th>Total</th>
                                  <th>Costo</th>
                                   <th>Utilidad</th>
                                    <th>Acciones</th>
                                 
                                
							  </tr>
						  </thead>   
						  <tbody>
 <?php if($todo_ventas): ?>
												<?php foreach($todo_ventas as $todo): ?>
                                                    <tr>
                                                        <td><?php echo $todo->dat_fecha_facturacion;?></td>
                                                        <td><?php echo $todo->aau_alamacen;?></td>
                                                        <td><?php echo $todo->dats_factura;?></td>
                                                        <td><?php echo $todo->aau_modelo;?></td>
                                                        <td><?php echo $todo->aau_color_exterior;?></td>
                                                        <td><?php echo $todo->aau_IdFk;?></td>
                                                        <td><?php echo $todo->hus_nombre.' '.$todo->hus_apellido;?></td>
                                                         <td><?php echo $todo->dats_valorfactura;?></td>
                                                         <td><?php echo $todo->dats_valorfactura;?></td>
                                                         <td><?php echo $todo->dats_valorfactura;?></td>
                                                        <td><div class="btn btn-small">
                                                        <a href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>">
                                                        Bitacora
                                                        </a>
                                                        </div></td>
                                                    </tr>
                                                <?php endforeach ?><?php else: ?>No se encontraron registros.<?php endif ?>	

</tbody>

</table>

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>      

  <?php $this->load->view('globales/jsC'); ?> 
  <?php }else{ ?>
  <?php $this->load->view('globales/js'); ?> 
  <?php } ?>
    </body>
</html>