<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="eve"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Eventos</a></li>
                                     <li style="float:right">
                                     <?php if($_SESSION['nivel']=='Administrador'){ ?>
<a href="<?php echo base_url().'index.php/eventos/add';?>" class="btn btn-small btn-success">Crear nuevo evento</a>
<?php } ?>
                                     </li>                          
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                    
                                       <?php echo $flash_message ?>
</div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Lista de Eventos</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                            <table  class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                                <th>Foto</th>
								  <th>Titulo</th>
								  <th>Descripci&oacute;n</th>
								  <th>Fecha Inicio</th>
                                  <th>Fecha Fin</th>
                                  <th>Estado</th>
                                  
                                   <?php if($_SESSION['nivel']=='Administrador'){ ?>
                                   <th>Acciones</th>
								        <?php } ?>      
							  </tr>
						  </thead>   
						  <tbody>
<?php if($todo_eventos): ?><?php foreach($todo_eventos as $fidb): ?>
<tr>

<td class="center">
  <a href="<?php echo base_url(); ?>index.php/eventos/vistaevento/<?php echo $fidb->eve_IDeventos ?>">
  <?php 
  list($nm,$xt)=explode(".",$fidb->eve_foto);
  if($xt=='pdf'){$foto='evento.jpg';}else{$foto=$fidb->eve_foto;}
  ?>
<img width="100" height="100" src="<?php echo base_url()?>upload/eventos/<?php echo $foto ;?>">
</a>
</td>
<td class="center"><?php echo $fidb->eve_titulo;?></td>
<td class="center"><?php echo $fidb->eve_descripcion;?></td>
<td class="center"><?php echo $fidb->eve_fecha_inicio;?></td>
<td class="center"><?php echo $fidb->eve_fecha_fin;?></td>
<td class="center"><?php echo $fidb->eve_status;?></td>

 <?php if($_SESSION['nivel']=='Administrador'){ ?>
<td class="center">
 <div class="dropdown dropdown-right">
<a href="#" class="btn btn-mini" data-toggle="dropdown">Acciones</a>
<ul class="dropdown-menu">
<li>
<?php echo anchor("eventos/update/$fidb->eve_IDeventos", 'Editar'); ?> 
</li><li>
<?php echo anchor("eventos/delete/$fidb->eve_IDeventos", 'Eliminar',array('onClick' => "return confirm('Desea eliminar :$fidb->eve_titulo ?')")); ?> 
</li>
</ul></div>
</td><?php } ?>



</tr>
      
  <?php endforeach ?><?php else: ?><?php endif ?>	
</tbody>

</table>

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 <?php $this->load->view('globales/js'); ?> 


 

    </body>
</html>