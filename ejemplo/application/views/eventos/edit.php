<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="eve"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/eventos/">Eventos</a></li>
                                                                       
                                </ul>
                          </section>



                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
  <?php if($uno_eventos): ?><?php foreach($uno_eventos as $fidb): ?>
    
    <?php
	$ide=$fidb->eve_IDeventos;
    $tit=$fidb->eve_titulo;
	$ini=$fidb->eve_fecha_inicio;
	$fin=$fidb->eve_fecha_fin;
	$fot=$fidb->eve_foto;
	$des=$fidb->eve_descripcion;
	$sta=$fidb->eve_status;
	
	?>
    
    <?php endforeach ?><?php else: ?><?php endif ?>	                                   
                                    
                                    
      <script type='text/javascript'>

	$(document).ready(function() {
	$('.filename').html('<?php echo $fot;?>');
	$('.action').html('Change File');
	
	});
</script>                               
                                    
                                        <div class="box_a_heading">
                                            <h3>Crear nuevo evento</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        
  <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
							<?php echo form_open_multipart('eventos/update/'.$ide.'','class="form-horizontal"'); ?>
						  <fieldset>
    <br><br>                      
                           <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                               <?php
									
									
									$data = array(
              'name'        => 'titulo',
              'id'          => 'titulo',
              'value'       => $tit,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                
								  </div>
								</div>
							  </div>
                              
						
                              
                              
                        
                              
                              <div class="control-group">
							  <label class="control-label" for="date01">Fecha de Inicio</label>
							  <div class="controls">
                              
                              
                                 <?php 
								  list($ano,$mes,$dia)=explode('-',$ini);
								 $fecha=$mes.'/'.$dia.'/'.$ano;	
								 
								 $datai= array(
              'name'        => 'ini',
              'id'          => 'ini',
			  'class'          => 'input-xlarge datepicker',
              'value'       => $fecha,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                            
                            
                            
                             <div class="control-group">
							  <label class="control-label" for="date01">Fecha Fin</label>
							  <div class="controls">
                              
                              
                                 <?php 
								  list($anob,$mesb,$diab)=explode('-',$fin);
								 $fechab=$mesb.'/'.$diab.'/'.$anob;	
								 
								 $datai= array(
              'name'        => 'fin',
              'id'          => 'fin',
			  'class'          => 'input-xlarge datepicker',
              'value'       => $fechab,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                            
                            
                       <div class="control-group">
								<label class="control-label" for="focusedInput">Foto</label>
								<div class="controls">
								<div class="input-prepend">
							
                                      <input type="file" name="userfile" value="<?php echo $fot;?>" id="userfile" />  
                                
								  </div>
								</div>
							  </div>       
                            
                               
         <div class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripcion:</label>
							  <div class="controls">
								
                                 <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => 'cleditor',
              'value'       => $des,
              'maxlength'   => '',
              'row'        => '',
              'style'       => '',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
                            
                            
                                  <div class="control-group">
								<label class="control-label" for="selectErrorw">Estado</label>
								<div class="controls">
                                 <?php
                                $options = array(
                                 'activa'  => 'Activa',
                                 'inactiva'    => 'Inactiva',);
								 echo form_dropdown('status', $options, $sta,'style="width:246px;" id="selectErrorxw" data-rel="chosen"');
								?>
                                
								
								</div>
							  </div>
                            
                            
                       
                             
                            
                            
                            
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Guardar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>                      
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 <?php $this->load->view('globales/js'); ?> 


 

    </body>
</html>