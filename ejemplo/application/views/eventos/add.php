<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="eve"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Eventos</a></li>
                                                                       
                                </ul>
                          </section>



                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Crear nuevo evento</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
   <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
							<?php echo form_open_multipart('eventos/add','class="form-horizontal"'); ?>
						  <fieldset>
                          <br><br>
                           <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('titulo', set_value('titulo'), 'id="titulo" size="30"');?>
                                
								  </div>
								</div>
							  </div>
                              
						
                              
                              
                        
                              
                              <div class="control-group">
							  <label class="control-label" for="date01">Fecha de Inicio</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 
								 $datai= array(
              'name'        => 'ini',
              'id'          => 'ini',
			  'class'          => 'input-xlarge bs_datepicker',
              'value'       => ''.date('m').'/'.date('d').'/'.date('Y').'',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                            
                            
                            
                             <div class="control-group">
							  <label class="control-label" for="date01">Fecha Fin</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 
								 $datai= array(
              'name'        => 'fin',
              'id'          => 'fin',
			  'class'          => 'input-xlarge bs_datepicker',
              'value'       => ''.date('m').'/'.date('d').'/'.date('Y').'',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                            
                            
                       <div class="control-group">
								<label class="control-label" for="focusedInput">Archivo PDF</label>
								<div class="controls">
								<div class="input-prepend">
							
                                      <input type="file" name="file_1"   /> 
                                       <input type="file" name="file_2"   /> 
                                      
                                
								  </div>
								</div>
							  </div>  
                              
                               
                            
                               
         <div class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripcion:</label>
							  <div class="controls">
								
                                 <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => 'cleditor',
              'value'       => '',
              'maxlength'   => '',
              'row'        => '',
              'style'       => '',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
                            
                            
                                  <div class="control-group">
								<label class="control-label" for="selectErrorw">Estado</label>
								<div class="controls">
                                 <?php
                                $options = array(
                                 'activa'  => 'Activa',
                                 'inactiva'    => 'Inactiva',);
								 echo form_dropdown('status', $options, 'activa','style="width:246px;" id="selectErrorxw" data-rel="chosen"');
								?>
                                
								
								</div>
							  </div>
                            
                            
                       
                             
                            
                            
                            
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Guardar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>                                         
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 <?php $this->load->view('globales/js'); ?> 


 

    </body>
</html>