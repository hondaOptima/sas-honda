<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="eve"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url().'index.php/eventos/';?>">Eventos</a></li>
                                                                       
                                </ul>
                          </section>

                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                               <?php if($uno_eventos): ?><?php foreach($uno_eventos as $fidb): ?>
    
    <?php
	$ide=$fidb->eve_IDeventos;
    $tit=$fidb->eve_titulo;
	$ini=$fidb->eve_fecha_inicio;
	$fin=$fidb->eve_fecha_fin;
	$fot=$fidb->eve_foto;
	$des=$fidb->eve_descripcion;
	$sta=$fidb->eve_status;
	
	?>
    
    <?php endforeach ?><?php else: ?><?php endif ?>	       
                                    
                                        <div class="box_a_heading">
                                            <h3>Evento:<?php echo $tit;?></h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                             <table><tr><td>
                  <img class="grayscale"  alt="" src="<?php echo base_url(); ?>upload/eventos/<?php echo $fot;?>">
                  
                  </td></tr></table>
                  
                  
                   <table><tr><td>
                  <tr>
                  <td width="150px"  class="ffa" >Titulo:</td>
                  <td width="150px" class="ff" ><?php echo $tit;?></td>
                  <td width="150px" class="ffa" >Estado:</td>
                  <td width="150px" class="ff" ><?php echo $sta;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px" class="ffa" >Fecha Inicio:</td>
                  <td width="150px"class="ff" ><?php echo $ini;?></td>
                  <td width="150px" class="ffa" >Fecha Fin:</td>
                  <td width="150px" class="ff" ><?php echo $fin;?></td>
                  </tr>
                  
                   </table>
                   
                    <table>
                  
                  <tr>
                  <td width="150px" class="ffa" >Descripcion:</td></tr>
                  <tr><td width="150px"class="ff" ><?php echo $des;?></td>
                  </tr>
                  
                   </table>

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 <?php $this->load->view('globales/js'); ?> 


 

    </body>
</html>