<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Iniciar Sesion</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
        <link rel="icon" type="image/ico" href="favicon.ico">
                  <?php $this->load->view('globales/estilosLogin'); ?>   
      </head>
    <body>
        <div class="login_box">
        <!-- sign in -->   
        <?php echo $flash_message ?>
       <?php echo form_open('login/recuperar',''); ?>
						
					 <div class="box_top"><img src="<?php echo base_url();?>img/logo.png" alt=""></div>    
                <div class="box_content">
                    <div class="row-fluid">
                        <div class="text-center">
                            <label for="l_username">Usuario</label>
								<?php  echo form_input('email_address', set_value('email_address'), 'id="email_address"  class="input-large span10" placeholder="Ingrese su Correo electrónico "  autofocus');?>
							 </div>
                        
   
   
							 <?php echo form_submit('submit', 'Enviar','class="btn btn-block btn-primary btn-large"'); ?>
							 <p class="text-center minor_text"><a class="" href="<?php echo  base_url()."index.php/login";?>">Iniciar sesion</a></p>
                    </div>
                </div>
				<?php echo form_close(); ?>
                
         
      
            
        </div>
          <?php $this->load->view('globales/jsLogin'); ?>   

    </body>
</html>