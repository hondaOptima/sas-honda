<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.go(-1)">Cotizaciones</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Crear Nueva Cotizaci&oacute;n</a></li>
                                                                       
                                </ul>
                          </section>

                       
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Crear Nueva Cotizaci&oacute;n</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                      	

<div style="font-size:1.1em;">
<div class="row-fluid" style="max-width:800px; margin:0 auto; position:relative;">

<?php echo $flash_message ?>
<?php


if(!empty($_GET['ficha'])): 

foreach($detalle_fichas as $todof): 
$ext12=$todof->fmo_ext_12;
$ext24=$todof->fmo_ext_24;
$ext36=$todof->fmo_ext_36;

$pre5=$todof->fmo_pre_5;
$pre10=$todof->fmo_pre_10;
$pre20=$todof->fmo_pre_20;

endforeach ?>
<?php else: ?>None<?php endif ?>

<?php 
if($uno_asesor): 
foreach($uno_asesor as $todo): 
$nombre=$todo->hus_nombre.' '.$todo->hus_apellido;
$cel=$todo->hus_celular; 
$correoh=$todo->hus_correo;
$tel=$todo->hus_telefono;
$radiop=$todo->hus_radio_personal;
$radioh=$todo->hus_radio_honda;  
endforeach ?>
<?php else: ?>None<?php endif ?>

<?php 
if($lista): 
foreach($lista as $todo): 
$idlista=$todo->lsp_IDlista_espera_cotizacion;
$ncnombre=$todo->lsp_nombre;
$ncapellido=$todo->lsp_apellido;
$nctelefono=$todo->lsp_email;
endforeach ?>
<?php else: ?>None<?php endif ?>

<?php 
if($lista_ver): 
$IDVA=$lista_ver[0]->ver_IDversion;
$Anombre=$lista_ver[0]->ver_nombre;
$Aprecio=$lista_ver[0]->ver_precio;
$Afinanciera=$lista_ver[0]->ver_financiera;
$Aenganche=$lista_ver[0]->ver_enganche;
$AseguroAuto=$lista_ver[0]->ver_seguro_auto;
$AseguroVida=$lista_ver[0]->ver_seguro_vida;
$Aapertura=$lista_ver[0]->ver_apertura;
$Aplacas=$lista_ver[0]->ver_placas_tenecia;
$Ainversion=$lista_ver[0]->ver_inversion;
$Aextencion=$lista_ver[0]->ver_extencion;
$Aseguro=$lista_ver[0]->ver_seguro;
$Abono=$lista_ver[0]->ver_bono;
$Atotal=$lista_ver[0]->ver_total;
$Amensualidad=$lista_ver[0]->ver_mensualidad;
$Aplazo=$lista_ver[0]->ver_plazo;
$Aporenganche=$lista_ver[0]->ver_por_enganche;
//
$IDVB=$lista_ver[1]->ver_IDversion;
$Bnombre=$lista_ver[1]->ver_nombre;
$Bprecio=$lista_ver[1]->ver_precio;
$Bfinanciera=$lista_ver[1]->ver_financiera;
$Benganche=$lista_ver[1]->ver_enganche;
$BseguroAuto=$lista_ver[1]->ver_seguro_auto;
$BseguroVida=$lista_ver[1]->ver_seguro_vida;
$Bapertura=$lista_ver[1]->ver_apertura;
$Bplacas=$lista_ver[1]->ver_placas_tenecia;
$Binversion=$lista_ver[1]->ver_inversion;
$Bextencion=$lista_ver[1]->ver_extencion;
$Bseguro=$lista_ver[1]->ver_seguro;
$Bbono=$lista_ver[1]->ver_bono;
$Btotal=$lista_ver[1]->ver_total;
$Bmensualidad=$lista_ver[1]->ver_mensualidad;
$Bplazo=$lista_ver[1]->ver_plazo;
$Bporenganche=$lista_ver[1]->ver_por_enganche;
//
$IDVC=$lista_ver[2]->ver_IDversion;
$Cnombre=$lista_ver[2]->ver_nombre;
$Cprecio=$lista_ver[2]->ver_precio;
$Cfinanciera=$lista_ver[2]->ver_financiera;
$Cenganche=$lista_ver[2]->ver_enganche;
$CseguroAuto=$lista_ver[2]->ver_seguro_auto;
$CseguroVida=$lista_ver[2]->ver_seguro_vida;
$Capertura=$lista_ver[2]->ver_apertura;
$Cplacas=$lista_ver[2]->ver_placas_tenecia;
$Cinversion=$lista_ver[2]->ver_inversion;
$Cextencion=$lista_ver[2]->ver_extencion;
$Cseguro=$lista_ver[2]->ver_seguro;
$Cbono=$lista_ver[2]->ver_bono;
$Ctotal=$lista_ver[2]->ver_total;
$Cmensualidad=$lista_ver[2]->ver_mensualidad;
$Cplazo=$lista_ver[2]->ver_plazo;
$Cporenganche=$lista_ver[2]->ver_por_enganche;
?>

<?php 
if($uno_cotizacion): 
foreach($uno_cotizacion as $todo): 
$idficha=$todo->cot_IDficha;
$cot_email=$todo->cot_email; 
$cot_nota=$todo->cot_nota;
$cot_precioex4=$todo->cot_precioex4;
$cot_precioex5=$todo->cot_precioex5;
$cot_precioex6=$todo->cot_precioex6;
$cot_pre1=$todo->cot_precio1;
$cot_pre2=$todo->cot_precio2;
$cot_pre3=$todo->cot_precio3;
endforeach ?>
<?php else: ?>None<?php endif ?>

<?php else: ?>None<?php endif ?>
<?php $url='';



if(empty($_GET['ficha'])){
if(empty($idficha)){$idficha=0;}	
	}
else{
$idficha=$_GET['ficha'];	
	}


?>
            <form name="formawwx" id="formarazon" action="<?php echo $url;?>" >
	<br>
	<div class="row-fluid">
    <div class="span4">
    Nombre:<br>
    <input type="text" class="keydatahuser" value="<?php if(!empty($ncnombre)){echo $ncnombre;}?>"    name="lsp_nombre">
    </div>
    <div class="span4">
    Apellido:<br>
    <input type="text" class="keydatahuser" value="<?php if(!empty($ncapellido)){echo $ncapellido;}?>"  name="lsp_apellido">
    </div>
    <div class="span4">
    Telefono:<br>
    <input type="text" class="keydatahuser" value="<?php if(!empty($nctelefono)){echo $nctelefono;}?>" name="lsp_email">
    </div>
    
    </div>
	<div class="row-fluid">
<div class="span5">
<b>Seleccione un modelo:</b>
<?php
echo form_dropdown('ficha', $lista_fichas,$idficha,'style="width:246px;"   id="s2_single" data-rel="chosen"');
?>
</div>
<div class="span5">
<b>Email Contacto:</b>
<input type="text" name="email" value="<?php if(empty($cot_email)){}else{echo $cot_email;}?>" class="span12 keyupEmail"> 
</div>
						                              
 </div>            
<br><br>

<div class="row-fluid">

<div class="box_a">
<div class="box_a_content no_sp inv_item" id="inv_all_items">
<table id="sorting_basic " class="table table-striped table_sorter">
<tbody>
<tr>
<td class="span2">Version</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input type="text" id="<?php echo $IDVA;?>" value="<?php echo $Anombre?>" name="ver_nombre" class="span10 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input type="text" id="<?php echo $IDVB;?>" value="<?php echo $Bnombre?>" name="ver_nombre"  class="span10 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input type="text" id="<?php echo $IDVC;?>" value="<?php echo $Cnombre?>" name="ver_nombre" class="span10 keyupdatax"></div>
</td>
</tr>
<tr>
<td>Precio del Auto</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input type="text" id="<?php echo $IDVA;?>" value="<?php echo $Aprecio;?>" name="ver_precio" class="span10 jQinv_item_qty jQinv_item_precio keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input type="text" id="<?php echo $IDVB;?>" value="<?php echo $Bprecio;?>" name="ver_precio" class="span10 jQinv_item_qty jQinv_item_precio2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input type="text" id="<?php echo $IDVC;?>" value="<?php echo $Cprecio;?>" name="ver_precio"
 class="span10 jQinv_item_qty jQinv_item_precio3  keyupdatax"></div>
</td>
</tr>
<tr>
<td>INST.FINANCIERA</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input id="<?php echo $IDVA;?>" name="ver_financiera" value="<?php echo $Afinanciera?>" type="text" class="span10 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input id="<?php echo $IDVB;?>" name="ver_financiera" value="<?php echo $Bfinanciera?>" type="text" class="span10 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input id="<?php echo $IDVC;?>" name="ver_financiera" value="<?php echo $Cfinanciera?>" type="text" class="span10 keyupdatax"></div>
</td>
</tr>
<tr>
<td>ENGANCHE</td>
<td class="span2">
<div  class="input-prepend input-append">
<span class="add-on">%</span>
<input type="text" id="<?php echo $IDVA;?>" name="ver_por_enganche" value="<?php echo $Aporenganche;?>" class="span3 jQinv_item_por_eng keyupdatax">
<span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_enganche" value="<?php echo $Aenganche?>" type="text" class="span6 jQinv_item_eng keyupdatax" readonly></div>
</td>
<td class="span2">
<div class="input-prepend input-append">
<span class="add-on">%</span>
<input type="text" id="<?php echo $IDVB;?>" name="ver_por_enganche" value="<?php echo $Bporenganche;?>" class="span3 jQinv_item_por_eng2 keyupdatax">
<span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_enganche" value="<?php echo $Benganche?>" type="text" class="span6 jQinv_item_eng2 keyupdatax" readonly></div>
</td>
<td class="span2">
<div class="input-prepend input-append">
<span class="add-on">%</span>
<input type="text" id="<?php echo $IDVC;?>" name="ver_por_enganche" value="<?php echo $Cporenganche;?>" class="span3 jQinv_item_por_eng3 keyupdatax">
<span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_enganche" value="<?php echo $Cenganche?>" type="text"class="span6 jQinv_item_eng3 keyupdatax" readonly></div>
</td>
</tr>
<tr>
<td>Seguro Auto</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_seguro_auto" value="<?php echo $AseguroAuto?>" type="text" class="span10 jQinv_item_sau keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_seguro_auto"  value="<?php echo $BseguroAuto?>" type="text" class="span10 jQinv_item_sau2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_seguro_auto"  value="<?php echo $CseguroAuto?>" type="text" class="span10 jQinv_item_sau3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>Seguro Vida</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_seguro_vida" value="<?php echo $AseguroVida?>" type="text" class="span10 jQinv_item_svi keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_seguro_vida" value="<?php echo $BseguroVida?>" type="text" class="span10 jQinv_item_svi2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_seguro_vida" value="<?php echo $CseguroVida?>" type="text" class="span10 jQinv_item_svi3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>Com.Apertura</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_apertura" value="<?php echo $Aapertura?>" type="text" class="span10 jQinv_item_ape keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_apertura" value="<?php echo $Bapertura?>" type="text" class="span10 jQinv_item_ape2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_apertura" value="<?php echo $Capertura?>" type="text" class="span10 jQinv_item_ape3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>Placas y Tenencia</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_placas_tenecia" value="<?php echo $Aplacas?>" type="text" class="span10 jQinv_item_pla keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_placas_tenecia" value="<?php echo $Bplacas?>" type="text" class="span10 jQinv_item_pla2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_placas_tenecia" value="<?php echo $Cplacas?>" type="text" class="span10 jQinv_item_pla3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>Inversión Inicial</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_inversion" value="<?php echo $Ainversion?>" type="text" class="span10 jQinv_item_ini keyupdatax" readonly></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_inversion"  value="<?php echo $Binversion?>" type="text" class="span10 jQinv_item_ini2 keyupdatax" readonly></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_inversion"  value="<?php echo $Cinversion?>" type="text" class="span10 jQinv_item_ini3 keyupdatax" readonly></div>
</td>
</tr>
<tr>
<td>Extensión Garantía</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_extencion" value="<?php echo $Aextencion?>" type="text" class="span10 jQinv_item_gar keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_extencion" value="<?php echo $Bextencion?>" type="text" class="span10 jQinv_item_gar2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_extencion" value="<?php echo $Cextencion?>" type="text" class="span10 jQinv_item_gar3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>Seguro R.C USA</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_seguro" value="<?php echo $Aseguro?>" type="text" class="span10 jQinv_item_seg keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_seguro" value="<?php echo $Bseguro?>" type="text" class="span10 jQinv_item_seg2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_seguro" value="<?php echo $Cseguro?>" type="text" class="span10 jQinv_item_seg3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>BONO</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_bono" value="<?php echo $Abono?>" type="text" class="span10 jQinv_item_bon keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_bono" value="<?php echo $Bbono?>" type="text" class="span10 jQinv_item_bon2 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_bono" value="<?php echo $Cbono?>" type="text" class="span10 jQinv_item_bon3 keyupdatax"></div>
</td>
</tr>
<tr>
<td>TOTAL:</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_total" value="<?php echo $Atotal?>" type="text" class="span10 jQinv_item_tot" readonly></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_total" value="<?php echo $Btotal?>" type="text" class="span10 jQinv_item_tot2" readonly></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_total" value="<?php echo $Ctotal?>" type="text" class="span10 jQinv_item_tot3" readonly></div>
</td>
</tr>
<tr>
<td>Mensualidades de</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVA;?>" name="ver_mensualidad" value="<?php echo $Amensualidad?>" type="text" class="span10 jQinv_item_qty keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVB;?>" name="ver_mensualidad" value="<?php echo $Bmensualidad?>" type="text" class="span10 jQinv_item_qty keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">$</span>
<input id="<?php echo $IDVC;?>" name="ver_mensualidad" value="<?php echo $Cmensualidad?>" type="text" class="span10 jQinv_item_qty keyupdatax"></div>
</td>
</tr>
<tr>
<td>Plazo</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input id="<?php echo $IDVA;?>" name="ver_plazo" value="<?php echo $Aplazo?>" type="text" class="span10 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input id="<?php echo $IDVB;?>" name="ver_plazo" value="<?php echo $Bplazo?>" type="text" class="span10 keyupdatax"></div>
</td>
<td class="span2">
<div class="input-prepend input-append"><span class="add-on">Aa</span>
<input id="<?php echo $IDVC;?>" name="ver_plazo" value="<?php echo $Cplazo?>" type="text" class="span10 keyupdatax"></div>
</td>
</tr>
</tbody>
</table>
<br>
<table id="sorting_basic"  class="table table-striped table_sorter">
<tbody>
<tr>
<td colspan="2">Su auto tiene Garantía de fábrica por 3 años o 60'000
Km.</td>
</tr>
<tr >
<td colspan="2">PRECIOS DE EXTENSION DE GARANTIA *</td>
</tr>
<tr>
<td><input type="text" name="cot_precioex1" value="12 MESES o 20 MIL KM" class="span12" readonly></td>
<td><input type="text" name="cot_precioex4"  value="<?php if(empty($_GET['ficha'])){echo $cot_precioex4;}else{echo $ext12;}?>" class="span12 jQinv_item_qty" readonly></td>
</tr>
<tr>
<td><input type="text" name="cot_precioex2" value="24 MESES o 40 MIL KM" class="span12" readonly></td>
<td><input type="text" name="cot_precioex5" value="<?php if(empty($_GET['ficha'])){echo $cot_precioex5;}else{echo $ext24;}?>" class="span12 jQinv_item_qty" readonly></td>
</tr>
<tr>
<td><input type="text" name="cot_precioex3" value="36 MESES o 60 MIL KM" class="span12" readonly></td>
<td><input type="text" name="cot_precioex6" value="<?php if(empty($_GET['ficha'])){echo $cot_precioex6;}else{ echo $ext36;}?>"class="span12 jQinv_item_qty" readonly></td>
</tr>
<tr>
<td>SERVICIO Km</td>
<td>PRECIO*</td>
</tr>
<tr>
<td><input type="text" name="cot_servicio1" value="5000" class="span12" readonly></td>
<td><input type="text" name="cot_precio1" value="<?php if(empty($_GET['ficha'])){echo $cot_pre1;}else{ echo $pre5;}?>" class="span12 jQinv_item_qty" readonly></td>
</tr>
<tr>
<td><input type="text" name="cot_servicio2"  value="10000" class="span12" readonly></td>
<td><input type="text" name="cot_precio2" value="<?php if(empty($_GET['ficha'])){echo $cot_pre2;}else{ echo $pre10;}?>" class="span12 jQinv_item_qty" readonly></td>
</tr>
<tr>
<td><input type="text" name="cot_servicio3"  value="20000" class="span12" readonly></td>
<td><input type="text" name="cot_precio3" value="<?php if(empty($_GET['ficha'])){echo $cot_pre3;}else{ echo $pre20;}?>" class="span12 " readonly></td>
</tr>
</tr></table>
<br>

<table id="sorting_basic"  class="table table-striped table_sorter">
<tbody>
<tr>
<td colspan="2">Su Asesor de Ventas:</td>
<td><input type="text" value="<?php echo $nombre;?>" class="span12" readonly></td>
</tr>
<tr >
<td colspan="2">Mail:</td>
<td><input type="text" value="<?php echo $correoh;?>" class="span12" readonly></td>
</tr>
<tr >
<td colspan="2">Celular:</td>
<td><input type="text" value="<?php echo $cel;?>" class="span12" readonly></td>
</tr>
<tr >
<td colspan="2">Radio Personal:</td>
<td><input type="text" value="<?php echo $radiop; ?>" class="span12" readonly></td>
</tr>
<tr >
<td colspan="2">Radio de Honda:</td>
<td><input type="text" value="<?php echo $radioh ?>" class="span12" readonly></td>
</tr>
<tr >
<td colspan="2">Teléfono Honda:</td>
<td><input type="text" value="<?php echo $tel; ?>" class="span12" readonly></td>
</tr>
</tr></table>
</div>

</div>
<br>
<textarea class="span12 keyupNota" name="cot_nota" rows="3" cols="70" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo $cot_nota;?></textarea>	                              
 </div>
	
     
      </form>
      <div class="respuesta" style="display:none">
     <div class="alert alert-success">
<a class="close" data-dismiss="alert">×</a>
<span class="noty_text">Hecho: su informacion se guardo con exito!</span>
</div>
      
      </div>             

<div class="keydatasave btn btn-success ">Guardar</div>
<a href="<?php echo base_url();?>index.php/cotizaciones/viewcotizarpdfhuser/<?php echo $idcot;?>/<?php echo $idlista;?>" target="_blank"  class="btn btn-warning" >Vista Previa PDF</a>


<a href="<?php echo base_url();?>index.php/cotizaciones/cotizarpdfHuser/<?php echo $idcot;?>/<?php echo $idlista;?>"   class="btn btn-primary" >Enviar y Salir.</a>

	</div>	</div>
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?>
  <script>
 $(document).ready(function(){

$('.keyupdatax').live('keyup',function(){
var id=$(this).attr('id');
var table='versio_cotizacion';
var row=$(this).attr('name');
var valor=$(this).val();
$.post("<?php echo base_url(); ?>querys/cotizaciones/insertCotizacion.php",{id:id,table:table,row:row,valor:valor}, function(data) { });
});

$('.keyupEmail').live('keyup',function(){
var id=<?php echo $idcot;?>;
var valor=$(this).val();
$.post("<?php echo base_url(); ?>querys/cotizaciones/insertCotizacionEmail.php",{id:id,valor:valor}, function(data) { });
});

$('.keydatahuser').live('keyup',function(){
var id=<?php echo $idlista;?>;
var valor=$(this).val();
var name=$(this).attr('name');
$.post("<?php echo base_url(); ?>querys/cotizaciones/insertCotizacionHuser.php",{id:id,valor:valor,name:name}, function(data) { });
});



$('.keyupNota').live('keyup',function(){
var id=<?php echo $idcot;?>;
var valor=$(this).val();
$.post("<?php echo base_url(); ?>querys/cotizaciones/insertCotizacionNota.php",{id:id,valor:valor}, function(data) { });
});

$('#s2_single').live('click',function(){	

var idh=$('select[name=contactos]').val();
var id=$(this).attr('id');

if(id=="s2_single"){ 
var id=<?php echo $idcot;?>;
var ficha=$('select[name=ficha]').val();
window.location="<?php echo base_url();?>/index.php/cotizaciones/LlenarCotizacionHuser/"+id+"-<?php echo $idlc;?>/?ficha="+ficha+"";}
});


$('.keydatasave').live('click',function(){
var id=<?php echo $idcot;?>;
var ficha=$('select[name=ficha]').val();
var email=$('input[name=email]').val();
var prex1=$('input[name=cot_precioex1]').val();
var prex2=$('input[name=cot_precioex2]').val();
var prex3=$('input[name=cot_precioex3]').val();


var prex4=$('input[name=cot_precioex4]').val();
var prex5=$('input[name=cot_precioex5]').val();
var prex6=$('input[name=cot_precioex6]').val();



var prex4=$('input[name=cot_precioex4]').val();
var prex5=$('input[name=cot_precioex5]').val();
var prex6=$('input[name=cot_precioex6]').val();
var ser1=$('input[name=cot_servicio1]').val();
var ser2=$('input[name=cot_servicio2]').val();
var ser3=$('input[name=cot_servicio3]').val();
var pre1=$('input[name=cot_precio1]').val();
var pre2=$('input[name=cot_precio2]').val();
var pre3=$('input[name=cot_precio3]').val();

$.post("<?php echo base_url(); ?>querys/cotizaciones/insertCotizacionPadre.php",{id:id,ficha:ficha,email:email,prex1:prex1,prex2:prex2,prex3:prex3,prex4:prex4,prex5:prex5,prex6:prex6,ser1:ser1,ser2:ser2,ser3:ser3,pre1:pre1,pre2:pre2,pre3:pre3}, function(data) {
	$('.respuesta').show();
	 });
});

	
});

  </script> 


    </body>
</html>