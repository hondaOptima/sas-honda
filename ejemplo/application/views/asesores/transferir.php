<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] =""; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                   <li><a href="<?php echo base_url(); ?>index.php/asesores">Asesores de venta</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Transferir contactos</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     <?php echo $flash_message ?>
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Contactos</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <?php echo form_open('asesores/transfer/','class="form-horizontal"'); ?>
                                            <table id="example" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                                <th width="20px;">Select</th>
                              <th>Asesor</th>
                                  <th>Contacto</th>
								  <th>Empresa</th>
								  <th>Estado</th>
								  <th>Tel&eacute;fono</th>
                                  <th>Email</th>
                                
							  </tr>
						  </thead>   
<tbody>



</tbody>

</table>

 <br><br>
                            <div class="controls" style="float:left; height:230px; margin-top:-20px;">
                              
                               <table><tr><td>
                               <b>Transferir Clientes Seleccionados a:</b>
                          </td><td>      
                                <?php
								
                               
								 echo form_dropdown('vendedor',$todo_vendedores, 'unos','style="width:246px;" id="selectErrorf" data-rel="chosen"');
								?>
                                </td></tr></table> <?php echo form_submit('submit', 'Transferir Información','class="btn btn-primary"'); ?>
								  
								</div>
                      
           <br><br> <br><br>
                            
                           

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
  <script type='text/javascript'>

	$(document).ready(function() {
	$('#example').dataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?php echo base_url();?>querys/dataProspectosTransferir.php",
		
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
	
} );
</script>
    </body>
</html>