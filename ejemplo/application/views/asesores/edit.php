<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Configuraci&oacute;n</a></li>
                                                                       
                                </ul>
                            </section>
                      
                      
                      
                      <div class="box_a">
                       <?php if($uno_vendedor): ?>
                          <?php foreach($uno_vendedor as $todo): ?>
                           <?php
						 $idh=$todo->hus_IDhuser; 
						    $nom=$todo->hus_nombre; 
							$ape=$todo->hus_apellido;
							$tel=$todo->hus_telefono; 
							$cor=$todo->hus_correo;
							$ant=$todo->hus_antiguedad; 
							$niv=$todo->hus_nivel;
							$sta=$todo->hus_status; 
							$rho=$todo->hus_radio_honda;
							$rpe=$todo->hus_radio_personal; 
							$cel=$todo->hus_celular;
							$ciu=$todo->hus_ciudad; 
							$foto=$todo->hus_foto;
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                                        <div class="box_a_heading">
                                            <h3><?php echo $nom.' '.$ape;?> -> <?php echo $ciu;?> </h3>
                                        </div>
                                        <div class="box_a_content cnt_a user_profile">
                                            <div class="row-fluid">
                                                <div class="span2">
                                                <a href="<?php echo base_url(); ?>index.php/asesores/picture/<?php echo $idh;?>">
                                 <div class="nailthumb-container square-thumb">
                                 <img width="150px" src="<?php echo base_url(); ?>upload/usuarios/<?php echo $foto;?>">
                                 </div>
                                 </a>
                                                </div>
                                                <div class="span10">
                                                    <div class="tabbable tabbable-bordered">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a data-toggle="tab" href="#tbp_a">Descripci&oacute;n</a></li>
                                                            <li><a data-toggle="tab" href="#tbp_b">Cambiar contraseña</a></li>
                                                          
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div id="tbp_a" class="tab-pane active">
                                                                    <p class="formSep"><small class="muted">Nombre:</small> <?php echo $nom.' '.$ape;?></p>
                                                                <p class="formSep"><small class="muted">Estado:</small><?php echo $sta;?></p>
                                                                <p class="formSep"><small class="muted">Nivel:</small><?php echo $niv;?></span></p>
                                                                <p class="formSep"><small class="muted">Telefono:</small> <?php echo $tel;?></p>
                                                                <p class="formSep"><small class="muted">Celular:</small> <?php echo $cel;?></p>
                                                                <p class="formSep"><small class="muted">Radio Honda:</small><?php echo $rho;?></p>
                                                                 <p class="formSep"><small class="muted">Radio Honda:</small><?php echo $rpe;?></p>
                                                                <p class="formSep"><small class="muted">Correo Electronico:</small><?php echo $cor;?></p>
                                                                 <br>
                            <?php echo anchor('asesores/editvendedor/'.$idh.'', 'Editar','class="btn btn-small btn-primary"'); ?>
                            <br><br>
                                                               
                                                                
                                                            </div>
                                                            <div id="tbp_b" class="tab-pane">
                                                                	<?php echo form_open('vendedores/changepass/'.$idh.'',''); ?>
										
                                        <div class="control-group">
								 <span class="status">Contraseña</span>
								<div class="controls">
								 <?php  echo form_input('password', set_value('password'), 'id="password"  class="input-large span10" placeholder=""  autofocus');?>
								</div>
							  </div>
                              
                              <div class="control-group">
								 <span class="status">Nueva Contraseña</span>
								<div class="controls">
								 <?php 
  
      echo form_password('npassword', '', 'id="npassword"  class="input-large span10" placeholder="" ');
   ?>
                                 
								</div>
							  </div>
                                     
                                      
								<?php echo form_submit('submit', 'Cambiar','class="btn btn-primary"'); ?>
						
						
                                       
                                        
									</span>	                                                        
								
							</li>
						  	<?php echo form_close(); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>