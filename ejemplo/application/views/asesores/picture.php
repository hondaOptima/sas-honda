<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>index.php/asesores/configuracion/<?php echo $_SESSION['id'];?>">Configuraci&oacute;n</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Cambira foto de perfil</a></li>
                                                                       
                                </ul>
                            </section>
                      
                      
                      
                      <div class="box_a">
                       <?php if($uno_vendedor): ?>
                          <?php foreach($uno_vendedor as $todo): ?>
                           <?php
						 $idh=$todo->hus_IDhuser; 
						  $foto=$todo->hus_foto;
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                     
                                        <div class="box_a_heading">
                                            <h3>Cambiar foto de perfil</h3>
                                        </div>
                                        <div class="box_a_content cnt_a user_profile">
                                            <div class="row-fluid">
                                                <div class="span2">
                                               <?php
                       echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
						<?php echo form_open_multipart('asesores/doUpload/'.$idh.''); ?>
                        
                       

<input type="file" name="userfile" id="userfile" />
<input type="submit" value ="Upload" />

<?php echo form_close(); ?>
                                                </div>
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>