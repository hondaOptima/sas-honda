<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.back()">Asesores de venta</a></li>
                                                                       
                                </ul>
                            </section>
                      
                      
                      
                      <div class="box_a">
                      
                                        <div class="box_a_heading">
                                            <h3>Editar informaci&oacute;n </h3>
                                        </div>
                                        <div class="box_a_content cnt_a user_profile">
                                           
                                            <?php if($uno_vendedor): ?>
                          <?php foreach($uno_vendedor as $todo): ?>
                           <?php
						 $idh=$todo->hus_IDhuser; 
						    $nom=$todo->hus_nombre; 
							$ape=$todo->hus_apellido;
							$tel=$todo->hus_telefono; 
							$cor=$todo->hus_correo;
							$ant=$todo->hus_antiguedad; 
							$niv=$todo->hus_nivel;
							$sta=$todo->hus_status; 
							$rho=$todo->hus_radio_honda;
							$rpe=$todo->hus_radio_personal; 
							$cel=$todo->hus_celular;
							$ciu=$todo->hus_ciudad; 
							$pass=md5($todo->hus_contrasena); 
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                    
                    
                    
                    
                    
                   <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                    
                    
							<?php echo form_open('asesores/updatetodo/'.$idh.'','class="form-horizontal"'); ?>
                            
                            
                         
						  <fieldset>
                          
                          <div class="control-group">
								<label class="control-label" for="selectError">Ciudad</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
                                 'Tijuana'  => 'Tijuana',
                                 'Mexicali'    => 'Mexicali',
								 'Ensenada'    => 'Ensenada',);
								 echo form_dropdown('ciudad', $options, $ciu,'style="width:246px;" id="selectError" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                              
							<div class="control-group">
								<label class="control-label" for="focusedInput">Nombre</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    
                                    <?php
									
									
									$data = array(
              'name'        => 'nombre',
              'id'          => 'nombre',
              'value'       => $nom,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                   
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Apellido</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  
									  
								
									$datab = array(
              'name'        => 'apellido',
              'id'          => 'apellido',
              'value'       => $ape,
              'maxlength'   => '',
              'size'        => '30',
              'style'       => '',
            );
									  
									  echo form_input($datab);?>
                                
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Telefono</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php 
									  
									
									$datac = array(
              'name'        => 'telefono',
              'id'          => 'telefono',
              'value'       => $tel,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  
									   echo form_input($datac);?>
                                    
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Celular</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php 
									  $datad = array(
              'name'        => 'celular',
              'id'          => 'celular',
              'value'       => $cel,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									   echo form_input($datad);?>
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Radio Personal</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php
									  $datae = array(
              'name'        => 'radiopersonal',
              'id'          => 'radiopersonal',
              'value'       => $rpe,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  
									    echo form_input($datae);?>
                                    
								  </div>
								</div>
							  </div>
                            
                            <div class="control-group">
								<label class="control-label" for="focusedInput">Radio Honda</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php 
									$dataf = array(
              'name'        => 'radiohonda',
              'id'          => 'radiohonda',
              'value'       => $rho,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									
									 echo form_input($dataf);?>
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              						
                            
                            <div class="control-group">
								<label class="control-label" for="prependedInput">Correo Electronico</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">@</span>
                                    <?php
									
									$datag = array(
              'name'        => 'correo',
              'id'          => 'correo',
              'value'       => $cor,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									
									  echo form_input($datag);?>
                                    
								  </div>
								 
								</div>
							  </div>
                              
                             
                            
                              
                              <div class="control-group">
								<label class="control-label" for="selectErrorx">Nivel</label>
								<div class="controls">
                                  <?php
                                $options = array(
                                 'Administrador'  => 'Administrador',
                                 'Vendedor'    => 'Vendedor',
								 'Recepcion'    => 'Recepcion',);
								 echo form_dropdown('nivel', $options,''.$niv.'','style="width:246px;" id="selectErrorx" data-rel="chosen"');
								?>
                                
                                
								  
								</div>
							  </div>
                              
                              
                             <div class="control-group">
								<label class="control-label" for="selectErrorw">Estado</label>
								<div class="controls">
                                 <?php
                                $options = array(
                                 'Empleado'  => 'Empleado',
                                 'Ex Empleado'    => 'Ex Empleado',
								 'Vacaciones'    => 'Vacaciones',);
								 echo form_dropdown('status', $options,$sta,'style="width:246px;" id="selectErrorxw" data-rel="chosen"');
								?>
                                
								
								</div>
							  </div>
                              
                              <div class="control-group">
							  <label class="control-label" for="date01">Fecha de Ingreso</label>
							  <div class="controls">
                                 <?php 
								 
								 list($ano,$mes,$dia)=explode('-',$ant);
								 $fecha=$mes.'/'.$dia.'/'.$ano;	
								 $datai= array(
              'name'        => 'antiguedad',
              'id'          => 'antiguedad',
			  'class'          => 'input-xlarge datepicker',
              'value'       => $fecha,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                            
                            
                            
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>
                                                    </div>
                                                </div>
 </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>