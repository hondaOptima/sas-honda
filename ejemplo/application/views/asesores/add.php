<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>index.php/asesores">Asesores de venta</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Agregar asesor de venta</a></li>
                                                                       
                                </ul>
                            </section>
                      
                      
                      
                      <div class="box_a">
                      
                                        <div class="box_a_heading">
                                            <h3>Agregar nuevo asesor de ventas</h3>
                                        </div>
                                        
                                        
                                          <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
                     	<?php echo form_open('asesores/add','class="form-horizontal"'); ?>
						  <fieldset>
                          <br>
                          <div class="control-group">
								<label class="control-label" for="selectError">Ciudad</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
                                 'Tijuana'  => 'Tijuana',
                                 'Mexicali'    => 'Mexicali',
								 'Ensenada'    => 'Ensenada',);
								 echo form_dropdown('ciudad', $options, 'Tijuana','style="width:246px;" id="selectError" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
                              
							<div class="control-group">
								<label class="control-label" for="focusedInput">Nombre</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('nombre', set_value('nombre'), 'id="nombre" size="16"');?>
                                   
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Apellido</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('apellido', set_value('apellido'), 'id="apellido" size="30"');?>
                                
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Tel&eacute;fono</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('telefono', set_value('telefono'), 'id="telefono" size="16"');?>
                                    
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Celular</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('celular', set_value('celular'), 'id="celular" size="16"');?>
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Radio Personal</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('radiopersonal', set_value('radiopersonal'), 'id="radiopersonal" size="16"');?>
                                    
								  </div>
								</div>
							  </div>
                            
                            <div class="control-group">
								<label class="control-label" for="focusedInput">Radio Honda</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('radiohonda', set_value('radiohonda'), 'id="radiohonda" size="16"');?>
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              						
                            
                            <div class="control-group">
								<label class="control-label" for="prependedInput">Correo Electr&oacute;nico</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">@</span>
                                    <?php  echo form_input('correo', set_value('correo'), 'id="correo" size="16"');?>
                                    
								  </div>
								 
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contraseña</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                     <?php  echo form_password('contrasena', set_value('contrasena'), 'id="contrasena" size="16"');?>
                                    
								  </div>
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="selectErrorx">Nivel</label>
								<div class="controls">
                                  <?php
                                $options = array(
                                 'Administrador'  => 'Administrador',
                                 'Vendedor'    => 'Vendedor',
								 'Recepcion'    => 'Recepcion',
								 'Contabilidad'    => 'Contabilidad',
								 'Facturacion'    => 'Facturacion',
								 'FI'    => 'F&I',
								 
								 );
								 echo form_dropdown('nivel', $options, 'Vendedor','style="width:246px;" id="selectErrorx" data-rel="chosen"');
								?>
                                
                                
								  
								</div>
							  </div>
                              
                              
                             <div class="control-group">
								<label class="control-label" for="selectErrorw">Estado</label>
								<div class="controls">
                                 <?php
                                $options = array(
                                 'Empleado'  => 'Empleado',
                                 'Ex Empleado'    => 'Ex Empleado',
								 'Vacaciones'    => 'Vacaciones',);
								 echo form_dropdown('status', $options, 'Empleado','style="width:246px;" id="selectErrorxw" data-rel="chosen"');
								?>
                                
								
								</div>
							  </div>
                              
                              <div class="control-group">
							  <label class="control-label" for="date01">Fecha de Ingreso</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 
								 $datai= array(
              'name'        => 'antiguedad',
              'id'          => 'antiguedad',
			  'class'          => 'input-xlarge datepicker',
              'value'       => '12/07/2012',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
                            
                            
                        
                            
                            
                            
                            
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Guardar Información','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>

                                        
                                        
                                        
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>