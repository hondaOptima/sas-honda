<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Configuraci&oacute;n</a></li>
                                                                       
                                </ul>
                            </section>
                      
                      
                      
                      <div class="box_a">
                       <?php if($uno_vendedor): ?>
                          <?php foreach($uno_vendedor as $todo): ?>
                           <?php
						 $idh=$todo->hus_IDhuser; 
						    $nom=$todo->hus_nombre; 
							$ape=$todo->hus_apellido;
							$tel=$todo->hus_telefono; 
							$cor=$todo->hus_correo;
							$ant=$todo->hus_antiguedad; 
							$niv=$todo->hus_nivel;
							$sta=$todo->hus_status; 
							$rho=$todo->hus_radio_honda;
							$rpe=$todo->hus_radio_personal; 
							$cel=$todo->hus_celular;
							$ciu=$todo->hus_ciudad; 
							$foto=$todo->hus_foto;
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                                        <div class="box_a_heading">
                                            <h3><?php echo $nom.' '.$ape;?> -> <?php echo $ciu;?> </h3>
                                        </div>
                                        <div class="box_a_content cnt_a user_profile">
                                            <div class="row-fluid">
                                                <div class="span2">
                                               
                                 <div class="nailthumb-container square-thumb">
                                 <img width="150px" src="<?php echo base_url(); ?>upload/usuarios/<?php echo $foto;?>">
                                 </div>
                                
                                                </div>
                                                <div class="span10">
                                                    <div class="tabbable tabbable-bordered">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a data-toggle="tab" href="#tbp_a">Descripci&oacute;n</a></li>

                                                          
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div id="tbp_a" class="tab-pane active">
                                                                    <p class="formSep"><small class="muted">Nombre:</small> <?php echo $nom.' '.$ape;?></p>
                                                                <p class="formSep"><small class="muted">Estado:</small><?php echo $sta;?></p>
                                                                <p class="formSep"><small class="muted">Nivel:</small><?php echo $niv;?></span></p>

  <p class="formSep"><small class="muted">Contactos:</small><?php echo $ncontactos[0]->ncon;?></span><span style="color:#F00; font-weight:bold">  Seran Transferidos al Gerente de Ventas</span></p>
    <p class="formSep"><small class="muted">Ventas:</small><?php echo $nventas[0]->total + $nventas[1]->total;?></span> <span style="color:#060; font-weight:bold">  Seran Transferidas a Ventas de Casa</span></p>  
<a href="<?php echo base_url().'index.php/asesores/baja/'.$idh.'/'.$ciu.'';?>" onclick="return confirm('Esta  seguro que desea dar de baja a: <?php echo $nom.' '.$ape;?>?');" class="btn btn-small btn-danger" >Dar de Baja</a>
 <br><br>
                                                               
                                                                
                                                            </div>
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>