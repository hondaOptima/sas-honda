<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Asesores de venta</a></li>
                                                                       
                                </ul>
                            </section>
                      
      
			<?php echo anchor('asesores/add', 'Agregar nuevo asesor de ventas','class="btn btn-small"'); ?> | 
           
            
                <?php echo anchor('asesores/transferir', 'Transferir Contactos a un asesor','class="btn btn-small"'); ?>
            
 <br><br>
 
<?php echo $flash_message ?>
                      
                      <div class="box_a">
                       <?php if($todo_vendedores): ?>
                       
                                      
                                        
                                             <div class="box_a_heading">
                                            <h3>Lista de asesores de venta</h3>
                                        </div>
                                        <div class="box_a_content">
                                            <div id="contact-list">
                                                <div class="filters list_filters clearfix">
                                                    <ul class="sort-by inline sorting">
                                                        <li class="sort btn btn-small" data-sort="l_username">Ordenar por nombre <i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></li>
                                                        <li class="sort btn btn-small" data-sort="l_company">Ordenar por ciudad<i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></li>
                                                        <li class="sort btn btn-small" data-sort="l_email">Ordenar por correo eletr&oacute;nico <i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></li>
                                                    </ul>
                                                    
                                                    <div id="count_list">Asesores registrados: <span>20</span></div>
                                                    <input class="search-fuzzy span4" placeholder="Search..." type="text" />
                                                </div>
                                                <ul class="list clearfix">
                                                       
                                                    <?php foreach($todo_vendedores as $todo): ?>    
                                                          <li>
<?php if($todo->hus_foto=='usuarios.jpg'){$foto='avatar_1.jpg';}else{$foto=$todo->hus_foto;}?>                                                          
<img class="img-polaroid l_avatar" alt="" src="<?php echo base_url();?>upload/usuarios/<?php echo $foto;?>">
<p class="l_username"><?php echo $todo->hus_nombre.' '.$todo->hus_apellido ?></p>
<p class="l_company l_other"><?php echo $todo->hus_nivel.'/'.$todo->hus_ciudad ?></p>
<p class="l_email l_other"><span class="muted">Correo Electr&oacute;nico:</span> <?php echo $todo->hus_correo ?></p>
<p class="l_phone l_other"><span class="muted">Tel&eacute;fono:</span> <?php echo $todo->hus_telefono ?></p>
<p class="l_address l_other"><span class="muted">Radio Honda:</span><?php echo $todo->hus_radio_honda ?></p>
<p class="l_address l_other"><span class="muted"><a class="btn btn-small" href="<?php echo base_url();?>index.php/asesores/updatetodo/<?php echo $todo->hus_IDhuser; ?>">Editar</a></span> <a class="btn btn-small" href="<?php echo base_url().'index.php/asesores/viewbaja/'.$todo->hus_IDhuser.'/'.$todo->hus_ciudad.'';?>">Baja</a></p>
                                                      
                                                    </li>
                                                    
                                                 
                            <?php endforeach ?>   
                                                    
                                                    
                                                    
                                                                                                     
                                                                                                    </ul>
                                                <div class="list-helpers">
                                                    <div class="hagal_pagination pagination-centered">
                                                        <ul class="paging bottomPaging"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
                                        
                                        
                                                <?php else: ?>
None
<?php endif ?>
                                        
                                        
                                        
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>