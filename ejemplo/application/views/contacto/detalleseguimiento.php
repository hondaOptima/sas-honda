<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.back()">Facturaci&oacute;n</a></li>
                                      <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Detalle de seguimiento de venta</a></li>
                                                                       
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
<?php echo $flash_message; ?>
<?php foreach($detalle_procesodeventa as $tod){
	
	$siuser=$tod->prcv_status_asesor;
	$sigere=$tod->prcv_status_gerente;
	$sifac=$tod->prcv_prcv_status_credito_contado;
	$sifi=$tod->prcv_status_fi;
	$sicont=$tod->prcv_status_contabilidad;
	$fechae=$tod->prcv_fechaentrega;
	$sidire=$tod->prcv_director;
	$sigene=$tod->prcv_director_gral;
	$ido=$tod->prcv_IDordencompra;
	$contacto=$tod->con_nombre.' '.$tod->con_apellido;
	$asesor=$tod->hus_nombre.' '.$tod->hus_apellido;
	$correo=$tod->hus_correo;
	$idh=$tod->hus_IDhuser;
	$ciudadase=$tod->hus_ciudad;
	$id=$tod->prcv_IDprocesodeventa;
	$celular=$tod->hus_celular;
	$idc=$tod->con_IDcontacto;
	$tit=$tod->con_titulo;
	$nom=$tod->con_nombre;
	$ape=$tod->con_apellido;
	$folder=$tod->con_folder;
	$auto=$tod->data_auto;
	$tauto1=$tod->datco_nuevo; //demo
	$tauto2=$tod->datco_snuevo; //nuevo
	$tauto3=$tod->datco_nuevox; //seminuevo	
	$fechafactura=$tod->dat_fecha_facturacion;	
	if($tauto1=='si'){
		$tauto='Demo';
		}
		elseif($tauto2=='si'){
			$tauto='Nuevo';	
			}
			elseif($tauto3=='si'){
				$tauto='Seminuevo';	
				}
	
	
	}

if($candado){foreach($candado as $tod){  $candado2=$tod->ctb_desc;}}else{  $candado2='no';}
if($gerencia){foreach($gerencia as $tod){  $gerencia2=$tod->ctb_desc;}}else{$gerencia2='no';}
if($casesor){foreach($casesor as $tod){  $casesor2=$tod->ctb_desc;}}else{$casesor2='no';}
if($cmerca){foreach($cmerca as $tod){   $merca2=$tod->ctb_desc;}}else{$merca2='no';}
if($ccontacto){foreach($ccontacto as $tod){   $ccontacto2=$tod->ctb_desc;}}else{$ccontacto2='no';}

//id f&i
//echo $_SESSION['username'];
if($_SESSION['username']=='creditomxli@hondaoptima.com' || $_SESSION['username']=='eruiz@hondaoptima.com' || $_SESSION['username']=='egranados@hondaoptima.com'){
$IDFANDI='sessionfi';}else{$IDFANDI='';}
//id gerente
if($_SESSION['username']=='frivera@hondaoptima.com' || $_SESSION['username']=='oscarv@hondaoptima.com' || $_SESSION['username']=='afierro@hondaoptima.com' || $_SESSION['username']=='josemaciel@hondaoptima.com'){
$IDGERENTE='session';}else{ $IDGERENTE='';}
//id facturacion
if($_SESSION['username']=='auxmex@hondaoptima.com' || $_SESSION['username']=='auxventas@hondaoptima.com' || $_SESSION['username']=='amartinez@hondaoptima.com'){
$IDFACT='sessionfac'; $DISFACT='';}else{$IDFACT=''; $DISFACT='disabled="disabled"';}
//id contabilidad
if($_SESSION['username']=='contamxli@hondaoptima.com' || $_SESSION['username']=='contaens@hondaoptima.com'){
$IDCONTA='sessioncont'; $DISCONTA='';}else{ $IDCONTA=''; $DISCONTA='disabled="disabled"';}
// vista para agendar
if($_SESSION['nivel']=='Administrador'){
if($_SESSION['username']=='frivera@hondaoptima.com' || $_SESSION['username']=='oscarv@hondaoptima.com' || $_SESSION['username']=='afierro@hondaoptima.com' || $_SESSION['username']=='josemaciel@hondaoptima.com'){
$VERAGENDAR='block'; $DISGERE='';}else{$VERAGENDAR='none';$DISGERE='disabled="disabled"';}}
else{$VERAGENDAR='none';$DISGERE='disabled="disabled"';}
//Vista para candado de seguridad, solo contabilidad
if($_SESSION['username']=='monica@hondaoptima.com' || $_SESSION['username']=='contamxli@hondaoptima.com' || $_SESSION['username']=='contaens@hondaoptima.com'){
$VERCANDADO='block';}else{$VERCANDADO='none';}
//des habilitar F&i para que nadie pueda dar click
if($_SESSION['username']=='creditomxli@hondaoptima.com' || $_SESSION['username']=='eruiz@hondaoptima.com' || $_SESSION['username']=='egranados@hondaoptima.com'){
$DISFI='';	}else{$DISFI='disabled="disabled"';}  
//fandi aprobado si o no.
if($sifi=='aprobado'){$valfi='checked';}else{$valfi=''; $IDGERENTE='';}
//gerente aprobado si o no
if($sigere=='aprobado'){$valgere='checked';}else{$valgere=''; $IDFACT='';}
//facturacion aprobado si o no
if($sifac=='aprobado'){$valfac='checked';}else{$valfac=''; $IDASESOR='';}
//contabilidad aprobado si o no
if($sicont=='aprobado'){$valcont='checked';}else{$valcont='';}
//asesor aprobado si o no
if($sigene=='aprobado'){$valetodo='checked';}else{$valetodo=''; }

if($sifac=='aprobado' && $sigene=='aprobado'){$IDCONTA='sessioncont'; }else{$IDCONTA=''; }
//id asesor segunda revision
if($sifac=='aprobado' && $_SESSION['nivel']=='Vendedor'){$IDASESOR='sessiondos'; $DISASESOR='';  
}else{$DISASESOR='disabled="disabled"';$IDASESOR=''; }

if($fechafactura=='0000-00-00'){$IDFACT='';}
//datax fecha de e7ntrega
$datax['lista_fechas']=$lista_fechas;
$datax['info_venta']=$info_venta; 
$datax['info_entrega']=$info_entrega;
$datax['ido']=$idd;
$datax['id']=$idd;
?>
               
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Detalle de seguimiento .:<a href="<?php echo base_url();?>index.php/contacto/bitacora/<?php echo $idc;?>"><?php echo $tit.' '.$nom.' '.$ape;?></a></h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<div style="100%">
<div class="tabbable tabbable-bordered">
<ul class="nav nav-tabs">
<li class="active"><a data-toggle="tab" href="#tb1_a">Aprobaciones</a></li>
<li><a data-toggle="tab" href="#tb1_b">Avisos</a></li>
<li><a data-toggle="tab" href="#tb1_c">Orden de Compra</a></li>
<li style="display:<?php echo $VERAGENDAR;?>"><a data-toggle="tab" href="#tb1_d">Agendar entrega</a></li>
<li style="display:<?php echo $VERCANDADO;?>"><a data-toggle="tab" href="#tb1_e">Seguridad</a></li>
<li><a data-toggle="tab" href="#tb1_f">Repositorio</a></li>
</ul>

<div class="tab-content">
<div id="tb1_a" class="tab-pane active">
<div class="row-fluid">
<div class="span2">
<h4 class="heading_b">Asesor</h4>
<div class="switch switch-small" data-on="success">
<input type="checkbox" disabled='disable' checked>
</div>
<h4 class="heading_b"><?php echo $asesor;?></h4>
</div>

<div class="span2">
<h4 class="heading_b">F&I</h4>

<div id="<?php echo $IDFANDI;?>"  class="switch switch-small"  data-on="success">
<input type="checkbox" <?php echo $DISFI; echo $valfi;?>>
</div>
<h4 class="heading_b"><?php echo $fandi;?></h4>
</div>

<div class="span2">
<h4 class="heading_b">Gerente</h4>
<div id="<?php echo $IDGERENTE; ?>" class="switch switch-small" data-on="success">
<input type="checkbox" <?php echo $DISGERE; echo $valgere;?>>
</div>
<h4 class="heading_b"><?php echo $gerente;?></h4>
</div>
                                                
<div class="span2">
<h4 class="heading_b">Facturaci&oacute;n</h4>
<div id="<?php echo $IDFACT; ?>" class="switch switch-small" data-on="success">
<input type="checkbox" <?php echo $DISFACT; echo $valfac;?>>
</div>
<h4 class="heading_b"><?php echo $facturacion;?></h4>



</div>

<div class="span2">
<h4 class="heading_b">Asesor</h4>
<div id="<?php echo $IDASESOR; ?>" class="switch switch-small" data-on="success">
<input type="checkbox" <?php echo $DISASESOR; echo $valetodo;?>>
</div> 
<h4 class="heading_b"><?php echo $asesor;?></h4>
</div>
                                               
<div class="span2">
<h4 class="heading_b">Contabilidad</h4>
<div id="<?php echo $IDCONTA;?>" class="switch switch-small" data-on="success">
<input type="checkbox" <?php echo $DISCONTA; echo $valcont;?>>
</div>
<h4 class="heading_b"><?php echo $contabilidad;?></h4>
</div>

</div>                                                               
</div>
<div id="tb1_b" class="tab-pane">
<!-- Aviso--><?php 
$data['DatFandi']=$DatFandi;
$data['DatGeren']=$DatGeren;
$data['DatFactu']=$DatFactu;
$data['DatContab']=$DatContab;
$data['DatAsesor']=$celular.'/'.$correo;
$this->load->view('contacto/avisosaAsesor',$data); ?>  <!--Fin aviso-->
</div>

<div id="tb1_c" class="tab-pane">
<?php if($fechafactura=='0000-00-00'){?>
<?php echo form_open('contacto/facturar/'.$ido.'','class="form-horizontal"');?>
La documentacion esta completa ?
<input type="checkbox"><br>
Fecha de facturacion:<input type="text" class="span2" name="fecha" value="<?php echo date('d - m - Y');?>" readonly="readonly">
<input type="submit" value="Facturar">
</form>
<?php } ?>
<iframe frameborder="0" width="100%" height="600px" src="<?php echo site_url('contacto/viewordendecomprapdf/'.$ido.'');?>">
</iframe>
<!--Aviso de pedio load ajax
<div class="todoavisodepedido" ></div>-->                                                              
</div>
<div id="tb1_d" class="tab-pane">
<!-- fecha entrega--><?php 
$this->load->view('contacto/fechaentrega',$datax); ?>  
<!--Fin fecha entrega-->
</div>


<div id="tb1_e" class="tab-pane">
 <?php
if($_SESSION['username']=='monica@hondaoptima.com' || $_SESSION['username']=='contamxli@hondaoptima.com' || $_SESSION['username']=='contaens@hondaoptima.com'){
$can='no';}else{$can='si';}
if($_SESSION['nivel']=='Administrador' && $can=='no') {  ?>
<div class="row-fluid sortable">	
                   	<div class="box span12">
				
					<div class="box-content">
				
                 <div class="row-fluid">      
                 <div class="span4">
								<h6>Candado</h6>
                                <p><input class="keyupdataxchcon" <?php if($candado2=='candado'){echo 'checked="checked"';} ?> name="candado" type="checkbox" ></p>
								</div>
							  <div class="span4">
								<h6>Informe a Gerencia</h6>
                              <p><input class="keyupdataxchcon" name="gerencia" <?php if($gerencia2=='gerencia'){echo 'checked="checked"';} ?> type="checkbox" ></p>
								 </div>
                                  <div class="span4">
								<h6>Informe Asesor de venta:</h6>
                             <p><input class="keyupdataxchcon"  name="asesor" <?php if($casesor2=='asesor'){echo 'checked="checked"';} ?> type="checkbox"></p>
								 </div>		


                
                
                          
                
                 <div class="row-fluid">      
                 <div class="span4">
								<h6>Informe Mercadotecnia</h6>
                                <p><input name="merca" <?php if($merca2=='merca'){echo 'checked="checked"';} ?> class="keyupdataxchcon" type="checkbox" ></p>
								
                               
				</div>
														<?php
if($_SESSION['username']=='monica@hondaoptima.com' && $sicont!='aprobado' ){
	
if($sifac=='aprobado' && $sigene=='aprobado'){
?>                                 
                                  <div class="span4">
                                  <br><br><br>
                             <p>
<?php echo form_open('contacto/aprobarcont','class="form-horizontal"');?>
  <input type="hidden" value="<?php echo $id;?>" name="id">
                 <input type="hidden" value="<?php echo $idc;?>" name="idc">
                <input type="hidden" value="Administrador" name="tipo">
                 <input type="hidden" value="<?php echo $idh;?>" name="asesor">
                 <input type="hidden" value="<?php echo $contacto;?>" name="nombre">                             
<input class="button button-small" value="Aprobar."  name="button" type="submit">

</form>
</p>
								 </div>		
                                 
<?php }	} ?>  		</div>
	
    
    					
                                 
                                 
				</div>
                
                
                
						
					</div>	
				</div><!--/span-->
				
			</div><!--/row-->             
                		 <?php } ?>
                         
                        
</div> <div id="tb1_f" class="tab-pane">
<!-- Lista de archivos--><?php $ver['ver']='off'; $this->load->view('contacto/repositorio',$ver); ?> 
</div>	
                                                       
                                                       
                                                        </div>
                                                    </div>
                                                </div>

 </div>
 
 

                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        
                        
                           <!-- Inicio aprobacion f&1 -->        
		<div class="modal hide fade" id="myModalfi">
			<div class="modal-header">
            
				<button type="button" class="close" data-dismiss="modal">×</button>
            <h2>Informaci&oacute;n de F&I para <?php if($sifi=='aprobado'){echo 'desaprobar';}else{echo 'aprobar';}?></h2>
				
			</div>
               <?php
			    if($sifi=='aprobado'){
					
				echo form_open('contacto/desaprobarfi','class="form-horizontal"');
					}else{
						
				echo form_open('contacto/aprobarfi','class="form-horizontal"');
						}
				
				 ?>
			<div class="modal-body">
				<p>
                
                <input type="hidden" value="<?php echo $id;?>" name="id">
                <input type="hidden" value="<?php echo $idh;?>" name="asesor">
                <input type="hidden" value="Administrador" name="tipo">
                <input type="hidden" value="<?php echo $contacto;?>" name="nombre">
                  <div class="control-group">
								<label class="control-label" for="focusedInput">Usuario:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('email', set_value('email'), 'id="titulo" size="16"');?>
                                   </div>
								</div>
							  </div>
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contrase&ntilde;a</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <input type="password" name="pass">
                                   
								  </div>
								</div>
							  </div>
                              
                              
                
                
                </p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn" >Cerrar</a>
				<input type="submit" value="Aprobar" class="btn btn-primary">
			</div>
            </form>
		</div>
		
        <!-- Fin aprobacion f&i -->
        
        
        
         <!-- Inicio aprobacion f&1 -->        
		<div class="modal hide fade" id="myModalcont">
			<div class="modal-header">
            
				<button type="button" class="close" data-dismiss="modal">×</button>
            <h2>Informaci&oacute;n de Contabilidad para <?php if($sicont=='aprobado'){echo 'desaprobar';}else{echo 'aprobar';}?></h2>
				
			</div>
               <?php
			    if($sicont=='aprobado'){
					
				echo form_open('contacto/aprobarcont','class="form-horizontal"');
					}else{
						
				echo form_open('contacto/aprobarcont','class="form-horizontal"');
						}
				
				 ?>
			<div class="modal-body">
				<p>
                <input type="hidden" value="<?php echo $id;?>" name="id">
                 <input type="hidden" value="<?php echo $idc;?>" name="idc">
                <input type="hidden" value="Administrador" name="tipo">
                 <input type="hidden" value="<?php echo $idh;?>" name="asesor">
                 <input type="hidden" value="<?php echo $contacto;?>" name="nombre">
                  <div class="control-group">
								<label class="control-label" for="focusedInput">Usuario:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('email', set_value('email'), 'id="titulo" size="16"');?>
                                   </div>
								</div>
							  </div>
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contrase&ntilde;a</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <input type="password" name="pass">
                                   
								  </div>
								</div>
							  </div>
                
                
                </p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cerrar</a>
				<input type="submit" value="Aprobar" class="btn btn-primary">
			</div>
            </form>
		</div>
        <!--fac--->
       
		<div class="modal hide fade" id="myModalfac">
			<div class="modal-header">
            
				<button type="button" class="close" data-dismiss="modal">×</button>
            <h2>Informaci&oacute;n de Facturaci&oacute;n para <?php if($sicont=='aprobado'){echo 'desaprobar';}else{echo 'aprobar';}?></h2>
				
			</div>
               <?php
			    if($sifac=='aprobado'){
					
				echo form_open('contacto/desaprobarfac','class="form-horizontal"');
					}else{
						
				echo form_open('contacto/aprobarfac','class="form-horizontal"');
						}
				
				 ?>
			<div class="modal-body">
				<p>
                <input type="hidden" value="<?php echo $id;?>" name="id">
                 <input type="hidden" value="<?php echo $idc;?>" name="idc">
                 <input type="hidden" value="<?php echo $idh;?>" name="asesor">
                <input type="hidden" value="Administrador" name="tipo">
                <input type="hidden" value="<?php echo $contacto;?>" name="nombre">
                  <div class="control-group">
								<label class="control-label" for="focusedInput">Usuario:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('email', set_value('email'), 'id="titulo" size="16"');?>
                                   </div>
								</div>
							  </div>
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contrase&ntilde;a</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <input type="password" name="pass">
                                   
								  </div>
								</div>
							  </div>
                              
                              
                
                
                </p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cerrar</a>
				<input type="submit" value="Aprobar" class="btn btn-primary">
			</div>
            </form>
		</div>
        
        
        
        
        
        
                    <!-- Inicio aprobacion gerente -->        
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
            
				<button type="button" class="close" data-dismiss="modal">×</button>
            <h2>Informaci&oacute;n del Gerente para <?php if($sigere=='aprobado'){echo 'desaprobar';}else{echo 'aprobar';}?></h2>
				
			</div>
               <?php
			    if($sigere=='aprobado'){
					
				echo form_open('contacto/desaprobargerente','class="form-horizontal"');
					}else{
						
				echo form_open('contacto/aprobargerente','class="form-horizontal"');
						}
				
				 ?>
			<div class="modal-body">
				<p>
                <input type="hidden" value="<?php echo $id;?>" name="id">
                <input type="hidden" value="Administrador" name="tipo">
                <input type="hidden" value="<?php echo $idh;?>" name="asesor">
                <input type="hidden" value="<?php echo $contacto;?>" name="nombre">
                <input type="hidden" value="<?php echo $ciudadase;?>" name="ciudadase">
                
                  <div class="control-group">
								<label class="control-label" for="focusedInput">Usuario:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('email', set_value('email'), 'id="titulo" size="16"');?>
                                   </div>
								</div>
							  </div>
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contrase&ntilde;a</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <input type="password" name="pass">
                                   
								  </div>
								</div>
							  </div>
                
                
                </p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cerrar</a>
				<input type="submit" value="Aprobar" class="btn btn-primary">
			</div>
            </form>
		</div>
 <!-- Inicio aprobacion gerente -->        
		<div class="modal hide fade" id="myModaldos">
			<div class="modal-header">
            
				<button type="button" class="close" data-dismiss="modal">×</button>
            <h2>Informaci&oacute;n del Asesor para <?php if($sigene=='aprobado'){echo 'desaprobar';}else{echo 'aprobar';}?></h2>
				
			</div>
               <?php
			    if($sigene=='aprobado'){
					
				echo form_open('contacto/desaprobarasedos','class="form-horizontal"');
					}else{
						
				echo form_open('contacto/aprobarasedos','class="form-horizontal"');
						}
				
				 ?>
			<div class="modal-body">
				<p>
                <input type="hidden" value="<?php echo $id;?>" name="id">
                <input type="hidden" value="Administrador" name="tipo">
                <input type="hidden" value="<?php echo $idh;?>" name="asesor">
                <input type="hidden" value="<?php echo $contacto;?>" name="nombre">
                  <div class="control-group">
								<label class="control-label" for="focusedInput">Usuario:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('email', set_value('email'), 'id="titulo" size="16"');?>
                                   </div>
								</div>
							  </div>
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Contrase&ntilde;a</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <input type="password" name="pass">
                                   
								  </div>
								</div>
							  </div>
                
                
                </p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cerrar</a>
				<input type="submit" value="Aprobar" class="btn btn-primary">
			</div>
            </form>
		</div>
        
        <?php 
$datax['idc']=$idc;$datax['folder']=$folder;
$this->load->view('contacto/formUploadfile',$datax); 
?>   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?>
<?php 
if($ciudadase=='Tijuana'){$emailg='frivera@hondaoptima.com'; $emailmerca='mercadotecnia@hondaoptima.com';}
if($ciudadase=='Mexicali'){$emailg='oscarv@hondaoptima.com'; $emailmerca='lucero@hondaoptima.com';}
if($ciudadase=='Ensenada'){$emailg='afierro@hondaoptima.com'; $emailmerca='casa@hondaoptima.com';}
?>    
    <script>
$(document).ready(function(){
	
	 $(".eliminarfoto").live('click',function() {

disp_confirm();
      
    });

 $('.arbol').load('<?php echo base_url();?>arbol.php?idc=<?php echo $idc;?>');

$('.keyupdataxchcon').live('click',function(){	
	
var mcCbxCheck = $(this);
var id=<?php echo $idd;?>;
var table=$(this).attr('id');
var row=$(this).attr('name');
var tauto="<?php echo $tauto;?>";
var auto='<?php echo $auto;?>';
if(row=='gerencia'){ var email='<?php echo $emailg;?>';}
if(row=='asesor'){ var email='<?php echo $correo;?>';}
if(row=='merca'){ var email='<?php echo $emailmerca;?>';}

var con='<?php echo $contacto; ?>';
var huser='<?php echo $asesor; ?>';
if(mcCbxCheck.is(':checked')) {
$.post("<?php echo base_url(); ?>querys/insertcandado.php",{id:id,table:'contabilidad',row:row,valor:'si',email:email,con:con,huser:huser,tauto:tauto,auto:auto}, function(data) { });
    }
    else{
$.post("<?php echo base_url(); ?>querys/deletecandado.php",{id:id,table:'contabilidad',row:row}, function(data) { });
    }
	
});


		
		$('#session').live('click',function(e){
		
		e.preventDefault();
		$('#myModal').modal('show');
		
		
		});
		
		$('#sessiondos').live('click',function(e){
		
		e.preventDefault();
		$('#myModaldos').modal('show');
		
		
		});
		
		$('#sessionfi').live('click',function(e){
		
		e.preventDefault();
		$('#myModalfi').modal('show');
		
		
		});
		
		
		$('#sessioncont').live('click',function(e){
		
		e.preventDefault();
		$('#myModalcont').modal('show');
		
		
		});
		
		$('#sessionfac').live('click',function(e){
		
		e.preventDefault();
		$('#myModalfac').modal('show');
		
		
		});
		
		$('#fechaentrega').live('click',function(){
		
		window.location.href="<?php echo base_url(); ?>index.php/contacto/fechaentrega/<?php echo $id;?>/<?php echo $ido;?>";
		
		
		});
		
		
		$('.insertchat').live('click',function(e){
			var mns=$('#menssage').val();
			var persona=$('select[name=persona]').val();
		    var enviador='<?php echo $_SESSION['name'].' '.$_SESSION['apellido'];?>';
		var con="<?php echo $contacto;?>";
		var cel="<?php echo $celular;?>";
		$.post("<?php echo base_url(); ?>querys/insertdatachat.php",{ido:<?php echo $ido;?>,idh:<?php echo $idh;?>,mns:mns,con:con,celular:cel,persona:persona,enviador:enviador}, function(data) {
$.post("<?php echo base_url(); ?>querys/datachat.php",{ido:<?php echo $ido;?>}, function(data) {
	$('.vistachat').html(data);
	 });
	
	
	 });
		
		
		});
		
		
		$.post("<?php echo base_url(); ?>querys/datachat.php",{ido:<?php echo $ido;?>}, function(data) {
	$('.vistachat').html(data);
	 });
		
		
		$.post("<?php echo base_url(); ?>querys/dataordendecompra.php",{ido:<?php echo $ido;?>,page:'seguimiento'}, function(data) {
	$('.todoavisodepedido').html(data);
	 });
		
		 $('#FileUploader').on('submit', function(e)
    {
		
        e.preventDefault();
        $('#uploadButton').attr('disabled', ''); // disable upload button
        //show uploading message
        
        $("#output").html('<div style="padding:10px"><img src="<?php echo base_url();?>img/ajax-loader.gif" alt="Please Wait"/> <span>Uploading...</span></div>');
		
        $(this).ajaxSubmit({target: '#output',
        success:  afterSuccess});
        
        	 
        
    });
		
		
		});
		
		function afterSuccess()
{
    $('#FileUploader').resetForm();  // reset form
    $('#uploadButton').removeAttr('disabled'); //enable submit button
    $('.arbol').load('<?php echo base_url();?>arbol.php?idc=<?php echo $idc;?>');
}

	function disp_confirm() {
		
var r=confirm("Esta seguro de elimira este archivo ?");
if (r==true)
  {
var idfile=$('.eliminarfoto').attr('id');
$.post("<?php echo base_url(); ?>querys/deletearchivo.php",{idfiel:idfile}, function(data) { 
$('.arbol').load('<?php echo base_url();?>arbol.php?idc=<?php echo $idc;?>');
document.getElementById('#framepdf').contentDocument.location.reload(true);
$(".eliminarfoto").remove();  		  
$(".editarfoto").remove();
});
}else {} }	
		
		
		</script> 

    </body>
</html>