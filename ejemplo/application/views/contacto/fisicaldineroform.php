	

<div style="font-size:1.1em;">
<div class="row-fluid" style="max-width:800px; margin:0 auto; position:relative;">


<div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Complete la informaci&oacute;n Formato Lavado de Dinero para Personas F&iacute;sicas. </h3>
                                           
                                        </div>
                                        <div class="box_a_content no_sp">
 <form name="form" method="get" action="<?php echo base_url();?>index.php/contacto/fisicaldinero/<?php echo $id;?>/" target="_blank">                                          
<span style="font-size:11">Este formato cumple con lo establecido por la LFPIORPI</span><br>
<table  width="100%" align="center" bgcolor="#e41937" style="color:white"><tr><td>
Formato de Identificaci&oacute;n del cliente persona f&iacute;sica
</td></tr></table>
<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td style=" height:18px;">
Por este medio proporciono los datos y documentos requeridos con la &uacute;nica finalidad de identificarme
</td></tr></table>
<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr bgcolor="#dddddd" style="font-size:11px;" >
<td width="10%" style=" height:18px;" >
Fecha:
</td>
<td width="90%"><?php echo date('d / m / Y');?></td></tr></table>
<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="4"></td></tr>
<tr bgcolor="#e7e7e7" style="font-size:11px;">
<td width="25%" style="font-size:11px;height:18px;" >Nombre Completo del Cliente</td>
<td width="25%"><input type="text" value="<?php echo utf8_decode($OD[0]->datc_apellidopaterno);?>" name="apaterno" readonly="readonly"></td>
<td width="25%"><input type="text" value="<?php echo utf8_decode($OD[0]->datc_apellidomaterno);?>"  name="amaterno" readonly="readonly"></td>
<td width="25%"><input type="text" value="<?php echo utf8_decode($OD[0]->datc_primernombre.' '.$OD[0]->datc_segundonombre);?>"  name="nombres" readonly="readonly"></td>
</tr>
<tr bgcolor="#e7e7e7" style="font-size:11px;">
<td width="25%">(SIN ABREVIATURA)</td>
<td width="25%">Apellido Paterno</td>
<td width="25%">Apellido Materno</td>
<td width="25%">Nombre(s)</td>
</tr>
</table>
<table  width="100%"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="3"></td></tr>
<tr  style="font-size:11px;">
<td width="25%" bgcolor="#f3f3f3" style="font-size:11px; height:18px;">Fecha de Nacimiento:<input type="text" name="fechanac" value="<?php echo $OD[0]->datc_fechanac;?>" readonly="readonly"></td>
<td width="37.5%" bgcolor="#dddddd">Pais de Nacimiento:<input type="text" name="pais"></td>
<td width="37.5%" bgcolor="#f3f3f3">Nacionalidad:<input type="text" name="nacionalidad"></td>
</tr>
<tr  style="font-size:11px;">
<td width="25%"  bgcolor="#dddddd"  style="height:18px;">R.F.C.:<input type="text" name="rfc" value="<?php echo $OD[0]->datc_rfc;?>" readonly="readonly"></td>
<td colspan="2" width="75%"  bgcolor="#e7e7e7">Curp:<input type="text" name="curp"></td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr bgcolor="#f3f3f3" style="font-size:11px;" >
<td width="30%"  style="height:18px;">
Actividad, Ocupaci&oacute;n o Profesi&oacute;n:
</td>
<td width="70%"><input type="text" name="actividad"></td></tr></table>

<table   width="100%"  >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:11px;" >
<td width="60%" bgcolor="#dddddd">
Tipo de Documento de Identificaci&oacute;n Oficial<br>
emitida por autoridad local y federal:<input type="text" name="identificacion">
</td>
<td width="40%" bgcolor="#e7e7e7">
N&uacute;mero<br> de Folio :<input type="text" name="folio">
</td></tr></table>

<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="4"></td></tr>
<tr bgcolor="#e7e7e7" style="font-size:11px;" >
<td width="34%" style="font-size:11px; height:18px;" align="left">Domicilio Particular</td>
<td width="33%" align="center"><input type="text" name="calle" value="<?php echo $OD[0]->datc_calle;?>" readonly="readonly"></td>
<td width="33%" align="center"><input type="text" name="numeroint"></td>
</tr>
<tr align="center" bgcolor="#e7e7e7" style="font-size:11px;">
<td colspan="2" width="67%">Calle, Avenida &oacute; v&iacute;a y n&uacute;mero exterior</td>
<td width="33%">N&uacute;mero interior</td>
</tr>
</table>


<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="3"></td></tr>
<tr  style="font-size:11px;">
<td width="25%" bgcolor="#f3f3f3" style="font-size:11px; height:18px;">Colonia:<input type="text" name="colonia" value="<?php echo $OD[0]->datc_colonia;?>" readonly="readonly"> </td>
<td width="37.5%" bgcolor="#dddddd">Delegaci&oacute;n/Municipio/Demarcacion Politica:<input type="text" name="delegacion" ></td>
<td width="37.5%" bgcolor="#f3f3f3">Pa&iacute;s:<input type="text" name="pais"></td>
</tr>
<tr  style="font-size:11px;">
<td width="25%"  bgcolor="#dddddd" style="height:18px;">Ciudad/Poblaci&oacute;n:<input type="text" name="ciudad" value="<?php echo $OD[0]->datc_ciudad;?>" readonly="readonly"></td>
<td width="37.5%"  bgcolor="#e7e7e7">Entidad Federativa/Estado:<input type="text" name="estado" value="<?php echo $OD[0]->datc_estado;?>" readonly="readonly"></td>
<td width="37.5%"  bgcolor="#e7e7e7">C.P.:<input type="text" name="cp"></td>
</tr>
</table>

<table  width="100%"   >
<tr bgcolor="#ffffff" style="font-size:4px;"><td colspan="4"></td></tr>
<tr align="left" bgcolor="#e7e7e7" style="font-size:11px;">
<td width="34%"  style="height:18px;">Tel&eacute;fono1:<input type="text" name="telefono" value="<?php echo $OD[0]->datc_tcasa;?>" readonly="readonly"></td>
<td width="33%">Celular:<input type="text" name="celular" value="<?php echo $OD[0]->datc_tcelular;?>" readonly="readonly"></td>
<td width="33%"><input type="text" name="ext"></td>
</tr>
<tr align="center" bgcolor="#e7e7e7" style="font-size:11px;">
<td width="34%">(Incluir Clave Lada/ Clave Internacional)</td>
<td width="33%">(Incluir Clave Lada/ Clave Internacional)</td>
<td width="33%">Extensi&oacute;n</td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr bgcolor="#dddddd" style="font-size:11px;" >
<td width="20%" style="height:18px;" >
Correo Electr&oacute;nico:<input type="text" name="email" value="<?php echo $OD[0]->datc_email;?>" readonly="readonly">
</td>
<td width="80%"></td></tr></table>
<table  width="100%" align="center"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:11;"><td>
Agrego a la presente copia simple legible de los siguientes documentos previamente cotejados
</td></tr></table>
<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="cidenti"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> Identificaci&oacute;n Oficial</td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="ccurp"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> CURP</td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="cdomi"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> Comprobante de domicilio</td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="crfc"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> RFC</td></tr>
</table>
<table  width="100%" align="center"  >
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td width="60%" style="font-size:11px;">
Manifiesto que he tenido a la vista los documentos originales para cotejo:<input type="text" name="funcionario">
</td>
<td width="40%">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td width="60%"></td>
<td width="40%" style="font-size:11px;">
Nombre y firma del funcionario de la agencia
</td>
</tr>
</table>
<table  width="100%" align="left"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#dddddd" style="font-size:11;"><td>
Refencias Personales y  Comerciales ( solo en caso de que los documentos originales tengan tachaduras o enmendaduras deber&aacute;n agregarce al expediente dos referencias personales y dos bancarias o comerciales que incluyan los siguientes datos, debidamente suscrita por quien otorga la refencia ).
</td></tr></table>

<table  width="100%"  >
<tr bgcolor="#ffffff" style="font-size:4px;"><td></td></tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:11px; height:18px;">
1.Nombre completo:<input type="text" name="unobnom">
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:11px;">
1.Nombre completo:<input type="text" name="unopnom">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:11px; height:18px;">
Direcci&oacute;n:<input type="text" name="unobdire">
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:11px;">
Direcci&oacute;n:<input type="text" name="unopdire">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:11px; height:18px;">
Tel&eacute;fono:<input type="text" name="unobtele">
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:11px;">
Tel&eacute;fono:<input type="text" name="unoptele">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:11px; height:18px;">
1.Nombre completo:<input type="text" name="dosbnom">
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:11px;">
1.Nombre completo:<input type="text" name="dospnom">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:11px; height:18px;">
Direcci&oacute;n:<input type="text" name="dosbdire">
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:11px;">
Direcci&oacute;n:<input type="text" name="dospdire">
</td>
</tr>
<tr bgcolor="#e6e6e6" style="font-size:11;">
<td bgcolor="#f3f3f3" width="50%" style="font-size:11px; height:18px;">
Tel&eacute;fono:<input type="text" name="dosbtele">
</td>
<td bgcolor="#e7e7e7" width="50%" style="font-size:11px;">
Tel&eacute;fono:<input type="text" name="dosptele">
</td>
</tr>
</table>

<table   width="100%" align="left" >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="cacto"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> El acto u operaci&oacute;n celebrada con la presente empresa automotriz sera&aacute; para beneficio propio y no tengo conocimiento de la existencia de algun proveedor de recursos, due&ntilde;o o beneficiario controlador</td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="cpersona"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> La persona o grupo de personas que ejercer&aacute;n los derechos de uso, goce, disfrute, aprovechamiento o disposicion del veh&iacute;culo objeto de la operacion son distintas al cliente (Due&ntilde;o Beneficiario)(Requisitar Informaci&oacute;n al reverso)</td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="crecursos"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> Los recursos para el acto u operaci&oacute;n son aportados por persona distinta al cliente("Proveedor de recursos") (Requisistar informacion ala reverso)</td></tr>
<tr  style="font-size:11px;" ><td width="10%"bgcolor="#dddddd" ><select name="cmani"><option value="Si">Si</option><option value="No">No</option></select></td>
<td width="90%" bgcolor="#f3f3f3"> Manifiesto bajo protesta de decir la verdad que no cuento con RFC debido a que:</td></tr>
</table>

<table   width="100%"  >
<tr bgcolor="#ffffff" style="font-size:4px;">
<td colspan="2"></td></tr>
<tr  style="font-size:11px;" >
<td width="70%"bgcolor="#f3f3f3" style="height:18px;" >
Raz&oacute;n Social de la Distribuidora:<input type="text" name="razondis">
</td>
<td rowspan="2" bgcolor="#ffffff" width="30%" align="center">
<img src="<?php echo base_url(); ?>img/topleft.jpg">
</td></tr>
<tr  style="font-size:11px;" >
<td width="70%" align="center" bgcolor="#e7e7e7" >
Declaro bajo protesta decir la verdad que todos y cada uno de los datos proporcionados son verdaderos<br> 
<input type="text" name="declaro">
<br>

*Datos y/o documentos obligatorios
</td></tr>
</table>
<span style="font-size:11px;">Formato de Identificaci&oacute;n Persona F&iacute;sica(Anverso)</span>

<input align="right" type="submit" value="Imprimir PDF">
</form>
                                        </div>
                                    </div>

    

</div>	</div>