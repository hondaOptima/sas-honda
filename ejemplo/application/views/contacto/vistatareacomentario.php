<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="javascript:history.go(-1)">Bitacora</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Editar comentario</a></li>
                                                                       
                                </ul>
                          </section>


                       
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Editar comentario</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
  <?php if($todo_tareas): ?><?php foreach($todo_tareas as $tod): ?> 
  <?php
  $ida=$tod->act_IDactividades;
  $tit=$tod->act_titulo;
  $sta=$tod->act_status;
   $fei=$tod->act_fecha_inicio;
    $fef=$tod->act_fecha_fin;
	$des=$tod->act_descripcion;
	$not=$tod->act_notificar;
	
$hr=$tod->act_hora;
if($hr <10){$hr='0'.$hr;}
$mn=$tod->act_min;
if($mn==0){$mn='00';}
	$hora=$hr.':'.$mn;
	
  
  ?>        
   <?php endforeach ?><?php else: ?>None<?php endif ?>	 
   
   <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>                            
     
     
  <?php echo form_open('contacto/updatecomentario/'.$ida.'/'.$idc.'/'.$idb.'','class="form-horizontal"'); ?>
							
                            <fieldset>   
                            
                           
                      
      <br>                      
                            
                            
     
         
   <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'titulo',
              'id'          => 'titulo',
              'value'       => $tit,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                    
                                   

								  </div>
								</div>
							  </div>      
         
         
        
         
        
         
         
         <div class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripci&oacute;n:</label>
							  <div class="controls">
								
                                 <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => '',
              'value'       => $des,
              'maxlength'   => '',
              'row'        => '',
              'style'       => 'width:237px; height:75px;',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
         
         
         
     

         
         
         <div class="form-actions">
            <?php echo anchor("contacto/deletetareas/".$idc."/".$ida."", ' <div class="btn btn-small btn-danger">Eliminar</div>',array('onClick' => "return confirm('Esta seguro que desea eliminar el comentario ?')",'class'=>"")); ?> 
         
 
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         
         
         
 
 
   </fieldset>
						<?php echo form_close(); ?>            

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 


    </body>
</html>