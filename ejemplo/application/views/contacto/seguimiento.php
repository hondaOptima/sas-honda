<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="sventas"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Facturaci&oacute;n</a></li>
                                                                       
                                </ul>
                          </section>

 
                   <br><br>
                    <!-- jPanel sidebar -->
                     
<?php echo $flash_message; ?>
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Facturaci&oacute;n</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                            <table id="foo_example" class="table table-striped table-condensed grid">
                                                <thead>
							  <tr>
                              <th data-class="expand" >Ciudad</th>
                                  <th data-class="expand" >Mes</th>
								  <th data-class="expand" >Contacto</th>
                                  <th data-class="expand" >Cliente</th>
                                  <th data-class="expand" >Asesor</th>
                                  <th data-hide="phone," >Auto</th>
                                  <th data-hide="phone">Vin</th>
                                  <th data-hide="phone"  >F&I</th>
								  <th data-hide="phone," >Gerente</th>
								  <th data-hide="phone,tablet">Factu..</th>
                                  <th data-hide="phone,tablet" >Asesor</th>
								  <th data-hide="phone,tablet" >Conta..</th>
								  <th data-hide="phone,tablet" > Entrega</th>
								  <th data-hide="phone" width="100px;" >Acciones</th>
                              </tr>
						  </thead>   
						  <tbody>
                           <?php  
						  foreach($todo_procesodeventa as $tod){ 
						  
							  
						echo '	<tr>
						<td>'.$tod->hus_ciudad.'</td>
								<td>';
								$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

								if($tod->dat_fecha_facturacion == '0000-00-00')
								{
									
									}
								else{
									list($anio,$mes,$dia)=explode('-',$tod->dat_fecha_facturacion);
									echo  $meses[date($mes)-1];
									}
								echo'</td>
								<td >'.ucwords(strtolower($tod->con_titulo.' '.$tod->con_nombre.' '.$tod->con_apellido)).'</td>
								
								<td>';
								
								if($tod->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$tod->datc_primernombre.' '.$tod->datc_segundonombre.' '.$tod->datc_apellidopaterno.' '.$tod->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$tod->datc_moral; echo ucwords(strtolower($facturaNombre));}
								echo '</td>
								<td>'.ucwords(strtolower($tod->hus_nombre.' '.$tod->hus_apellido)).'</td>
								<td>'.ucwords(strtolower(substr($tod->data_auto,0,25))).'</td>
								<td>'.$tod->data_vin.'</td>
								<td class="center">
									<span class="label';
									if($tod->prcv_status_fi=='aprobado'){echo ' label-success';}
									else{echo ' label-warning';}
									echo'">'.substr($tod->prcv_status_fi,0,5).'</span>
									
								</td>
								<td class="center">
								
									<span class="label ';
									if($tod->prcv_status_gerente=='aprobado'){echo ' label-success';}
									else{echo ' label-warning';}
									echo' ">'.substr($tod->prcv_status_gerente,0,5).'</span>
								</td>
								<td class="center">
										<span class="label ';
									if($tod->prcv_prcv_status_credito_contado=='aprobado'){echo ' label-success';}
									else{echo ' label-warning';}
									echo'">'.substr($tod->prcv_prcv_status_credito_contado,0,5).'</span>
								</td>
								<td class="center">
										<span class="label ';
									if($tod->prcv_director_gral=='aprobado'){echo ' label-success';}
									else{echo ' label-warning';}
									echo'">'.substr($tod->prcv_director_gral,0,5).'</span>
								</td>
								<td class="center">
										<span class="label ';
									if($tod->prcv_status_contabilidad=='aprobado'){echo ' label-success';}
									else{echo ' label-warning';}
									echo'">'.substr($tod->prcv_status_contabilidad,0,5).'</span>
								</td>
								
									<td class="center"><span class="label ';
									if($tod->prcv_fechaentrega=='0000-00-00'){echo ' label-warning';}
									else{echo $tod->prcv_fechaentrega;}
									echo'">';
								if($tod->prcv_fechaentrega=='0000-00-00'){echo 'espe';}
								else{
								$month = array(0=>"","01"=>"Ene","02"=>"Feb", "03"=>"Mar", "04"=>"Abr", "05"=>"May", "06"=>"Jun", "07"=>"Jul", "08"=>"Ago", "09"=>"Sep", "10"=>"Oct", "11"=>"Nov", "12"=>"Dic");
								
									list($an,$ms,$di)=explode('-',$tod->prcv_fechaentrega);
									
									echo $di.'/'.$month[$ms];}
								echo'</span></td>
								
								<td class="center">
								
<a class="btn btn-small"  href="'.base_url().'index.php/contacto/detalleseguimiento/'.$tod->prcv_IDprocesodeventa.'">Ver</a>';  
										
 if($_SESSION['nivel']=='Administrador' ) {

echo anchor("/contacto/updateProcesodeVenta/$tod->prcv_IDprocesodeventa", 'Cancelar',array('onClick' => "return confirm('Esta seguro que desea cancelar venta:$tod->con_nombre $tod->con_apellido ?')",'class'=>"btn btn-small"));

//echo '<a class="btn btn-small"  href="'.base_url().'index.php/contacto/updateProcesodeVenta/'.$tod->prcv_IDprocesodeventa.'">Cancelar</a>';  
	 
if($tod->prcv_status_contabilidad=='aprobado' &&$tod->prcv_fechaentrega!='0000-00-00' ){ 
if(empty($tod->data_vin)){$vin=0;}else{$vin=$tod->data_vin;}
echo anchor("/contacto/convertirventa/$tod->prcv_IDprocesodeventa/$tod->bit_IDbitacora/$tod->con_IDcontacto/$vin", 'Entregar',array('onClick' => "return confirm('Esta seguro que desea convertir a venta:$tod->con_nombre $tod->con_apellido ?')",'class'=>"btn btn-small"));
  
}
 }
								
								echo '</td>
							</tr>';	  
							  
							  }
						  
						  ?>


</tbody>


</table>

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 

  <?php $this->load->view('globales/js'); ?> 
  <script type='text/javascript'>

	$(document).ready(function() {
	$('#foo_example').dataTable( {
		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
	
} );
</script>

    </body>
</html>
