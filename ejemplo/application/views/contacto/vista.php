<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
 <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Sistema de Prospecci&oacute;n Honda Optima.</a></li>
                                                                       
                                </ul>
                          </section>
                          
           

    <div class="row-fluid">
                                <div class="stat_boxes">
                                  
                                        <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user " style="color:orange;"></i></div>
                                            <h2 class="stat_title">
                                             <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Prospecto">
											<?php echo $prospectos->num;?></a></h2>
                                            <p class="stat_expl">Prospectos</p>
                                        </div>
                                       
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:#2cc0f0;"></i></div>
                                            <h2 class="stat_title">
                                            <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Caliente">
											<?php echo $calientes->num;?></a></h2>
                                            <p class="stat_expl">Calientes</p>
                                        </div>
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:green;"></i></div>
                                            <h2 class="stat_title">
                              <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Proceso%20de%20Venta">
											<?php echo $proceso->num;?></a></h2>
                                            <p class="stat_expl">En Proceso</p>
                                        </div>
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:red;" ></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Caida">
											<?php echo $caidas->num;?></a></h2>
                                            <p class="stat_expl">Ca&iacute;das</p>
                                        </div>
                                        
                                        <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:#365690;"></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Cliente">
											<?php echo $cliente->num;?></a></h2>
                                            <p class="stat_expl">Clientes</p>
                                        </div>
                                        
                                        <?php if(empty($_SESSION['coucheo'])){?>
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:#cccccc;"></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Todos">
											<?php echo $todos->num;?></a></h2>
                                            <p class="stat_expl">Todos</p>
                                        </div>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                       <div class="stat_boxes"><br>
                                    <div class="row-fluid">
                                       
<?php echo $flash_message ?>
               <?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion') {  ?>
<div class="span3 "><div class="control-group"><div class="controls">
 <?php echo form_dropdown('vendedor', $todo_vendedores,$id_vendedor,'style="width:246px;" id="s2_single" data-rel="chosen"');?>
</div></div></div>
 <?php }?>
</div></div>
                   
                    <!-- jPanel sidebar -->
                 
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3><?php 
											if($tipoconadmin=='Prospecto'){echo 'Prospectos';}
											if($tipoconadmin=='Caliente'){echo 'Calientes';}
											if($tipoconadmin=='Proceso%20de%20Venta'){echo 'Proceso de venta';}
											if($tipoconadmin=='Caida'){echo 'Ventas Ca&iacute;das';}
											if($tipoconadmin=='Cliente'){echo 'Clientes';}
											if($tipoconadmin=='Todos'){echo 'Todos los contactos';}
											
											?></h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        
<?php if($tipoconadmin=='Prospecto' or $tipoconadmin=='Caliente' or $tipoconadmin=='Caida' or $tipoconadmin=='Todos' && $ajax=='no') {?>                                        
 <table id="example" class="table  table-condensed">
                                                <thead>
							  <tr>
                               <th><div class="icon-globe"></div></th>
                                  <th data-class="expand">Contacto</th>
                                  <th data-hide="expand">Empresa</th> 
                                  <th data-hide="expand">Tel&eacute;fono</th> 
								  <th data-hide="phone,tablet" >Correo</th>
								  <th data-hide="phone,tablet" >Auto de Interes</th>
								  <th data-hide="phone,tablet" >Registro</th> 
								 <th data-hide="phone" >Sig. Actividad</th>
                                 <th data-hide="phone" >Comentario</th>
                                 <th data-hide="small" >Acciones</th>
                                 
                                
							  </tr>
						  </thead>   

</table>
<?php }  elseif($ajax=='no' && $tipoconadmin=='Proceso%20de%20Venta'){?>

 <table id="foo_example"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr><?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
                                  <th data-class="expand" >Asesor</th>  <?php } ?>
                                  <th data-class="expand"  >Contacto</th>
								  <th data-hide="phone"  >Modelo</th>
								  <th data-hide="phone">Color</th>
                                  <th data-hide="phone,tablet" >A&ntilde;o</th>
                                  <th data-hide="phone" >Vin</th>                                  
                                  <th data-hide="phone,tablet" >Inventario</th>
                                  <th data-hide="phone" >Operaci&oacute;n</th>  
                                  <th  data-hide="phone">Status</th>
								  <th  data-hide="phone,tablet">Comentario</th>
								  <th data-hide="phone">Revision</th>
								  <th data-class="small" >Acciones</th>
							  </tr>
						  </thead>
<tbody>
<?php if($todo_contacto){ foreach($todo_contacto as $todo): ?>
<?php 

$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);

$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto,$INFOAUTO->aau_IdFk);
?>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_nombre; ?></td>
<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_modelo));}
}
else{echo ucwords(strtolower($INFO->data_modelo));}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_color_exterior));}	
	}
else{echo ucwords(strtolower($INFO->data_color));}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_ano));}	
	}
else{echo $INFO->data_ano;}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_IdFk));}	
	}
else{echo $INFO->data_vin;}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower('Intellisis'));}
	}
else{echo 'Intellisis';}
?>
</td>
<td>
<?php 
if(empty($INFO)){}
else{
$fina=ucwords(strtolower($INFO->dats_compra));
if($fina=='Contado'){
	echo ucwords(strtolower($INFO->dats_compra));
	}
	else{
		echo ucwords(strtolower($INFO->dats_financiera));
		}
}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower('Auto Apartado'));}	
	}
else{
if($INFO->prcv_status_contabilidad=='aprobado'){echo 'Contabilidad';}
elseif($INFO->prcv_director_gral=='aprobado'){echo 'Asesror';}
elseif($INFO->prcv_prcv_status_credito_contado=='aprobado'){echo 'Facturacion';}	
elseif($INFO->prcv_status_gerente=='aprobado'){echo 'Gerente';}
elseif($INFO->prcv_status_fi=='aprobado'){echo 'F&I';}	
	}
?>
</td>
<td><?php
$coucheo=$this->Homemodel->lastActvCoucheo($todo->bit_IDbitacora);
if(empty($coucheo)){$nota='Sin Coucheo'; $fnota='';}
else{$nota=$coucheo->act_descripcion; $fnota=$coucheo->act_fecha_inicio; list($an,$msn,$din)=explode('-',$fnota);
$fnota=$din.'/'. $arrmes[$msn];}?>

<span title="<?php echo $nota;?>" class="label"><?php echo substr($nota,0,13);?></span>
</td>
<td><?php echo $fnota;?></td>
<td class="nolink">
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a"><i class="icon-book"></i></a>

<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>

<a title="Editar" href="<?php echo base_url()."index.php/contacto/update/$todo->con_IDcontacto";?>" class="sepV_a"><i class="icon-pencil"></i></a>

<a title="Eliminar"  href="<?php echo base_url()."index.php/contacto/delete/$todo->con_IDcontacto";?>" onClick="return confirm('Esta seguro que desea eliminar a:<?php echo $todo->con_nombre.' '.$todo->con_apellido; ?> ?')"><i class="icon-remove"></i></a>
<?php } ?>
</td>
</tr>
<?php endforeach; }?>
</tbody>
                            
                          </table>
                          
<?php }  elseif($ajax=='no' & $tipoconadmin=='Cliente'){?>

 <table id="example2"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr>
                                  <th style="width:190px" data-class="expand"  >Cliente</th>
								  <th data-hide="phone"  >Modelo</th>
								  <th data-hide="phone,tablet" >Color</th>
                                  <th data-hide="phone,tablet" >Año</th>
                                  <th data-hide="phone" >Vin</th>
                                  <th data-hide="phone" >Facturación</th>  
								  <th  data-hide="phone">Sig. Actividad</th>
                                  <th  data-hide="phone">Comentario</th>
								  <th style="width:50px;" data-class="small" >Acciones</th>
							  </tr>
						  </thead>
                          <tbody>

      
                          </tbody>
                            
                          </table>
                          <?php } ?>                          
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
   <?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>    
     
  <?php  $this->load->view('globales/jsC'); ?> 
  <?php }else{ ?>
  <?php $this->load->view('globales/js'); ?> 
  <?php } ?>

 
<script type='text/javascript'>

	$(document).ready(function() {
				
$('#s2_single').live('click',function(){	
var idh=$('select[name=vendedor]').val();	
window.location='<?php echo base_url(); ?>index.php/contacto/coucheo/'+idh;
});

$('#s23_single').live('click',function(){
		
var tipo=$('select[name=tipocontacto]').val();
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontactoadmin/'+tipo;
});

$('#s24_single').live('click',function(){
var tipo=$('select[name=tipocontacto]').val();	
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontacto/'+tipo;	
});
	});	
</script>

<script type='text/javascript'>

	$(document).ready(function() {
		
		$('#example2').dataTable( {
		
		"bProcessing": true,
		"sAjaxSource": "<?php echo base_url();?>querys/contactos/lista_clientes_asesor.php",
		
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example2_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );

		
	$('#example').dataTable( {
		
		"bProcessing": true,
		"sAjaxSource": "<?php echo base_url();?>querys/lista_contactos_tipos_asesor.php?tip=<?php echo $tipoconadmin;?>",
		
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );



$('#foo_example').dataTable( {
		
		
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );


	
	$('.dataTables_filter input').each(function() {
            $(this).attr("placeholder", "Buscar");
        });
} );
</script>

    </body>
</html>