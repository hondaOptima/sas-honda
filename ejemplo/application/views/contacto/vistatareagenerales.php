<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Bitacora"; $this->load->view('commons/head',$data); ?>  
       <style>
                    .ffa{font-size:.9em; font-weight:bold}
					 .ff{font-size:.9em; }
                    </style> 
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "contactos"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading"></h4>
					<p></p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				
 <ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/contacto">Contactos</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="javascript:window.history.go(-1);">Bitacora</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="">Tarea</a>
					</li>
                    
				</ul>
				
			</div>
		
			
			
			
			<hr>

  <?php if($uno_contacto): ?>
                          <?php foreach($uno_contacto as $todo): ?>
                           <?php
						    $nom=$todo->con_nombre; 
						    $ape=$todo->con_apellido; 
							$tof=$todo->con_telefono_officina;
							$tca=$todo->con_telefono_casa; 
							$cor=$todo->con_correo;
							$cob=$todo->con_correo_b; 
							$cal=$todo->con_calle;
							$col=$todo->con_colonia; 
							$ciu=$todo->con_ciudad;
							$cp=$todo->con_codigo_postal; 
							$ocu=$todo->con_ocupacion;
							$cum=$todo->con_cumpleanos;
							
							$fue=$todo->fue_nombre;
							$est=$todo->est_nombre;
							$hus=$todo->huser_hus_IDhuser; 
							
							$idc=$todo->con_IDcontacto;
							$sta=$todo->con_status;
							
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>			
	
<div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><?php echo $nom.' '.$ape.' ('.$sta.')';?></h2>
						<div class="box-icon">
						
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
					
						</div>
					</div>
					<div class="box-content" style="display:none;">
                    
                
                   
                  
                  <table>
                  
                  <tr>
                  <td width="150px"  class="ffa" >Nombre:</td>
                  <td width="150px" class="ff" ><?php echo $nom.' '.$ape;?></td>
                  <td width="150px" class="ffa" >Telefono Oficinas:</td>
                  <td width="150px" class="ff" ><?php echo $tof;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px" class="ffa" >Telefono Personal:</td>
                  <td width="150px"class="ff" ><?php echo $tca;?></td>
                  <td width="150px" class="ffa" >Correo Electronico:
:</td>
                  <td width="150px" class="ff" ><?php echo $cor;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px"class="ffa" > Correo Electronico alternativo:</td>
                  <td width="150px"  class="ff"><?php echo $cob;?></td>
                  <td width="150px" class="ffa" >Ocupacion:</td>
                  <td width="150px" class="ff" ><?php echo $ocu;?></td>
                  </tr>
                  
                  <tr>
                  <td width="150px" class="ffa" > Cumpleaños:</td>
                  <td width="150px" class="ff" ><?php echo $cum;?></td>
                  <td width="150px" class="ffa" >Fuente:</td>
                  <td width="150px" class="ff" ><?php echo $fue;?></td>
                  </tr>
                  
                   <tr>
                  <td width="150px" class="ffa" >Tipo de Contacto:</td>
                  <td width="150px" class="ff"><?php echo $sta;?></td>
                
                  </tr>
                  
                
                  
                  
                  </table>
                  
      
								 <hr/>
								     
                   <table>
                  
                  <tr>
                  <td width="150px"  class="ffa" >Calle:</td>
                  <td width="150px" class="ff" ><?php echo $cal;?></td>
                  <td width="150px" class="ffa" >Colonia:</td>
                  <td width="150px" class="ff" ><?php echo $col;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px" class="ffa" >Ciudad:</td>
                  <td width="150px"class="ff" ><?php echo $ciu;?></td>
                  <td width="150px" class="ffa" >Codigo Postal:
:</td>
                  <td width="150px" class="ff" ><?php echo $cp;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px"class="ffa" > Estado:</td>
                  <td width="150px"  class="ff"><?php echo $est;?></td>
                
                  </tr>
                  
                  
                
                  
                  
                  </table>
                                    
                                    
                                    
								  </div>
								 
								</div>
							  </div>
                              
                                        
                             
                             
                              
                            
                              

  
   <a href="javascript:window.history.go(-1);"><button class="btn btn-small btn-success">Regresar</button></a>
  
  
<div class="row-fluid sortable"><div class="box span12"><div class="box-header" data-original-title>
<h2><i class="icon-edit"></i><span class="break"></span>Tareas Generales
 	   
</h2>
<div class="box-icon"><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a></div></div>
<div class="box-content">
  <?php if($todo_tareas_generales): ?><?php foreach($todo_tareas_generales as $tod): ?> 
  <?php
  $ida=$tod->gen_IDgenerales;
  $tit=$tod->gen_titulo;
  $sta=$tod->gen_status;
   $fei=$tod->gen_fecha_inicio;
    $fef=$tod->gen_fecha_fin;
	$des=$tod->gen_descripcion;
	
	$hr=$tod->gen_hora;
if($hr <10){$hr='0'.$hr;}
$mn=$tod->gen_min;
if($mn==0){$mn='00';}
	$hora=$hr.':'.$mn;

  
  ?>        
   <?php endforeach ?><?php else: ?>None<?php endif ?>	 
   
   <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>                            
     
     
  <?php echo form_open('contacto/updatetareagenerales/'.$ida.'/'.$idc.'','class="form-horizontal"'); ?>
							'
                            <fieldset>   
                            
  <div class="control-group">
								<label class="control-label" for="selectErrorfxx">Categoria</label>
								<div class="controls">
                                
                                <?php
                              
								 echo form_dropdown('categoria', $todot_oportunidades,'','style="width:246px;" id="selectErrorfxx" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>                       
                            
                            
                            
     
         
   <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'titulo',
              'id'          => 'titulo',
              'value'       => $tit,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                    
                                   

								  </div>
								</div>
							  </div>      
         
         
          <div class="control-group">
							  <label class="control-label" for="date01">Fecha Inicio:</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 list($an,$me,$di)=explode('-',$fei);
								 $fcu=$me.'/'.$di.'/'.$an;
								 $datai= array(
              'name'        => 'fei',
              'id'          => 'fei',
			  'class'       => 'input-xlarge datepicker',
              'value'       => $fcu,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
         
         
         
          <div class="control-group">
							 	<label class="control-label" for="selectErrorkk">Hora</label>
								<div class="controls">
                              
                                 <?php
                                $options = array(
								 
                                 '24:00'  => '12:00 am',
                                 '24:30'    => '12:30 am',
								 '01:00'    => '1:00 am',
								 '01:30'  => '1:30 am',
                                 '02:00'    => '2:00 am',
								 '02:30'    => '2:30 am',
								 '03:00'  => '3:00 am',
                                 '03:30'    => '3:30 am',
								 '04:00'    => '4:00 am',
								 '04:30'  => '4:30 am',
								 '05:00'    => '5:00 am',
								 '05:30'  => '5:30 am',
                                 '06:00'    => '6:00 am',
								 '06:30'    => '6:30 am',
								 '07:00'  => '7:00 am',
                                 '07:30'    => '7:30 am',
								 '08:00'    => '8:00 am',
								 '08:30'  => '8:30 am',
                                 '09:00'    => '9:00 am',
								 '09:30'    => '9:30 am',
								  '10:00'  => '10:00 am',
                                 '10:30'    => '10:30 am',
								 '11:00'    => '11:00 am',
								 '11:30'  => '11:30 am',
                                 '12:00'    => '12:00 pm',
								 '12:30'    => '12:30 pm',
								  '13:00'    => '1:00 pm',
								 '13:30'  => '1:30 pm',
                                 '14:00'    => '2:00 pm',
								 '14:30'    => '2:30 pm',
								 '15:00'  => '3:00 pm',
                                 '15:30'    => '3:30 pm',
								 '16:00'    => '4:00 pm',
								 '16:30'  => '4:30 pm',
								 '17:00'    => '5:00 pm',
								 '17:30'  => '5:30 pm',
                                 '18:00'    => '6:00 pm',
								 '18:30'    => '6:30 pm',
								 '19:00'  => '7:00 pm',
                                 '19:30'    => '7:30 pm',
								 '20:00'    => '8:00 pm',
								 '20:30'  => '8:30 pm',
                                 '21:00'    => '9:00 pm',
								 '21:30'    => '9:30 pm',
								  '22:00'  => '10:00 pm',
                                 '22:30'    => '10:30 pm',
								 '23:00'    => '11:00 pm',
								 '23:30'  => '11:30 pm',
								 );
								 echo form_dropdown('hora', $options,$hora,'style="width:246px;" id="selectErrorkk" data-rel="chosen"');
								?>
                              
								
							  </div>
							</div>
         
         
         
         
         
          <div class="control-group">
							  <label class="control-label" for="date01">Fecha Fin:</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 list($an,$me,$di)=explode('-',$fef);
								 $fcuf=$me.'/'.$di.'/'.$an;
								 $datai= array(
              'name'        => 'fef',
              'id'          => 'fef',
			  'class'       => 'input-xlarge datepicker',
              'value'       => $fcuf,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
         
         
         <div class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripcion:</label>
							  <div class="controls">
								
                                 <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => 'cleditor',
              'value'       => $des,
              'maxlength'   => '',
              'row'        => '',
              'style'       => '',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
         
         
         
          <div class="control-group">
								<label class="control-label" for="selectError">Estado</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
								 ''  => 'Seleccione una opcion',
                                 'pendiente'  => 'Pendiente',
                                 'realizada'    => 'Realizada',
								 'proceso'    => 'En Proceso',
								 );
								 echo form_dropdown('status', $options,$sta,'style="width:246px;" id="selectError" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
         
         
         
         <div class="form-actions">
         <?php echo anchor("contacto/deletegenerales/".$idc."/".$ida."", ' <div class="btn btn-small btn-danger">Eliminar</div>',array('onClick' => "return confirm('Esta seguro que decea eliminar esta Tarea ?')",'class'=>"")); ?> 
         
 
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         
         
         
         
         
         
         
         
         
         
         
 
 
   </fieldset>
						<?php echo form_close(); ?>                                   
                                    
</div></div> </div>                   
                           
  
                           
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
	

</body>
</html>
