<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Bitacora"; $this->load->view('commons/head',$data); ?>  
       <style>
                    .ffa{font-size:.9em; font-weight:bold}
					 .ff{font-size:.9em; }
                    </style> 
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "contactos"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading"></h4>
					<p></p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				
 <ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/contacto">Contactos</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="#">Bitacora</a>
					</li>
                    
				</ul>
				
			</div>
		
			
			
			
			<hr>
       <?php echo $flash_message; ?>     

  <?php if($uno_contacto): ?>
                          <?php foreach($uno_contacto as $todo): ?>
                           <?php
						    $nom=$todo->con_nombre; 
						    $ape=$todo->con_apellido; 
							$tof=$todo->con_telefono_officina;
							$tof2=$todo->con_telefono_off;
							$tof3=$todo->con_telefono_off2;
							$tca=$todo->con_telefono_casa; 
							$cor=$todo->con_correo;
							$cob=$todo->con_correo_b; 
							$rad=$todo->con_radio; 
							
							$fue=$todo->fue_nombre;
							
							$hus=$todo->huser_hus_IDhuser; 
							
							$idc=$todo->con_IDcontacto;
							$sta=$todo->con_status;
							$mod=$todo->con_modelo;
							$ano=$todo->con_ano;
							$col=$todo->con_color;
						   $tip=$todo->con_tipo;
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>			
	
<div class="row-fluid sortable">
 
 <div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="fa-icon-user btn-minimize"></i><span class="break table-striped"></span><?php echo $nom.' '.$ape.' ('.$sta.') - '.$mod.' '.$ano.' '.$col.' ['.$tip.']';?></h2>
						<div class="box-icon">
						
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
					
						</div>
					</div>
					<div class="box-content" style="display:none;">
                    
                
                   
                  <input type="hidden" name="idc" value="<?php echo $idc;?>">
                  <table>
                  
                  <tr>
                  <td width="150px"  class="ffa" >Nombre:</td>
                  <td width="150px" class="ff" ><?php echo $nom.' '.$ape;?></td>
                  <td width="150px" class="ffa" >Correo Electronico:</td>
                  <td width="150px" class="ff" ><?php echo $cor;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px" class="ffa" >Correo Electronico alternativo:</td>
                  <td width="150px"class="ff" ><?php echo $cob;?></td>
                  <td width="150px" class="ffa" >Celular: </td>
                  <td width="150px" class="ff" ><?php echo $tca;?></td>
                  </tr>
                  
                  
                  <tr>
                  <td width="150px"class="ffa" >Telefono Casa:</td>
                  <td width="150px"  class="ff"><?php echo $tof;?></td>
                  <td width="150px" class="ffa" >Telefono de Oficina</td>
                  <td width="150px" class="ff" ><?php echo $tof2;?></td>
                  </tr>
                  
                  <tr>
                  <td width="150px" class="ffa" >Telefono de Oficina 2</td>
                  <td width="150px" class="ff" ><?php echo $tof3;?></td>
                  <td width="150px" class="ffa" >Fuente:</td>
                  <td width="150px" class="ff" ><?php echo $fue;?></td>
                  </tr>
                  
                  
                
                  
                  
                  </table>
                  
                        
								  </div>
								 
								</div>
							 </div>
                              
                              
                   <a href="javascript:window.history.go(-1);"><button class="btn btn-small btn-success">Regresar</button></a>
                   
                  
                   
                   <?php echo '<input type="hidden" value="'.$IDOC.'" name="IDOC"">';?>              
          	<div class="row-fluid sortable">
	
				<div class="box span12">
					<div class="box-header">
						<h2><i class="icon-th table-striped"></i>Orden de Compra</h2>
					</div>
					<div class="box-content">
						<ul class="nav tab-menu nav-tabs" id="myTab">
							<li class="active"><a href="#info">Datos</a></li>
							<li><a href="#custom">Aviso de Pedido</a></li>
							
						</ul>
						 
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane active" id="info">
								<p>

<table class="table table-striped"><tbody><tr>
<td>Datos Completos: Cliente ,Financiamiento , Compra Auto, Toma Seminueva</td>
<td></tr></tbody></table>
                                
                                
   <div class="row-fluid">  
   
       
							  <div class="span4">
								<h6>Fecha de Facturacion:</h6>
								<p>
                                <input id="datos" name="dat_fecha_facturacion" type="text">
                         
								</p>
								
							  </div>
							<!--  <div class="span4">
								<h6>Fecha de Entrega:</h6>
								<p><input  id="datos" name="dat_fecha_entrega"  type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Garantia Apartir de:</h6>
								<p><input  id="datos" name="dat_garantia"  type="text"></p>
							  </div>
                              -->
						  </div>
                          <hr/>
                             <div class="row-fluid">  
                              <div class="span4">
								<h6>Personal Fisica:</h6>
								<p>
								<input type="radio" checked="checked" class="radiobutton"  id="datos_cliente" style="margin-left:5px;"
                                 value="fisica" name="datc_persona_fisica_moral">
								</p>
								
                                
                                
							  </div>
                              
							  <div class="span4">
								<h6>Persona Moral:</h6>
								<p>
                                <input type="radio" value="moral" class="radiobutton" id="datos_cliente" style="margin-left:5px;"
                                 name="datc_persona_fisica_moral">
                                </p>
								
							  </div>
						  </div>
                          
                          
                          <div class="row-fluid perfis" id="">            
						
                             
                              <div class="span4">
								<h6>Parentesco:</h6>
								<p><input id="datos_cliente" name="datc_parentesco" class="keyupdatax" type="text" ></p>
								
							  </div>
                                
							  <div class="span4">
								<h6>Primer Nombre:</h6>
								<p><input id="datos_cliente" name="datc_primernombre" class="keyupdatax" type="text" value="<?php echo $nom;?>"></p>
								
							  </div>
							  <div class="span4">
								<h6>Segundo Nombre:</h6>
								<p>
								<input id="datos_cliente"  name="datc_segundonombre" class="keyupdatax" type="text">
								</p>
								
                                
                                
							  </div>
                              
							
						  </div>
                          
                          
                          
                           <div class="row-fluid perfis">  
                           
                             <div class="span4">
								<h6>Apellido Paterno:</h6>
								<p><input  id="datos_cliente" name="datc_apellidopaterno" class="keyupdatax" type="text" value="<?php echo $ape;?>"></p>
								
							  </div>          
							 
							  <div class="span4 ">
								<h6>Apellido Materno:</h6>
								<p><input  id="datos_cliente"  name="datc_apellidomaterno" class="keyupdatax"  type="text"></p>
							  </div>
						  </div>
                          
                          <div class="row-fluid permor" style="display:none">            
							 
							  <div class="span4 ">
								<h6>Nombre de La Empresa:</h6>
								<p><input  id="datos_cliente"  name="datc_nombrecompleto" class="keyupdatax"  type="text"></p>
							  </div>
						  </div>
                          
                          
                          <hr>
                          <b>Direcci&oacute;n </b>
                            <div class="row-fluid">            
							<!--  <div class="span4">
								<h6>Direccion Completo:</h6>
								<p>
								<input id="datos_cliente" name="datc_direccion" type="text"  class="input-xlarge keyupdatax ">
								</p>
								
                                
                                
							  </div>-->
							  <div class="span4">
								<h6>Calle , Numero:</h6>
								<p><input id="datos_cliente" name="datc_calle"  class="keyupdatax" type="text"></p>
								
							  </div>
							  <div class="span4">
								<h6>Colonia / Fraccionamiento:</h6>
								<p>
								<input id="datos_cliente" name="datc_colonia" class="keyupdatax"  type="text">
								</p>
								
                                
                                
							  </div>
                                <div class="span4">
								<h6>Ciudad:</h6>
								<p><input id="datos_cliente" name="datc_ciudad" class="keyupdatax"  type="text"></p>
								
							  </div>
						  </div>
                          
                          
                          
                           <div class="row-fluid">            
							 
							
							  <div class="span4 ">
								<h6>Estado:</h6>
								<p><input id="datos_cliente" name="datc_estado" class="keyupdatax"  type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Municipio:</h6>
								<p><input id="datos_cliente" name="datc_municipio" class="keyupdatax"  type="text"></p>
							  </div>
                            <div class="span4 ">
								<h6>Codigo Postal:</h6>
								<p><input id="datos_cliente" name="datc_cp" class="keyupdatax"  type="text"></p>
							  </div>
                          
						  </div>
                          <hr>
                          <b>Direcci&oacute;n de Facturacion.<br> Si desea utilizar la misma dirección del contacto seleccione la siguiente casilla<input type="checkbox" id="chadd" ></b>
               <br><br>           
                <div class="row-fluid">            
							<!--  <div class="span4">
								<h6>Direccion Completo:</h6>
								<p>
								<input id="datos_cliente" name="datc_direccion" type="text"  class="input-xlarge keyupdatax ">
								</p>
								
                                
                                
							  </div>-->
							  <div class="span4">
								<h6>Calle , Numero:</h6>
								<p><input id="dirfacturacion" name="dfa_calle"  class="keyupdatax" type="text"></p>
								
							  </div>
							  <div class="span4">
								<h6>Colonia / Fraccionamiento:</h6>
								<p>
								<input id="dirfacturacion" name="dfa_colonia" class="keyupdatax"  type="text">
								</p>
								
                                
                                
							  </div>
                                <div class="span4">
								<h6>Ciudad:</h6>
								<p><input id="dirfacturacion" name="dfa_ciudad" class="keyupdatax"  type="text"></p>
								
							  </div>
						  </div>
                          
                          
                          
                           <div class="row-fluid">            
							 
							
							  <div class="span4 ">
								<h6>Estado:</h6>
								<p><input id="dirfacturacion" name="dfa_estado" class="keyupdatax"  type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Municipio:</h6>
								<p><input id="dirfacturacion" name="dfa_municipio" class="keyupdatax"  type="text"></p>
							  </div>
                            <div class="span4 ">
								<h6>Codigo Postal:</h6>
								<p><input id="dirfacturacion" name="dfa_codigopostal" class="keyupdatax"  type="text"></p>
							  </div>
                          
						  </div>
                          <br>
                       
                       <hr>   
                          <div class="row-fluid">            
							 
							  <div class="span4">
								<h6>RFC Completo:</h6>
								<p><input id="datos_cliente" name="datc_rfc" class="keyupdatax"  type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Telefono de Casa:</h6>
								<p><input id="datos_cliente" value="<?php echo $tof;?>" name="datc_tcasa" class="keyupdatax"  type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Celular:</h6>
								<p><input id="datos_cliente"   value="<?php echo $tca;?>" name="datc_tcelular" class="keyupdatax"  type="text"></p>
							  </div>
                          
                          
						  </div>
                          
                          <div class="row-fluid">            
							 
							  <div class="span4">
								<h6>Telefono de Oficina:</h6>
								<p><input  id="datos_cliente" value="<?php echo $tof2;?>" name="datc_oficinia" class="keyupdatax"  type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Fax:</h6>
								<p><input id="datos_cliente" name="datc_fax" class="keyupdatax"  type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Radio:</h6>
								<p><input  id="datos_cliente" value="<?php echo $rad;?>" name="datc_radio" class="keyupdatax"  type="text"></p>
							  </div>
                          
						  </div>
                          
                          <div class="row-fluid">            
							 
							  <div class="span4">
								<h6>Correo Electronico:</h6>
								<p><input id="datos_cliente" name="datc_email" value="<?php echo $cor;?>"  class="keyupdatax"  type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Fecha de Nacimiento:</h6>
								<p><input id="datos_cliente" name="datc_fechanac"  class="keyupdatax"  type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Aniversario:</h6>
								<p><input id="datos_cliente" name="datc_aniversario" class="keyupdatax"  type="text"></p>
							  </div>
                          
                          
						  </div>
                          
                          <hr/>
                          
                          <div class="row-fluid">            
							 <div class="span4">
								<h6>Auto(Marca,Modelo,Version,Tipo,Año):</h6>
								<p><input id="datos_auto" name="data_auto" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Tipo de Carroceria:</h6>
								<p><input id="datos_auto" name="data_carroceria" type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Capacidad:</h6>
								<p><input id="datos_auto" name="data_capacidad" type="text"></p>
							  </div>
                           </div>
                           
                           
                           <div class="row-fluid">            
							 <div class="span4">
								<h6>No. de Placas:</h6>
								<p><input  id="datos_auto" name="data_placas" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>KM/Millas:</h6>
								<p><input id="datos_auto" name="data_km" type="text"></p>
							  </div>
                           </div>
                           
                           
                           <div class="row-fluid">            
							 <div class="span4">
								<h6>Marca:</h6>
								<p><input id="datos_auto" name="data_marca" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Modelo:</h6>
								<p><input id="datos_auto" name="data_modelo" type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Version:</h6>
								<p><input id="datos_auto" name="data_version" type="text"></p>
							  </div>
                           </div>
                           
                           <div class="row-fluid">            
							 <div class="span4">
								<h6>Año:</h6>
								<p><input id="datos_auto" name="data_ano" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Color Exterior:</h6>
								<p><input id="datos_auto" name="data_color" type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>Motor y Transmision:</h6>
								<p><input id="datos_auto" name="data_motor" type="text"></p>
							  </div>
                           </div>
                          
                          <div class="row-fluid">            
							 <div class="span4">
								<h6>No. Motor:</h6>
								<p><input id="datos_auto" name="data_no_motor" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Codigo de Llaves:</h6>
								<p><input id="datos_auto" name="data_llaves" type="text"></p>
							  </div>
						  <!-- <div class="span4 ">
								<h6>Pedimento de Importacion:</h6>
								<p><input id="datos_auto" name="data_importacion" type="text"></p>
							  </div>
                              -->
                              	 <div class="span4">
								<h6>No. Serie / VIN:</h6>
								<p><input id="datos_auto" name="data_vin" type="text"></p>
								
							  </div>
                           </div>
                          
                          <div class="row-fluid">            
						
							  <div class="span4 ">
								<h6>Cod. de Radio:</h6>
								<p><input id="datos_auto" name="data_codigoderadio" type="text"></p>
							  </div>
                           </div>
                          
                          <hr/>
                          
                          <div class="row-fluid">            
							 <div class="span4">
								<h6>Contado / Financiamiento:</h6>
								<p><input id="datos_seguro" name="dats_compra" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Financiera:</h6>
								<p><input id="datos_seguro" name="dats_financiera" type="text"></p>
							  </div>
						   <div class="span4 ">
								<h6>&nbsp;</h6>
                                <p><h6>Financiado Anual:<input id="datos_seguro" name="dats_financiadoanual" type="checkbox"></h6></p>
								
							  </div>
                           </div>
                           
                           <div class="row-fluid">            
							 <div class="span4">
								<h6>Financiado Multianual:<input id="datos_seguro" name="dats_financiadomultianual" type="checkbox"></h6>
								
								
							  </div>
							  <div class="span4 ">
								<h6>Contado Anual:<input id="datos_seguro" name="dats_contadoanual" type="checkbox"></h6>
								
							  </div>
						   <div class="span4 ">
								<h6>Contado Multianual:<input id="datos_seguro" name="dats_contadomultianual" type="checkbox"></h6>
							  </div>
                           </div>
                           
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Financiado Semestral:<input id="datos_seguro" name="dats_financiadosemestral" type="checkbox"></h6>
								</div>
							  <div class="span4 ">
								<h6>Contado Semestral:<input id="datos_seguro" name="dats_contadosemestral" type="checkbox"></h6>
								</div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>No. Factura:</h6>
								 <p><input id="datos_seguro" name="dats_factura" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Tipo de Moneda:</h6>
                                <p><input id="datos_seguro" name="dats_tipomoneda" type="text"></p>
								
							  </div>
						   <div class="span4 ">
								<h6>Valor de Factura:</h6>
                                 <p><input id="datos_seguro" name="dats_valorfactura" type="text"></p>
							  </div>
                           </div>
                           
                             <div class="row-fluid">            
							 <div class="span4">
								<h6>Cantidad en Letra:</h6>
								 <p><input id="datos_seguro" name="dats_cantidadletra" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>% de Enganche:</h6>
                                <p><input id="datos_seguro" name="dats_enganche" type="text"></p>
								
							  </div>
						   <div class="span4 ">
								<h6>Monto:</h6>
                                 <p><input id="datos_seguro" name="dats_monto" type="text"></p>
							  </div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Tipo de Cambio:</h6>
								 <p><input id="datos_seguro" name="dats_tipocambio" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>Tasa:</h6>
                                <p><input id="datos_seguro" name="dats_taza" type="text"></p>
								
							  </div>
						   <div class="span4 ">
								<h6>Plazo:</h6>
                                 <p><input id="datos_seguro" name="dats_plazo" type="text"></p>
							  </div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Mensualidad:</h6>
								 <p><input id="datos_seguro" name="dats_mensualidad" type="text"></p>
								
							  </div>
							  <div class="span4 ">
								<h6>No. de Pagares:</h6>
                                <p><input id="datos_seguro" name="dats_pagaresnum" type="text"></p>
								
							  </div>
						   <div class="span4 ">
								<h6>Pagares / Cantida a Pagar:</h6>
                                 <p><input  id="datos_seguro" name="dats_pagarescant" type="text"></p>
							  </div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Pagares / Vencimiento de los Dias:</h6>
								 <p><input id="datos_seguro" name="dats_pagaresvencimiento" type="text"></p>
								
							  </div>
                           </div>
                           
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Caic:<input id="datos_seguro" name="dats_tipocaic" type="checkbox"></h6>
								
								
							  </div>
							  <div class="span4 ">
								<h6>GNP:<input id="datos_seguro" name="dats_tipognp" type="checkbox"></h6>
                                </div>
                                
						   <div class="span4 ">
								<h6>AIG:<input id="datos_seguro" name="dats_tiipoaig" type="checkbox"></h6>
							  </div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>.: Solicitud de instalacion de accesorios genuinos honda</h6>
								 </div>
							   </div>
                           
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Incluir en la Facturacion del Auto:<input id="datos_seguro" name="dats_accfactura" type="checkbox"></h6>
								</div>
							  <div class="span4 ">
								<h6>Factura por Mostrador (Refacciones):<input id="datos_seguro" name="dats_mosrefacc" type="checkbox"></h6>
                             </div>
						 
                           </div>
                           
                           
                           <hr/>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>.:Datos de la Recompra:</h6>
								</div>
							 
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Marca:</h6>
                                <p><input id="datos_compra" name="datco_marca" type="text"></p>
								</div>
							  <div class="span4">
								<h6>Modelo:</h6>
                                <p><input id="datos_compra" name="datco_modelo" type="text"></p>
								 </div>
                                
						  <div class="span4">
								<h6>Version:</h6>
                                <p><input id="datos_compra" name="datco_version" type="text"></p>
								</div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Año:</h6>
                                <p><input type="text" id="datos_compra"  name="datco_ano"></p>
								</div>
							  <div class="span4">
								<h6>Color Exterior:</h6>
                                <p><input id="datos_compra" name="datco_color" type="text"></p>
								 </div>
                                
						  <div class="span4">
								<h6>No. de Compra:</h6>
                                <p><input  id="datos_compra" name="datco_nocompra" type="text"></p>
								</div>
                           </div>
                           
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>No. de Serie:</h6>
                                <p><input id="datos_compra" name="datco_noserie" type="text"></p>
								</div>
							
                           </div>
                           
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Garantias Autos: Nuevos,Demos y Seminuevos(Extencion o Certificado)</h6>
                               
								</div>
							  <div class="span4">
								<h6>Años:</h6>
                                <p><input id="datos_compra" name="datco_garantiaanos" type="text"></p>
								 </div>
                                
						  <div class="span4">
								<h6>Kilometraje:</h6>
                                <p><input id="datos_compra" name="datco_garacntiakm" type="text"></p>
								</div>
                           </div>
                           
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Demo:</h6>
                                <input id="datos_compra" name="datco_nuevo" type="checkbox">
								</div>
							  <div class="span4">
								<h6>Nuevo:</h6>
                                <p><input  id="datos_compra" name="datco_snuevo" type="checkbox"></p>
								 </div>
                                
						  <div class="span4">
								<h6>Seminuevo:</h6>
                                <p><input id="datos_compra" name="datco_nuevox" type="checkbox"></p>
								</div>
                           </div>
                           
                           <hr/>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>.:Documentos Recibidos:</h6>
                               </div> <div class="span4">
								<h6>Factura Original:<input id="datos_documentos_recibidos"   name="datdr_facoriginal" type="checkbox"></h6>
								</div>
							  <div class="span4">
								<h6>Copia de Factura:<input id="datos_documentos_recibidos"   name="datdr_coipafact" type="checkbox"></h6>
								 </div></div>
                           
                           <div class="row-fluid">            
							<div class="span4">
								<h6>Factura Accesorios:<input id="datos_documentos_recibidos"   name="datdr_facaccesorios" type="checkbox"></h6>
								</div> <div class="span4">
								<h6>Factura Placas:<input id="datos_documentos_recibidos"   name="datdr_facplacas" type="checkbox"></h6>
								</div>
							  <div class="span4">
								<h6>Carta Factura:<input id="datos_documentos_recibidos"   name="datdr_cartafact" type="checkbox"></h6>
								 </div>
                           </div>
                          
                           
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>.:Manual e Instructivos del Auto:</h6>
								</div>
							  <div class="span4">
								<h6>Manual de Servicio de Garantia:<input id="datos_documentos_recibidos"   name="datdr_manualgarantia" type="checkbox"></h6>
								 </div>
                                
						  <div class="span4">
								<h6>Manual de Usuario y Sist. SRS:<input id="datos_documentos_recibidos"   name="datdr_manual_deusuario" type="checkbox"></h6>
								</div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Poliza de Seguro Automotriz:<input id="datos_documentos_recibidos"   name="datdr_poliza" type="checkbox"></h6>
								</div>
							  <div class="span4">
								<h6>Manual de Seguro Automotriz:<input id="datos_documentos_recibidos"   name="datdr_msautomotriz" type="checkbox"></h6>
								 </div>
                                
						  <div class="span4">
								<h6>Manual de Asistencia Honda en Camino:<input id="datos_documentos_recibidos"   name="datdr_manualdeasistemcia" type="checkbox"></h6>
								</div>
                           </div>
                           
                            <div class="row-fluid">            
							 <div class="span4">
								<h6>Contrato de Adhesivo Profeco:<input id="datos_documentos_recibidos"   name="datdr_profeco" type="checkbox"></h6>
								</div> 
                           </div>
                               
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>.:Accesorios , Llaves y Herramientas del Auto:</h6>
								</div>
							  <div class="span4">
								<h6>Llave Principal:</h6>
                                <p><input  id="datos_documentos_recibidos"   name="datdr_llaveprincipal"type="text"></p>
								 </div>
                                
						  <div class="span4">
								<h6>LLave de Valet:</h6>
                                <p><input id="datos_documentos_recibidos"   name="datdr_llavevalet"  type="text"></p>
								</div>
                           </div>
                           
                           
                             <div class="row-fluid">            
							 <div class="span4">
								<h6>Con radio:</h6>
                                <p><input id="datos_documentos_recibidos"   name="datdr_codradio"  type="checkbox"></p>
								</div>
							  <div class="span4">
								<h6>Llanta de Refaccion:</h6>
                                <p><input id="datos_documentos_recibidos"   name="datdr_llantarefa" type="checkbox"></p>
								 </div>
                                
						  <div class="span4">
								<h6>Llaves con Control Remoto y Apertura de Puertas:</h6>
                                <p><input id="datos_documentos_recibidos"   name="datdr_llacontrolrepuerta" type="text"></p>
								</div>
                           </div>
                               
                               
                                 <div class="row-fluid">            
							 <div class="span4">
								<h6>Extintor:<input id="datos_documentos_recibidos"   name="datdr_extintor" type="checkbox"></h6>
								</div>
							  <div class="span4">
								<h6>Cables de Corriente:<input id="datos_documentos_recibidos"   name="datdr_cablecorr" type="checkbox"></h6>
								 </div>
                                
						  <div class="span4">
								<h6>Gato Hidraulico y su Herramienta:<input id="datos_documentos_recibidos"   name="datdr_gatohydraulico" type="checkbox"></h6>
								</div>
                           </div>
                           
                           
                             <div class="row-fluid">            
							 <div class="span4">
								<h6>Triangulo de Seguridad:<input id="datos_documentos_recibidos"   name="datdr_triangulodeseguridad" type="checkbox"></h6>
								</div>
							  <div class="span4">
								<h6>Paquete de Herramientas:<input id="datos_documentos_recibidos"   name="datdr_paqherra" type="checkbox"></h6>
								 </div>
                                 </div>
                                 
                                  <div class="row-fluid">            
							 <div class="span4">
								<h6>Asesor Profesional de Venta: Liusber Estrada</h6>
								</div>
							  
                                 </div>
                               
                               
                               
                          
                          
								</p>

							</div>
							<div class="tab-pane" id="custom">
								<p>
								
                                	 <div class="row-fluid">            
							 <div class="span4">
								<h6>Clave SICOP/NO.CTE:</h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							 <!-- <div class="span4">
								<h6>Fecha de Entrega:</h6>
                              <p><input type="text" readonly="readonly"></p>
								 </div>
                                 -->
                                  <div class="span4">
								<h6>Hora de Entrega:</h6>
                             <p><input type="text" readonly="readonly"></p>
								 </div>
                               </div>
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>Fecha de Facturacion:</h6>
                               <p><input type="text" readonly="readonly"></p>
								</div>
							 
                               </div>
                                 
                                 
                                 
                                  <div class="row-fluid">            
							 <div class="span4">
								<h6>Factura a:</h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							  <div class="span4">
								<h6>Domicilio:</h6>
                             <p><input type="text" readonly="readonly"></p>
								 </div>
                                  <div class="span4">
								<h6>Ciudad / Edo:</h6>
                               <p><input type="text" readonly="readonly"></p>
								 </div>
                               </div>
                               
                                  <div class="row-fluid">            
							 <div class="span4">
								<h6>RFC / CURP:</h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							  <div class="span4">
								<h6>Tel / Cel / Radio:</h6>
                             <p><input type="text" readonly="readonly"></p>
								 </div>
                                  <div class="span4">
								<h6>Correo Electronico:</h6>
                               <p><input type="text" readonly="readonly"></p>
								 </div>
                               </div>
                               
                               
                                 <div class="row-fluid">            
							 <div class="span4">
								<h6>Marca, Modelo, Version y Año:</h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							  <div class="span4">
								<h6>Color Exterior:</h6>
                             <p><input type="text" readonly="readonly"></p>
								 </div>
                                  <div class="span4">
								<h6>No Motor:</h6>
                               <p><input type="text" readonly="readonly"></p>
								 </div>
                               </div>
                               
                                 <div class="row-fluid">            
							 <div class="span4">
								<h6>No. Serie / Vin:</h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							  
                               </div>
                               
                                 <div class="row-fluid">            
							 <div class="span4">
								<h6>Tipo de Operacion </h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							  <div class="span4">
								<h6>Financiera:</h6>
                             <p><input type="text" readonly="readonly"></p>
								 </div>
                                  <div class="span4">
								<h6>Tipo de Moneda:</h6>
                               <p><input type="text" readonly="readonly"></p>
								 </div>
                               </div>
                               
                                 <div class="row-fluid">            
							 <div class="span4">
								<h6>Mensualida:</h6>
                                <p><input type="text" readonly="readonly"></p>
								</div>
							  <div class="span4">
								<h6>Plazo:</h6>
                             <p><input type="text" readonly="readonly"></p>
								 </div>
                                  <div class="span4">
								<h6>Tasa:</h6>
                               <p><input type="text" readonly="readonly"></p>
								 </div>
                               </div>
                               
                               
                                 <div class="row-fluid">
							  <div class="span12">
								
								  <div class="row-fluid">
									<div class="span6">
									
									  <blockquote>
										<p><h5>CONDICIONES GENERALES</h5></p>
										<table><tr><td><div class=" icon-check"></div>Honda Nuevo</td><td colspan="2"><center><h6>Garantias(auto,ext.,Certificado)</h6></center></td></tr>
                                       
                                        <tr><td>Honda Demos</td><td><center><input type="text" readonly="readonly" style="width:35px"></center></td><td><center><input type="text" readonly="readonly" style="width:35px"></center></td></tr>
                                        <tr><td>Honda Seminuevo</td><td><center><small>Años</small></center></td><td><center><small>Kilometros</small></center></td></tr>
                                        
                                        <tr><td colspan="3"><center><h6>DATOS DE LA RECOMPRA</h6></center></td></tr>
					
					<tr><td>Marca</td><td>Modelo</td><td>Version</td></tr>
					<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
					<tr><td colspan="3"></td></tr>
					
					<tr><td>Año</td><td>No. de Compra</td><td>No. Serie</td></tr>
					<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
                                        
                                        <tr><td colspan="3"><center><h5>Detalle de Pago (Anexar Soportes)</h5></center></td></tr>
										<tr><td>Fecha</td><td>Folio / Dcto</td><td>Importe</td></tr>
								      	<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
										<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
											<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
												<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
													<tr><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td><td><input type="text" readonly="readonly" style="width:50px"></td></tr>
														<tr><td colspan="2"><center><h5>Total Pagos</h5></center></td><td>000000</td></tr>
								      
								      </table>
									  </blockquote>
									</div>
									<div class="span6">
									  
									  <blockquote class="pull-right">
										<p><h5>DESGLOSE DE OPERACIONES</h5></p>
										<table>
                                        <tr><td>Precio de Venta de Auto:</td><td>00000</td></tr>
                                        <tr><td>Accesorios:</td><td>00000</td></tr>
                                        <tr><td>IVA:</td><td>00000</td></tr>
                                        <tr><td><h6>Valor de la Factura:</h6></td><td>00000</td></tr>
                                        <tr><td>Gastos Adicionales:</td><td>00000</td></tr>
                                        <tr><td>Recompra Usado:</td><td>00000</td></tr>
                                        <tr><td>Enganche o Completo:</td><td>00000</td></tr>
                                         <tr><td><h6>Total de Enganche /Inv. Inicia:</h6></td><td>00000</td></tr>
                                          <tr><td>Saldo a pagar por el Banco</td><td>00000</td></tr>
                                        <tr><td><h6>Total de la Operacion:</h6></td><td>00000</td></tr>
                                          <tr><td colspan="2"><center><h6>* Detalle de Gastos Adicionales:</h6></center></td></tr>
                                        <tr><td>Seguro de Auto nacional:</td><td>00000</td></tr>
                                        <tr><td>Seguro Resp. Civil (USA):</td><td>00000</td></tr>
                                        <tr><td>Seguro de Vida:</td><td>00000</td></tr>
                                        <tr><td>Comision por Apertura:</td><td>00000</td></tr>
                                        <tr><td>Placa y Tenencia:</td><td>00000</td></tr>
                                        <tr><td>Tramites:</td><td>00000</td></tr>
                                        <tr><td>Intereses:</td><td>00000</td></tr>
                                        <tr><td>Carroceria:</td><td>00000</td></tr>
                                        <tr><td>Accesorios(Anexar Pedido):</td><td>00000</td></tr>
                                        <tr><td>Seguro de Desempleo:</td><td>00000</td></tr>
                                        <tr><td>Ext./ Certif de  Garantia:</td><td>00000</td></tr>
                                        <tr><td ><h6>Total de Gastos Adicionales:</h6></td><td>00000</td></tr>
                                        </table>
									  </blockquote>
									</div>
                                    
                                    
                                    
                                      <div class="row-fluid">            
							 <div class="span4">
								<h6>.:Tipo de Seguro:</h6>
								</div>
							  
                               </div>
                               
                               
                                 <div class="row-fluid">            
							 <div class="span4">
								<h6>Financiado Anual</h6>
								</div>
							  <div class="span4">
								<h6>Financiado Multianual</h6>
								 </div>
                                  <div class="span4">
								<h6>Contado Anual</h6>
								 </div>
                               </div>
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>Contado Multianual</h6>
								</div>
							  <div class="span4">
								<h6>Financiado Semestral</h6>
								 </div>
                                  <div class="span4">
								<h6>Contado Semestral</h6>
								 </div>
                               </div>
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>Tipo de Seguro USA.</h6>
								</div>
                               </div>
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>Caic</h6>
								</div>
							  <div class="span4">
								<h6>AIG</h6>
								 </div>
                                  <div class="span4">
								<h6>GNP</h6>
								 </div>
                               </div>
                               
                                <div class="row-fluid">            
							 <div class="span4">
								<h6>Saldo a Favor o Flotante</h6>
                                <p>$0000</p>
								</div>
							  <div class="span4">
								<h6>Observaciones</h6>
                                <p><input type="text"></p>
								 </div>
                                  <div class="span4">
								<h6><table><tr><td>Desglozar iva</td><td>Tipo de Cambio</td></tr></table></h6>
                                <p><table><tr><td></td><td></td></tr></table></p>
								 </div>
                               </div>
				 
				 
				 
				    <div class="row-fluid">            
							 <div class="span4"><p><center><input type="checkbox"></center></p>
								<h6><center>Hector Revilla Ortiz <br> GTE. Seminuevos</center></h6>
                                
								</div>
							  <div class="span4">
								 <p><center><input type="checkbox"></center></p>
								<h6><center>Hector Revilla Ortiz <br> Asesor de Ventas</center></h6>
								 </div>
                                  
								 <div class="span4"><p><center><input type="checkbox"></center></p>
								<h6><center>Jose A. Maciel <br> GTE. General</center></h6>
								 </div>
                               </div>
				    
				    
				       <div class="row-fluid">            
							 <div class="span4"><p><center><input type="checkbox"></center></p>
								<h6><center>Monica Negrete <br> Contabilidad</center></h6>
                                
								</div>
							  <div class="span4">
								 <p><center><input type="checkbox"></center></p>
								<h6><center>Lic. Fernando Rivera <br> Gerente de Ventas</center></h6>
								 </div>
                                  
								 <div class="span4"><p><center><input type="checkbox"></center></p>
								<h6><center>Fedra Montoya <br> Credito / Seguros</center></h6>
								 </div>
                               </div>
                               
                               
			          <div class="row-fluid">            
							 <div class="span4"><p><center><input type="checkbox"></center></p>
								<h6><center>Salvador Gutierrez <br> Director</center></h6>
                                
								</div>
							  
                               </div>
                                    
                                    
                                    
								  </div>
							  </div>
						  </div>
                                 
                                 
                                 
                                 
                                 
								</p>
							</div>
							
						</div>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->		                  
                            
                         
                           
                       
                          
                               
                              
                              
                              
                              
                              
 
 
 
<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
               
				
	
        
      
      
   
      
        
        
		
		<div class="clearfix"></div>
        
                
        <div class="modal hide fade" id="myModalsavepago">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Agregar Pago</h3>
			</div>
            
            
            
			<div class="modal-body">
	 

</div>
		<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cancelar</a>
					  <button type="submit" class="btn btn-primary red-button" id="uploadButton">Subir Archivo</button>
			</div> 
		</div>
        
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

<script>
$(document).ready(function(){
	
	
	
$('.radiobutton').live('click',function(){

var id=$('input[name=IDOC]').val();

var table=$(this).attr('id');

var row=$(this).attr('name');

var valor=$(this).val();	
	
	$.post(" echo base_url(); ?>querys/insertordendecompra.php",{id:id,table:table,row:row,valor:valor}, function(data) { });
});
	
	
	
	$('#chadd').live('click',function(){	
var id=$('input[name=IDOC]').val();	
var mcCbxCheck = $(this);
	 
var calle=$('input[name=datc_calle]').val();
var colonia=$('input[name=datc_colonia]').val();
var ciudad=$('input[name=datc_ciudad]').val();
var estado=$('input[name=datc_estado]').val();
var municipio=$('input[name=datc_municipio]').val();
var codigo=$('input[name=datc_cp]').val();
	 
	 
	if(mcCbxCheck.is(':checked')) {
	
		$('input[name=dfa_calle]').val(calle);
		$('input[name=dfa_colonia]').val(colonia);
		$('input[name=dfa_ciudad]').val(ciudad);
		$('input[name=dfa_estado]').val(estado);
		$('input[name=dfa_municipio]').val(municipio);
		$('input[name=dfa_codigopostal]').val(codigo);
$.post("<?php echo base_url(); ?>querys/insertdireccionfacturacion.php",{id:id,icalle:calle,icolonia:colonia,iciudad:ciudad,iestado:estado,imunicipio:municipio,icodigo:codigo}, function(data) { });
    }
    else{
		$('input[name=datc_calleb]').val(" ");
		$('input[name=datc_coloniab]').val(" ");
		$('input[name=datc_ciudadb]').val(" ");
		$('input[name=datc_estadob]').val(" ");
		$('input[name=datc_municipiob]').val(" ");
		$('input[name=datc_cpb]').val(" ");
		
		$.post("<?php echo base_url(); ?>querys/insertdireccionfacturacion.php",{id:id,icalle:'',icolonia:'',iciudad:'',iestado:'',imunicipio:'',icodigo:''}, function(data) { });

    }
	
});
	
$('#perfisc').live('click',function(){	
$('.permor').hide();
$('.perfis').show();
});

$('#permorc').live('click',function(){	
$('.perfis').hide();
$('.permor').show();
});
	
$('.keyupdataxch').live('click',function(){	
	
var mcCbxCheck = $(this);
	 
var id=$('input[name=IDOC]').val();

var table=$(this).attr('id');

var row=$(this).attr('name');

var valor=$(this).val();
	 
	 
	if(mcCbxCheck.is(':checked')) {
$.post("<?php echo base_url(); ?>querys/insertordendecompra.php",{id:id,table:table,row:row,valor:'si'}, function(data) { });
    }
    else{
$.post("<?php echo base_url(); ?>querys/insertordendecompra.php",{id:id,table:table,row:row,valor:'no'}, function(data) { });
    }
	
});

$("input:text").addClass('keyupdatax');



$("input:checkbox").addClass('keyupdataxch');


	

$('.keyupdatax').live('keyup',function(){

var id=$('input[name=IDOC]').val();

var table=$(this).attr('id');

var row=$(this).attr('name');

var valor=$(this).val();	

$.post("<?php echo base_url(); ?>querys/insertordendecompra.php",{id:id,table:table,row:row,valor:valor}, function(data) { });



	
});

			
$('.chk-box').click(function(e){
	
var id= $(this).attr('id');
var clase='.uno'+id;
var ida=$('input[name=ida'+id+']').attr('value');
if($(clase).hasClass('lk')){$('.uno'+id).removeClass('lk');
$.post("<?php echo base_url(); ?>updateactividad.php",{ida:ida,val:'pendiente'}, function(data) { });
}


else{$('.uno'+id).addClass('lk');
	 $.post("<?php echo base_url(); ?>updateactividad.php",{ida:ida,val:'realizada'}, function(data) { });}
});

$('.noty').click(function(e){
var idc=$('input[name=idc]').val();
var idb=$('input[name=idb]').val();
var val=$(this).attr('value');
 $.post("<?php echo base_url(); ?>querys/updatestatus.php",{idc:idc,val:val,idb:idb}, function(data) { 
 
   document.location.href='<?php echo base_url(); ?>index.php/contacto/bitacora/<?php echo $idc;?>';
 });	
 

 
 });
 
 
 
	$('#checkproceso').click(function(e){
		e.preventDefault();
		$('#myModalC').modal('show');
		
		var idc=$('input[name=idc]').val();
var idb=$('input[name=idb]').val();
var val=$(this).attr('value');

 $.post("<?php echo base_url(); ?>querys/checkstatus.php",{idc:idc,val:val,idb:idb}, function(data) { 
if(data=='Prospecto' || data=='Caliente' || data=='Cliente' ){
	$.post("<?php echo base_url(); ?>querys/updatestatus.php",{idc:idc,val:val,idb:idb}, function(data) { 
 
  
 });
	}
else{
	
	
	
	}


 	

	 });
	
		
	});
 
 
 
	
	});

</script>	
	

</body>
</html>
