<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Agregar nuevo contacto</a></li>
                                                                       
                                </ul>
                            </section>
                      
                        <?php if($uno_contacto): ?>
                          <?php foreach($uno_contacto as $todo): ?>
                           <?php
						    $nom=$todo->con_nombre; 
						    $ape=$todo->con_apellido; 
							$emp=$todo->con_ocupacion;
							$tof=$todo->con_telefono_officina;
							$tof2=$todo->con_telefono_off;
							$tof3=$todo->con_telefono_off2;
							$tca=$todo->con_telefono_casa; 
							$cor=$todo->con_correo;
							$cob=$todo->con_correo_b; 
							$fue=$todo->fuente_fue_IDfuente;
							$hus=$todo->huser_hus_IDhuser; 
							$idc=$todo->con_IDcontacto;
							$sta=$todo->con_status;
							$tit=$todo->con_titulo;
							$tia=$todo->con_tipo;
							$ano=$todo->con_ano;
							$mod=$todo->con_modelo;
							$col=$todo->con_color;
							$rad=$todo->con_radio;
							$notas=$todo->con_notas;
							$pub=$todo->publicidad_pub_IDpublicidad;
							$hor=$todo->con_hora;
							$ver=$todo->con_version;
							$pman=$todo->con_pmanejo;
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                      
                      <div class="box_a">
                      
                                        <div class="box_a_heading">
                                            <h3>Editar informaci&oacute; de <?php echo $nom.' '.$ape;?></h3>
                                        </div>
                                        
                                        
<?php echo validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
<?php echo form_open('contacto/update/'.$idc.'/?edit=si','class="form-horizontal"'); ?>
 <fieldset>
                          
                         
                         <div class="control-group">
								<label class="control-label" for="prependedInput">Información personal</label>
								<div class="controls">
								 <hr/>
								</div>
							  </div>
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="selectErroraac">Hora de Llegada del Contacto a la Agencia</label>
                                
								<div class="controls">
                                <div class="input-prepend">
									<span class="add-on"></span>
                                  <?php
									/*$data = array(
              'name'        => 'hora',
              'id'          => 'hora',
              'value'       => $hor,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
                                      
             <?php  echo form_input('hora',set_value('hora', $hor), 'id="hora" size="16"');?> 
                                      
								</div></div>
							  </div>
                              
                             
                              <div class="control-group">
								<label class="control-label" for="selectErrorz">Titulo</label>
								<div class="controls">
                               <div class="input-prepend">
									<span class="add-on"></span>
                                
                                <?php  echo form_input('titulo', set_value('titulo',$tit), 'id="titulo" size="16"');?>
                                </div>
                                
								  
								</div>
							  </div> 
                              
                              
                              
							<div class="control-group">
								<label class="control-label" for="focusedInput">Nombre</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									/*$data = array(
              'name'        => 'nombre',
              'id'          => 'nombre',
              'value'       => $nom,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
             <?php  echo form_input('nombre',set_value('nombre', $nom), 'id="nombre" size="16"');?>                        
                                   

								 </div>*
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Apellido</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                      <?php
									
									/*
									$data = array(
              'name'        => 'apellido',
              'id'          => 'apellido',
              'value'       => $ape,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);*/?>
                                            <?php  echo form_input('apellido',set_value('apellido', $ape), 'id="apellido" size="16"');?> 
                                
								 </div>*
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="focusedInput">Empresa</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                      <?php
									
									/*
									$data = array(
              'name'        => 'empresa',
              'id'          => 'empresa',
              'value'       => $emp,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
                                      
                                            <?php  echo form_input('empresa',set_value('empresa', $emp), 'id="empresa" size="16"');?> 
                                
								 </div>*
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Telefono de Casa</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php
									/*
									
									$data = array(
              'name'        => 'toficina',
              'id'          => 'toficina',
              'value'       =>$tof,
              'maxlength'   => '',
              'size'        => '25',
              'style'       => '',
			  'class'=>'mask_phonedos'
            );
									  echo form_input($data); */?>
                                            <?php  echo form_input('toficina',set_value('toficina', $tof), 'id="toficina" size="16"');?> 
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              
                              
                                <div class="control-group">
								<label class="control-label" for="focusedInput">Telefono Oficina</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                     <?php
									
									/*
									$data = array(
              'name'        => 'toficina2',
              'id'          => 'toficina2',
              'value'       =>$tof2,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  'class'=>'mask_phonedos'
            );
									  echo form_input($data); */?>
                   <?php  echo form_input('toficina2',set_value('toficina2', $tof2), 'id="toficina2" size="16"');?>                     
                                    
								  </div>
                                 
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="focusedInput">Telefono Oficina 2</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                     <?php
									/*
									
									$data = array(
              'name'        => 'toficina3',
              'id'          => 'toficina3',
              'value'       =>$tof3,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  'class'=>'mask_phonedos'
            );
									  echo form_input($data); */?>
             <?php  echo form_input('toficina3',set_value('toficina3', $tof3), 'id="toficina3" size="16"');?>                         
								  </div>
                                 
								</div>
							  </div>
                              
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Celular</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                   <?php
									
									/*
									$data = array(
              'name'        => 'tcasa',
              'id'          => 'tcasa',
              'value'       =>$tca,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
			  'class'=>'mask_phonedos'
            );
									  echo form_input($data); */?>
              <?php  echo form_input('tcasa',set_value('tcasa', $tca), 'id="tcasa" size="16"');?>                          
                                    
								  </div>
								</div>
							  </div>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Radio</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php
									
									/*
									$data = array(
              'name'        => 'radio',
              'id'          => 'radio',
              'value'       =>$rad,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
 <?php  echo form_input('radio',set_value('radio', $rad), 'id="radio" size="16"');?>                                     
								  </div>
                                   
								</div>
							  </div>
                             
                              
                              						
                            
                            <div class="control-group">
								<label class="control-label" for="prependedInput">Correo Electronico</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">@</span>
                                   <?php
									
									/*
									$data = array(
              'name'        => 'correo',
              'id'          => 'correo',
              'value'       =>$cor,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
 <?php  echo form_input('correo',set_value('correo', $cor), 'id="correo" size="16"');?>                                       
                                    
								  </div>*
								 
								</div>
							  </div>
                              
                              
                               <div class="control-group">
								<label class="control-label" for="prependedInput">Correo Electronico alternativo</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">@</span>
                                  <?php
								/*	
									
									$data = array(
              'name'        => 'correob',
              'id'          => 'correob',
              'value'       =>$cob,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
<?php  echo form_input('correob',set_value('correob', $cob), 'id="correob" size="16"');?> 
								  </div>
								 
								</div>
							  </div>
                              
                              
                              
                              
                              
                              
                              
                              
                             
                              
                            
                            
                            <div class="control-group">
								<label class="control-label" for="selectErrorxx">Fuente</label>
								<div class="controls">
                              <select name="fuente" style="width:246px;">
										<option value=""  >Seleccione una opcion</option>	
<?php foreach($todo_fuente as $todo): ?>	
							
<option value="<?php echo $todo->fue_IDfuente;?>" <?php if($todo->fue_IDfuente==$fue){echo 'selected="selected"';}?> ><?php echo $todo->fue_nombre;?></option>
<?php endforeach ?>
</select>* 
								</div>
							  </div>
							  
							     <div class="control-group">
								<label class="control-label" for="selectErrorxxpcc">Publicidad</label>
								<div class="controls">
									 <select name="publicidad" style="width:246px;">
                               		<option value="" <?php echo set_select('publicidad','', TRUE); ?> >Seleccione una opcion</option>	
<?php foreach($todo_publicidad as $todo): ?>	
							
<option value="<?php echo $todo->pub_IDpublicidad;?>" <?php if($todo->pub_IDpublicidad==$pub){echo 'selected="selected"';}?> ><?php echo $todo->pub_nombre;?></option>
<?php endforeach ?>
</select>* 
						  
								</div>
							  </div>
                              
                              
                              
                              <div class="control-group">
								<label class="control-label" for="selectError">Tipo de Contacto</label>
								<div class="controls">
<select name="status" style="width:246px;">
<option value="" <?php echo set_select('status','', TRUE); ?> >Seleccione una opcion</option>								
<option value="Prospecto" <?php if('Prospecto'==$sta){echo 'selected="selected"'; } ?> >Prospecto</option>
<option value="Caliente" <?php if('Caliente'==$sta){echo 'selected="selected"'; } ?>>Caliente</option>
<option value="Cliente" <?php if('Caidas'==$sta){echo 'selected="selected"'; } ?> >Ventas Ca&iacute;das</option>
<option value="Proceso de Venta"  <?php echo set_select('status','Proceso de Venta'); ?> <?php if('Proceso de Venta'==$sta){echo 'selected="selected"'; } ?> >Proceso de Venta</option>
<option value="Cliente"  <?php echo set_select('status','Cliente'); ?> <?php if('Cliente'==$sta){echo 'selected="selected"'; } ?> >Cliente</option>
</select>* 
                                
								  
								</div>
							  </div>
                            
                            
                             <div class="control-group">
								<label class="control-label" for="selectError">Prueba de Manejo</label>
								<div class="controls">
<select name="pmanejo" style="width:246px;">
<option value="" <?php echo set_select('pmanejo','', TRUE); ?> >Seleccione una opci&oacute;n</option>			
<option value="Si" <?php if('Si'==$pman){echo 'selected="selected"'; } ?>>Si</option>
<option value="No" <?php if('No'==$pman){echo 'selected="selected"'; } ?> >No</option>
</select>* 
                                
								  
								</div>
							  </div>
                            
                              
                             <div class="control-group">
								<label class="control-label" for="prependedInput">Auto de Interes</label>
								<div class="controls">
								 <hr/>
								</div>
							  </div>         
                              
                            
                            
                             <div class="control-group">
								<label class="control-label" for="selectErrorff">Tipo de Auto</label>
								<div class="controls">
                                
                               <select name="tipoa" style="width:246px;">
										<option value="" <?php echo set_select('tipoa','', TRUE); ?> >Seleccione una opcion</option>	

							
<option value="Nuevo" <?php if('Nuevo'==$tia){echo 'selected="selected"'; } ?> >Nuevo</option>
<option value="Seminuevo"<?php if('Seminuevo'==$tia){echo 'selected="selected"'; } ?> >Seminuevo</option>

</select>* 
                                
								  
								</div>
							  </div>
                              
                              
                             <div class="control-group">
								<label class="control-label" for="selectErroraa">Año</label>
								<div class="controls">
									 <div class="input-prepend">
									<span class="add-on"></span>
                                
                                 <?php
									
									/*
									$data = array(
              'name'        => 'ano',
              'id'          => 'ano',
              'value'       =>$ano,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);*/?>
 <?php  echo form_input('ano',set_value('ano', $ano), 'id="ano" size="16"');?>                                       
                                
								  </div>*
								</div>
							  </div>
                              
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraam">Modelo</label>
								<div class="controls">
<select name="modelo" style="width:246px;">
<option value="" <?php echo set_select('modelo','', TRUE); ?> >Seleccione una opcion</option>							
<option value="ACCORD" <?php if('ACCORD'==$mod){echo 'selected="selected"';} ?> >Accord</option>
<option value="FIT" <?php if('FIT'==$mod){echo 'selected="selected"';} ?>>Fit</option>
<option value="CIVIC" <?php if('CIVIC'==$mod){echo 'selected="selected"';} ?> >Civic</option>
<option value="PILOT" <?php if('PILOT'==$mod){echo 'selected="selected"';} ?> >Pilot</option>
<option value="CRV" <?php if('CRV'==$mod){echo 'selected="selected"';} ?> >CRV</option>
<option value="ODYSSEY" <?php if('ODYSSEY'==$mod){echo 'selected="selected"';} ?> >Odyssey</option>
<option value="CITY" <?php if('CITY'==$mod){echo 'selected="selected"';} ?> >City</option>
<option value="CRZ" <?php if('CRZ'==$mod){echo 'selected="selected"';} ?> >CRZ</option>
</select>*                                 
</div>
							  </div>
                              
                             
                              
                              <div class="control-group">
								<label class="control-label" for="selectErroraac">Version</label>
								<div class="controls">
									 <div class="input-prepend">
									<span class="add-on"></span>
                               <?php
									
									/*
									$data = array(
              'name'        => 'version',
              'id'          => 'version',
              'value'       =>$ver,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
                                      
<?php  echo form_input('version',set_value('version', $ver), 'id="version" size="16"');?> 
								  </div>
								</div>
							  </div>
                              
                              
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraac">Color</label>
								<div class="controls">
									 <div class="input-prepend">
									<span class="add-on"></span>
                               <?php
									
									/*
									$data = array(
              'name'        => 'color',
              'id'          => 'color',
              'value'       =>$col,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data); */?>
 <?php  echo form_input('color',set_value('color', $col), 'id="color" size="16"');?>                                       
								  </div>
								</div>
							  </div>
                              
                              
                              
                                 <div class="control-group">
								<label class="control-label" for="selectErroraac">Notas</label>
                                
								<div class="controls">
                                <div class="input-prepend">
                            <?php
                            
                            $data = array(
              'name'        => 'notas',
              'id'          => 'notas',
              'value'       => $notas,
              'rows'   => '4',
              'cols'        => '60',
              'style'       => 'width:235px',
            );

echo form_textarea($data);
                            
                            
                            ?>
                            
								</div></div>
							  </div>
                              
                              
                              
                            
                            
                            
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Actualizar Información','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>
                                        
                                        
                                        
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>