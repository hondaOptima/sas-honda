<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cotiza"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Cotizaciones</a></li>
                                     <li style="float:right">
                                      <a class="btn btn-small btn-success " style="padding-top:-4px;" href="<?php echo base_url();?>index.php/cotizaciones/CrearCotizacionHuser">Crear nueva cotizacion</a>
                                     </li>                                  
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
<?php echo $flash_message ?>
             
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Lista de Cotizaciones</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
 <table id="dt_table_toolse"   class="table table-striped table-condensed">
                                                <thead>
							  <tr>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
                                  <th >Asesor</th>  
<?php } ?>                                  
                                  <th  >Fecha</th>
								  <th >Nombre</th>
								  <th>Apellido</th>
                                  <th >Email</th>
                                  <th >Ficha</th>  
                                  <th >Nota</th>
								  <th >Acciones</th>
							  </tr>
						  </thead> 
                                                <tbody >
                                                    
                                                    
                                                    <?php 
if($lista_cotizacion): 
foreach($lista_cotizacion as $todo): ?><tr>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_nombre.' '.$todo->hus_apellido;?></td>
<?php } ?>
<td><?php echo $todo->cot_fecha;?></td>
<td><?php echo $todo->lsp_nombre;?></td>
<td><?php echo $todo->lsp_apellido;?></td>
<td><?php echo $todo->cot_email;?></td>
<td><?php echo $todo->fmo_nombre;?></td>
<td><?php echo $todo->cot_nota;?></td>
<td>
<a  title="Ver Pdf"href="<?php echo base_url();?>index.php/cotizaciones/viewcotizarpdfhuser/<?php echo $todo->cot_IDcotizador;?>/<?php echo $todo->lsp_IDlista_espera_cotizacion;?>" target="_blank"><i class="icon-eye-open"></i></a>
<a title="Editar" href="<?php echo base_url();?>index.php/cotizaciones/LlenarCotizacionHuser/<?php echo $todo->cot_IDcotizador.'-'.$todo->lsp_IDlista_espera_cotizacion;?>"><i class="icon-pencil"></i></a>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<a title="Transferir" href="<?php echo base_url();?>index.php/contacto/add/?TPOLC=<?php echo $todo->lsp_IDlista_espera_cotizacion.'-'.$todo->cot_IDcotizador;?>"><i class="icon-random"></i></a>
<?php }?>

<a title="Eliminar" href="<?php echo base_url();?>index.php/contacto/eliminarListaCotizacion/<?php echo $todo->lsp_IDlista_espera_cotizacion.'-'.$todo->cot_IDcotizador;?>"><i class="icon-trash"></i></a>

 
</td>
</tr>
<?php endforeach ?>
<?php else: ?>None<?php endif ?>
                                                        
                                                    
                                               
                                                </tbody>
                                            </table>
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        
                                        	<?php $todo_contacto;?>
				
		<div class="modal hide fade" id="myModalDecidete">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Tipo de Contacto</h3>
				
			</div><?php echo form_open('contacto/decide','class=""'); ?>
			<div class="modal-body">
			
				<p>
					 <div class="control-group">
								<label class="control-label" for="selectErrorrt">Buscar Contacto</label>
								<div class="controls">
                                
                   <input style="margin-top:10px;" type="text" name="buscarcontacto"><a class="btn btn-small addnuevo" style="padding-top:-4px;" href="#">Agregar nuevo Contacto</a>
								  
								</div>
							  </div>
                              <div class="respuestatabla"></div>
</p>
				
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cerrar</a>

			</div>	<?php echo form_close(); ?>
		
		</div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 
<script type='text/javascript'>

	$(document).ready(function() {
		
		$('.llamadaEntrante').click(function(e){
		e.preventDefault();
		$('#myModalDecidete').modal('show');
	});
	
		$('input[name=buscarcontacto]').live('keyup',function(){	
		var valor=$(this).val();
		
   $.ajax({
   type: 'POST',
   url: '<?php echo base_url(); ?>index.php/hpp/buscarcontacto/',
   data: 'valor='+valor+'',   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
   $('.respuestatabla').html(data);
   }
});
	});
	
	
	$('.addnuevo').live('click',function(e){	
		var valor=$('input[name=buscarcontacto]').val();
		
		$('input[name=cliente]').val(valor);
		$('input[name=lle_cliente_interes]').val(valor);
		 e.preventDefault();
		$('#myModalDecidete').modal('hide');
	
	});
	
	
	$('.addexistente').live('click',function(e){	
	var id=$(this).attr('id');
	var elem = id.split('[');
valor = elem[0];
telefono = elem[1];
correo = elem[2];
idcontacto = elem[3];
		
		$('input[name=cliente]').val(valor);
		$('input[name=lle_cliente_interes]').val(valor);
		$('input[name=telefono]').val(telefono);
		$('input[name=lle_correo]').val(correo);
		$('input[name=tipollamada]').val(idcontacto);
		
		 e.preventDefault();
		$('#myModalDecidete').modal('hide');
	
	});
	
		
		
		$('.active-result').live('click',function(){	

var idc=$('select[name=tipocontactosave]').val();
var id=$(this).attr('id');

var elem = id.split('_');
nombre = elem[0];
mes = elem[1];

if(nombre=="selectErrorrt"){ 

if(idc!='generales'){
//$('#displaycont').show();
}
else{}

}
});
		
	});
	</script>

 

    </body>
</html>