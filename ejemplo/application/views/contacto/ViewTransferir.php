<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Transferir Contacto</a></li>
                                                                       
                                </ul>
                            </section>
                            
                            
                      
                        <?php if($uno_contacto): ?>
                          <?php foreach($uno_contacto as $todo): ?>
                           <?php
						    $nom=$todo->con_nombre; 
						    $ape=$todo->con_apellido; 
							
							$tit=$todo->con_titulo;
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>
                      
                      <div class="box_a">
                      
                                        <div class="box_a_heading">
                                            <h3>Transferir informacion de <?php echo $nom.' '.$ape;?></h3>
                                        </div>
                                        
                                        
<?php echo validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>
<?php echo form_open('contacto/ViewTransferir/'.$idc.'','class="form-horizontal"'); ?>
 <fieldset>
                          
                         
                        
                              
                              
                                
                             
                              <div class="control-group">
								<label style="width:400px;" class="control-label" for="selectErrorz"><b>Contacto:</b>  <?php  echo $tit.' '.$nom.' '.$ape;?></label>
								
                              
                                
							  </div> 
                              
                              
                              
							<div class="control-group">
								<label class="control-label" for="focusedInput"><b>Asesor:</b></label>
								<div class="controls">
								
									 <?php
echo form_dropdown('vendedor', $todo_vendedor,'','style="width:246px;" id="s2_single" data-rel="chosen"');
								?>

								</div>
							  </div>
                              
                              <div class="form-actions">
							 <?php echo form_submit('submit', 'Transferir Información','class="btn btn-primary"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>
                                        
                                        
                                        
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>