<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="javascript:history.go(-1)">Bitacora</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Editar comentario</a></li>
                                                                       
                                </ul>
                          </section>


                       
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Editar comentario</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <br>
 <?php if($todo_tareas): ?><?php foreach($todo_tareas as $tod): ?> 
  <?php
  $ida=$tod->act_IDactividades;
  $tit=$tod->act_titulo;
  $sta=$tod->act_status;
   $fei=$tod->act_fecha_inicio;
    $fef=$tod->act_fecha_fin;
	$des=$tod->act_descripcion;
	$not=$tod->act_notificar;
	$tmn=$tod->tipo_mensage_tip_IDtipo_mensage;
	
$hr=$tod->act_hora;
if($hr <10){$hr='0'.$hr;}
$mn=$tod->act_min;
if($mn==0){$mn='00';}
	$hora=$hr.':'.$mn;
	
  
  ?>        
   <?php endforeach ?><?php else: ?>None<?php endif ?>	 
   
   <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>                            
     
     
  <?php echo form_open('contacto/updatetarea/'.$ida.'/'.$idc.'/'.$idb.'','class="form-horizontal"'); ?>
							
                            <fieldset>   
                            
                           
                      
                            
                            
                            
     
         
   <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php
									
									
									$data = array(
              'name'        => 'titulo',
              'id'          => 'titulo',
              'value'       => $tit,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
                                    
                                   

								  </div>
								</div>
							  </div>      
         
         
          <div class="control-group">
							  <label class="control-label" for="date01">Fecha Inicio:</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 list($an,$me,$di)=explode('-',$fei);
								 $fcu=$me.'/'.$di.'/'.$an;
								 $datai= array(
              'name'        => 'fei',
              'id'          => 'dpStart',
			  'class'       => 'input-xlarge datepicker',
              'value'       => $fcu,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; data-date-format="dd/mm/yyyy" data-date-autoclose="true" ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
         
         
         
         
         
         
           <div class="control-group">
							 	<label class="control-label" for="selectErrorkk">Hora</label>
								<div class="controls">
                              
                                 <?php
                                $options = array(
                                 '24:00'    => '12:00 am',
                                 '24:15'    => '12:15 am',
                                 '24:30'    => '12:30 am',
                                 '24:45'    => '12:45 am',
								 '01:00'    => '1:00 am',
								 '01:15'    => '1:15 am',
								 '01:30'    => '1:30 am',
								 '01:45'    => '1:45 am',
                                 '02:00'    => '2:00 am',
                                 '02:15'    => '2:15 am',
								 '02:30'    => '2:30 am',
								 '02:45'    => '2:45 am',
								 '03:00'    => '3:00 am',
								 '03:15'    => '3:15 am',
                                 '03:30'    => '3:30 am',
                                 '03:45'    => '3:45 am',
								 '04:00'    => '4:00 am',
								 '04:15'    => '4:15 am',
								 '04:30'    => '4:30 am',
								 '04:45'    => '4:45 am',
								 '05:00'    => '5:00 am',
								 '05:15'    => '5:15 am',
								 '05:30'    => '5:30 am',
								 '05:45'    => '5:45 am',
                                 '06:00'    => '6:00 am',
                                 '06:15'    => '6:15 am',
								 '06:30'    => '6:30 am',
								 '06:45'    => '6:45 am',
								 '07:00'    => '7:00 am',
								 '07:15'    => '7:15 am',
                                 '07:30'    => '7:30 am',
                                 '07:45'    => '7:45 am',
								 '08:00'    => '8:00 am',
								 '08:15'    => '8:15 am',
								 '08:30'    => '8:30 am',
								 '08:45'    => '8:45 am',
                                 '09:00'    => '9:00 am',
                                 '09:15'    => '9:15 am',
								 '09:30'    => '9:30 am',
								 '09:45'    => '9:45 am',
								 '10:00'    => '10:00 am',
								 '10:15'    => '10:15 am',
                                 '10:30'    => '10:30 am',
                                 '10:45'    => '10:45 am',
								 '11:00'    => '11:00 am',
								 '11:15'    => '11:15 am',
								 '11:30'  => '11:30 am',
								  '11:45'  => '11:45 am',
                                 '12:00'    => '12:00 pm',
                                 '12:15'    => '12:15 pm',
								 '12:30'    => '12:30 pm',
								 '12:45'    => '12:45 pm',
								 '13:00'    => '1:00 pm',
								 '13:15'    => '1:15 pm',
								 '13:30'  => '1:30 pm',
								 '13:45'  => '1:45 pm',
                                 '14:00'    => '2:00 pm',
                                 '14:15'    => '2:15 pm',
								 '14:30'    => '2:30 pm',
								 '14:45'    => '2:45 pm',
								 '15:00'  => '3:00 pm',
								  '15:15'  => '3:15 pm',
                                 '15:30'    => '3:30 pm',
                                 '15:45'    => '3:45 pm',
								 '16:00'    => '4:00 pm',
								 '16:15'    => '4:15 pm',
								 '16:30'  => '4:30 pm',
								 '16:45'  => '4:45 pm',
								 '17:00'    => '5:00 pm',
								 '17:15'    => '5:15 pm',
								 '17:30'  => '5:30 pm',
								  '17:45'  => '5:45 pm',
                                 '18:00'    => '6:00 pm',
                                 '18:15'    => '6:15 pm',
								 '18:30'    => '6:30 pm',
								  '18:45'    => '6:45 pm',
								 '19:00'  => '7:00 pm',
								  '19:15'  => '7:15 pm',
                                 '19:30'    => '7:30 pm',
                                 '19:45'    => '7:45 pm',
								 '20:00'    => '8:00 pm',
								 '20:15'    => '8:15 pm',
								 '20:30'  => '8:30 pm',
								 '20:45'  => '8:45 pm',
                                 '21:00'    => '9:00 pm',
                                 '21:15'    => '9:15 pm',
								 '21:30'    => '9:30 pm',
								 '21:45'    => '9:45 pm',
								 '22:00'  => '10:00 pm',
								 '22:15'  => '10:15 pm',
                                 '22:30'    => '10:30 pm',
                                 '22:45'    => '10:45 pm',
								 '23:00'    => '11:00 pm',
								 '23:15'    => '11:15 pm',
								 '23:30'  => '11:30 pm',
								 );
								 echo form_dropdown('hora', $options,$hora,'style="width:246px;" id="selectErrorkk" data-rel="chosen"');
								?>
                              
								
							  </div>
							</div>
         
         
         
        
         
         
         <div class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripcion:</label>
							  <div class="controls">
								
                                 <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => '',
              'value'       => $des,
              'maxlength'   => '',
              'row'        => '',
              'style'       => 'width:237px; height:75px;',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
         
         
         
          <div class="control-group">
								<label class="control-label" for="selectError">Estado</label>
								<div class="controls">
                                
                                <?php
                                $options = array(
								
                                 'pendiente'  => 'Pendiente',
                                 'realizada'    => 'Realizada',
                              
                                 'cancelada'    => 'Cancelada',
								
								 );
								 echo form_dropdown('status', $options,$sta,'style="width:246px;" id="selectError" data-rel="chosen"');
								?>
                                
								  
								</div>
							  </div>
<div class="form-actions">
            <?php echo anchor("contacto/deletetareas/".$idc."/".$ida."", ' <div class="btn btn-small btn-danger">Eliminar</div>',array('onClick' => "return confirm('Esta seguro que desea eliminar esta Tarea ?')",'class'=>"")); ?> 
         
 
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         
         
         
         
         
         
         
         
         
         
         
 
 
   </fieldset>
						<?php echo form_close(); ?>         

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 
  <script >
												$(document).ready(function()
{
                                 $('#dpStart').datepicker().on('changeDate', function(e){
                    
                });
                $('#dpEnd').datepicker().on('changeDate', function(e){
                    $('#dpStart').datepicker('setEndDate', e.date)
                });
				});
                                                </script>

    </body>
</html>