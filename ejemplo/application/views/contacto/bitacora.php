<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                     <li><a href="<?php echo base_url();?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Bitacora</a></li>
                                    
                                    <li style="float:right">
                                    
                                    <?php if($id_vendedor) {?>
<a class="btn-settingcouch btn btn-small btn-success" href="<?php echo base_url();?>index.php/contacto/">
Regresar a Coucheo
</a>           


<button class="btn btn-small btn-warning btnmodalcomentariocoucheo">
<span class="fa-icon-legal" style="margin-right: 3px;"></span>Registrar Coucheo
</button>

<?php } else{} ?>

<button class="btn btn-small btn-warning btnmodaltarea">
<span class="fa-icon-legal" style="margin-right: 3px;"></span>Nueva Tarea
</button>			
                          
<a class="btn-settingcomentario btn btn-small btn-primary" href="">Comentario</a>
<button class="btn btn-small btn-primary btn-credito">Solicitud de Cr&eacute;dito</button>
<a class="btn-settingB btn btn-small btn-primary" href="">Ordenes de Compra</a>
                                    
                                    
                                    </li>
                                                                       
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
<?php echo $flash_message ?>
   
<?php 
if($uno_contacto): 
foreach($uno_contacto as $todo): 
$idc=$todo->con_IDcontacto;
$folder=$todo->con_folder;
$sta=$todo->con_status;
$nom=$todo->con_nombre; 
$ape=$todo->con_apellido; 
$tit=$todo->con_titulo;
$nota=$todo->con_notas;
$hora=$todo->con_hora;   
endforeach ?>
<?php else: ?>None<?php endif ?>

<?php if($registro): ?><?php foreach($registro as $reg): ?> 
<input type="hidden" name="idb" value="<?php echo $reg->bit_IDbitacora;?>">
<input type="hidden" name="idc" value="<?php echo $idc;?>">
<?php   
$idb=$reg->bit_IDbitacora;$date=$reg->bit_fecha; list($an,$ms,$di)=explode('-',$date); 
?> 
<?php endforeach ?><?php else: ?>No<?php endif ?>		
                              
                              </div></div>
                              
<?php $this->load->view('contacto/detalle',$uno_contacto); ?>                               
                   
                    <!-- jPanel sidebar -->
<div class="row-fluid">
 <div class="span12">
<div class="box_a">
                                                                      

<div class="box_a_content">
                       
<div  style="width:100%">
<div class="tabbable tabs-left tabbable-bordered">
<ul class="nav nav-tabs">
                                                         
<li class="active"><a data-toggle="tab" href="#tb3_a">Linea del tiempo</a></li>                                                
<li><a data-toggle="tab" href="#tb3_b">Repositorio</a></li>
<li ><a data-toggle="tab" href="#tb3_d"> Gu&iacute;a Telef&oacute;nica de Internet</a>
<li ><a data-toggle="tab" href="#tb3_e"> Hoja de Una raz&oacute;n</a>
<li ><a data-toggle="tab" href="#tb3_f"> Cotizaciones</a>

</ul>
<div class="tab-content">

<div id="tb3_a" class="tab-pane active ">

<ul class="tmtimeline">  

<?php if($todo_actividades): ?><?php foreach($todo_actividades as $fidb): ?>
<?php
$fechaini=$fidb->act_fecha_inicio;
list($an,$ms,$di)=explode('-',$fechaini);
if($fidb->act_hora < 12){$ti="am";}else{$ti="pm";}
if($fidb->act_notificar=='on'){$icon='fa-icon-bullhorn yellow';}
if($fidb->act_status=='realizada'){$icon='elusive-icon-ok green';}
if($fidb->act_status=='pendiente'){$icon='icon-time yellow';}
if($fidb->act_status=='cancelada'){$icon='elusive-icon-remove red';}									
?>	                                                              
<li>
<time class="tmtime" datetime="24-07-2013 10:30"><span><?php echo $fidb->act_hora.':'.$fidb->act_min.' '.$ti;?></span><span><?php echo $di.'/'.$ms.'/'.$an;?></span></time>
<div class="tmicon"><i class="<?php echo $icon;?>"></i></div>
<div class="tmlabel">
<h2>
<?php
if($fidb->tipo_actividad_tac_IDtipo_actividad==4){$url='updatecomentario';}else{$url='updatetarea';}
if($fidb->tipo_actividad_tac_IDtipo_actividad=='2'){}else{
if($fidb->tipo_actividad_tac_IDtipo_actividad=='3'){}else{	
echo '<a href="'.base_url().'index.php/contacto/'.$url.'/'.$fidb->act_IDactividades.'/'.$idc.'/'.$idb.'" >'; }}
echo $fidb->act_titulo;
if($fidb->tipo_actividad_tac_IDtipo_actividad=='2'){}else{
echo '</a >';}?>
</h2>
<p>
<?php if($fidb->tipo_actividad_tac_IDtipo_actividad=='3'){}else{?>
<?php echo 'Descripcion: '. $fidb->act_descripcion; }?>
</p>
</div>
</li> 
<?php endforeach ?><?php else: ?><?php endif ?>	
<!-- Fin Lista de Actividades-->  


<!-- Lista llamada hoja de uan razon-->
<?php 
$datax['idc']=$idc; $datax['todo_hoja_deunarazon']=$todo_hoja_deunarazon; 
$this->load->view('contacto/lista_hoja_razon',$datax); 
?>

<!-- Lista llamada entrante-->
<?php 
$datax['idc']=$idc; $datax['llamada_entrante']=$llamada_entrante; 
$this->load->view('contacto/lista_llamada_entrante',$datax); 
?>

<!-- Lista guia telefonica-->
<?php 
$datax['idc']=$idc; $datax['guia_telefonica']=$guia_telefonica; 
$this->load->view('contacto/lista_guia_telefonica',$datax); 
?>

<!-- Lista guia internet-->
<?php 
$datax['idc']=$idc;$datax['idb']=$idb; $datax['guia_telefonicaInternet']=$guia_telefonicaInternet; 
$this->load->view('contacto/lista_guia_internet',$datax); 
?>  


<li>
<time class="tmtime" datetime="24-07-2013 10:30"><span><?php echo $hora;?></span><span>
<?php 
list($an,$ms,$di)=explode("-",$registro[0]->bit_fecha);
echo $di.'/'.$ms.'/'.$an;?></span></time>
<div class="tmicon"><i class="elusive-icon-asterisk"></i></div>
<div class="tmlabel">
<h2>Alta en el Sistema</h2>
<p>
Nota:<?php echo $nota;?>
</p>
</div>
</li>                                      
</ul></div>
                                                            
                                                            
<div id="tb3_b" class="tab-pane">
<!-- Lista de archivos--><?php $this->load->view('contacto/repositorio',$uno_contacto); ?> 
</div>
                                                                                                                    
                                                                                                                       
<div id="tb3_d" class="tab-pane">
<!-- guia internet--><?php 

$this->load->view('hpp/guia-telefonica-internet',$uno_contacto); ?>
</div>

<div id="tb3_e" class="tab-pane">
<!--Hoja raz&oacute;n--><?php $this->load->view('hpp/una-razon',$uno_contacto); ?> <!--modal razon fin-->
</div>

<div id="tb3_f" class="tab-pane">
<!--Hoja raz&oacute;n--><?php 
$datact['idc']=$idc;
$datact['lista_cotizacion']=$lista_cotizacion;
$this->load->view('cotizaciones/cotizar',$datact); ?> <!--modal razon fin-->
</div>
                                                            
                                                            
                                                           
                                                        </div>
                                                    </div>
                                                </div>
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        
 <!--Modal nueva tareas-->
<?php 
$datax['idc']=$idc;$datax['idb']=$idb;
$this->load->view('contacto/nuevaTareaContacto',$datax); 
?>

 <!--Modal comentario Coucheo-->
<?php 
$this->load->view('contacto/nuevoComentarioCoucheo',$datax); 
?>
 <!--Modal lista ordenes de compra-->
<?php 
$datax['idc']=$idc;$datax['ordenes_de_compra']=$ordenes_de_compra;
$this->load->view('contacto/lista_orden_decompra',$datax); 
?>

<!--Modal lista de proceso de venta-->
<?php 
$datax['idc']=$idc;$datax['idb']=$idb;$datax['sta']=$sta;
$this->load->view('contacto/lista_procesosdeventa',$datax); 
?>
        
<!--Modal form comentario -->
<?php 
$datax['idc']=$idc;$datax['idb']=$idb;
$this->load->view('contacto/formComentario',$datax); 
?>

 <!--Modal form upload file pdf-->
<?php 
$datax['idc']=$idc;$datax['folder']=$folder;
$this->load->view('contacto/formUploadfile',$datax); 
?>     

 <!--Modal Solicitud credito-->
<?php 
if(!empty($todo_credito)):  
$datax['todo_credito']=$todo_credito;
?>
<?php else: ?>
<?php $datax['todo_credito']='';?>
<?php endif ?>
<?php 
$datax['idc']=$idc;
$this->load->view('contacto/formSolicitudCredito',$datax); 
?> 
        
        
		
        
      
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
 <?php $this->load->view('globales/footer'); ?> 
<?php $this->load->view('globales/js'); ?> 

<script>
$(document).ready(function()
{
	
	 $(".eliminarfoto").live('click',function() {

disp_confirm();
      
    });
	
	
	 $('.arbol').load('<?php echo base_url();?>arbol.php?idc=<?php echo $idc;?>');
	
    $('#FileUploader').on('submit', function(e)
    {
		
        e.preventDefault();
        $('#uploadButton').attr('disabled', ''); // disable upload button
        //show uploading message
        
        $("#output").html('<div style="padding:10px"><img src="<?php echo base_url();?>img/ajax-loader.gif" alt="Please Wait"/> <span>Uploading...</span></div>');
		
        $(this).ajaxSubmit({target: '#output',
        success:  afterSuccess});
        
        	 
        
    });
	
	$('.chk-box').click(function(e){
	
var id= $(this).attr('id');
var clase='.uno'+id;
var ida=$('input[name=ida'+id+']').attr('value');
if($(clase).hasClass('lk')){$('.uno'+id).removeClass('lk');
$.post("<?php echo base_url(); ?>updateactividad.php",{ida:ida,val:'pendiente'}, function(data) { });
}


else{$('.uno'+id).addClass('lk');
	 $.post("<?php echo base_url(); ?>updateactividad.php",{ida:ida,val:'realizada'}, function(data) { });}
});

$('.noty').live('click',function(){
var idc=$('input[name=idc]').val();
var idb=$('input[name=idb]').val();
var val=$(this).attr('value');
//alert(idc + idb + val);
 $.post("<?php echo base_url(); ?>querys/updatestatus.php",{idc:idc,val:val,idb:idb}, function(data) { 
 
   document.location.href='<?php echo base_url(); ?>index.php/contacto/bitacora/<?php echo $idc;?>';
 });	
 

 
 });
 

$('#saveguiainternetty').live('click',function(){
	var	query_params='';
	 query_params = $('#formainternetx').serialize();

 $.post("<?php echo base_url(); ?>querys/saveguiatelinternet.php",{array:query_params}, function(data) {
	  document.location.href='<?php echo base_url(); ?>index.php/contacto/bitacora/<?php echo $idc;?>';	 });	
	
		
		});
		
		
			$('#saveunarazon').live('click',function(){
				
	var query_params = $('#formarazon').serialize();
	
 $.post("<?php echo base_url(); ?>querys/saveunarazon.php",{array:query_params}, function(data) {
    document.location.href='<?php echo base_url(); ?>index.php/contacto/bitacora/<?php echo $idc;?>'; });
		
		
		});
		
		$('.btnmodaltarea').click(function(e){
		e.preventDefault();
		$('#myModaltarea').modal('show');
	});
		

$('.btnmodalcomentariocoucheo').click(function(e){
		e.preventDefault();
		$('#myModaltareaCoucheo').modal('show');
	});




});
 
function afterSuccess()
{
    $('#FileUploader').resetForm();  // reset form
    $('#uploadButton').removeAttr('disabled'); //enable submit button
    $('.arbol').load('<?php echo base_url();?>arbol.php?idc=<?php echo $idc;?>');
}

	function disp_confirm() {
		
var r=confirm("Esta seguro de elimira este archivo ?");
if (r==true)
  {
var idfile=$('.eliminarfoto').attr('id');
$.post("<?php echo base_url(); ?>querys/deletearchivo.php",{idfiel:idfile}, function(data) { 
$('.arbol').load('<?php echo base_url();?>arbol.php?idc=<?php echo $idc;?>');
$(".eliminarfoto").remove();  		  
$(".editarfoto").remove();
});
}else {} }	
</script>

    </body>
</html>