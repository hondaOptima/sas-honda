<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">
                      
                      <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Agregar nuevo contacto</a></li>
                                                                       
                                </ul>
                            </section>
                      
                      
                      
                      <div class="box_a">
                      
                                        <div class="box_a_heading">
                                            <h3>Editar contacto</h3>
                                        </div>
                                        
                                        
 <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>');
if(!empty($TPOLC)){ $urlIDTPOLC='/?TPOLC='.$TPOLC.'';}else{$urlIDTPOLC='';}
 ?>
                    
                    
<?php echo form_open('contacto/update/'.$idc.'/?edit=si','class="form-horizontal"'); ?>
							
<fieldset>
<input type="hidden" name="TPOLC" value="<?php if(!empty($TPOLC)){echo  set_value('TPOLC',$TPOLC);}else{ set_value('TPOLC',0);}?>">
<div class="control-group">
<label class="control-label" for="prependedInput">Información personal</label>
<div class="controls"> <hr/></div></div>
							  

                             
<div class="control-group">
<label class="control-label" for="selectErroraac">Hora de Llegada del Contacto a la Agencia</label>
<div class="controls"><div class="input-prepend">
<span class="add-on"></span>
<?php
$data = array(
              'name'        => 'hora',
              'id'          => 'hora',
              'value'       => ''.date('h:i A').'',
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
								</div></div>
							  </div>
							  
                              <div class="control-group">
								<label class="control-label" for="selectErrorz">Titulo</label>
								<div class="controls">
									<div class="input-prepend">
									<span class="add-on"></span>
                                
                                <?php  echo form_input('titulo', set_value('titulo'), 'id="titulo" size="16"');?>
                                </div>
								  
								</div>
							  </div>
                              
                              
                              
							<div class="control-group">
								<label class="control-label" for="focusedInput">Nombre</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  
									if(isset($contacto)){$con=$contacto;$datanom=$con;}else{$con=''; $datanom=set_value('nombre');}
									echo form_input('nombre',$datanom, 'id="nombre"  size="16"');?>
                                 
								  </div>*
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Apellido</label>
								<div class="controls">
								<div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('apellido', set_value('apellido'), 'id="apellido" size="16"');?>
                                
								  </div>*
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Empresa</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('empresa', set_value('empresa'), 'id="empresa" size="16"');?>
                                 
								  </div>
								</div>
							  </div>
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Tel&eacute;fono de Casa</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('toficina', set_value('toficina'), 'id="toficina" class="mask_phonedos" size="16"');?>
                                    
								  </div>
                               
								</div>
							  </div>
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Tel&eacute;fono Oficina</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('toficina2', set_value('toficina2'), 'id="toficina2" class="mask_phonedos" size="16"');?>
                                    
								  </div>
                                 
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="focusedInput">Tel&eacute;fono Oficina 2</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('toficina3', set_value('toficina3'), 'id="toficina3" class="mask_phonedos" size="16"');?>
                                    
								  </div>
                                 
								</div>
							  </div>
                              
                              
                              
                              <div class="control-group">
								<label class="control-label" for="focusedInput">Celular</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php
									  if(isset($telefono)){$tel=$telefono; $datacel=$tel;}else{$tel='';$datacel=set_value('tcasa');}
									    echo form_input('tcasa',$datacel, 'id="tcasa" class="mask_phonedos" size="16" maxlength="10"');?>
                                    
								 </div>Solo n&uacute;meros
                                   <div class="msncelular"></div>
                                   <div class="msncelularf"></div>
								</div>
								
							  </div>
							  
							  
							  <div class="control-group">
								<label class="control-label" for="focusedInput">Radio</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                      <?php  echo form_input('radio', set_value('radio'), 'id="radio" size="16"');?>
                                    
								  </div>
                                   
								</div>
							  </div>
                              
                              
                             
                              
                              						
                            
                            <div class="control-group">
								<label class="control-label" for="prependedInput">Correo Electr&oacute;nico</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">@</span>
                                    <?php
									if(isset($correo)){$cor=$correo;}else{$cor='';}
									  echo form_input('correo', $cor,set_value('correo'), 'id="correo" size="16"');?>
                                    
								  </div>*<br>
								  <div class="buscandoemail"></div>
								 
								</div>
							  </div>
                              
                              
                               <div class="control-group">
								<label class="control-label" for="prependedInput">Correo Electr&oacute;nico alternativo</label>
								<div class="controls">
								  <div class="input-prepend">
									<span class="add-on">@</span>
                                    <?php  echo form_input('correob', set_value('correob'), 'id="correob" size="16"');?>
                                    
								  </div>
								 
								</div>
							  </div>
                              
                              
                             
                         
                            
                            
                            <div class="control-group">
								<label class="control-label" for="selectErrorxx">Fuente</label>
								<div class="controls">
									
									
									
		
									<select name="fuente" style="width:246px;">
										<option value="" <?php echo set_select('fuente','', TRUE); ?> >Seleccione una opcion</option>	
<?php foreach($todo_fuente as $todo): ?>	
							
<option value="<?php echo $todo->fue_IDfuente;?>" <?php echo set_select('fuente',''.$todo->fue_IDfuente.''); ?> ><?php echo $todo->fue_nombre;?></option>
<?php endforeach ?>
</select>* 
									
									
                               
						  
								</div>
							  </div>
							  
							  
							    
                            <div class="control-group">
								<label class="control-label" for="selectErrorxxp">Publicidad</label>
								<div class="controls">
                              	<select name="publicidad" style="width:246px;">
										<option value="" <?php echo set_select('publicidad','', TRUE); ?> >Seleccione una opcion</option>	
<?php foreach($todo_publicidad as $todo): ?>	
							
<option value="<?php echo $todo->pub_IDpublicidad;?>" <?php echo set_select('publicidad',''.$todo->pub_IDpublicidad.''); ?> ><?php echo $todo->pub_nombre;?></option>
<?php endforeach ?>
</select>* 
						  
								</div>
							  </div>
                              
                              
                              
                              <div class="control-group">
								<label class="control-label" for="selectError">Tipo de Contacto</label>
								<div class="controls">
                              <select name="status" style="width:246px;">
										<option value="" <?php echo set_select('status','', TRUE); ?> >Seleccione una opci&oacute;n</option>	

							
<option value="Prospecto" <?php echo set_select('status','Prospecto'); ?> >Prospecto</option>
<option value="Caliente" <?php echo set_select('status','Caliente'); ?> >Caliente</option>
<option value="Proceso de Venta" <?php echo set_select('status','Proceso de Venta'); ?> >Proceso de Venta</option>
<option value="Cliente" <?php echo set_select('status','Cliente'); ?> >Cliente</option>


</select>* 
                                
								  
								</div>
							  </div>
                            
      <div class="control-group">
								<label class="control-label" for="selectError">Prueba de Manejo</label>
								<div class="controls">
<select name="pmanejo" style="width:246px;">
<option value="" <?php echo set_select('pmanejo','', TRUE); ?> >Seleccione una opci&oacute;n</option>			
<option value="Si" <?php echo set_select('pmanejo','Si'); ?> >Si</option>
<option value="No" <?php echo set_select('pmanejo','No'); ?> >No</option>
</select>* 
                                
								  
								</div>
							  </div>                      
                              
                   
                              
                       <div class="control-group">
								<label class="control-label" for="prependedInput">Auto de Interes</label>
								<div class="controls">
								 <hr/>
								</div>
							  </div>         
                              
                            
                            
                             <div class="control-group">
								<label class="control-label" for="selectErrorff">Tipo de Auto</label>
								<div class="controls">
                                
                               <select name="tipoa" style="width:246px;">
										<option value="" <?php echo set_select('tipoa','', TRUE); ?> >Seleccione una opci&oacute;n</option>	

							
<option value="Nuevo" <?php echo set_select('tipoa','Nuevo'); ?> >Nuevo</option>
<option value="Seminuevo" <?php echo set_select('tipoa','Seminuevo'); ?> >Seminuevo</option>

</select>* 
                                
								  
								</div>
							  </div>
                              
                              
                             <div class="control-group">
								<label class="control-label" for="selectErroraa">Año</label>
                                
								<div class="controls">
                                <div class="input-prepend">
									<span class="add-on"></span>
                                  <?php  echo form_input('ano', set_value('ano'), 'id="ano" size="16"');?>
                              </div>*</div>
							  </div>
                              
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraam">Modelo</label>
                                
								<div class="controls">
									  <select name="modelo" style="width:246px;">
<option value="" <?php echo set_select('modelo','', TRUE); ?> >Seleccione una opci&oacute;n</option>						
<option value="ACCORD" <?php echo set_select('modelo','ACCORD'); ?> >Accord</option>
<option value="FIT" <?php echo set_select('modelo','FIT'); ?> >Fit</option>
<option value="CIVIC" <?php echo set_select('modelo','CIVIC'); ?> >Civic</option>
<option value="PILOT" <?php echo set_select('modelo','PILOT'); ?> >Pilot</option>
<option value="CRV" <?php echo set_select('modelo','CRV'); ?> >CRV</option>
<option value="ODYSSEY" <?php echo set_select('modelo','ODYSSEY'); ?> >Odyssey</option>
<option value="CITY" <?php echo set_select('modelo','CITY'); ?> >City</option>
<option value="CRZ" <?php echo set_select('modelo','CRZ'); ?> >CRZ</option>
<option value="CROSSTOUR" <?php echo set_select('modelo','CROSSTOUR'); ?> >Crosstour</option>
<option value="RIDGELINE" <?php echo set_select('modelo','RIDGELINE'); ?> >Ridgeline</option>
<option value="OTRO" <?php echo set_select('modelo','OTRO'); ?> >Otro ...</option>
</select>* 
									
                              
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraa">Versi&oacute;n</label>
                                
								<div class="controls">
                                <div class="input-prepend">
									<span class="add-on"></span>
                                  <?php 
								  if(isset($auto)){$au=$auto; $dataver=$au;}else{$au='';$dataver=set_value('version');}
								   echo form_input('version',$dataver, 'id="version" size="16"');?>
                              </div></div>
							  </div>
                              
                              
                               <div class="control-group">
								<label class="control-label" for="selectErroraac">Color</label>
                                
								<div class="controls">
                                <div class="input-prepend">
									<span class="add-on"></span>
                                 <?php  echo form_input('color', set_value('color'), 'id="color" size="16"');?>
								</div></div>
							  </div>
							  
							
							   <div class="control-group">
								<label class="control-label" for="selectErroraac">Notas</label>
                                
								<div class="controls">
                                <div class="input-prepend">
                             <?php  echo form_textarea('notas', set_value('notas'), ' style="width:230px; height:150px;"id="color" size="16"');?>
								</div></div>
							  </div>
                              
                             
                                
                            
							<div class="form-actions">
							 <?php echo form_submit('submit', 'Guardar Información','class="btn btn-primary btnhide"'); ?>
							  
							</div>
						  </fieldset>
						<?php echo form_close(); ?>
                                        
                                        
                                        
                                    </div>
                      

                          </div></div></div>            </section>
        </div>
        <!-- #main-wrapper end -->
        
        <div class="modal hide fade" id="myModaldetallec">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Informaci&oacute;n de Contacto Existente</h3>
			</div>
			<div class="modal-body">
				<div id="info-detalle"></div>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?>  
    </body>
</html>