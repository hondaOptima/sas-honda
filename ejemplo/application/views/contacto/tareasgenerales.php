<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Home"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>

				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "home"; $this->load->view('commons/menu-left',$data); ?>
            
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>">Escritorio</a>
					</li>
                    /
                    <li>
						<a href="#">Tareas Generales</a>
					</li>
				</ul>
				
			</div>
		
			
			
			
			<hr>
			
			
            <a href="<?php echo base_url(); ?>index.php/contacto/addtarea/"><button class="btn btn-small btn-primary">Crear Tarea</button></a>
			
			
		<div class="row-fluid sortable"><div class="box span12"><div class="box-header" data-original-title>
<h2><i class="icon-edit"></i><span class="break"></span>Actividades Generales
 	   
</h2>
<div class="box-icon"><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a></div></div>
<div class="box-content" style="display:block;">
  
  
  
<?php if($todo_generales): ?><?php foreach($todo_generales as $fidb): ?>

<?php

if($fidb->gen_status=='pendiente'){$class="alert-block";}if($fidb->gen_status=='realizada'){$class="alert-success";}
if($fidb->gen_status=='proceso'){$class="alert-info";}
echo '<div class="alert '.$class.'">
							
							
							<strong><a href="#" class="btn-minimizeB">'.$fidb->gen_titulo.'</strong></a> ['.$fidb->gen_status.']	
							
							
								
						</div>
				<div class="box-contentB" style="display:none">
				  <table>
                  <tr>
                  <td width="150px" class="ffa" >Fecha Inicio:</td>
                  <td width="150px"class="ff" >'.$fidb->gen_fecha_inicio.'</td>
                   <td width="150px"class="ffa" >Fecha fin:</td>
                  <td width="50px" style="text-align:right"  class="ff">'.$fidb->gen_fecha_fin.'</td>
                  </tr>
                  </table>
				  
				  <table width="600px;">
                  <tr><td width="600px"  class="ffa" >Descripcion:</td></tr>
				   <tr><td width="600px"  class="ff" >'.$fidb->gen_descripcion.'</td></tr>
                  
                  </table>
				  
				  <table width="550px;">
                  <tr><td width="150px"  class="ffa" >&nbsp;</td>
				  <td width="150px"  class="ffa" >&nbsp;</td>
				  <td width="150px"  class="ffa" >&nbsp;</td>
				   <td width="50px"  class="ffa" style="text-align:right" >
				   <a href="'.base_url().'index.php/contacto/vistatareagenerales/'.$fidb->huser_hus_IDhuser.'/'.$fidb->gen_IDgenerales.'">
				   <button class="btn btn-mini">Editar</button></a></td></tr>
                  
                  </table>
				
				
				
				</div>
						
						
						';
?>
               
<?php endforeach ?><?php else: ?>No<?php endif ?>	                
                            
                                    
                                    
</div></div> </div>
            
            
            

            
            
            
            
            
            
            
            
            
            
            
            
            
  <hr>          
            
            
            
            
            
            
            
            
            
            	<!--/#content.span10-->
				</div><!--/fluid-row-->
				
	
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>
		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	

</body>
</html>
