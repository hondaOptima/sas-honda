<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="llamada"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/hpp/llamada">Llamada entrante</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                     <li><a href="#">Centro de llamadas</a></li>
                                                                       
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
<?php echo $flash_message ?>
                 
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Lista de llamadas entrantes</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                            <table id="dt_table_tools" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                              <th>Fecha</th>
								  <th>Hora</th>
								  <th>Asesor</th>
								  <th>Auto </th>
                                  <th>Contacto</th>
                                  <th>Tel&eacute;fono</th>
                                  <th>Tipo de Llamada</th>
                                   <th>Nota</th>
                                    <th>Acciones</th>
							  </tr>
						  </thead>   
						  <tbody>
<?php if($todo_llamadasentrantes): ?><?php foreach($todo_llamadasentrantes as $fidb): ?>
<tr>

<td class="center"><?php echo $fidb->lle_fecha;?></td>
<td class="center"><?php echo $fidb->lle_hora;?></td>
<td class="center"><?php echo $fidb->lle_asesor_deventa;?></td>
<td class="center"><?php echo $fidb->lle_autointeres;?></td>
<td class="center"><?php echo $fidb->lle_nombre_cliente;?></td>
<td class="center"><?php echo $fidb->lle_telefono;?></td>
<td class="center"><?php echo $fidb->lle_tipo_llamada;?></td>
<td class="center"><?php echo $fidb->lle_nota;?></td>

								<td class="center">
   <div class="dropdown dropdown-right">
                                <a href="#" class="btn btn-mini" data-toggle="dropdown">Acciones</a>
                                <ul class="dropdown-menu">

<li><?php echo anchor("contacto/add/?TPO=".$fidb->lle_IDllamada_entrante."", 'Transferir'); ?></li> 
   	
<li><?php echo anchor("hpp/centrollamadaupdate/".$fidb->lle_IDllamada_entrante."", 'Editar'); ?></li> 
	</ul></div>
</td></tr>
 <?php endforeach ?><?php else: ?><?php endif ?>	 
								
</tbody></table>

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
   
  <?php $this->load->view('globales/js'); ?> 


 

    </body>
</html>