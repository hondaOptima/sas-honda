<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Editar Llamada Entrante"; $this->load->view('commons/head',$data); ?>  
       <style>
                    .ffa{font-size:.9em; font-weight:bold}
					 .ff{font-size:.9em; }
                    </style> 
</head>
<body >





<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="Perfectum Dashboard" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
				<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "contactos"; $this->load->view('commons/menu-left',$data); ?>
            <!--/span-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading"></h4>
					<p></p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				
 <ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a  href="<?php echo base_url(); ?>index.php/contacto">Contactos</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="javascript:window.history.go(-1);">Bitacora</a><span class="divider">/</span>
					</li>
                    
                    <li>
						<a href="">Tarea</a>
					</li>
                    
				</ul>
				
			</div>
		
			
			
			
			<hr>

 
	                 	
	                 	
	                 	
	                 	

                              
                                        
                             
                             
                              
                            
                              
  
  
   <a href="javascript:window.history.go(-1);"><button class="btn btn-small btn-success">Regresar</button></a>
  
    <?php if($uno_llamadasentrantes): ?>
                          <?php foreach($uno_llamadasentrantes as $todo): ?>
                           <?php
                           $idlla=$todo->lle_IDllamada_entrante; 
						    $cli=$todo->lle_nombre_cliente; 
						    $tel=$todo->lle_telefono; 
							$tip=$todo->lle_tipo_llamada;
							$ase=$todo->lle_asesor_deventa; 
							$not=$todo->lle_nota;
							$aui=$todo->lle_autointeres; 
							$clientei=$todo->lle_cliente_interes; 
							$email=$todo->lle_correo; 
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>			
	
	
<div class="row-fluid sortable"><div class="box span12"><div class="box-header" data-original-title>
<h2><i class="icon-edit"></i><span class="break"></span>Editar Llamada Entrante
 	   
</h2>
<div class="box-icon"><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a></div></div>
<div class="box-content">

		
		<?php echo form_open('hpp/centrollamadaupdate/'.$idlla.'','class="form-horizontal"'); ?>
		 
          
	
	 
	<div class="row-fluid">
								<div class="span4">
								<h6>Nombre del Asesor de Venta</h6N></h6>
								<p>
									
							  <div id="vendedores-select">
							  	<select name="asesor">
							  		 <?php if($todo_vendedores): ?>
                          <?php foreach($todo_vendedores as $todo): ?>
                           <?php
                           if($todo->hus_IDhuser ==$ase){ $val='selected="selected"';}else{ $val='';}
                           echo '<option '.$val.' value="'.$todo->hus_IDhuser.'">'.$todo->hus_nombre.' '.$todo->hus_apellido.'</option>';
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>	
							  		
							  		
							  	</select>
								
							  </div>
									
									
								</p>
                                
								</div>
							 <div class="span4"><h6>Auto de Interes Modelo Año</h6>
								<p><input type="text" name="autointeres" value="<?php echo $aui;?>" style="" ></p>
								</div>
							  <div class="span4">
								<h6>Nombre del Cliente</h6>
								<p><input type="text" value="<?php echo $cli;?>"  name="cliente" style="" ></p>
								 </div>
                                  
								
                               </div>
                               <br>
                                 <div class="row-fluid">
								
								
								<div class="span4">
								<h6>Cliente Interesado:</h6N></h6>
								<p><input type="text" style="width:160px;" value="<?php echo $clientei;?>  "name="lle_cliente_interes" ></p>
                                
								</div>
								
								<div class="span4">
								<h6>Telefono:</h6></h6>
								<p><input type="text" style="width:160px;"  value="<?php echo $tel;?>"  name="telefono" ></p>
                                
								</div>
								
								<div class="span4">
								<h6>Correo Electr&oacute;nico:</h6N></h6>
								<p><input type="text" style="width:160px;" value="<?php echo $email;?>"  name="lle_correo" ></p>
                                
								</div>
							
                                  
								
                               </div>
                             
	<br>
	
		<div class="row-fluid">
								<div class="span4">
								<h6>Llamada Entrante </h6>
								<p><input type="radio" <?php if($tip=='Llamada Entrante'){ echo 'checked="checked"';}?> name="grupoll" value="Llamada Entrante"  ></p>
                                
								</div>
							 <div class="span4"><h6>Call Back</h6>
								<p><input <?php if($tip=='Call Back'){ echo 'checked="checked"';}?> name="grupoll" value="Call Back" type="radio"  ></p>
								</div>
							  <div class="span4">
								<h6>Rec</h6>
								<p><input <?php if($tip=='Rec'){ echo 'checked="checked"';}?> type="radio" name="grupoll" value="Rec" ></p>
								 </div>
                                  
								
                               </div>
                               
                               
                               
<br>


<div class="row-fluid">
								<div class="span4">
								<h6>Nota:</h6N></h6>
								<p><textarea name="nota"><?php echo $not;?></textarea></p>
                                
								</div>
							
                                  
								
                               </div>              
                               
         
         
         
         

         
         <div class="form-actions">
            
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         	<?php echo form_close(); ?>     
            
         </div>
         
         </div>
         </div>
         
         
         
         
         
         
         
         
 
 
   </fieldset>
					                              
                                    
</div></div> </div>       </div>            
                          
  
      
      
      
                                  
<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
	

</body>

<script>
$(document).ready(function(){	
	
$('#unochecked').live('click',function(){	
	
var mcCbxCheck = $(this);
	 

	 
	 
	if(mcCbxCheck.is(':checked')) {
$('#displaycontb').show();
    }
    else{
    	$('#displaycontb').hide();

    }
	
});	
});	
	
</script>


</html>
