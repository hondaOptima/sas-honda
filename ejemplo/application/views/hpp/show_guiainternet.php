<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="javascript:history.go(-1)">Bitacora</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Editar - Guía Telefónica de Internet para Seguimiento</a></li>
                                                                       
                                </ul>
                          </section>


                       
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Editar - Guía Telefónica de Internet para Seguimiento </h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <br>
                                        
                                        <div style="font-size:1.3em;">
<div class="row-fluid" style="max-width:800px; margin:0 auto; position:relative;">
<div class="row-fluid">
<div class="span2"><img src="<?php echo base_url()?>img/topleft.jpg"></div>
<div class="span7" style="padding-top:30px; padding-bottom:20px; text-align:center; background-color:#333; color:white"><h4>Gu&iacute;a Telef&oacute;nica de Internet para Seguimiento</h4></div>						 
<div class="span3"><img src="<?php echo base_url()?>img/topright.jpg"></div>                               
 </div>
 <?php if($guiainternet): ?>

                          <?php foreach($guiainternet as $todo): ?>
                           <?php
						    $idg=$todo->gin_IDguiainternet; 
						    $eleccion=$todo->gin_eleccion; 
							$ambos=$todo->gin_ambos;
							$opcion1=$todo->gin_opcion1; 
							$opcion2=$todo->gin_opcion2;
							$opcion3=$todo->gin_opcion3; 
							$opcion4=$todo->gin_opcion4;
							$claro=$todo->gin_claro; 
							$numero=$todo->gin_numero;
							$apellido=$todo->gin_apellido; 
							$nombre=$todo->gin_nombre;
							$hoy=$todo->gin_hoymanana;
							$hora=$todo->gin_hora;
						    $posible=$todo->gin_posible; 
							$numero2=$todo->gin_numero2;
							$cita=$todo->gin_citalas;
							$notas=$todo->gin_notas;
							$localizarlo=$todo->gin_localizarlo;
							$porque=$todo->gin_porque;
							$idc=$todo->contacto_con_IDcontacto;
							$hnom=$todo->hus_nombre;
							$hape=$todo->hus_apellido;
							$hnum=$todo->hus_celular;
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>	
               
		
      	<?php echo form_open('hpp/guiastelefonicasinternet/'.$idg.'/'.$idc.'','class="form-horizontal"'); ?>
            <input type="hidden" name="idc" value="<?php echo $idc;?>">

<div class="row-fluid">
<div class="span8"></div>
						 
<div class="span2" style="border-bottom:
#333 1px solid;">
<b>Fecha:</b><?php echo date('d').'-'.date('m').'-'.date('Y');?>
</div>
<div class="span2" style="border-bottom:
#333 1px solid;">
<b>Hora:</b><?php echo date('H:i');?>
</div>                               
 </div>            
<br>
	  <p>1. Buenos días, tardes ó noches. Soy <b><?php echo $hnom.' '.$hape;?></b> es éste un buen momento para mi llamada? Recibí su<br>
solicitud de información. Tengo para usted varias otras opciones similares en estilo y precio.</p>
<br> 
      
      <p>2.- Sinceramente realice un cumplido por la elección del cliente.</p>
      
      
        <div class="row-fluid">
								<div class="span4" style="margin-left:25px;">
								<h6><input name="eleccion" value="nuevo" <?php if($eleccion=='nuevo'){echo "checked='checked'";}?>  type="checkbox"> Nuevo</h6>
                                
								</div>
							 <div class="span3">	<h6><input value="usado"  <?php if($eleccion=='usado'){echo "checked='checked'";}?> name="eleccion" type="checkbox"> Usado</h6>
                                
								</div>
							  <div class="span3">
								<h6><input name="eleccion" <?php if($eleccion=='periodico'){echo "checked='checked'";}?> value="periodico" type="checkbox">En el periódico</h6>
								 </div>
                                  
								
                               </div>
<br>
<div class="row-fluid" style="border: 1px solid #999;">	
	 <p style="margin-left:15px;">¿Qué es lo más importante, el modelo o el precio?<br>
      <b >Si ambos:</b> " 51% y el otro 49%" (Reafirme para crear flexibilidad)
      <br><br>
      </p>
</div>	 
<div class="row-fluid" style="border: 1px solid #999; border-top:none;">	
	 <p style="margin-left:15px;"><b>Suponga:</b> "Suponga que yo tenga una oferta especial que no he publicado todavía …..¿le gustaría conocer esa<br>información?<br><br></p>
        
</div>	
<br>
<p style="margin-left:15px;"><b>Sólo para estar seguro, ¿Qué equipo está usted esperando?</b></p>
<br>
	<div class="row-fluid">
								<div class="span2">
								
								<p><input  name="ambos" value="<?php echo $ambos;?>" type="text" style="width:110px"></p><h6>CD/Nav/Ambos</h6>
                                
								</div>
							 <div class="span2">
								<p><input name="opcion1" value="<?php echo $opcion1;?>" type="text" style="width:110px"></p><h6>Opci&oacute;n 1</h6>
								</div>
							  <div class="span2">
								
								<p><input name="opcion2" value="<?php echo $opcion2;?>" type="text" style="width:110px"></p><h6>Opci&oacute;n 2</h6>
								 </div>
                                  <div class="span2">
								
								<p><input name="opcion3" value="<?php echo $opcion3;?>" type="text" style="width:110px"></p><h6>Opci&oacute;n 3</h6>
                                
								</div>
							 <div class="span2">
								<p><input name="opcion4" value="<?php echo $opcion4;?>" type="text" style="width:110px"></p><h6>Opci&oacute;n 4</h6>
								</div>
							  <div class="span2">
								
								<p><input name="clarooscuro" value="<?php echo $claro;?>" type="text" style="width:110px"></p>
								<h6>Claro/Oscuro</h6>
                               </div>
	
	<div class="row-fluid">
								
								 </div>
                                  
								
                               </div>
      
    <br>
       
      <p>3.- (Repite el equipo)...Entonces lo qué usted desea es...</p>
      <br>
      <p>4.- Quiero verificar que el vehículo que Usted desea está disponible no quiero que Ud. venga sin tener una buena
razón sólo me tomará de 5 a 10 minutos............<a target="_blank" href="<?php echo base_url()?>index.php/inventario/?ubicacion=Piso&ubicacion3=Asignados&sucursal=0">Invenario</a>
      ¿Me está llamando de su casa ó de su trabajo?<br>
      
         <div class="row-fluid">
								<div class="span4"><p><center>Y el número es:</center></p>
								<h6><center><input name="sunumero" value="<?php echo $numero?>" type="text" style="width:110px"></center></h6>
                                
								</div>
							 <div class="span4"><p><center>y su Apellido se deletrea:</center></p>
								<h6><center><input name="apellido" value="<?php echo $apellido;?>"  type="text" style="width:110px"></center></h6>
                                
								</div>
							  <div class="span4">
								 <p><center>y su Nombre es.</center></p>
								<h6><center><input name="nombre" value="<?php echo $nombre;?>" type="text"style="width:110px"></center></h6>
								 </div>
                                  
								
                               </div>
Un placer en conocerlo.
      </p>
      <br>
         
<p>5.- Se me acaba de ocurrir algo, <b>¿Me puede esperar un momento ?</b></p>
<br>
<p>6.- Cree un sentido de urgencia profesional</b></p>
    
     <div class="row-fluid" style="border:1px solid #CCC">            
							 <div class="span3" >
								<h4><center>Lo Tiene</center></h4>
                                <hr/>
								<p style="margin-left:10px;">Buenas noticias!<br>
								Tenemos la Unidad exacta...<br>
								Necesitamos actuar <b>pronto</b>
								¿A menos que piensa Usted<br>
								esperar algunos ……….? 
								</p>
                                
								</div>
							  <div class="span3"  style="border-left:1px solid #CCC;">
								<h4><center>Similar</center></h4>
                                 <hr/>
								<p style="margin-left:10px;">Buenas noticias!<br>
								Tenemos varias opciones que cumplen con sus necesidades...<br>
								Necesitamos actuar <b>pronto</b>
								¿A menos .....?
								</p> </div>
                                  
								 <div class="span6" style="border-left:1px solid #CCC;" >
								<h4><center>No lo tiene (Nuevo)</center></h4>
                                 <hr/>
								<p style="margin-left:10px;">¿Necesita comprar esa unidad hoy?<br>
								Usted está bien <b>decidido por ese modelo</b> y no<br>
								quiere ver otras opciones...<br>
								¿Aún si esto tomará tiempo?<br>
								¿Cuándo sería el mejor momento para<br>
								reunirnos y empezar la.....?
								
								
								
								</p>
								 </div>
                               </div>
     
     7.
       <div class="row-fluid" style="border:1px solid #CCC">
								<div class="span2" ><p><center>¿Hoy ó mañana?:</center></p>
								<h6 style="margin-left:5px;"><center><input name="hoymanana" value="<?php echo $hoy;?>" type="text" style="width:110px"></center></h6>
                                
								</div>
							 <div class="span2"><p><center>Por la ma&ntilde;ana a medio dia o por la tarde</center></p>
								
                                
								</div>
							  <div class="span2">
								 <p><center>Hora:</center></p>
								<h6><center><input name="hora" value="<?php echo $hora;?>" type="text"style="width:110px"></center></h6>
								 </div>
                                  
								<div class="span2"><p><center>Tengo un pendiente a las...</center></p>
                                
								</div>
							  <div class="span4">
								 <p><center>Este/Oeste . Ser&iacute;a posible a las</center></p>
								<h6><center><input name="posiblealas" value="<?php echo $posible;?>" type="text"style="width:110px"></center></h6>
								 </div>
                                 
                               </div>
       
<br>
       
       <p>8. ¿Tiene una pluma a la mano? Lo espero.</p>
          <div class="row-fluid">
								<div class="span4">
								<h6><center>Mi nombre es:</center></h6>
                                <p><center><?php echo $hnom.' '.$hape;?> </center></p>
								</div>
							 <div class="span4"><h6><center>Mi Apellido se deletrea:</center></h6>
								<p><center><?php echo $hape;?> </center></p>
                                
								</div>
							  <div class="span4">
								 <h6><center>y mi número es:</center></h6>
								<p><center><?php echo $hnum;?></center></p>
								 </div>
                                  
								
                               </div>
    
       <p>y nuestra cita es para las:<input name="cita" value="<?php echo $cita;?>" type="text">.</p>
       <br>
       <p>9.¿Sabe dónde estamos ubicados?¿<b>De dónde viene</b>?</p>
       <br>
       <p>10.¿Sí Usted se demora ó tarda me podría llamar por favor?.(Pausa) " Yo haría los mismo por usted<b>por cortes&iacute;a</b>.
     De hecho sí algo sucede y no puedo localizarlo en este número. ¿Cuál sería la <b>siguiente mejor manera</b> en que puedo localizarlo?
       <input name="localizarlo" value="<?php echo $localizarlo;?>"  style=" width:100%" type="text">
       </p>
       <br>
         <div class="row-fluid" style="border: 1px solid #CCC">
         <div class="span4">
         <p style="margin-left:10px;">Notas:
       <br>
       <textarea name="notas" style="width:250px"> <?php echo $notas;?></textarea>
       </p>
         </div>
								<div class="span6"><br>
								<p style="margin-left:10px;"><b>Representante:</b><?php echo $hnom.' '.$hape;?></p>
								<p style="margin-left:10px;"><b>Confirmado por:</b><?php echo $hnom.' '.$hape;?></p>
                                <p style="margin-left:10px;"><b>Si no hizo cita, ¿Por qué?:</b><input name="porque" value="<?php echo $porque; ?>" type="text"style="width:110px"></p>
								 </div>
                                 </div>
       
  <br>     
    <div class="row-fluid" style="text-align:center; font-weight:bold">Copyright © 2011, ADP, Inc. All Rights Reserved.</div> 
            
   <div class="form-actions">
          
        <a href="<?php echo base_url();?>index.php/hpp/deleteguiaInternet/<?php echo $idc;?>/<?php echo $idg;?>"
         class="btn btn-warning">Eliminar</a> 
 
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         	<?php echo form_close(); ?>   
              
	</div>	</div>
        

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 


    </body>
</html>