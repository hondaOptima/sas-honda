<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/hpp/centrodellamadas">Centro de llamadas </a></li>
                                                                       
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
<?php echo $flash_message ?>

 <?php if($uno_llamadasentrantes): ?>
                          <?php foreach($uno_llamadasentrantes as $todo): ?>
                           <?php
                           $idlla=$todo->lle_IDllamada_entrante; 
						    $cli=$todo->lle_nombre_cliente; 
						    $tel=$todo->lle_telefono; 
							$tip=$todo->lle_tipo_llamada;
							$ase=$todo->lle_asesor_deventa; 
							$not=$todo->lle_nota;
							$aui=$todo->lle_autointeres; 
							$clientei=$todo->lle_cliente_interes; 
							$email=$todo->lle_correo; 
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>		
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Editar llamada entrante</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <?php echo form_open('hpp/centrollamadaupdate/'.$idlla.'','class="form-horizontal"'); ?>
		 
          
	
	 
	<div class="row-fluid">
								<div class="span4">
								<h6>Nombre del Asesor de Venta</h6N></h6>
								<p>
									
							  <div id="vendedores-select">
							  	<select name="asesor">
							  		 <?php if($todo_vendedores): ?>
                          <?php foreach($todo_vendedores as $todo): ?>
                           <?php
                           if($todo->hus_IDhuser ==$ase){ $val='selected="selected"';}else{ $val='';}
                           echo '<option '.$val.' value="'.$todo->hus_IDhuser.'">'.$todo->hus_nombre.' '.$todo->hus_apellido.'</option>';
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>	
							  		
							  		
							  	</select>
								
							  </div>
									
									
								</p>
                                
								</div>
							 <div class="span4"><h6>Auto de Interes Modelo Año</h6>
								<p><input type="text" name="autointeres" value="<?php echo $aui;?>" style="" ></p>
								</div>
							  <div class="span4">
								<h6>Nombre del Cliente</h6>
								<p><input type="text" value="<?php echo $cli;?>"  name="cliente" style="" ></p>
								 </div>
                                  
								
                               </div>
                               <br>
                                 <div class="row-fluid">
								
								
								<div class="span4">
								<h6>Cliente Interesado:</h6N></h6>
								<p><input type="text" style="width:160px;" value="<?php echo $clientei;?>  "name="lle_cliente_interes" ></p>
                                
								</div>
								
								<div class="span4">
								<h6>Telefono:</h6></h6>
								<p><input type="text" style="width:160px;"  value="<?php echo $tel;?>"  name="telefono" ></p>
                                
								</div>
								
								<div class="span4">
								<h6>Correo Electr&oacute;nico:</h6N></h6>
								<p><input type="text" style="width:160px;" value="<?php echo $email;?>"  name="lle_correo" ></p>
                                
								</div>
							
                                  
								
                               </div>
                             
	<br>
	
		<div class="row-fluid">
								<div class="span4">
								<h6>Llamada Entrante </h6>
								<p><input type="radio" <?php if($tip=='Llamada Entrante'){ echo 'checked="checked"';}?> name="grupoll" value="Llamada Entrante"  ></p>
                                
								</div>
							 <div class="span4"><h6>Call Back</h6>
								<p><input <?php if($tip=='Call Back'){ echo 'checked="checked"';}?> name="grupoll" value="Call Back" type="radio"  ></p>
								</div>
							  <div class="span4">
								<h6>Rec</h6>
								<p><input <?php if($tip=='Rec'){ echo 'checked="checked"';}?> type="radio" name="grupoll" value="Rec" ></p>
								 </div>
                                  
								
                               </div>
                               
                               
                               
<br>


<div class="row-fluid">
								<div class="span4">
								<h6>Nota:</h6N></h6>
								<p><textarea name="nota"><?php echo $not;?></textarea></p>
                                
								</div>
							
                                  
								
                               </div>              
                               
         
         
         
         

         
         <div class="form-actions">
            
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         	<?php echo form_close(); ?>     
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        
				
	
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 


 

    </body>
</html>