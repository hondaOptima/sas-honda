<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="hpp"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Honda Prospecting program</a></li>
                                                                       
                                </ul>
                          </section>

                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Vistas pdf</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                          
 <div  style="width:100%">
                                                    <div class="tabbable tabbable-bordered">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a data-toggle="tab" href="#tb1_a">Formatos Hpp</a></li>
                                                            <li><a data-toggle="tab" href="#tb1_b">Encuestas productos Honda</a></li>
                                                           
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div id="tb1_a" class="tab-pane active">
<object data="<?php echo base_url();?>readpdf/web/guiaReferidos.pdf" width="100%" height="600" type="application/pdf">
alt : <a href="<?php echo base_url();?>readpdf/web/guiaReferidos.pdf">archivo.pdf</a>
</object>
                                                            </div>
                                                            <div id="tb1_b" class="tab-pane">
<object data="<?php echo base_url();?>readpdf/web/uno.pdf" width="100%" height="600" type="application/pdf">
alt : <a href="<?php echo base_url();?>readpdf/web/uno.pdf">archivo.pdf</a>
</object>
                                                            </div>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 

    </body>
</html>