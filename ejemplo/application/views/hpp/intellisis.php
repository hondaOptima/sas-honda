<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="hpp"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Ventas de Mes Intellisis</a></li>
                                                                       
                                </ul>
                          </section>

                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
                    
                     
                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
<div class="row-fluid"><div class="span12">
<div class="box_a">
                                    
<?php echo $flash_message ?> 
                          
<div class="box_a_heading"> <h3>Ventas Intellisis</h3> </div>
<div class="box_a_content">
<table id="foo_example" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                               <th data-hide="phone">Sucursal</th>
								   <th data-hide="all" >Almacen</th>
								  <th data-hide="phone,tablet">Movimiento</th>
								  <th  >Factura</th>
								  <th data-hide="phone">Fecha</th>
								  <th data-hide="phone">Cliente</th>
                                  <th >Modelo</th>
                                 <th  data-hide="phone,tablet">Version</th>
                                  <th data-hide="phone,tablet">Eje..</th>
                                  <th data-hide="phone">Color</th>
                                  <th data-hide="phone">Vin</th>
                                  <th data-hide="all">Fecha Compras</th>
                                  <th data-hide="phone">Dias Inv</th>
								   <th data-hide="all" >Precio Venta</th>
								  <th data-hide="all">ISAN</th>
								  <th data-hide="all" >IVA</th>
								  <th data-hide="all">Total</th>
								  <th  data-hide="all">Costo</th>
                                  <th data-hide="all">Utilidad</th>
                                 <th data-hide="tablet">Agente</th>
                                 <th data-hide="tablet">Entrega</th>
                                 <th data-hide="tablet">Status</th>
                                 
								 
                                 
                                 
                                
							  </tr>
						  </thead>   
						  <tbody>


 <?php foreach($todo_piso as $todo): ?>
<?php if ($todo[0]=='TIJUANA' || $todo[0]=='Mexicali' || $todo[0]=='Ensenada'){?>
<tr>
<td>  <?php echo $todo[0];?></td>
<td  >  <?php echo $todo[1];?></td>
<td >  <?php echo $todo[2];?></td>
<td > <?php echo $todo[3];?> </td>
<td>  <?php  echo $todo[4];?></td>
<td >  <?php $cli=ucwords(strtolower($todo[5])); echo htmlentities($cli);?></td>
<td >  <?php  echo $todo[6];?></td>
<td >  <?php echo $todo[7];?></td>
<td >  <?php  echo $todo[8];?></td>
<td > <?php echo $todo[9];?> </td>
<td > <?php echo $todo[10];?></td>
<td >  <?php echo $todo[11];?></td>
<td >  <?php echo $todo[12];?></td>
<td > <?php echo $todo[13];?> </td>
<td >  <?php  echo $todo[14];?></td>
<td >  <?php echo ucwords(strtolower($todo[15]));?></td>
<td >  <?php  echo $todo[16];?></td>
<td >  <?php echo $todo[17];?></td>
<td >  <?php  echo $todo[18];?></td>
<td > <?php $ase=ucwords(strtolower($todo[19])); echo htmlentities($ase);?> </td>
<td > <?php $apar=$this->Reportesmodel->entregaAuto($todo[10]); if($apar->dat_fecha_entrega=='0000-00-00'){echo '<span class="label label-warning">Pendiente</span>';}else{ echo '<span class="label label-success">'.$apar->dat_fecha_entrega.'</span>'; } ?> </td>
<td > <?php if($apar->prcv_status=='venta'){echo '<span class="label label-success"> Entregado</span>';}
else{ echo '<span class="label label-warning">Pendiente</span>';}?> </td>
</tr>
<?php } ?>
<?php endforeach ?>
</tbody>
</table>

 </div>
<br><br>

                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
 
  <?php $this->load->view('globales/js'); ?> 
  
  <script type='text/javascript'>

	$(document).ready(function() {
	$('#foo_example').dataTable( {
		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
	$('#foo_example2').dataTable( {
		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example2_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
	
} );
</script>


    </body>
</html>