<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="llamada"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Llamada entrante</a></li>
                                                                       
                                </ul>
                          </section>


                       <div class="stat_boxes">
                                    <div class="row-fluid">
                                       
<?php echo $flash_message ?>

 <a class="btn btn-small " style="padding-top:-4px;" href="<?php echo base_url();?>index.php/hpp/centrodellamadas">Centro de Llamadas</a>
  <br><br>                  

                              
                              </div></div>
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Llamada Entrante</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                          <?php echo form_open('hpp/llamada/','class="form-horizontal"'); ?>
          
	<input style="width:160px; " type="hidden" value="nuevo"  name="tipollamada"  >
	
	
	<div class="row-fluid">
								<div class="span4">
								<h6>Nombre del Asesor de Venta</h6>
								<p>
									
						 
                                
                                <?php
                              
								echo form_dropdown('asesor', $asesores_de_venta,'','style="width:246px;" id="s2_single" data-rel="chosen"');
								?>
                                
								  
							
									
									
								</p>
                                
								</div>
							 <div class="span4"><h6>Auto de Interes Modelo Año</h6>
								<p>
									
									<select style="width:160px;" name="autointeres">
										
									<option value="ninguno">Seleccione uns opci&oacute;n</option>	
									<option value="ACCORD">Accord</option>
<option value="FIT">Fit</option>
<option value="CIVIC">Civic</option>
<option value="PILOT">Pilot</option>
<option value="CRV">CRV</option>
<option value="ODISSEY">Odissey</option>
<option value="CITY">City</option>
<option value="CRZ">CRZ</option>
<option value="OTRO">Otro</option>
										
										
									</select>
									
								</p>
								</div>
							  <div class="span4">
								<h6>Nombre del Cliente</h6>
								<div class="icon-search llamadaEntrante" style="padding-left: 11px; color: green; margin-top: 5px; cursor: pointer"></div>
								<p style="float: left"><input style="width:160px; "  type="text" name="cliente"  ></p>
								 </div>
                                  
								
                               </div>
                           
                               
                               <div class="row-fluid">
								
								
								<div class="span4">
								<h6>Cliente Interesado:</h6N></h6>
								<p><input type="text" style="width:160px;"  name="lle_cliente_interes" ></p>
                                
								</div>
								
								<div class="span4">
								<h6>Tel&eacute;fono:</h6N></h6>
								<p><input type="text" style="width:160px;"  name="telefono" ></p>
                                
								</div>
								
								<div class="span4">
								<h6>Correo Electr&oacute;nico:</h6N></h6>
								<p><input type="text" style="width:160px;"  name="lle_correo" ></p>
                                
								</div>
							
                                  
								
                               </div>
                             
	
	
		<div class="row-fluid">
								<div class="span4">
								<h6>Llamada Entrante </h6>
								<p><input type="radio" name="grupoll" style="margin-left:5px" value="Llamada Entrante"  ></p>
                                
								</div>
							 <div class="span4"><h6>Call Back</h6>
								<p><input name="grupoll" value="Call Back" style="margin-left:5px" type="radio"  ></p>
								</div>
							  <div class="span4">
								<h6>Rec</h6>
								<p><input type="radio" name="grupoll" style="margin-left:5px" value="Rec" ></p>
								 </div>
                                  
								
                               </div>
                               
                               
                               
<br>


<div class="row-fluid">
								<div class="span4">
								<h6>Nota:</h6N></h6>
								<p><textarea name="nota"></textarea></p>
                                
								</div>
							
                                  
								
                               </div>              
                               
		
	

		
		
			<div class="form-actions">
							 <?php echo form_submit('submit', 'Guardar Informacion','class="btn btn-primary "'); ?>
							  
							</div>
                            </form>	

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        
                                        	<?php $todo_contacto;?>
				
		<div class="modal hide fade" id="myModalDecidete">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Tipo de Contacto</h3>
				
			</div><?php echo form_open('contacto/decide','class=""'); ?>
			<div class="modal-body">
			
				<p>
					 <div class="control-group">
								<label class="control-label" for="selectErrorrt">Buscar Contacto</label>
								<div class="controls">
                                
                   <input style="margin-top:10px;" type="text" name="buscarcontacto"><a class="btn btn-small addnuevo" style="padding-top:-4px;" href="#">Agregar nuevo Contacto</a>
								  
								</div>
							  </div>
                              <div class="respuestatabla"></div>
</p>
				
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cerrar</a>

			</div>	<?php echo form_close(); ?>
		
		</div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 
<script type='text/javascript'>

	$(document).ready(function() {
		
		$('.llamadaEntrante').click(function(e){
		e.preventDefault();
		$('#myModalDecidete').modal('show');
	});
	
		$('input[name=buscarcontacto]').live('keyup',function(){	
		var valor=$(this).val();
		
   $.ajax({
   type: 'POST',
   url: '<?php echo base_url(); ?>index.php/hpp/buscarcontacto/',
   data: 'valor='+valor+'',   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
   $('.respuestatabla').html(data);
   }
});
	});
	
	
	$('.addnuevo').live('click',function(e){	
		var valor=$('input[name=buscarcontacto]').val();
		
		$('input[name=cliente]').val(valor);
		$('input[name=lle_cliente_interes]').val(valor);
		 e.preventDefault();
		$('#myModalDecidete').modal('hide');
	
	});
	
	
	$('.addexistente').live('click',function(e){	
	var id=$(this).attr('id');
	var elem = id.split('[');
valor = elem[0];
telefono = elem[1];
correo = elem[2];
idcontacto = elem[3];
		
		$('input[name=cliente]').val(valor);
		$('input[name=lle_cliente_interes]').val(valor);
		$('input[name=telefono]').val(telefono);
		$('input[name=lle_correo]').val(correo);
		$('input[name=tipollamada]').val(idcontacto);
		
		 e.preventDefault();
		$('#myModalDecidete').modal('hide');
	
	});
	
		
		
		$('.active-result').live('click',function(){	

var idc=$('select[name=tipocontactosave]').val();
var id=$(this).attr('id');

var elem = id.split('_');
nombre = elem[0];
mes = elem[1];

if(nombre=="selectErrorrt"){ 

if(idc!='generales'){
//$('#displaycont').show();
}
else{}

}
});
		
	});
	</script>

 

    </body>
</html>