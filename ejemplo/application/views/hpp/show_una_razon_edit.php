<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   
          
    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                            
                        </div>
                    </div>
                </div>
            </header>
            
           

            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">

                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/contacto">Contactos</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="javascript:history.go(-1)">Bitacora</a></li>
                                    <li class="crumb_sep"><i class="elusive-icon-play"></i></li>
                                    <li><a href="#">Editar - Hoja de "Una Razón" </a></li>
                                                                       
                                </ul>
                          </section>


                       
                   
                    <!-- jPanel sidebar -->
                   
                <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
                                    
                                    
                                        <div class="box_a_heading">
                                            <h3>Editar - Hoja de "Una Razón"  </h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <br>
                                        
                                        <div >
  <?php if($uno_contacto): ?>
                          <?php foreach($uno_contacto as $todo): ?>
                           <?php
						    $nom=$todo->con_nombre; 
						    $ape=$todo->con_apellido; 
							$tof=$todo->con_telefono_officina;
							$tca=$todo->con_telefono_casa; 
							$cor=$todo->con_correo;
							$cob=$todo->con_correo_b; 
							$cal=$todo->con_calle;
							$col=$todo->con_colonia; 
							$ciu=$todo->con_ciudad;
							$cp=$todo->con_codigo_postal; 
							$ocu=$todo->con_ocupacion;
							$cum=$todo->con_cumpleanos;
							
							$fue=$todo->fue_nombre;
						
							$hus=$todo->huser_hus_IDhuser;
							$hnom= $todo->hus_nombre;
							$hape= $todo->hus_apellido;
							$idc=$todo->con_IDcontacto;
							$sta=$todo->con_status;
							$mod=$todo->con_modelo;
							$ano=$todo->con_ano;
							$col=$todo->con_color;
						    $tip=$todo->con_tipo;
							
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>	
    <?php if($todo_hojaRazon): ?>
                          <?php foreach($todo_hojaRazon as $todo): ?>
                           <?php
						    $idr=$todo->hoj_IDhojaunarazon; 
						    $fec=$todo->hoj_fecha; 
							$cli=$todo->hoj_cliente;
							$ano=$todo->hoj_unidadano; 
							$tip=$todo->hoj_unidadtipo;
							$te1=$todo->hoj_telefonomejor; 
							$te2=$todo->hoj_telefonosiguiente;
							$te3=$todo->hoj_telefono; 
							$em1=$todo->hoj_emailmejor;
							$em2=$todo->hoj_emailsiguiente; 
							$pen=$todo->hoj_pendiente;
							$nve=$todo->hoj_nombrevehi;
							 $com=$todo->hoj_comparado;
						    $sor=$todo->hoj_sorprendio; 
							 $uni=$todo->hoj_unicarazon;
							$cog=$todo->hoj_comentgerente;
							$ffi=$todo->hoj_fechafin;
							$hor=$todo->hoj_hora;
							$min=$todo->hoj_min;
							$con=$todo->hoj_confirmado;

							
							
						   ?>
						   <?php endforeach ?>
	                 <?php else: ?>None<?php endif ?>		
               
		
<div style="font-size:1.3em;">
<div class="row-fluid" style="max-width:800px; margin:0 auto; position:relative;">
<div class="row-fluid">
<div class="span2"><img src="<?php echo base_url()?>img/topleft.jpg"></div>
<div class="span7" style="padding-top:30px; padding-bottom:20px; text-align:center; background-color:#333; color:white">
<h4> Hoja de "Una Razón"
</h4></div>						 
<div class="span3"><img src="<?php echo base_url()?>img/topright.jpg"></div>                               
 </div>

            
<?php echo form_open('contacto/vistaHojaRazon/'.$idc.'/'.$idr.'','class="form-horizontal"'); ?>
		 
            <input type="hidden" name="idc" value="<?php echo $idc;?>">
            <input type="hidden" name="idc" value="<?php echo $idc;?>">
	<br>
	<div class="row-fluid">
<div class="span9">
<b>Asesor de Ventas:</b><?php echo $hnom.' '.$hape;?>
</div>
						 
<div class="span3" style="border-bottom:
#333 1px solid;">
<b>Fecha:</b><?php echo date('d').'-'.date('m').'-'.date('Y');?>
</div>                              
 </div>            
<br>

	
	
	
	<div class="row-fluid" style="background-color:#CCC; border:1px solid #999">
<p style="margin-left:10px; margin-top:10px;">
<b>Cliente</b> <b style="margin-left:10px;">Nombre: </b> <?php echo $nom.' '.$ape?>
</p>

<div class="row-fluid" style="margin-top:8px;">
<div class="span4">
<p  style="margin-left:10px;">
<b>Unidad</b> <b style="margin-left:10px;">Año: </b> <?php echo $ano;?>
</p>
</div>
<div class="span3">
<b style="margin-left:10px;">Modelo: </b> <?php echo $mod;?>
</div>
<div class="span3">
<b style="margin-left:10px;">Tipo: </b> <?php echo $tip?>
</div>
</div>

<div class="row-fluid">
<div class="span2">
<b style="margin-left:10px;">Tel&eacute;fono</b>
</div>

<div class="span5">
<p><b>Mejor durante el d&iacute;a:</b><input class="span4" type="text" name="telefonomejor"value="<?php echo $te1;?>"/></p>
 </div>
 
 <div class="span5">
<p><b>Siguiente mejor:</b><input class="span6"  value="<?php echo $te2;?>" name="telefonosiguinete"type="text"  ></p>
 </div>
 
 </div>  
 
<div class="row-fluid">
<div class="span2"><b style="margin-left:10px;"></b></div>
<div class="span5">
<p><b>Otro:</b><input name="telefonootro" value="<?php echo $te3;?>" type="text" class="span8" ></p>
</div>
<div class="span5"> </div>
</div>  


<div class="row-fluid">
<div class="span2">
<b style="margin-left:10px;">E-mail</b>
</div>

<div class="span5">
<p><b>Mejor :</b><input class="span7" type="text"  name="correomejor" value="<?php echo $em1;?>"/></p>
 </div>
 
 <div class="span5">
<p><b>Siguiente mejor:</b><input class="span6" type="text" name="correosiguiente" value="<?php echo $em1;?>"></p>
 </div>
 
 </div> </div>
 
 <br>
 <ul>
 <li>¿Es este un buen momento para recibir mi llamada?</li>
 <li>Estoy llamando para AGRADECRELE …….</li>
 </ul>
<br> 

 <div style="margin-left:0px;">        
<p>1.- ¿Tiene usted algún tipo de pregunta pendiente?<br>
<textarea name="preguntapen" style="width: 100%; margin-top: 10px; "><?php echo $pen;?></textarea></p>
<br>
<p>2.- ¿Le gustó el (nombre del vehículo)?<br>
<textarea name="nomveiculo" style="width: 100%;  margin-top: 10px; "><?php echo $nve;?></textarea></p>
<br>
				<p>3.- ¿Comparado con qué?<br>
<textarea name="comparado" style="width: 100%; ; margin-top: 10px; "><?php echo $com;?></textarea></p>
<br>
				<p>4.- ¿Qué fué lo que más le sorprendió del (nombre del vehículo)?<br>
<textarea name="sorprendio" style="width: 100%;  margin-top: 10px; "><?php echo $sor;?></textarea></p>
<br>
				
<p>5.- ¿Hubo alguna razón que lo detuviera a usted para dar el siguiente paso?
¿Cuál sería esa razón?<br>
<textarea name="unicarazon" style="width: 100%;  margin-top: 10px; "><?php echo $uni;?></textarea></p>
<br>
				
				<br>
				
<p>Comentarios del gerente<br>
<textarea name="comentario" style="width: 100%;  margin-top: 10px; "><?php echo $cog;?></textarea></p>
<br>	
</div>

<div class="row-fluid">
								<div class="span3">
								<h6>Fecha de la Aplicaci&oacute;n</h6>
								<p><input name="fechafin" value="<?php echo $ffi;?>" type="text" style="width:110px" ></p>
                                
								</div>
							 <div class="span3"><h6>Hora</h6>
								<p><input name="hora" value="<?php echo $hor;?>" type="text" style="width:110px"></p>
								</div>
								
								 <div class="span3"><h6>Minutos</h6>
								<p><input name="minutos" value="<?php echo $min;?>"  type="text" style="width:110px"></p>
								</div>
                                
                                	 <div class="span3"><h6>Confirmado por:</h6>
								<p><input name="confirmado" value="<?php echo $hnom.' '.$hape;?>" type="text" readonly="readonly"></p>
								</div>
							  
								
                               </div>
<br>
      <div class="form-actions">
            <?php echo anchor("contacto/deletehojarazon/".$idc."/".$idr."/", ' <div class="btn btn-small btn-danger">Eliminar</div>',array('onClick' => "return confirm('Esta seguro que desea eliminar esta forma ?')",'class'=>"")); ?> 
         
 
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         	<?php echo form_close(); ?>
        
      <div class="row-fluid" style="text-align:center; font-weight:bold">Copyright © 2011, ADP, Inc. All Rights Reserved.</div>      



	</div>	</div>
              
	</div>	</div>
        

 </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?> 
  <?php $this->load->view('globales/js'); ?> 


    </body>
</html>