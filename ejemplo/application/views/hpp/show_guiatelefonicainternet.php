<!DOCTYPE html>
<html lang="en">
<head>
	  <?php $data["title"] = "Guias Telefonicas Internet"; $this->load->view('commons/head',$data); ?>   
</head>
<body >





<div id="center"></div>
<div id="overlay">
		<ul>
		  <li class="li1"></li>
		  <li class="li2"></li>
		  <li class="li3"></li>
		  <li class="li4"></li>
		  <li class="li5"></li>
		  <li class="li6"></li>
		</ul>
	</div>	
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.html"> <img alt="" src="<?php echo base_url(); ?>img/logo.png" /></a>
								
				<!-- start: Header Menu -->
					<?php  $this->load->view('commons/menu-top'); ?>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php $data["clase"] = "eventos"; $this->load->view('commons/menu-left',$data); ?>
			<!-- end: Main Menu -->
			
			
			
			<div id="content" class="span10">
			<!-- start: Content -->
			
			<div>
				<hr>
				<ul class="breadcrumb">
					
					<li>
						<a href="<?php echo base_url(); ?>index.php/home">Escritorio</a><span class="divider">/</span>
					</li>
                    <li>
						<a href="#">Guias Telefonicas Internet</a>
					</li>
				</ul>
				
			</div>
		
			
			
			
			<hr>
      
 
			
		
           
           
            
            
 
 
<?php echo $flash_message; ?>


		
			
            
            
            	<div class="row-fluid sortable">
                
                
             
                
                		
				<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-tasks"></i><span class="break"></span>Centro de Llamadas.</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
							
						</div>
					</div>
					<div class="box-content">
                    <?php if($_SESSION['nivel']=='Administrador') {?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Eleccion</th>
								  <th>Nombre</th>
								  <th>Apellido</th>
								  <th>Telefono</th>
                                  <th>Notas</th>
                                 
                                  
                                   <?php if($_SESSION['nivel']=='Administrador'){ ?>
                                   <th>Acciones</th>
								        <?php } ?>      
							  </tr>
						  </thead>   
						  <tbody>						
<?php if($todo_guias_telefonicas_internet): ?><?php foreach($todo_guias_telefonicas_internet as $fidb): ?>
<tr>

<td class="center"><?php echo $fidb->gin_eleccion;?></td>
<td class="center"><?php echo $fidb->gin_nombre;?></td>
<td class="center"><?php echo $fidb->gin_apellido;?></td>
<td class="center"><?php echo $fidb->gin_numero;?></td>
<td class="center"><?php echo $fidb->gin_notas;?></td>

 
								<td class="center">
                              
    
                             
										
									
								</td>



</tr>
      
  <?php endforeach ?><?php else: ?><?php endif ?>	 
								
							
						  </tbody>
					  </table> 
                      
                      <?php } else {?>
                      
                      <table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							<tr>
								  <th>Eleccion</th>
								  <th>Nombre</th>
								  <th>Apellido</th>
								  <th>Telefono</th>
                                  <th>Notas</th>
                                  
                   
                                   <th>Acciones</th>
								
							  </tr>
						  </thead>   
						  <tbody>
							
<?php if($todo_guias_telefonicas_internet): ?><?php foreach($todo_guias_telefonicas_internet as $fidb): ?>
<tr>

<td class="center"><?php echo $fidb->gin_eleccion;?></td>
<td class="center"><?php echo $fidb->gin_nombre;?></td>
<td class="center"><?php echo $fidb->gin_apellido;?></td>
<td class="center"><?php echo $fidb->gin_numero;?></td>
<td class="center"><?php echo $fidb->gin_notas;?></td>

								<td class="center">
                              
    <div class="btn btn-info" >
<i class="icon-eye-open icon-white"></i>
</div>

<div class="btn btn-danger" >
<i class="icon-trash icon-white"></i>
</div>
                         
										
									
								</td>


</tr>
      
  <?php endforeach ?><?php else: ?><?php endif ?>	 
								
							
						  </tbody>
					  </table>
                      
                      <?php } ?>
                                 
					</div>
				</div><!--/span-->
			
		
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/span-->
			
   
			
			
       
					<hr>
			<!-- end: Content -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
                
                	</div><!--/fluid-row-->
				
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<footer>
			<?php  $this->load->view('commons/footer'); ?>

		</footer>
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

	
		

</body>
</html>
