<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.back()">Plan de Ventas</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->                    

                 <div class="row-fluid">
                                <div class="span12">

<div class="box_a"> <?php
 echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>');


 echo form_open('reporte/editarplanventas/'.$id.'','class="form-horizontal"'); ?>
 
 <div class="box_a_heading"> <h3>Editar Plan de Ventas.:</h3></div><div class="box_a_content">


<table class="table" align="center">
                        <thead>
                            <tr>
                                <th >Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                
                            </tr>
                        </thead>
                        <tbody>
    <?php foreach($planVentas as $todo): ?>                       
                            <tr>
                                <td><?php echo $todo->descripcion;?></td>
                                <th><input style="width:25px" type="text" maxlength="3" name="ene" value="<?php echo $todo->plv_ene;?>" title="<?php echo $todo->descripcion;?>/Enero"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="feb" value="<?php echo $todo->plv_feb;?>" title="<?php echo $todo->descripcion;?>/Febrero"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="mar" value="<?php echo $todo->plv_mar;?>" title="<?php echo $todo->descripcion;?>/Marzo"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="abr" value="<?php echo $todo->plv_abri;?>" title="<?php echo $todo->descripcion;?>/Abril"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="may" value="<?php echo $todo->plv_may;?>" title="<?php echo $todo->descripcion;?>/Mayo"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="jun" value="<?php echo $todo->plv_jun;?>" title="<?php echo $todo->descripcion;?>/Junio"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="jul" value="<?php echo $todo->plv_jul;?>" title="<?php echo $todo->descripcion;?>/Julio"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="ago" value="<?php echo $todo->plv_ago;?>" title="<?php echo $todo->descripcion;?>/Agosto"></th>
                             <th><input style="width:25px" type="text" maxlength="3" name="sep" value="<?php echo $todo->plv_sep;?>" title="<?php echo $todo->descripcion;?>/Septiembre"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="oct" value="<?php echo $todo->plv_oct;?>" title="<?php echo $todo->descripcion;?>/Octubre"></th>
                            <th><input style="width:25px" type="text" maxlength="3" name="nov" value="<?php echo $todo->plv_nov;?>" title="<?php echo $todo->descripcion;?>/Noviembre"></th>                            <th><input style="width:25px" type="text" maxlength="3" name="dic" value="<?php echo $todo->plv_dic;?>" title="<?php echo $todo->descripcion;?>/Diciembre"></th>
                            </tr>   
                          
 <?php endforeach ?>
</tbody>
</table>


</div><?php

echo form_submit('submit', 'Guardar Informacion','class="btn btn-primary"');	
?></div>





</div></div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>