<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                            <div class="row-fluid">
                                <div class="stat_boxes">
                                <div class="span3 stat_box"> 
                                            <div class="peity_canvas peity_line_down">16,8,12,7,6,5,3</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/">
                                            Actividades Realizadas
                                            </a>
                                            </p>
                                        </div>
                                        <div class="span3 stat_box"> 
                                           <div class="peity_canvas peity_bar_up">2,5,3,6,8,5</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/ventas">
                                            Ventas de Mes
                                            </a>
                                            </p>
                                        </div>
                                         <div class="span3 stat_box"> 
                                             <div class="peity_canvas peity_line_up">3,5,9,7,12,8,16</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/inventario">
                                            Inventario consumido
                                            </a>
                                            </p>
                                        </div>
                                        
                                         <div class="span3 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-th-list" ></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/>/Caida">
											</a></h2>
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/ventas/">
                                            Diseñador de consulta
                                            </a>
                                            </p>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            
                     
                    
                   <br>  
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Diseñador de consultas</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<div class="row-fluid">                                        
      <form  name="form1" method="get"  action="<?php echo base_url(); ?>index.php/reporte/consulta">
      
  
      
<div class="span3"><h4 class="heading_b">Usuarios y Prospectos</h4>
<div class="row-fluid">
<table>
<tr><td><input type="submit" name="usuarios" value="Usuarios(Administradores y Asesores de Venta)"></td></tr>
<tr><td><input type="submit" name="contacto" value="Contacto (Prospecto,Caliente,Proceso de Venta)"></td></tr>
</table>
</div>  
</div>
<div class="span3"><h4 class="heading_b">Clientes</h4>
<div class="row-fluid">
<table>

<tr><td><input type="submit" name="factura" value="Cliente por Factura"></td></tr>

</table>
</div>  
</div>

                                                </form> 
</div>                                      
<br>


 

 
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>