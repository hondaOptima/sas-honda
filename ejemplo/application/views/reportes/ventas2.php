<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                  
                    
                   <br>  
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Diseñador de Consultas</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<div class="row-fluid">                                        
  <form  name="form1" method="get"  action="<?php echo base_url(); ?>index.php/reporte/ventas2">
   <div class="span3"><h4 class="heading_b">Seleccione un rango de fechas de facturación</h4>
  
 <div class="row-fluid">
 
 <input class="span4"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $finicio;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
<input class="span4" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $ffin;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
</div>  </div>

<?php if($_SESSION['username']=='josemaciel@hondaoptima.com') { ?>
 <div class="span3">
                                                    <h4 class="heading_b">Ciudad</h4>
<div class="span3 "><div class="control-group"><div class="controls">
<select name="ciudad">
<option  value="0" <?php if($ciudad==0){ echo 'selected="selected"';}?>>Todas las ciudades</option>
<option value="Tijuana" <?php if($ciudad=="Tijuana"){ echo 'selected="selected"';}?>>Tijuana</option>
<option value="Mexicali" <?php if($ciudad=="Mexicali"){ echo 'selected="selected"';}?>>Mexicali</option>
<option value="Ensenada" <?php if($ciudad=="Ensenada"){ echo 'selected="selected"';}?>>Ensenada</option>
</select>
</div></div></div>                                                    
                                                    
                                                </div>
                                                <?php } else{?> 
                                                 
 <div class="span3">
                                                    <h4 class="heading_b">Seleccione un asesor</h4>
<div class="span3 "><div class="control-group"><div class="controls">
 <?php echo form_dropdown('vendedor', $todo_vendedores,$vendedor,'style="width:246px;" id="s2_single" data-rel="chosen"');?>
</div></div></div>
                                                </div>
                                              
    <?php } ?>                                          

                                                                                            
                                                 <div class="span2">
                                                    <h4 class="heading_b">Hacer la consulta</h4>
                                                    <button type="submit" class="btn btn-primary" >Consultar</button>
                                                </div>
                                                </form>  
</div>                                      
<br>


 <div class="box_a_heading">
                                            <h3>Ventas Entregados y Facturados con Fecha de Entrega</h3>
                                        </div>
                                        <div class="box_a_content no_sp">
                                        
                                  <table id="foo_example"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr><?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
                                  <th data-class="expand" >Ciudad</th>
                                  <th data-class="expand" >Contacto</th>
                                  <th data-class="expand" >Factura</th>  <?php } ?>
                                  <th data-class="expand"  >Direccion</th>
                                  <th data-class="expand"  >RFC con Homoclabe</th>
                                  <th data-class>Celular_con_lada</th>
								  <th data-hide="phone"  >Fecha de Cumpleaños</th>
								  <th data-hide="phone" >Fecha_de_facturaci&oacute;n del vehiculo</th>
                                  <th data-hide="phone,tablet" >Modelo</th>
                                  <th data-hide="phone" >Version</th>               
                                  <th data-hide="phone" >Año</th>  
                                  <th  data-hide="phone">Color</th>
								  <th data-hide="phone">Vin</th>
								  <th data-hide="phone">Nuevo/SemiNuevo/ Demo</th>
								  <th data-class="small" >Telefono_1_con lada</th>
                                  <th data-hide="phone">Radio</th>
								  <th data-hide="phone">Email</th>
								  <th data-class="small" >Asesor</th>
                                  <th data-class="small" >Observaciones/Comentarios</th>
							  </tr>
						  </thead>
<tbody>
<?php if($todo_ventas){ $i=0;$x=0;$y=0;foreach($todo_ventas as $todo): ?>
<?php 
$INFO=$this->Homemodel->ProcesodeVentaHome($todo->con_IDcontacto);
$INFOAUTO=$this->Homemodel->AutoApartado($todo->con_IDcontacto);
if(empty($INFO)){}else{
	
if($INFO->datco_snuevo=='si'){
?>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_ciudad; ?></td>

<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if($todo->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$todo->datc_primernombre.' '.$todo->datc_segundonombre.' '.$todo->datc_apellidopaterno.' '.$todo->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$todo->datc_moral; echo ucwords(strtolower($facturaNombre));}
?>
</td>
<td><?php 
echo $todo->dfa_calle.' '.$todo->dfa_colonia.' '.$todo->dfa_ciudad.' '.$todo->dfa_estado.' '.$todo->dfa_municipio.' '.$todo->dfa_codigopostal;
?></td>
<td>
<?php echo $todo->datc_rfc;?>
</td>
<td>
<?php echo $todo->datc_tcelular;?>
</td>
<td>
<?php echo $todo->datc_fechanac;?>
</td>
<td><?php
echo $todo->dat_fecha_facturacion;
?>
</td>

<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_modelo));}
}
else{echo ucwords(strtolower($INFO->data_modelo));}
?>
</td>
<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_version));}
}
else{echo ucwords(strtolower($INFO->data_version));}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_ano));}	
	}
else{echo $INFO->data_ano;}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_color_exterior));}	
	}
else{echo ucwords(strtolower($INFO->data_color));}
?>
</td>

<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_IdFk));}	
	}
else{echo $INFO->data_vin;}
?>
</td>
<td>
<?php if(empty($INFO)){}else{
if($INFO->datco_nuevo=='si'){echo 'Demo';}	
if($INFO->datco_snuevo=='si'){echo 'Nuevo';}
if($INFO->datco_nuevox=='si'){echo 'Semi nuevos';}			
	}?>
</td>
<td>
<?php 
echo $todo->datc_tcasa;
?>
</td>
<td>
<?php 
echo $todo->datc_radio;
?>
</td>

<td><?php
echo $todo->datc_email;
?>
</td>
<td><?php
echo $todo->hus_nombre.' '.$todo->hus_apellido;
?>
</td>

<td >
 <ul class="nav">
                            <li class="dropdown dropdown-right">
                                <a class="btn btn-small" data-toggle="dropdown" href="#">Acciones</a>
                                <ul class="dropdown-menu">
                                    <li>
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a">Bitacora</a>
                                    </li>
                                    <li>
<a title="Bitacora" target="_blank" href="<?php echo base_url()."index.php/contacto/viewordendecomprapdf/$todo->dor_IDdordencomprat";?>" class="sepV_a">Orden de Compra</a>
                                    </li>

                              
                                </ul>
                            </li>
                            </ul>
</td>
</tr>
<?php }elseif($INFO->datco_nuevox=='si'){ ?>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_ciudad; ?></td>

<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if($todo->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$todo->datc_primernombre.' '.$todo->datc_segundonombre.' '.$todo->datc_apellidopaterno.' '.$todo->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$todo->datc_moral; echo ucwords(strtolower($facturaNombre));}
?>
</td>
<td><?php 
echo $todo->dfa_calle.' '.$todo->dfa_colonia.' '.$todo->dfa_ciudad.' '.$todo->dfa_estado.' '.$todo->dfa_municipio.' '.$todo->dfa_codigopostal;
?></td>
<td>
<?php echo $todo->datc_rfc;?>
</td>
<td>
<?php echo $todo->datc_tcelular;?>
</td>
<td>
<?php echo $todo->datc_fechanac;?>
</td>
<td><?php
echo $todo->dat_fecha_facturacion;
?>
</td>

<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_modelo));}
}
else{echo ucwords(strtolower($INFO->data_modelo));}
?>
</td>
<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_version));}
}
else{echo ucwords(strtolower($INFO->data_version));}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_ano));}	
	}
else{echo $INFO->data_ano;}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_color_exterior));}	
	}
else{echo ucwords(strtolower($INFO->data_color));}
?>
</td>

<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_IdFk));}	
	}
else{echo $INFO->data_vin;}
?>
</td>
<td>
<?php if(empty($INFO)){}else{
if($INFO->datco_nuevo=='si'){echo 'Demo';}	
if($INFO->datco_snuevo=='si'){echo 'Nuevo';}
if($INFO->datco_nuevox=='si'){echo 'Semi nuevos';}			
	}?>
</td>
<td>
<?php 
echo $todo->datc_tcasa;
?>
</td>
<td>
<?php 
echo $todo->datc_radio;
?>
</td>

<td><?php
echo $todo->datc_email;
?>
</td>
<td><?php
echo $todo->hus_nombre.' '.$todo->hus_apellido;
?>
</td>
<td >
 <ul class="nav">
                            <li class="dropdown dropdown-right">
                                <a class="btn btn-small" data-toggle="dropdown" href="#">Acciones</a>
                                <ul class="dropdown-menu">
                                    <li>
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a">Bitacora</a>
                                    </li>
                                    <li>
<a title="Bitacora" target="_blank" href="<?php echo base_url()."index.php/contacto/viewordendecomprapdf/$todo->dor_IDdordencomprat";?>" class="sepV_a">Orden de Compra</a>
                                    </li>

                              
                                </ul>
                            </li>
                            </ul>
</td>
</tr>

<?php }elseif($INFO->datco_nuevo=='si'){ ?>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_ciudad; ?></td>

<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if($todo->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$todo->datc_primernombre.' '.$todo->datc_segundonombre.' '.$todo->datc_apellidopaterno.' '.$todo->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$todo->datc_moral; echo ucwords(strtolower($facturaNombre));}
?>
</td>
<td><?php 
echo $todo->dfa_calle.' '.$todo->dfa_colonia.' '.$todo->dfa_ciudad.' '.$todo->dfa_estado.' '.$todo->dfa_municipio.' '.$todo->dfa_codigopostal;
?></td>
<td>
<?php echo $todo->datc_rfc;?>
</td>
<td>
<?php echo $todo->datc_tcelular;?>
</td>
<td>
<?php echo $todo->datc_fechanac;?>
</td>
<td><?php
echo $todo->dat_fecha_facturacion;
?>
</td>

<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_modelo));}
}
else{echo ucwords(strtolower($INFO->data_modelo));}
?>
</td>
<td>
<?php 
if(empty($INFO))
{
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_version));}
}
else{echo ucwords(strtolower($INFO->data_version));}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_ano));}	
	}
else{echo $INFO->data_ano;}
?>
</td>
<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_color_exterior));}	
	}
else{echo ucwords(strtolower($INFO->data_color));}
?>
</td>

<td>
<?php 
if(empty($INFO)){
if(empty($INFOAUTO)){}
else{echo ucwords(strtolower($INFOAUTO->aau_IdFk));}	
	}
else{echo $INFO->data_vin;}
?>
</td>
<td>
<?php if(empty($INFO)){}else{
if($INFO->datco_nuevo=='si'){echo 'Demo';}	
if($INFO->datco_snuevo=='si'){echo 'Nuevo';}
if($INFO->datco_nuevox=='si'){echo 'Semi nuevos';}			
	}?>
</td>
<td>
<?php 
echo $todo->datc_tcasa;
?>
</td>
<td>
<?php 
echo $todo->datc_radio;
?>
</td>

<td><?php
echo $todo->datc_email;
?>
</td>
<td><?php
echo $todo->hus_nombre.' '.$todo->hus_apellido;
?>
</td>
<td >
 <ul class="nav">
                            <li class="dropdown dropdown-right">
                                <a class="btn btn-small" data-toggle="dropdown" href="#">Acciones</a>
                                <ul class="dropdown-menu">
                                    <li>
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a">Bitacora</a>
                                    </li>
                                    <li>
<a title="Bitacora" target="_blank" href="<?php echo base_url()."index.php/contacto/viewordendecomprapdf/$todo->dor_IDdordencomprat";?>" class="sepV_a">Orden de Compra</a>
                                    </li>

                              
                                </ul>
                            </li>
                            </ul>
</td>
</tr>
<?php }} ?>

<?php endforeach; }?>
</tbody>
                            
                          </table>               
                                            
                                            
                                            

                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
  <script type='text/javascript'>

	$(document).ready(function() {
				

$('#s23_single').live('click',function(){
		
var tipo=$('select[name=tipocontacto]').val();
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontactoadmin/'+tipo;
});

$('#s24_single').live('click',function(){
var tipo=$('select[name=tipocontacto]').val();	
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontacto/'+tipo;	
});
	});	
</script>

<script type='text/javascript'>

	$(document).ready(function() {

$('#foo_example').dataTable( {
		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
					"aaSorting": [[ 5, "desc" ]],
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
} );
</script>
    </body>
</html>