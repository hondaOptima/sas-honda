<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="precios"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Precios</a></li>
                                    <li style="float:right"> <?php if($_SESSION['nivel']!='Vendedor'){?>
                   <a class="btn btn-small btn-success" href="<?php echo base_url();?>index.php/reporte/preciosAdd/">Agregar</a> 
                   <?php } ?> </li>                              
                                </ul>
                            </section>

                            <!-- main content -->
                           
                      <?php echo $flash_message ?> 
                     
                    <br>
                 <div class="row-fluid">
                                <div class="span12">
                                      <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Precios</h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div class="row-fluid">



<div class="span12">
<div class="accordion" id="accordion1">


<?php $i=1; if($precios): ?>

<?php setlocale(LC_MONETARY, 'en_US'); foreach($precios as $todo): ?>
<?php  $i++;?>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne<?php echo $i;?>"><?php echo $todo->tp_nombre;?></a>
</div>
<div id="collapseOne<?php echo $i?>" class="accordion-body collapse">
<div class="accordion-inner">
<?php $lista=$this->Reportesmodel->listaPrecios($todo->tp_IDtipo_precios);
if($lista){
?>

<table style=" margin:0 auto; position:relative; text-align:center; font-size:1em; border:1px solid #CCC" class="table  ">
<thead ><tr style="font-weight:bold"
bgcolor="#F9F9F9" >
<td width="25%"><?php echo $todo->tp_nombre;?></td>
<td width="25%">Precio Unico Nal</td>
<td width="25%">Rebate</td>
<td width="25%">Con Rebate</td>
<?php if($_SESSION['nivel']!='Vendedor'){?>
<td width="25%">Acciones</td>
<?php }?>
</tr></thead>
<tbody>

<?php
foreach($lista as $data){
if($data->pr_rebate){
$pre= filter_var($data->pr_precio, FILTER_SANITIZE_NUMBER_INT);
$reb= filter_var($data->pr_rebate, FILTER_SANITIZE_NUMBER_INT);
$total=$pre - $reb;
}else{
$total=0;}	
echo '<tr bgcolor="white"><td>'.$data->pr_nombre.'</td>';
echo '<td>'.$data->pr_precio.'</td>';
echo '<td> '.money_format('%(#10n',$total).'</td>';
echo '<td>'.$data->pr_rebate.'</td>';
if($_SESSION['nivel']!='Vendedor'){	
echo '<td><a href="'.base_url().'index.php/reporte/preciosEdit/'.$data->pr_IDprecio.'"><li class="icon-pencil"></li></a><a href="'.base_url().'index.php/reporte/deleteprecio/'.$data->pr_IDprecio.'"  onClick=\'return confirm("Esta seguro que desea eliminar:'.$data->pr_nombre.'")\'><li class="icon-trash"></li></a></td></tr>';	
}
}
?>

</tbody>
</table>
<?php } ?>
</div></div>
</div>
<?php endforeach ?><?php else: ?>No se encontraron registros.<?php endif ?>	
                                                        
                                                      
                                                    </div>
                                                </div>                                       
                                      
                                               
  
                                                
                                                
      
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>