<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="cont"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          <?php $this->load->view('contacto/coucheo/ModalCoucheo'); ?>
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                     <li style="float:right"><a  target="_blank" class="btn btn-small" href="<?php echo base_url();?>index.php/asesores/citas">Citas vista Tv</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
<div class="row-fluid"> <div class="span12">
<div class="box_a"><div class="box_a_heading">
<h3>Filtros</h3>
</div>
<div class="box_a_content"> 
<form name="form" method="get" action="<?php echo base_url();?>index.php/reporte/citas"> 
<input type="hidden" name="sucursal" value="<?php echo $sucursal;?>">
<div class="span3"><h4 class="heading_b">Rango por fecha</h4>

 <div class="row-fluid">
 
 <input class="span6"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $mesi;;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
<input class="span6" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $mesf;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
</div>  </div>




<div class="span2"><h4 class="heading_b">Asesor</h4><div class="row-fluid">
 <?php echo form_dropdown('asesor', $todo_vendedores,$ase,'style="width:135px;" id="s2_single" data-rel="chosen"');?>
</div>  </div>


<div class="span2"><h4 class="heading_b">Status</h4><div class="row-fluid">

 <?php
 $astatus=array(
 '0'=>'Todos',
  'Prospecto'=>'Prospecto',
   'Caliente'=>'Caliente',
    'Caida'=>'Caida',
	 'Proceso de venta'=>'Proceso de Venta',
	  'Cliente'=>'Cliente',
 );
  echo form_dropdown('status', $astatus,$sta,'style="width:135px;" id="s24_single" data-rel="chosen"');?>
</div>  </div>

<div class="span2"><h4 class="heading_b">Fuente</h4><div class="row-fluid">
 <?php

 $afue=array(
 '0'=>'Todos',
  'Fresh up'=>'Fresh up',
   'Llamada'=>'Llamada',
    'Recompra'=>'Recompra',
	 'Correo Electronico'=>'Internet'
 );
  echo form_dropdown('fuente', $afue,$fue,'style="width:135px;" id="s23_single" data-rel="chosen"');?>
</div>  </div>

<div class="span2"><h4 class="heading_b">Accion</h4><div class="row-fluid">
 
 <input type="submit" value="Buscar" class="btn btn-primary">
</div>  </div>
</form>

</div></div></div></div>                            
                            
<div class="row-fluid"> <div class="span12">
<div class="box_a"><div class="box_a_heading">
<?php if($fue=='0'){$fue='Todos';}?>
<h3><?php echo $fue.' de '.$sucursal;?></h3>
</div>
<div class="box_a_content">
     <table id="example2"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr>
                                  <th>Ciudad</th>
                                  <th>Fuente</th>
                                  <th style="width:150px;">Contacto</th>
                                  <th data-class="expand">Cita</th>
                                  <th  data-hide="expand">Hora</th> 
                                  <th data-hide="phone,tablet" >Confirmada</th>
                                  <th data-hide="phone,tablet" >Cumplida</th>
								  <th data-hide="phone,tablet" >Prueba de manejo</th>
								 <th data-hide="phone,tablet" >Status</th>
                                 <th data-hide="phone,tablet" >Siguiente actividad</th>
                                 <th  style="width:150px;" data-hide="phone,tablet" >Asesor</th>
                                 <th data-hide="phone,tablet" >Acciones</th>
                                  </tr>
						  </thead>   
						  <tbody>

</tbody>


</table>                      
</div></div>                          





</div></div></div> </div>
 <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
<?php $this->load->view('globales/footer'); ?>       
<?php $this->load->view('globales/js');?> 
 <script>
  	$(document).ready(function() {
		
		
$(function() {
        cargar_tabla.tabeje();
   });
    //* charts
    cargar_tabla= {
        tabeje: function() {		
		
		
$('#example2').dataTable( {

		     "iDisplayLength": 50,
					 "bAutoWidth": true,
					 	"bProcessing": true,
	"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>querys/reportes/citasList.php?finicio=<?php echo $mesi;?>&ffin=<?php echo $mesf;?>&cd=<?php echo $sucursal;?>&asesor=<?php echo $ase; ?>&status=<?php echo $sta;?>&fuente=<?php echo $fue;?>&act=<?php echo $act;?>",
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example2_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );		
		
		}}
		
$('#saveComentario').live('click',function(){
	$('.loading').show();
var com=$('textarea[name=com]').val();
var idb=$('input[name=idb]').val();
var idt=$('input[name=idc]').val();
$.ajax({
   type: 'POST',
   url: '<?php echo base_url();?>querys/bitacora/saveComentarioCoucheo.php',
   data: 'id='+idb+'&com='+com+'&idt='+idt,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.loading').hide();
  $(function() {
        cargar_tabla.tabeje();
   });
  
  $('#ModalCoucheo').modal('show');

$.ajax({
   type: 'POST',
   url: '<?php echo base_url().'querys/bitacora/infoBitacora.php'; ?>',
   data: 'idt='+idt,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.informacion').html(data);



   }
});	
  
  }
  
   });
});
		
		
		
			$('.ModalCoucheo').live('click',function(){
$('#ModalCoucheo').modal('show');
var idt=$(this).attr('id');

$.ajax({
   type: 'POST',
   url: '<?php echo base_url().'querys/bitacora/infoBitacora.php'; ?>',
   data: 'idt='+idt,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.informacion').html(data);



   }
});	
});
		

$('.saveNuevaTarea').live('click',function(){
	$('.loading').show();
var tit=$('input[name=titulo]').val();
var fec=$('input[name=ffin]').val();
var com=$('textarea[name=desctarea]').val();
var hor=$('select[name=hora]').val();
var tipo=$('select[name=tipo]').val();


var idb=$('input[name=idb]').val();
var idt=$('input[name=idc]').val();
$.ajax({
   type: 'POST',
   url: '<?php echo base_url();?>querys/bitacora/saveNuevaTarea.php',
   data: 'id='+idb+'&com='+com+'&idt='+idt+'&tit='+tit+'&fec='+fec+'&hor='+hor+'&tipo='+tipo,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.loading').hide();
  $(function() {
        cargar_tabla.tabeje();
   });
  
   $('#ModalCoucheo').modal('show');

$.ajax({
   type: 'POST',
   url: '<?php echo base_url().'querys/bitacora/infoBitacora.php'; ?>',
   data: 'idt='+idt,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.informacion').html(data);
 }
});	
  
  
   }
});
});


$('.status').live('click',function(){
$('.loading').show();
var val=$(this).attr('id');
var id=$('input[name=status]').val();
$.ajax({
   type: 'POST',
   url: '<?php echo base_url();?>querys/bitacora/saveStatus.php',
   data: 'id='+id+'&val='+val,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
	   $('.loading').hide();
	   
	    $(function() {
        cargar_tabla.tabeje();
   });
  
   $('#ModalCoucheo').modal('show');

$.ajax({
   type: 'POST',
   url: '<?php echo base_url().'querys/bitacora/infoBitacora.php'; ?>',
   data: 'idt='+id,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
  $('.informacion').html(data);
 }
});	
	   
	   }
   
});
	
	
	});
		
	});
	</script>
    
 </body>
</html>