<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="plan"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Plan de Ventas Grupo Optima 2014</a></li>
                                    <li style="float:right">
                                    <a class="btn btn-small btn-success" href="<?php echo base_url();?>index.php/reporte/crearplanventas/">Crear plan de ventas</a>
                                    </li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                         
                     
<?php $ncount=13; $x=0; 	$tottijSUMA=0; $totmexSUMA=0;  $totensSUMA=0; $sumglobalHonda=0;?>
                 <div class="row-fluid">
                                <div class="span12">

<div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Tijuana 2014</h3> </div>
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody> 

                            <?php foreach($tij as $todo): ?>  
                            <tr>
<th ><b><a href="<?php echo base_url();?>index.php/reporte/editarplanventas/<?php echo $todo->plv_IDplan_ventas;?>"><?php echo $todo->descripcion;?></a></b></th>
                                <th><?php echo $todo->plv_ene;?></th>
                                <th><?php echo $todo->plv_feb;?></th>
                                <th><?php echo $todo->plv_mar;?></th>
                                <th><?php echo $todo->plv_abri;?></th>
                                <th><?php echo $todo->plv_may;?></th>
                               <th><?php echo $todo->plv_jun;?></th>
                                <th><?php echo $todo->plv_jul;?></th>
                                <th><?php echo $todo->plv_ago;?></th>
                                <th><?php echo $todo->plv_sep;?></th>
                                <th><?php echo $todo->plv_oct;?></th>
                                <th><?php echo $todo->plv_nov;?></th>
                                <th><?php echo $todo->plv_dic;?></th>
                                <th><?php echo $tottij=($todo->plv_ene + $todo->plv_feb + $todo->plv_mar +$todo->plv_abri +$todo->plv_may +$todo->plv_jun +$todo->plv_jul +$todo->plv_ago +$todo->plv_sep +$todo->plv_oct +$todo->plv_nov +$todo->plv_dic);
							
								$tottijSUMA+=($todo->plv_ene + $todo->plv_feb + $todo->plv_mar +$todo->plv_abri +$todo->plv_may +$todo->plv_jun +$todo->plv_jul +$todo->plv_ago +$todo->plv_sep +$todo->plv_oct +$todo->plv_nov +$todo->plv_dic);
								?></th>
                            </tr>   
                           <?php endforeach ?>
                          
                          <tr>
                          <th>Total</th>
                          <th><?php $tensuma=0; for($a=0; $a<$ncount; $a++){$tensuma+=$tij[$a]->plv_ene;} echo $tensuma;?></th>
                          <th><?php $tfesuma=0; for($a=0; $a<$ncount; $a++){$tfesuma+=$tij[$a]->plv_feb;} echo $tfesuma;?></th>
                          <th><?php $tmrsuma=0; for($a=0; $a<$ncount; $a++){$tmrsuma+=$tij[$a]->plv_mar;} echo $tmrsuma;?></th>
                          <th><?php $tabsuma=0; for($a=0; $a<$ncount; $a++){$tabsuma+=$tij[$a]->plv_abri;} echo $tabsuma;?></th>
                          <th><?php $tmysuma=0; for($a=0; $a<$ncount; $a++){$tmysuma+=$tij[$a]->plv_may;} echo $tmysuma;?></th>
                          <th><?php $tjnsuma=0; for($a=0; $a<$ncount; $a++){$tjnsuma+=$tij[$a]->plv_jun;} echo $tjnsuma;?></th>
                          <th><?php $tjlsuma=0; for($a=0; $a<$ncount; $a++){$tjlsuma+=$tij[$a]->plv_jul;} echo $tjlsuma;?></th>
                          <th><?php $tagsuma=0; for($a=0; $a<$ncount; $a++){$tagsuma+=$tij[$a]->plv_ago;} echo $tagsuma;?></th>
                          <th><?php $tsesuma=0; for($a=0; $a<$ncount; $a++){$tsesuma+=$tij[$a]->plv_sep;} echo $tsesuma;?></th>
                          <th><?php $tocsuma=0; for($a=0; $a<$ncount; $a++){$tocsuma+=$tij[$a]->plv_oct;} echo $tocsuma;?></th>
                          <th><?php $tnosuma=0; for($a=0; $a<$ncount; $a++){$tnosuma+=$tij[$a]->plv_nov;} echo $tnosuma;?></th>
                          <th><?php $tdisuma=0; for($a=0; $a<$ncount; $a++){$tdisuma+=$tij[$a]->plv_dic;} echo $tdisuma;?></th>
                          <th><?php echo $tottijSUMA;?></th>
                          </tr>
                        </tbody>
                    </table>
</div>


<div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Mexicali 2014</h3> </div><div class="box_a_content">
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($mex as $todo): ?>  
                            <tr>
<th ><b><a href="<?php echo base_url();?>index.php/reporte/editarplanventas/<?php echo $todo->plv_IDplan_ventas;?>"><?php echo $todo->descripcion;?></a></b></th>
                                <th><?php echo $todo->plv_ene;?></th>
                                <th><?php echo $todo->plv_feb;?></th>
                                <th><?php echo $todo->plv_mar;?></th>
                                <th><?php echo $todo->plv_abri;?></th>
                                <th><?php echo $todo->plv_may;?></th>
                               <th><?php echo $todo->plv_jun;?></th>
                                <th><?php echo $todo->plv_jul;?></th>
                                <th><?php echo $todo->plv_ago;?></th>
                                <th><?php echo $todo->plv_sep;?></th>
                                <th><?php echo $todo->plv_oct;?></th>
                                <th><?php echo $todo->plv_nov;?></th>
                                <th><?php echo $todo->plv_dic;?></th>
                                <th><?php echo $totmex=($todo->plv_ene + $todo->plv_feb + $todo->plv_mar +$todo->plv_abri +$todo->plv_may +$todo->plv_jun +$todo->plv_jul +$todo->plv_ago +$todo->plv_sep +$todo->plv_oct +$todo->plv_nov +$todo->plv_dic);
							
								$totmexSUMA+=($todo->plv_ene + $todo->plv_feb + $todo->plv_mar +$todo->plv_abri +$todo->plv_may +$todo->plv_jun +$todo->plv_jul +$todo->plv_ago +$todo->plv_sep +$todo->plv_oct +$todo->plv_nov +$todo->plv_dic);
								?></th>
                           <?php endforeach ?>
                           
                            <tr>
                          <th>Total</th>
                          <th><?php $mensuma=0; for($a=0; $a<$ncount; $a++){$mensuma+=$mex[$a]->plv_ene;} echo $mensuma;?></th>
                          <th><?php $mfesuma=0; for($a=0; $a<$ncount; $a++){$mfesuma+=$mex[$a]->plv_feb;} echo $mfesuma;?></th>
                          <th><?php $mmrsuma=0; for($a=0; $a<$ncount; $a++){$mmrsuma+=$mex[$a]->plv_mar;} echo $mmrsuma;?></th>
                          <th><?php $mabsuma=0; for($a=0; $a<$ncount; $a++){$mabsuma+=$mex[$a]->plv_abri;}echo $mabsuma;?></th>
                          <th><?php $mmysuma=0; for($a=0; $a<$ncount; $a++){$mmysuma+=$mex[$a]->plv_may;} echo $mmysuma;?></th>
                          <th><?php $mjnsuma=0; for($a=0; $a<$ncount; $a++){$mjnsuma+=$mex[$a]->plv_jun;} echo $mjnsuma;?></th>
                          <th><?php $mjlsuma=0; for($a=0; $a<$ncount; $a++){$mjlsuma+=$mex[$a]->plv_jul;} echo $mjlsuma;?></th>
                          <th><?php $magsuma=0; for($a=0; $a<$ncount; $a++){$magsuma+=$mex[$a]->plv_ago;} echo $magsuma;?></th>
                          <th><?php $msesuma=0; for($a=0; $a<$ncount; $a++){$msesuma+=$mex[$a]->plv_sep;} echo $msesuma;?></th>
                          <th><?php $mocsuma=0; for($a=0; $a<$ncount; $a++){$mocsuma+=$mex[$a]->plv_oct;} echo $mocsuma;?></th>
                          <th><?php $mnosuma=0; for($a=0; $a<$ncount; $a++){$mnosuma+=$mex[$a]->plv_nov;} echo $mnosuma;?></th>
                          <th><?php $mdisuma=0; for($a=0; $a<$ncount; $a++){$mdisuma+=$mex[$a]->plv_dic;} echo $mdisuma;?></th>
                          <th><?php echo $totmexSUMA;?></th>
                          </tr>
                        </tbody>
                    </table>
</div></div>


<div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Ensenada 2014</h3> </div>
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($ens as $todo): ?>  
                            <tr>
<th ><b><a href="<?php echo base_url();?>index.php/reporte/editarplanventas/<?php echo $todo->plv_IDplan_ventas;?>"><?php echo $todo->descripcion;?></a></b></th>
                                <th><?php echo $todo->plv_ene;?></th>
                                <th><?php echo $todo->plv_feb;?></th>
                                <th><?php echo $todo->plv_mar;?></th>
                                <th><?php echo $todo->plv_abri;?></th>
                                <th><?php echo $todo->plv_may;?></th>
                               <th><?php echo $todo->plv_jun;?></th>
                                <th><?php echo $todo->plv_jul;?></th>
                                <th><?php echo $todo->plv_ago;?></th>
                                <th><?php echo $todo->plv_sep;?></th>
                                <th><?php echo $todo->plv_oct;?></th>
                                <th><?php echo $todo->plv_nov;?></th>
                                <th><?php echo $todo->plv_dic;?></th>
                                <th><?php echo $sum=($todo->plv_ene + $todo->plv_feb + $todo->plv_mar +$todo->plv_abri +$todo->plv_may +$todo->plv_jun +$todo->plv_jul +$todo->plv_ago +$todo->plv_sep +$todo->plv_oct +$todo->plv_nov +$todo->plv_dic);
								
								$totensSUMA+=($todo->plv_ene + $todo->plv_feb + $todo->plv_mar +$todo->plv_abri +$todo->plv_may +$todo->plv_jun +$todo->plv_jul +$todo->plv_ago +$todo->plv_sep +$todo->plv_oct +$todo->plv_nov +$todo->plv_dic);
								?></th>
                           <?php endforeach ?>
                           
                            <tr>
                          <th>Total</th>
                          <th><?php $eensuma=0; for($a=0; $a<$ncount; $a++){$eensuma+=$ens[$a]->plv_ene;} echo $eensuma;?></th>
                          <th><?php $efesuma=0; for($a=0; $a<$ncount; $a++){$efesuma+=$ens[$a]->plv_feb;} echo $efesuma;?></th>
                          <th><?php $emrsuma=0; for($a=0; $a<$ncount; $a++){$emrsuma+=$ens[$a]->plv_mar;} echo $emrsuma;?></th>
                          <th><?php $eabsuma=0; for($a=0; $a<$ncount; $a++){$eabsuma+=$ens[$a]->plv_abri;}echo $eabsuma;?></th>
                          <th><?php $emysuma=0; for($a=0; $a<$ncount; $a++){$emysuma+=$ens[$a]->plv_may;} echo $emysuma;?></th>
                          <th><?php $ejnsuma=0; for($a=0; $a<$ncount; $a++){$ejnsuma+=$ens[$a]->plv_jun;} echo $ejnsuma;?></th>
                          <th><?php $ejlsuma=0; for($a=0; $a<$ncount; $a++){$ejlsuma+=$ens[$a]->plv_jul;} echo $ejlsuma;?></th>
                          <th><?php $eagsuma=0; for($a=0; $a<$ncount; $a++){$eagsuma+=$ens[$a]->plv_ago;} echo $eagsuma;?></th>
                          <th><?php $esesuma=0; for($a=0; $a<$ncount; $a++){$esesuma+=$ens[$a]->plv_sep;} echo $esesuma;?></th>
                          <th><?php $eocsuma=0; for($a=0; $a<$ncount; $a++){$eocsuma+=$ens[$a]->plv_oct;} echo $eocsuma;?></th>
                          <th><?php $enosuma=0; for($a=0; $a<$ncount; $a++){$enosuma+=$ens[$a]->plv_nov;} echo $enosuma;?></th>
                          <th><?php $edisuma=0; for($a=0; $a<$ncount; $a++){$edisuma+=$ens[$a]->plv_dic;} echo $edisuma;?></th>
                          <th><?php echo $totensSUMA;?></th>
                          </tr>
                        </tbody>
                    </table>
</div>





<div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Global 2014</h3> </div>
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $y=0; foreach($tij as $todo): ?>  
                            
                            <tr>
<th ><b><a href=""><?php echo $todo->descripcion;?></a></b></th>
                                <th><?php echo $uno=$tij[$y]->plv_ene + $mex[$y]->plv_ene +$ens[$y]->plv_ene;?></th>
                               <th><?php echo $dos=$tij[$y]->plv_feb + $mex[$y]->plv_feb +$ens[$y]->plv_feb;?></th>
                               <th><?php echo $tres=$tij[$y]->plv_mar + $mex[$y]->plv_mar +$ens[$y]->plv_mar;?></th>
                                <th><?php echo $cuatro=$tij[$y]->plv_abri + $mex[$y]->plv_abri +$ens[$y]->plv_abri;?></th>
                              <th><?php echo $cinco=$tij[$y]->plv_may + $mex[$y]->plv_may +$ens[$y]->plv_may;?></th>
                               <th><?php echo $seis=$tij[$y]->plv_jun + $mex[$y]->plv_jun +$ens[$y]->plv_jun;?></th>
                               <th><?php echo $siete=$tij[$y]->plv_jul + $mex[$y]->plv_jul +$ens[$y]->plv_jul;?></th>
                              <th><?php echo $ocho=$tij[$y]->plv_ago + $mex[$y]->plv_ago +$ens[$y]->plv_ago;?></th>
                               <th><?php echo $nueve=$tij[$y]->plv_sep + $mex[$y]->plv_sep +$ens[$y]->plv_sep;?></th>
                               <th><?php echo $diez=$tij[$y]->plv_oct + $mex[$y]->plv_oct +$ens[$y]->plv_oct;?></th>
                                <th><?php echo $once=$tij[$y]->plv_nov + $mex[$y]->plv_nov +$ens[$y]->plv_nov;?></th>
                                <th><?php echo $doce=$tij[$y]->plv_dic + $mex[$y]->plv_dic +$ens[$y]->plv_dic;?></th>
                                <th><?php echo $sumglobal=($uno + $dos + $tres +$cuatro +$cinco +$seis +$siete +$ocho +$nueve +$diez +$once +$doce);
								
$sumglobalHonda+=($uno + $dos + $tres +$cuatro +$cinco +$seis +$siete +$ocho +$nueve +$diez +$once +$doce);								?></th>
                            </tr> <?php $y++;?>  
                           <?php endforeach ?>
                            <tr>
                          <th>Total</th>
                          <th><?php echo $mensuma + $tensuma + $eensuma ;?></th>
                          <th><?php echo $mfesuma + $tfesuma + $efesuma;?></th>
                          <th><?php echo $mmrsuma + $tmrsuma + $emrsuma;?></th>
                          <th><?php echo $mabsuma + $tabsuma + $eabsuma;?></th>
                          <th><?php echo $mmysuma + $tmysuma + $emysuma;?></th>
                          <th><?php echo $mjnsuma + $tjnsuma + $ejnsuma;?></th>
                          <th><?php echo $mjlsuma + $tjlsuma + $ejlsuma;?></th>
                          <th><?php echo $magsuma + $tagsuma + $eagsuma;?></th>
                          <th><?php echo $msesuma + $tsesuma + $esesuma;?></th>
                          <th><?php echo $mocsuma + $tocsuma + $eocsuma;?></th>
                          <th><?php echo $mnosuma + $tnosuma + $enosuma;?></th>
                          <th><?php echo $mdisuma + $tdisuma + $edisuma;?></th>
                          <th><?php echo $sumglobalHonda;?></th>
                          </tr>
                        </tbody>
                    </table>
</div>




</div></div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>