<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/reporte/ventas">Reportes</a></li>
                                    <li class="crumb_sep">
<i class="elusive-icon-play"></i>
</li>
                                     <li><a href="<?php echo base_url();?>index.php/reporte/facturascheck">Opciones</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                            
<?php 
if(empty($_GET['asesor'])){ $asesor='';}else{$asesor=$_GET['asesor'];}?>
<?php if(empty($_GET['persona'])){ $persona='';}else{$persona=$_GET['persona'];}?>
<?php if(empty($_GET['tipoAuto'])){ $tipoAuto='';}else{$tipoAuto=$_GET['tipoAuto'];}?>
<?php if(empty($_GET['modelo'])){ $modelo='';}else{$modelo=$_GET['modelo'];}?>
<?php if(empty($_GET['anios'])){ $anios='';}else{$anios=$_GET['anios'];}?>
<?php if(empty($_GET['finicio'])){ $finicio=date('d-m-Y');}else{$finicio=$_GET['finicio'];}?>
<?php if(empty($_GET['ffin'])){ $ffin=date('d-m-Y');}else{$ffin=$_GET['ffin'];}?>

<?php if(empty($_GET['cliente'])){ $cliente='';}else{$cliente=$_GET['cliente'];}?>

                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Filtros</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<form name="form1" method="get" action="<?php echo base_url();?>index.php/reporte/facturas/">

<input type="hidden" name="cadena" value="<?php echo $cadena;?>">
                                        
                                        <div class="row-fluid">
                                          <div class="span3">
                                                    <h4 class="heading_b">Ciudad</h4>
                               
                                                    <?php
$options = array(
                  'Tijuana'  => 'Tijuana',
                  'Mexicali'    => 'Mexicali',
                  'Ensenada'   => 'Ensenada',
				  '0'   => 'Todas las ciudades',
                );
echo form_dropdown('cid', $options, $cid );
                                                    ?>
                                                </div>
                                        
                                                <div class="span3">
                                                    <h4 class="heading_b">Asesores</h4>
                                                  <?php                                         
echo form_dropdown('asesor', $asesores,$asesor );
?>
                                                </div>
                                                 
                                                 <div class="span3">
                                                    <h4 class="heading_b">Fisica/Moral</h4>
<?php

 $options = array(
                  'fisica'  => 'Fisica',
                  'moral'    => 'Moral',
                  ''   => 'Todos'
                );
echo form_dropdown('persona', $options,$persona );
?>
                                                </div>
                                                 <div class="span3">
                                                    <h4 class="heading_b">Tipo Auto</h4>
<?php
 $options = array(
                  'Nuevo'  => 'Nuevo',
                  'Demo'    => 'Demo',
                  'Seminuevo'   => 'Seminuevo',
				  ''   => 'Todos'
                );
echo form_dropdown('tipoAuto', $options,$tipoAuto );
?>
                                                </div>
                                              
                                               
                                               
                                                </div> 
                                                <div class="row-fluid">
                                                
                                                  <div class="span3">
                                                    <h4 class="heading_b">Modelo</h4>
<?php

 $options = array(
                  'ACCORD'  => 'ACCORD',
                  'FIT'    => 'FIT',
				  'CIVIC'    => 'CIVIC',
				  'PILOT'    => 'PILOT',
				  'CRV'    => 'CRV',
				  'ODYSSEY'    => 'ODYSSEY',
				  'CITY'    => 'CITY',
				  'CRZ'    => 'CRZ',
				  'CROSSTOUR'    => 'CROSSTOUR',
				  'RIDGELINE'=>'RIDGELINE',
				  ''   => 'Todos'
				  
                );
echo form_dropdown('modelo', $options,$modelo );
?>                                                    
   
                                                </div>
                                                
                                                 <div class="span3">
                                                    <h4 class="heading_b">Año</h4>
<?php
echo form_dropdown('anios', $oanios,$anios );
?>
                                                </div>
                                                
                                                <div class="span4">
                                                    <h4 class="heading_b">Fecha facturacion</h4>
                                                     <input class="span4"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $finicio;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
<input class="span4" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $ffin;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>
                                                <div class="span2">
                                                    <h4 class="heading_b">Cliente</h4>
                                                    <input class="span12" name="cliente" type="text"  value="<?php echo $cliente;?>">
                                                </div>
                                                </div>
                                                
                                                
                                                <div class="row-fluid">
                                                   <div class="span2">
                                                    <h4 class="heading_b">Accion</h4>
                                                    <input type="submit" class="btn btn-primary" value="Buscar">
                                                </div>
                                                </div>
                                                
                                                </form>
                                                
                                        </div>
                                    
                                    </div></div></div> 
                    <br>
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Reporte Contactos </h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <div class="box_a_content no_sp">
                                        
                                  <table id="example"   class="table table-striped  "  >
                                                <thead>
							  <tr>
                             <?php
                             $titulos=array('0'=>'Ciudad','1'=>'Fisica / Moral','2'=>'Fecha_factura','3'=>'Factura','4'=>'Calle','5'=>'Colonia','6'=>'Ciudad','7'=>'Estado','8'=>'Codigo_postal','9'=>'RFC','10'=>'Celular','11'=>'Telefono_1','12'=>'Telefono_2','13'=>'Radio','14'=>'Email','15'=>'Cumpleaños','16'=>'Modelo','17'=>'Tipo','22'=>'Vin','21'=>'Color','20'=>'Año','23'=>'Placas','24'=>'Asesor','24'=>'Observaciones');
							 
							 $i=0;
							 $cadena=rtrim($cadena, "-");
							 $cadarra=explode('-',$cadena);
							 foreach($cadarra as $nbar){
								 
								 echo '<th>'.$titulos[$nbar].'</th>';
								 
								 }
							 ?>
                                 <th>Acciones</th>
								
							  </tr>
						  </thead>  
                          </table>
                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
<script type='text/javascript'>

	$(document).ready(function() {
	$('#example').dataTable( {"sScrollX": "100%",
		"sScrollXInner": "200%",
		"bScrollCollapse": true,
	"bPaginate": true,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?php echo base_url();?>querys/reportes/reporte_ventas.php?cadena=<?php echo $cadena;?>&cid=<?php echo $cid;?>&asesor=<?php echo $asesor?>&persona=<?php echo $persona;?>&tipoAuto=<?php echo $tipoAuto;?>&modelo=<?php echo $modelo;?>&anio=<?php echo $anios;?>&finicio=<?php echo $finicio;?>&ffin=<?php echo $ffin;?>&cliente=<?php echo $cliente;?>",
		"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );




	
} );
</script>
    </body>
</html>