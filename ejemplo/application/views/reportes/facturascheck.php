<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/reporte/ventas">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->

                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Filtros</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<form name="form1" method="get" action="<?php echo base_url();?>index.php/reporte/facturas/">

<div class="row-fluid">  
<div class="span3"><h4 class="heading_b">Ciudad y Factura</h4>
<div class="row-fluid">
   <select multiple="yes" name="colors[]" style="height:100px;">
       <option value="0" selected="selected"> Ciudad </option>
       <option value="1" selected="selected"> Fisica / Moral</option>
       <option value="2" selected="selected"> Fecha _factura </option>
       <option value="3" selected="selected"> Factura </option>
   </select>
</div></div>
  
 <div class="span3"><h4 class="heading_b">Dirección de Factura</h4>
<div class="row-fluid">  
     <select multiple="yes" name="colors[]" style="height:100px;">
       <option value="4" selected="selected"> Calle</option>
       <option value="5" selected="selected"> Colonia </option>
       <option value="6" selected="selected"> Ciudad </option>
       <option value="7" selected="selected"> Estado </option>
       <option value="8" selected="selected"> Codigo Postal </option>
   </select>
</div></div>
 
  <div class="span3"><h4 class="heading_b">Datos del Cliente</h4>
<div class="row-fluid">    
    <select multiple="yes" name="colors[]" style="height:100px;">
       <option value="9" selected="selected"> Rfc</option>
       <option value="10" selected="selected"> Celular </option>
       <option value="11" selected="selected"> Telefono 1</option>
       <option value="12" selected="selected"> Telefono 2 </option>
       <option value="13" selected="selected"> Radio</option>
       <option value="14" selected="selected"> Email</option>
       <option value="15" selected="selected"> Cumpleaños</option>
   </select>
 </div></div>
 
  <div class="span3"><h4 class="heading_b">Datos del Auto</h4>
<div class="row-fluid">      
   <select multiple="yes" name="colors[]" style="height:100px;">
       <option value="16" selected="selected"> Modelo</option>
       <option value="17" selected="selected"> Tipo </option>
       <option value="22" selected="selected"> Vin</option>
       <option value="21" selected="selected"> Color </option>
       <option value="20" selected="selected"> Año</option>
       <option value="23" selected="selected"> Placas</option>
   </select>
    </div></div>
    </div>
   
   
  <div class="row-fluid">   
    <div class="span3"><h4 class="heading_b">Datos de Contacto</h4>
<div class="row-fluid">  
    <select multiple="yes" name="colors[]" style="height:100px;">
       <option value="24" selected="selected"> Asesor</option>
       <option value="24" selected="selected"> Comentarios</option>
   </select>
  </div></div>
  
  <div class="span3"><h4 class="heading_b">Acción</h4>
<div class="row-fluid">   
<input type="submit" value="Siguiente">
  </div></div>
  
  </div>
</form>
                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
<script type='text/javascript'>

	$(document).ready(function() {
	$('#example').dataTable( {"sScrollX": "100%",
		"sScrollXInner": "200%",
		"bScrollCollapse": true,
		"sScrollY": 300,
	"bPaginate": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?php echo base_url();?>querys/reportes/reporte_ventas.php?cid=<?php echo $cid;?>&asesor=<?php echo $asesor?>&persona=<?php echo $persona;?>&tipoAuto=<?php echo $tipoAuto;?>&modelo=<?php echo $modelo;?>&anio=<?php echo $anios;?>&finicio=<?php echo $finicio;?>&ffin=<?php echo $ffin;?>"
	} );




	
} );
</script>
    </body>
</html>