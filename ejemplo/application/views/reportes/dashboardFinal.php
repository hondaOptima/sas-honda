<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
    <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="dash"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
                 
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                           
                    <?php $this->load->view('reportes/graficas/ventas');?>
                    <?php $this->load->view('reportes/graficas/inventario');?>            
                    <?php $this->load->view('reportes/graficas/piso');?>
                    <?php $this->load->view('reportes/graficas/citas');?>          
                    <?php $this->load->view('reportes/graficas/ventasAsesorMes');?>  
                    <?php $this->load->view('reportes/graficas/ventasAsesorAnual');?>
                                
                                
                        
                            
                    </div> </div>
                    
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
 <?php
$dataV['totalVentasMes']=$totalVentasMes;
$dataV['totalVentasAnual']=$totalVentasAnual;
$dataV['planMes']=$planMes;
$dataV['planAnual']=$planAnual;
?>      
<?php $this->load->view('globales/footer'); ?>       
<?php $this->load->view('globales/jsDashboard');?>
<?php $this->load->view('reportes/graficas/js/ventas',$dataV);?>
<?php $this->load->view('reportes/graficas/js/ajax');?>

  
    </body>
</html>