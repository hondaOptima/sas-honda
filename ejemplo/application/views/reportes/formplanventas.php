<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.back()">Plan de Ventas</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->                    

                 <div class="row-fluid">
                                <div class="span12">

<div class="box_a"> <?php
 echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>');


 echo form_open('reporte/crearplanventas/','class="form-horizontal"'); ?>
 
 <div class="box_a_heading"> <h3>Formulario Plan de Ventas.:</h3> Año:<input style="width:30px; margin-top:4px;" type="text" name="anio" value="<?php echo date('Y');?>" maxlength="4">
Agencia:<?php
$options = array(
								'0'  => 'Todas las Sucursales',
                                 '1'  => 'Tijuana',
                                 '2'    => 'Mexicali',
								 '3'    => 'Ensenada',
								
								 );
echo form_dropdown('agencia', $options,'','style="width:186px; margin-top:-3" id="s2_single" data-rel="chosen"');
								?> </div><div class="box_a_content">


<table class="table" align="center">
                        <thead>
                            <tr>
                                <th >Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                
                            </tr>
                        </thead>
                        <tbody>
    <?php foreach($modelos as $todo): ?>                       
                            <tr>
                                <td><?php echo $todo->descripcion;?></td>
                                <th><input style="width:25px" type="text" maxlength="3" name="ene[]" value="0" title="<?php echo $todo->descripcion;?>/Enero"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="feb[]" value="0" title="<?php echo $todo->descripcion;?>/Febrero"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="mar[]" value="0" title="<?php echo $todo->descripcion;?>/Marzo"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="abr[]" value="0" title="<?php echo $todo->descripcion;?>/Abril"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="may[]" value="0" title="<?php echo $todo->descripcion;?>/Mayo"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="jun[]" value="0" title="<?php echo $todo->descripcion;?>/Junio"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="jul[]" value="0" title="<?php echo $todo->descripcion;?>/Julio"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="ago[]" value="0" title="<?php echo $todo->descripcion;?>/Agosto"></th>
                             <th><input style="width:25px" type="text" maxlength="3" name="sep[]" value="0" title="<?php echo $todo->descripcion;?>/Septiembre"></th>
                                <th><input style="width:25px" type="text" maxlength="3" name="oct[]" value="0" title="<?php echo $todo->descripcion;?>/Octubre"></th>
                            <th><input style="width:25px" type="text" maxlength="3" name="nov[]" value="0" title="<?php echo $todo->descripcion;?>/Noviembre"></th>                            <th><input style="width:25px" type="text" maxlength="3" name="dic[]" value="0" title="<?php echo $todo->descripcion;?>/Diciembre"></th>
                            </tr>   
                          
 <?php endforeach ?>
</tbody>
</table>


</div><?php

echo form_submit('submit', 'Guardar Informacion','class="btn btn-primary"');	
?></div>





</div></div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>