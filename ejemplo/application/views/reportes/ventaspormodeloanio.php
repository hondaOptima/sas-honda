<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.back()">Dashboard</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                             <div class="row-fluid">
                                
                                 <div class="row-fluid">
                               
                                <div class="span12">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Ventas del Año VS Plan de Ventas  * Modelo * Agencia <?php echo $cd;?></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                              
 <div id="ch_sales" class="chart_a"></div>
                                        </div>
                                    </div>
                                    
                                    
     <?php $ncount=13; $x=0; 	$tottijSUMA=0; $totmexSUMA=0;  $totensSUMA=0; $sumglobalHonda=0;?>                               
                                    
                                    
  <?php if($cd=='Tijuana') {?>                                 
                                    <div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Tijuana 2014</h3> </div>
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody> 

                            <?php foreach($tij as $todoR): ?>  
                            <tr>
<th ><b><a href="<?php echo base_url();?>index.php/reporte/editarplanventas/<?php echo $todoR->plv_IDplan_ventas;?>"><?php echo $todoR->descripcion;?></a></b></th>
                                <th><?php echo $todoR->plv_ene;?></th>
                                <th><?php echo $todoR->plv_feb;?></th>
                                <th><?php echo $todoR->plv_mar;?></th>
                                <th><?php echo $todoR->plv_abri;?></th>
                                <th><?php echo $todoR->plv_may;?></th>
                               <th><?php echo $todoR->plv_jun;?></th>
                                <th><?php echo $todoR->plv_jul;?></th>
                                <th><?php echo $todoR->plv_ago;?></th>
                                <th><?php echo $todoR->plv_sep;?></th>
                                <th><?php echo $todoR->plv_oct;?></th>
                                <th><?php echo $todoR->plv_nov;?></th>
                                <th><?php echo $todoR->plv_dic;?></th>
                                <th><?php echo $tottij=($todoR->plv_ene + $todoR->plv_feb + $todoR->plv_mar +$todoR->plv_abri +$todoR->plv_may +$todoR->plv_jun +$todoR->plv_jul +$todoR->plv_ago +$todoR->plv_sep +$todoR->plv_oct +$todoR->plv_nov +$todoR->plv_dic);
							
								$tottijSUMA+=($todoR->plv_ene + $todoR->plv_feb + $todoR->plv_mar +$todoR->plv_abri +$todoR->plv_may +$todoR->plv_jun +$todoR->plv_jul +$todoR->plv_ago +$todoR->plv_sep +$todoR->plv_oct +$todoR->plv_nov +$todoR->plv_dic);
								?></th>
                            </tr>   
                           <?php endforeach ?>
                          
                          <tr>
                          <th>Total</th>
                          <th><?php $tensuma=0; for($a=0; $a<$ncount; $a++){$tensuma+=$tij[$a]->plv_ene;} echo $tensuma;?></th>
                          <th><?php $tfesuma=0; for($a=0; $a<$ncount; $a++){$tfesuma+=$tij[$a]->plv_feb;} echo $tfesuma;?></th>
                          <th><?php $tmrsuma=0; for($a=0; $a<$ncount; $a++){$tmrsuma+=$tij[$a]->plv_mar;} echo $tmrsuma;?></th>
                          <th><?php $tabsuma=0; for($a=0; $a<$ncount; $a++){$tabsuma+=$tij[$a]->plv_abri;} echo $tabsuma;?></th>
                          <th><?php $tmysuma=0; for($a=0; $a<$ncount; $a++){$tmysuma+=$tij[$a]->plv_may;} echo $tmysuma;?></th>
                          <th><?php $tjnsuma=0; for($a=0; $a<$ncount; $a++){$tjnsuma+=$tij[$a]->plv_jun;} echo $tjnsuma;?></th>
                          <th><?php $tjlsuma=0; for($a=0; $a<$ncount; $a++){$tjlsuma+=$tij[$a]->plv_jul;} echo $tjlsuma;?></th>
                          <th><?php $tagsuma=0; for($a=0; $a<$ncount; $a++){$tagsuma+=$tij[$a]->plv_ago;} echo $tagsuma;?></th>
                          <th><?php $tsesuma=0; for($a=0; $a<$ncount; $a++){$tsesuma+=$tij[$a]->plv_sep;} echo $tsesuma;?></th>
                          <th><?php $tocsuma=0; for($a=0; $a<$ncount; $a++){$tocsuma+=$tij[$a]->plv_oct;} echo $tocsuma;?></th>
                          <th><?php $tnosuma=0; for($a=0; $a<$ncount; $a++){$tnosuma+=$tij[$a]->plv_nov;} echo $tnosuma;?></th>
                          <th><?php $tdisuma=0; for($a=0; $a<$ncount; $a++){$tdisuma+=$tij[$a]->plv_dic;} echo $tdisuma;?></th>
                          <th><?php echo $tottijSUMA;?></th>
                          </tr>
                        </tbody>
                    </table>
</div
    ><?php } elseif($cd=='Mexicali') {?>                                    
        <div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Mexicali 2014</h3> </div><div class="box_a_content">
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($mex as $todoM): ?>  
                            <tr>
<th ><b><a href="<?php echo base_url();?>index.php/reporte/editarplanventas/<?php echo $todoM->plv_IDplan_ventas;?>"><?php echo $todoM->descripcion;?></a></b></th>
                                <th><?php echo $todoM->plv_ene;?></th>
                                <th><?php echo $todoM->plv_feb;?></th>
                                <th><?php echo $todoM->plv_mar;?></th>
                                <th><?php echo $todoM->plv_abri;?></th>
                                <th><?php echo $todoM->plv_may;?></th>
                               <th><?php echo $todoM->plv_jun;?></th>
                                <th><?php echo $todoM->plv_jul;?></th>
                                <th><?php echo $todoM->plv_ago;?></th>
                                <th><?php echo $todoM->plv_sep;?></th>
                                <th><?php echo $todoM->plv_oct;?></th>
                                <th><?php echo $todoM->plv_nov;?></th>
                                <th><?php echo $todoM->plv_dic;?></th>
                                <th><?php echo $totmex=($todoM->plv_ene + $todoM->plv_feb + $todoM->plv_mar +$todoM->plv_abri +$todoM->plv_may +$todoM->plv_jun +$todoM->plv_jul +$todoM->plv_ago +$todoM->plv_sep +$todoM->plv_oct +$todoM->plv_nov +$todoM->plv_dic);
							
								$totmexSUMA+=($todoM->plv_ene + $todoM->plv_feb + $todoM->plv_mar +$todoM->plv_abri +$todoM->plv_may +$todoM->plv_jun +$todoM->plv_jul +$todoM->plv_ago +$todoM->plv_sep +$todoM->plv_oct +$todoM->plv_nov +$todoM->plv_dic);
								?></th>
                           <?php endforeach ?>
                           
                            <tr>
                          <th>Total</th>
                          <th><?php $mensuma=0; for($a=0; $a<$ncount; $a++){$mensuma+=$mex[$a]->plv_ene;} echo $mensuma;?></th>
                          <th><?php $mfesuma=0; for($a=0; $a<$ncount; $a++){$mfesuma+=$mex[$a]->plv_feb;} echo $mfesuma;?></th>
                          <th><?php $mmrsuma=0; for($a=0; $a<$ncount; $a++){$mmrsuma+=$mex[$a]->plv_mar;} echo $mmrsuma;?></th>
                          <th><?php $mabsuma=0; for($a=0; $a<$ncount; $a++){$mabsuma+=$mex[$a]->plv_abri;}echo $mabsuma;?></th>
                          <th><?php $mmysuma=0; for($a=0; $a<$ncount; $a++){$mmysuma+=$mex[$a]->plv_may;} echo $mmysuma;?></th>
                          <th><?php $mjnsuma=0; for($a=0; $a<$ncount; $a++){$mjnsuma+=$mex[$a]->plv_jun;} echo $mjnsuma;?></th>
                          <th><?php $mjlsuma=0; for($a=0; $a<$ncount; $a++){$mjlsuma+=$mex[$a]->plv_jul;} echo $mjlsuma;?></th>
                          <th><?php $magsuma=0; for($a=0; $a<$ncount; $a++){$magsuma+=$mex[$a]->plv_ago;} echo $magsuma;?></th>
                          <th><?php $msesuma=0; for($a=0; $a<$ncount; $a++){$msesuma+=$mex[$a]->plv_sep;} echo $msesuma;?></th>
                          <th><?php $mocsuma=0; for($a=0; $a<$ncount; $a++){$mocsuma+=$mex[$a]->plv_oct;} echo $mocsuma;?></th>
                          <th><?php $mnosuma=0; for($a=0; $a<$ncount; $a++){$mnosuma+=$mex[$a]->plv_nov;} echo $mnosuma;?></th>
                          <th><?php $mdisuma=0; for($a=0; $a<$ncount; $a++){$mdisuma+=$mex[$a]->plv_dic;} echo $mdisuma;?></th>
                          <th><?php echo $totmexSUMA;?></th>
                          </tr>
                        </tbody>
                    </table>
</div></div>
                            
      <?php } elseif($cd=='Ensenada') {?>                                  
 <div class="box_a"> <div class="box_a_heading"> <h3>Plan de Ventas Ensenada 2014</h3> </div>
<table class="table">
                        <thead>
                            <tr>
                                <th style="width:80px">Auto</th>
                                <th>Ene</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Abr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ago</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dic</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($ens as $todoE): ?>  
                            <tr>
<th ><b><a href="<?php echo base_url();?>index.php/reporte/editarplanventas/<?php echo $todoE->plv_IDplan_ventas;?>"><?php echo $todoE->descripcion;?></a></b></th>
                                <th><?php echo $todoE->plv_ene;?></th>
                                <th><?php echo $todoE->plv_feb;?></th>
                                <th><?php echo $todoE->plv_mar;?></th>
                                <th><?php echo $todoE->plv_abri;?></th>
                                <th><?php echo $todoE->plv_may;?></th>
                               <th><?php echo $todoE->plv_jun;?></th>
                                <th><?php echo $todoE->plv_jul;?></th>
                                <th><?php echo $todoE->plv_ago;?></th>
                                <th><?php echo $todoE->plv_sep;?></th>
                                <th><?php echo $todoE->plv_oct;?></th>
                                <th><?php echo $todoE->plv_nov;?></th>
                                <th><?php echo $todoE->plv_dic;?></th>
                                <th><?php echo $sum=($todoE->plv_ene + $todoE->plv_feb + $todoE->plv_mar +$todoE->plv_abri +$todoE->plv_may +$todoE->plv_jun +$todoE->plv_jul +$todoE->plv_ago +$todoE->plv_sep +$todoE->plv_oct +$todoE->plv_nov +$todoE->plv_dic);
								
								$totensSUMA+=($todoE->plv_ene + $todoE->plv_feb + $todoE->plv_mar +$todoE->plv_abri +$todoE->plv_may +$todoE->plv_jun +$todoE->plv_jul +$todoE->plv_ago +$todoE->plv_sep +$todoE->plv_oct +$todoE->plv_nov +$todoE->plv_dic);
								?></th>
                           <?php endforeach ?>
                           
                            <tr>
                          <th>Total</th>
                          <th><?php $eensuma=0; for($a=0; $a<$ncount; $a++){$eensuma+=$ens[$a]->plv_ene;} echo $eensuma;?></th>
                          <th><?php $efesuma=0; for($a=0; $a<$ncount; $a++){$efesuma+=$ens[$a]->plv_feb;} echo $efesuma;?></th>
                          <th><?php $emrsuma=0; for($a=0; $a<$ncount; $a++){$emrsuma+=$ens[$a]->plv_mar;} echo $emrsuma;?></th>
                          <th><?php $eabsuma=0; for($a=0; $a<$ncount; $a++){$eabsuma+=$ens[$a]->plv_abri;}echo $eabsuma;?></th>
                          <th><?php $emysuma=0; for($a=0; $a<$ncount; $a++){$emysuma+=$ens[$a]->plv_may;} echo $emysuma;?></th>
                          <th><?php $ejnsuma=0; for($a=0; $a<$ncount; $a++){$ejnsuma+=$ens[$a]->plv_jun;} echo $ejnsuma;?></th>
                          <th><?php $ejlsuma=0; for($a=0; $a<$ncount; $a++){$ejlsuma+=$ens[$a]->plv_jul;} echo $ejlsuma;?></th>
                          <th><?php $eagsuma=0; for($a=0; $a<$ncount; $a++){$eagsuma+=$ens[$a]->plv_ago;} echo $eagsuma;?></th>
                          <th><?php $esesuma=0; for($a=0; $a<$ncount; $a++){$esesuma+=$ens[$a]->plv_sep;} echo $esesuma;?></th>
                          <th><?php $eocsuma=0; for($a=0; $a<$ncount; $a++){$eocsuma+=$ens[$a]->plv_oct;} echo $eocsuma;?></th>
                          <th><?php $enosuma=0; for($a=0; $a<$ncount; $a++){$enosuma+=$ens[$a]->plv_nov;} echo $enosuma;?></th>
                          <th><?php $edisuma=0; for($a=0; $a<$ncount; $a++){$edisuma+=$ens[$a]->plv_dic;} echo $edisuma;?></th>
                          <th><?php echo $totensSUMA;?></th>
                          </tr>
                        </tbody>
                    </table>
</div>                                   
     <?php } ?>                               
                                    
                                    
                                    
                                </div>
                                
                                
                                
                               
                                
                                      
                                </div>
                            </div>
                            
                        </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/jsDashboard'); ?> 
  
  <?php
if($cd=="Tijuana"){$cd=1;}
elseif($cd=="Mexicali"){$cd=2;}
elseif($cd=='Ensenada'){$cd=3;}
$suno=$this->Reportesmodel->plv_year_mod($cd,date('Y'),1);
$sdos=$this->Reportesmodel->plv_year_mod($cd,date('Y'),2);
$stre=$this->Reportesmodel->plv_year_mod($cd,date('Y'),3);
$scua=$this->Reportesmodel->plv_year_mod($cd,date('Y'),4);
$scin=$this->Reportesmodel->plv_year_mod($cd,date('Y'),5);
$ssei=$this->Reportesmodel->plv_year_mod($cd,date('Y'),6);
$ssie=$this->Reportesmodel->plv_year_mod($cd,date('Y'),7);
$soch=$this->Reportesmodel->plv_year_mod($cd,date('Y'),8);
$snue=$this->Reportesmodel->plv_year_mod($cd,date('Y'),9);
$sdie=$this->Reportesmodel->plv_year_mod($cd,date('Y'),10);
$sonc=$this->Reportesmodel->plv_year_mod($cd,date('Y'),11);
$sdoc=$this->Reportesmodel->plv_year_mod($cd,date('Y'),12);
$strc=$this->Reportesmodel->plv_year_mod($cd,date('Y'),13);
function vmaset_mod($todo,$modelo){
$num=0;	
foreach($todo as $tod){
 $tod->data_modelo;
if (strpos($tod->data_modelo, ''.$modelo.'')!==false) {$num++;}
	}	
	return $num;
	}
  ?>
  
 <script type='text/javascript'>
   $(document).ready(function(){
    // For horizontal bar charts, x an y values must will be "flipped"
      var elem = $('#ch_sales');

        var build_pass_data = [
                   [1, <?php echo vmaset_mod($todo,'FIT');?>],
                [2, <?php echo vmaset_mod($todo,'CITY');?>],
                [3, <?php echo vmaset_mod($todo,'CIVIC');?>],
				[4, 0],
                [5, 0],
                [6, <?php echo vmaset_mod($todo,'ACCORD');?>],
				[7, 0],
                [8, <?php echo vmaset_mod($todo,'CRV');?>],
                [9, <?php echo vmaset_mod($todo,'PILOT');?>],
				[10, <?php echo vmaset_mod($todo,'ODYSSEY');?>],
                [11, <?php echo vmaset_mod($todo,'RIDGELINE');?>],
                [12, <?php echo vmaset_mod($todo,'CROSSTOUR');?>],
				[13, <?php echo vmaset_mod($todo,'CRZ');?>]
            ];
var build_fail_data = [
                [1, <?php echo $suno[0]->suma;?>],
                [2, <?php echo $sdos[0]->suma;?>],
                [3, <?php echo $stre[0]->suma;?>],
				[4, <?php echo $scua[0]->suma;?>],
                [5, <?php echo $scin[0]->suma;?>],
                [6, <?php echo $ssei[0]->suma;?>],
				[7, <?php echo $ssie[0]->suma;?>],
                [8, <?php echo $soch[0]->suma;?>],
                [9, <?php echo $snue[0]->suma;?>],
				[10, <?php echo $sdie[0]->suma;?>],
                [11, <?php echo $sonc[0]->suma;?>],
                [12, <?php echo $sdoc[0]->suma;?>],
				[13, <?php echo $strc[0]->suma;?>]
				
            ];
 
var ds = [
            {label: "Ventas", data: build_pass_data, bars: {fillColor: "#0094bb"}, color: "#0094bb"},
            {label: "Plande Ventas", data: build_fail_data, bars: {fillColor: "#86ae00"}, color: "#86ae00"}
           ]
                
                var options = {
					
							
                xaxis: {
                    min: 0,
                    max: 14,
                    mode: null,
                    ticks: [
                        [1, "Fit"],
                        [2, "City"],
                        [3, "Civic"],
						[4, "Civic Si"],
                        [5, "Civic IMA"],
                        [6, "Accord 2D"],
						[7, "Accord 4D"],
                        [8, "CR-V"],
                        [9, "Pilot"],
						[10, "Odyssey"],
                        [11, "Ridgeline"],
                        [12, "Crosstour"],
						[13, "CRZ"]
                    ],
                    tickLength: 0,
                    axisLabel: "App",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 14,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, yaxis: {
                    axisLabel: "No of builds",
                    tickDecimals: 0,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 14,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 1
                },
                        tooltip: true,
                        tooltipOpts: {
                            content: "%s - %y",
                            shifts: {
                                x: 20,
                                y: 0
                            },
                            defaultTheme: false
                        }, legend: {
                    labelBoxBorderColor: "none",
                    position: "right"
                }, series: {
                    shadowSize: 1,
					
                    bars: {
                        show: true,
                        barWidth: 0.40,
                        order: 1,
						  showNumbers: true,
            numbers : {
                yAlign: function(y) { return y + 18; },
            },					
						
                    }
                },legend: {labelBoxBorderColor:"white"}
            };
                
                $.plot(elem, ds, options);
				

});
   </script>
   </script>
    </body>
</html>