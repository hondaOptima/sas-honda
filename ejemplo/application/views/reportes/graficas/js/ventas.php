<script type='text/javascript'>
   $(document).ready(function(){
	    $.jqplot.config.enablePlugins = true;
    // For horizontal bar charts, x an y values must will be "flipped"
      var elem = $('#ch_sales');

        var build_pass_data = [
                [1, <?php echo $totalVentasMes[0]->total + $totalVentasMes[1]->total;?>],
                [2, <?php echo $totalVentasMes[2]->total + $totalVentasMes[3]->total;?>],
                [3, <?php echo $totalVentasMes[4]->total + $totalVentasMes[5]->total;?>]
            ];
var build_fail_data = [
                [1, <?php echo $planMes[0]->suma;?>],
                [2, <?php echo $planMes[1]->suma;?>],
                [3, <?php echo $planMes[2]->suma;?>]
            ];
 
var ds = [
            {label: "Ventas", data: build_pass_data, bars: {fillColor: "#0094bb"}, color: "#0094bb"},
            {label: "Plan de Ventas", data: build_fail_data, bars: {fillColor: "#86ae00"}, color: "#86ae00"}
           ];
		   
		  
                
                var options = {
					 animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
                xaxis: {
                    min: 0,
                    max: 4,
                    mode: null,
                    ticks: [
                        [1, "Tijuana"],
                        [2, "Mexicali"],
                        [3, "Ensenada"]
                    ],
                    tickLength: 0,
                    axisLabel: "No of builds",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 14,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5,
				
                }, yaxis: {
					 min: 0,
                    max: 60,
                    axisLabel: "No of builds",
                    tickDecimals: 0,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 18,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5,
					
					}, grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 1
                },
                        tooltip: true,
                        tooltipOpts: {
                            content: "%s - %y",
                            shifts: {
                                x: 20,
                                y: 0
                            },
                            defaultTheme: false
                        }, legend: {
                    labelBoxBorderColor: "none",
                    position: "right"
                }, series: {
					 
                    shadowSize: 1,
                    bars: {
                        show: true,
                        barWidth: 0.40,
                        order: 1,
						
						
            showNumbers: true,
            numbers : {
                yAlign: function(y) { return y + 3; },
            },			
						
                    },
                },
				legend: {
					labelBoxBorderColor:"#cccccc"
        },
            };
$.plot(elem, ds, options);
				
$("#ch_sales").bind("plotclick", function (event, pos, item) {

var x = item.datapoint[0].toFixed(0),y = item.datapoint[1].toFixed(2);
   if(x==1){
	   window.location='<?php echo base_url();?>index.php/reporte/ventaspormodelo/?cd=Tijuana';
	   }
	   
	    if(x==2){
	   window.location='<?php echo base_url();?>index.php/reporte/ventaspormodelo/?cd=Mexicali';
	   }
	   
	    if(x==3){
	   window.location='<?php echo base_url();?>index.php/reporte/ventaspormodelo/?cd=Ensenada';
	   }
   
});


//ventas Acumulado


var elem = $('#ch_sales_year');
  var build_pass_data = [
                   [1, <?php echo $totalVentasAnual[0]->total + $totalVentasAnual[1]->total;?>],
                [2, <?php echo $totalVentasAnual[2]->total + $totalVentasAnual[3]->total;?>],
                [3, <?php echo $totalVentasAnual[4]->total + $totalVentasAnual[5]->total;?>]
            ];
var build_fail_data = [
                [1, <?php echo $planAnual[0]->suma;?>],
                [2, <?php echo $planAnual[1]->suma;?>],
                [3, <?php echo $planAnual[2]->suma;?>]
            ];
 
var ds = [
            {label: "Ventas", data: build_pass_data, bars: {fillColor: "#0094bb"}, color: "#0094bb"},
            {label: "Plan de Ventas", data: build_fail_data, bars: {fillColor: "#86ae00"}, color: "#86ae00"}
           ]
                
                var options = {
					
							
                xaxis: {
                    min: 0,
                    max: 4,
                    mode: null,
                    ticks: [
                        [1, "Tijuana"],
                        [2, "Mexicali"],
                        [3, "Ensenada"]
                    ],
                    tickLength: 0,
                    axisLabel: "App",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 14,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, yaxis: {
					 min: 0,
                    max: 700,
                    axisLabel: "No of builds",
                    tickDecimals: 0,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 16,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 1
                },
                        tooltip: true,
                        tooltipOpts: {
                            content: "%s - %y",
                            shifts: {
                                x: 20,
                                y: 0
                            },
                            defaultTheme: false
                        }, legend: {
                    labelBoxBorderColor: "none",
                    position: "right"
                }, series: {
					 
                    shadowSize: 1,
                    bars: {
                        show: true,
                        barWidth: 0.40,
                        order: 1,
						
						
            showNumbers: true,
            numbers : {
                yAlign: function(y) { return y + 32; },
            },			
						
                    },
                },legend: {labelBoxBorderColor:"#cccccc"}
            };
                
                $.plot(elem, ds, options);
$("#ch_sales_year").bind("plotclick", function (event, pos, item) {

var x = item.datapoint[0].toFixed(0),y = item.datapoint[1].toFixed(2);
   if(x==1){
	   window.location='<?php echo base_url();?>index.php/reporte/ventaspormodeloA/?cd=Tijuana';
	   }
	   
	    if(x==2){
	   window.location='<?php echo base_url();?>index.php/reporte/ventaspormodeloA/?cd=Mexicali';
	   }
	   
	    if(x==3){
	   window.location='<?php echo base_url();?>index.php/reporte/ventaspormodeloA/?cd=Ensenada';
	   }
   
});

			

});
   </script>
   