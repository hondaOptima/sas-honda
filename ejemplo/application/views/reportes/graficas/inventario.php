 <div class="row-fluid">
                               
                                <div class="span4">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Inventario Tijuana </h3>
                                            <h3 class="natij"  style="float:right; margin-right:50px;"></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div id="chart_pie" class="chart_a">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="span4">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Inventario Mexicali</h3>
                                            <h3 class="namex"  style="float:right;  margin-right:50px;"></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div id="chart_piem" class="chart_a"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="span4">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Inventario Ensenada </h3>
                                            <h3 class="naens"  style="float:right;  margin-right:50px;"></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div id="chart_piee" class="chart_a"></div>
                                        </div>
                                    </div>
                                </div>

                                      
                                </div>