<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                          
                    
                   <br>  
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                        <div class="box_a_content">
 <div class="box_a_heading">
                                            <h3>Reporte Intellisis <?php echo date('d M Y');?></h3>
                                        </div>
                                        <div class="box_a_content no_sp">
                                        
                                       
                                            <table id="dt_table_toolsc" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                               <th >Ejercicio</th>
                                  <th >Color</th>
                                  <th >Vin</th>  
                                  <th >Fecha de Compra</th>
                                  <th >Dias inv.</th>
								  <th>Precio venta</th>
                                  <th>ISAN</th>
								  <th>IVA</th>
                                  <th>Total</th> 
                                  <th>Costo</th> 
                                  <th>Utilidad</th> 
                                  <th>Agente</th> 
							  </tr>
						  </thead>   
                                               
                                               
                                            </table>
                                            
                                            
                                            
 
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>