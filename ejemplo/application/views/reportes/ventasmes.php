<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                            <div class="row-fluid">
                                <div class="stat_boxes">
                                <div class="span3 stat_box"> 
                                            <div class="peity_canvas peity_line_down">16,8,12,7,6,5,3</div>
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/">
                                            Actividades Realizadas
                                            </a>
                                            </p>
                                        </div>
                                        <div class="span3 stat_box"> 
                                           <div class="peity_canvas peity_bar_up">2,5,3,6,8,5</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/ventasmes">
                                            Ventas de Mes
                                            </a>
                                            </p>
                                        </div>
                                         <div class="span3 stat_box"> 
                                             <div class="peity_canvas peity_line_up">3,5,9,7,12,8,16</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/inventario">
                                            Inventario consumido
                                            </a>
                                            </p>
                                        </div>                                        
                                         <div class="span3 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-th-list" ></i></div>
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/ventas/">
                                            Diseñador de consulta
                                            </a>
                                            </p>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            
                     
                    
                   <br>  
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Reporte de ventas</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<div class="row-fluid">                                        
  <form  name="form1" method="get"  action="<?php echo base_url(); ?>index.php/reporte/ventasmes">
   <div class="span3"><h4 class="heading_b">Seleccione un rango de fechas de facturación</h4>
  
 <div class="row-fluid">
 
 <input class="span4"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $finicio;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
<input class="span4" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $ffin;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
</div>  </div>

<?php if($_SESSION['username']=='josemaciel@hondaoptima.com' || $_SESSION['username']=='sgutierrez@hondaoptima.com') { ?>
 <div class="span3">
                                                    <h4 class="heading_b">Ciudad</h4>
<div class="span3 "><div class="control-group"><div class="controls">
<select name="ciudad">
<option  value="0" <?php if($ciudad==0){ echo 'selected="selected"';}?>>Todas las ciudades</option>
<option value="Tijuana" <?php if($ciudad=="Tijuana"){ echo 'selected="selected"';}?>>Tijuana</option>
<option value="Mexicali" <?php if($ciudad=="Mexicali"){ echo 'selected="selected"';}?>>Mexicali</option>
<option value="Ensenada" <?php if($ciudad=="Ensenada"){ echo 'selected="selected"';}?>>Ensenada</option>
</select>
</div></div></div>                                                    
                                                    
                                                </div>
                                                <?php } else{?> 
                                                 
 <div class="span3">
                                                    <h4 class="heading_b">Seleccione un asesor</h4>
<div class="span3 "><div class="control-group"><div class="controls">
 <?php echo form_dropdown('vendedor', $todo_vendedores,$vendedor,'style="width:246px;" id="s2_single" data-rel="chosen"');?>
</div></div></div>
                                                </div>
                                              
    <?php } ?>                                          

                                                                                            
                                                 <div class="span2">
                                                    <h4 class="heading_b">Hacer la consulta</h4>
                                                    <button type="submit" class="btn btn-primary" >Consultar</button>
                                                </div>
                                                </form>  
</div>                                      
<br>


 <div class="box_a_heading">
                                            <h3>Ventas Entregados y Facturados con Fecha de Entrega</h3>
                                        </div>
                                        <div class="box_a_content no_sp">
                                        
                                  <table id="foo_example"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr><?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
                                  <th data-class="expand" >Ciudad</th>
                                  <th data-class="expand" >Asesor</th>  <?php } ?>
                                  <th data-class="expand"  >Contacto</th>
                                  <th data-class="expand"  >Cliente</th>
                                  <th data-class>#</th>
                                  <th data-class>Tipo</th>
								  <th data-hide="phone"  >Modelo</th>
								  <th data-hide="phone">Color</th>
                                  <th data-hide="phone,tablet" >A&ntilde;o</th>
                                  <th data-hide="phone" >Vin</th>               
                                  <th data-hide="phone" >Operaci&oacute;n</th>  
                                  <th  data-hide="phone">Status</th>
								  <th data-hide="phone">Factu..</th>
								  <th data-hide="phone">Entrega</th>
								  <th data-class="small" >Acciones</th>
							  </tr>
						  </thead>
<tbody>
<?php if($todo_ventas){ $i=0;$x=0;$y=0;foreach($todo_ventas as $todo): ?>
<?php 

	
if($todo->datco_snuevo=='si'){
?>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_ciudad; ?></td>
<td><?php echo $todo->hus_nombre; ?></td>
<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if($todo->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$todo->datc_primernombre.' '.$todo->datc_segundonombre.' '.$todo->datc_apellidopaterno.' '.$todo->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$todo->datc_moral; echo ucwords(strtolower($facturaNombre));}
?>
</td>
<td><?php $i++; echo $i;?></td>
<td>
Nuevo
</td>
<td>
<?php 

echo ucwords(strtolower($todo->data_modelo));
?>
</td>
<td>
<?php 
echo ucwords(strtolower($todo->data_color));
?>
</td>
<td>
<?php 
echo ucwords(strtolower($todo->data_ano));
?>
</td>
<td>
<?php 
 echo ucwords(strtolower($todo->data_vin));
?>
</td>
<td>
<?php 

$fina=ucwords(strtolower($todo->dats_compra));
if($fina=='Contado' || $fina=='Conta'){
	echo ucwords(strtolower($todo->dats_compra));
	}
	else{
		echo ucwords(strtolower($todo->dats_financiera));
		}

?>
</td>
<td>
<?php 

if($todo->prcv_status=='venta'){
	echo '<span class="label label-success" >Entregado</span>';
	}else{
	echo '<span class="label label-warning" >Facturado</span>';}



?>
</td>
<td><?php
echo $todo->dat_fecha_facturacion;
?>
</td>
<td><?php
echo $todo->dat_fecha_entrega;
?>
</td>

<td >
 <ul class="nav">
                            <li class="dropdown dropdown-right">
                                <a class="btn btn-small" data-toggle="dropdown" href="#">Acciones</a>
                                <ul class="dropdown-menu">
                                    <li>
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a">Bitacora</a>
                                    </li>
                                    <li>
<a title="Bitacora" target="_blank" href="<?php echo base_url()."index.php/contacto/viewordendecomprapdf/$todo->dor_IDdordencomprat";?>" class="sepV_a">Orden de Compra</a>
                                    </li>

                              
                                </ul>
                            </li>
                            </ul>
</td>
</tr>
<?php }elseif($todo->datco_nuevox=='si'){ ?>
<tr>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_ciudad; ?></td>
<td><?php echo $todo->hus_nombre; ?></td>
<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if($todo->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$todo->datc_primernombre.' '.$todo->datc_segundonombre.' '.$todo->datc_apellidopaterno.' '.$todo->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$todo->datc_moral; echo ucwords(strtolower($facturaNombre));}
?>
</td>
<td><?php $x++; echo $x;?></td>
<td>
Semi nuevos
</td>
<td>
<?php 
echo ucwords(strtolower($todo->data_modelo));
?>
</td>
<td>
<?php 
echo ucwords(strtolower($todo->data_color));
?>
</td>
<td>
<?php 
echo $todo->data_ano;
?>
</td>
<td>
<?php 
echo $todo->data_vin;
?>
</td>
<td>
<?php 

$fina=ucwords(strtolower($todo->dats_compra));
if($fina=='Contado' || $fina=='Conta'){
	echo ucwords(strtolower($todo->dats_compra));
	}
	else{
		echo ucwords(strtolower($todo->dats_financiera));
		}

?>
</td>
<td>
<?php 
if($todo->prcv_status=='venta'){
echo '<span class="label label-success" > Entregado</span>';
}else{
echo '<span class="label label-warning" > Facturado</span>';}
?>
</td>
<td><?php
echo $todo->dat_fecha_facturacion;
?>
</td>
<td><?php
echo $todo->dat_fecha_entrega;
?>
</td>

<td >
 <ul class="nav">
                            <li class="dropdown dropdown-right">
                                <a class="btn btn-small" data-toggle="dropdown" href="#">Acciones</a>
                                <ul class="dropdown-menu">
                                    <li>
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a">Bitacora</a>
                                    </li>
                                    <li>
<a title="Bitacora" target="_blank" href="<?php echo base_url()."index.php/contacto/viewordendecomprapdf/$todo->dor_IDdordencomprat";?>" class="sepV_a">Orden de Compra</a>
                                    </li>

                              
                                </ul>
                            </li>
                            </ul>
</td>
</tr>

<?php }elseif($todo->datco_nuevo=='si'){ ?>
<tr>
<?php if($_SESSION['nivel']=='Administrador' || $_SESSION['nivel']=='Recepcion'){?>
<td><?php echo $todo->hus_ciudad; ?></td>
<td><?php echo $todo->hus_nombre; ?></td>
<?php } ?>  
<td ><?php $ntxt=substr($todo->con_titulo.' '.$todo->con_nombre.' '.$todo->con_apellido,0,35);echo ucwords(strtolower($ntxt));?></td>
<td>
<?php 
if($todo->datc_persona_fisica_moral=='fisica'){
$facturaNombre=$todo->datc_primernombre.' '.$todo->datc_segundonombre.' '.$todo->datc_apellidopaterno.' '.$todo->datc_apellidomaterno;
echo ucwords(strtolower($facturaNombre));}
else{ $facturaNombre=$todo->datc_moral; echo ucwords(strtolower($facturaNombre));}
?>
</td>
<td><?php $y++; echo $y;?></td>
<td>
Demo
</td>
<td>
<?php 
echo ucwords(strtolower($todo->data_modelo));
?>
</td>
<td>
<?php 
echo ucwords(strtolower($todo->data_color));
?>
</td>
<td>
<?php 
echo $todo->data_ano;
?>
</td>
<td>
<?php 
echo $todo->data_vin;
?>
</td>
<td>
<?php 
echo ucwords(strtolower($todo->dats_financiera));
?>
</td>
<td>
<?php 

if($todo->prcv_status=='venta'){
echo '<span class="label label-success" > Entregado</span>';
}else{
echo '<span class="label label-warning" > Facturado</span>';}
?>
</td>
<td><?php
echo $todo->dat_fecha_facturacion;
?>
</td>
<td><?php
echo $todo->dat_fecha_entrega;
?>
</td>

<td >
 <ul class="nav">
                            <li class="dropdown dropdown-right">
                                <a class="btn btn-small" data-toggle="dropdown" href="#">Acciones</a>
                                <ul class="dropdown-menu">
                                    <li>
<a title="Bitacora" href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>" class="sepV_a">Bitacora</a>
                                    </li>
                                    <li>
<a title="Bitacora" target="_blank" href="<?php echo base_url()."index.php/contacto/viewordendecomprapdf/$todo->dor_IDdordencomprat";?>" class="sepV_a">Orden de Compra</a>
                                    </li>

                              
                                </ul>
                            </li>
                            </ul>
</td>
</tr>
<?php } ?>

<?php endforeach; }?>
</tbody>
                            
                          </table>               
                                            
                                            
                                            

                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
  <script type='text/javascript'>

	$(document).ready(function() {
				

$('#s23_single').live('click',function(){
		
var tipo=$('select[name=tipocontacto]').val();
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontactoadmin/'+tipo;
});

$('#s24_single').live('click',function(){
var tipo=$('select[name=tipocontacto]').val();	
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontacto/'+tipo;	
});
	});	
</script>

<script type='text/javascript'>

	$(document).ready(function() {

$('#foo_example').dataTable( {

		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
					"aaSorting": [[ 5, "desc" ]],
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
} );
</script>
    </body>
</html>