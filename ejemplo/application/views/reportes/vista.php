<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                            <div class="row-fluid">
                                <div class="stat_boxes">
                                <div class="span3 stat_box"> 
                                            <div class="peity_canvas peity_line_down">16,8,12,7,6,5,3</div>
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/">
                                            Actividades Realizadas
                                            </a>
                                            </p>
                                        </div>
                                        <div class="span3 stat_box"> 
                                           <div class="peity_canvas peity_bar_up">2,5,3,6,8,5</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/ventasmes">
                                            Ventas de Mes
                                            </a>
                                            </p>
                                        </div>
                                         <div class="span3 stat_box"> 
                                             <div class="peity_canvas peity_line_up">3,5,9,7,12,8,16</div>
                                            
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/inventario">
                                            Inventario consumido
                                            </a>
                                            </p>
                                        </div>                                        
                                         <div class="span3 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-th-list" ></i></div>
                                            <p class="stat_expl">
                                            <a href="<?php echo base_url(); ?>index.php/reporte/ventas/">
                                            Diseñador de consulta
                                            </a>
                                            </p>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            
                     
                    
                   <br>  
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Control de Actividades</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<div class="row-fluid">                                        
  <form  name="form1" method="get"  action="<?php echo base_url(); ?>index.php/reporte/">
   <div class="span4"><h4 class="heading_b">Seleccione un rango de fechas</h4>
  
 <div class="row-fluid">
 
 <input class="span4"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $finicio;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
<input class="span4" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $ffin;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
</div>  </div>

 <div class="span4">
                                                    <h4 class="heading_b">Seleccione un asesor</h4>
<div class="span3 "><div class="control-group"><div class="controls">
 <?php echo form_dropdown('vendedor', $todo_vendedores,$vendedor,'style="width:246px;" id="s2_single" data-rel="chosen"');?>
</div></div></div>
                                                </div>
                                              
                                                 <div class="span4">
                                                    <h4 class="heading_b">Hacer la consulta</h4>
                                                    <button type="submit" class="btn btn-primary" >Consultar</button>
                                                </div>
                                                </form>  
</div>                                      
<br>


 <div class="box_a_heading">
                                            <h3>Actividades no realizadas</h3>
                                        </div>
                                        <div class="box_a_content no_sp">
                                        
     <?php if($todo_no_realizadas): ?>                                    
                                            <table id="dt_table_tools" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                               <th>Status</th>
                              
                                  
								  <th >Fecha</th>  
                                  <th >Asesor</th>
								  <th >Contacto</th>
                                  <th >Tel&eacute;fono</th>
                                  <th >Correo</th>
                                  <th >Status Contacto</th>
								  <th>Titulo</th>
								  <th>Descripción</th>
								  <th>Ver</th> 
							  </tr>
						  </thead>   
                                              
												<?php foreach($todo_no_realizadas as $todo): ?>
                                                    <tr>
                                                        <td><?php echo $todo->act_status;?></td>
                                                        <td><?php echo $todo->act_fecha_inicio;?></td>
                                                        <td><?php echo $todo->hus_nombre.' '.$todo->hus_apellido;?></td>
                                                        <td><?php echo $todo->con_nombre.' '.$todo->con_apellido;?></td>
                                                        <td><?php echo $todo->con_telefono_officina.' '.$todo->con_telefono_casa.' ';?></td>
                                                        <td><?php echo $todo->con_correo;?></td>
                                                        <td><?php echo $todo->con_status;?></td>
                                                        <td><?php echo $todo->act_titulo;?></td>
                                                         <td><?php echo $todo->act_descripcion;?></td>
                                                        <td><div class="btn btn-small">
                                                        <a href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>">
                                                        Bitacora
                                                        </a>
                                                        </div></td>
                                                    </tr>
                                                <?php endforeach ?>
                                                </tbody>
                                            </table>
											<?php else: ?>No se encontraron registros.<?php endif ?>	
                                        </div>
                                        
<br><br>

 <div class="box_a_heading">
                                            <h3>Actividades realizadas</h3>
                                            
                                        </div>
                                        <div class="box_a_content no_sp">
 <?php if($todo_realizadas): ?>                                           
  <table id="dt_table_toolsb" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                               <th>Status</th>
                              
                                  
								  <th >Fecha</th>  
                                  <th >Asesor</th>
								  <th >Contacto</th>
                                  <th >Tel&eacute;fono</th>
                                  <th >Correo</th>
                                  <th >Status Contacto</th>
								  <th>Titulo</th>
								  <th>Descripción</th>
								  <th>Ver</th> 
							  </tr>
						  </thead>   
                                                <tbody class="todo">
                                                   
												<?php foreach($todo_realizadas as $todo): ?>
                                                    <tr>
                                                        <td><?php echo $todo->act_status;?></td>
                                                        <td><?php echo $todo->act_fecha_inicio;?></td>
                                                        <td><?php echo $todo->hus_nombre.' '.$todo->hus_apellido;?></td>
                                                        <td><?php echo $todo->con_nombre.' '.$todo->con_apellido;?></td>
                                                        <td><?php echo $todo->con_telefono_officina.' '.$todo->con_telefono_casa.' ';?></td>
                                                        <td><?php echo $todo->con_correo;?></td>
                                                        <td><?php echo $todo->con_status;?></td>
                                                        <td><?php echo $todo->act_titulo;?></td>
                                                         <td><?php echo $todo->act_descripcion;?></td>
                                                        <td><div class="btn btn-small">
                                                        <a href="<?php echo base_url()."index.php/contacto/bitacora/$todo->con_IDcontacto";?>">
                                                        Bitacora
                                                        </a>
                                                        </div></td>
                                                    </tr>
                                                <?php endforeach ?>
                                                </tbody>
                                            </table>
											<?php else: ?>No se encontraron registros.<?php endif ?>	
                                        </div>                                        
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>