<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/reporte/ventas/">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->

                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Filtros</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <div class="row-fluid">
                                        <form name="form1" method="get" action="<?php echo base_url();?>index.php/reporte/usuarios/">
                                      
                                                <div class="span3">
                                                    <h4 class="heading_b">Ciudad</h4>
                               
                                                    <?php
$options = array(
                  'Tijuana'  => 'Tijuana',
                  'Mexicali'    => 'Mexicali',
                  'Ensenada'   => 'Ensenada',
				  '0'   => 'Todas las ciudades',
                );
echo form_dropdown('cid', $options, $cid );
                                                    ?>
                                                </div>
                                                
                                                <div class="span3">
                                                    <h4 class="heading_b">Nivel</h4>
                               
                                                    <?php
$options = array(
                  'Administrador'  => 'Administrador',
                  'Recepcion'    => 'Recepcion',
                  'Vendedor'   => 'Vendedor',
				  '0'   => 'Todos',
                );
echo form_dropdown('nivel', $options, $nivel );
                                                    ?>
                                                </div>
                                                 <div class="span3">
                                                    <h4 class="heading_b">Status</h4>
                                                   
                                                    
                                                    <?php
                                                    $options = array(
                  'Empleado'  => 'Empleado',
                  'Ex Empleado'    => 'Ex Empleado',
				  '0'   => 'Todos',
                );
echo form_dropdown('status', $options, $status );
													?>
                                                </div>
                                                <div class="span3">
                                                    <h4 class="heading_b">Accion</h4>
                                                    <input type="submit" value="Buscar">
                                                </div>
                                                </form>
                                                </div>
                                        </div>
                                    
                                    </div></div></div> 
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Reporte Usuarios </h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <div class="box_a_content no_sp">
                                        
                                  <table id="foo_example"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr><th data-hide="phone" >#</th>  
                                  <th data-class="expand" >Ciudad</th>
                                  <th data-class>Nivel</th> 
                                  <th data-hide="phone"  >Status</th>
                                  <th data-class="expand" >Nombre</th>  
                                  <th data-class="expand"  >Apellido</th>
                                  <th data-class="expand"  >Telefono</th>
                                  <th data-hide="phone" >Celular</th>   
                                  <th data-class>E-mail</th>
                                  
								 
								  <th data-hide="phone">Radio_Honda</th>
                                  <th data-hide="phone,tablet" >Radio_Personal</th>
                                              
							  </tr>
						  </thead>
<tbody>
 <?php if($datax): ?>
<?php $i=0;foreach($datax as $todo): ?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $todo->hus_ciudad;?></td>
<td><?php echo $todo->hus_nivel;?></td>
<td><?php echo $todo->hus_status;?></td>
<td><?php echo $todo->hus_nombre;?></td>
<td><?php echo $todo->hus_apellido;?></td>
<td><?php echo $todo->hus_telefono;?></td>
<td><?php echo $todo->hus_celular;?></td>
<td><?php echo $todo->hus_correo;?></td>

<td><?php echo $todo->hus_radio_honda;?></td>
<td><?php echo $todo->hus_radio_personal;?></td>

</tr>
<?php $i++;?>
<?php endforeach;?>
<?php else: ?>No se encontraron registros.<?php endif ?>
</tbody>
                            
                          </table>               
                                            
                                            
                                            

                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
  <script type='text/javascript'>

	$(document).ready(function() {
				

$('#s23_single').live('click',function(){
		
var tipo=$('select[name=tipocontacto]').val();
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontactoadmin/'+tipo;
});

$('#s24_single').live('click',function(){
var tipo=$('select[name=tipocontacto]').val();	
window.location='<?php echo base_url(); ?>index.php/contacto/tipocontacto/'+tipo;	
});
	});	
</script>

<script type='text/javascript'>

	$(document).ready(function() {

$('#foo_example').dataTable( {
		            "bPaginate":false,
					 "bAutoWidth": true,
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
					"aaSorting": [[ 5, "desc" ]],
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#foo_example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );
	
	
} );
</script>
    </body>
</html>