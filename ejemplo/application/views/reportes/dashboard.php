<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                             <div class="row-fluid">
                                <div class="span12">
                                    <div class="grid_alt clearfix">
                                        <div class="grid_item grid_space">
                                            <h2 class="heading_a">Tijuana 
<?php $suma=$prospectost->num + $calientest->num + $procesot->num +$caidast->num + $ventasMest;?></h2>
<?php $tott=($prospectost->num / $suma); $xx=$tott * 100;?>
                                            <div class="progress progress-info progress-small">

<div class="bar" style="width:<?php echo $xx;?>%"></div></div>
                                            <div class="help-block">
                                            <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Tijuana&tp=Prospecto">
                                            Prospectos <div style="float:right"> <?php echo $prospectost->num;?></div>
                                            </a>
                                            </div>
<?php $tott=($calientest->num / $suma); $xx2=$tott * 100;?>                                            
                                            <div class="progress progress-success progress-small"><div class="bar" style="width: <?php echo $xx2;?>%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Tijuana&tp=Caliente">
                                            Calientes <div style="float:right"> <?php echo $calientest->num;?></div>
                                            </a></div>
<?php $tott=($procesot->num / $suma); $xx3=$tott * 100;?>                                              
                                            <div class="progress progress-warning progress-small"><div class="bar" style="width:<?php echo $xx3;?>%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Tijuana&tp=Proceso de Venta">
                                            Proceso de venta <div style="float:right"> <?php echo $procesot->num;?></div>
                                            </a></div>
<?php $tott=($caidast->num / $suma); $xx4=$tott * 100;?>                                               
                                            <div class="progress progress-danger progress-small"><div class="bar" style="width:<?php echo $xx4;?>%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Tijuana&tp=Caida">
                                            Ventas Caidas <div style="float:right"> <?php echo $caidast->num;?></div>
                                            </a>
                                            </div>
<?php $tott=($ventasMest / $suma); $xx5=$tott * 100;?>                                             
                                            <div class="progress progress-info progress-small"><div class="bar" style="width:<?php echo $xx5;?>%"></div></div>
                                            <div class="help-block">
<a href="http://hondaoptima.com/ventas/index.php/reporte/ventasmes?finicio=01-01-2014&ffin=31-01-2014&ciudad=Tijuana&vendedor=no">
Ventas del Mes  <div style="float:right"> <?php echo $ventasMest;?></div>
</a>
                                            </div>
                                          
                                        </div>
                                        <div class="grid_item grid_space stat_boxes">
                                            <h2 class="heading_a">Mexicali</h2>
                                            <div class="progress progress-info progress-small"><div class="bar" style="width: 20%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Mexicali&tp=Prospecto">
                                            Prospectos <div style="float:right"> <?php echo $prospectosm->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-success progress-small"><div class="bar" style="width: 40%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Mexicali&tp=Caliente">
                                            Calientes <div style="float:right"> <?php echo $calientesm->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-warning progress-small"><div class="bar" style="width: 60%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Mexicali&tp=Proceso de Venta">
                                            Proceso de venta <div style="float:right"> <?php echo $procesom->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-danger progress-small"><div class="bar" style="width: 80%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Mexicali&tp=Caida">
                                            Ventas Caidas <div style="float:right"> <?php echo $caidasm->num;?></div></a></div>
                                            <div class="progress progress-info progress-small"><div class="bar" style="width: 52%"></div></div>
<a href="http://hondaoptima.com/ventas/index.php/reporte/ventasmes?finicio=01-01-2014&ffin=31-01-2014&ciudad=Mexicali&vendedor=no">
Ventas del Mes <div style="float:right"> <?php echo $ventasMesm;?></div>
</a>
                                        </div>
                                        <div class="grid_item grid_space">
                                            <h2 class="heading_a">Ensenada</h2>
                                            <div class="progress progress-info progress-small"><div class="bar" style="width: 20%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Ensenada&tp=Prospecto">
                                            Prospectos <div style="float:right"> <?php echo $prospectose->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-success progress-small"><div class="bar" style="width: 40%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Ensenada&tp=Caliente">
                                            Calientes <div style="float:right"> <?php echo $calientese->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-warning progress-small"><div class="bar" style="width: 60%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Ensenada&tp=Proceso de Venta">
                                            Proceso de venta  <div style="float:right"> <?php echo $procesoe->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-danger progress-small"><div class="bar" style="width: 80%"></div></div>
                                            <div class="help-block">
                                             <a href="<?php echo base_url();?>index.php/contacto/session/?cd=Ensenada&tp=Caida">
                                            Ventas Caidas <div style="float:right"> <?php echo $caidase->num;?></div>
                                            </a>
                                            </div>
                                            <div class="progress progress-info progress-small"><div class="bar" style="width: 52%"></div></div>
<a href="http://hondaoptima.com/ventas/index.php/reporte/ventasmes?finicio=01-01-2014&ffin=31-01-2014&ciudad=Ensenada&vendedor=no">
Ventas del Mes <div style="float:right"> <?php echo $ventasMese;?></div>
</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>