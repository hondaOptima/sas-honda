<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                   <li><a href="<?php echo base_url();?>index.php/reporte/ventas/">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->

                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Filtros</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
<?php if(empty($_GET['cid'])){ $cid='0';}else{$cid=$_GET['cid'];}?>
<?php if(empty($_GET['statuscon'])){ $statuscon='';}else{$statuscon=$_GET['statuscon'];}?><br>
<?php if(empty($_GET['fuente'])){ $fuente='';}else{$fuente=$_GET['fuente'];}?>
<?php if(empty($_GET['publicidad'])){ $publicidad='';}else{$publicidad=$_GET['publicidad'];}?>
<?php if(empty($_GET['tipo'])){ $tipo='';}else{$tipo=$_GET['tipo'];}?>
<?php if(empty($_GET['anios'])){ $anios='';}else{$anios=$_GET['anios'];}?>
<?php if(empty($_GET['modelo'])){ $modelo='';}else{$modelo=$_GET['modelo'];}?>

<?php if(empty($_GET['finicio'])){ $finicio=date('d').'-'.date('m').'-'.date('Y');}else{$finicio=$_GET['finicio'];}?>
<?php if(empty($_GET['ffin'])){ $ffin=date('d').'-'.date('m').'-'.date('Y');}else{$ffin=$_GET['ffin'];}?>


  <form name="form1" method="get" action="<?php echo base_url();?>index.php/reporte/contacto/">

                                        <div class="row-fluid">
                                          <div class="span3">
                                                    <h4 class="heading_b">Ciudad</h4>
                               
                                                    <?php
$options = array(
                  'Tijuana'  => 'Tijuana',
                  'Mexicali'    => 'Mexicali',
                  'Ensenada'   => 'Ensenada',
				  '0'   => 'Todas las ciudades',
                );
echo form_dropdown('cid', $options, $cid );
                                                    ?>
                                                </div>
                                        
                                                <div class="span3">
                                                    <h4 class="heading_b">Status Contacto</h4>
                                                 
                                                    <?php
 $options = array(
                  'Prospecto'  => 'Prospecto',
                  'Caliente'    => 'Caliente',
                  'Proceso de Venta'   => 'Proceso de Venta',
				  'Venta Caida'   => 'Venta Caida',
				  ''   => 'Todos',
                );
echo form_dropdown('statuscon', $options,$statuscon );
													?>
                                                </div>
                                                 <div class="span3">
                                                    <h4 class="heading_b">Fuente</h4>
<?php                                         
echo form_dropdown('fuente', $ofuente,$fuente );
?>
                                                </div>
                                                 <div class="span3">
                                                    <h4 class="heading_b">Publicidad</h4>
<?php                                         
echo form_dropdown('publicidad', $opublicidad,$publicidad );
?>
                                                </div>
                                                 
                                                </div>
                                                
                                                <div class="row-fluid">
                                                <div class="span3">
                                                    <h4 class="heading_b">Tipo Auto</h4>
<?php

 $options = array(
                  'Nuevo'  => 'Nuevo',
                  'Seminuevo'    => 'Seminuevo',
				  ''   => 'Todos',
                );
echo form_dropdown('tipo', $options,$tipo );
?>
                                                </div>
                                                <div class="span3">
                                                    <h4 class="heading_b">Año Auto</h4>
<?php
echo form_dropdown('anios', $oanios,$anios );
?>
                                                </div>
                                                <div class="span3">
                                                    <h4 class="heading_b">Modelo Auto</h4>
<?php

 $options = array(
                  'ACCORD'  => 'ACCORD',
                  'FIT'    => 'FIT',
				  'CIVIC'    => 'CIVIC',
				  'PILOT'    => 'PILOT',
				  'CRV'    => 'CRV',
				  'ODYSSEY'    => 'ODYSSEY',
				  'CITY'    => 'CITY',
				  'CRZ'    => 'CRZ',
				  'CROSSTOUR'    => 'CROSSTOUR',
				  'RIDGELINE'=>'RIDGELINE',
				  ''   => 'Todos'
				  
                );
echo form_dropdown('modelo', $options,$modelo );
?>                                                    
                                                </div>
                                                
                                                 <div class="span3">
                                                    <h4 class="heading_b">Rango Registro</h4>
                                                   <input class="span4"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $finicio;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>
                                                
                                               
                                                </div>
                                                
                                                <div class="row-fluid">
                                                
                                                <div class="span3">
                                                    <h4 class="heading_b">Rango Registro</h4>
                                                  <input class="span4" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $ffin;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>
                                                
                                               
                                                
                                                
                                                
                                                <div class="span3">
                                                    <h4 class="heading_b">Accion</h4>
                                                    <input type="submit" value="Buscar">
                                                </div>
                                               </div>
                                                </form>
                                                
                                        </div>
                                    
                                    </div></div></div> 
                    
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Reporte Contactos </h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                        <div class="box_a_content no_sp">
                                        
                                 <table id="example"   class="table table-striped table-condensed " data-page-size="15">
                                                <thead>
							  <tr>
                             
                                  
								  <th data-class="expand"  width="5px;">Asesor</th>
                                  <th data-class="expand" width="50px;">Ciudad</th>  
                                  <th data-class="expand" width="50px;">Status</th> 
                                   <th data-class="expand" width="50px;">Fecha Registro</th>
                                  <th data-class="expand" width="150px;" >Nombre</th>
                                  <th data-class="expand" width="150px;" >Apellido</th>
								  <th data-hide="phone" width="40px;"  >Empresa</th>
                                  
                                  <th data-hide="phone" width="40px;">Tel_Casa</th>  
                                  <th  data-hide="phone"width="110px;">Celular</th>
                                  <th data-hide="phone" width="100px;">Correo</th>  
								  <th  data-hide="phone"width="50px;">Tipo_Auto</th>
                                  <th data-class="small" width="65px;">Modelo</th>
                                    <th  data-hide="phone"width="110px;">Año</th>
								  <th  data-hide="phone"width="50px;">Color</th>
                                  <th data-class="small" width="65px;">Publicidad</th>
                                    <th data-class="small" width="65px;">Fuente</th>
							  </tr>
						  </thead>  
                          </table>
                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
<?php $this->load->view('globales/footer'); ?>       
<?php $this->load->view('globales/js'); ?> 


<script type='text/javascript'>

	$(document).ready(function() {
	$('#example').dataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?php echo base_url();?>querys/reportes/reporte_fuente_publicidad.php?cid=<?php echo $cid;?>&statuscon=<?php echo $statuscon;?>&fuente=<?php echo $fuente;?>&publicidad=<?php echo $publicidad;?>&tipo=<?php echo $tipo;?>&anios=<?php echo $anios;?>&modelo=<?php echo $modelo;?>&finicio=<?php echo $finicio;?>&ffin=<?php echo $ffin;?>",
		
					"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row-fluid'ip>",
                    "oTableTools": {
                       "aButtons": [
                            {
								"sExtends":    "print",
                                "sButtonText": 'Imprimir'
							},
                            {
                                "sExtends":    "collection",
                                "sButtonText": 'Guardar <span class="caret" />',
                                "aButtons":    [ "csv", "xls", "pdf" ]
                            }
                        ],
                        "sSwfPath": "http://hondaoptima.com/ventas/js/lib/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $(this).closest('#example_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function(){
                            $(this).addClass('btn-small');
							 $('.ColVis_Button').addClass('btn btn-small').html('Columns');
                        });
                    }
	} );




	
} );
</script>
    </body>
</html>