<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="bateo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="<?php echo base_url();?>index.php/reporte/ventas">Porcentaje de Bateo 2014</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                            <?php

function fuente($array,$idf,$idh)
{
	$i=0;
	foreach($array as $num){	
		list($an,$ms,$di)=explode('-',$num->bit_fecha);
		if($num->hus_IDhuser==$idh and  $num->fue_IDfuente==$idf){
			$i++;
			}
		}
	return $i;
}
function getNumVRango($array,$idf,$idh)
{
	$i=0;
	foreach($array as $num){

		if($num->hus_IDhuser==$idh and  $num->fuente_fue_IDfuente==$idf){
			$i++;
			}
			
		}
	return $i;
}
function getNum($idh,$mes,$array)
{
	$i=0;
	foreach($array as $num){
		list($an,$ms,$di)=explode('-',$num->bit_fecha);
		if($num->hus_IDhuser==$idh and $an=='2014' and $mes==$ms){
			$i++;
			}
		}
	return $i;
}

function getNumVT($idh,$mes,$array)
{
	$i=0;
	foreach($array as $num){
		list($an,$ms,$di)=explode('-',$num->dat_fecha_facturacion);
		if($num->hus_IDhuser==$idh and $an=='2014' and $mes==$ms){
			$i++;
			}
		}
	return $i;
}

function operacion($venta,$piso){

$tot1=($venta / $piso);
$tot=($tot1 * 100);

return $tot;	
	
	}
	
function thlista(){
$i=0; for($i; $i<13; $i++){
echo '<th>Leads</th>
<th>VTS</th>
<th style="border-right:1px solid #CCC;">%</th>';
} 
	}
	
	
function color($col){
	
if($col>=20){return 'label-success';}
if($col>=10 && $col<=20){return 'label-warning';}
if($col<10){return 'label-important';}	
	}	
		
		
function todoRango($VRT2014,$R2014,$VR2014,$IDF,$IDH){
$basev=0;
$basef=0;
if($IDF==3){
$vbmex=getNumVRango($VRT2014,6,$IDH);
$fbmex=fuente($R2014,6,$IDH);
$vbtij=getNumVRango($VRT2014,5,$IDH);
$fbtij=fuente($R2014,5,$IDH);

$basev=$vbmex + $vbtij;
$basef=$fbmex + $fbtij;
	}

$vnfue=getNumVRango($VRT2014,$IDF,$IDH) + $basev;

$lla=fuente($R2014,$IDF,$IDH) + $basef;

$vlla=getNumVRango($VR2014,$IDF,$IDH);

if($vnfue==0 || $vnfue==$vlla){
 $leads=$lla;
 $ventas=$vlla;}
else {
 $resta=$vnfue - $vlla;
 $leads=$lla + $resta;
 $ventas=$vlla + $resta;
 }

if($ventas==0){ 
$por='<span class="label label-important">0.0</span>';}else{
$porlla=operacion($ventas,$leads ); 
$por='<span class="label '.color(number_format($porlla, 1)).'">'.number_format($porlla, 1).'</span>';}	

return $TOD=array('leads'=>$leads,'ventas'=>$ventas,'porcentaje'=>$por);	
	}	
	
	
function bateoCiudad($A2014,$V2014,$VT2014,$MS,$IDH){
	
	$eneTotalV=getNumVT($IDH,$MS,$VT2014);
	$lla=getNum($IDH,$MS,$A2014);
	$vlla=getNum($IDH,$MS,$V2014);
	
 if($eneTotalV==0 || $eneTotalV==$vlla){
 $leads=$lla;
 $ventas=$vlla;}
 else {
 $resta=$eneTotalV - $vlla;
 $leads=$lla + $resta;
 $ventas=$vlla + $resta;
    }
	
if($ventas==0){ 
$por='<span class="label label-important">0.0</span>';}else{
$porene=operacion($ventas,$leads); 
$por='<span class="label '.color(number_format($porene, 1)).'">'.number_format($porene, 1).'</span>';}
return $TOD=array('leads'=>$leads,'ventas'=>$ventas,'porcentaje'=>$por);
	}	

?>     

 <div class="row-fluid">
                               
                                <div class="span4">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Porcentaje de Bateo Tijuana <?php echo date('Y');?></h3>
                                            <h3 class="natij"  style="float:right; margin-right:50px;"></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div id="chart_pie" class="chart_a">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="span4">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Porcentaje de Bateo Mexicali <?php echo date('Y');?></h3>
                                            <h3 class="namex"  style="float:right;  margin-right:50px;"></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div id="chart_piem" class="chart_a"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="span4">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Porcentaje de Bateo Ensenada <?php echo date('Y');?></h3>
                                            <h3 class="naens"  style="float:right;  margin-right:50px;"></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                            <div id="chart_piee" class="chart_a"></div>
                                        </div>
                                    </div>
                                </div>

                                      
                                </div>
                                
                         <br>       
     <div class="row-fluid">
<?php



?>                               
                                <div class="span12">
                                  
                                    <div class="box_a">

                                        <div class="box_a_heading">
<form name="form1" method="get" action="<?php echo base_url();?>index.php/reporte/estadisticasbateo/">
                                            <h3 >Porcentaje de Bateo de por Asesor</h3>
                                            
                                            <h3 class="natij"  style="float:right; margin-right:10px;">
                                             
                                            Del <input class="span4" style="margin-top:3px;"  type="text" placeholder="Fecha de inicio" id="dpStart" name="finicio" value="<?php echo $finicio;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true"> al
<input class="span4" style="margin-top:3px;" type="text" placeholder="Fecha fin" id="dpEnd" name="ffin" value="<?php echo $ffin;?>" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
<input type="submit" class="btn btn-primary" value="Ver" style="margin-top:-6px;">

                                            </h3><h3 style="float:right">Agencia: 
                                            <?php
                                           
echo form_dropdown('ciudad', $cid,$cd,'style="width:146px; background-color:white" id="s29_single" data-rel="chosen"');
											
											?> </h3>
</form>
                                        </div>
                                        
                                      
                                        
<style>
#s2id_s29_single{  margin-top:-2px;}
</style>
                                          
                                        <div class="box_a_content">
<table  style="width:100%; margin-top:-3px"  >
<thead style="background:#E7F6FC;  border-bottom:1px solid #CCC;" >
<tr  text-align="center" style="  border-bottom:1px solid #CCC;">
<th  rowspan="2">Asesor</th>
<th style="border-left:1px solid #CCC; border-right:1px solid #CCC;"  text-align="center" colspan="3">Llamada</th>
<th style="border-right:1px solid #CCC;" text-align="center" colspan="3">Fresh Up</th>
<th style="border-right:1px solid #CCC;"  colspan="3">Internet</th>
<th style="border-right:1px solid #CCC;" colspan="3">Recompra</th>
<th style="border-right:1px solid #CCC;" colspan="3">Prospección</th>
<th style="border-right:1px solid #CCC;" colspan="3">Global</th>
</tr>
<tr>
<th>Leads</th>
<th>Ventas</th>
<th style="border-right:1px solid #CCC;">%</th>
<th>Leads</th>
<th>Ventas</th>
<th style="border-right:1px solid #CCC;">%</th>
<th>Leads</th>
<th>Ventas</th>
<th style="border-right:1px solid #CCC;">%</th>
<th>Leads</th>
<th>Ventas</th>
<th style="border-right:1px solid #CCC;">%</th>
<th>Leads</th>
<th>Ventas</th>
<th style="border-right:1px solid #CCC;">%</th>
<th>Leads</th>
<th>Ventas</th>
<th style="border-right:1px solid #CCC;">%</th>

</tr>
</thead>
<tbody style="text-align:center">
<?php
$c=0;
if($cd=='todas'){$con=array_merge($asesoresTij,$asesoresMex,$asesoresEns);}
if($cd=='Tijuana'){$con=$asesoresTij;}
if($cd=='Mexicali'){$con=$asesoresMex;}
if($cd=='Ensenada'){$con=$asesoresEns;}
 ?>
<?php foreach($con as $todo): ?>
<?php 
$TOLLA=todoRango($VRT2014,$R2014,$VR2014,2,$todo->hus_IDhuser);
$TOFRU=todoRango($VRT2014,$R2014,$VR2014,1,$todo->hus_IDhuser);
$TOINT=todoRango($VRT2014,$R2014,$VR2014,4,$todo->hus_IDhuser);
$TOREC=todoRango($VRT2014,$R2014,$VR2014,3,$todo->hus_IDhuser);
$TOPRO=todoRango($VRT2014,$R2014,$VR2014,7,$todo->hus_IDhuser);

?>
<tr class="hover <?=($c++%2==1) ? 'si' : 'no' ?>" style="border-bottom:1px solid #CCC" >
<td style=" text-align:left; font-weight:bold; width:150px;">
<?php echo ucwords(strtolower(substr($todo->hus_nombre.' '.$todo->hus_apellido,0,23)));?></td>
<td style="border-left:1px solid #CCC; border-right:1px solid #CCC;">
<?php echo $lla=$TOLLA['leads'];?>
</td>

<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $vlla=$TOLLA['ventas'];?>
</td>

<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TOLLA['porcentaje'];?>%
</td>


<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $fre=$TOFRU['leads'];?>
</td>

<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $vfre=$TOFRU['ventas'];?>
</td>

<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TOFRU['porcentaje'];?>%
</td>
<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $int=$TOINT['leads'];?>
</td>

<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $vint=$TOINT['ventas'];?>
</td>

<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TOINT['porcentaje'];?>%
</td>
<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $rec=$TOREC['leads'];?>
</td>

<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $vrec=$TOREC['ventas'];?>
</td>

<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TOREC['porcentaje'];?>%
</td>
<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $pro=$TOPRO['leads'];?>
</td>

<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $vpro=$TOPRO['ventas'];?>
</td>

<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TOPRO['porcentaje'];?>%
</td>
<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $fue=($lla + $fre + $int + $rec + $pro);?>
</td>
<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $vfue=($vlla + $vfre + $vint + $vrec + $vpro);?>
</td>
<td  style="border-right:1px solid #CCC; font-weight:bold">
<?php 
if($vfue==0){ echo '<span class="label label-important">0.0</span>';}else{
$porfue=operacion($vfue,$fue ); 
echo '<span class="label '.color(number_format($porfue, 1)).'">'.number_format($porfue, 1).'</span>';}?> %
</td>
</tr>
<?php endforeach ?>
</tbody>
</table>  
                                        </div>
                                    </div>
                                </div>
                                
                              

                                      
                                </div>                            
<br>
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Porcentaje de Bateo Tijuana 2014</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                            
                                        
<table  style="width:100%"  >
<thead style="background:#E7F6FC;  border-bottom:1px solid #CCC;" >
<tr  text-align="center" style="  border-bottom:1px solid #CCC;">
<th  rowspan="2">Asesor</th>
<th style="border-left:1px solid #CCC; border-right:1px solid #CCC;"  text-align="center" colspan="3">Enero</th>
<th style="border-right:1px solid #CCC;" text-align="center" colspan="3">Febrero</th>
<th style="border-right:1px solid #CCC;"  colspan="3">Marzo</th>
<th style="border-right:1px solid #CCC;" colspan="3">Abril</th>
<th style="border-right:1px solid #CCC;" colspan="3">Mayo</th>
<th style="border-right:1px solid #CCC;" colspan="3">Junio</th>
<th style="border-right:1px solid #CCC;" colspan="3">Julio</th>
<th style="border-right:1px solid #CCC;" colspan="3">Agosto</th>
<th style="border-right:1px solid #CCC;" colspan="3">Septiembre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Octubre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Noviembre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Diciembre</th>
<th  colspan="3">Total</th>

</tr>
<tr>
<?php thlista();?>
</tr>
</thead>
<tbody style="text-align:center">
<?php
$c=0;
 if($asesoresTij): ?>
<?php foreach($asesoresTij as $todo): ?>
<?php 
$TODCD=bateoCiudad($A2014,$V2014,$VT2014,01,$todo->hus_IDhuser);
$TODCDFEB=bateoCiudad($A2014,$V2014,$VT2014,02,$todo->hus_IDhuser);
$TODCDMAR=bateoCiudad($A2014,$V2014,$VT2014,03,$todo->hus_IDhuser);
$TODCDABR=bateoCiudad($A2014,$V2014,$VT2014,04,$todo->hus_IDhuser);
$TODCDMAY=bateoCiudad($A2014,$V2014,$VT2014,05,$todo->hus_IDhuser);
$TODCDJUN=bateoCiudad($A2014,$V2014,$VT2014,06,$todo->hus_IDhuser);
$TODCDJUL=bateoCiudad($A2014,$V2014,$VT2014,07,$todo->hus_IDhuser);
$TODCDAGO=bateoCiudad($A2014,$V2014,$VT2014,08,$todo->hus_IDhuser);
$TODCDSEP=bateoCiudad($A2014,$V2014,$VT2014,09,$todo->hus_IDhuser);
$TODCDOCT=bateoCiudad($A2014,$V2014,$VT2014,10,$todo->hus_IDhuser);
$TODCDNOV=bateoCiudad($A2014,$V2014,$VT2014,11,$todo->hus_IDhuser);
$TODCDDIC=bateoCiudad($A2014,$V2014,$VT2014,12,$todo->hus_IDhuser);
?>
<tr class="hover <?=($c++%2==1) ? 'si' : 'no' ?>" style="border-bottom:1px solid #CCC" >
<td style=" text-align:left; font-weight:bold"><?php echo ucwords(substr($todo->hus_nombre.' '.$todo->hus_apellido,0,18));?></td>

<td style="border-left:1px solid #CCC;"><?php echo $enePiso=$TODCD['leads']; ?></td>
<td><?php echo $eneVenta=$TODCD['ventas']; ?></td>
<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TODCD['porcentaje']; ?> %</td>

<td><?php echo $febPiso=$TODCDFEB['leads'];?></td>
<td><?php echo $febVenta=$TODCDFEB['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDFEB['porcentaje'];?>%</td>

<td><?php echo $marPiso=$TODCDMAR['leads'];?></td>
<td><?php echo $marVenta=$TODCDMAR['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDMAR['porcentaje'];?>%</td>

<td><?php echo $abrPiso=$TODCDABR['leads'];?></td>
<td><?php echo $abrVenta=$TODCDABR['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDABR['porcentaje'];?>%</td>

<td><?php echo $mayPiso=$TODCDMAY['leads'];?></td>
<td><?php echo $mayVenta=$TODCDMAY['ventas'];?></td>
<td style="border-right:1px solid #CCC;"><?php echo $TODCDMAY['porcentaje'];?>%</td>

<td><?php echo $junPiso=$TODCDJUN['leads'];?></td>
<td><?php echo $junVenta=$TODCDJUN['ventas'];?></td>
<td style="border-right:1px solid #CCC;"><?php echo $TODCDJUN['porcentaje'];?>%</td>

<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>

<td><?php echo $totPiso=($enePiso+$febPiso+$marPiso+$abrPiso+$mayPiso+$junPiso);?></td>
<td><?php echo $totVenta=($eneVenta+$febVenta+$marVenta+$abrVenta+$mayVenta+$junVenta);?></td>
<td> 
<?php if($totVenta==0){echo '<span class="label label-important">0.0</span>';}else{
$tijpor=operacion($totVenta,$totPiso); 
echo '<span class="label '.color(number_format($tijpor, 1)).'">'.number_format($tijpor, 1).'</span>';}?>%</td>

</tr>
<?php endforeach ?><?php else: ?>No se encontraron registros.<?php endif ?>	
</tbody>
</table>                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                           
                            
<br>                            
                            
                            
                             <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Porcentaje de Bateo Mexicali 2014</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                   
                                        
<table  style="width:100%"  >
<thead style="background:#E7F6FC;  border-bottom:1px solid #CCC;" >
<tr  text-align="center" style="  border-bottom:1px solid #CCC;">
<th  rowspan="2">Asesor</th>
<th style="border-left:1px solid #CCC; border-right:1px solid #CCC;"  text-align="center" colspan="3">Enero</th>
<th style="border-right:1px solid #CCC;" text-align="center" colspan="3">Febrero</th>
<th style="border-right:1px solid #CCC;"  colspan="3">Marzo</th>
<th style="border-right:1px solid #CCC;" colspan="3">Abril</th>
<th style="border-right:1px solid #CCC;" colspan="3">Mayo</th>
<th style="border-right:1px solid #CCC;" colspan="3">Junio</th>
<th style="border-right:1px solid #CCC;" colspan="3">Julio</th>
<th style="border-right:1px solid #CCC;" colspan="3">Agosto</th>
<th style="border-right:1px solid #CCC;" colspan="3">Septiembre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Octubre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Noviembre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Diciembre</th>
<th  colspan="3">Total</th>

</tr>
<tr>
<?php thlista();?>
</tr>
</thead>
<tbody style="text-align:center">
<?php if($asesoresMex): ?>
<?php foreach($asesoresMex as $todo): ?>
<?php 
$TODCD=bateoCiudad($A2014,$V2014,$VT2014,01,$todo->hus_IDhuser);
$TODCDFEB=bateoCiudad($A2014,$V2014,$VT2014,02,$todo->hus_IDhuser);
$TODCDMAR=bateoCiudad($A2014,$V2014,$VT2014,03,$todo->hus_IDhuser);
$TODCDABR=bateoCiudad($A2014,$V2014,$VT2014,04,$todo->hus_IDhuser);
$TODCDMAY=bateoCiudad($A2014,$V2014,$VT2014,05,$todo->hus_IDhuser);
$TODCDJUN=bateoCiudad($A2014,$V2014,$VT2014,06,$todo->hus_IDhuser);
$TODCDJUL=bateoCiudad($A2014,$V2014,$VT2014,07,$todo->hus_IDhuser);
$TODCDAGO=bateoCiudad($A2014,$V2014,$VT2014,08,$todo->hus_IDhuser);
$TODCDSEP=bateoCiudad($A2014,$V2014,$VT2014,09,$todo->hus_IDhuser);
$TODCDOCT=bateoCiudad($A2014,$V2014,$VT2014,10,$todo->hus_IDhuser);
$TODCDNOV=bateoCiudad($A2014,$V2014,$VT2014,11,$todo->hus_IDhuser);
$TODCDDIC=bateoCiudad($A2014,$V2014,$VT2014,12,$todo->hus_IDhuser);
?>
<tr class="hover <?=($c++%2==1) ? 'si' : 'no' ?>" style="border-bottom:1px solid #CCC" >
<td style=" text-align:left; font-weight:bold"><?php echo ucwords(substr($todo->hus_nombre.' '.$todo->hus_apellido,0,18));?></td>

<td style="border-left:1px solid #CCC;"><?php echo $enePiso=$TODCD['leads']; ?></td>
<td><?php echo $eneVenta=$TODCD['ventas']; ?></td>
<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TODCD['porcentaje']; ?> %</td>

<td><?php echo $febPiso=$TODCDFEB['leads'];?></td>
<td><?php echo $febVenta=$TODCDFEB['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDFEB['porcentaje'];?>%</td>

<td><?php echo $marPiso=$TODCDMAR['leads'];?></td>
<td><?php echo $marVenta=$TODCDMAR['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDMAR['porcentaje'];?>%</td>

<td><?php echo $abrPiso=$TODCDABR['leads'];?></td>
<td><?php echo $abrVenta=$TODCDABR['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDABR['porcentaje'];?>%</td>

<td><?php echo $mayPiso=$TODCDMAY['leads'];?></td>
<td><?php echo $mayVenta=$TODCDMAY['ventas'];?></td>
<td style="border-right:1px solid #CCC;"><?php echo $TODCDMAY['porcentaje'];?>%</td>

<td><?php echo $junPiso=$TODCDJUN['leads'];?></td>
<td><?php echo $junVenta=$TODCDJUN['ventas'];?></td>
<td style="border-right:1px solid #CCC;"><?php echo $TODCDJUN['porcentaje'];?>%</td>

<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>

<td><?php echo $totPiso=($enePiso+$febPiso+$marPiso+$abrPiso+$mayPiso+$junPiso);?></td>
<td><?php echo $totVenta=($eneVenta+$febVenta+$marVenta+$abrVenta+$mayVenta+$junVenta);?></td>
<td>
<?php if($totVenta==0){echo '<span class="label label-important">0.0</span>';}else{
$tijpor=operacion($totVenta,$totPiso); 
echo '<span class="label '.color(number_format($tijpor, 1)).'">'.number_format($tijpor, 1).'</span>';}?>%</td>

</tr>
<?php endforeach ?><?php else: ?>No se encontraron registros.<?php endif ?>	
</tbody>
</table>                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>
                                
                          
   <br>                       
                          
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                    <div class="box_a_heading">
                                            <h3>Porcentaje de Bateo Ensenada 2014</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                   
                                        
<table  style="width:100%"  >
<thead style="background:#E7F6FC;  border-bottom:1px solid #CCC;" >
<tr  text-align="center" style="  border-bottom:1px solid #CCC;">
<th  rowspan="2">Asesor</th>
<th style="border-left:1px solid #CCC; border-right:1px solid #CCC;"  text-align="center" colspan="3">Enero</th>
<th style="border-right:1px solid #CCC;" text-align="center" colspan="3">Febrero</th>
<th style="border-right:1px solid #CCC;"  colspan="3">Marzo</th>
<th style="border-right:1px solid #CCC;" colspan="3">Abril</th>
<th style="border-right:1px solid #CCC;" colspan="3">Mayo</th>
<th style="border-right:1px solid #CCC;" colspan="3">Junio</th>
<th style="border-right:1px solid #CCC;" colspan="3">Julio</th>
<th style="border-right:1px solid #CCC;" colspan="3">Agosto</th>
<th style="border-right:1px solid #CCC;" colspan="3">Septiembre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Octubre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Noviembre</th>
<th style="border-right:1px solid #CCC;" colspan="3">Diciembre</th>
<th  colspan="3">Total</th>

</tr>
<tr>
<?php thlista();?>
</tr>
</thead>
<tbody style="text-align:center">
<?php if($asesoresEns): ?>
<?php foreach($asesoresEns as $todo): ?>
<?php 
$TODCD=bateoCiudad($A2014,$V2014,$VT2014,01,$todo->hus_IDhuser);
$TODCDFEB=bateoCiudad($A2014,$V2014,$VT2014,02,$todo->hus_IDhuser);
$TODCDMAR=bateoCiudad($A2014,$V2014,$VT2014,03,$todo->hus_IDhuser);
$TODCDABR=bateoCiudad($A2014,$V2014,$VT2014,04,$todo->hus_IDhuser);
$TODCDMAY=bateoCiudad($A2014,$V2014,$VT2014,05,$todo->hus_IDhuser);
$TODCDJUN=bateoCiudad($A2014,$V2014,$VT2014,06,$todo->hus_IDhuser);
$TODCDJUL=bateoCiudad($A2014,$V2014,$VT2014,07,$todo->hus_IDhuser);
$TODCDAGO=bateoCiudad($A2014,$V2014,$VT2014,08,$todo->hus_IDhuser);
$TODCDSEP=bateoCiudad($A2014,$V2014,$VT2014,09,$todo->hus_IDhuser);
$TODCDOCT=bateoCiudad($A2014,$V2014,$VT2014,10,$todo->hus_IDhuser);
$TODCDNOV=bateoCiudad($A2014,$V2014,$VT2014,11,$todo->hus_IDhuser);
$TODCDDIC=bateoCiudad($A2014,$V2014,$VT2014,12,$todo->hus_IDhuser);
?>
<tr class="hover <?=($c++%2==1) ? 'si' : 'no' ?>" style="border-bottom:1px solid #CCC" >
<td style=" text-align:left; font-weight:bold"><?php echo ucwords(substr($todo->hus_nombre.' '.$todo->hus_apellido,0,18));?></td>

<td style="border-left:1px solid #CCC;"><?php echo $enePiso=$TODCD['leads']; ?></td>
<td><?php echo $eneVenta=$TODCD['ventas']; ?></td>
<td style="border-right:1px solid #CCC; font-weight:bold">
<?php echo $TODCD['porcentaje']; ?> %</td>

<td><?php echo $febPiso=$TODCDFEB['leads'];?></td>
<td><?php echo $febVenta=$TODCDFEB['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDFEB['porcentaje'];?>%</td>

<td><?php echo $marPiso=$TODCDMAR['leads'];?></td>
<td><?php echo $marVenta=$TODCDMAR['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDMAR['porcentaje'];?>%</td>

<td><?php echo $abrPiso=$TODCDABR['leads'];?></td>
<td><?php echo $abrVenta=$TODCDABR['ventas'];?></td>
<td style="border-right:1px solid #CCC; font-weight:bold"><?php echo $TODCDABR['porcentaje'];?>%</td>

<td><?php echo $mayPiso=$TODCDMAY['leads'];?></td>
<td><?php echo $mayVenta=$TODCDMAY['ventas'];?></td>
<td style="border-right:1px solid #CCC;"><?php echo $TODCDMAY['porcentaje'];?>%</td>

<td><?php echo $junPiso=$TODCDJUN['leads'];?></td>
<td><?php echo $junVenta=$TODCDJUN['ventas'];?></td>
<td style="border-right:1px solid #CCC;"><?php echo $TODCDJUN['porcentaje'];?>%</td>

<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>
<td>0</td>
<td>0</td>
<td style="border-right:1px solid #CCC;">0</td>

<td><?php echo $totPiso=($enePiso+$febPiso+$marPiso+$abrPiso+$mayPiso+$junPiso);?></td>
<td><?php echo $totVenta=($eneVenta+$febVenta+$marVenta+$abrVenta+$mayVenta+$junVenta);?></td>
<td>
<?php if($totVenta==0){echo '<span class="label label-important">0.0</span>';}else{
$tijpor=operacion($totVenta,$totPiso); 
echo '<span class="label '.color(number_format($tijpor, 1)).'">'.number_format($tijpor, 1).'</span>';}?>%</td>
</tr>
<?php endforeach ?><?php else: ?>No se encontraron registros.<?php endif ?>	
</tbody>
</table>                                            
  </div>
                                        
                         
                                        

 </div>
                                    </div>
                                </div>      
                                
                                
                                
                                
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?>
  <?php $this->load->view('reportes/estadisticas/js/ajax');?>

    </body>
</html>