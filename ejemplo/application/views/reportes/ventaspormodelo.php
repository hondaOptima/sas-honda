<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="javascript:history.back()">Dashboard</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                             <div class="row-fluid">
                                
                                 <div class="row-fluid">
                               
                                <div class="span12">
                                  
                                    <div class="box_a">
                                        <div class="box_a_heading">
                                            <h3>Ventas del Mes VS Plan de Ventas  * Modelo * Agencia <?php echo $cd;?></h3>
                                        </div>
                                        <div class="box_a_content cnt_a">
                                              
 <div id="ch_sales" class="chart_a"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                               
                                
                                      
                                </div>
                            </div>
                            
                        </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/jsDashboard'); ?> 
  
  <?php
if($cd=="Tijuana"){$cd=1;}
elseif($cd=="Mexicali"){$cd=2;}
elseif($cd=='Ensenada'){$cd=3;}
$suno=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),1);
$sdos=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),2);
$stre=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),3);
$scua=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),4);
$scin=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),5);
$ssei=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),6);
$ssie=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),7);
$soch=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),8);
$snue=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),9);
$sdie=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),10);
$sonc=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),11);
$sdoc=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),12);
$strc=$this->Reportesmodel->plv_mont_mod('plv_ene',$cd,date('Y'),13);
function vmaset_mod($todo,$modelo){
$num=0;	
foreach($todo as $tod){
 $tod->data_modelo;
if (strpos($tod->data_modelo, ''.$modelo.'')!==false) {$num++;}
	}	
	return $num;
	}
  ?>
  
 <script type='text/javascript'>
   $(document).ready(function(){
    // For horizontal bar charts, x an y values must will be "flipped"
      var elem = $('#ch_sales');

        var build_pass_data = [
                   [1, <?php echo vmaset_mod($todo,'FIT');?>],
                [2, <?php echo vmaset_mod($todo,'CITY');?>],
                [3, <?php echo vmaset_mod($todo,'CIVIC');?>],
				[4, 0],
                [5, 0],
                [6, <?php echo vmaset_mod($todo,'ACCORD');?>],
				[7, 0],
                [8, <?php echo vmaset_mod($todo,'CRV');?>],
                [9, <?php echo vmaset_mod($todo,'PILOT');?>],
				[10, <?php echo vmaset_mod($todo,'ODYSSEY');?>],
                [11, <?php echo vmaset_mod($todo,'RIDGELINE');?>],
                [12, <?php echo vmaset_mod($todo,'CROSSTOUR');?>],
				[13, <?php echo vmaset_mod($todo,'CRZ');?>]
            ];
var build_fail_data = [
                [1, <?php echo $suno[0]->suma;?>],
                [2, <?php echo $sdos[0]->suma;?>],
                [3, <?php echo $stre[0]->suma;?>],
				[4, <?php echo $scua[0]->suma;?>],
                [5, <?php echo $scin[0]->suma;?>],
                [6, <?php echo $ssei[0]->suma;?>],
				[7, <?php echo $ssie[0]->suma;?>],
                [8, <?php echo $soch[0]->suma;?>],
                [9, <?php echo $snue[0]->suma;?>],
				[10, <?php echo $sdie[0]->suma;?>],
                [11, <?php echo $sonc[0]->suma;?>],
                [12, <?php echo $sdoc[0]->suma;?>],
				[13, <?php echo $strc[0]->suma;?>]
				
            ];
 
var ds = [
            {label: "Ventas", data: build_pass_data, bars: {fillColor: "#0094bb"}, color: "#0094bb"},
            {label: "Plande Ventas", data: build_fail_data, bars: {fillColor: "#86ae00"}, color: "#86ae00"}
           ]
                
                var options = {
					
							
                xaxis: {
                    min: 0,
                    max: 14,
                    mode: null,
                    ticks: [
                        [1, "Fit"],
                        [2, "City"],
                        [3, "Civic"],
						[4, "Civic Si"],
                        [5, "Civic IMA"],
                        [6, "Accord 2D"],
						[7, "Accord 4D"],
                        [8, "CR-V"],
                        [9, "Pilot"],
						[10, "Odyssey"],
                        [11, "Ridgeline"],
                        [12, "Crosstour"],
						[13, "CRZ"]
                    ],
                    tickLength: 0,
                    axisLabel: "App",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 14,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, yaxis: {
                    axisLabel: "No of builds",
                    tickDecimals: 0,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 14,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 1
                },
                        tooltip: true,
                        tooltipOpts: {
                            content: "%s - %y",
                            shifts: {
                                x: 20,
                                y: 0
                            },
                            defaultTheme: false
                        }, legend: {
                    labelBoxBorderColor: "none",
                    position: "right"
                }, series: {
                    shadowSize: 1,
					
                    bars: {
                        show: true,
                        barWidth: 0.40,
                        order: 1,
						   showNumbers: true,
            numbers : {
                yAlign: function(y) { return y + 3; },
            },				
						
                    }
                },legend: {labelBoxBorderColor:"white"}
            };
                
                $.plot(elem, ds, options);
				

});
   </script>
   </script>
    </body>
</html>