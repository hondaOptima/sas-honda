<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="repo"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Reportes</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                             <div class="row-fluid">
                                <div class="span12">
                                    <div class="grid_alt clearfix">
                                        <div class="grid_item grid_space">
                                            <h2 class="heading_a">Contactos de Octubre</h2>
                                            <div class="progress progress-info progress-small"><div class="bar" style="width: 20%"></div></div>
                                            <div class="help-block">Prospectos</div>
                                            <div class="progress progress-success progress-small"><div class="bar" style="width: 40%"></div></div>
                                            <div class="help-block">Calientes</div>
                                            <div class="progress progress-warning progress-small"><div class="bar" style="width: 60%"></div></div>
                                            <div class="help-block">Proceso de venta</div>
                                            <div class="progress progress-danger progress-small"><div class="bar" style="width: 80%"></div></div>
                                            <div class="help-block">Ventas Caidas</div>
                                            <div class="progress progress-info progress-small"><div class="bar" style="width: 52%"></div></div>
                                            <div class="help-block">Clientes</div>
                                            <a href="#" class="pull-right btn btn-small">Ver mas</a>
                                          
                                        </div>
                                        <div class="grid_item grid_space stat_boxes">
                                            <h2 class="heading_a">Ventas de Octubre</h2>
                                            <div class="stat_box">
                                                <a href="#" class="pull-right btn btn-small">Ver mas</a>
                                                <div class="peity_canvas peity_bar_up">2,5,3,6,8,5</div>
                                                <h2 class="stat_title">$10 233,23</h2>
                                                <p class="stat_expl">Ventas Tijuana</p>
                                                
                                            </div>
                                            <div class="stat_box">
                                                <a href="#" class="pull-right btn btn-small">Ver mas</a>
                                                <div class="peity_canvas peity_line_up">3,5,9,7,12,8,16</div>
                                                <h2 class="stat_title">13 801</h2>
                                                <p class="stat_expl">Ventas Mexicali</p>
                                            </div>
                                            <div class="stat_box">
                                                <a href="#" class="pull-right btn btn-small">Ver mas</a>
                                                <div class="peity_canvas peity_bar_down">9,6,7,4,6,3</div>
                                                <h2 class="stat_title">$80 173,81</h2>
                                                <p class="stat_expl">Ventas Ensenada</p>
                                            </div>
                                        </div>
                                        <div class="grid_item grid_space">
                                            <h2 class="heading_a">Citas de hoy</h2>
                                            <table id="example" class="table table-bordered">
                                            <thead>
                                            <tr>
                                            <td>Uno</td>
                                            <td>Uno</td>
                                            <td>Uno</td>
                                            </tr>
                                            </thead>
                                                <tbody class="todo">
                                                  
                                                </tbody>
                                            </table>
                                            <a href="#" class="pull-right btn btn-small">Ver mas</a>
                                        </div>
                                        <div class="grid_item grid_space">
                                         <h2 class="heading_a">Plan de ventas Tijuana y Mexicali</h2>
                                            <div id="ch_users" class="chart_b"></div>
                                        </div>
                                        <div class="grid_item grid_space">
                                         <h2 class="heading_a">Plan de Venta Anual</h2>
                                            <div id="ch_pages" class="chart_b"></div>
                                        </div>
                                        <div class="grid_item grid_space">
                                             <h2 class="heading_a">Inventario Consumido por Agencia</h2>
                                            <div id="ch_sales" class="chart_b"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
  <script>
  $(document).ready(function() {
	$('#example').dataTable( {
		"bProcessing": true,
		"sAjaxSource": '<?php echo base_url();?>example.php'
	} );
} )
  </script>
    </body>
</html>