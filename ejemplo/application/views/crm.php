<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Prospecci&oacute;n Honda Optima.</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
            <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="crm"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
          
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                        <div id="content">

                            <!-- breadcrumbs -->
                            <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Inicio</a></li>
                                                                       
                                </ul>
                            </section>

                            <!-- main content -->
                            <div class="row-fluid">
                                <div class="stat_boxes">
                               
                                        <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user " style="color:green;"></i></div>
                                            <h2 class="stat_title">
                                            <a href="<?php echo base_url(); ?>index.php/contacto/">
											<?php echo $prospectos->num;?></a></h2>
                                            <p class="stat_expl">Prospectos</p>
                                        </div>
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:orange;"></i></div>
                                            <h2 class="stat_title">
                                            <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Caliente">
											<?php echo $calientes->num;?></a></h2>
                                            <p class="stat_expl">Calientes</p>
                                        </div>
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:#2cc0f0;"></i></div>
                                            <h2 class="stat_title">
                              <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Proceso%20de%20Venta">
											<?php echo $proceso->num;?></a></h2>
                                            <p class="stat_expl">Proceso a venta</p>
                                        </div>
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" ></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Caida">
											<?php echo $caidas->num;?></a></h2>
                                            <p class="stat_expl">Ventas Ca&iacute;das</p>
                                        </div>
                                        
                                        <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:#365690;"></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Caida">
											<?php echo $caidas->num;?></a></h2>
                                            <p class="stat_expl">Clientes</p>
                                        </div>
                                        
                                         <div class="span2 stat_box"> 
                                            <div class="stat_ico"><i class="elusive-icon-user" style="color:#cccccc;"></i></div>
                                            <h2 class="stat_title">
                                           <a href="<?php echo base_url(); ?>index.php/contacto/<?php echo $tipcon;?>/Caida">
											<?php echo $caidas->num;?></a></h2>
                                            <p class="stat_expl">Todos</p>
                                        </div>
                                    </div>
                                </div>
                            
                     
                    
                   <br>   
                 <div class="row-fluid">
                                <div class="span12">
                                    <div class="box_a">
                                    
                                     
                                    
                                    
   
                                         
                                    
                                        <div class="box_a_heading">
                                            <h3>Prospectos</h3>
                                         
                                        </div>
                                        <div class="box_a_content">
                                            <table id="dt_table_toolsc" class="table table-striped table-condensed">
                                                <thead>
							  <tr>
                               <?php if($_SESSION['nivel']=='Administrador'){ ?>  <th>Asesor</th>   <?php } ?> 
								  <th>Contacto</th>
								  
                                  
                                   <th>Pendientes</th>
                                  <th>Auto de Interes</th>
                                  <th>Ultima Actividad</th>
								  <th>Ver</th>
                                 
                                
							  </tr>
						  </thead>   
						  <tbody>
 <?php foreach($todo_tareas_admin as $todo): ?>
							<tr>
							 <?php if($_SESSION['nivel']=='Administrador'){ ?> 
                            <td ><?php echo $todo->hus_nombre.' '.$todo->hus_apellido?></td>
                            <?php } ?>
                           
                            <td ><?php echo $todo->con_nombre.' '.$todo->con_apellido;?></td>
                           
                           
                            <td >
                              <?php
							
							$num=$this->Homemodel->getTareasP($todo->bit_IDbitacora);
						  echo '<center><span class="label label-important">'.$num->numero.'</span></center>';
							 ?>
							
							</td>
                           <td ><?php echo $todo->con_modelo.' '.$todo->con_ano.' '.$todo->con_color.' ['.$todo->con_tipo ;?>]</td>
<td  >
<?php
$lact=$this->Homemodel->lastActv($todo->bit_IDbitacora);
if($lact) {
	foreach($lact as $data){
$tit=$data->act_titulo;
$desc=$data->act_descripcion;$status=$data->act_status;
$date=$data->act_fecha_inicio;} 

 $descripcion=$tit.' (';
if($data->tipo_actividad_tac_IDtipo_actividad=='3'){
$dat1=$this->Inventariomodel->getBitApartado($data->act_descripcion);
$descripcion.= 'Auto Apartado: ';
$descripcion.= ' Folio :'.$dat1[0]->aau_folio;
$descripcion.=' Monto $:'.$dat1[0]->aau_pago;
$descripcion.= ' '.$dat1[0]->aau_modelo;
$descripcion.= ' '.$dat1[0]->aau_ano;
$descripcion.= ' '.$dat1[0]->aau_color_exterior;
}							
else{$descripcion.= $desc;}list($an,$ms,$di)=explode('-',$date);
$descripcion.=')'.'['.$status.']['.$di.' de '.$arrmes[$ms].' del '.$an.']'; }

else{

$registro=$this->Contactomodel->getRegistro($todo->bit_IDbitacora);
if($registro): ?><?php foreach($registro as $reg): ?> 
<?php   
$idb=$reg->bit_IDbitacora;$date=$reg->bit_fecha; list($an,$ms,$di)=explode('-',$date);?> 
<?php endforeach ?><?php else: ?>No<?php endif ?>	
<?php $descripcion=''.$di.' de '.$arrmes[$ms].' del '.$an.' - Alta en el Sistema';?>
<?php }  ?>
<button class="btn btn-mini pop-over" data-content="<?php echo $descripcion;?>" data-title="<?php echo $todo->con_nombre.' '.$todo->con_apellido;?>" data-placement="left" data-trigger="hover"><?php echo substr($descripcion,0,15);?></button>
</td>
<td>
<a class=" btn btn-mini" href="<?php echo base_url()."index.php/contacto/bitacorabus/$todo->con_IDcontacto/2013-01-01/".date('Y')."-".date('m')."-".date('d')."/pendiente/tactividades";?>">Bitacora</a>
 
</td>                            </tr>


							<?php endforeach ?>


</tbody>

</table>


 </div>
                                    </div>
                                </div>
                            </div>
                 
              
                 
   
                 
                    <!-- jPanel sidebar -->
                    <aside id="jpanel_side" class="jpanel_sidebar"></aside>
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
    </body>
</html>