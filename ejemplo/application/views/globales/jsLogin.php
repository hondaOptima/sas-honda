 <script src="js/jquery.min.js"></script>
        <script src="js/jquery.actual.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/lib/jquery-validation/jquery.validate.js"></script>
        <script>
            $(function() {
                
                //* boxes animation
                var form_wrapper = $('.login_box');
                form_wrapper.css({
                    marginTop : ( - ( form_wrapper.height() / 2) ),
                    top: '50%'
                });
                
                $('.form_toggle').on('click',function(e){
                    var target  = $(this).attr('href'),
                        target_height = $(target).actual('height');
                    $(form_wrapper).css({
                        'height'        : form_wrapper.height()
                    }); 
                    $(form_wrapper.find('form:visible')).fadeOut(400,function(){
                        form_wrapper.stop().animate({
                            height   : target_height,
                            marginTop: ( - (target_height/2) )
                        },500,function(){
                            $(target).fadeIn(400);
                            $(form_wrapper).css({
                                'height'        : ''
                            }); 
                        });
                    });
                    e.preventDefault();
                });
                
                //* validation
                $('#login_form').validate({
                    onkeyup: false,
                    errorClass: 'error',
                    validClass: 'valid',
                    rules: {
                        l_username: { required: true, minlength: 3 },
                        l_password: { required: true, minlength: 3 }
                    },
                    errorPlacement: function(error, element) {
                        $(element).closest('div').append(error);
                    }
                })
            });
        </script>