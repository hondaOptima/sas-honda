  <!-- common scripts -->
      
  
        <!-- jQuery library -->
            <script src="<?php echo base_url();?>js/jquery.min.js"></script>  
            <!-- FullCalendar --> 
           

			
            <script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.min.js"></script>
        <!-- bootstrap framework plugins -->
            <script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="<?php echo base_url();?>js/jquery.actual.min.js"></script>
        <!-- jquery cookie plugin -->
            <script src="<?php echo base_url();?>js/jquery_cookie.js"></script>
        <!-- jquery tooltips -->
            <script src="<?php echo base_url();?>js/lib/powertip/jquery.powertip.min.js"></script>
        <!-- off-canvas sidebar -->
            <script src="<?php echo base_url();?>js/jquery.jpanelmenu.js"></script>
        <!-- mobile navigation -->
            <script src="<?php echo base_url();?>js/tinynav.js"></script>

        <!-- common functions -->
            <script src="<?php echo base_url();?>js/hagal_common.js"></script>
            
                   <script src="<?php echo base_url();?>js/comunes.js"></script>
   <!-- tooltips scripts -->
            <script src="<?php echo base_url();?>js/pages/hagal_tooltips.js"></script>
    <!-- dashboard scripts -->
        <!-- jQuery UI -->
            <script src="<?php echo base_url();?>js/lib/jqueryUI/jquery-ui-1.10.2.custom.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/jqueryUI/jquery.ui.touch-punch.min.js"></script>
  
        <!-- flot (charts) -->
            <script src="<?php echo base_url();?>js/lib/flot/jquery.flot.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/flot/jquery.flot.resize.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/flot/jquery.flot.orderBars.js"></script>
            <script src="<?php echo base_url();?>js/lib/flot/jquery.flot.pie.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/flot/jquery.flot.tooltip.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/flot/jquery.flot.time.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.flot.barnumbers.js"></script>            
            
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jquery.jqplot.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jqplot.barRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jqplot.categoryAxisRenderer.min.js"></script>

<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jqplot.categoryAxisRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jqplot.canvasTextRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/jqplot.canvasOverlay.min.js"></script>

<script src="<?php echo base_url();?>js/jqplot.pointLabels.min.js"></script>

<link rel="stylesheet" type="text/css" hrf="<?php echo base_url();?>js/lib/jquery.jqplot.min.css" />

        <!-- peity (small charts) -->
            <script src="<?php echo base_url();?>js/lib/peity/jquery.peity.min.js"></script>

            <script src="<?php echo base_url();?>js/pages/hagal_dashboard.js"></script>
            <script src="<?php echo base_url();?>js/pages/hagal_dashboard_alternative.js"></script>
            
            
            <!-- responsive table scripts -->
            <script src="<?php echo base_url();?>js/lib/footable/js/footable.js"></script>
            <script src="<?php echo base_url();?>js/lib/footable/js/footable.sortable.js"></script>
            <script src="<?php echo base_url();?>js/lib/footable/js/footable.filter.js"></script>
            <script src="<?php echo base_url();?>js/lib/footable/js/footable.paginate.js"></script>

            <script src="<?php echo base_url();?>js/pages/hagal_responsive_table.js"></script>
            
            
<script src="<?php echo base_url();?>js/lib/fullcalendar/fullcalendar.js"></script>            
              <!-- contact list scripts -->
        <!-- list_js -->
            <script src="<?php echo base_url();?>js/lib/listJS/list.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/listJS/plugins/fuzzy/list.fuzzySearch.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/listJS/plugins/paging/list.paging.min.js"></script>

            <script src="<?php echo base_url();?>js/pages/hagal_contact_list.js"></script>

             <!-- calendar scripts -->
        <!-- jQuery UI -->
            <script src="<?php echo base_url();?>js/lib/jqueryUI/jquery-ui-1.10.2.custom.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/jqueryUI/jquery.ui.touch-punch.min.js"></script>
            <!-- iCheck -->
            <script src="<?php echo base_url();?>js/lib/iCheck/jquery.icheck.min.js"></script>
        <!-- FullCalendar -->
            <script src="<?php echo base_url();?>js/lib/fullcalendar/gcal.js"></script>
            
               <!-- datatables -->
            <script src="<?php echo base_url();?>js/lib/datatables/jquery.dataTables.min.js"></script>
        <!-- datatables column reorder -->
            <script src="<?php echo base_url();?>js/lib/datatables/extras/ColReorder/media/js/ColReorder.min.js"></script>
        <!-- datatables column toggle visibility -->
            <script src="<?php echo base_url();?>js/lib/datatables/extras/ColVis/media/js/ColVis.min.js"></script>
        <!-- datatable table tools -->
            <script src="<?php echo base_url();?>js/lib/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/datatables/extras/TableTools/media/js/ZeroClipboard.js"></script>
        <!-- bootstrap -->
            <script src="<?php echo base_url();?>js/lib/datatables/bootstrap.dataTables.js"></script>

            <script src="<?php echo base_url();?>js/pages/hagal_datatables.js"></script>
            
             <!-- 2col multiselect -->
            <script src="<?php echo base_url();?>js/lib/multi-select/js/jquery.multi-select.min.js"></script>
            <script src="<?php echo base_url();?>js/lib/multi-select/js/jquery.quicksearch.min.js"></script>
            
              <!-- inputmask -->
            <script src="<?php echo base_url();?>js/lib/inputmask/jquery.inputmask.bundle.min.js"></script>
        <!-- bootbox -->
            <script src="<?php echo base_url();?>js/lib/bootbox/bootbox.min.js"></script>
            
            <script src="<?php echo base_url();?>js/pages/hagal_invoices.js"></script>
            
            
                  <script src="<?php echo base_url();?>js/lib/multi-select/js/jquery.quicksearch.min.js"></script>
        <!-- combobox -->
            <script src="<?php echo base_url();?>js/lib/fuelUX/combobox.js"></script>
        <!-- enhanced select -->
            <script src="<?php echo base_url();?>js/lib/select2/select2.min.js"></script>
              <script src="<?php echo base_url();?>js/lib/inputmask/jquery.inputmask.bundle.min.js"></script>
            <script src="<?php echo base_url();?>js/pages/hagal_form_elements.js"></script>
              <!-- datepicker -->
            <script src="<?php echo base_url();?>js/lib/datepicker/js/bootstrap-datepicker.js"></script>
        <!-- timepicker -->
            <script src="<?php echo base_url();?>js/lib/timepicker/js/bootstrap-timepicker.min.js"></script>
        <!-- timepicker -->
            <script src="<?php echo base_url();?>js/lib/colorpicker/js/bootstrap-colorpicker.js"></script>
            <script src="<?php echo base_url();?>js/jquery.form.js"></script>
             <!-- bootstrap switch -->
            <script src="<?php echo base_url();?>js/lib/bootstrap-switch/bootstrapSwitch.js"></script>
            
              <!-- MixItUp -->
            <script src="<?php echo base_url();?>js/jquery.mixitup.min.js"></script>
        <!-- magnific -->
            <script src="<?php echo base_url();?>js/lib/magnific/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url();?>js/pages/hagal_gallery.js"></script>
            
   
           <script>
  	$(document).ready(function() {
		
		$(function() {
        citas_pendientes.cita();
   });
    //* charts
    citas_pendientes= {
        cita: function() {
			$.ajax({
   type: 'POST',
   url: '<?php echo base_url();?>querys/agenda/citasPorConfirmar.php',
   data: '',   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
   success: function(data){
	  $('#cita').html(data)
	   
	   }
			});
			
			}}
	$('#online').live('click',function(){
	$('#ModalUsuarios').modal('show');
	$.post("<?php echo base_url();?>querys/session/listaenLinea.php", function(list) 
{ $("#infoSesion").html(list); }); 	
		});	
	});
	</script>
            
<script>

$(function() {
        cargar_getList.ListUser();
   });
    //* charts
    cargar_getList= {
        ListUser: function() {

$.post("<?php echo base_url();?>querys/session/online.php", function(list) 
{ $("#viewonline").html(list); }); 
	
			}}



setInterval("update()", 10000); // Update every 10 seconds 
function update() 
{ 
$.post("<?php echo base_url();?>querys/session/update.php"); // Sends request to update.php 
} 

setInterval("cargar_getList.ListUser()", 21000) // Get users-online every 10 seconds 
</script>