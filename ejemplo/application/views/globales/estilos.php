<?php header("Content-Type: text/html; charset=utf-8"); ?>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <link rel="icon" type="image/ico" href="favicon.ico">

        
    <!-- common stylesheets-->
        <!-- bootstrap framework css -->
            <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
        <!-- flag icons -->
            <link rel="stylesheet" href="<?php echo base_url();?>img/flags/flags.css">
        <!-- elusive webicons -->
            <link rel="stylesheet" href="<?php echo base_url();?>css/elusive/css/elusive-webfont.css">

    <!-- aditional stylesheets -->
        <!-- fullcalenar -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/fullcalendar/fullcalendar.css">
        <!-- main stylesheet -->
            <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
            
             <link rel="stylesheet" href="<?php echo base_url();?>css/formbubble.css">
              <link rel="stylesheet" href="<?php echo base_url();?>css/viewmore.css">

        <!-- theme -->
            <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" id="theme">
            
            

           <link rel="stylesheet" href="<?php echo base_url();?>js/lib/datatables/css/bootstrap.dataTables.css">
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/datatables/extras/TableTools/media/css/TableTools.css">
            
             <!-- jQuery UI theme -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/jqueryUI/css/Aristo/Aristo.css">
        <!-- 2col multiselect -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/multi-select/css/multi-select.css">
        <!-- enhanced select -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/select2/select2.css">
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/select2/select2-bootstrap.css">

 <!-- iCheck -->
            <link href="<?php echo base_url();?>js/lib/iCheck/skins/square/square.css" rel="stylesheet">
            
              <!-- datepicker -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/datepicker/css/datepicker.css">
        <!-- timepicker -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/timepicker/css/bootstrap-timepicker.css">
        <!-- colorpicker -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/colorpicker/css/colorpicker.css">
               <!-- bootstrap switch -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/bootstrap-switch/bootstrapSwitch.css">
 <!-- responsive table -->
            <link rel="stylesheet" href="<?php echo base_url();?>js/lib/footable/css/hagal_footable.css">
            
           
    <link rel="stylesheet" href="<?php echo base_url();?>js/lib/files/css/datatables.responsive.css"/>
    
     <link rel="stylesheet" href="<?php echo base_url();?>js/lib/magnific/magnific-popup.css">
        <!--[if lt IE 9]><link rel="stylesheet" href="css/ie8.css"><![endif]-->

        <!--[if lt IE 9]>
            <script src="js/ie/html5shiv.js"></script>
            <script src="js/ie/respond.min.js"></script>
            <script src="js/ie/excanvas.min.js"></script>
        <![endif]-->

        <!-- custom fonts -->
            <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300&amp;subset=latin,latin-ext' rel='stylesheet'>
            <link rel="stylesheet" type="text/css" hrf="<?php echo base_url();?>js/lib/jquery.jqplot.min.css" />