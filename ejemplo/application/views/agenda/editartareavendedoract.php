<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Crm - Gestion de Prospectos</title>
          <?php $this->load->view('globales/estilos'); ?>   

    </head>
    <body>
        <!-- main wrapper (without footer) -->
        <div id="main-wrapper">

            <!-- top bar -->
               <?php $this->load->view('globales/topBar'); ?>
            
            <!-- header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                     <?php $data["mn"] ="age"; $this->load->view('globales/menu',$data); ?>   
                           
                        </div>
                    </div>
                </div>
            </header>
            
           
            <section id="main_section">
                <div class="container-fluid">
                    <div id="contentwrapper">
                      <div id="content">




                            <!-- breadcrumbs -->
                        <section id="breadcrumbs">
                                <ul>
                                    <li><a href="#">Agenda</a></li>
                                                                       
                                </ul>
                          </section>

                        <!-- main content --></div>
                        <!-- #content end -->
                    </div>
                    
                                   
                   
                    <!-- jPanel sidebar -->
            <div class="span10">
      
	    

            
            
                                    <div class="box_a">
                                    <div class="box_a_content">
      <div class="box_a_heading">
                                            <h3>Editar datos</h3>
                                        </div>
                                
                                       
                                       <br><br>
 <?php if($todo_actividades): ?><?php foreach($todo_actividades as $tod): ?> 
  <?php
  $ida=$tod->act_IDactividades;
  $tit=$tod->act_titulo;
  $sta=$tod->act_status;
   $fei=$tod->act_fecha_inicio;
   $not=$tod->act_notificar;
  
  $idbit=$tod->bitacora_bit_IDbitacora;
  
	$des=$tod->act_descripcion;
		$tim=$tod->tipo_mensage_tip_IDtipo_mensage;
	
	$hr=$tod->act_hora;
if($hr <10){$hr='0'.$hr;}
$mn=$tod->act_min;
if($mn==0){$mn='00';}
	$hora=$hr.':'.$mn;

  
  ?>        
   <?php endforeach ?><?php else: ?>None<?php endif ?>	 
   
   <?php
   $num=$this->Contactomodel->getIdContacto($idbit);

   ?>
 
 
   <?php echo validation_errors('<div class="alert alert-error">
<button class="close" data-dismiss="alert" type="button">×</button>','</div>'); ?>                            
     
     
   <?php echo form_open('agenda/updatetarea/'.$ida.'/'.$idh.'/'.$type.'','class="form-horizontal"'); ?>
							
                            <fieldset>   
                            
                            
                            <?php 
							$totx=count($todo_contactos);
							if($idh==$_SESSION['id']){?>
                             
                            
                               <div class="control-group">
								<label class="control-label" for="selectErrorf">Categoria</label>
								<div class="controls">
                                
                                <?php
                              
								 echo form_dropdown('categoria', $todo_contactos,$num->con_IDcontacto,'style="width:246px;" id="selectErrorf" data-rel="chosen"');
								?>
                                </div>
							  </div>
                            
                            
                             <?php }else{?>
                             
							 <input type="hidden" name="categoria" value="<?php echo $num->con_IDcontacto;?>">
							 
							 <div class="control-group">
								<label class="control-label" for="selectErrorf">Contacto</label>
								<div class="controls" style="padding-top:4px;">
                                <b>
                                 <?php
   $nombrec=$this->Contactomodel->getIdContactoName($idbit);
echo $nombrec[0]->con_nombre.' '.$nombrec[0]->con_apellido;
   ?></b>
                                
                                </div>
							  </div>
							 
							 
							 
							 <?php }?>
                            
                            
                            
                            
     
 <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php 
										
									$data = array(
              'name'        => 'titulo',
              'id'          => 'titulo',
              'value'       => $tit,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => '',
            );
									  echo form_input($data);?>
									
									?>
                                   
								  </div>
								</div>
							  </div>
         
         
          <div class="control-group">
							  <label class="control-label" for="date01">Fecha Inicio:</label>
							  <div class="controls">
                              
                              
                                 <?php 
								 list($an,$me,$di)=explode('-',$fei);
								 $fcu=$me.'/'.$di.'/'.$an;
								 $datai= array(
              'name'        => 'fei',
              'id'          => 'fei',
			  'class'       => 'input-xlarge datepicker',
              'value'       => $fcu,
              'maxlength'   => '',
              'size'        => '16',
              'style'       => ' width:235px; ',
            );
								 
								 
								  echo form_input($datai);?>
								
							  </div>
							</div>
         
          <div class="control-group">
							 	<label class="control-label" for="selectErrorkk">Hora</label>
								<div class="controls">
                              
                                 <?php
                                $options = array(
                                 '24:00'  => '12:00 am',
                                 '24:30'    => '12:30 am',
								 '01:00'    => '1:00 am',
								 '01:30'  => '1:30 am',
                                 '02:00'    => '2:00 am',
								 '02:30'    => '2:30 am',
								 '03:00'  => '3:00 am',
                                 '03:30'    => '3:30 am',
								 '04:00'    => '4:00 am',
								 '04:30'  => '4:30 am',
								 '05:00'    => '5:00 am',
								 '05:30'  => '5:30 am',
                                 '06:00'    => '6:00 am',
								 '06:30'    => '6:30 am',
								 '07:00'  => '7:00 am',
                                 '07:30'    => '7:30 am',
								 '08:00'    => '8:00 am',
								 '08:30'  => '8:30 am',
                                 '09:00'    => '9:00 am',
								 '09:30'    => '9:30 am',
								  '10:00'  => '10:00 am',
                                 '10:30'    => '10:30 am',
								 '11:00'    => '11:00 am',
								 '11:30'  => '11:30 am',
                                 '12:00'    => '12:00 pm',
								 '12:30'    => '12:30 pm',
								  '13:00'    => '1:00 pm',
								 '13:30'  => '1:30 pm',
                                 '14:00'    => '2:00 pm',
								 '14:30'    => '2:30 pm',
								 '15:00'  => '3:00 pm',
                                 '15:30'    => '3:30 pm',
								 '16:00'    => '4:00 pm',
								 '16:30'  => '4:30 pm',
								 '17:00'    => '5:00 pm',
								 '17:30'  => '5:30 pm',
                                 '18:00'    => '6:00 pm',
								 '18:30'    => '6:30 pm',
								 '19:00'  => '7:00 pm',
                                 '19:30'    => '7:30 pm',
								 '20:00'    => '8:00 pm',
								 '20:30'  => '8:30 pm',
                                 '21:00'    => '9:00 pm',
								 '21:30'    => '9:30 pm',
								  '22:00'  => '10:00 pm',
                                 '22:30'    => '10:30 pm',
								 '23:00'    => '11:00 pm',
								 '23:30'  => '11:30 pm',
								 );
								 echo form_dropdown('hora', $options,$hora,'style="width:246px;" id="selectErrorkk" data-rel="chosen"');
								?>
                              
								
							  </div>
							</div>
         
         
         
         
         
         
        
         
         
         <div class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripcion:</label>
							  <div class="controls">
								
                                  <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => '',
              'value'       => $des,
              'maxlength'   => '',
              'row'        => '',
              'style'       => 'width:247px; height:75px;',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
         
         
         
          <div class="control-group">
								<label class="control-label" for="selectError">Estado</label>
								<div class="controls">
                                
                                 <?php
                                $options = array(
								 ''  => 'Seleccione una opcion',
                                 'pendiente'  => 'Pendiente',
                                 'realizada'    => 'Realizada',
								 'cancelada'    => 'Cancelada',
								 );
								 echo form_dropdown('status', $options,$sta,'style="width:246px;" id="selectError" data-rel="chosen"');
								?>
								  
								</div>
							  </div>
         
         
       
         
         <div class="form-actions">
						  <?php echo anchor("agenda/deletetareascal/".$ida."", ' <div class="btn btn-small btn-danger">Eliminar</div>',array('onClick' => "return confirm('Esta seguro que desea eliminar esta Tarea ?')",'class'=>"")); ?> 
         
 
							 <?php echo form_submit('submit', 'Actualizar Informacion','class="btn btn-primary"'); ?>
							  
							</div>
         
         
         
         
         
         
         
         
         
         
         
 
 
   </fieldset>
						<?php echo form_close(); ?>                 
                                       
                                       
                                        </div>
                                    </div>
                                </div>
                                
                                
              
                     
                   
                    <!-- sticky footer space -->
                    <div id="footer_space"></div>
                </div>
            </section>
        </div>
        <!-- #main-wrapper end -->

        <!-- footer -->
       
  <?php $this->load->view('globales/footer'); ?>       
  <?php $this->load->view('globales/js'); ?> 
  
  
    </body>
</html>