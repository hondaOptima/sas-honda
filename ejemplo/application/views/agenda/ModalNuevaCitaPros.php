<div class="modal hide fade" id="myModalCitaPros">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Nueva Cita - Prosección Hpp <div class="fechtext"></div></h3>
			</div>
			<div class="modal-body">
<?php
 echo form_open('agenda/addtareaPros/','class="form-horizontal todoformPros"'); 
?>   
            <div class="control-group">
  
  <label class="control-label" for="selectErrorkky">Vendedores:</label>
								<div class="controls">
								 
                                  <?php  echo form_dropdown('vendedorPros', $todo_lista,'',' id="s27_single" data-rel="chosen"');?>
                                   
								 
								</div>
                                </div>
                                
                                  <div class="control-group">
  
  <label class="control-label" for="selectErrorkky">Contacto registrado en CRM?</label>
								<div class="controls">
<input type="radio" name="registrado"   class="registrado" value="si">SI
<input type="radio" name="registrado" class="registrado" value="no">NO							 
								 
								</div>
			</div>
  <div class="si" style="display:none">
     <div class="control-group">
								<label class="control-label" for="selectErrorkkf">Contactos:</label>
								<div class="controls">
								 
                                  <div id="listadeProspectos"></div>
                                   
								 
								</div>
							  </div>
  </div>          
 <div class="no" style="display:none"> 
                <div class="control-group">
  
  <label class="control-label" for="selectErrorkky">Nombre Completo:</label>
								<div class="controls">
<input type="text" name="nombre">					 
								 
								</div></div>
			
                <div class="control-group">
  
  <label class="control-label" for="selectErrorkky">Email:</label>
								<div class="controls">
<input type="text" name="email">							 
								 
								</div>
                                </div>
			
                <div class="control-group">
  
  <label class="control-label" for="selectErrorkky">Telefono:</label>
								<div class="controls">
<input type="text" name="telefono">							 
								 
								</div>
                                </div>
                                
</div>                                
                                 <div class="control-group">
								<label class="control-label" for="focusedInput">Titulo:</label>
								<div class="controls">
								 <div class="input-prepend">
									<span class="add-on"></span>
                                    <?php  echo form_input('titulo', set_value('titulo'), 'id="titulo" size="16"');?>
                                   
								  </div>
								</div>
							  </div>
      
         
          <input type="hidden" value="" name="fei">
         
          <div class="control-group">
							 	<label class="control-label" for="selectErrorkk">Hora:</label>
								<div class="controls">
                              
                                 <?php
                                $options = array(
                                 '24:00'    => '12:00 am',
                                 '24:15'    => '12:15 am',
                                 '24:30'    => '12:30 am',
                                 '24:45'    => '12:45 am',
								 '01:00'    => '1:00 am',
								 '01:15'    => '1:15 am',
								 '01:30'    => '1:30 am',
								 '01:45'    => '1:45 am',
                                 '02:00'    => '2:00 am',
                                 '02:15'    => '2:15 am',
								 '02:30'    => '2:30 am',
								 '02:45'    => '2:45 am',
								 '03:00'    => '3:00 am',
								 '03:15'    => '3:15 am',
                                 '03:30'    => '3:30 am',
                                 '03:45'    => '3:45 am',
								 '04:00'    => '4:00 am',
								 '04:15'    => '4:15 am',
								 '04:30'    => '4:30 am',
								 '04:45'    => '4:45 am',
								 '05:00'    => '5:00 am',
								 '05:15'    => '5:15 am',
								 '05:30'    => '5:30 am',
								 '05:45'    => '5:45 am',
                                 '06:00'    => '6:00 am',
                                 '06:15'    => '6:15 am',
								 '06:30'    => '6:30 am',
								 '06:45'    => '6:45 am',
								 '07:00'    => '7:00 am',
								 '07:15'    => '7:15 am',
                                 '07:30'    => '7:30 am',
                                 '07:45'    => '7:45 am',
								 '08:00'    => '8:00 am',
								 '08:15'    => '8:15 am',
								 '08:30'    => '8:30 am',
								 '08:45'    => '8:45 am',
                                 '09:00'    => '9:00 am',
                                 '09:15'    => '9:15 am',
								 '09:30'    => '9:30 am',
								 '09:45'    => '9:45 am',
								 '10:00'    => '10:00 am',
								 '10:15'    => '10:15 am',
                                 '10:30'    => '10:30 am',
                                 '10:45'    => '10:45 am',
								 '11:00'    => '11:00 am',
								 '11:15'    => '11:15 am',
								 '11:30'  => '11:30 am',
								  '11:45'  => '11:45 am',
                                 '12:00'    => '12:00 pm',
                                 '12:15'    => '12:15 pm',
								 '12:30'    => '12:30 pm',
								 '12:45'    => '12:45 pm',
								 '13:00'    => '1:00 pm',
								 '13:15'    => '1:15 pm',
								 '13:30'  => '1:30 pm',
								 '13:45'  => '1:45 pm',
                                 '14:00'    => '2:00 pm',
                                 '14:15'    => '2:15 pm',
								 '14:30'    => '2:30 pm',
								 '14:45'    => '2:45 pm',
								 '15:00'  => '3:00 pm',
								  '15:15'  => '3:15 pm',
                                 '15:30'    => '3:30 pm',
                                 '15:45'    => '3:45 pm',
								 '16:00'    => '4:00 pm',
								 '16:15'    => '4:15 pm',
								 '16:30'  => '4:30 pm',
								 '16:45'  => '4:45 pm',
								 '17:00'    => '5:00 pm',
								 '17:15'    => '5:15 pm',
								 '17:30'  => '5:30 pm',
								  '17:45'  => '5:45 pm',
                                 '18:00'    => '6:00 pm',
                                 '18:15'    => '6:15 pm',
								 '18:30'    => '6:30 pm',
								  '18:45'    => '6:45 pm',
								 '19:00'  => '7:00 pm',
								  '19:15'  => '7:15 pm',
                                 '19:30'    => '7:30 pm',
                                 '19:45'    => '7:45 pm',
								 '20:00'    => '8:00 pm',
								 '20:15'    => '8:15 pm',
								 '20:30'  => '8:30 pm',
								 '20:45'  => '8:45 pm',
                                 '21:00'    => '9:00 pm',
                                 '21:15'    => '9:15 pm',
								 '21:30'    => '9:30 pm',
								 '21:45'    => '9:45 pm',
								 '22:00'  => '10:00 pm',
								 '22:15'  => '10:15 pm',
                                 '22:30'    => '10:30 pm',
                                 '22:45'    => '10:45 pm',
								 '23:00'    => '11:00 pm',
								 '23:15'    => '11:15 pm',
								 '23:30'  => '11:30 pm',
								 );
								 echo form_dropdown('hora', $options,'09:00','style="width:246px;" id="s28_single" data-rel="chosen"');
								?>
                              
								
							  </div>
							</div>
         
         
         
         
         
         
          
         
         
         <div  class="control-group hidden-phone">
							  <label class="control-label" for="desc">Descripcion:</label>
							  <div class="controls">
								
                                 <?php
									
									
									$data = array(
              'name'        => 'desc',
              'id'          => 'desc',
			  'class'          => '',
              'value'       => '',
              'maxlength'   => '',
              'row'        => '',
              'style'       => 'width:247px; height:75px;',
            );
									  echo form_textarea($data);?>
                                
							  </div>
							</div>
         
         
         
       
                              
                              
                              <div class="control-group" id="displaycont2" style="display:show">
								<label class="control-label" for="selectErrorio"></label>
								<div class="controls">
                                
                              <input type="checkbox" name="not">Notificar a Cliente.
                                
								  
								</div>
							  </div>
							  
							  
							 
			
<?php echo form_close(); ?>               
            
</div>
			<div class="modal-footer">
            <div id="loading"></div>
				<a href="#" class="btn" data-dismiss="modal">Cancelar</a>
					 <div  class="btn btn-primary btnSaveProspeccion">Guardar Informacion</div>
			</div><?php echo form_close(); ?>   
		</div>
