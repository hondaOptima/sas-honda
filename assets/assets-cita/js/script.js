
var clock = null;
var digit_to_name = '';
var digits = {};
var positions = [];
var digit_holder = null;
var weekday_names = '';
var weekdays = null;
var mouse_is_inside = false;
var timer = null;

$(document).ready(function(){
 clock = $('#clock'),
		alarm = clock.find('.alarm'),
		ampm = clock.find('.ampm');

	// Map digits to their names (this will be an array)
	digit_to_name = 'zero one two three four five six seven eight nine'.split(' ');

	// This object will hold the digit elements

	// Positions for the hours, minutes, and seconds
	positions = [
		'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
	];

	// Generate the digits with the needed markup,
	// and add them to the clock

	var digit_holder = clock.find('.digits');

	$.each(positions, function(){

		if(this == ':'){
			digit_holder.append('<div class="dots">');
		}
		else{

			var pos = $('<div>');

			for(var i=1; i<8; i++){
				pos.append('<span class="d' + i + '">');
			}

			// Set the digits as key:value pairs in the digits object
			digits[this] = pos;

			// Add the digit elements to the page
			digit_holder.append(pos);
		}

	});

	// Add the weekday names

	weekday_names = '',
		weekday_holder = clock.find('.weekdays');

	//$.each(weekday_names, function(){
		//weekday_holder.append('<span>' + this + '</span>');
	//});

	weekdays = clock.find('.weekdays span');


	// Run a timer every second and update the clock

	$('.display').hover(function(){ 
        mouse_is_inside=true; 
    }, function(){ 
        mouse_is_inside=false; 
    });

    $('#modal-cita').on('click',function(){ 
        if(mouse_is_inside==false) {
            $('.div-back-cita').fadeOut();
            clearInterval(timer);
        }
        digits.h1.attr('class', digit_to_name['0']);
		digits.h2.attr('class', digit_to_name['0']);
		digits.m1.attr('class', digit_to_name['0']);
		digits.m2.attr('class', digit_to_name['0']);
		digits.s1.attr('class', digit_to_name['0']);
		digits.s2.attr('class', digit_to_name['0']);
    });

	// Switch the theme

	$('a.button').click(function(){
		clock.toggleClass('light dark');
	});
});
	// Cache some selectors



function update_time($timein, elem){

    var status = $(elem).attr('status');
    
    
		// Use moment.js to output the current time as a string
		// hh is for the hours in 12-hour format,
		// mm - minutes, ss-seconds (all with leading zeroes),
		// d is for day of week and A is for AM/PM
    var d = new Date($timein);
    var hh = d.getHours();
    var nowtime = null;
    var stopin = null;
    
    
		if($timein == ''){
			var now = '000000';
		}
		else{
            
            if($timein.toString().length >6){
                $timein = $timein.replace(/:/g,"");
                var section = ($timein.length-6);
                $timein = $timein.substr(section,6);
                
            }
            
            if(status == '1'){
                stopin = $(elem).attr('stop');
                if(stopin.toString().length >6){
                    stopin = stopin.replace(/:/g,"");
                    var section = (stopin.length-6);
                    stopin = stopin.substr(section,6);

                }
                nowtime = stopin;
            }
            else{
                nowtime = moment().format("HHmmss");
            }
            var hin = (($timein[0]+$timein[1])*60)*60;
            var min = ($timein[2]+$timein[3])*60;
            var sin = ($timein[4]+$timein[5])*1;
            
            var totalin = hin+min+sin;
            
            var hn = ((nowtime[0]+nowtime[1])*60)*60;
            var mn = (nowtime[2]+nowtime[3])*60;
            var sn = (nowtime[4]+nowtime[5])*1;
            
			var nowTemp = hn+mn+sn;
            
            var resta = nowTemp-totalin;
            
            var horaAMinuto = parseFloat(resta/3600);
            console.log(horaAMinuto);
            var intHora = parseInt( horaAMinuto );
            if(intHora.toString().length==1){intHora='0'+intHora.toString()}
            console.log(intHora);
            var restarHora = parseFloat((horaAMinuto*60)-(intHora*60));
            console.log(restarHora);
            var intMin = parseInt( restarHora );
            console.log(intMin);
            var Seg = parseFloat((restarHora*60)-(intMin*60));
            console.log(Seg);
            var intSeg = parseInt(Seg);
            if(Math.round(Seg)>intSeg){intSeg=intSeg+1;}
            if(intSeg.toString().length==1){intSeg='0'+intSeg.toString();}
            console.log(intSeg);
            var newmin = '';
            if(intMin.toString().length == 1){
                newmin = '0'+intMin.toString();
            }else{
                newmin = intMin.toString();
            }
            
            var now = intHora.toString()+newmin+intSeg.toString();
            console.log(now);
            
            for(var i = now.toString().length;i < 6; i++){
                now = '0'+now;
            }
		}

		digits.h1.attr('class', digit_to_name[now[0]]);
		digits.h2.attr('class', digit_to_name[now[1]]);
		digits.m1.attr('class', digit_to_name[now[2]]);
		digits.m2.attr('class', digit_to_name[now[3]]);
		digits.s1.attr('class', digit_to_name[now[4]]);
		digits.s2.attr('class', digit_to_name[now[5]]);

		$('#modal-cita').fadeIn();

		// The library returns Sunday as the first day of the week.
		// Stupid, I know. Lets shift all the days one position down, 
		// and make Sunday last

		var dow = now[6];
		dow--;
		
		// Sunday!
		if(dow < 0){
			// Make it last
			dow = 6;
		}

		// Mark the active day of the week
		weekdays.removeClass('active').eq(dow).addClass('active');

		// Set the am/pm text:
		ampm.text('');

		// Schedule this function to be run again in 1 sec
		timer = setTimeout(update_time, 1000, $timein, elem);

	};
