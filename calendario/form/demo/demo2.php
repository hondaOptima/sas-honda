<?php
header('Content-Type: text/html; charset=UTF-8'); 
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
		<link href="../src/bootstrap-wizard.css" rel="stylesheet" />
		<link href="chosen/chosen.css" rel="stylesheet" />
		<style type="text/css">
			.wizard-modal p {
				margin: 0 0 10px;
				padding: 0;
			}

			#wizard-ns-detail-servers, .wizard-additional-servers {
				font-size: 12px;
				margin-top: 10px;
				margin-left: 15px;
			}
			#wizard-ns-detail-servers > li, .wizard-additional-servers li {
				line-height: 20px;
				list-style-type: none;
			}
			#wizard-ns-detail-servers > li > img {
				padding-right: 5px;
			}

			.wizard-modal .chzn-container .chzn-results {
				max-height: 150px;
			}
			.wizard-addl-subsection {
				margin-bottom: 40px;
			}
			.create-server-agent-key {
				margin-left: 15px; 
				width: 90%;
			}
		</style>
     
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv-3.7.0.js"></script>
		<script src="js/respond-1.3.0.min.js"></script>
		<![endif]-->
	</head>
	<body style="padding: 30px;" class="modal-open">

		<div id="open-wizard">ok</div>
		<!--<img src="bac.jpg" id="open-wizard" >-->

		<div class="wizard" id="satellite-wizard" data-title="">

			<!-- Step 1 Name & FQDN -->
			<div class="wizard-card wizard-card-overlay" data-cardname="name">
				<h3>Datos del Servicio</h3>

				<div class="wizard-input-section" style="margin-top:-65px; font-size:16px;">
           
           Bienvenido!
           <br><br>
           Ingresa los datos solicitado para consultar el calendario de disponibilidad del taller seleccionado.
            <br><br>
           
                <b>Taller de Servicio(*):</b>
					<br><span style="margin-top:30px;">
<input type="radio"  id="ip"  name="ip" />Ensenada
<input type="radio" style="margin-left:70px;"  id="ip" name="ip" />Tijuana
<input type="radio"  id="ip" name="ip" style="margin-left:70px;" />Mexicali
        </span><br><br>
           
                <b>Servicio requerido(*):</b>					<select name="services" data-placeholder="Service List" style="width:350px;" class="chzn-select create-server-service-list form-control" >

						<option value="Ensenada">10 MIL KM.</option>
                        <option value="Ensenada">Tijuana</option>
                        <option value="Ensenada">Mexicali</option>
                        </select>
                        
                        <br><br>
           
               <b>Modelo del Vehiculo(*):</b>					<select name="services" data-placeholder="Service List" style="width:350px;" class="chzn-select create-server-service-list form-control" >

						<option value="Ensenada">Accord V6</option>
                        <option value="Ensenada">Tijuana</option>
                        <option value="Ensenada">Mexicali</option>
                        </select>
                        
                        <br><br>
           <p>
					<b>Año del Vehículo(*):</b>
					</p>

					<select name="services" data-placeholder="Service List" style="width:350px;" class="chzn-select create-server-service-list form-control" >

						<option value="Ensenada">2014</option>
                        <option value="Ensenada">Tijuana</option>
                        <option value="Ensenada">Mexicali</option>
                        </select>
                       
                       
					
                    
                    
                    
                    </div>
                    
                    
					
				
			</div>

			<div class="wizard-card " data-cardname="group" style="width:100%">
				<h3>Calendario de Citas</h3>

				<div class="wizard-input-section" style="margin-top:-65px;">
               
						<div id='calendar'></div>
                        <h2>Haga click en la hora deseada</h2>
                      <div style=" margin-top:4px; height:150px; border:1px solid #CCC">
                     
                      <div style="font-size:12px;">
                      <span style="background-color:#79b532; border:1px solid #549712; margin:3px 11px 4px 4px; color:white;">
                      <input type="radio" style="cursor:pointer" name="hora">08:00 a.m.
                      </span>
                      <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">08:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                       <input type="radio" style="cursor:pointer" name="hora">08:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">09:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 0px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">09:00 a.m.
                      </span>
                      </div>
                      
                      <div style="font-size:12px;">
                      <span style="background-color:#79b532; border:1px solid #549712; margin:3px 11px 4px 4px; color:white">
                      <input type="radio" style="cursor:pointer" name="hora">09:00 a.m.
                      </span>
                      <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">10:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                       <input type="radio" style="cursor:pointer" name="hora">10:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">10:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 0px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">11:00 a.m.
                      </span>
                      </div>
                      
                      <div style="font-size:12px;">
                      <span style="background-color:#79b532; border:1px solid #549712; margin:3px 11px 4px 4px; color:white">
                      <input type="radio" style="cursor:pointer" name="hora">11:00 a.m.
                      </span>
                      <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">11:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                       <input type="radio" style="cursor:pointer" name="hora">12:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">12:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 0px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">12:00 p.m.
                      </span>
                      </div>
                      
                      <div style="font-size:12px;">
                      <span style="background-color:#79b532; border:1px solid #549712; margin:3px 11px 4px 4px; color:white">
                      <input type="radio" style="cursor:pointer" name="hora">13:00 p.m.
                      </span>
                      <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">13:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                       <input type="radio" style="cursor:pointer" name="hora">13:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">14:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 0px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">14:00 p.m.
                      </span>
                      </div>
                      <div style="font-size:12px;">
                      <span style="background-color:#79b532; border:1px solid #549712; margin:3px 11px 4px 4px; color:white">
                      <input type="radio" style="cursor:pointer" name="hora">14:00 a.m.
                      </span>
                      <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">15:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                       <input type="radio" style="cursor:pointer" name="hora">15:00 p.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 11px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">15:00 a.m.
                      </span>
                        <span style="background-color:#79b532; border:1px solid #549712; margin:1px 0px 4px 1px; color:white">
                        <input type="radio" style="cursor:pointer" name="hora">16:00 a.m.
                      </span>
                      </div>
                      
                     
                      
                     
                      </div>
				</div>
			</div>

			<div class="wizard-card wizard-card-overlay" data-cardname="services">
				<h3>Datos del Cliente</h3>
               
               <div class="wizard-input-section" style="margin-top:-65px; font-size:13px;">
               Datos de Cliente:<br><br>
                <div class="form-group">
						<div class="col-sm-6">
                        <b>Nombre(*):</b>
							<input type="text" class="form-control" id="ip" name="ip" placeholder="Nombre" data-serialize="1" />
						</div>
                        <div class="col-sm-6">
                        <b>Apellido(*):</b>
							<input type="text" class="form-control" id="ip" name="ip" placeholder="Apellido" data-serialize="1" />
						</div>
                        
                        <div class="col-sm-6" style="margin-top:15px;">
                          <b>Tel&eacute;fono(*):</b>
							<input type="text" class="form-control" id="ip" name="ip" placeholder="Tel&eacute;fono" data-serialize="1" />
						</div>
                        <div class="col-sm-6" style="margin-top:15px;">
                          <b>Email(*):</b>
							<input type="text" class="form-control" id="ip" name="ip" placeholder="Email" data-serialize="1" />
						</div>
                        
                        </hr>

 <div class="col-sm-6" style="margin-top:15px;">
                          <b>Asesor de Servicio(*):</b>
							<select name="services" data-placeholder="Service List" style="width:200px;" class="chzn-select create-server-service-list form-control" >

						<option value="Ensenada">Fernando Contreras</option>
                        <option value="Ensenada">Tijuana</option>
                        <option value="Ensenada">Mexicali</option>
                        </select>
						</div>
                       
                        
                        
                             <div class="col-sm-6" style="margin-top:15px;">
                          <b>Tipo de Recordatorio:</b>
							<select name="services" data-placeholder="Service List" style="width:200px;" class="chzn-select create-server-service-list form-control" >

						<option value="Ensenada">SMS</option>
                        <option value="Ensenada">Tijuana</option>
                        <option value="Ensenada">Mexicali</option>
                        </select>
						</div>
                        
                         <div class="col-sm-6" style="margin-top:15px;">
                          <b>Serivicio de Valet?:</b>
							<input type="checkbox" id="ip" name="ip"   />
						</div>  
                        
                        <div class="col-sm-6" style="margin-top:15px;">
                          <b>Comentario:</b>
							<textarea class="form-control" id="ip" name="ip" placeholder="Email" data-serialize="1" /></textarea>
						</div>
                        
                                              
					</div>
                

				<div class="alert hide">
					It's recommended that you select at least one
					service, like ping.
				</div>
                
                </div>

				<div class="wizard-input-section">
				
				</div>
			</div>

		


				<div class="wizard-input-section">
					<p>You will be given a server key after you install the Agent
						on <strong class="create-server-name"></strong>.
						If you know your server key now, please enter it
						below.</p>

					<div class="form-group">
						<input type="text" class="create-server-agent-key form-control" placeholder="Server key (optional)" data-validate="">
					</div>
				</div>


				<div class="wizard-error">
					<div class="alert alert-error">
						<strong>There was a problem</strong> with your submission.
						Please correct the errors and re-submit.
					</div>
				</div>
	
				<div class="wizard-failure">
					<div class="alert alert-error">
						<strong>There was a problem</strong> submitting the form.
						Please try again in a minute.
					</div>
				</div>
	
				<div class="wizard-success wizard-card-overlay">
					<div class="alert alert-success">
						<span class="create-server-name"></span>Cita reservada <strong>Exitosamente.</strong><br>
                           Una confirmación de la cita fue enviada al correo electrónico proporcionado.<br>
                        <strong>Gracias !.</strong>
                        
					</div>
	
					<a class="btn btn-default create-another-server">Crear una nueva cita</a>
					<span style="padding:0 10px">o</span>
					<a class="btn btn-success im-done">Salir</a>
				</div>
			</div>
		</div>
        
        
        

		<script src="js/jquery-2.0.3.min.js" type="text/javascript"></script>
		<script src="chosen/chosen.jquery.js"></script>
		<script src="js/bootstrap.min.js" type="text/javascript"></script>
		<script src="js/prettify.js" type="text/javascript"></script>
		<script src="../src/bootstrap-wizard.js" type="text/javascript"></script>
        
		<script type="text/javascript">
			$(document).ready(function() {
				$.fn.wizard.logging = false;
				var wizard = $('#satellite-wizard').wizard({
					keyboard : false,
					contentHeight : 500,
					contentWidth : 700,
					backdrop: 'static'
				});
				
				//wizard.show();
				//jQuery('#satellite-wizard').wizard({show:true});
				
				$(".chzn-select").chosen();

				$('#fqdn').on('input', function() {
					if ($(this).val().length != 0) {
						$('#ip').val('').attr('disabled', 'disabled');
						$('#fqdn, #ip').parents('.form-group').removeClass('has-error has-success');
					} else {
						$('#ip').val('').removeAttr('disabled');
					}
				});

				var pattern = /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/;
				x = 46;

				$('#ip').on('input', function() {
					if ($(this).val().length != 0) {
						$('#fqdn').val('').attr('disabled', 'disabled');
					} else {
						$('#fqdn').val('').removeAttr('disabled');
					}
				}).keypress(function(e) {
					if (e.which != 8 && e.which != 0 && e.which != x && (e.which < 48 || e.which > 57)) {
						console.log(e.which);
						return false;
					}
				}).keyup(function() {
					var $this = $(this);
					if (!pattern.test($this.val())) {
						//$('#validate_ip').text('Not Valid IP');
						console.log('Not Valid IP');
						$this.parents('.form-group').removeClass('has-error has-success').addClass('has-error');
						while ($this.val().indexOf("..") !== -1) {
							$this.val($this.val().replace('..', '.'));
						}
						x = 46;
					} else {
						x = 0;
						var lastChar = $this.val().substr($this.val().length - 1);
						if (lastChar == '.') {
							$this.val($this.val().slice(0, -1));
						}
						var ip = $this.val().split('.');
						if (ip.length == 4) {
							//$('#validate_ip').text('Valid IP');
							console.log('Valid IP');
							$this.parents('.form-group').removeClass('has-error').addClass('has-success');
						}
					}
				});

				wizard.on('closed', function() {
					wizard.reset();
				});

				wizard.on("reset", function() {
					wizard.modal.find(':input').val('').removeAttr('disabled');
					wizard.modal.find('.form-group').removeClass('has-error').removeClass('has-succes');
					wizard.modal.find('#fqdn').data('is-valid', 0).data('lookup', 0);
				});

				wizard.on("submit", function(wizard) {
					var submit = {
						"hostname": $("#new-server-fqdn").val()
					};
					
					this.log('seralize()');
					this.log(this.serialize());
					this.log('serializeArray()');
					this.log(this.serializeArray());
			
					setTimeout(function() {
						wizard.trigger("success");
						wizard.hideButtons();
						wizard._submitting = false;
						wizard.showSubmitCard("success");
						wizard.updateProgressBar(0);
					}, 2000);
				});
				
				wizard.el.find(".wizard-success .im-done").click(function() {
					wizard.hide();
					setTimeout(function() {
						wizard.reset();	
					}, 250);
					
				});
			
				wizard.el.find(".wizard-success .create-another-server").click(function() {
					wizard.reset();
				});
			
				$(".wizard-group-list").click(function() {
					alert("Disabled for demo.");
				});

				$('#open-wizard').click(function(e) {
					e.preventDefault();
					wizard.show();
				});
				
				$('.wizard-title').html('<img height="30" src="http://webmarketingnetworks.com/sas/assets/img/logo.png"> <div style="float:right"> Sistema de Registro para Citas de Servicio</div>');
			});

			function validateServerLabel(el) {
				var name = el.val();
				var retValue = {};

				if (name == "") {
					retValue.status = false;
					retValue.msg = "Please enter a label";
				} else {
					retValue.status = true;
				}

				return retValue;
			};

			function validateFQDN(el) {
				var $this = $(el);
				var retValue = {};

				if ($this.is(':disabled')) {
					// FQDN Disabled
					retValue.status = true;
				} else {
					if ($this.data('lookup') === 0) {
						retValue.status = false;
						retValue.msg = "Preform lookup first";
					} else {
						if ($this.data('is-valid') === 0) {
							retValue.status = false;
							retValue.msg = "Lookup Failed";
						} else {
							retValue.status = true;
						}
					}
				}

				return retValue;
			};

			function lookup() {
				// Normally a ajax call to the server to preform a lookup
				$('#fqdn').data('lookup', 1);
				$('#fqdn').data('is-valid', 1);
				$('#ip').val('127.0.0.1');
			};
		</script>
        
      
        
 <link href='../../full/fullcalendar.css' rel='stylesheet' />
<link href='../../full/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='../../full/lib/moment.min.js'></script>

<script src='../../full/lib/jquery-ui.custom.min.js'></script>
<script src='../../full/fullcalendar.min.js'></script>
<script>

jQuery(document).ready(function() {
	

	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

         

            $('#calendar').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendar').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
               header: {
				left: 'Selccione una fecha',
				center: 'title',
				right: 'prev,next'
			},
				dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
   dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
				 height: 200,
                editable: false,
               events: [
				{
					title: 'All Day Event',
					start: '2014-06-01'
				},
				{
					title: 'Long Event',
					start: '2014-06-07',
					end: '2014-06-10'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2014-06-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2014-06-16T16:00:00'
				},
				{
					title: 'Meeting',
					start: '2014-06-12T10:30:00',
					end: '2014-06-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2014-06-12T12:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2014-06-13T07:00:00'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2014-06-28'
				}
			]
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			$('.fc-header-left').html('<h3>Haga click en la fecha deseada.</h3>');
			
			
			$('.wizard-next').on('click',function(){
				alert('ok')
				});			
});
</script>

	</body>
</html>
