/*
    // ---------
    //  HTML
    // ---------

    <div id="modalalerta">
        <p></p>
        <p>
            Click para cerrar
        </p>
    </div>
*/

/*
    // ---------
    //  CSS
    // ---------

    #modalalerta {
        text-align: left;
        margin: 0px;
        width: 30%;
        padding: 5px;
        display: none;
        position: absolute;
        background-color: #272822;
        color: #F8F8F8;
        z-index: 999999999;
        border: 1px solid rgba(39, 40, 34, 0.25);
        border-left: 2px solid #DD312F;
        overflow: hidden;
    }
    #modalalerta p {
        font-size: 12pt;
        overflow: hidden;
        display: none;
        width: 100%;
    }
    #modalalerta p:last-child {
        font-size: 10pt;
        color: #75715E;
    }
*/

//hijooo
//madre mia (guilly)
//eu
//pasa tu pass xD

//"inicializa" la modal
//cualquier cambio que se le desee hacer a la modal con respecto a estilo, se modifica aqui
//(favor de tambien cambiar el superior para mejor lectura)
$(document).ready()
{
    $(document.body).prepend('<div id="modalalerta"><p></p><p>Click para cerrar</p></div>', '<style>#modalalerta {text-align: left;margin: 0px;width: 30%;padding: 5px;display: none;position: absolute;background-color: #272822;color: #F8F8F8;z-index: 999999999;border: 1px solid rgba(39, 40, 34, 0.25);border-left: 2px solid #DD312F;overflow: hidden;} #modalalerta p {font-size: 12pt;overflow: hidden;display: none;width: 100%;} #modalalerta p:last-child {font-size: 10pt;color: #75715E;}</style>'); $('#modalalerta').css("margin-top", window.pageYOffset + (window.innerHeight * 0.8) + "px");
}


//variable de ventana modal
$ventanaModal = $('#modalalerta');
//temporizador (necesario para reiniciar el tiempo de la modal)
var modalTimer;
//booleano para saber si esta desplegada o no la modal (para evitar falsos despliegues)
var modalActiva = false;
//altura (en %, siendo 1 el 100%) que tendrá de margen la modal
var ratioMargen = 0.8;

$(window).resize(function() {
    ajustarModal();
});

$(window).scroll(function() {
    ajustarModal();
});


//despliega con el mensaje especifcado
function desplegarModal(mensaje) {
    //activa la modal (si se encuentra activada, no lo hace y solo pone el mensaje)
    if(!modalActiva)
        activarModal(mensaje);
    else
        $ventanaModal.find("p:first").css('display', 'none').animate({ height: "toggle" }, 250).html(mensaje);
    //despues de un tiempo, desactiva la modal
    clearTimeout(modalTimer);
    modalTimer = setTimeout(function() {
        quitarModal();
    }, 2000);
}
//si se hace click a la ventana, se cierra
$ventanaModal.click(function() {
    quitarModal();
});

$ventanaModal.hover(function() {
    //cuando se pone el cursor encima de la modal, detiene el cierre automatico
    clearTimeout(modalTimer);
}, function() {
    //cuando se quita el cursor de la modal, se resume el cierre automatico
    modalTimer = setTimeout(function() {
        quitarModal();
    }, 2000);
});

//esta funcion activa la modal, si se encuentra desactivada
function activarModal(mensaje)
{
    if(!modalActiva)
    {
        modalActiva = true;
        $ventanaModal.animate({ width: "toggle" }, 250);
        $ventanaModal.find("p:last").delay(125).animate({ width: "toggle", height: "toggle" }, 250);
        $ventanaModal.find("p:first").delay(200).animate({ height: "toggle" }, 250, function() { $(this).delay(50); ajustarModal(); }).html(mensaje);
    }
}
//esta funcion es la inversa de activarModal
function quitarModal()
{
    if(modalActiva)
    {
        modalActiva = false;
        $ventanaModal.find("p:last").animate({ width: "toggle", height: "toggle" }, 250);
        $ventanaModal.find("p:first").animate({ width: "toggle", height: "toggle" }, 250, function() { $(this).html(""); });
        $ventanaModal.animate( { width: "toggle" }, 250);
    }
    //a prueba de fallos, limpia el timeout al cerrar la modal
    clearTimeout(modalTimer);
}
//esta funcion ajusta la modal, para que el texto siempre sea visible
function ajustarModal()
{
    //ingresa el margen superior de la modal
    $ventanaModal.css("margin-top", window.pageYOffset + (window.innerHeight * ratioMargen) + "px");
    //si el tamaño de la modal es mayor que el restante, reingresar el tamaño de la modal
    if(window.innerHeight*(1-ratioMargen) < $ventanaModal.height())
        $ventanaModal.css('margin-top', (window.pageYOffset + window.innerHeight - ($ventanaModal.height()*1.25)) +'px' );
}
