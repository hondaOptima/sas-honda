

$doc = $(document);


//este metodo se activa al hacer click a un punto, lo cambia al color seleccionado y actualiza la orden
$doc.on('click','.fa-check-circle, .fa-calendar-check-o', function(){
    $(this).parent().find('i').attr('style','opacity:0.2');  
    $(this).parent().find('i').removeClass('active');  
    $(this).removeClass('opa-i'); 
    $(this).attr('style','opacity:1');   
    $(this).addClass('active');
    upCounter();
    
    var orden = $('#inputOrden').attr('orden');
    var campo = $(this).parent().attr('id');
    var valor = 3;	

    if($(this).hasClass('circle1')) {
        valor = 0;
    }
    if($(this).hasClass('circle2')) {
        valor = 1;
    }
    if($(this).hasClass('circle3')) {
        valor = 2;
    }
    
    $.get('http://hsas.gpoptima.net/multipuntos/setMultipuntos/?campo='+campo+'&orden='+orden+'&valor='+valor, function(data) {
        dataJSON = JSON.parse(data);
        //console.log(dataJSON);
        if(dataJSON.status == 0)
            desplegarModal(dataJSON.message);
        else
            desplegarModal(dataJSON.errorMessage);
    });
});

$(document).on('click','#sendMultipuntos',function(){
    $('#sendMultipuntos').fadeOut(function(){
        $('#enviando').fadeIn();
    });
    var orden = $(this).attr('orden');
    $.get('http://hsas.gpoptima.net/reportes/pdf_multipuntos?orden='+orden,function(data){
        if(data == 'success'){
            alert('FORMATO MULTIPUNTOS ENVIADO');
        }
        $('#enviando').fadeOut(function(){
            $('#sendMultipuntos').fadeIn();
        });
    });
});

//este metodo se activa al cambiar 
$('.frm-llantas').change(function() {

	var valor = $(this).val();
    //si el valor no es vacío
    if(valor !== '')
    {
	    var orden = $('#inputOrden').attr('orden');
	    var campo = $(this).attr('id');

        $.get('http://hsas.gpoptima.net/multipuntos/setMultipuntos/?campo='+campo+'&orden='+orden+'&valor='+valor, function(data) {
            dataJSON = JSON.parse(data);
            //console.log(dataJSON);
            if(dataJSON.status == 0)
                desplegarModal(dataJSON.message);
            else
                desplegarModal(dataJSON.errorMessage);
        });
    }
});


function upCounter(){
    var green = $('.circle1.active').length; 
    var orange = $('.circle2.active').length;
    var red = $('.circle3.active').length;
    $('#circle1').html(green);
    $('#circle2').html(orange);
    $('#circle3').html(red);
}

$('#menu-bars').on('click', function(){
    if($('.div-container-lbl-cont').css('display') != 'none'){
        $('.div-container-lbl-cont').fadeOut();
        $('.div-contador').fadeOut();
    } else {
        $('.div-container-lbl-cont').fadeIn();
        $('.div-contador').fadeIn();
    }
});

$doc.ready(function() {
	upCounter();
});







//valores y funciones para ventana modal

//variable de ventana modal
$ventanaModal = $('#modal');
//temporizador (necesario para reiniciar el tiempo de la modal)
var modalTimer;
//booleano para saber si esta desplegada o no la modal (para evitar falsos despliegues)
var modalActiva = false;
//altura (en %, siendo 1 el 100%) que tendrá de margen la modal
var ratioMargen = 0.8;
//altura de la página (documento) no visible
var offset = 0;

//trigger de modal al moverse en la página
$(window).scroll(function() {
	//offset de la página (para que la modal esté "estatica")
	offset = window.pageYOffset;
	//se acomoda la modal
    $ventanaModal.css("margin-top", ($(window).height()*ratioMargen) + offset + "px");
    //si el tamaño de la modal es mayor que el restante
    if($(window).height()*(1-ratioMargen) < $ventanaModal.height())
    {
    	//se obtiene la diferencia entre ventana y modal
    	var restante = $(window).height() - $ventanaModal.height();
    	//y se asigna en el margen (la ventana aparecerá en el costado inferior izquierdo, pegado a la ventana)
        $ventanaModal.css('margin-top', restante + offset + 'px' );
	}
});

//trigger de modal al ajustar el tamaño de la ventana
$(window).resize(function() {
	//offset de la página (para que la modal esté "estatica")
	offset = window.pageYOffset;
	//se acomoda la modal
    $ventanaModal.css("margin-top", ($(window).height()*ratioMargen) + offset + "px");
    //si el tamaño de la modal es mayor que el restante
    if($(window).height()*(1-ratioMargen) < $ventanaModal.height())
    {
    	//se obtiene la diferencia entre ventana y modal
    	var restante = $(window).height() - $ventanaModal.height();
    	//y se asigna en el margen (la ventana aparecerá en el costado inferior izquierdo, pegado a la ventana)
        $ventanaModal.css('margin-top', restante + offset + 'px' );
	}
});

//despliega con el mensaje especifcado
function desplegarModal(mensaje) {
    //pone el mensaje obtenido
    $ventanaModal.find("p:first").html(mensaje);
    //se acomoda la modal
    $ventanaModal.css("margin-top", ($(window).height()*ratioMargen) + offset + "px");
    //si el tamaño de la modal es mayor que el restante
    if($(window).height()*(1-ratioMargen) < $ventanaModal.height())
    {
    	//se obtiene la diferencia entre ventana y modal
    	var restante = $(window).height() - $ventanaModal.height();
    	//y se asigna en el margen (la ventana aparecerá en el costado inferior izquierdo, pegado a la ventana)
        $ventanaModal.css('margin-top', restante + offset + 'px' );
	}
    //activa la modal (si se encuentra activada, no lo hace)
    if(!modalActiva)
    {
        modalActiva = true;
        $ventanaModal.animate( { width: "toggle" }, 250);
    }
    //despues de un tiempo, desactiva la modal
    clearTimeout(modalTimer);
    modalTimer = setTimeout(function() {
        if(modalActiva)
        {
            modalActiva = false;
            $ventanaModal.animate( { width: "toggle" }, 250);
        }
    }, 3000);
}

//si se hace click a la ventana, se cierra automaticamente
$ventanaModal.click(function() {
    if(modalActiva)
    {
        modalActiva = false;
        $ventanaModal.animate( { width: "toggle" }, 250);
    }
});