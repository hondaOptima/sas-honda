   <?
    $ruta = "calendario/ajax/uploads/";

    @chmod($ruta, 0777);
    
    echo "Abiendo directorio $ruta...<br/>";

    if ($directorio = opendir($ruta)) 
    {
        echo "Leyendo el directorio...<br/>";
        while (false !== ($carpeta = readdir($directorio))) 
        {
            if(!pathinfo($carpeta, PATHINFO_EXTENSION))
            {
                if ($carpeta === '.' || $carpeta === '..') continue;
                
                if($subDirectorio = opendir($ruta.$carpeta))
                {
                    while (false !== ($archivo = readdir($subDirectorio))) 
                    {
                        if ($archivo === '.' || $archivo === '..') continue;
                        
                        $extencion = pathinfo($archivo, PATHINFO_EXTENSION);
                        if($extencion == "jpg")
                        {
                            reducirCalidadImagenJPG($ruta.$carpeta.'/'.$archivo);
                        }
                    }
                    
                    closedir($subDirectorio);
                }
            }
            else   
            {
                $extencion = pathinfo($carpeta, PATHINFO_EXTENSION);
                if($extencion == "jpg")
                {
                    reducirCalidadImagenJPG($ruta . $carpeta);
                }
            }
        }
        echo "Cerrando directorio... <br/>";
        closedir($directorio);
    }


    function reducirCalidadImagenJPG($imagen)
    {
        if(decoct(fileperms($imagen) & 0777) == 777)
        {
            if(filesize($imagen) > 197885)
            {
                $origen = $imagen;
                $imagen_temp = str_replace(".jpg", "_temp", $imagen).".jpg";
                $t = filesize($imagen);

                list($ancho, $alto, $tipo, $attr) = getimagesize($origen);

                $destino_temporal = tempnam("tmp/","tmp");
                redimensionar_jpeg($origen, $destino_temporal, ($ancho/2), ($alto/2), 100);

                $fp = fopen($imagen_temp, "w");
                fputs($fp, fread(fopen($destino_temporal, "r"), filesize($destino_temporal)));
                fclose($fp);

                @chmod($imagen_temp, 0777);

                if(unlink($origen))
                    rename($imagen_temp, $origen);

                clearstatcache();

                echo "$imagen Tamaño de: $t bytes a: ".filesize($imagen)."bytes<br/>";
            }
        }
        else 
        {
            
            if(@chmod($imagen, 0777))
                reducirCalidadImagenJPG($imagen);
            else
                echo "$imagen permisos: ".decoct(fileperms($imagen) & 0777)." se denego la accion<br/>";
        }

    }

    function redimensionar_jpeg($img_original, $img_nueva, $img_nueva_anchura, $img_nueva_altura, $img_nueva_calidad)
    {
        //header('Content-Type: image/jpeg');
        // crear una imagen desde el original
        $img = CargarJpeg($img_original);
        // crear una imagen nueva
        $thumb = imagecreatetruecolor($img_nueva_anchura, $img_nueva_altura);
        // redimensiona la imagen original copiandola en la imagen
        imagecopyresized($thumb, $img, 0, 0, 0, 0, $img_nueva_anchura, $img_nueva_altura, imagesx($img), imagesy($img));
        // guardar la nueva imagen redimensionada donde indicia $img_nueva
        imagejpeg($thumb, $img_nueva, $img_nueva_calidad);
        imagedestroy($img);
    }

    function CargarJpeg($imagen)
    {
        // Intentar abrir
        $im = @imagecreatefromjpeg($imagen);

        // Ver si falló 
        if(!$im)
        {
            // Crear una imagen en blanco 
            $im  = imagecreatetruecolor(150, 30);
            $fondo = imagecolorallocate($im, 255, 255, 255);
            $ct  = imagecolorallocate($im, 0, 0, 0);

            imagefilledrectangle($im, 0, 0, 150, 30, $fondo);

            // Imprimir un mensaje de error 
            imagestring($im, 1, 5, 5, 'Error cargando ' . $imagen, $ct);
        }

        return $im;
    }
?>