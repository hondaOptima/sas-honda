<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-group"></i> <a href="<?php echo base_url();?>usuario/equipos">Equipos</a> <i class="fa fa-angle-right"></i> </li>
    <li> <i class="fa fa-pencil"></i> <a href="">Editar Equipo</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<?php echo $flash_message;?>
<div class="row"> 
  <!-- BEGIN EXAMPLE TABLE PORTLET-->
  
<?php
function ver($array,$id){
	$tot=0;
	foreach($array as $ar){
		if($ar->equ_tecnico==$id){
			$tot=1;
			}
		
		}
	
	
	return $tot;
	
	}
?>
  <div class="col-md-3 "> 
    <!-- BEGIN Portlet PORTLET-->
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-reorder"></i>Editar Equipo </div>
        <div class="actions"> <a href="<?php echo base_url()?>usuario/equipos/" class="btn btn-success btn-sm"><i class="fa  fa-angle-left"></i> Regresar</a> </div>
      </div>
      <div class="portlet-body">
        <div class="scroller" style="height:250px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd"> 
          
          <table class="table table-striped table-bordered table-hover"  >
            <thead>
              <tr style="font-weight:bold">
                <th> <b>Asesor de Ventas</b> </th>
              </tr>
            </thead>
            <tbody>
            <td><?php echo $usuario[0]->sus_name.' '.$usuario[0]->sus_lastName;?></td>
                </tbody>
          </table>
          <div class="respuesta"></div>
          <table class="table table-striped table-bordered table-hover"  >
            <thead>
              <tr style="font-weight:bold">
                <th> </th>
                <th> <b>Tecnicos</b> </th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($tecnicos as $tec){
			if($tec->rol_idRol==4){	
			
			$nu=ver($lista,$tec->sus_idUser);
			?>
            <tr>
            <td><input type="checkbox" class="checkConfirmar" id="<?php echo $usuario[0]->sus_idUser.'-'.$tec->sus_idUser;?>" name="idt" <?php if(1==$nu){echo'checked';}?>></td>
            <td><?php echo $tec->sus_name.' '.$tec->sus_lastName;?></td>
            </tr>
            <?php }} ?>
            
            
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END Portlet PORTLET--> 
  </div>
  
</div>
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('usuario/js/script');?>
<script>
	$(document).ready(function() {

		$('.checkConfirmar').on('click',function(){
$('.respuesta').html('Guardando... !');	

var mcCbxCheck = $(this);
var id=$(this).attr('id');

if(mcCbxCheck.is(':checked')) {
	
 $.post("<?php echo base_url(); ?>usuario/updateEquipo/",{id:id,eve:'add'}, function(data) {
		 $('.respuesta').html('');
		
		 });

}
else{
	
 $.post("<?php echo base_url(); ?>usuario/updateEquipo/",{id:id,eve:'delete'}, function(data) {
		 $('.respuesta').html('');	
		
		 });	
	
	}



});




});
</script>
