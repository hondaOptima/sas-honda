<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='productividad';$this->load->view('globales/menu',$data); 
$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
header("refresh:300; url=$self"); //Refrescamos cada 300 segundos
?>

<form name="form1" method="get" action="<?php echo base_url();?>productividad/">
  <ul class="page-breadcrumb breadcrumb">
    <li> <i class="fa fa-gears"></i> <a href="#">Productividad</a> <i class="fa fa-angle-right"></i> </li>
    <li class="pull-right" style="text-align:right; margin-left:15px;">
      <Select name="asesor" onchange='this.form.submit()'>
        <option value="0" <?php if($asesor==0){echo 'selected="selected"';}?>>Todos</option>
        <?php foreach($usuario as $us) {
				if($us->rol_idRol=='3' && $us->sus_workshop==$taller && $us->sus_isActive==1) {
				?>
        <option value="<?php echo $us->sus_idUser?>" <?php if($asesor==$us->sus_idUser){echo 'selected="selected"';}?>><?php echo $us->sus_name.' '.$us->sus_lastName;?></option>
        <?php }} ?>
      </select>
    </li>
    <li class="pull-right" style="text-align:right; margin-left:15px;">
      <?php if($_SESSION['sfrol']<=2) {?>
      <select name="taller" onchange='this.form.submit()'>
        <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
        <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
        <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>
      </select>
      <?php } ?>
    </li>
    <li class="pull-right" >
      <div  class="input-group input-medium date " id="date-pickerbb" data-date-format="yyyy-mm-dd" style="text-align:right;">
        <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
        Fecha: <?php echo $fecha;?> <i style="cursor:pointer; float:right; margin-left:5px;" class="fa fa-calendar  input-group-btn"></i> </div>
    </li>
  </ul>
</form>

<!-- END PAGE TITLE & BREADCRUMB-->
<?php
    $name=array('Monday'=>'Lun','Tuesday'=>'Mar','Wednesday'=>'Mie','','Thursday'=>'Jue','Friday'=>'Vie','Saturday'=>'Sab');	
	

	function listaOrden($arr,$da){
		$cat=array();	
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da and $ar->ssr_venta=='venta'){
				$cat[]=array('tipo'=>$ar->ssr_tipo,'servicio'=>$ar->set_name,'codigo'=>$ar->set_codigo,'duracion'=>$ar->ser_approximate_duration,'torre'=>$ar->seo_tower,'venta'=>$ar->ssr_venta,'ido'=>$ar->ssr_IDNULL);	
				}
			
			}
		return $cat;
		}
	
	function numeros($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da and $ar->ssr_venta=='venta')$sum++;
			
			}
		return $sum;
		}
		
		
		function buscarsv($arr,$ido){
		$res='no';
		foreach($arr as $ar){
			
			if($ar->sev_id_orden==$ido and $ar->sev_tipo_venta=='venta')$res='si';
			
			}
		return $res;
		}
		
		function publico($arr){
		$sum=0;
		foreach($arr as $ar){
			if($ar->ssr_venta=='venta' || $ar->ssr_venta=='adicional')$sum+=$ar->ser_approximate_duration;
			}
		return $sum;
		}
		
		function publicodia($arr,$da){
		$sum=0;
		foreach($arr as $ar){
if($ar->piz_fecha==$da && ($ar->ssr_venta=='venta' || $ar->ssr_venta=='adicional'))$sum+=$ar->ser_approximate_duration;
			}
		return $sum;
		}
		
		function publicodiasv($arr,$da){
		$sum=0;
		foreach($arr as $ar){
if($ar->piz_fecha==$da && ($ar->sev_tipo_venta=='venta' || $ar->sev_tipo_venta=='adicional'))$sum+=$ar->sev_tiempo;
			}
		return $sum;
		}
	
	function countos($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			if($ar->piz_fecha==$da )$sum++;
			}
		return $sum;
		}
		
		function garantia($os,$day){
		 $num=count($os);
		 $ssumgar=0;
		for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
	$ssumgar++;
		}}
		return $ssumgar;		
			}
		
		
		function horasV($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da )$sum+=$ar->ser_approximate_duration;	
			
			
			}
		return $sum;
		}
		
		function horasVsv($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da && ($ar->sev_tipo_venta=='garantia' ||  $ar->sev_tipo_venta=='garantiai' ||  $ar->sev_tipo_venta=='interna' || $ar->sev_tipo_venta=='venta' || $ar->sev_tipo_venta=='adicional'))$sum+=$ar->sev_tiempo;	
			
			
			}
		return $sum;
		}


function garantias($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da && ($ar->ssr_venta=='garantia' ||  $ar->ssr_venta=='garantiai' ||  $ar->ssr_venta=='interna'))$sum+=$ar->ser_approximate_duration;	
			
			
			}
		return $sum;
		}


	?>
<?php
function ver($array,$id,$fecha){
	$tot=0;
	if($id==0){
		foreach($array as $ar){
		if($ar->eqp_fecha==$fecha){
			$tot++;
			}
		
		}

		}
	
	else{
	foreach($array as $ar){
		if($ar->eqp_fecha==$fecha && $ar->eqp_asesor==$id){
			$tot++;
			}
		
		}
	}
	
	return $tot;
	
	}
$shorasV=0;	
$scounos=0;
$tot=0;	
$stot=0;	
$sgarant=0;	

//echo  count($ossv);
	?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row">
  <?php for($x=0; $x<6; $x++){
					$day=$this->Productividadmodel->dias_semana($x,$inicio);
						
							
						?>
  <div class="col-md-2 col-sm-2">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <a style="color:white" href="<?php echo base_url()?>productividad/editarequipo?fecha=<?php echo $day['dayg'];?>&asesor=<?php echo $asesor;?>&taller=<?php echo $taller;?>"> <i class="fa fa-user"></i>Tecnicos | <?php echo $nu=ver($lista,$asesor,$day['dayg']);?> </a> </div>
      </div>
      <div class="portlet-body" style="padding:0px;">
        <table class="table " width="100%" height="100%" >
          <thead>
            <tr style="font-weight:bold; background-color:black; color:white">
              <th style="border-right:1px solid #ccc; text-align:center"> <b></b> </th>
              <th style="border-right:1px solid #ccc; text-align:center"> <b>
                <?php 
$dname = date('l', strtotime($day['dayg']));
echo $name[$dname];?>
                </b> </th>
              <th colspan="2" style="border-right:1px solid #000; text-align:center"> <b><?php echo $day['days'];?></b> </th>
            </tr>
          </thead>
          <tbody>
            <tr  style="font-weight:bold; font-size:11px; text-align:center; background-color:red; color:yellow;">
              <td style="border-bottom:1px solid #000;border-top:1px solid #000; border-right:1px solid #000;"><b>#</b></td>
              <td style="border-bottom:1px solid #000;border-top:1px solid #000; border-right:1px solid #000;"><b>O.S.</b></td>
              <td style="border-bottom:1px solid #000;border-top:1px solid #000; border-right:1px solid #000;"><b>SERVICIO</b></td>
              <td style="border-bottom:1px solid #000;border-top:1px solid #000; border-right:1px solid #000;"><b>TIEMPO</b></td>
            </tr>
            <?php 
			$numb=numeros($os,$day['dayg']);
							$tnumb=$resta-$numb;
							$sum=0;
							$rr=0;
			
								
							if($numb!=0){
								
								$rr=0;
								$cc=0;
								for($dd=0; $dd< $numb; $dd++){
								$orden=listaOrden($os,$day['dayg']);
								$uu=0;	
								if($orden[$dd]['tipo']=='pro'){$cod=$orden[$dd]['servicio'];}else{$cod=$orden[$dd]['codigo'];}
								$sum+=$orden[$dd]['duracion'];	
								
								$cc=$dd - 1;
								if($dd==0){
									$torre=$orden[$dd]['torre'];
									$rr++;
									}
								elseif($orden[$cc]['torre']==$orden[$dd]['torre']){
								$torre='';
								}else{
									$torre=$orden[$dd]['torre'];
									$rr++;
									}
								
								if($torre==''){}else{	
								?>
            <tr >
              <td  align="center" style="border-bottom:1px solid #000; border-right:1px solid #000;"><?php  echo $rr;?></td>
              <td title="<?php echo $orden[$dd]['torre'];?>" align="center" style="border-bottom:1px solid #000; border-right:1px solid #000;"><?php 
			  echo $torre;?></td>
              <td  style="cursor:help; border-bottom:1px solid #000; border-right:1px solid #000;"  title="<?php echo $orden[$dd]['servicio'];?>"><?php echo mb_strtoupper($cod);?></td>
              <td align="center" style="background-color:red; color:yellow; border-bottom:1px solid #000; border-right:1px solid #000;"><?php echo $orden[$dd]['duracion'];?></td>
            </tr>
            <?php 
			
			//buscar si existe la misma orden con servicios varios
			$buscar=buscarsv($ossv,$orden[$dd]['ido']);
			//si hay mas dibujarlos con la informacion correspondiente
			if($buscar=='si'){
			foreach($ossv as $ossvres){
				if($ossvres->sev_id_orden==$orden[$dd]['ido']){$rr++;
		   echo ' 
		   <tr >
           <td  align="center" style="border-bottom:1px solid #000; border-right:1px solid #000;">'.$rr.'</td>
           <td title="" align="center" style="border-bottom:1px solid #000; border-right:1px solid #000;"></td>
           <td  style="cursor:help; border-bottom:1px solid #000; border-right:1px solid #000;"  title="'.$ossvres->sev_nombre.'">SV</td>
		   <td align="center" 
		   style="background-color:red; color:yellow; border-bottom:1px solid #000; border-right:1px solid #000;">'.$ossvres->sev_tiempo.'           </td>
           </tr>';
		   
				}
				}
			    }
			
			} $uu++;  }
			
			}
			$tnumb=$resta - $rr;
			for($ss=0; $ss<$tnumb; $ss++){
			 ?>
            <tr height="17px;" >
              <td align="center" style="border-bottom:1px solid #000; height:17px; border-right:1px solid #000;">&nbsp;</td>
              <td align="center" style="border-bottom:1px solid #000; border-right:1px solid #000;">&nbsp;</td>
              <td  style="cursor:help; border-bottom:1px solid #000; border-right:1px solid #000;"  title="">&nbsp;</td>
              <td align="center" style="background-color:red; color:yellow; border-bottom:1px solid #000; border-right:1px solid #000;"></td>
            </tr>
            <?php } 
			 $counosgar=garantia($os,$day['dayg']);
			 $publicodia=publicodia($os,$day['dayg']);
			 $publicodiasv=publicodiasv($ossv,$day['dayg']);
			 $counos=countos($cos,$day['dayg']);
			 ?>
            <tr class="odd gradeX" style="font-weight:bold; font-size:11px;">
              <td colspan="3" style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"> HRS. TOTALES </td>
              <td align="center" style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"><?php echo $horasV=horasV($os,$day['dayg']) + horasVsv($ossv,$day['dayg']);?></td>
            </tr>
            <tr class="odd gradeX" style="font-weight:bold; font-size:11px;">
              <td colspan="3"style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"> HRS. PUBLICO </td>
              <td align="center" style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"><?php echo $publicodia + $publicodiasv;?></td>
            </tr>
            <tr class="odd gradeX" style="font-weight:bold; font-size:11px;">
              <td colspan="3"style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"> O.S. PUBLICO </td>
              <td align="center" style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"><?php if($horasV==0){echo $counos=0;}else{echo $counos=$rr;}?></td>
            </tr>
            <tr class="odd gradeX" style="font-weight:bold; font-size:11px;">
              <td colspan="3"style=" background-color:red; color:yellow; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"> HRS. X O.S. PUB. </td>
              <td align="center" style=" background-color:red; color:white; border-left:1px solid #333; border-right:1px solid #333; border-bottom:1px solid #333; text-align:center;"><?php 
$garantias=garantias($os,$day['dayg']);
$sosoToto=$horasV - $garantias;
if($counos) $tot=($sosoToto / $counos);
if($horasV==0){echo 0;}
else{
echo number_format($tot, 2, '.', '');}?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php 
					$shorasV+=$horasV;
					$scounos+=$counos;
					$stot+=$tot;
					$sgarant+=$garantias; } ?>
</div>
<div class="row"> <br>
  <div align="center" style="font-weight:bold; font-size:16px" >VENTA ADICIONAL</div>
  <br>
</div>
<div class="row">
  <?php 
  $contarsv=0;
  $sumsv=0;
 for($x=0; $x<6; $x++){
					$day=$this->Productividadmodel->dias_semana($x,$inicio);
						
	
	?>
  <div class="col-md-2 col-sm-2">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption" style="font-size:11px;"> SERVICIOS NO TABULADOS </div>
      </div>
      <div class="portlet-body" style="padding:0px;">
        <table class="table " >
          <thead>
          </thead>
          <tbody>
            <tr class="odd gradeX" style="font-weight:bold; background-color:black; color:white; font-size:11px; text-align:center">
              <td style="border-top:1px solid #000;border-right:1px solid #000;"><b>O.S.</b></td>
              <td style="border-top:1px solid #000;border-right:1px solid #000;"><b>SERVICIO</b></td>
              <td style="border-top:1px solid #000;border-right:1px solid #000;"><b>TIEMPO</b></td>
            </tr>
            <?php           $numa=count($os);
			
							$tnuma=10-$numa;
							$sum=0;
							$contar=0;
			for($yy=0; $yy< $numa; $yy++){
								
							if($os[$yy]->piz_fecha==$day['dayg'] && $os[$yy]->ssr_venta=='adicional'){
							
								if($os[$yy]->set_tipo=='pro'){$cod=$os[$yy]->set_name;}else{$cod=$os[$yy]->set_codigo;}
								$sum+=$os[$yy]->ser_approximate_duration;	$contar++;
								?>
            <tr class="odd gradeX">
              <td align="center" style=" border-bottom:1px solid #000;border-right:1px solid #000;"><?php echo $os[$yy]->seo_tower;?></td>
              <td  style=" border-bottom:1px solid #000;border-right:1px solid #000;" title="<?php echo $os[$yy]->set_name;?>"><?php echo mb_strtoupper($cod);?></td>
              <td style="background-color:red; color:white; border-bottom:1px solid #000;border-right:1px solid #000;" align="center"><?php echo $os[$yy]->ser_approximate_duration;?></td>
            </tr>
            <?php } }
			
			
			//servicios varios
			$numasv=count($ossv);
			$sumsv=0;
			for($yysv=0; $yysv< $numasv; $yysv++){
								
							if($ossv[$yysv]->piz_fecha==$day['dayg'] && $ossv[$yysv]->sev_tipo_venta=='adicional'){
								$cod=$ossv[$yysv]->sev_nombre;
								$sumsv+=$ossv[$yysv]->sev_tiempo;	$contarsv++;
								?>
            <tr class="odd gradeX">
              <td align="center" style=" border-bottom:1px solid #000;border-right:1px solid #000;">
			  <?php echo $ossv[$yysv]->seo_tower;?></td>
              <td  style=" border-bottom:1px solid #000;border-right:1px solid #000;" title="<?php echo $ossv[$yysv]->sev_nombre;?>"><?php echo mb_strtoupper($cod);?></td>
              <td style="background-color:red; color:white; border-bottom:1px solid #000;border-right:1px solid #000;" align="center"><?php echo $ossv[$yysv]->sev_tiempo;?></td>
            </tr>
            <?php
							}}
			//tabla con renglones vacios
			 $contar=10 - $contar;
			for($zz=0; $zz<$contar; $zz++)
			{
			 ?>
            <tr class="odd gradeX" height="17px;">
              <td align="center" style=" border-bottom:1px solid #000;border-right:1px solid #000;"></td>
              <td  style=" border-bottom:1px solid #000;border-right:1px solid #000;" ></td>
              <td style="background-color:red; color:white; border-bottom:1px solid #000;border-right:1px solid #000;" align="center"></td>
            </tr>
            <?php } ?>
            <tr class="odd gradeX" style="font-weight:bold; background-color:red; color:yellow; font-size:11px;">
              <td style=" border-bottom:1px solid #000;border-right:1px solid #000;"></td>
              <td style=" border-bottom:1px solid #000;border-right:1px solid #000;"> HRS </td>
              <td style=" border-bottom:1px solid #000;border-right:1px solid #000;" align="center"><?php echo $sum + $sumsv;?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<div class="row"> <br>
  <div align="center" style="font-weight:bold; font-size:16px" >GARANTIAS, INTERNAS Y CAMPAÑAS</div>
  <br>
</div>
<div class="row">
  <?php for($x=0; $x<6; $x++){
					$day=$this->Productividadmodel->dias_semana($x,$inicio);
						
	
	?>
  <div class="col-md-2 col-sm-2">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption" style="font-size:11px;"> SERVICIOS NO TABULADOS </div>
      </div>
      <div class="portlet-body" style="padding:0px;">
        <table class="table " >
          <thead>
          </thead>
          <tbody>
            <tr class="odd gradeX" style="font-weight:bold; background-color:black; color:white; font-size:11px; text-align:center">
              <td style="border-top:1px solid #000;border-right:1px solid #000;"><b>O.S.</b></td>
              <td style="border-top:1px solid #000;border-right:1px solid #000;"><b>SERVICIO</b></td>
              <td style="border-top:1px solid #000;border-right:1px solid #000;"><b>TIEMPO</b></td>
            </tr>
            <?php 
							$num=count($os);
							$tnum=10-$num;
							$sum=0;
							$contarg=0;
							for($xx=0; $xx< $num; $xx++){
								
							if($os[$xx]->piz_fecha==$day['dayg'] && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
								
								if($os[$xx]->set_tipo=='pro'){$cod=$os[$xx]->set_name;}else{$cod=$os[$xx]->set_codigo;}
								$sum+=$os[$xx]->ser_approximate_duration;	
								$contarg++;
								?>
            <tr  class="odd gradeX">
              <td  style="border-bottom:1px solid #000;border-right:1px solid #000;" align="center"><?php echo $os[$xx]->seo_tower;?></td>
              <td style="cursor:help; border-bottom:1px solid #000;border-right:1px solid #000;"  title="<?php echo $os[$xx]->set_name;?>"><?php echo mb_strtoupper($cod);?></td>
              <td style="background-color:red;border-bottom:1px solid #000;border-right:1px solid #000; color:yellow" align="center"><?php echo $os[$xx]->ser_approximate_duration;?></td>
            </tr>
            
            <?php } } 
            
							$num=count($ossv);
							$tnum=10-$num;
							$sumsvi=0;
							$contarg=0;
							for($xxsv=0; $xxsv< $num; $xxsv++){
								
							if($ossv[$xxsv]->piz_fecha==$day['dayg'] && ($ossv[$xxsv]->sev_tipo_venta=='garantia' || $ossv[$xxsv]->sev_tipo_venta=='garantiai' || $ossv[$xxsv]->sev_tipo_venta=='interna') ){
								
								$cod=$ossv[$xxsv]->sev_nombre;
								$sumsvi+=$ossv[$xxsv]->sev_tiempo;	
								$contarg++;
								?>
            <tr  class="odd gradeX">
              <td  style="border-bottom:1px solid #000;border-right:1px solid #000;" align="center"><?php echo $ossv[$xxsv]->seo_tower;?></td>
              <td style="cursor:help; border-bottom:1px solid #000;border-right:1px solid #000;"  title="<?php echo $ossv[$xxsv]->sev_nombre;?>"><?php echo mb_strtoupper($cod);?></td>
              <td style="background-color:red;border-bottom:1px solid #000;border-right:1px solid #000; color:yellow" align="center"><?php echo $ossv[$xxsv]->sev_tiempo;?></td>
            </tr>
            
            
            <?php } } 
					$contarg=10 - $contarg;		
							for($i=0; $i<$contarg; $i++){
							?>
            <tr class="odd gradeX" height="17px;">
              <td  style="border-bottom:1px solid #000;border-right:1px solid #000;" align="center"></td>
              <td style="cursor:help; border-bottom:1px solid #000;border-right:1px solid #000;"  ></td>
              <td style="background-color:red;border-bottom:1px solid #000;border-right:1px solid #000; color:yellow" align="center"></td>
            </tr>
            <?php } ?>
            <tr class="odd gradeX" style="font-weight:bold; background-color:red; color:yellow; font-size:11px;">
              <td style="border-top:1px solid #000;border-bottom:1px solid #000;border-right:1px solid #000;" ></td>
              <td style="border-top:1px solid #000;border-bottom:1px solid #000;border-right:1px solid #000;"> HRS.VEND </td>
              <td style="border-top:1px solid #000;border-bottom:1px solid #000;border-right:1px solid #000;" align="center"><?php echo $sum + $sumsvi;?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<table style="border:1px solid #000; text-align:center" width="200px;" >
  <tr>
    <td style="background-color:red; color:yellow;">CONCENTRADO SEMANAL</td>
  </tr>
</table>
<br>
<table style="border:1px solid #000;" width="200px;">
  <tr>
    <td style="background-color:red; color:yellow; border-bottom:1px solid #000; padding-right:3px; border-right:1px solid #000;">HORAS VENDIDAS TOTALES</td>
    <td style="background-color:red; color:yellow;text-align:center; border-bottom:1px solid #000;"><?php echo $shorasV;?></td>
  </tr>
  <tr>
    <td style="background-color:red; color:yellow; padding-right:3px; border-right:1px solid #000; border-bottom:1px solid #000;">HORAS  PUBLICO</td>
    <td style="background-color:red; color:yellow;text-align:center; border-bottom:1px solid #000;"><?php echo $pubb=publico($os);?></td>
  </tr>
  <tr>
    <td style="background-color:red; color:yellow; padding-right:3px; border-right:1px solid #000; border-bottom:1px solid #000;">ORD. DE SERVICIO PUBLICO</td>
    <td style="background-color:red; color:yellow;text-align:center; border-bottom:1px solid #000;"><?php echo $scounos;?></td>
  </tr>
  <tr>
    <td style="background-color:red; color:yellow; padding-right:3px; border-right:1px solid #000; border-bottom:1px solid #000;">HRS X ORDEN DE SERVICIO PUBLICO</td>
    <td style="background-color:red; color:yellow;text-align:center; border-bottom:1px solid #000;"><?php  
	if($pubb==0 && $scounos==0)
	$TTT=0;
	else
	$TTT=$pubb/$scounos;
	echo number_format($TTT, 2, '.', '');?></td>
  </tr>
</table>

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('control/js/script');?>
<script>
jQuery(document).ready(function() {
	$(function () {
	 $('#date-pickerbb').datepicker({ 
	language: 'es',
	isRTL: false,
         autoclose:true     
	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });
	});
});
</script>