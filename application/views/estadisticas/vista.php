<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='estadisticas';$this->load->view('globales/menu',$menu);
$name=array('Monday'=>'Lun','Tuesday'=>'Mar','Wednesday'=>'Mie','','Thursday'=>'Jue','Friday'=>'Vie','Saturday'=>'Sab');
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
function tecnicos($arr,$da){
		$sum=0;
		foreach($arr as $ar){
		if($ar->eqp_fecha==$da )$sum++;
		}
		return $sum;
		}
		
function horasV($arr,$da){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da  )$sum+=$ar->ser_approximate_duration;	
		}
		return $sum;
		}
		
function horasVsv($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da && ($ar->sev_tipo_venta=='garantia' ||  $ar->sev_tipo_venta=='garantiai' ||  $ar->sev_tipo_venta=='interna' || $ar->sev_tipo_venta=='venta' || $ar->sev_tipo_venta=='adicional'))$sum+=$ar->sev_tiempo;	
			
			
			}
		return $sum;
		}
		
function horasVCON($arr,$da,$ida){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da and $ar->seo_adviser==$ida  )$sum+=$ar->ser_approximate_duration;	
		}
		return $sum;
		}
		
function horasVCONsv($arr,$da,$ida){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da and $ar->seo_adviser==$ida  )$sum+=$ar->sev_tiempo;	
		}
		return $sum;
		}				
		
function horasVV($arr,$da){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da and ( $ar->ssr_venta=='venta' || $ar->ssr_venta=='adicional'))$sum+=$ar->ser_approximate_duration;	
		}
		return $sum;
		}
	function horasVVsv($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da && ( $ar->sev_tipo_venta=='venta' || $ar->sev_tipo_venta=='adicional'))$sum+=$ar->sev_tiempo;	
			
			
			}
		return $sum;
		}	
		
function horasVVAA($arr,$da,$ida){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da and $ar->seo_adviser==$ida and ( $ar->ssr_venta=='venta' || $ar->ssr_venta=='adicional'))$sum+=$ar->ser_approximate_duration;	
		}
		return $sum;
		}
		
function horasVVAAsv($arr,$da,$ida){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da and $ar->seo_adviser==$ida and ( $ar->sev_tipo_venta=='venta' || $ar->sev_tipo_venta=='adicional'))$sum+=$ar->sev_tiempo;	
		}
		return $sum;
		}				
		
function countos($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da  )$sum++;
			
			}
		return $sum;
		}		

function countosAse($arr,$da,$ase){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da && $ar->seo_adviser==$ase )$sum++;
			
			}
		return $sum;
		}		

function grafica($day,$inicio,$os,$ossv,$idase){
 
			$CONsum=0;
			$TCONsum=0;
			$STCONnumb=0;
			 for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						
           //
           
		      $CONnumbsv=horasVCONsv($ossv,$day,$idase); 
			  $CONnumb=horasVCON($os,$day,$idase); 
			  $CONsum+=$CONnumb + $CONnumbsv;
			  
			  $TCONnumb=horasV($os,$day); 
			  $TCONnumbsv=horasVsv($ossv,$day);
			  $STCONnumb+=$TCONnumb + $TCONnumbsv;
			  
			  if($CONnumb==0 && $CONnumbsv==0){$OCONoper=0;}
			  else{
			  $OCONoper=($CONnumb / $TCONnumb) * 100;}
			//echo number_format($OCONoper, 2, '.', '').' %';
			
             } 
            // resutaldo
			$SCONsum=($CONsum / $STCONnumb) * 100;
			return number_format($SCONsum, 2, '.', '');	
	
	}
 ?>

<form name="form1" method="get" action="<?php echo base_url();?>estadisticas/">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="fa fa-bar-chart-o"></i>
				<a href="#">Concentrado</a>
			<i class="fa fa-angle-right"></i>
		</li>
        
		
        <li class="pull-right" style="text-align:right; margin-left:15px;">
    <?php if($_SESSION['sfrol']<=2) {?>
    
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    <?php } ?>
    </li>
    
    <li class="pull-right" >
            <div  class="input-group input-medium date " id="date-pickerbb" data-date-format="yyyy-mm-dd" style="text-align:right;">
            <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
            Fecha: <?php echo $fecha;?> <i style="cursor:pointer; float:right; margin-left:5px;" class="fa fa-calendar  input-group-btn"></i>
            </div>
		</li> 
		
	</ul>
</form>
<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th"></i>CONCENTRADO </div>
      </div>
      <div class="portlet-body" style="padding:0px;">
        <table width="100%">
          <tr>
            <td rowspan="2"  style="background-color:#333; color:white; font-weight:bold; font-size:15px;  border-bottom:1px solid #ccc; border-right:1px solid #333; width:113px; " align="center"><?php echo $meses[date('n')-1];?></td>
           <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           <td style="background-color:#333; color:white; border-right:1px solid #ccc; width:85px;" align="center"> 
		   <?php $dname = date('l', strtotime($day));echo $name[$dname];?>
           </td>
           <?php } ?>
           
           <td rowspan="2" style="background-color:#333; border-bottom:1px solid #333; color:white; text-align:center; ">SEMANA</td>
          </tr>
          <tr align="center">
            <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['days'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333"><?php echo $day;?></td>
             <?php } ?>
          </tr>
          <tr>
            <td style="border-bottom:1px solid #ccc;border-right:1px solid #333; background-color:#333; color:white" align="center">HRS.TOTALES<br>VENDIDAS</td>
            <?php $sum=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
        <td style="border-bottom:1px solid #333;border-right:1px solid #333" align="center">
		<?php echo $numb=horasV($os,$day) + horasVsv($ossv,$day); $sum+=$numb;?></td>
            <?php } ?>
<td style="border-bottom:1px solid #333;border-right:1px solid #333; background-color:red; color:yellow;" align="center">
<?php echo number_format($sum, 2, '.', '');?>
</td>
          </tr>
          <tr>
            <td style="border-bottom:1px solid #ccc ; background-color:red; font-weight:bold; color:yellow;" align="center">HRS. PUBLICO<br>VENDIDAS</td>
 <?php $sumb=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
<td style="border-bottom:1px solid #333;border-right:1px solid #ccc; background-color:#333; color:white;" align="center">
<?php echo $numbb=horasVV($os,$day) + horasVVsv($ossv,$day); $sumb+=$numbb;?>
</td>
<?php } ?>
            <td style="border-bottom:1px solid #333; background-color:#333; color:white;" align="center"><?php echo number_format($sumb, 2, '.', '');?></td>
          </tr>
          <tr>
          <tr>
            <td style="border-bottom:1px solid #ccc;border-right:1px solid #333;background-color:#333; color:white" align="center">ORDENES  PUBLICO</td>
<?php $sumc=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333" align="center"><?php
						 $counos=countos($cos,$day); 
						 $sumgar=0;
						 $num=count($os);
			for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$sumgar++;			
							}}
			
			echo $counos - $sumgar ;
						  $sumc+=($counos-$sumgar);
						 ?></td>
            <?php } ?>
            <td style="border-bottom:1px solid #333;background-color:red; color:yellow;" align="center"><?php echo number_format($sumc, 2, '.', '');?></td>
          </tr>
          <tr>
            <td style="border-bottom:1px solid #ccc;border-right:1px solid #333;background-color:#333; color:white " align="center">HRS. PROMEDIO<br>O.S. PUBLICO</td>
           <?php $sumpro=0; $hpro=0;for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333" align="center">
            <?php
			
			
			$numbb=horasVV($os,$day) + horasVsv($ossv,$day); 
			$counos=countos($cos,$day);
			if($numbb==0){
			echo $hpro=0;}
			else{
			 $hpro=$numbb/$counos;
			echo number_format($hpro, 2, '.', '');
			}
			
			?>
            </td>
            <?php } ?>
            <td style="border-bottom:1px solid #333;background-color:red; color:yellow;" align="center">
            <?php if($sumb==0 && $sumc==0 )$tsum=0; else {$tsum=$sumb/$sumc;} echo number_format($tsum, 2, '.', '');?>
            </td>
          </tr>
          <tr>
            <td style="border-bottom:1px solid #ccc;border-right:1px solid #333;background-color:#333; color:white" align="center">NUMERO DE <br> TECNICOS</td>
           <?php $sumt=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333" align="center">
            <?php echo $tec=tecnicos($tecnicos,$day); $sumt+=$tec;?>
            </td>
            <?php } ?>
            <td style="border-bottom:1px solid #333;background-color:red; color:yellow;" align="center">
            <?php echo $sumt;?>
            </td>
          </tr>
          <tr>
            <td style="border-bottom:1px solid #ccc;border-right:1px solid #333;background-color:#333; color:white" align="center">DISPONIBILIDAD<br> MANO DE OBRA</td>
             <?php $sumo=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333" align="center">
            <?php 
			$tec=tecnicos($tecnicos,$day); 
            if($x==5){$semana=$capacidades[0]->woc_weekends_service_hours;}
			else{$semana=$capacidades[0]->woc_weekdays_service_hours;}
			$oper=$tec * $semana * $capacidades[0]->woc_productivity_factor;
			echo number_format($oper, 2, '.', '');
			$sumo+=$oper;?>
            </td>
            <?php } ?>
            <td style="border-bottom:1px solid #333;background-color:red; color:yellow;" align="center">
            <?php echo number_format($sumo, 2, '.', '');?></td>
          </tr>
          <tr>
            <td style="border-right:1px solid #333;background-color:#333; color:white; font-weight:bold" align="center">% UTILIZACION</td>
            <?php $sum=0;$sumo=0;for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
        <td style="border-bottom:1px solid #333; background-color:red; color:yellow; border-right:1px solid #333; font-weight:bold" align="center">
		<?php  
		$numb=horasV($os,$day) + horasVsv($ossv,$day);
		$tec=tecnicos($tecnicos,$day); 
        if($x==5)
		  {
			 $semana=$capacidades[0]->woc_weekends_service_hours;
		  }
	    else
		  {
			  $semana=$capacidades[0]->woc_weekdays_service_hours;
		  }
		
		$oper=$tec * $semana * $capacidades[0]->woc_productivity_factor; 
		
		if($oper==0)
		{ 
		  echo 0;
		}
		else
		{
		$poru=($numb / $oper)*100;
		echo number_format($poru, 2, '.', '').'%';
		}
		$sum+=$numb;
		$sumo+=$oper;
		?></td>
            <?php } ?>
<td style="border-bottom:1px solid #333;border-right:1px solid #333; background-color:#333; color:yellow;font-weight:bold;" align="center">
<?php 
if($sumo==0){echo $porut=0;}
else $porut=($sum / $sumo)*100; 

echo $UTILIZACION=number_format($porut, 2, '.', '').'%';?>
</td>
          </tr>
        </table>
      </div>
    </div>
  <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th"></i>DETALLE DE ORDENES DE SERVICIO POR ASESOR</div>
      </div>
      <div class="portlet-body" style="padding:0px">
         <table width="100%">
          <tr>
            <td rowspan="2"  style="background-color:#333; color:white; font-weight:bold; font-size:15px;  border-bottom:1px solid #ccc; border-right:1px solid #333; width:113px; " align="center"><?php echo $meses[date('n')-1];?></td>
           <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           <td style="background-color:#333; color:white; border-right:1px solid #ccc; width:85px;" align="center"> 
		   <?php $dname = date('l', strtotime($day));echo $name[$dname];?>
           </td>
           <?php } ?>
           
           <td rowspan="2" style="background-color:#333; border-bottom:1px solid #333; border-right:1px solid white; color:white; text-align:center; ">SEMANA</td>
           <td rowspan="2" style="background-color:#333; border-bottom:1px solid #333; color:white; text-align:center; ">CONT %</td>
          </tr>
          <tr align="center">
            <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['days'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333"><?php echo $day;?></td>
             <?php } ?>
          </tr>
          
          <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          <tr>
            <td align="center" style=" border-bottom:1px solid #CCC; background-color:#333; color:white;border-right:1px solid #333"><?php echo strtoupper($us->sus_name.'<br>'.$us->sus_lastName);?></td>
          <?php 
		  $asumc=0;
		  $asumgar=0;
		  $ssumgar=0;
		  $ascte=0;
		  $num=count($os);
		  for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333">
           <?php 
		   $sumgar=0;
		   
//ventas		   
for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day && $us->sus_idUser==$os[$xx]->sus_idUser && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$sumgar++;			
							}}
							
for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$ssumgar++;			
							}}							
		   $asumgar+=$sumgar;
		   $cte=countos($cos,$day); 
		   $ascte+=$cte;
		   $acounos=countosAse($cos,$day,$us->sus_idUser); 
		   $asumc+=$acounos - $sumgar;
		   echo $acounos-$sumgar;
		   
		   
		   ?>
            </td>
            <?php } ?>
            <td align="center" style="background-color:red;border-right:1px solid #333; border-bottom:1px solid #333; color:yellow"><?php echo $asumc;?></td>
            <td align="center" style="background-color:red;border-right:1px solid #333; border-bottom:1px solid #333; color:yellow"><?php 
			$ascte=$ascte - $ssumgar;
			$tasumc=$asumc;
			if($tasumc==0 &&  $ascte==0){$asecteT=0; }
			else {$asecteT=( $tasumc / $ascte) * 100; }
			
			echo number_format($asecteT, 1, '.', '');?> %</td>
          </tr>
          <?php }} ?>
          <tr style="background-color:#333; color:white">
            <td align="center" style=" background-color:red; color:yellow;font-weight:bold;border-right:1px solid #333;border-bottom:1px solid #CCC">PUBLICO</td>
            <?php 
			
			$num=count($os);
			 for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td align="center" style="border-right:1px solid #333;border-bottom:1px solid #333">
			<?php 
			
			$counos=countos($cos,$day); 
			
			$sumgar=0;
			for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$sumgar++;			
							}}
			
			echo $counos - $sumgar ;
			$asumc+=$counos - $sumgar ;
			?></td>
            
			
			<?php } ?>
            <td align="center" style="border-right:1px solid #333; border-bottom:1px solid #333"><?php echo $asumc;?></td>
            <td align="center" style="border-right:1px solid #333; border-bottom:1px solid #333">100%</td>
          </tr>
          
         
          <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          <tr>
            <td align="center" style=" border-bottom:1px solid #CCC; background-color:#333; color:white;border-right:1px solid #333"><?php echo strtoupper($us->sus_name.'<br>'.$us->sus_lastName);?></td>
            
<?php
$num=count($os);
$tsumgar=0;
$ssumgar=0;
			 for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
?>

<td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333"><?php 
$sumgar=0;
$torr=0;
$m=0;
for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					
						
					
							$ssumgar++;	
							
						
						
								
							}}	

for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day && $us->sus_idUser==$os[$xx]->sus_idUser && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
	                
					
							$sumgar++;	
							
					
					    
							
					
						
							}}
			$tsumgar+=$sumgar;			

echo $sumgar;?></td>
<?php } ?>            
            
            
<td align="center" style="background-color:red;border-right:1px solid #333; border-bottom:1px solid #333; color:yellow"><?php
echo $tsumgar;
?></td>
<td align="center" style="background-color:red;border-right:1px solid #333; border-bottom:1px solid #333; color:yellow">
<?php 
			
			if($tsumgar==0 &&  $ssumgar==0){$asecteT=0; }
			else {$asecteT=( $tsumgar / $ssumgar) * 100; }
			
			 echo number_format($asecteT, 1, '.', '');?> %</td>
          </tr>
          <?php }} ?>
          <tr style="background-color:#333; color:white;">
            <td align="center" style=" background-color:red; color:yellow;font-weight:bold;border-right:1px solid #333;border-bottom:1px solid #CCC">INT. Y GTIA.</td>
            
<?php
$num=count($os);
$tsumgar=0;

			 for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
?>

            <td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333">
<?php

$sumgar=0;
for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$sumgar++;			
							}}
			$tsumgar+=$sumgar;			

echo $sumgar;?></td>
<?php } ?>             
            
            
            </td>
            <td align="center" style="border-right:1px solid #333; border-bottom:1px solid #333">
            <?php echo $tsumgar;?>
            </td>
            <td align="center" style="border-right:1px solid #333;border-bottom:1px solid #333">100 %</td>
          </tr>
         
          <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          <tr>
            <td align="center" style=" border-bottom:1px solid #CCC; background-color:#333; color:white;border-right:1px solid #333"><?php echo strtoupper($us->sus_name.'<br>'.$us->sus_lastName);?></td>
            
             <?php 
		  $asumc=0;
		  $asumgar=0;
		  $ssumgar=0;
		  $ascte=0;
		  $num=count($os);
		  for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            
            <td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333">
              <?php 
		   
		   
		  						
		   
		   $cte=countos($cos,$day); 
		   $ascte+=$cte;
		   $acounos=countosAse($cos,$day,$us->sus_idUser); 
		 $asumc+=$acounos;
		   echo $acounos;
		   
		   
		   ?>
            
            
            
            </td>
            <?php }?>
            <td align="center" style="background-color:red;border-right:1px solid #333; border-bottom:1px solid #333; color:yellow"><?php echo $asumc;?></td>
            <td align="center" style="background-color:red;border-right:1px solid #333; border-bottom:1px solid #333; color:yellow">
            <?php
			
			if($asumc==0 & $ascte==0) $ttasetett=0;
			else $ttasetett=($asumc / $ascte) * 100;
			//echo $ascte;
			echo number_format($ttasetett, 1, '.', '');?> % 
		
            </td>
          </tr>
          <?php }} ?>
          <tr style="background-color:#333; color:white;">
            <td align="center" style="font-weight:bold;border-right:1px solid #333;border-bottom:1px solid #333">TOTALES</td>
  <?php $sumc=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>         
           
<td align="center" style=" background-color:red; color:yellow;font-weight:bold;border-bottom:1px solid #333;border-right:1px solid #333">
<?php echo $counos=countos($cos,$day); $sumc+=$counos;?>
</td>
<?php } ?>
<td align="center" style="font-weight:bold;background-color:#333;border-right:1px solid #333; color:yellow;border-bottom:1px solid #333">
<?php echo number_format($sumc, 2, '.', '');?></td>
<td align="center" style="font-weight:bold;background-color:#333;border-right:1px solid #333;color:yellow;border-bottom:1px solid #333">100 %</td>
          </tr>
          
        </table>
        
        
      </div>
    </div>
  </div>
  
  
  
  
  
  <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-circle"></i>HORAS PROMEDIO POR O.S. </div>
      </div>
      <div class="portlet-body" style="padding:0px">
        <table width="100%">
          <tr>
            <td rowspan="2"  style="background-color:#333; color:white; font-weight:bold; font-size:15px;  border-bottom:1px solid #ccc; border-right:1px solid #333; width:113px; " align="center"><?php echo $meses[date('n')-1];?></td>
           <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           <td style="background-color:#333; color:white; border-right:1px solid #ccc; width:85px;" align="center"> 
		   <?php $dname = date('l', strtotime($day));echo $name[$dname];?>
           </td>
           <?php } ?>
           
           <td rowspan="2" style="background-color:#333; border-bottom:1px solid #333; color:white; text-align:center; ">SEMANA</td>
          </tr>
          <tr align="center">
            <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['days'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333"><?php echo $day;?></td>
             <?php } ?>
          </tr>
          <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          <tr>
            <td  align="center" style="width:100px; border-bottom:1px solid #ccc;border-right:1px solid #333; background-color:#333; color:white;" ><?php echo strtoupper($us->sus_name.'<br>'.$us->sus_lastName);?></td>
           
           <?php 
		  $HPasumc=0;
		  $HPasumgar=0;
		  $HPssumgar=0;
		  $HPascte=0;
		  $HPnum=count($os);
		  $aasumb=0; 
		  $HPaasumb=0;
		  $numsv=count($ossv); 
		   for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           
           
            <td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333;">
            <?php 
			//horas publico
			$aanumbbsv=horasVVAAsv($ossv,$day,$us->sus_idUser);
			$aanumbb=horasVVAA($os,$day,$us->sus_idUser); $aasumb+=$aanumbb + $aanumbbsv;
			
		   //calculas ordenes de servicio publico de un asesor servicios varios
		   $sumgarsv=0;
for($xx=0; $xx< $numsv; $xx++){								
if($ossv[$xx]->piz_fecha==$day && $us->sus_idUser==$ossv[$xx]->sus_idUser && ($ossv[$xx]->sev_tipo_venta=='garantia' || $ossv[$xx]->sev_tipo_venta=='garantiai' || $ossv[$xx]->sev_tipo_venta=='interna') ){
					$sumgarsv++;			
							}}
							
		   //calculas ordenes de servicio publico de un asesor
		   $sumgar=0;
for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day && $us->sus_idUser==$os[$xx]->sus_idUser && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$sumgar++;			
							}}
							
for($xx=0; $xx< $num; $xx++){								
if($os[$xx]->piz_fecha==$day  && ($os[$xx]->ssr_venta=='garantia' || $os[$xx]->ssr_venta=='garantiai' || $os[$xx]->ssr_venta=='interna') ){
					$ssumgar++;			
							}}							
		   $asumgar+=$sumgar + $sumgarsv;
		   $cte=countos($cos,$day); 
		   $ascte+=$cte;
		   $acounos=countosAse($cos,$day,$us->sus_idUser); 
		   $asumc+=$acounos - ($sumgar + $sumgarsv);
		   $HPP=$acounos-($sumgar + $sumgarsv);
		   if($HPP==0){echo 0;}
		   else{
		   $HP=($aanumbb + $aanumbbsv) /$HPP;
		   echo number_format($HP, 2, '.', '');
		   }
		   $HPaasumb+=$HPP;
		   ?>
            </td>
            <?php } ?>
            <td align="center"  style="background-color:red; color:yellow; border-bottom:1px solid #333;">
            <?php if($HPaasumb==0){echo 0;} else{$HPoper=$aasumb / $HPaasumb; echo number_format($HPoper, 2, '.', '');}?>
            </td>
          </tr>
          <?php }} ?>
          <tr>
            <td align="center"  style="width:100px; background-color:#333; color:white;" >TOT X DIA</td>
             <?php $sumpro=0; $hpro=0;for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           <td align="center" style=" font-weight:bold; background-color:red; color:yellow">
           <?php
			
			
			$numbb=horasVV($os,$day) + horasVVsv($ossv,$day); 
			$counos=countos($cos,$day);
			if($numbb==0){
			echo $hpro=0;}
			else{
			 $hpro=$numbb/$counos;
			echo number_format($hpro, 2, '.', '');
			}
			
			?>
           </td>
           <?php } ?>
            <td align="center"  style=" font-weight:bold; background-color:#333; color:yellow">
            <?php if($sumb==0 && $sumc==0 )$tsum=0; else $tsum=$sumb/$sumc; echo number_format($tsum, 2, '.', '');?>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  
    <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-circle"></i>CONTRIBUCION EN HORAS POR ASESOR </div>
      </div>
      <div class="portlet-body" style="padding:0px">
         <table width="100%">
          <tr>
            <td rowspan="2"  style="background-color:#333; color:white; font-weight:bold; font-size:15px;  border-bottom:1px solid #ccc; border-right:1px solid #333; width:113px; " align="center"><?php echo $meses[date('n')-1];?></td>
           <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           <td style="background-color:#333; color:white; border-right:1px solid #ccc; width:85px;" align="center"> 
		   <?php $dname = date('l', strtotime($day));echo $name[$dname];?>
           </td>
           <?php } ?>
           
           <td rowspan="2" style="background-color:#333; border-bottom:1px solid #333; color:white; text-align:center; ">SEMANA</td>
          </tr>
          <tr align="center">
            <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['days'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333"><?php echo $day;?></td>
             <?php } ?>
          </tr>
          <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          <tr>
            <td  align="center" style="width:100px; border-bottom:1px solid #ccc; background-color:#333; color:white;" ><?php echo strtoupper($us->sus_name.'<br>'.$us->sus_lastName);?></td>
            <?php $CONsum=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333;">
            <?php 
			$CONnumbsv=horasVCONsv($ossv,$day,$us->sus_idUser);
			$CONnumb=horasVCON($os,$day,$us->sus_idUser); $CONsum+=$CONnumb + $CONnumbsv;
			
			echo number_format($CONnumb + $CONnumbsv, 2, '.', '')
			?>
            </td>
            <?php } ?>
            <td align="center"  style="background-color:red; color:yellow; border-bottom:1px solid #333;">
            <?php echo number_format($CONsum, 2, '.', '');?>
            </td>
          </tr>
          <?php }} ?>
          <tr>
            <td align="center"  style="width:100px; background-color:#333; color:white;" >TOT X DIA</td>
           <?php $TCONsum=0; for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td align="center" style=" font-weight:bold; background-color:red; color:yellow">
            <?php 
			$TCONnumbsv=horasVsv($ossv,$day);
			$TCONnumb=horasV($os,$day);
			echo number_format($TCONnumb + $TCONnumbsv, 2, '.', '');
			 $TCONsum+=$TCONnumb + $TCONnumbsv;?>
            </td>
            <?php } ?>
            <td align="center"  style=" font-weight:bold; background-color:#333; color:yellow">
            <?php echo number_format($TCONsum, 2, '.', '');?>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  
   <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-circle"></i>CONTRIBUCION % EN HORAS X ASESOR </div>
      </div>
      <div class="portlet-body" style="padding:0px">
                <table width="100%">
          <tr>
            <td rowspan="2"  style="background-color:#333; color:white; font-weight:bold; font-size:15px;  border-bottom:1px solid #ccc; border-right:1px solid #333; width:113px; " align="center"><?php echo $meses[date('n')-1];?></td>
           <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
           <td style="background-color:#333; color:white; border-right:1px solid #ccc; width:85px;" align="center"> 
		   <?php $dname = date('l', strtotime($day));echo $name[$dname];?>
           </td>
           <?php } ?>
           
           <td rowspan="2" style="background-color:#333; border-bottom:1px solid #333; color:white; text-align:center; ">SEMANA</td>
          </tr>
          <tr align="center">
            <?php for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['days'];
						?>
            <td style="border-bottom:1px solid #333;border-right:1px solid #333"><?php echo $day;?></td>
             <?php } ?>
          </tr>
          <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          <tr>
            <td  align="center" style="width:100px; border-bottom:1px solid #ccc; background-color:#333; color:white;" ><?php echo strtoupper($us->sus_name.'<br>'.$us->sus_lastName);?></td>
            <?php 
			$CONsum=0;
			$TCONsum=0;
			$STCONnumb=0;
			 for($x=0; $x<6; $x++){
						$day=$this->Productividadmodel->dias_semana($x,$inicio);
						$day=$day['dayg'];
						?>
            <td align="center" style="border-bottom:1px solid #333;border-right:1px solid #333;">
            <?php 
			  //servicios varios
			  $CONnumbsv=horasVCONsv($ossv,$day,$us->sus_idUser); 
			  $TCONnumbsv=horasVsv($ossv,$day); 
			  //tabla
			  $CONnumb=horasVCON($os,$day,$us->sus_idUser); 
			  $CONsum+=$CONnumb + $CONnumbsv;
			  
			  $TCONnumb=horasV($os,$day); 
			  $STCONnumb+=$TCONnumb + $TCONnumbsv;
			  ///Operaciones
			  if($CONnumb==0 && $CONnumbsv==0){$OCONoper=0;}
			  else{
			  $OCONoper=(($CONnumb + $CONnumbsv) / ($TCONnumb + $TCONnumbsv)) * 100;}
			echo number_format($OCONoper, 2, '.', '').' %';
			?>
            </td>
            <?php } ?>
            <td align="center"  style="background-color:red; color:yellow; border-bottom:1px solid #333;">
            <?php 
			if($STCONnumb==0){$SCONsum=0;}
			else{
			$SCONsum=($CONsum / $STCONnumb) * 100;}
			echo number_format($SCONsum, 2, '.', '').' %';?>
            </td>
          </tr>
          <?php }} ?>
         
        </table>

      </div>
    </div>
    
    
    <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-circle"></i>Porcentaje de Utilización de Taller
                </div>
               
            </div>
            <div class="portlet-body form"   >
            
            
            
            <form role="form" class="form-horizontal form-bordered">
               <div class="form-group last">
										<div class="col-md-12">
											<input id="range_333" type="text" name="range_3" value="0;100"/>
											
											
										</div>
                                        </div>
                
                </form>
                
            </div>
        </div> 
        
        
        <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-circle"></i>Porcentaje por Asesor
                </div>
               
            </div>
            <div class="portlet-body form"   >
            
            <div id="grafica"></div>
                
            </div>
        </div> 
        
    
  </div>

  
  
  </div><div class="row">
  

   


</div>
<script src="<?php echo base_url(); ?>assets/scripts/handlebars-v1.3.0.js" type="text/javascript"></script>
<!-- END OVERVIEW STATISTIC BARS-->
<?php $this->load->view('globales/footer');?>
<?php 
echo $UTILIZACION=substr($UTILIZACION, 0, -1);
$datav['utilizacion']=$UTILIZACION;
$this->load->view('cotizaciones/js/script',$datav);?>

    
    
    <script>
jQuery(document).ready(function() {
	$(function () {
	 $('#date-pickerbb').datepicker({ 
	language: 'es',
	isRTL: false,
         autoclose:true
                
	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });

	 
	});
});
</script>

<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php foreach($usuario as $us) {
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {?>
          ['<?php echo strtoupper($us->sus_name.' '.$us->sus_lastName);?>',<?php echo grafica($day,$inicio,$os,$ossv,$us->sus_idUser)?>],
          
      
        <?php }}?>
        ]);

        var options = {
           height:'1000px',
		  width:'1000px',
		   
		   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
		   
        };

        var chart = new google.visualization.PieChart(document.getElementById('grafica'));
        chart.draw(data, options);
      }
    </script> 