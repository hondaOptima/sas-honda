
 
 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Multipuntos Seminuevos</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="<?php echo base_url();?>assets/css/seminuevos/multi.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 
  </head>
  <body>
    <!-- Aqui va todo  -->
    <div id="multi-bar" class=" ">
        <div class="div-contador">
            <div class="cont-1 cont">
                <label id="circle1">0</label>
            </div>
            <div class="cont-2 cont">
                <label id="circle2">0</label> 
            </div>
        </div>
        <div id="menu-bars" >
            <i class="fa fa-bars fa-2x"></i>
        </div>
        <div class="div-container-lbl-cont">
            <div class="first-lbl-cont div-lbl-cont">
            <label class="lbl-cont">Cumple Estandar Honda</label>
            </div>
            <div class="div-lbl-cont">
                <label class="lbl-cont">Necesita servicio</label>
            </div>
        </div>
        <div class="div-title-diag text-center">
            <h2 style="cursor:pointer" class="h5-back">
                Inspeccion Mecanica
            </h2>
        </div>
    </div>
    <div class="columna columna1">
        <div id="div-interior" class="firstcol col-md-12">
            <div data-div="sec1"  class="fctitles isClose col-md-12">
                <h5 class="text-center h5-fctitle">Refacciones</h5>
            </div>  
            <div data-div="sec2" class="fctitles isClose">
                <h5 class="text-center h5-fctitle">Prueba de manejo</h5>
            </div>
            <div data-div="sec3" class="fctitles isClose col-md-12">
                <h5 class="text-center h5-fctitle">Debajo del cofre</h5>
            </div>
            <div data-div="sec4" class="fctitles isClose col-md-12">
                <h5 class="text-center h5-fctitle">Debajo Del Vehiculo</h5>
            </div>
            <div data-div="sec5" class="fctitles isClose col-md-12">
                <h5 class="text-center h5-fctitle">Exterior</h5>
            </div>
            <div data-div="sec6" class="fctitles isClose col-md-12">
                <h5 class="text-center h5-fctitle">Interior</h5>
            </div>
        </div>
    </div>
        
        <div class="columna2" style="display:none">
            <div id="sec1" class="semaforos" style="display:none">
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Aceite de motor y filtro</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                    
                    
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Filtro de aire</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Liquido de frenos</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Tapetes (reemplácelos, a menos que estén nuevos)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Hojas de los limpiaparabrisas</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Refrigerante – si está contaminado</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Llantas – 4/32” de profundidad mínima superficie de rodamiento</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Balatas – si el desgaste es mayor al 50%</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Zapatas de frenos - si el desgaste es mayor al 50%</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Batería – si tiene más de tres años</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Poste de la antena – si esta torcido o pegado</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">ATF – si está descolorido</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                </div>
                
             <div id="sec2" class="semaforos" style="display:none">
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Arranque (frío / caliente)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Aceleración (potencia, puntos de cambio de la A/T)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Manejo (suavidad)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Frenado (ruido, vibración, esfuerzo)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Calidad de los cambios de A/T</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento de cambios de M/T</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Embrague de M/T (suavidad, esfuerzo, patinamiento)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Ruido de la articulación CV (giro I/D asegurado totalmente)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Vibración en marcha mínima (frío / caliente)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Dirección (esfuerzo, tendencia D/I)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento del control de crucero</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento del freno de estacionamiento</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento del claxon </h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Ruido de viento / rechinidos / traqueteos</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento del velocímetro / odometro</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento de la calefacción/ desempañador</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Desempeño de enfriamiento del A/C</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Vibración de las llantas</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Temperatura de funcionamiento del motor</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                    
             </div>
             
             
           <div id="sec3" class="semaforos" style="display:none">
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Batería (corrosión, edad, nivel de carga)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Radiador (fugas, daños por golpes)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Funcionamiento del ventilador de enfriamiento</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Refrigerante (reemplácelo si está sucio o si no es Honda)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Dirección hidráulica (nivel del fluido, tensión de la banda)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Distribuidor (Estado de la tapa / rotor, holgura del eje)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Cables de encendido</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Conexiones del sistema de combustible (fugas)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Compresor del A/C (ciclos adecuados, tensión de la banda)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Aire del A/C – temperatura de salida</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Ajuste de la válvula de la calefacción</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Tensión de la banda del alternador</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Ruido de válvulas</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Estado de los soportes del motor</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Liquido de frenos (reemplácelo con liquido Honda)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Condensador del A/C (corrosión, daños por golpes)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
                <div class="div-semaforo">
                        <h5 class="lbl-semaforo">Motor (retire la cubierta de válvulas, verifique manto. adecuado)</h5>
                        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                        <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                        <div class="div-inp-costo">   
                            <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                        </div>
                </div>
            </div>
            
            <!--    **************SECCION 4**************    -->
            <div id="sec4" class="semaforos" style="display:none">
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Llantas / rango de velocidad OEM</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Llantas / tamaño OEM</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Daños producidos por el camino</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Misma calidad en las cuatro llantas</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Desgaste disparejo</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Grietas en la pared lateral</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Llantas / rines</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Estado del vástago de la válvula</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Tuercas de la rueda apretadas adecuadamente (no de más)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Motor / transmisión</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Motor (fugas de aceite, cuerda del tapón de drenado)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas en la transmisión</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas de refrigerante</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Cubrepolvos del eje motriz (grietas, fugas)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Frenos</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Espesor de las balatas</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas en el calibrador</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Espesor de las balatas traseras</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas en los cilindros de las ruedas traseras</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Juego y ajuste del freno de estacionamiento</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Espesor y superficie del rotor</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Juego del calibrador</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Espesor y superficie de los tambores traseros</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas en mangueras / conductos hidráulicos</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Suspensión</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas en las barras McPherson / amortiguadores</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Estados de los bujes</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Ajuste de las rótulas</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas en la dirección / cremallera de la dirección hidráulica</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>  
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Ajuste de los extremos de la barra de acoplamiento</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Sistema de escape</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>    
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas / traqueteos / daños por golpes</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div>      
            </div>
            <!--    **************SECCION 5%**************    -->
            <div id="sec5" class="semaforos" style="display:none">
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Faros (alta / baja, ajuste)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Luces de freno / direccionales</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Antena (baja totalmente)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Luces de rodaje</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Fugas de agua</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Luz de la placa</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Luz de reversa</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Luces de emergencia</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
            </div>
            
            <!--    **************  SECCION 6  **************    -->
            
            <div id="sec6" class="semaforos" style="display:none">
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento del medidor de combustible</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Todas las luces de aviso (encendidas con interruptor en ON)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento de radio / CD / navegación</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento de los espejos exteriores (D/I)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento de ventanas (ruido, velocidad, recorrido)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Seguros de las puertas</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento de la alarma (revise el control remoto)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Llanta de refacción (revise presión)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Cinturones (abroche / retracción apropiadas)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento del medidor de temperatura</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento de limpiaparabrisas / lavadores</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento del desempañador trasero</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Espejo interior / espejos de vanidad</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento de asientos eléctricos</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Luces interiores (funcionamiento interruptor / puerta)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Cajuela (gato, herramientas)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funcionamiento del techo corredizo</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">SRS (sin luces de aviso)</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Apertura de la puerta de combustible</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Apertura del cofre</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Apertura de la cajuela</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Movimiento del reposacabezas</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Calefacción de los asientos</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Inclinación del volante</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Función de modo automático del radio</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Funciones del reloj</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
                <div class="div-semaforo">
                    <h5 class="lbl-semaforo">Encendedor</h5>
                    <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-2x circle1 opa-i"></i>
                    <i class="fa fa-check-circle fa-2x circle2 opa-i"></i></div>
                    <div class="div-inp-costo">   
                        <input type="text" class="inp-semi-costo form-control" placeholder="Precio De Reparacion">
                    </div>
                </div> 
            </div>
        </div>
    <!-- Aqui se termina todo  -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/multipuntos/multi.js"></script>
    
    
    <script type="text/javascript">    
        $(document).ready(function(){
            $('.isOpen').live('click',function(){
                $('#'+$(this).attr('data-div')).fadeOut(100);
                $(this).removeClass('isOpen');
                $(this).addClass('isClose');
            });
            
            $('.isClose').live('click',function(){
                var isopen = $('.isOpen');
                isopen.removeClass('isOpen');
                isopen.addClass('isClose');
                $('.semaforos').fadeOut(100);
                $('#'+$(this).attr('data-div')).delay('400').fadeIn(100);
                $(this).removeClass('isClose');
                $(this).addClass('isOpen');
            });
            
            
            $('.fctitles').live('click',function(){
                if(!$('.columna2').is(':visible')){
                    $('.columna2').delay('100').fadeIn();
                    var width = $(window).width(), height = $(window).height();
                    if ((width > 1200) ) {
                        $('.columna1').animate({width:'31.5%'},300)
                    }else{
                        $('.columna1').animate({width:'20%'},300)
                    }
                    
                }
            });
            
            $('.h5-back').live('click',function(){
                if($('.columna2').is(':visible')){
                    $('.columna2').fadeOut(100);
                    $('.columna1').animate({width:'95%'},350)
                    var isopen = $('.isOpen');
                    isopen.removeClass('isOpen');
                    isopen.addClass('isClose');
                    $('#'+isopen.attr('data-div')).fadeOut();
                }
            });
        });
        
    </script>
  </body>
</html>

<?php  $this->load->view('globales/footer'); ?>