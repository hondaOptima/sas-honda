
 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Multipuntos Honda Optima</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="<?php echo base_url();?>assets/css/seminuevos/seminuevo.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 
  </head>
  <body>
    <!-- Aqui va todo  -->
    <form action="seminuevos/guardar" method="post" >
        <div id="semi-bar" class=" ">
            <div id="menu-bars">
                <i class="fa fa-bars fa-2x"></i>
            </div>
            <div class="div-cliente">
                <h4>Cliente: <?php echo $infoVin[0]->sec_nombre; ?></h4>
            </div>
            <div class="div-title-diag text-center">
                 <!--Boton para enviar formulario-->
                    <li class="pull-right" style="margin-top:-8px;">
                        <!--input type="submit" value="Enviar" class"btn"-->
                        <button id="btn-enviar" class="btn btn-warning ">Enviar</button>                   

                    </li>

                    <!-- fin boton-->
                <h2>
                    Lista / Orden De Servicio De Inspecion
                </h2>

            </div>
        
        </div>

    <div class="col-md-7">
        <!-- Aqui comienza la columna 1-->
        <div class="columna columna1">
            <div class="first-col-div-titles">
                <h4 class="first-col-h3">No De Serie: </h4>
                <input type="text" name="vin" class="form-control first-col-inp" value="<? echo $infoVin[0]->sev_vin; ?>">
            </div>
            <div class="first-col-div-titles">
                <h4 class="first-col-h3">Modelo: </h4>
                <input type="text" name="modelo" class="form-control first-col-inp" value="<? echo $infoVin[0]->sev_marca." ".$infoVin[0]->sev_modelo; ?>">
            </div>
            <div class="first-col-div-titles">
                <h4 class="first-col-h3">Kilometraje: </h4>
                <input type="text" name="kilometraje" class="form-control first-col-inp" value="<? echo $infoVin[0]->sev_km_recepcion; ?>">
            </div>
            <div class="first-col-div-titles">
                <h4 class="first-col-h3">Color: </h4>
                <input type="text" name="color" class="form-control first-col-inp" value="<? echo $infoVin[0]->sev_color; ?>">
            </div>
            <div class="first-col-div-titles">
                <h4 class="first-col-h3">A&ntilde;o: </h4>
                <input type="text" name="anio" class="form-control first-col-inp" value="<? echo $infoVin[0]->sev_modelo; ?>">
            </div>
            <div class="first-col-div-titles">
                <h4 class="first-col-h3">Placas: </h4>
                <input type="text" name="placas" class="form-control first-col-inp" value="<? echo $infoVin[0]->sev_placas; ?>">
            </div>
        </div>
	<!-- Fin de la columna 1-->
    <!-- Aqui comienza la columna 2-->
	   <div class="columna columna2">
            <div  class="fctitles col-md-12">
                <h3 class="text-center title-fctitle">Inspeccion De Apariencia</h3>
            </div>
                <table class="table table-bordered table-second-col">
                    <tr class="table-th">
                        <td class="th-exterior">Exterior   </td>
                        <td id="selector">Cumple con<br> estandar<br> honda</td>
                        <td id="selector">Requiere<br> servicio</td>
                        <td class="rep-cost">Costo de<br> reparacion estimado</td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Debajo del cofre (compartimiento del motor limpio)</td>
                        <td><input type="checkbox" name="check1"></td>
                        <td><input type="checkbox" name="check2"></td>
                        <td><input type="number" class="check-table form-control" name="debajoCofre"></td>
                    </tr>
                    <tr class="table-tr">
                            <td class="td-txt-exterior">Defensa delantera (fascia, guardas, acabado)</td>
                            <td><input type="checkbox" name="check3"></td>
                            <td><input type="checkbox" name="check4"></td>
                            <td><input type="number" class="check-table form-control" name="defensaDelantera"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Parrilla (faros, otras luces, ornamentos, emblemas)</td>
                        <td><input type="checkbox" name="check5"></td>
                        <td><input type="checkbox" name="check6"></td>
                        <td><input type="number" class="check-table form-control" name="parrilla"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Cofre (emblemas, acabado)</td>
                        <td><input type="checkbox" name="check7"></td>
                        <td><input type="checkbox" name="check8"></td>
                        <td><input type="number" class="check-table form-control" name="cofre"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Salpicadera delantera izquierda (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check9"></td>
                        <td><input type="checkbox" name="check10"></td>
                        <td><input type="number" class="check-table form-control" name="salpicaderaDelanteraIzquierda"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Puerta delantera izquierda (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check11"></td>
                        <td><input type="checkbox" name="check12"></td>
                        <td><input type="number" class="check-table form-control" name="puertaDelanteraIzquierda"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Puerta trasera izquierda (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check13"></td>
                        <td><input type="checkbox" name="check14"></td>
                        <td><input type="number" class="check-table form-control" name="puertaTraseraIzquierda"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Salpicadera trasera izquierda (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check15"></td>
                        <td><input type="checkbox" name="check16"></td>
                        <td><input type="number" class="check-table form-control" name="SalpicaderaTraseraIzquierda"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Cajuela (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check17"></td>
                        <td><input type="checkbox" name="check18"></td>
                        <td><input type="number" class="check-table form-control" name="cajuela"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Defensa trasera (fascia, guardas, acabado)</td>
                        <td><input type="checkbox" name="check19"></td>
                        <td><input type="checkbox" name="check20"></td>
                        <td><input type="number" class="check-table form-control" name="defensaTrasera"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Salpicadera trasera derecha (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check21"></td>
                        <td><input type="checkbox" name="check22"></td>
                        <td><input type="number" class="check-table form-control" name="salpicaderaTraseraDerecha"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Puerta trasera derecha (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check23"></td>
                        <td><input type="checkbox" name="check24"></td>
                        <td><input type="number" class="check-table form-control" name="puertaTraseraDerecha"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Puerta delantera derecha (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check25"></td>
                        <td><input type="checkbox" name="check26"></td>
                        <td><input type="number" class="check-table form-control" name="puertaDelanteraDerecha"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Salpicadera delantera derecha (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check27"></td>
                        <td><input type="checkbox" name="check28"></td>
                        <td><input type="number" class="check-table form-control" name="salpicaderaDelanteraDerecha"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Techo (acabado, ornamentos)</td>
                        <td><input type="checkbox" name="check29"></td>
                        <td><input type="checkbox" name="check30"></td>
                        <td><input type="number" class="check-table form-control" name="techo"></td>
                    </tr>
                    <tr class="table-tr">
                        <td class="td-txt-exterior">Vidrios (grietas, rayones, despostilladuras, picaduras)</td>
                        <td><input type="checkbox" name="check31"></td>
                        <td><input type="checkbox" name="check32"></td>
                        <td><input type="number" class="check-table form-control" name="vidrios"></td>
                    </tr>
                </table>
 
        </div>
    <!-- Fin de la columna 2-->
		</DIV>
    <!-- Aqui comienza class="col-md-5"-->
		<div class="col-md-5">
    <!-- Aqui comienza la columna 3-->
			<div class="column3 columna">
				<canvas id="canvas" name="img" class="canvas" ></canvas>
				    <div>
    					<div class="div-info-canvas1">
    						<label class="canvas-label">Fecha: </label>
    						<input type="date" id="fecha" name="fec" class="form-control canvas-date">
    					</div>
    					<div class="div-info-canvas2">
    						<label class="canvas-label">Torre: </label>
    						<input type="number" class="form-control canvas-torre" name="torre" value="<? echo $infoVin[0]->seo_tower; ?>">
    					</div>
                            <button id="btn-limpiar" class="btn btn-warning " onclick="limpiarFirma()">Limpiar</button>                         
				</div>
            </div><!-- Fin de la columna 3-->
    <!-- Aqui comienza la columna 4-->
			<div class="column4 columna">	
				<div class="costo-total">
                    <h4 class="h4-total">Costo Total De Reparcion De Apariencia</h4><br>
                        <input placeholder="$ 0.00"class="inp-total form-control" name="costo" type="number">
                </div>
                <textarea name="nota" id=""  class="form-control costo-ta" placeholder="Notas: (cualquier elemento no cubierto en esta lista)"></textarea>
			</div><!-- Fin de la columna 4-->
		</div><!-- Fin de class="col-md-5"-->
     </form>
    <!-- Aqui se termina todo  -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!--<script src="<?php echo base_url();?>js/seminuevos/seminuevo.js"></script>-->
   <script src="<?php echo base_url();?>signature/jquery.jqscribble.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>signature/jqscribble.extrabrushes.js" type="text/javascript"></script>
    
  </body>
</html>
<script type="text/javascript">
    
        document.getElementById("fecha").valueAsDate = new Date();
        $("#canvas").jqScribble();
		
		var canvasOBJ = document.getElementById('canvas');
		var alto = canvas.clientHeight;
		
        $("#canvas").data("jqScribble").update({brushColor:"red",width:"220", height:alto,backgroundImageX:'45',backgroundImageY:'40'});
        $(document).ready(function(){
            limpiarFirma();
        });  
            
        function limpiarFirma(){
			var canvas = $('#canvas')[0]; // or document.getElementById('canvas');
            var context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);
            $('#canvas').css("background-image", "url(http://hsas.gpoptima.net/assets/images/imagen.jpg)");
		}
    </script>
    <?php  $this->load->view('globales/footer'); ?>
    <?php $this->load->view('orden/js/script'); ?>
    <script src="<?php echo base_url();?>appointments/jquery.jqscribble.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>appointments/jqscribble.extrabrushes.js" type="text/javascript"></script>
        <script type="text/javascript">
        /*Funcion para aguardar la imagen de appointments */
        function save()
        {
          $("#canvas").data("jqScribble").save(function(imageData)
          {
            $('.canvas').html('Guardando Imagen ...');
              $.post('<?php echo base_url();?>appointments/save_imApp.php?ido=<?php echo $orden[0]->sem_idOrden;?>', {imagedata: imageData}, function(response)
              {
                            console.log('probando ruta');
                $('.canvas').html('Imagen Guardada !');
              }); 
            
          });
        }/*Fin de appointments */
    </script>

    
