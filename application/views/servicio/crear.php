<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>servicio">Servicios</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-edit"></i> <a href="<?php echo base_url();?>servicio/crear">Nuevo</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-reorder"></i>Nuevo Servicio </div>
  </div>
  <div class="portlet-body form"> 
    <!-- BEGIN FORM--> 
    
    <?php echo form_open('servicio/crear','class="form-horizontal"'); ?>
    <div class="form-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Tipo de Servicio</label>
            <div class="col-md-9">
              <?php 
																$tiposServicio=array(
																'pro'=>'Programados',
																'nopro'=>'No Programados',																
																);
																 echo form_dropdown('tipoServicio', $tiposServicio, '1',
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen" required');
																?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3">Servicio</label>
            <div class="col-md-9">
              <?php 
																 echo form_dropdown('servicio', $servicio, '1',
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen" required');
																?>
            </div>
          </div>
        </div>
      </div>
      <!--/row--> 
      
      <!--/row-->
      <div class="row">
        <div class="col-md-6">
          <div class="form-group last">
            <label class="control-label col-md-3"><b>Modelo</b></label>
            <div class="col-md-3"> <b>Tiempo Aproximado</b> </div>
            
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <?php $x=0; foreach($modelos as $m){ $x++;?>
          <input type="hidden" name="modelo[]" value="<?php echo $m->vem_idVehicleModel;?>">
          
          <div class="form-group last">
            <label class="control-label col-md-3"><?php echo $m->vem_name?></label>
            <div class="col-md-3">
              <input type="text" name="hora[]" value="0">
            </div>
            
          </div>
          <?php } ?>
          <input type="hidden" name="num" value="<?php echo $x;?>">          
        </div>
        <!--/span--> 
        
      </div>
    </div>
  </div>
  <div class="form-actions fluid">
    <div class="row">
      <div class="col-md-6">
        <div class="col-md-offset-3 col-md-9">
          <button type="submit" class="btn btn-success">Añadir</button>
          <a href="<?php echo base_url();?>servicio">
          <button type="button" class="btn btn-default">Cancelar</button>
          </a> </div>
      </div>
      <div class="col-md-6"> </div>
    </div>
  </div>
  <?php echo form_close(); ?> 
  <!-- END FORM--> 
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('servicio/js/script');?>
<script>

$(document).ready(function() {
    $('#selecctall').live('click',function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});

</script>