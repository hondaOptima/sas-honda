<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>	
<ul class="page-breadcrumb breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="<?php echo base_url();?>pizarron">Dashboard</a>
		<i class="fa fa-angle-right"></i>
	</li>
	<li>
		<i class="fa fa-wrench"></i>
		<a href="<?php echo base_url();?>servicio">Servicios</a>
		<i class="fa fa-angle-right"></i>
	</li>
	<li>
		<i class="fa fa-edit"></i>
		<a href="#">Editar</a>
		<i class="fa fa-angle-right"></i>
	</li>
</ul>   
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
	<div class="portlet-title">
		<div class="caption">
		<i class="fa fa-reorder"></i>Editar Servicio
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->


		<?php if($servicio):?>

		
		<?php echo form_open('servicio/actualizar/','class="form-horizontal"'); ?>
		<div class="form-body">
			<h3 class="form-section">Información de Servicio de <?php echo $servicio[0]->set_name;?></h3>
            
        <div class="row">
                    <table><tr><td>
						<label class="control-label col-md-4"><b>Modelo</b></label></td>
                        <td>
						<label class="control-label col-md-6"><b>Tiempo Aproximado</b></label></td>
                        <td>
						<label class="control-label col-md-6"><b>Mano de Obra</b></label></td><td>
                        <label class="control-label col-md-6"><b>Refacciones</b></label></td>
                        <td><label class="control-label col-md-4"><b>Mat. Varios</b></label></td>
                        <td><label class="control-label col-md-6"><b>Precio Comercial</b></label></td><td>
                        <label class="control-label col-md-2"><b>Status</b></label></td>
                        </tr>
					
            <?php foreach($servicio as $s): ?>
			
				<tr><td>
						<label class="control-label col-md-4"><?php echo $s->vem_name; ?></label></td>
                        
<td>                        
 <input type="text" name="duracion" id="<?php echo $s->ser_idService; ?>"  value="<?php echo $s->ser_approximate_duration;?>" maxlength="8" class="form-control addtime" placeholder="Duracion Aproximada" required>
</td>

<td>						
 <input type="text" name="price" id="<?php echo $s->ser_idService; ?>"  value="<?php echo $s->ser_price;?>" maxlength="8" class="form-control addprice" placeholder="Monto" required>
</td>

<td>						
 <input type="text" name="refac" id="<?php echo $s->ser_idService; ?>"  value="<?php echo $s->ser_refacciones;?>" maxlength="8" class="form-control addrefac" placeholder="Refacciones" required>
</td>

<td>						
 <input type="text" name="materiales" id="<?php echo $s->ser_idService; ?>"  value="<?php echo $s->ser_mat_varios;?>" maxlength="8" class="form-control addmateriales" placeholder="Materiales Varios" required>
</td>

<td>						
 <input type="text" name="comercial" id="<?php echo $s->ser_idService; ?>"  value="<?php echo $s->ser_comercial;?>" maxlength="8" class="form-control addcomercial" placeholder="Precio comercial" required>
</td>

<td>						
<input type="checkbox" class="addstatus" id="<?php echo $s->ser_idService; ?>" <?php if($s->ser_isActive==1){echo 'checked="checked"';}?>>
</td>
</tr>
 
			
			
		<?php endforeach ?>
        </table>
		</div></div>
		<div class="form-actions fluid">
			<div class="row">
				<div class="col-md-6">
					<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn btn-success">Guardar</button>
					<a href="<?php echo base_url();?>servicio"><button type="button" class="btn btn-default">Cancelar</button></a>
					</div>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>
<?php echo form_close(); ?>
		
		<?php else: ?>None<?php endif ?>
		<!-- END FORM-->
	</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('servicio/js/script');?> 
<script>
jQuery(document).ready(function() {    
 $('.addtime').live('keyup',function(){
var id=$(this).attr('id');
var val=$(this).attr('value');	
$.post("<?php echo base_url(); ?>servicio/update",{id:id,valor:val,tipo:"time"}, function(data) {}); 

});

 $('.addprice').live('keyup',function(){
var id=$(this).attr('id');
var val=$(this).attr('value');	
$.post("<?php echo base_url(); ?>servicio/update",{id:id,valor:val,tipo:"price"}, function(data) {}); 

}); 

$('.addrefac').live('keyup',function(){
var id=$(this).attr('id');
var val=$(this).attr('value');	
$.post("<?php echo base_url(); ?>servicio/update",{id:id,valor:val,tipo:"refac"}, function(data) {}); 

});

$('.addmateriales').live('keyup',function(){
var id=$(this).attr('id');
var val=$(this).attr('value');	
$.post("<?php echo base_url(); ?>servicio/update",{id:id,valor:val,tipo:"materiales"}, function(data) {}); 

}); 


$('.addcomercial').live('keyup',function(){
var id=$(this).attr('id');
var val=$(this).attr('value');	
$.post("<?php echo base_url(); ?>servicio/update",{id:id,valor:val,tipo:"comercial"}, function(data) {}); 

}); 

$('.addstatus').live('click',function(){	
var mcCbxCheck = $(this);
var id=$(this).attr('id');
if(mcCbxCheck.is(':checked')) {
$.post("<?php echo base_url(); ?>servicio/updateSta",{id:id,valor:'1'}, function(data) { });
    }
    else{
$.post("<?php echo base_url(); ?>servicio/updateSta",{id:id,valor:'2'}, function(data) { });
    }
	
});
  
   
});
</script>
