<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<form method="get" action="<?php echo base_url();?>setupcalendario">
  <ul class="page-breadcrumb breadcrumb">
    <li> <i class="fa fa-calendar"></i> <a href="<?php echo base_url();?>cita">Configuracion de Citas en el calendario de <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?></a> </li>
    <li class="pull-right">
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    </li>
  </ul>
</form>


<?php echo $flash_message; ?>
<div class="clearfix"> </div>
<div class="row">
  <div class="col-md-4 col-sm-4">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-calendar"></i>Calendario </div>
        <div class="tools"> <span class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on"><span class="fc-text-arrow"><a style="color:white" href="<?php echo base_url()?>setupcalendario?date=<?php echo $ant;?>&taller=<?php echo $taller;?>"><i class="fa fa-toggle-left"></i></a></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a style="color:white" href="<?php  echo base_url()?>setupcalendario?date=<?php echo $sig;?>&taller=<?php echo $taller;?>"><i class="fa  fa-toggle-right"></i></a></span></span> </div>
      </div>
      <div class="portlet-body">
        <div id="calendarB"> </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"><i class="fa fa-clock-o"></i><b>Lista de Horas del <?php echo $di.' '.$meses[date($ms)].' del '.$an;?> </b> </div>
      </div>
      <div class="portlet-body">
        <table class="table table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold; font-size:11px;">
              <th style="text-align:center"> <b> </b> </th>
              <th style=" width:45px; text-align:center"> <b>Hora</b> </th>
              <th> <b>Cliente</b> </th>
              <th> <b>Servicio</b> </th>
            </tr>
          </thead>
          <?php  foreach ($lista as $cita) { 
					
						 
									if($cita->cal_status=='2'){
										$clas='warning';
										$txt='ban';
										$url='';
										}else{
											$clas='info';
											$txt='edit';
$url="".base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."#tabcon";
											}
											if($cita->cal_status==2){$bg='#3cc051';}
											else{ $bg='';}
									
									?>
          <tr  >
            <td style="text-align:center"><?php if(empty($cita->set_name)){?>
              <input style="cursor:pointer" type="checkbox" name="rhora" id="<?php echo $cita->cal_ID?>" class="checkhora">
              <?php } ?></td>
            <td style="text-align:center"><?php echo $cita->cal_hora?></td>
            <td><?php echo $cita->app_customerName.' '.$cita->app_customerName; ?></td>
            <td><?php echo $cita->set_name; ?></td>
          </tr>
          <?php  } ?>
            </tbody>
          
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-calendar"></i>Acciones </div>
      </div>
      <div class="portlet-body">
        <ul style="list-style:none">
          <li>
            <input type="radio" style="cursor:pointer" name="opcion" class="festivo" value="eliminarhora">
            Marcar como Festivo</li>
          <li>
            <input type="radio" style="cursor:pointer" name="opcion" class="ahora" value="eliminarhora">
            Agregar hora</li>
          <li>
            <input type="radio" style="cursor:pointer" name="opcion" class="bloquear" value="eliminarhora">
            Bloquear  hora</li>
          <li>
            <input type="radio" style="cursor:pointer" name="opcion" class="infinito" value="eliminarhora">
            Modificar horas infinito</li>
          <li>
            <input type="radio" style="cursor:pointer" name="opcion" class="horasfecha" value="eliminarhora">
            Agregar horas a una fecha no seleccionada</li>
          <li></li>
          <li id="festivo" style="display:none"><br>
            <br>
            <a href="<?php echo base_url()?>setupcalendario/diaOcupado/<?php echo $date;?>/<?php echo $taller;?>" class="btn btn-primary btn-block">Siguiente</a></li>
             <li id="infinito" style="display:none"><br>
            <br>
            <a href="<?php echo base_url()?>setupcalendario/infinito/<?php echo $date;?>/<?php echo $taller;?>" class="btn btn-primary btn-block">Siguiente</a></li>
             <li id="bloquear" style="display:none"><br>
            <br>
            <input type="hidden" name="blohora" value="0" >
            <div   class="btn btn-primary btn-block bloquearhora">Siguiente</div></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4" id="ahora" style="display:none">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-calendar"></i>Seleccione la hora a agregar </div>
      </div>
      <div class="portlet-body">
         <?php echo form_open('setupcalendario/agregarHora/'.$date.'/'.$taller.'/','class="form-horizontal"'); ?>
        <ul style="list-style:none">
          
          <li style=" font-weight:bold">Hora:<br>
            <select name="nhora">
            <?php for($h=7; $h<24; $h++) { if($h<10){$h='0'.$h;}else{}?>
            <option value="<?php echo $h;?>"><?php echo $h;?></option>
            <?php } ?>
            </select>
          </li>
          <li style=" font-weight:bold">Minuto:<br>
            <select name="nmin">
            <?php for($m=0; $m<60; $m++) { if($m<10){$m='0'.$m;}else{}?>
            <option value="<?php echo $m;?>"><?php echo $m;?></option>
            <?php } ?>
            </select>
          </li>
          <li><br>
            <br>
            <input type="submit" class="btn btn-primary btn-block" value="Siguiente"></li>
        </ul>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4" id="horasfecha" style="display:none">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-calendar"></i>Seleccione la Fecha y el Formato de Horas</div>
      </div>
      <div class="portlet-body">
        <ul style="list-style:none">
        <li>Fecha:
            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
												<input type="text" name="fechano" class="form-control" readonly>
												<span class="input-group-btn">
													<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div></li>
          <li>
          <li>
            <input type="radio" style="cursor:pointer" name="opcionxxx" value="1">
            Formato de Horas de Lunes a Viernes</li>
          <li>
            <input type="radio" style="cursor:pointer" name="opcionxxx" value="2">
            Formato de Horas dia Sabado</li>
          <li><br>
            <br>
            <div class="loading" style="display:none"> Guardando...<img src="<?php echo base_url()?>assets/img/input-spinner.gif"></div>
            <div class="btn btn-primary btn-block savefechano">Siguiente</div></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>

<!-- BEGIN EXAMPLE TABLE PORTLET--> 
<!-- END EXAMPLE TABLE PORTLET--> 

<!-- Factor de produccion 100 % del taller en numero de horas--> 

<!-- Factor de produccion 100 % del taller en numero de horas gastadas-->

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('setupcalendario/js/script');?>
<script>
	
jQuery(document).ready(function() {

$('.festivo').live('click',function(){
$('#festivo').show();
$('#ahora').hide();	
$('#bloquear').hide();
$('#infinito').hide();	
$('#horasfecha').hide();		
	});

$('.ahora').live('click',function(){
$('#festivo').hide();
$('#bloquear').hide();
$('#ahora').show();
$('#infinito').hide();	
$('#horasfecha').hide();
	});	
	
$('.bloquear').live('click',function(){
$('#festivo').hide();
$('#bloquear').show();
$('#ahora').hide();
$('#infinito').hide();	
$('#horasfecha').hide();
	});		
	
	$('.infinito').live('click',function(){
$('#festivo').hide();
$('#bloquear').hide();
$('#ahora').hide();
$('#infinito').show();
$('#horasfecha').hide();	
	});	
	
	$('.horasfecha').live('click',function(){
$('#festivo').hide();
$('#bloquear').hide();
$('#ahora').hide();
$('#infinito').hide();
$('#horasfecha').show();	
	});		
	


$('.bloquearhora').live('click',function(){
var info=$('input[name=blohora]').val();
if(info==0){
alert('Seleciona una hora');	
	}
else{
window.location="<?php echo base_url()?>setupcalendario/eliminarHora/?date=<?php echo $date;?>&taller="+<?php echo $taller;?>+"&info="+info+"";	
	}		
	});
	
	
	
	
	$('.savefechano').live('click',function(){
var info=$('input[name=fechano]').val();
var opcion = jQuery("input[name=opcionxxx]:checked").val();
var booleanVlaueIsChecked=false;

if(info==''){
alert('Seleciona una fehca');	
	}
else{
if (opcion){
$('.savefechano').hide();
$('.loading').show();	
	
 $.ajax({
	url:"<?php echo base_url();?>setupcalendario/agregarHorasCalendario/?date=<?php echo $date;?>&taller=<?php echo $taller;?>&fechano="+info+"&formato="+opcion+"",
    success:function(result){
    if(result=='si'){
		alert('La fecha seleccionada ya contiene horas /n Seleccione otra fecha ');
		$('.savefechano').show();
		$('.loading').hide();	}
	else{
	
	window.location="<?php echo base_url()?>setupcalendario?date=<?php echo $date;?>&taller="+<?php echo $taller;?>+""
		
		}
  }});
	}
else{	
alert('Seleccione un Formato de hora');
    }
	}		
	});
	
	
	
	
	$('.checkhora').live('click',function(){
var info=$(this).attr('id');
    $('input[name=blohora]').val(info);	
	});
	
$('.agendar').live('click',function(){ 
var id=$(this).attr('id');
$('#wide').modal('show');    
$('input[name=fecha]').val(id);
});	
	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
			
                disableDragging: true,
                header: h,
				dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
   dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
				
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>setupcalendario/porcentajeCita?fecha=<?php echo $date;?>&taller=<?php echo $taller;?>', // use the `url` property
           
        }


    ],
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
			
			
});
</script>