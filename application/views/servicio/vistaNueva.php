<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>servicio">Servicios</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<?php echo $flash_message;?> 

<!-- <?php foreach ( $result as $elem) {
  echo $elem[0][1][0]->ser_idService;
} ?> -->

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa fa-wrench"></i>Servicios de Mantenimiento Programados </div>
    <div class="actions">
      <div class="btn-group"> <a class="btn btn-info btn-sm dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Acciones <i class="fa fa-angle-down"></i> </a>
        <ul class="dropdown-menu pull-right">
          <li> <a href="<?php echo base_url();?>modelovehiculo" >Lista de Modelos</a> </li>
          <li> <a href="<?php echo base_url();?>tiposervicio" >Lista de Servicios</a> </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="portlet-body table-responsive2">
    <table class="table table-bordered " >
      <thead>
        <tr style="font-weight:bold">
          <th style="width:200px;"><center>
              <b>MODELOS</b>
            </center></th>
          <?php $ncolp=count($colPro)-1; for($t=0; $t<=$ncolp; $t++){?>
          <th><center>
              <a href="<?php echo base_url()?>servicio/editar/<?php echo $colPro[$t]->set_idServiceType; ?>"> <b><?php echo $colPro[$t]->set_name;?></b> </a>
            </center></th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <tr>
          <?php 
            foreach ($result as $elem) {
                echo "<tr class='tro'>";
                    echo "<td class='td-title-modelo'>".$elem[0][0]."</td>";
                    foreach ($elem[0][1] as $proElem) {
                        echo "<td style='text-align:center; font-size:1.2em;padding:3px;'> $".$proElem->ser_comercial."</td>";
                    }
                echo "</tr>";
            }
        ?>
      </tbody>
    </table>
  </div>
</div>
<div class="portlet">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa fa-wrench"></i>Servicios de Mantenimiento No Programados </div>
  </div>
  <div class="portlet-body table-responsive2">
    <table class="table table-bordered " >
      <thead>
        <tr style="font-weight:bold">
          <th ><center>
              <b style="margin: 50px;">MODELOS</b>
            </center></th>
          <?php 
							$cts=count($tservicios);
							$cts=$cts -1;
							$ncolpro=count($colNopro)-1;
							for($t=0; $t<=$ncolpro; $t++){?>
          <th title="<?php echo $colNopro[$t]->set_name;?>"><center>
              <a href="<?php echo base_url()?>servicio/editar/<?php echo $colNopro[$t]->set_idServiceType; ?>"> <b><?php echo strtoupper($colNopro[$t]->set_codigo);?></b> </a>
            </center></th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php 
            foreach ($result as $elem) {
                echo "<tr class='tro'>";
                    echo "<td >".$elem[0][0]."</td>";
                    foreach ($elem[0][2] as $proElem) {
                        echo "<td style='text-align:center; font-size:1.2em;padding:3px;'> <div style='margin:5px;'>$".$proElem->ser_comercial."</div></td>";
                    }
                echo "</tr>";
            }
        ?>
      </tbody>
    </table>
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->

<style>


    .tro:hover{
        background: rgba(46, 138, 138, 0.5) !important;
    }

   .table-responsive2 {
        width: 100%;
        margin-bottom: 15px;
        overflow-x: auto;
        overflow-y: hidden;
        -webkit-overflow-scrolling: touch;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        border: 1px solid #DDD;
    }
</style>

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('servicio/js/script');?>
