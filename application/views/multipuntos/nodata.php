

 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Multipuntos Honda Optima</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="<?php echo base_url();?>assets/css/multi/multi.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <style>
        .nodata{
            width: 95%;
            height: 100px;
            border-radius: 4px;
            background: rgba(232, 41, 42, 1);
            border:2px solid rgba(126, 128, 128, 1);
            color:white;
            font-size: 1.4em;
            padding-top: 20px;
            text-align: center;
            margin-left: 2.5%;
            margin-top: 30px;
            position:fixed;
            z-index: 1;
            top:20px;
        }
    </style>
  </head>
  <body>
      <div class="nodata">
          INFORMACION NO DISPONIBLE PARA ESTE SERVICIO.
      </div>
  </body>
