<!DOCTYPE html>
<html lang="en">

<header>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
</header>

<style>
    .navbar{
        border-radius:0 !important;
        height: 50px;
        text-align:center !important;
    }

    .navbar-brand{
        font-size:2em;
        color: white !important;
        text-align: center !important;
    }
    .btn-m{
        width:33%;
        margin-top:1px;
        margin-bottom:10px;
        margin-left:1%;
        height:45px;
        padding:10px;
        float: left;
        color:rgba(95, 92, 91, 1);
        font-size: 0.8em !important;
        border-radius:0 !important;
    }

    .btn-c{
        width:33%;
        margin-top:1px;
        margin-bottom:10px;
        height:45px;
        padding:10px;
        float: left;
        color:rgba(95, 92, 91, 1);
        font-size: 0.8em !important;
        border-radius:0 !important;
    }

    .navbar-header{
      text-align: center !important;
    }

    #frame,#frame2{
      width: 100%;
      height: 400px;
      display: none;
    }

</style>
<nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">ORDEN: <? echo $idOrden; ?></a>
            </div>
        </div>
</nav>

<body onload="fill_table()">
    <div id="f-div" class="btn-m btn btn-default" onclick="multipuntos()">
        MULTIPUNTOS
        <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>
    </div>
    <div id="f-div" class="btn-c btn btn-default" onclick="">
        FACTURA
        <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>
    </div>

    <table class="table table-hover table-inverse">
  <thead>
    <tr style="background:rgba(68, 68, 64, 1);color:white;font-size: 0.9em !important;" onclick="location.href='http://hsas.gpoptima.net/documents/estimates/b095e82dea.pdf'">
      <th>Servicio </th>
      <th>Mano de obra</th>
      <th>Refacciones</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody id="tbody" style="font-size: 1em !important;">
  </tbody>
</table>

<iframe src="http://hsas.gpoptima.net/multipuntos/index2/<? echo $idOrden; ?>" id="frame" style="display:none" ></iframe>
<iframe id="frame2" style="display:none" ></iframe>

</body>
<script>
    
   function fill_table(){
       var tbody = $('#tbody');
       $.get('http://hsas.gpoptima.net/app/api/getCotizaciones.php?ORDEN=14', function(data){
           var json = JSON.parse(data);
           if(json.Id != null){
               for(var i = 0; i < json.Childs.length; i++){
                   var new_tr = "<tr style='font-size: 0.9em !important;'>"+
                                    "<td >"+json.Childs[i].set_name+"</td>"+
                                    "<td>$"+json.Childs[i].ess_handwork+"</td>"+
                                    "<td>$"+json.Childs[i].ess_unit_price+"</td>"+
                                    "<td>$"+json.Childs[i].ess_total_price+"</td>"+
                                "</tr>";
                    tbody.append(new_tr);
               }
               $('#f-div').after('<div class="btn-c btn btn-default" onclick="cotizacion(\''+json.File+'\')"> COTIZACION <i class="fa fa-download" aria-hidden="true"></i></div>');
           }else{
               $('#f-div').after('<div class="btn-c btn btn-default" disabled> COTIZACION <i class="fa fa-download" aria-hidden="true"></i></div>');
           }
       });
   }


   function multipuntos(url){
      $('.table, #frame2').fadeOut(80,function(){
        $('#frame').fadeIn(100);
      });
    }

    function cotizacion(file){
      var url = 'http://hsas.gpoptima.net/'+file;
      window.open(url,"_system")
      $('#frame').fadeOut(100,function(){
        $('.table').fadeIn(100);
      });
    }
</script>


