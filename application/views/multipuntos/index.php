<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Multipuntos Honda Optima</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="<?php echo base_url();?>assets/css/multi/multi.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
<!-- Aqui va todo  -->
<div id="modal">
  <p></p>
  <p class="modalfijo">Click para cerrar</p>
</div>
<div id="multi-bar" class=" ">
  <div class="div-contador">
    <div class="cont-1 cont">
      <label id="circle1">0</label>
    </div>
    <div class="cont-2 cont">
      <label id="circle2">0</label>
    </div>
    <div class="cont-3 cont">
      <label id="circle3">0</label>
    </div>
  </div>
  <div id="menu-bars" > <i class="fa fa-bars fa-2x"></i> </div>
  <div class="div-container-lbl-cont">
    <div class="first-lbl-cont div-lbl-cont">
      <label class="lbl-cont">OK</label>
    </div>
    <div class="div-lbl-cont">
      <label class="lbl-cont">Necesita servicio</label>
    </div>
    <div class="div-lbl-cont">
      <label class="lbl-cont">Atencion inmediata</label>
    </div>
  </div>
  <div class="div-title-diag text-center">
    <h2> <h3>Hoja de diagnostico</h3>
      <?php if(($_SESSION['sfrol']==3 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==1) && $cliente == 0) {?>
      <div orden="<?php echo $idOrden; ?>" id="sendMultipuntos" style="float:right;margin-right:20px; !important;" class="btn btn-success">ENVIAR</div>
      <div id="enviando" style="color:rgba(165, 228, 255, 1);font-size:0.8em;display:none;float:right;">ENVIANDO ...</div>
      <?php } ?>
    </h2>
  </div>

</div>
<div class="columna columna1">
  <div id="div-interior" class="firstcol col-md-12">
    <div  class="fctitles col-md-12">
      <h4 class="text-center">Interior / Exterior</h4>
    </div>
    <div class="semaforos">
      <input type="hidden" id="inputOrden" orden="<?php echo $idOrden; ?>" />
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M1->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 opa-i <?php echo $M1->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 opa-i <?php echo $M1->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Luz de tablero
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate " id="mul_luces"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M2->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 opa-i <?php echo $M2->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 opa-i <?php echo $M2->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Luces, luces traseras, direccionales,<br>
        stops, luces de emergencia, cuartos.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_parabrisas"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M3->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 opa-i <?php echo $M3->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 opa-i <?php echo $M3->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Operacion de limpiaparabrisas,<br>
        plumas, liquido lipiaparabrisas.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate"  id="mul_frenomano"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M4->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 opa-i <?php echo $M4->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 opa-i <?php echo $M4->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Freno de mano.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate"  id="mul_claxon"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M5->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 opa-i <?php echo $M5->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 opa-i <?php echo $M5->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Operacion del claxon.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_clutch"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M6->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 opa-i <?php echo $M6->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 opa-i <?php echo $M6->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Operacion del clutch o embrague.
        </h4>
      </div>
      <div class="fctitles ">
        <h4 class="text-center">Debajo de la unidad</h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_gomas"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M17->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M17->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M17->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Gomas de suspension.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_basesamortiguadores"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M18->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M18->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M18->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Bases de amortiguadores.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_terminalesdirecion"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M19->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M19->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M19->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Terminales de direccion.
        </h4>
      </div>
      <div class="div-semaforo">
        <div class="semaforo unvalidate" id="mul_amortiguadores"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M20->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M20->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M20->rojo; ?>"></i></div>
        <h5 class="lbl-semaforo">
        Amortiguadores.
        </h4>
      </div>
    </div>
  </div>
</div>
<div class="columna columna2">
  <div id="div-debajocofre" class="firstcol col-md-12">
    <div class="fctitles col-md-12">
      <h4 class="text-center">Debajo del cofre</h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_soportes"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M10->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M10->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M10->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Soportes de motor.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_liquidos"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M11->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M11->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M11->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Liquidos: aceite, anticongelante,<br>
      direccion, transmision, frenos.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_radiadores"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M12->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M12->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M12->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Radiadores, mangueras, bandas.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_ajustefreno"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M13->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M13->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M13->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Ajuste de freno de mano, lineas.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_flechas"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M14->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M14->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M14->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Flechas y juntas homocineticas.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_escape"><i class="fa fa-check-circle fa-3x circle1 opa-i <?php echo $M15->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M15->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M15->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Sistema de escape.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_fugas"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M16->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M16->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M16->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Fugas de aceite.
      </h4>
    </div>
  </div>
  <div id="div-debajo-unidad" class=" col-md-12">
    <div class="fctitles col-md-12">
      <h4 class="text-center">Funcionamiento de la Bateria</h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_electrolito"><i class="fa fa-calendar-check-o fa-3x circle1 opa-i <?php echo $M7->verde; ?>"></i><i class="fa fa-calendar-check-o fa-3x circle2 opa-i <?php echo $M7->amarillo; ?>"></i><i class="fa fa-calendar-check-o fa-3x circle3 opa-i <?php echo $M7->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Nivel de electrolito.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_terminales"><i class="fa fa-calendar-check-o fa-3x circle1 opa-i <?php echo $M8->verde; ?>"></i><i class="fa fa-calendar-check-o fa-3x circle2 opa-i <?php echo $M8->amarillo; ?>"></i><i class="fa fa-calendar-check-o fa-3x circle3 opa-i <?php echo $M8->rojo; ?>"></i></div>
      <h5 class="lbl-semaforo">
      Condicion de terminales.
      </h4>
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="mul_sujecion">
      <i class="fa fa-calendar-check-o fa-3x circle1 opa-i <?php echo $M9->verde; ?>"></i>
      <i class="fa fa-calendar-check-o fa-3x circle2 opa-i <?php echo $M9->amarillo; ?>"></i>
      <i class="fa fa-calendar-check-o fa-3x circle3 opa-i <?php echo $M9->rojo; ?>"></i>
      </div>
      <h5 class="lbl-semaforo">
      Estado fisico y de sujecion.
      </h4>
      </div>

  		<br>
        <br>
        <br>
        <table>
        <!--<tr height="40px;">
    <td>Proximo Servicio:</td>
    <td>
    <input style="border:1px solid #666" type="text" id="mul_proximo" class="multi" name="prox" value="?= $prox?>" >
    </td>
    	</tr>-->
        <tr height="40px;">
    <td>
    Tipo de Servicio:
    </td>
    <td>
    <select class="multic" id="mul_tipo" name="tipo">
      <option value="0" >Seleccione una opcion</option>
      <option value="s" >Servicio</option>
      <option value="g">Garantía / Revisión</option>
      <option value="c">Carroceria</option>
    </select>
    </td>
    	</tr>
        <tr height="40px;">
    <td>
    Observaciones:
    </td>
    <td>
    <input height="80px;" style="border:1px solid #666" name="km" id="mul_km"  class="multi" value="<?= $val;?>">

    </td>
    <tr></table>

  </div>
</div>
<div class="columna columna3">
  <div id="div-debaj" class="firstcol col-md-12">
    <div class="fctitles col-md-12">
      <h4 class="text-center">Condicion De Las Llantas</h4>
    </div>
    <div class="div-semaforo first-semaforo-llanta">
      <div class="semaforo unvalidate" id="llan_di">
          <i class="fa fa-check-circle fa-3x circle1 <?php echo $M21->verde; ?>"></i>
          <i class="fa fa-check-circle fa-3x circle2 <?php echo $M21->amarillo; ?>"></i>
          <i class="fa fa-check-circle fa-3x circle3 <?php echo $M21->rojo; ?>"></i>
      </div>
      <h4 class="text-center first-h4-llantas h4-llantas">Delantera izquierda</h4>
      <input id="llan_diprof" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" value="<?php echo $llan_diprof ?>" />
      <input id="llan_dipres" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Presion PSI" value="<?php echo $llan_dipres ?>" />
    </div>
    <div class="div-semaforo first-semaforo-llanta">
      <div class="semaforo unvalidate" id="llan_dd"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M22->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M22->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M22->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Delantera derecha</h4>
      <input id="llan_ddprof" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" value="<?php echo $llan_ddprof ?>" />
      <input id="llan_ddpres" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Presion PSI" value="<?php echo $llan_ddpres ?>" />
    </div>
    <div class="div-semaforo first-semaforo-llanta">
      <div class="semaforo unvalidate" id="llan_ti"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M23->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M23->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M23->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Trasera izquierda</h4>
      <input id="llan_tiprof" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" value="<?php echo $llan_tiprof ?>" />
      <input id="llan_tipres" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Presion PSI" value="<?php echo $llan_tipres ?>" />
    </div>
    <div class="div-semaforo first-semaforo-llanta">
      <div class="semaforo unvalidate" id="llan_td"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M24->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M24->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M24->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Trasera derecha</h4>
      <input id="llan_tdprof" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" value="<?php echo $llan_tdprof ?>" />
      <input id="llan_tdpres" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Presion PSI" value="<?php echo $llan_tdpres ?>" />
    </div>
    <div class="div-semaforo first-semaforo-llanta">
      <div class="semaforo unvalidate" id="llan_ref"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M25->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M25->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M25->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Llanta De Refaccion</h4>
      <input id="llan_refprof" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" value="<?php echo $llan_refprof ?>" />
      <input id="llan_refpres" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Presion PSI" value="<?php echo $llan_refpres ?>" />
    </div>
  </div>
  <div id="div-debaj" class="firstcol col-md-12">
    <div class="fctitles col-md-12">
      <h4 class="text-center">Medicion de frenos</h4>
    </div>
    <div class="div-semaforo first-semaforo-llanta">
      <div class="semaforo unvalidate" id="fre_di"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M26->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M26->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M26->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas first-h4-llantas">Delantera izquierda</h4>
      <input id="fre_dicero" type="number" class="form-control lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" value="<?php echo $fre_dicero ?>" />
      <input id="fre_diactual" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" value="<?php echo $fre_diactual ?>" />
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="fre_dd"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M27->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M27->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M27->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Delantera derecha</h4>
      <input id="fre_ddcero" type="number" class="form-control lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" value="<?php echo $fre_ddcero ?>" />
      <input id="fre_ddactual" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" value="<?php echo $fre_ddactual ?>" />
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="fre_ti"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M28->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M28->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M28->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Trasera izquierda</h4>
      <input id="fre_ticero" type="number" class="form-control lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" value="<?php echo $fre_ticero ?>" />
      <input id="fre_tiactual" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" value="<?php echo $fre_tiactual ?>" />
    </div>
    <div class="div-semaforo">
      <div class="semaforo unvalidate" id="fre_td"><i class="fa fa-check-circle fa-3x circle1 <?php echo $M29->verde; ?>"></i><i class="fa fa-check-circle fa-3x circle2 <?php echo $M29->amarillo; ?>"></i><i class="fa fa-check-circle fa-3x circle3 <?php echo $M29->rojo; ?>"></i></div>
      <h4 class="text-center h4-llantas">Trasera derecha</h4>
      <input id="fre_tdcero" type="number" class="form-control lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" value="<?php echo $fre_tdcero ?>" />
      <input id="fre_tdactual" type="number" class="form-control lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" value="<?php echo $fre_tdactual ?>"/>
    </div>

  </div>
</div>

<!-- Aqui se termina todo  -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<?php if($cliente == 0){ ?>
<script src="<?php echo base_url();?>js/multipuntos/multi.js"></script>
<script>
            $(document).ready(function(){



			$('.multi').live('focusout',function(){

				var valor=$(this).val();
				var campo=$(this).attr('id');


				//alert(text);
                $.ajax({
                    url:"<?= base_url();?>multipuntos/editmultipuntos?id=<?= $idOrden;?>&campo="+campo+"&valor="+valor+"",
                    success:function(resp){
                        console.log(resp);
                        console.log("Listo");
                    }
                });



				});


				//select

		   			$('.multic').on('change', function() {
						var valor=this.value;

                        $.ajax({
                            url:"<?= base_url();?>multipuntos/editmultipuntos?id=<?= $idOrden;?>&campo=mul_tipo&valor="+valor,
                            success:function(resp){
                                console.log("Listo");
                                console.log(resp);
                            }
                        });
					});

			});
        </script>
<?php } ?>
<?php if($cliente == 1){ ?>
<script src="<?php echo base_url();?>js/multipuntos/multicliente.js"></script>
<script>
            $(document).ready(function(){
                $( 'input' ).prop( 'disabled', true ); //Disable




			});
        </script>
<?php } ?>
</body>
</html>
<?php  $this->load->view('globales/footer'); ?>
