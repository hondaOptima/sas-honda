<!DOCTYPE html>
<html lang="esp" class="no-js">
    <head>
        <?php header('Content-Type: text/html; charset=UTF-8'); ?>
        <title>Honda Optima | SAS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.css" media="screen"/>

        <!-- BEGIN THEME STYLES -->
        <link href="<?= base_url();?>assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/jquery-ui-slider-pips.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <!-- PLUGINS -->
        <link href="<?= base_url() ?>assets-new/css/carroceria.css" rel="stylesheet" />

        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/datatables/responsive.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/sweet-alert-2/sweetalert2.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/amaran/amaran.min.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/select2/select2.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/select2/select2_conquer.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.css"/>

        <!-- Para los iconitos de la derecha -->
        <link href="<?= base_url() ?>assets-new/plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <?php $this->load->view('globales/topbar'); ?>
    </head>

<?php $menu['menu']='lavado';$this->load->view('globales/menu',$menu); ?>

<style type="text/css">
    #printable { display: none; }

    @media print
    {
    	#non-printable { display: none; }
    	#printable { display: block; }
    }
</style>
<div style="display: none;" id="modal-cita" class="div-back-cita">
    <div class="div-modal-cita" >
        <div class="title-delete">
            <div>
                <div id="div-result-manage">
                    <div >
                        <div class="display">
                            <div class="weekdays"></div>
                            <div class="ampm"></div>
                            <div class="alarm"></div>
                            <div class="digits"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="page-breadcrumb breadcrumb">
    <li> <i class="fa fa-shower" id="button_imprimir"></i> <a href="<?= base_url();?>">Autos Lavado <?= $_SESSION["sfworkshop"] ?></a><i class="fa fa-angle-right"></i></li>
    <li class="pull-right"></li>
</ul>
<style>
    .client-m{
        width:160px !important;
    }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-shower"></i><b>Autos Para Lavado </b> </div>
                <div class="tools"> <?= $fecha ?><i class="fa fa-shower"></i>&nbsp;
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="table-lavado">
                    <thead>
                        <tr>
                            <!-- 0 --><th><b>ORDEN ID</b></th>
                            <!-- 1 --><th><b>ASESOR ID</b></th>
                            <!-- 2 --><th><b>TIEMPO ENTREGA</b></th>
                            <!-- 3 --><th><b>TECNICO ID</b></th>

                            <!-- 4 --><th><b>#</b></th>
                            <!-- 5 --><th><b>Hora Entrega</b></th>
                            <!-- 6 --><th><b>Auto</b></th>
                            <!-- 7 --><th><b>Torre</b></th>
                            <!-- 8 --><th><b>Vin</b></th>
                            <!-- 9 --><th>Nombre Asesor</th>
                            <!-- 10 --><th><b>Nombre Técnico</b></th>
                            <!-- 11 --><th><b>Status</b></th>


                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php //$this->load->view('globales/footer');?>
<!-- FOOTER-->
</div>
	</div>
</div>
</div>
</body>
</html>
<script> window.base_url = "<?= base_url() ?>"; window.color_success = "#5cb85c"; window.color_error = '#EA1934'; window.color_warning = "#F28D3A"</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar <span id="spa_vin"></span></h4>
            </div>
            <div class="modal-body">
                <form id="form_mymodal">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_tipo">Tipo</label>
                                <select class="form-control" id="sel_tipo" tabindex="1">
                                    <option></option>
                                    <option value="1">Interno</option>
                                    <option value="2">Particulares</option>
                                    <option value="3">Por seguro</option>
                                </select>
                              </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fecha_recibido">Fecha recibido</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_recibido" placeholder="Fecha Recibido" tabindex="2">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fecha_enviado">Fecha Enviado</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_enviado" placeholder="Fecha Enviado" tabindex="3">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fecha_tentativa">Fecha Tentativa</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_tentativa" placeholder="Fecha Tentativa" tabindex="4">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_poliza">Póliza</label>
                                <input type="text" class="form-control" id="txt_poliza" placeholder="Póliza" tabindex="5">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_siniestro">Siniestro</label>
                                <input type="text" class="form-control" id="txt_siniestro" placeholder="Siniestro" tabindex="6">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_deducible">Deducible</label>
                                <select class="form-control" id="sel_deducible" tabindex="7">
                                    <option></option>
                                    <option value=""></option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                              </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_guardar">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    window.url_base = "<?= base_url() ?>";
</script>
<script src="<?= base_url() ?>assets-new/plugins/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>

<!-- <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/moment/moment.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/sweet-alert-2/sweetalert2.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datatables/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/amaran/jquery.amaran.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/select2/select2.js" type="text/javascript"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/js/lavado.js"></script>
