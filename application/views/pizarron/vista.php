<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='pizarron';$this->load->view('globales/menu',$menu);
//$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
//header("refresh:300; url=$self"); //Refrescamos cada 300 segundos
 ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-desktop"></i> <a href="#">Pizarr&oacute;n Electr&oacute;nico</a> </li>
  <li class="pull-right">
    <div class="actions" style="margin-top:-3px;">
      <div class="btn" style="background:#C00;color:white; border:2px solid #000;width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">En Espera</div>
      <div class="btn" style=" background:#fcb322; color:white; border:2px solid #000;width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;" >En Rampa</div>
      <div class="btn"  style="background-color:#FF0; border:2px solid #000;color:#333; width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">Pend. X Auto</div>
      <div class="btn"  style=" background:#3cc051; color:white; border:2px solid #000; width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">En Prueba</div>
      <div class="btn" style="background-color:green;  color:white; border:2px solid #000;width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">Terminado</div>
    </div>
  </li>
  <li class="pull-right"> </li>
</ul>
<?php
function contar($array,$id){
$res=0;
foreach($array as $ar){
	if($ar->seo_technicianTeam==$id)
	$res++;
	}	
	return $res;
	}
?>
<?php
date_default_timezone_set('America/Tijuana');
$hor=date('H:i');
list($h,$m)=explode(':',$hor);
if($h<12){$ty='AM';}else{$ty='PM';}
$hora=$hor.' '.$ty;

function servicios($id,$array){
$ser='';
$suma=0;
$lser='';
$como='';
$comp='';
foreach($array as $ar){
	if($ar->ssr_IDNULL==$id){
		if($ar->ssr_tipo=='master'){
		$ser=$ar->set_name;}
		$suma+=$ar->ser_approximate_duration;
		$lser.=$ar->set_name.', ';
		$como=$ar->seo_comments;
		$comp=$ar->app_message;
		}
	}	
	
	return $lista=array($lser,$ser,$suma,$como,$comp);
	}
	
function serviciossv($id,$array){
$suma=0;
$lser='';
foreach($array as $ar){
	if($ar->sev_id_orden==$id){
		$suma+=$ar->sev_tiempo;
		$lser.=$ar->sev_nombre.', ';
		}
	}	
return $lista=array($lser,$suma);
	}	
	
	
function asesi($array,$id){
$ok='no';	
foreach($array as $ar){
if($ar->seo_technicianTeam==$id)
$ok='ok';	
}	

return $ok;
	}
	
	
function getpizuser($array,$id){
$cat=array();	
foreach($array as $ar){
if($ar->seo_technicianTeam==$id)
$cat[]=array('torre'=>$ar->seo_tower,'idorden'=>$ar->seo_idServiceOrder,'prometido'=>$ar->seo_promisedTime,
'id'=>$ar->piz_ID,'lavado'=>$ar->piz_status_lavado,'mecanico'=>$ar->piz_status_mecanico,'hlavado'=>$ar->piz_entrega_lavado,'surtido'=>$ar->piz_status_surtido);	
}	

return $cat;
	}		
	
?>
<!-- BEGIN CONTENIDO--> 
<?php echo $flash_message;
//print_r($pizarron);
?>
<div class="row">
<div class="col-md-12">
<div class="portlet">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-desktop"></i>Pizarr&oacute;n Electr&oacute;nico </div>
    <div class="tools">
    <form method="get" action="<?php echo base_url();?>pizarron">
      <div class="col-md-6">
        <div  class="input-group input-small date " id="date-pickerbb" data-date-format="dd-mm-yyyy" >
          <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
          Fecha:
          <?php echo $fecha;?>
          <i style="cursor:pointer" class="fa fa-calendar  input-group-btn"></i> 
          </div>
      </div>
      <div class="col-md-1">
       <?php if($_SESSION['sfrol']<=2) {?>
    
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    <?php } ?>
      </div>
      </form>
      
    </div>
    </div>
    <div class="portlet-body" style=" overflow:scroll">
      <table class="table table-striped table-bordered  table-hover "  id="sample_2" >
        <thead>
          <tr style="font-weight:bold">
            <th width="110px;" style="border-top:2px solid #333; border-right:2px solid #333; border-left:2px solid #333; text-align:center "> <b>T&eacute;cnicos</b> </th>
            <th width="130px;" style="border-top:2px solid #333; border-bottom:2px solid #333; border-right:2px solid #333; text-align:center  " > <b>Descripci&oacute;n</b> </th>
            <?php
                                for($z=1; $z<10; $z++){
									echo'<th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Asignaci&oacute;n '.$z.'</b></th>';
									} 
								?>
                                <th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Acumulado</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($usuarios as $us) {
					if($us->wor_idWorkshop==$taller and $us->rol_idRol==4 and $us->sus_isActive==1 and $us->sus_email!='NA@GMAIL.COM'){
							  ?>
          <tr class="odd gradeX"  >
            <td rowspan="8"  width="110px"  style="border: 2px solid #000; text-align:center"><img src="<?php echo base_url()?>ajax/asesores/thumb.php?file=../../images/empleados/<?php echo $us->sus_foto;?>&size=110" ><br><?php 
			
			
			echo $us->sus_name.' '.$us->sus_lastName;?>
            </td>
            <td style="border-right:2px solid #333; text-align:right"><b> Torre</b></td>
            <?php 
			$sum=0;
			$asesi=asesi($pizarron,$us->sus_idUser);
			if('ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									//echo 'si';
									 for($i=0; $i<$totuno; $i++){
										 
									$pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);
									 $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv);	 
									$sum+=$ser[2] + $sersv[1];	 
									?>
            <td align="center" class="<?php if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 ){ echo 'cambiardia';} ?>" id="<?php echo $pizuser[$i]['idorden'];?>" style="font-weight:bold; cursor:pointer; font-size:16px;  border-right: 2px solid #333;" >
			<?php echo $pizuser[$i]['torre'];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php } 
			echo ' <td align="center" rowspan="8"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }
						echo ' <td align="center" rowspan="8"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';

			
			} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Servicio</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										$pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);	
									  $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv); 
										 ?>
								
            <td title="<?php echo $ser[0].$sersv[0];?>" style=" cursor:help;border-right: 2px solid #333; text-align:center">
			<?php echo $ser[1];
			
			if($ser[3]=='' && $ser[4]==''){}
			else{
			?>
            
            <i style="background-color:yellow" id="<?php echo $pizuser[$i]['idorden']; ?>" class="fa fa-comments-o getComentarios"></i><?php } ?>
            </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="border-right: 2px solid #333; text-align:center"></td>
            <?php } 
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center"></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Tiempo Tabulado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);
									 $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv); 
									?>
            <td style=" border-right: 2px solid #333; text-align:center"><?php echo $ser[2] + $sersv[1];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Hora Promesa</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 
										  $pizuser=getpizuser($pizarron,$us->sus_idUser);
									
									?>
            <td style=" border-right: 2px solid #333; text-align:center"><?php echo $pizuser[$i]['prometido'];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Entregado a Lavado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										   $pizuser=getpizuser($pizarron,$us->sus_idUser);
									?>
            <td style=" border-right: 2px solid #333; text-align:center">
            <?php if($pizuser[$i]['hlavado']==''){
				echo'<div class="savehora" style="cursor:pointer;"  id="'.$pizuser[$i]['id'].'">Cargar hora...</div>';
				}
			else{
			echo'<div class="savehorano" style="cursor:pointer;"  id="'.$pizuser[$i]['id'].'">'.$pizuser[$i]['hlavado'].'</div>';		
				}?>
          </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Estatus Mec&aacute;nico</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
							
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										$pizuser=getpizuser($pizarron,$us->sus_idUser); 
									?>
            <td style="font-weight:bold; border-right: 2px solid #333; "><div  class="btn-group" style="width:100px; height:19px;">
                <button class="btn  btn-xs dropdown-toggle" style=" width:100%;height:19px; background:<?php echo $arraystatus[1][ $pizuser[$i]['mecanico']];?>; color:<?php if($arraystatus[1][ $pizuser[$i]['mecanico']]=='#FF0'){echo'#333';}else{echo 'white';} ?> " type="button" data-toggle="dropdown"> <?php echo $arraystatus[0][ $pizuser[$i]['mecanico']];?><i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background:#C00; color:white;"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=1&fecha=<?php echo $fecha;?>">En Espera</a> </li>
                  <li style="background-color:#fcb322;"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=2&fecha=<?php echo $fecha;?>">En Rampa</a> </li>
                  <li style="background-color:#FF0;" > <a href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=3&fecha=<?php echo $fecha;?>">Pend. X Auto</a> </li>
                  <li style="background-color:#3cc051"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=4&fecha=<?php echo $fecha;?>">En Prueba</a> </li>
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=5&fecha=<?php echo $fecha;?>">Terminado</a> </li>
                </ul>
              </div></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }
			
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td  style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"><b> Estatus Lavado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser); 
									?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "><div class="btn-group" style="width:100px;height:19px;">
                <button class="btn  btn-xs dropdown-toggle" style=" width:100%; height:19px; background:<?php echo $arraystatus[1][ $pizuser[$i]['lavado']];?>; color:<?php if($arraystatus[1][$pizuser[$i]['lavado']]==3){echo'#333';}else{echo 'white';}?>" type="button" data-toggle="dropdown"> <?php echo $arraystatus[0][$pizuser[$i]['lavado']];?><i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background:#C00; color:white;"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=1&fecha=<?php echo $fecha;?>">En Espera</a> </li>
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=5&fecha=<?php echo $fecha;?>">Terminado</a> </li>
                </ul>
              </div></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "></td>
            <?php }
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333;  border-bottom: 2px solid #333;">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333;"></td>
            <?php }} ?>
          </tr>
          
          <!-- status surtido de pieza !-->
          
          <tr class="odd gradeX">
            <td  style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"><b> Surtido de Piezas</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser); 
									?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "><div class="btn-group" style="width:100px;height:19px;">
                <button class="btn  btn-xs dropdown-toggle" 
                style=" width:100%; height:19px;  background:<?php echo $arraystatus[1][ $pizuser[$i]['surtido']];?>; color:white " type="button" data-toggle="dropdown">
				<?php echo $arraystatus[0][$pizuser[$i]['surtido']];?><i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background:#C00; color:white;">
                  <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_surtido&value=6&fecha=<?php echo $fecha;?>">No Surtido </a>
                  </li>
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_surtido&value=7&fecha=<?php echo $fecha;?>">Surtido</a> </li>
                </ul>
              </div></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "></td>
            <?php }
			 } else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333;"></td>
            <?php }} ?>
          </tr>
          
           <!-- fin surtido de pieza !-->          
          
          
          <?php }}?>
        </tbody>
      </table>
      <br><br>
      <table class="table table-striped table-bordered  table-hover "  id="sample_2" >
        <thead>
          <tr style="font-weight:bold">
            <th width="110px;" style="border-top:2px solid #333; border-right:2px solid #333; border-left:2px solid #333; text-align:center "> <b>T&eacute;cnicos</b> </th>
            <th width="130px;" style="border-top:2px solid #333; border-bottom:2px solid #333; border-right:2px solid #333; text-align:center  " > <b>Descripci&oacute;n</b> </th>
            <?php
                                for($z=1; $z<10; $z++){
									echo'<th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Asignaci&oacute;n '.$z.'</b></th>';
									} 
								?>
                                <th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Acumulado</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($usuarios as $us) {
					if($us->wor_idWorkshop==$taller and $us->rol_idRol==4 and $us->sus_isActive==1 and $us->sus_email=='NA@GMAIL.COM'){
							  ?>
          <tr class="odd gradeX"  >
            <td rowspan="5"  width="110px"  style="border: 2px solid #000; text-align:center"><img src="<?php echo base_url()?>ajax/asesores/thumb.php?file=../../images/empleados/<?php echo $us->sus_foto;?>&size=110" ><br><?php 
			
			
			echo $us->sus_name.' '.$us->sus_lastName;?>
            </td>
            <td style="border-right:2px solid #333; text-align:right"><b> Torre</b></td>
            <?php 
			$sum=0;
			$asesi=asesi($pizarron,$us->sus_idUser);
			if('ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									//echo 'si';
									 for($i=0; $i<$totuno; $i++){
										 
									$pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);
									 $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv);	 
									$sum+=$ser[2] + $sersv[1];	 
									?>
            <td align="center" class="<?php if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 ){ echo 'cambiardia';} ?>" id="<?php echo $pizuser[$i]['idorden'];?>" style="font-weight:bold; cursor:pointer; font-size:16px;  border-right: 2px solid #333;" >
			<?php echo $pizuser[$i]['torre'];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; ">
            <select class="torre" sty>
            <?php for($t=1; $t<100; $t++) {?>
            <option value="<?= $t;?>"><?= $t;?></option>
            <?php } ?>
            </select>
            </td>
            <?php } 
			echo ' <td align="center" rowspan="5"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }
						echo ' <td align="center" rowspan="5"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';

			
			} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> VIN</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										$pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);	
									  $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv); 
										 ?>
								
            <td title="<?php echo $ser[0].$sersv[0];?>" style=" cursor:help;border-right: 2px solid #333; text-align:center">
			<?php echo $ser[1];
			
			if($ser[3]=='' && $ser[4]==''){}
			else{
			?>
            
            <i style="background-color:yellow" id="<?php echo $pizuser[$i]['idorden']; ?>" class="fa fa-comments-o getComentarios"></i><?php } ?>
            </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="border-right: 2px solid #333; text-align:center"></td>
            <?php } 
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center">JHHJD15619XDM8</td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Estado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);
									 $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv); 
									?>
            <td style=" border-right: 2px solid #333; text-align:center"><?php echo $ser[2] + $sersv[1];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Ubicaci&oacute;n</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 
										  $pizuser=getpizuser($pizarron,$us->sus_idUser);
									
									?>
            <td style=" border-right: 2px solid #333; text-align:center"><?php echo $pizuser[$i]['prometido'];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"><b>Tiempo Tabulado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										   $pizuser=getpizuser($pizarron,$us->sus_idUser);
									?>
            <td style=" border-right: 2px solid #333; text-align:center">
            <?php if($pizuser[$i]['hlavado']==''){
				echo'<div class="savehora" style="cursor:pointer;"  id="'.$pizuser[$i]['id'].'">Cargar hora...</div>';
				}
			else{
			echo'<div class="savehorano" style="cursor:pointer;"  id="'.$pizuser[$i]['id'].'">'.$pizuser[$i]['hlavado'].'</div>';		
				}?>
          </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"></td>
            <?php }} ?>
          </tr>
         
          
           <!-- fin surtido de pieza !-->          
          
          
          <?php }}?>
        </tbody>
      </table>
      
      
      
      
      
    </div>
  </div>
</div>

<!-- Modal cambiar fecha-->

<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Mover al Siguiente dia:</h4>
      </div>
      <div class="modal-body"> <?php echo form_open('pizarron/cambiardia/','class=""'); ?>
        <input type="hidden" name="ido" value="">
        <div class="form-group">
										
										<div class="col-md-3">
											
											<input type="text" name="fecha" class="form-control" value="<?php echo $end = date("Y-m-d",strtotime("+1 days"));?>" <?php if($_SESSION['sftype']==2){}else{echo 'readonly';}?>>
												
											</div>
											<!-- /input-group -->
											
										</div>
									
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <input type="submit" class="btn btn-info" value="Guardar">
      </div>
    </div>
    </form>
    <!-- /.modal-content --> 
  </div>
</div>

<!-- Modal cambiar fecha-->

<div class="modal fade" id="wideCom" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Comentarios:</h4>
      </div>
      <div class="modal-body"> 
     <div id="comentarios"></div>							
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
   
      </div>
    </div>
   
    <!-- /.modal-content --> 
  </div>
</div>


<!-- END CONTENIDO-->
<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('pizarron/js/script');?>

<script>
jQuery(document).ready(function() {
	$(function () {
	 $('#date-pickerbb').datepicker({ 
	language: 'es',
	isRTL: false,
         autoclose:true
                
	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });

	 
	});
	
$('.cambiardia').on('click',function(){
var id=$(this).attr('id');
$('#wide').modal('show');    
$('input[name=ido]').val(id); 
	
	});	
	
	$('.getComentarios').on('click',function(){
var id=$(this).attr('id');
$('#wideCom').modal('show');
 $.ajax({
	  url:"<?php echo base_url();?>ajax/pizarron/comentarios.php?ido="+id+"",
	  success:function(result){
         $('#comentarios').html(result);
		       
                              }
         });    

	
	});	
	
$('.savehora').on('click',function(){
var campo='piz_entrega_lavado';
var id=$(this).attr('id');
var valor="<?php echo $hora?>";

 $.ajax({
	  url:"<?php echo base_url();?>pizarron/update?id="+id+"&campo="+campo+"&value="+valor+"",
	  success:function(result){
         $('#'+id+'').html('<?php echo $hora?>');
		 $('#'+id+'').removeClass();
		 $('#'+id+'').addClass('savehorano');        
                              }
         });

});

$('.savehorano').on('click',function(){
var campo='piz_entrega_lavado';
var id=$(this).attr('id');
var valor="";

 $.ajax({
	  url:"<?php echo base_url();?>pizarron/update?id="+id+"&campo="+campo+"&value="+valor+"",
	  success:function(result){
         $('#'+id+'').html('Cargar hora...');
		 $('#'+id+'').removeClass();
		 $('#'+id+'').addClass('savehora');    
                              }
         });

});

});
</script>