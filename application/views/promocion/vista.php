<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>	
<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-tags"></i>
							<a href="<?php echo base_url();?>servicio">Promociones</a>
							<i class="fa fa-angle-right"></i>
						</li>
</ul>   
<?php echo $flash_message; ?>
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Promociones
							</div>
							<div class="actions">
								<a href="<?php echo base_url();?>promocion/crear" class="btn btn-success"><i class="fa fa-pencil"></i> Añadir Nueva Promoción</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" >
							<thead>
							<tr style="font-weight:bold; text-align:center">
								<th width="200px;">
									 <b>Imagen</b>
                                </th>
								<th width="200px;">
									 <b>Nombre</b>
								</th>
								<th >
									 <b>Descripción</b>
								</th>
								<th width="100px">
									 <b>Fecha de inicio</b>
								</th>
								<th width="150px;">
									 <b>Fecha de expiración</b>
								</th>
								<th width="50px;">
									 <b>Estatus</b>
								</th>		
								<th width="100px;">
									 <b>Control</b>
								</th>								
                            </tr>
							</thead>
							<tbody>
                            <?php if($promociones): ?>
                            <?php
                            foreach($promociones as $promocion){ ?>
								<tr class="odd gradeX">
								<td>
                                	<img src="<?php echo base_url().'images/promotions/'.$promocion->ima_name; ?>" 
                                    alt class="img-responsive" style="width:200px; height:100px;">
                                </td>
								<td>
                                	<?php echo $promocion->pro_name; ?>
								</td>
								<td>
									 <?php echo $promocion->pro_description; ?>
								</td>
								<td>
									 <?php echo date('Y-m-d', strtotime($promocion->pro_startDate)); ?>
								</td>
                                <td>
                                	<?php echo date('Y-m-d', strtotime($promocion->pro_endDate)); ?>
                                </td>
                                <td>
									 <?php if($promocion->pro_isActive==1){ $status = 'Activo'; $label='success'; $action='desactivar/'; }
                                                   else { $status = 'Inactivo'; $label='danger'; $action='activar/'; }?>
                                                    
                                        
                                        <a href="<?php echo base_url().'promocion/'.$action.$promocion->pro_idPromotion ?>" 
                                            onClick="return confirm('Está seguro que desea desactivar esta promoción?');">
                                            <span class="label label-sm label-<?php echo $label?>">
												<?php echo $status?>
                                            </span>
                                        </a>
                                </td>
                                <td>
                                
                                <div class="task-config-btn btn-group">
                                    <a class="btn btn-xs btn-default dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cog"></i>Acción
                                    <i class="fa fa-angle-down"></i>
                                    
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                        	<a href="<?php echo base_url().'promocion/editar/'.$promocion->pro_idPromotion ?>">
                                        <i class="fa fa-edit"></i>
                                        Editar</a>
                                        </li>
                                        <li>
                                        	 <a href="<?php echo base_url().'promocion/eliminar/'.$promocion->pro_idPromotion ?>" 
                                            onClick="return confirm('Está seguro que desea Eliminar esta promoción?');">
                                        <i class="fa fa-trash"></i>
                                        Eliminar</a>
                                        </li>
                                    </ul>
								</div>
                                    
                                </td>
							</tr>
								
								<?php } ?>
                                <?php endif ?>
							
							
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('produccion/js/script');?> 