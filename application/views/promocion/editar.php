<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>	
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>pizarron">Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-tags"></i>
							<a href="<?php echo base_url();?>promocion">Promociones</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-edit"></i>
							<a href="#">Editar</a>
							<i class="fa fa-angle-right"></i>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
                    
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Editar Promoción
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
                                        
                                        <?php if($promocion):?>
                                        
                          				<?php foreach($promocion as $p): ?>
                     					<?php echo form_open_multipart('promocion/actualizar/'.$p->pro_idPromotion,'class="form-horizontal"'); ?>
											<div class="form-body">
												<h3 class="form-section">Información de Promoción</h3>
												<div class="row">
                                                	<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Nombre</label>
															<div class="col-md-6">
                                   								<?php  echo form_input('nombre', $p->pro_name, 
																		'id="nombre" class="form-control"
																		placeholder="Nombre" required');?>
															</div>
														</div>
													</div>
                                                </div>
												<!--/row-->
                                              	<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Inicio y vencimiento</label>
                                                            <div class="col-md-6">
                                                                <div class="input-group date-picker input-daterange" 
                                                                data-date="2014-06-01" data-date-format="yyyy-mm-dd">
                                                                    <input type="text" class="form-control" 
                                                                    id="id" name="inicio" required readonly 
                                                                    value="<?php echo date('Y-m-d', strtotime($p->pro_startDate)) ?>">
                                                                    <span class="input-group-addon">
                                                                         a
                                                                    </span>
                                                                    <input type="text" class="form-control" 
                                                                    id="final" name="final" required readonly 
                                                                    value="<?php echo date('Y-m-d', strtotime($p->pro_endDate)) ?>">
                                                                </div>
                                                            </div>															
                                                        </div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
                                                            
															<label class="control-label col-md-3">Descripción</label>
															<div class="col-md-6">
                                   								<?php  echo form_textarea('descripcion',
																						  $p->pro_description, 
																		'id="descripcion" class="form-control" 
																		placeholder="Descripción" 
																		style="resize: none; height:120px;" required');?>
															</div>

														</div>
													</div>
												</div>
												<!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Cambiar Imagen</label>
                                                            
                                                            <div class="col-md-9">
                                                                <input type="file" id="userfile" name="userfile"/>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
												<!--/row-->
												<h3 class="form-section">Imagen</h3>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group" align="center">
                                                        	<img src="<?php echo base_url().'images/promotions/'.$p->ima_name; ?>" 
                                                            class="img-responsive" style="width:50%;">
															<input type="hidden" name="imagen" id="imagen" 
                                                            value="<?php echo $p->Images_ima_idImage ?>">
															<input type="hidden" name="nombreImagen" id="nombreImagen" 
                                                            value="<?php echo $p->ima_name ?>">
														</div>
													</div>
												</div>
												<!--/row-->
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-success">Guardar</button>
															<a href="<?php echo base_url();?>promocion">
                                                            	<button type="button" class="btn btn-default">
                                                                Cancelar
                                                                </button>
                                                            </a>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>

                                            
										<?php echo form_close(); ?>
						   				<?php endforeach ?>
	                 					<?php else: ?>None<?php endif ?>
										<!-- END FORM-->
									</div>
								</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				
		</div>
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<!--<php $data['script']='usuario'; $this->load->view('globales/footer',$data); ?>-->


<script src="<?php echo base_url();?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/scripts/app.js"></script>
<script src="<?php echo base_url();?>assets/scripts/form-components.js"></script>
<script src="<?php echo base_url();?>assets/scripts/form-samples.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormComponents.init();
});
</script>
<script type="text/javascript">
var RecaptchaOptions = {
   theme : 'custom',
   custom_theme_widget: 'recaptcha_widget'
};
</script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LcrK9cSAAAAALEcjG9gTRPbeA0yAVsKd8sBpFpR"></script>

<!-- END FOOTER -->


</body>
<!-- END BODY -->
</html>