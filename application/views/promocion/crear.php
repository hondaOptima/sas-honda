<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>	
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url();?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-tags"></i>
        <a href="<?php echo base_url();?>promocion">Promociones</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-edit"></i>
        <a href="<?php echo base_url();?>promocion/crear">Nuevo</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>  
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i>Nueva Promoción
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        
        <?php echo form_open_multipart('promocion/crear','class="form-horizontal"'); ?>
            <div class="form-body">
                <h3 class="form-section">Información de Promoción</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-6">
                                <?php  echo form_input('nombre',set_value('nombre'), 
                                        'id="nombre" class="form-control"
                                        placeholder="Nombre" required');?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Inicio y vencimiento</label>
                            <div class="col-md-6">
                                <div class="input-group date-picker input-daterange" 
                                data-date="2014-06-01" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control" 
                                    id="id" name="inicio" required readonly>
                                    <span class="input-group-addon">
                                         a
                                    </span>
                                    <input type="text" class="form-control" 
                                    id="final" name="final" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Descripción</label>
                            <div class="col-md-6">
                                <?php  echo form_textarea('descripcion',
                                                          set_value('descripcion'), 
                                        'id="descripcion" class="form-control" 
                                        placeholder="Descripción" 
                                        style="resize: none; height:120px;" required');?>
                            </div>

                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Imagen</label>
                            <div class="col-md-6">
                                <input type="file" id="userfile" name="userfile" required/>
                            </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-success">Añadir</button>
                            <a href="<?php echo base_url();?>servicio">
                                <button type="button" class="btn btn-default">
                                Cancelar
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>

            
        <?php echo form_close(); ?>
        <!-- END FORM-->
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('promocion/js/script');?> 