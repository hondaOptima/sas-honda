<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>servicio">Servicios</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>modelovehiculo">Modelos de Vehículo</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-user"></i>Modelos de Vehículo </div>
        <div class="actions"> <a href="<?php echo base_url();?>modelovehiculo/crear" class="btn btn-success"><i class="fa fa-pencil"></i> Añadir Modelo</a> </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
              <th> <b>Nombre</b> </th>
              <th> <b>Estatus</b> </th>
              <th> <b>Acciones</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php if($modelos): ?>
            <?php
                            foreach($modelos as $modelo){ ?>
            <tr class="odd gradeX">
              <td><?php echo $modelo->vem_name; ?></td>
              <td><?php if($modelo->vem_isActive==1){ $status = 'Activo'; $label='success'; $action='desactivar/'; }
                                                   else { $status = 'Inactivo'; $label='danger'; $action='activar/'; }?>
                <a href="<?php echo base_url().'modelovehiculo/'.$action.$modelo->vem_idVehicleModel ?>" 
                                        onClick="return confirm('Está seguro que desea desactivar este modelo de vehículo?');"> <span class="label label-sm label-<?php echo $label?>"> <?php echo $status?> </span> </a></td>
              <td><?php if($modelo->vem_isActive==1){ $status = 'Activo'; $label='success'; $action='desactivar/'; }
                                                   else { $status = 'Inactivo'; $label='danger'; $action='activar/'; }?>
                <a class="btn btn-default btn-xs" href="<?php echo base_url().'modelovehiculo/editar/'.$modelo->vem_idVehicleModel ?>"   > Editar </a></td>
            </tr>
            <?php } ?>
            <?php endif ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('modelovehiculo/js/script');?>
