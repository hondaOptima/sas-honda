
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/scripts/table-managed.js"></script>

<script src="<?php echo base_url();?>assets/scripts/form-samples.js"></script>
<script src="<?php echo base_url();?>assets/scripts/ui-toastr.js"></script>


<script>
jQuery(document).ready(function() {    
   TableManaged.init();
    FormSamples.init();
   UIToastr.init();
   
});
</script>