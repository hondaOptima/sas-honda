<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='tras'; $this->load->view('globales/menu',$data); ?>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?= base_url().'js/modalalert.js'; ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/less/traslados/config.css" />
    <script src="https://npmcdn.com/packery@2.1/dist/packery.pkgd.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!--<script src="<?php echo base_url();?>assets/less/less.js" type="text/javascript"></script>-->
</head>
<body>
    <div class="toptitle">
        <div class="btn btn-primary agregar" id="agregar">AGREGAR</div>
        <div style="position: absolute; width:95%; text-align: center;"><h3 style="position: relative; z-index: inherit">Configuracion Traslados</h3></div>
        <div class="btn btn-default back">VOLVER</div>
    </div>
    <div class="window">
        <div class="window1 child">
            <div class="conf-dias btn btn-default">
                <div class="div-h4-cal">
                    <h3>Configurar Dias</h4>
                </div>
                <div class="div-fa-calendar">
                    <span><i class="fa fa-calendar cal-tr"></i></span>
                </div>
            </div>
            <div class="conf-paquetes btn btn-default">
                <div class="div-h4-cal">
                    <h3>Configurar Paquetes</h4>
                </div>
                <div class="div-fa-calendar">
                    <span><i class="fa fa-columns"></i></span>
                </div>
            </div>
        </div>
        <div class="window2 child" style="display:none">
            <div class="col-md-2 btn btn-default dia">
                <h4>Lunes</h4>
                <h6>Horas: <span class="hrsact">0</span>/<span class="hrsmax">0</span></h6>
                <span class="btn btn-success" onclick="saveDay(this)">GUARDAR</span>
                <ul id="sortable1" class="sortable">
                
                </ul>

            </div>
            <div class="col-md-2 btn btn-default dia">
                <h4>Martes</h4>
                <h6>Horas: <span class="hrsact">0</span>/<span class="hrsmax">0</span></h6>
                <span class="btn btn-success" onclick="saveDay(this)">GUARDAR</span>
                <ul id="sortable2" class="sortable">
                
                </ul>
            </div>
            <div class="col-md-2 btn btn-default dia">
                <h4>Miercoles</h4>
                <h6>Horas: <span class="hrsact">0</span>/<span class="hrsmax">0</span></h6>
                <span class="btn btn-success" onclick="saveDay(this)">GUARDAR</span>
                <ul id="sortable3" class="sortable">
                
                </ul>
            </div>
            <div class="col-md-2 btn btn-default dia">
                <h4>Jueves</h4>
                <h6>Horas: <span class="hrsact">0</span>/<span class="hrsmax">0</span></h6>
                <span class="btn btn-success" onclick="saveDay(this)">GUARDAR</span>
                <ul id="sortable4" class="sortable">
                
                </ul>
            </div>
            <div class="col-md-2 btn btn-default dia">
                <h4>Viernes</h4>
                <h6>Horas: <span class="hrsact">0</span>/<span class="hrsmax">0</span></h6>
                <span class="btn btn-success" onclick="saveDay(this)">GUARDAR</span>
                <ul id="sortable5" class="sortable">
                
                </ul>
            </div>
            <div class="col-md-2 btn btn-default dia">
                <h4>Sabado</h4>
                <h6>Horas: <span class="hrsact">0</span>/<span class="hrsmax">0</span></h6>
                <span class="btn btn-success" onclick="saveDay(this)">GUARDAR</span>
                <ul id="sortable6" class="sortable">
                
                </ul>
            </div>
        </div>
        <div class="window2-1" style="display:none">
            <div class="containerpackages">
                <ul class="packages">
                </ul>
            </div>
            <div class="trashCan">
                <i class="fa fa-trash-o trash"></i>
            </div>
        </div>

        <div class="window3 child" style="display:none">
            <div class="packages-grid" style="display:none" id="packages-grid">
                
            </div>
        </div>
        
    </div>
</body>

<div class="modal-cover" style="display:none">
    <div class="create-p" style="display:none">
        <h3 style="color:gray">Crear Paquete</h3>
        <hr>

        <div>
            <div class="popup-lbl"><h4 style="float:left">Lugar de Origen</h4></div>
            <div class="popup-inp"><select id="makeorigen" type="time" class="form-control h-inicio"></select></div>
        </div>
        <div>
            <div class="popup-lbl"><h4 style="float:left">Lugar Destino</h4></div>
            <div class="popup-inp"><select id="makedestino" type="time" class="form-control h-fin"></select></div>
        </div>
        <div>
            <div class="popup-lbl"><h4 style="float:left">Horas Estimadas de Traslado</h4></div>
            <div class="popup-inp"><input id="makehoras" type="number" class="form-control h-fin"></div>
        </div>
        <div class="okcancelbuttons">
            <div id="makepkgCancel" class="btn-cancel-activ btn btn-danger">CANCELAR</div>
            <div id="makepkgAccept" class="btn-accept-activ btn btn-primary">CREAR</div>
        </div>

    </div>
    <div class="edit-p" style="display:none">
        <h3 style="color:gray">Modificar Paquete</h3>
        <hr>

        <div>
            <div class="popup-lbl"><h4 style="float:left">Lugar de Origen</h4></div>
            <div class="popup-inp"><select id="editorigen" type="time" class="form-control h-inicio"></select></div>
        </div>
        <div>
            <div class="popup-lbl"><h4 style="float:left">Lugar Destino</h4></div>
            <div class="popup-inp"><select id="editdestino" type="time" class="form-control h-fin"></select></div>
        </div>
        <div>
            <div class="popup-lbl"><h4 style="float:left">Horas Estimadas de Traslado</h4></div>
            <div class="popup-inp"><input id="edithoras" type="number" class="form-control h-fin"></div>
        </div>
        <div class="okcancelbuttons">
            <div id="editpkgCancel" class="btn-cancel-activ btn btn-danger">CANCELAR</div>
            <div id="editpkgAccept" class="btn-accept-activ btn btn-primary">GUARDAR</div>
        </div>
    </div>
</div>

<style>
    .agregar {
        float:left;
        display:none;
    }

    .back {
        float:right;
    }

    .agregar, .back {
        position: relative;
        margin:10px;
        z-index: 10 !important;
    }

    .popup-lbl, .popup-inp {
        position: relative; float: left;
        text-align: center;
        width: 100%;
    }

    .create-p > div, .edit-p > div {
        width: 90%;
        margin: 5px 5% 0px 5%;
        padding: 0px;
    }

    .okcancelbuttons {
        margin-top: 20px !important;
        position: relative; float: left;
        text-align: center;
        width: 90%;
    }

    .okcancelbuttons div {
        width: auto;
        float: none;
    }

    .packages-grid{
        overflow-y: scroll;
    }

    .create-p, .edit-p{
        position: fixed;
        text-align: center;
        /*z-index: 99;*/
        width: 40%;
        height: 500px;
        top: 150px;
        left: 30%;
        background: white;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    }
    .modal-cover{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
       /* z-index: 1;*/
        background: rgba(104, 104, 103, 0.6); 
    }

    .sortable, .packages {
        padding: 0px;
        padding-top: 2px;
        width: 100%;
        text-align: center;
    }

    .containerpackages {
        height: 80%;
        overflow-y: scroll;
    }

    .package {
        padding: 0px;
        margin: 0px;
    }

    .packages li, .sortable li {
        padding: 0px;
        margin: 1px 2%;
        padding-top: 5px;
        width: 96%;
        height: 70px;
        border: 1px solid rgba(255, 255, 255, 0.85);
        font-size: 10pt;
    }

    .paqhoras {
        position: relative;
        float: right; 
        font-size: 0.7em;
        margin-right: 5px;
        padding: 0px;
    }

    .lugar {
        font-size: 0.85em;
        text-align: center;
    }

    .trashCan {
        position: relative;
        text-align: center;
        font-size: 8px;
        list-style-type: none;
        background-color: rgba(0, 0, 0, 0);
        height: 17%;
        border: 0px solid #FFF;
        margin: 0px;
        padding: 0px;
        padding-top: 8%;
        padding-bottom: 8%;
    }

    .trashCan i {
        font-size: 36px;
        padding-top: inherit;
        padding-bottom: inherit;
    }

    .trashCan li {
        background-color: rgba(0, 0, 0, 0.5);
        height: 0px !important;
        border: 0px;
    }

    .window2-1 {
        height: 70% !important;
    }

    .paq-botones {
        position: relative;
        text-align: center;
        width: 100%;
    }

    #paq-edit, #paq-delete {
        position: relative;
        float: left;
        width: 24px;
        height: 22px;
        font-size: 12px;
        padding: 4px 2px 2px 3px;
        text-align: center;
        margin: 0px 5px;
    }

    #paq-delete {
        float: right;
    }

    #paq-edit:hover, #paq-delete:hover {
        background: rgb(56, 123, 172);
        cursor: pointer;
    }



</style>
<script>

    //-----------------------------
    //
    //  Funciones Compartidas
    //
    //-----------------------------


    var page = 0;
    $(document).ready(function(){
        page = 0;
    });
    $('.conf-dias').on('click', function(){
        //en lo que se carga la pagina, se busca la informacion
        getConfig('dias');
        $('.window1').fadeOut(100, function(){
            page = 2;
            $('.window2, .window2-1').fadeIn(100);
            
        });
    });

    $('.conf-paquetes').on('click', function(){
        //en lo que se carga la pagina, se busca la informacion
        getConfig('paquetes');
        $('.window1').fadeOut(100, function(){
            page = 3;
            $('.window3').fadeIn(100, function(){
                $('.packages-grid, .packages-trash-config').fadeIn(100);
                $('.agregar').fadeIn(100);
            });
            
        });
    });

    $('.back').on('click', function(){
        if(page > 0){
            if(page==2){
                $('.window2, .window2-1').fadeOut(100, function(){
                    $('.window1').fadeIn(200);
                });
            }else{
                if(page==3){
                    $('.window3, .packages-grid, .packages-trash-config').fadeOut(100, function(){
                        $('.agregar').fadeOut(50);
                        $('.window1').fadeIn(200);
                    });
                }
            }
        }
    });

    function getConfig(modo)
    {
        //se vacian los div de paquetes
        $('#packages-grid').empty(); //config de paquetes
        $('.packages').empty(); //config de dias

        //se vacian los div de dias
        $('.window2').children('div').each(function() {
            $(this).children('.sortable').empty();
        });

        $.post('http://hsas.gpoptima.net/traslados/configuration_getConfiguration/', { datos: modo }, function(data) {
            JSONdata = JSON.parse(data);
            if(JSONdata.status == 0)
            {
                if(modo == 'dias')
                {
                    displayWeekConfig(JSONdata.paquetes, JSONdata.dias);
                }
                if(modo == 'paquetes')
                {
                    displayPackageConfig(JSONdata.paquetes);
                    displayAgencyCatalog();
                }
            }
            else
            {
                desplegarModal(JSONdata.message);
            }
        });
    }


    //-----------------------------
    //
    //  Configuracion de Dias
    //
    //-----------------------------

    // $('.sortable').on('DOMSubtreeModified',function(){
    //     desplegarModal('UL content changed!!!');
    // });

    //variable para almacenar los dias en formato JSON
    //Posible cambio
    var jsons = [];


    $(function() {

        $(".packages li").draggable({
            connectToSortable: '.sortable',
            helper: 'clone',
            scroll: false,
            revertDuration: 0,
            revert: "invalid"
        });

        $(".sortable li").draggable({
            connectToSortable: '.sortable, .trashCan',
            revertDuration: 0,
            revert: true
        });

        $(".sortable").sortable({
            connectWith: '.trashCan',
            revert: 250,
            stop: function(event, data) {
                //var order = $(this).attr('id');
                var daynum = parseInt($(this).parent().attr('id'));

                var horasTotal = 0;
            
                //se inicializa el JSON y se almacena el numero de dia (en caso de cambiar el almacenamiento de datos, cambiar aqui)
                var phrases = JSON.parse('{ "dia" : "'+daynum+'" }');
                phrases.paquetes = [];
                var count = 0;
                $(this).find('li').each(function(i, nli){
                    horasTotal += parseInt($(this).attr('hrs'));
                    //se ingresa el valor del orden de acuerdo al contador (0-N)
                    $(this).attr('orden', count++);
                    //y se aniade el numero del paquete al json (en caso de cambiar el almacenamiento de datos, cambiar aqui)
                    phrases.paquetes.push($(this).attr('pkg'));

                });

                //CAMBIAR si es que se aumenta o se puede obtener hora de inicio y fin
                horasFin = parseInt($(this).parent().find('.hrsmax').text());

                if(horasTotal > 17)
                {
                    desplegarModal('Al agregar el paquete, el dia excede con las horas asignadas.');
                    $(data.item).remove();
                }
                else
                {
                    $(this).parent().find('.hrsact').text(horasTotal);
                    //y se almacena el JSON formado en el arreglo de JSONs
                    jsons[(parseInt($(this).parent().attr('id')) - 1)] = phrases;
                }
            }

        });

        $('.trashCan').sortable({
            placeholder: false,
            revert: false,
            items: 'li',
            receive: function()
            {
                $(this).children('li').remove();
            }
        });

        // $( "#sortable" ).sortable({
        //   revert: true
        // });
        // $( "#sortable2" ).sortable({
        //   revert: true
        // });
        // $( "#draggable" ).draggable({
        //   connectToSortable: "#sortable",
        //   helper: "clone",
        //   revert: "invalid"
        // });
        // $( "#draggable2" ).draggable({
        //   connectWith: ".sortable",
        //   helper: "clone",
        //   revert: "invalid"
        // });

        $( "ul, li, .trashCan" ).disableSelection();
    });


    function saveDay(elem)
    {
        var id = $(elem).parent().attr('id');
        var error = false;

        var org = '1';

        $(elem).parent().find("ul").find("li").each(function(i, nli) {
            var dst = $(this).attr('org');
            if(org !== dst) 
            {
                error = true;
            }

            org = $(this).attr('dst');
        });

        if(error)
        {
            desplegarModal('La relación origen-destino entre los paquetes no concuerda en el dia '+ $(elem).parent().find('h4').text());
        }
        else
        {
            var diajson = JSON.stringify(jsons[id-1]);
            $.post('http://hsas.gpoptima.net/traslados/configuration_saveConfiguration/', { json: diajson }, function(data) {
                var JSONdata = JSON.parse(data);
                desplegarModal(JSONdata.message);
            });
        }
    }

    function displayWeekConfig(paquetes, dias)
    {
        var divsdias = $('.window2').children('div');
        for(var i = 0; i < dias.length; i++)
        {
            //se comienza el json para guardado
            var json = JSON.parse('{}');
            json.dia = dias[i].numdia;
            json.horas = 0;
            json.paquetes = [];
            //se agrega el ID del dia al div
            divsdias[i].id = dias[i].numdia;
            //se obtiene el div sortable actual vacio
            var sortable = $('#sortable' + (i + 1)).empty();
            //y se recorren los paquetes para almacenamiento
            for(var j = 0; j < dias[i].paquetes.length; j++)
            {
                //se obtiene el ID del paquete
                var numpaquete = dias[i].paquetes[j].id;
                //se crea el nuevo elemento a ingresar y se le inserta los datos del paquete actual
                var $li = $('<li class="ui-state-highlight package"/>')
                .attr('pkg', numpaquete)
                .attr('hrs', dias[i].paquetes[j].horas)
                .attr('org', dias[i].paquetes[j].origen.id)
                .attr('dst', dias[i].paquetes[j].destino.id)
                .append('<span>Paquete: '+ numpaquete +'</span>')
                .append('<span class="paqhoras">'+ parseInt(dias[i].paquetes[j].horas) +'h</span>')
                .append('<br><span class="lugar">'+ dias[i].paquetes[j].origen.agencia +'</span>')
                .append('<br><span class="lugar">'+ dias[i].paquetes[j].destino.agencia +'</span>');
                //y se le aniade al sortable del dia
                sortable.append($li).sortable('refresh');
                //se almacena el ID en el json
                json.paquetes[j] = numpaquete;
                //y se le suman las horas del paquete
                json.horas += parseInt(dias[i].paquetes[j].horas);
            }
            //y se aniade el json al arreglo para almacenar (cambiar si es necesario)
            jsons[i] = json;

            sortable.parent().find('.hrsact').text(json.horas);
            sortable.parent().find('.hrsmax').text(17);
        }

        $('.packages').empty();
        for(var i = 0; i < paquetes.length; i++)
        {
            var $li = $('<li class="ui-state-highlight package"/>')
            .attr('pkg', paquetes[i].id)
            .attr('hrs', paquetes[i].horas)
            .attr('org', paquetes[i].origen.id)
            .attr('dst', paquetes[i].destino.id)
            .append('<span>Paquete: '+ paquetes[i].id +'</span>')
            .append('<span class="paqhoras">'+ parseInt(paquetes[i].horas) +'h</span>')
            .append('<br><span class="lugar">'+ paquetes[i].origen.agencia +'</span>')
            .append('<br><span class="lugar">'+ paquetes[i].destino.agencia +'</span>');
            $('.packages').append($li).sortable();
        }

        $(".packages li").draggable({
            connectToSortable: '.sortable',
            helper: 'clone',
            revertDuration: 0,
            revert: "invalid"
        });

    }

    //-----------------------------
    //
    //  Configuracion de Paquetes
    //
    //-----------------------------

    function displayPackageConfig(paquetes)
    {
        var divspaquetes = $('.packages-grid');
        var pGrid = $('#packages-grid');
        var divPaq = '';
        for(var i = 0; i < paquetes.length; i++)
        {
            divPaq += '<div class="col-md-3 paq-grid">'+
                '<div class="paq-botones">'+
                    '<div id="paq-edit">'+
                        '<i class="fa fa-pencil"></i>'+
                    '</div>'+
                    '<div id="paq-delete">'+
                        '<i class="fa fa-times"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="paq-name" paq="'+ paquetes[i].id +'">'+
                    'Paquete: '+paquetes[i].id+
                '</div>'+
                '<div class="paq-horas" hrs="'+ paquetes[i].horas +'">'+
                    ''+ paquetes[i].horas +' horas'+
                '</div>'+
                '<div class="paq-origen" org="'+ paquetes[i].origen.id +'">'+
                 '<i class="fa fa-level-up icon-tr"></i>'+
                    paquetes[i].origen.agencia+
                '</div>'+
                '<div class="paq-destino" dst="'+ paquetes[i].destino.id +'">'+
                    '<i class="fa fa-level-down icon-tr"></i>'+
                    paquetes[i].destino.agencia+
                '</div>'+
            '</div>';
        }

        pGrid.append(divPaq);
    }

    function displayAgencyCatalog() {

        $.get('http://hsas.gpoptima.net/traslados/configuration_getPlacesCatalog', function(data) {
        
            JSONdata = JSON.parse(data);
            if(JSONdata.status == 0)
            {
                //se limpian los select
                $('#makeorigen').empty();
                $('#makedestino').empty();
                $('#editorigen').empty();
                $('#editdestino').empty();

                var $option = $('<option/>')

                var agencias = JSONdata.agencias;
                for(var i = 0; i < agencias.length; i++)
                {
                    $option.val(agencias[i].id).text(agencias[i].nombre);
                    $option.clone().appendTo($('#makeorigen')); 
                    $option.clone().appendTo($('#makedestino'));
                    $option.clone().appendTo($('#editorigen'));
                    $option.clone().appendTo($('#editdestino'));
                }
            }
            else
            {
                desplegarModal(JSONdata.message);
            }
        });  
    }

    $('#agregar').on('click', function(){
        $('.modal-cover').fadeIn(80, function(){
            $('.create-p').slideDown();
            //bonus: Se limpia el input de horas
             $('#makehoras').val('');
        });
    });

    $(document).on('click', '#paq-edit', function() {
        var id = $(this).parent().parent().children('div[paq]').attr('paq');
        var horas = $(this).parent().parent().children('div[hrs]').attr('hrs');
        var origen = $(this).parent().parent().children('div[org]').attr('org');
        var destino = $(this).parent().parent().children('div[dst]').attr('dst');

        $('.modal-cover').fadeIn(80, function(){
            $('.edit-p').slideDown();
            //y se asignan los valores
            $('.edit-p').attr('paq', id).find('h3').html('Modificar Paquete '+ id);
            $('#edithoras').val(horas);
            $('#editorigen').find('option[value="'+ origen +'"]').prop('selected', true);
            $('#editdestino').find('option[value="'+ destino +'"]').prop('selected', true);
        });
    });

    $(document).on('click', '#paq-delete', function() {
        var $paq = $(this).parent().parent();
        var packdelete = window.confirm("¿Esta seguro de eliminar el paquete "+ $paq.find('.paq-name').attr('paq') +"? Los usos del paquete en la configuración serán eliminados (no afecta los traslados existentes)");

        if(packdelete == true)
        {
            $.post('http://hsas.gpoptima.net/traslados/configuration_changePacket/', { modalidad:'delete', id: $paq.find('.paq-name').attr('paq') }, function(data) {
                JSONdata = JSON.parse(data);
                desplegarModal(JSONdata.message);
                if(JSONdata.status == 0) {
                    $paq.remove();
                }
            });
        }
    });

    $("#makepkgCancel").click(function() {
        closeAddPacket();
    });

    $("#editpkgCancel").click(function() {
        closeAddPacket();
    });

    $(".modal-cover").click(function(a){
        var b=$(".create-p");
        var c=$(".edit-p");
        if(!(b.is(a.target) || c.is(a.target)) && (b.has(a.target).length === 0 && c.has(a.target).length === 0)) {
            closeAddPacket();
        }
    });

    $('#makepkgAccept').click(function() {
        var go = true;
        //se checa que el origen se encuentre ingresado
        if(go && $('#makeorigen')[0].selectedIndex == -1)
        {
            go = false;
            $('#makeorigen').focus();
        }
        //se checa que el destino se encuentre ingresado
        if(go && $('#makedestino')[0].selectedIndex == -1)
        {
            go = false;
            $('#makedestino').focus();
        }
        //se checa que el origen y destino no sean el mismo
        if(go && $('#makedestino')[0].selectedIndex == $('#makeorigen')[0].selectedIndex)
        {
            go = false;
            $('#makedestino').focus();
        }
        //se checa que las horas sean validas
        if(go && ($('#makehoras').val() == 0 || $('#makehoras').val() == ''))
        {
            go = false;
            $('#makehoras').focus();
        }

        //si los datos se ven correctos, se procede
        if(go)
        {
            $.post('http://hsas.gpoptima.net/traslados/configuration_changePacket/',
              { modalidad: 'insert', origen: $('#makeorigen').val(), destino: $('#makedestino').val(), horas: parseInt($('#makehoras').val()) },
              function(data) {
                var JSONdata = JSON.parse(data);
                if(JSONdata.status == 0)
                {
                    desplegarModal('Paquete creado satisfactoriamente');
                    //se definen las horas como int (para que salga el numero solo, sin numeros extras)
                    var horas = parseInt($('#makehoras').val());
                    //si se logro agregar, se crea el paquete recien agregado
                    var divPaq = '<div class="col-md-3 paq-grid">'+
                        '<div class="paq-botones">'+
                            '<div id="paq-edit">'+
                                '<i class="fa fa-pencil"></i>'+
                            '</div>'+
                            '<div id="paq-delete">'+
                                '<i class="fa fa-times"></i>'+
                            '</div>'+
                        '</div>'+
                        '<div class="paq-name" paq="'+ JSONdata.id +'">'+
                            'Paquete: '+JSONdata.id+
                        '</div>'+
                        '<div class="paq-horas" hrs="'+ horas +'">'+
                            ''+ horas +' horas'+
                        '</div>'+
                        '<div class="paq-origen" org="'+ $('#makeorigen option:selected').val() +'">'+
                         '<i class="fa fa-level-up icon-tr"></i>'+
                            $('#makeorigen option:selected').text()+
                        '</div>'+
                        '<div class="paq-destino" dst="'+ $('#makedestino option:selected').val() +'">'+
                            '<i class="fa fa-level-down icon-tr"></i>'+
                            $('#makedestino option:selected').text()+
                        '</div>'+
                    '</div>';
                    //se aniade al grid de paquetes
                    $('#packages-grid').append(divPaq);
                    //y se cierra la ventana modal
                    closeAddPacket();
                }
                else 
                {
                    desplegarModal(JSONdata.message);
                }    
            });
        }
    });

    $('#editpkgAccept').click(function() {
        var go = true;
        //se checa que el origen se encuentre ingresado
        if(go && $('#editorigen')[0].selectedIndex == -1)
        {
            go = false;
            $('#editorigen').focus();
        }
        //se checa que el destino se encuentre ingresado
        if(go && $('#editdestino')[0].selectedIndex == -1)
        {
            go = false;
            $('#editdestino').focus();
        }
        //se checa que el origen y destino no sean el mismo
        if(go && $('#editdestino')[0].selectedIndex == $('#editorigen')[0].selectedIndex)
        {
            go = false;
            $('#editdestino').focus();
        }
        //se checa que las horas sean validas
        if(go && ($('#edithoras').val() == 0 || $('#edithoras').val() == ''))
        {
            go = false;
            $('#edithoras').focus();
        }

        //si los datos se ven correctos, se procede
        if(go)
        {
            var id = $(this).closest('.edit-p').attr('paq');
            $.post('http://hsas.gpoptima.net/traslados/configuration_changePacket/',
              { modalidad: 'update', origen: $('#editorigen').val(), destino: $('#editdestino').val(), horas: parseInt($('#edithoras').val()), id: id },
              function(data) {
                var JSONdata = JSON.parse(data);
                if(JSONdata.status == 0)
                {
                    desplegarModal(JSONdata.message);
                    //se definen las horas como int (para que salga el numero solo, sin numeros extras)
                    var horas = parseInt($('#edithoras').val());
                    //si se logro agregar, se modifica el paquete seleccionado
                    var $paq = $('#packages-grid').find('div[paq="'+ id +'"]').parent();
                    $paq.children('.paq-horas').attr('hrs', horas).text(horas +' Horas');
                    $paq.children('.paq-origen').attr('org', $('#editorigen option:selected').val()).text($('#editorigen option:selected').text()).prepend('<i class="fa fa-level-up icon-tr"></i>');
                    $paq.children('.paq-destino').attr('dst', $('#editdestino option:selected').val()).text($('#editdestino option:selected').text()).prepend('<i class="fa fa-level-down icon-tr"></i>');;
                    //y se cierra la ventana modal
                    closeAddPacket();
                }
                else 
                {
                    desplegarModal(JSONdata.message);
                }
            });
        }
    });

    function closeAddPacket() {
        $('.modal-cover').fadeOut(100, function() {
            $('.create-p').fadeOut(100);
            $('.edit-p').fadeOut(100);
        });
    }

</script>

<?php  $this->load->view('globales/footer'); ?> 