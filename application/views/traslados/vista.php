<?php $this->load->view('globales/head'); ?>
<?php if($usuario){ $this->load->view('globales/topbar');
    $data['menu']='tras'; $this->load->view('globales/menu',$data); }?>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo base_url().'js/modalalert.js'; ?>" type="text/javascript"></script>
<style>.tree-divs-r,.tree-divs-l{background:rgba(82,111,177,1);width:6%}.tree-divs-m{background:white}.tree-divs-r,.tree-divs-m,.tree-divs-l{height:640px;margin:1px;float:left}.tree-divs-m{width:86%}.first-divs{height:85px;background:rgba(221,49,47,1);margin:1px;margin-bottom:2px;padding-top:7.5px;width:98.3%;text-align:center;color:white}.flecha{transform:scale(4,8);color:white;vertical-align:middle;margin-top:300px}.div-flecha{text-align:center}.div-flecha:hover{box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22)}.semana{height:645px;border-radius:3px}.dia{background:rgba(225,224,225,1);height:99%;border:1px solid white;float:left;border-radius:0;padding:0!important;text-align:center}.dia h3{color:rgba(82,111,177,1)}.dia .config{width:100%;height:32px}.dia div{margin-bottom:2px}.noview{display:none}.activar{width:100%!important;height:50px;margin-left:0!important;margin-top:0;border-radius:0!important}.paquete{width:100%;height:125px;margin-left:0!important;margin-top:0;border-radius:0;text-align:center}.receso{width:95%;margin-left:3%;color:white;margin-top:0;border-radius:4px;height:52px;padding:5px;text-align:center;background:rgba(61,137,59,1)}.plus{float:left;font-size:25px;margin-top:10px}.cover-activ,.cover-modif{position:absolute;top:0;left:0;z-index:10000;width:100%;height:100%;background:rgba(56,54,54,0.5);display:none}.cover-detail{position:absolute;top:0;left:0;z-index:10000;width:100%;height:100%;background:rgba(177,177,177,0.4);display:none}.mod-activ-1,.mod-activ-2,.mod-dia{width:30%;min-width:30%;background:white;margin-left:35%;margin-top:50px;padding:10px;height:350px;border-radius:4px;text-align:center;box-shadow:0 3px 6px rgba(0,0,0,0.16),0 3px 6px rgba(0,0,0,0.23)}.mod-detail{width:40%;min-width:30%;background:white;margin-left:30%;margin-top:40px;color:rgba(82,111,177,1);padding:10px;height:680px;text-align:center;box-shadow:0 19px 38px rgba(0,0,0,0.30),0 15px 12px rgba(0,0,0,0.22)}.h-inicio,.h-fin{width:100%;margin-top:5px;margin-bottom:15px!important;margin:10px;float:right}.div-lbl{float:left;width:30%;height:50px;margin-top:10px}.div-inp{width:70%;height:50px;margin-top:10px;float:left}.btn-cancel-activ{margin-left:2%;margin-top:25px}.btn-accept-activ{margin-top:25px;margin-left:10px}@media screen and (max-width:750px){.semana{overflow:scroll;height:389px}.tree-divs-m{height:390px}.div-flecha{margin-top:10px;height:380px}.flecha{transform:scale(3,13);color:white;vertical-align:middle;margin-top:200px}.tree-divs-r,.tree-divs-l{width:6%}#meses{width:80px!important;float:left!important}}</style>
</head>
<body>
<div class="first-divs">
<select name="anios" id="anios" class="input-medium">
<option value="2016">2016</option>
<option value="2017">2017</option>
<option value="2018">2018</option>
<option value="2019">2019</option>
<option value="2020">2020</option>
<option value="2021">2021</option>
<option value="2022">2022</option>
<option value="2023">2023</option>
<option value="2024">2024</option>
<option value="2025">2025</option>
<option value="2026">2026</option>
<option value="2027">2027</option>
</select>
<select name="meses" id="meses" class="input-medium">
<option value="1">Enero</option>
<option value="2">Febrero</option>
<option value="3">Marzo</option>
<option value="4">Abril</option>
<option value="5">Mayo</option>
<option value="6">Junio</option>
<option value="7">Julio</option>
<option value="8">Agosto</option>
<option value="9">Septiembre</option>
<option value="10">Octubre</option>
<option value="11">Noviembre</option>
<option value="12">Diciembre</option>
</select>
<h4>TRASLADOS OPTIMA</h4>
<h4>Semana <span id="numsemana"></span> de <span id="nombremes"></span> <span id="nombreanio"></span></h4>
</div>
<div class="tree-divs-r div-flecha left">
<span><i class="fa fa-angle-left flecha"></i></span>
</div>
<div id="containerWeeks" class="tree-divs-m">
</div>
<div class="tree-divs-l div-flecha right">
<span><i class="fa fa-angle-right flecha"></i></span>
</div>
</body>
<div class="cover-modif">
<div class="mod-dia">
<h2>Modificar Dia</h2>
<hr>
<div>
<div class="div-lbl"><h4 style="float:left">Hora de Inicio</h4></div>
<div class="div-inp"><input id="modinicio" type="time" class="form-control h-inicio"></div>
</div>
<div>
<div class="div-lbl"><h4 style="float:left">Hora de Fin</h4></div>
<div class="div-inp"><input id="modfin" type="time" class="form-control h-fin"></div>
</div>
<div>
<div class="div-lbl"><h4 style="float:left">Nombre Trasladista</h4></div>
<div class="div-inp"><input id="modnombre" type="text" class="form-control h-fin"></div>
</div>
<div id="diamodCancel" class="btn-cancel-activ btn btn-danger">CANCELAR</div>
<div id="diamodAccept" class="btn-accept-activ btn btn-primary">ACEPTAR</div>
</div>
</div>
<style>.div-auto{background:rgba(55,92,147,1);color:white;height:150px;padding-left:8px;<?=$usuario==true?"width:80%;":"width:100%;";?>float:left;margin-bottom:5px;transition:all .3s cubic-bezier(.25,.8,.25,1)}.imageloader{margin-top:40px;z-index:2}.div-auto-load{background:white;border:1px solid gray;z-index:1;color:white;height:150px;padding-left:8px;width:80%;float:left;margin-bottom:5px}.btn-save-auto{<?= $usuario==false?'display:none;':""; ?>width:19%;color:white;text-align:center;padding-top:50px;cursor:pointer;font-size:1.3em;height:150px;background:rgba(24,126,77,0.9);float:left;margin-bottom:5px;margin-left:3px;box-shadow:0 3px 6px rgba(0,0,0,0.16),0 3px 6px rgba(0,0,0,0.23);transition:all .3s cubic-bezier(.25,.8,.25,1)}.btn-save-auto:hover{background:rgba(2,93,28,0.9);box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22)}.div-auto:hover{background:rgba(28,69,118,1);box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22)}
.sel-dinamic{
    width:30%;
    margin-left: -10px;
    position:absolute;
    z-index:1;
    background:white;
    margin-top:50px;
    box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22)

    }
    #day-del {
        color: rgba(82, 111, 177, 1);
        position: absolute;
        right: 0px;
        padding: 1.5px 4px;
        margin: 2px 2px 0px 0px;
    }
    #day-del:hover {
        background-color: rgba(0, 0, 0, 0.1);
    }
    .opt-dinamic{width:100%;height:30px;color:rgba(231,55,79,1);padding:4px;font-size:1.4em;border-bottom:1px solid rgba(194,194,193,1);cursor:pointer}.opt-dinamic:hover{background:rgba(40,40,40,0.1)}.opt-none{width:100%;height:30px;color:rgba(164,163,162,1);padding:4px;font-size:1.4em;border-bottom:1px solid rgba(194,194,193,1);cursor:pointer}.opt-none:hover{background:rgba(40,40,40,0.1)}.paquete-faded{background-color:#6997bf}.paquete-faded:hover{background-color:#4b7aa0}

    @media(max-width:1366px)
    {
        .opt-dinamic {
            font-size: 11pt;
        }
    }
    @media(max-width:1280px)
    {
        .opt-dinamic {
            font-size: 10pt;
        }
    }
    @media(max-width:1024px)
    {
        .opt-dinamic {
            font-size: 8pt;
        }
    }
    </style>
<div class="cover-detail">
<div class="mod-detail">
<h2>Detalle Del Traslado</h2>
<hr>
<div class="div-auto autoForm1">
<div class="div-lbl"><h4 style="float:left">Auto 1</h4></div>
<div class="div-inp">
<input id="auto1" type="text" auto="1" class="form-control h-fin vinauto" <?= $usuario==false?"disabled":""; ?>>
<div id="sel-dinamic-1" class="sel-dinamic">
</div>
</div>
<div class="div-lbl"><h4 style="float:left">Modelo</h4></div>
<div class="div-inp"><input disabled id="modelo1" type="text" class="form-control h-fin"></div>
</div>
<div class="btn-save-auto" id="saveAuto1" auto="1">GUARDAR</div>
<hr>
<div class="div-auto autoForm2">
<div class="div-lbl"><h4 style="float:left">Auto 2</h4></div>
<div class="div-inp">
<input id="auto2" auto="2" type="text" class="form-control h-fin vinauto" <?= $usuario==false?"disabled":""; ?>>
<div id="sel-dinamic-2" class="sel-dinamic">
</div>
</div>
<div class="div-lbl"><h4 style="float:left">Modelo</h4></div>
<div class="div-inp"><input disabled id="modelo2" type="text" class="form-control h-fin"></div>
</div>
<div class="btn-save-auto" id="saveAuto2" auto="2">GUARDAR</div>
<div>
<div class="div-lbl"><h4 style="float:left">Hora Inicio</h4></div>
<div class="div-inp"><input disabled id="hinicio" type="time" class="form-control h-inicio"></div>
</div>
<div>
<div class="div-lbl"><h4 style="float:left">Hora Fin</h4></div>
<div class="div-inp"><input disabled id="hfin" type="time" class="form-control h-fin"></div>
</div>
<div>
<div class="div-lbl"><h4 style="float:left">Trasladista</h4></div>
<div class="div-inp"><input disabled id="tdtdetail" type="text" class="form-control h-fin"></div>
</div>
<div class="btn-cancel-activ btn btn-danger" onclick="downDetail()">CERRAR</div>
</div>
</div>
<script>/*<![CDATA[*/
var seriesArray=[];
var descTemp = [];
$(".vinauto").keyup(function(b){
    var c=$(this).attr("auto");
    var a=$(this).val();
    searchVins(a,c)
    });
    function selectOption(b){
        var c=$(b).attr("auto");
        var a=$("#modelo"+c);
        $.each(seriesArray,function(d,e){
            if(e[0].toLowerCase()==$(b).attr("vin").toLowerCase()){
                var passsel = true;

                    $("#auto"+c).val(e[0]).attr("nombremodelo",e[1]);
                    a.val(e[1]+" "+e[2]);
                    a.attr('color',e[2]);
                    $("#sel-dinamic-"+c).fadeOut(100)
            }
        })
    }
    
    function deleteDay(elem) {
        $e = $(elem).parent();
        var f = $e.children("h5").text();
        $.post("http://hsas.gpoptima.net/traslados/insertTransfer_removeDate/", { fecha: f }, function(r) {
            a = new Date(Date.parse(f));
            j = JSON.parse(r);
            desplegarModal(j.message);
            if(j.status == 0)
            {
                $e.find("div").remove();
                $e.find("#day-del").remove();
                $e.append('<div dia="'+a.getDay()+'" class="btn btn-warning activar" onclick="civerFadeIn(this)"><i class="fa fa-plus plus"></i><h4>ACTIVAR</h4></div>')
            }
        })
    };

    function notSelected(a){
        var b=$(a).attr("auto");
        $("#sel-dinamic-"+b).fadeOut(100);
        $("#auto"+b).val("");
        $("#modelo"+b).val("")
    }
    function searchVins(b,e){
        $("#sel-dinamic-"+e).empty();
        b=b.toLowerCase();
        var c=JSON.parse(localStorage.getItem("dataTable"));
        var a='<div auto="'+e+'" class="opt-none"  onclick="notSelected(this)">NINGUNO</div>';
        seriesArray=[];
        var d=0;
        $.each(c.vins,function(f,g){
            var i=" <div style='float:left'>"+g.vin+" | </div> ";
            var ori= " <div style='color:blue;float:left'>"+g.origen+" >> "+g.destino+" | </div> ";
            var fecha = " <div style='color:green;float:right'>"+g.fecha_solicitada+" </div> ";

        if(i.toLowerCase().indexOf(b)>=0&&d<13){
            a+='<div auto="'+e+'" class="opt-dinamic" style="z-index:1" vin="'+g.vin+'" onclick="selectOption(this)">'+i+ori+fecha+"</div>";var h=[g.vin,g.modelo,g.color];seriesArray.push(h);
            d++
            }
        });if(d==0){a=""}$("#sel-dinamic-"+e).append(a);$("#sel-dinamic-"+e).fadeIn(100)}

    function getDataTable(){
        $.get("http://hsas.gpoptima.net/traslados/getAllSerialNums/?status=0",function(b){
        var a=b;
        localStorage.setItem("dataTable",a)
        })
    }

    $(".btn-save-auto").click(function(){

        var c=$(this).attr("auto");
        var e=$(this).attr("paquete");
        var d=$(".autoForm"+c);
        var a=$("#auto"+c).val();

        var b=$("#modelo"+c).val();
        var colorin = $("#modelo"+c).attr('color');

        if(a != "")
        {
            var indexObt;
            var JSONvins = JSON.parse(localStorage.getItem("dataTable"));
            $.each(JSONvins.vins, function(cnt, elem) {
                if(a.toLowerCase() == elem.vin.toLowerCase()) {
                    indexObt = cnt;
                }
            });
            if(indexObt != undefined) {
                descTemp.push(JSONvins.vins.splice(indexObt, 1)[0]);
                localStorage.setItem("dataTable", JSON.stringify(JSONvins));
            }
        }
        else
        {
            //vin anterior
            var vinPrev = $("#"+e).attr("ns"+c);

            var indexObt;
            var JSONvins = JSON.parse(localStorage.getItem("dataTable"));
            $.each(descTemp, function(cnt, elem) {
                if(vinPrev.toLowerCase() == elem.vin.toLowerCase()) {
                    indexObt = cnt;
                }
            });
            if(indexObt != undefined) {
                JSONvins.vins.push(descTemp.splice(indexObt, 1)[0]);
                localStorage.setItem("dataTable", JSON.stringify(JSONvins));
            }
        }
        $("#loader"+c).remove();
        d.removeClass("div-auto");
        d.addClass("div-auto-load");
        d.find("div").fadeOut(100);
        d.append($('<img class="imageloader" id="loader'+c+'" src="http://hsas.gpoptima.net/assets/img/loader.gif" >'));
        $.get("http://hsas.gpoptima.net/traslados/insertTransfer_setPackage/?id="+e+"&numserie="+a+"&numero="+c,function(j){
            var g=JSON.parse(j);
            desplegarModal(g.message);
            if(g.status == 0) {
                var i=$("#"+e);
                i.attr("ns"+c,a);
                if(a!=''){
                    i.attr("m"+c,b);
                }else{
                    i.attr("m"+c,"");
                }
                var f=$("#"+e).removeClass("paquete-faded").children("h5");
                if(a==''){
                    f[(parseInt(c)+1)].innerHTML='';
                    var tmp = 0;
                    if(c==1){
                        tmp = 3;
                     }else{
                         tmp = 2;
                    }
                    if($(f[parseInt(tmp)][0]).text()==''){
                        $("#"+e).addClass("paquete-faded")
                    }
                }else{
                    f[(parseInt(c)+1)].innerHTML=a.substr(-5)+" "+$("#auto"+c).attr("nombremodelo")
                }
            }
            var h=$(".div-auto-load");
            $("#sel-dinamic-"+c).empty();
            h.find("div").fadeIn(10);
            h.removeClass("div-auto-load");
            h.addClass("div-auto");
            $(".imageloader").remove()})
            
            });

            var horario=function(){this.horas=0;this.minutos=0;this.segundos=0;this.horasString="";this.horarioResult="";this.updateHorario=function(){this.horas+=parseInt(this.horasString);if(this.minutos>=60){var e=this.minutos;var f=Math.floor(this.minutos/60);var d=((e/60)-f).toFixed(2)*60/1;this.horas+=f;this.minutos=d}var c=String(this.horas);var a=String(this.minutos);var b=String(this.segundos);if(c.length==1){c="0"+c}if(a.length==1){a="0"+a}if(b.length==1){b="0"+b}this.horarioResult=c+":"+a+":"+b}};var mes=0;var anio=0;$(document).ready(function(){var a=new Date();document.getElementById("meses").selectedIndex=a.getMonth();mes=$("#meses").val();anio=a.getFullYear();getMonthCalendar(mes,anio);getDataTable()});function setDetail(a){$("#auto1").val($(a).attr("ns1"));$("#modelo1").val($(a).attr("m1"));$("#auto2").val($(a).attr("ns2"));$("#modelo2").val($(a).attr("m2"));$("#hinicio").val($(a).attr("hi"));$("#hfin").val($(a).attr("hf"));$("#tdtdetail").val($(a).attr("tr"));$(".btn-save-auto").attr("paquete",$(a).attr("id"));$(".cover-detail").fadeIn(50)}function downDetail(){$(".cover-detail").fadeOut(100);var a=$(".div-auto-load");a.find("div").fadeIn(10);a.removeClass("div-auto-load");a.addClass("div-auto");$(".imageloader").remove()}$("#anios").change(function(){document.getElementById("meses").selectedIndex=-1});$("#meses").change(function(){mes=$("#meses").val();anio=$("#anios").val();getMonthCalendar(mes,anio)});function getMonthCalendar(a,b){$.get("http://hsas.gpoptima.net/traslados/getTransferTable/?mes="+a+"&anio="+b,function(t){$("#nombremes").html($("#meses option:selected").text());$("#nombreanio").html(b);var y=JSON.parse(t);if(y.status==0){$("#numsemana").html("1");var f=y.mes;var m="";for(var q=0;q<f.length;q++){var j="";if(q>0){j="noview"}else{j="view"}var p=f[q];m+='<div class="semana test2 '+j+'" id="2" idNav="'+q+'">';for(var n=0;n<p.dias.length;n++){var B=p.dias[n];var k=B.desactivado;if(B.activo==1){/*!marcador*/m+='<div class="dia col-md-2 col-sm-4 col-xs-6">';<?= $usuario==true?"m+= '<span id=\"day-del\" onclick=\"deleteDay(this);\"><i class=\"fa fa-times\"></i></span>';":""; ?> m+='<h3>'+B.dia+"</h3><h5>"+B.fecha+'</h5>';<?= $usuario==true?"m+='<div trasladista=\"'+B.trasladista+'\" inicio=\"'+B.horaEntrada+'\" fin=\"'+B.horaSalida+'\" class=\"btn btn-success config\" onclick=\"modifFadeIn(this)\" '+k+'><i class=\"fa fa-wrench wrench\"></i></div>';":"";?>var l=new horario();l.horas=parseInt(B.horaEntrada.substring(0,2));l.minutos=parseInt(B.horaEntrada.substring(3,5));l.segundos=parseInt(B.horaEntrada.substring(6,8));for(var w=0;w<B.paquetes.length;w++){var A=B.paquetes[w];var o=new horario();if(w>0){l.minutos+=30}else{l.horasString="0"}l.updateHorario();o.horasString="0";o.horas=l.horas+parseInt(A.horas);o.minutos=l.minutos;o.segundos=l.segundos;o.updateHorario();l.horas=o.horas;l.minutos=o.minutos;l.segundos=o.segundos;var v="";var s="";var z="";var x="";var h="";var g="";var c=true;var d=0;if(A.numeroSerie1!=""){z=A.numeroSerie1.vin.substr(12,16);v=A.numeroSerie1.vin;h=A.numeroSerie1.modelo;c=false}else{d+=1}if(A.numeroSerie2!=""){x=A.numeroSerie2.vin.substr(12,16);s=A.numeroSerie2.vin;g=A.numeroSerie2.modelo;c=false}else{d+=2}var r="";if(c||k!=""){r=" paquete-faded "}m+='<div id="'+A.id+'" ns1="'+v+'" ns2="'+s+'" tr="'+B.trasladista+'" hi="'+l.horarioResult+'" hf="'+o.horarioResult+'" m1="'+h+'" m2="'+g+'" class="paquete btn btn-primary '+r+'" onclick="setDetail(this)"'+k+'><h5 style="color:rgba(253, 252, 20, 1);">'+A.origen.agencia+'</<h5><h5 style="color:rgba(21, 255, 29, 1); ">'+A.destino.agencia+'</<h5><h5 style="color:white;">'+z+" "+h+'</<h5><h5 style="color:white;">'+x+" "+g+"</<h5></div>"}}else{m+='<div class="dia col-md-2 col-sm-4 col-xs-6"><h3>'+B.dia+"</h3><h5>"+B.fecha+"</h5>";<?= $usuario==true?"m+='<div dia=\"'+B.numdia+'\" class=\"btn btn-warning activar\" onclick=\"civerFadeIn(this)\"'+k+'><i class=\"fa fa-plus plus\"></i><h4>ACTIVAR</h4></div>'" : "";?>}m+="</div>"}m+="</div>"}$("#containerWeeks").empty().append(m)}else{desplegarModal(y.message)}}).fail(function(c){console.log(c)})}$("#activAccept1").click(function(){$(".mod-activ-1").fadeOut(100,function(){$(".mod-activ-2").fadeIn(100)})});$(".btn-cancel-activ").click(function(){$(this).parent().fadeOut(100,function(){$(this).parent().fadeOut(100);$(".sel-dinamic").fadeOut()}).delay(100).fadeIn(100)});$("#diamodCancel").click(function(){$(".cover-modif").click()});$("#diamodAccept").click(function(d){var a=$("#modnombre").val();var f=$("#modinicio").val();var c=$("#modfin").val();var b=$(".cover-modif").attr("fecha");$.post("http://hsas.gpoptima.net/traslados/insertTransfer_setTransferPerson/",{fecha:b,trasldaista:a,horainicio:f,horafin:c},function(l){var n=JSON.parse(l);desplegarModal(n.message);if(n.status==0){var p=$("#containerWeeks").find("h5:contains('"+b+"')").parent();p.children("div[trasladista]").attr("inicio",f).attr("fin",c).attr("trasladista",a);var o=p.children("div[tr]").attr("tr",a);var j=new horario();j.horas=parseInt(f.substring(0,2));j.minutos=parseInt(f.substring(3,5));j.segundos=0;for(var m=0;m<o.length;m++){var k=o[m].getAttribute("hi");var e=o[m].getAttribute("hf");var h=parseInt(e.substring(0,2))-parseInt(k.substring(0,2));var g=new horario();if(m>0){j.minutos+=30}else{j.horasString="0"}j.updateHorario();g.horasString="0";g.horas=j.horas+parseInt(h);g.minutos=j.minutos;g.segundos=j.segundos;g.updateHorario();j.horas=g.horas;j.minutos=g.minutos;j.segundos=g.segundos;o[m].setAttribute("hi",j.horarioResult);o[m].setAttribute("hf",g.horarioResult)}$(".cover-modif").click()}})});function modifFadeIn(b){var a=$(b).parent().find("h5").html();$("#modinicio").val($(b).attr("inicio"));$("#modfin").val($(b).attr("fin"));$("#modnombre").val($(b).attr("trasladista"));$(".cover-modif").fadeIn(200).attr("fecha",a)}function civerFadeIn(b){var a=$(b).parent().find("h5").html();$.post("http://hsas.gpoptima.net/traslados/insertTransfer_setDate/",{fecha:a},function(r){var i=JSON.parse(r);desplegarModal(i.message);if(i.status==0){var j='<div trasladista="" inicio="05:00" fin="22:00" class="btn btn-success config" onclick="modifFadeIn(this)"><i class="fa fa-wrench wrench"></i></div>';var g=i.paquetes;var h=new horario();h.horas=5;h.minutos=0;h.segundos=0;for(var p=0;p<g.length;p++){var t=g[p];var k=new horario();if(p>0){h.minutos+=30}else{h.horasString="0"}h.updateHorario();k.horasString="0";k.horas=h.horas+parseInt(t.horas);k.minutos=h.minutos;k.segundos=h.segundos;k.updateHorario();h.horas=k.horas;h.minutos=k.minutos;h.segundos=k.segundos;var n="";var m="";var s="";var q="";var f="";var e="";var c=true;var d=0;d+=2;var l="";if(c){l=" paquete-faded "}j+='<div id="'+t.id+'" ns1="'+n+'" ns2="'+m+'" tr="" hi="'+h.horarioResult+'" hf="'+k.horarioResult+'" m1="'+f+'" m2="'+e+'" class="paquete btn btn-primary '+l+'" onclick="setDetail(this)"><h5 style="color:rgba(253, 252, 20, 1);">'+t.origen.agencia+'</<h5><h5 style="color:rgba(21, 255, 29, 1); ">'+t.destino.agencia+'</<h5><h5 style="color:white;">'+s+" "+f+'</<h5><h5 style="color:white;">'+q+" "+e+"</<h5></div>"}var o=$(b).parent();o.append(j);o.prepend('<span id="day-del" onclick="deleteDay(this);"><i class="fa fa-times"></i></span>');$(b).remove();o.children("div[trasladista]").click()}else{if(i.status==2){$(b).remove()}}})}$(".cover-activ").click(function(a){var b=$(".mod-activ-1");if(!b.is(a.target)&&b.has(a.target).length===0){$(this).fadeOut(100,function(){$(this).children("div").fadeOut(100,function(){b.fadeIn(100)})})}});$(".cover-modif").click(function(a){var b=$(".mod-dia");if(!b.is(a.target)&&b.has(a.target).length===0){$(this).fadeOut(100,function(){$(this).children("div").fadeOut(100,function(){b.fadeIn(100)})})}});$(".cover-detail").click(function(a){var b=$(".mod-detail");if(!b.is(a.target)&&b.has(a.target).length===0){$(".sel-dinamic").fadeOut();downDetail()}});$(".div-flecha").click(function(){var b=$(this);var a=$(".view");if(b.hasClass("right")){var c=parseInt(a.attr("idNav"))+1;if($("[idNav="+c+"]").length){$("#numsemana").html(c+1);a.removeClass("view");a.fadeOut(100,function(){$("[idNav="+c+"]").addClass("view");$("[idNav="+c+"]").fadeIn(100)})}}else{var c=parseInt(a.attr("idNav"))-1;if($("[idNav="+c+"]").length){$("#numsemana").html(c+1);a.removeClass("view");a.fadeOut(100,function(){$("[idNav="+c+"]").addClass("view");$("[idNav="+c+"]").fadeIn(100)})}}});/*]]>*/</script>
<?php  $this->load->view('globales/footer'); ?>