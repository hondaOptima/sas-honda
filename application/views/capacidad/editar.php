<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='capacidad';$this->load->view('globales/menu',$menu); ?>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url();?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
        </li>
    <li>
        <i class="fa fa-user"></i>
        <a href="<?php echo base_url();?>capacidad">Capacidad</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-edit"></i>
        <a href="#">Editar</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i>Editar Factores de Capacidad <?php echo $capacidad[0]->wor_city;?>
        </div>
    </div>
	<div class="portlet-body form">
    <!-- BEGIN FORM-->
    
    
    <?php echo form_open('capacidad/actualizar/'.$capacidad[0]->woc_idcapability,'class="form-horizontal"'); ?>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">Horas de servicio (Lun. a Vier.)</label>
                    <div class="col-md-3">
                        <?php  echo form_input('weekdays',$capacidad[0]->woc_weekdays_service_hours, 
                        'id="mask_number_weekdays" class="form-control" 
                        placeholder="Horas" required');?>
                    </div>
                </div>
            </div>
        <!--/-->
        </div>
        <div class="row">                                    
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">Horas de servicio Sábados</label>
                    <div class="col-md-3">
                        <?php  echo form_input('weekends',$capacidad[0]->woc_weekends_service_hours,
                        'id="mask_decimal" class="form-control"
                        placeholder="Horas" required');?>
                    </div>
                </div>
            </div>
        <!--/-->
        </div>
         <div class="row">                                    
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">T&eacute;cnicos</label>
                    <div class="col-md-3">
                        <?php  echo form_input('tecnicos',$capacidad[0]->woc_tecnicos,
                        'id="mask_number_weekends" class="form-control"
                        placeholder="T&eacute;cnicos" required');?>
                    </div>
                </div>
            </div>
        <!--/-->
        </div>
        <div class="row">                                                    
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">Factor de Productividad</label>
                    <div class="col-md-3">
                        <?php  echo form_input('factor',$capacidad[0]->woc_productivity_factor,
                        'id="mask_decimal" class="form-control"
                        placeholder="Factor" required');?>
                    </div>
                </div>
            </div>
        <!--/--->
        </div>
        <div class="form-actions fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <a href="<?php echo base_url();?>capacidad"><button type="button" class="btn btn-default">Cancelar</button></a>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    
    
    <?php echo form_close(); ?>
    <!-- END FORM-->
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('capacidad/js/script');?> 