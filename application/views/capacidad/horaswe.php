<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='capacidad';$this->load->view('globales/menu',$menu); ?>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        	<a href="<?php echo base_url();?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-filter"></i>
        	<a href="<?php echo base_url();?>capacidad">Capacidad</a>
        <i class="fa fa-angle-right"></i>
    </li>
    
     <li>
        <i class="fa  fa-clock-o"></i>
        	<a href="#">Horas en la Semana</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<?php echo $flash_message;?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row">
<div class="col-md-4 col-sm-6">
<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa  fa-clock-o"></i>Listas de Horas
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
            <thead>
            <tr style="font-weight:bold">
            <th>
            <b>Taller</b>
            </th>
            <th>
            <b>Hora</b>
            </th>
            
            <th>
            <b>Control</b>
            </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($horaswd as $c) { ?>
            <tr class="odd gradeX">
                <td>
                	<?php echo $c->wor_city; ?>
                </td>
                
                <td>
                <?php echo $c->hor_hora; ?>
                </td>
               
                <td>
                   
                    
                    <a href="<?php echo base_url().'capacidad/eliminarHoraE/'.$c->hor_ID.'/'.$c->hor_wor_idWorkshop;?>" 
                        class="btn btn-xs btn-danger" onClick="return confirm('Está seguro que desea borrar: <?php echo $c->hor_hora; ?>?');">
                    <i class="fa  fa-trash-o"></i>
                    Eliminar</a>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
    </div>
    
    <div class="col-md-4 col-sm-6">
<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa  fa-clock-o"></i>Agregar Nueva Hora
        </div>
    </div>
    <div class="portlet-body">
       <?php echo form_open('capacidad/agregarHora/','class="form-horizontal"'); ?>
       <input type="hidden" name="taller" value="<?php echo $horaswd[0]->hor_wor_idWorkshop;?>">
       <input type="hidden" name="dia" value="<?php echo $horaswd[0]->hor_dia;?>">       
        <ul style="list-style:none">
          <li style=" font-weight:bold">Hora:<br>
            <select name="nhora">
            <?php for($h=7; $h<24; $h++) { if($h<10){$h='0'.$h;}else{}?>
            <option value="<?php echo $h;?>"><?php echo $h;?></option>
            <?php } ?>
            </select>
          </li>
          <li style=" font-weight:bold">Minuto:<br>
            <select name="nmin">
            <?php for($m=0; $m<60; $m++) { if($m<10){$m='0'.$m;}else{}?>
            <option value="<?php echo $m;?>"><?php echo $m;?></option>
            <?php } ?>
            </select>
          </li>
          <li><br>
            <br>
            <input type="submit" class="btn btn-primary btn-block" value="Guardar"></li>
        </ul>
       
       </form>
    </div>
</div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('capacidad/js/script');?> 
