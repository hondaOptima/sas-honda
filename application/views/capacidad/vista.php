<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='capacidad';$this->load->view('globales/menu',$menu); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-filter"></i> <a href="#">Capacidad</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-filter"></i>Factores de Capacidad | Lunes a Viernes </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
              <th> <b>Taller</b> </th>
              <th> <b>Hrs de Lun-Vier</b> </th>
             
              <th> <b>Técnicos</b> </th>
              <th> <b>Factor</b> </th>
              <th> <b>Horas Totales</b> </th>
              <th> <b>Control</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($capacidades as $c) { ?>
            <tr class="odd gradeX" style="text-align:center">
              <td style="text-align:left"><?php echo $c->wor_city; ?></td>
              <td><a href="<?php echo base_url();?>capacidad/horaswd/<?php echo $c->wor_idWorkshop;?>"> <?php echo $hor=$c->woc_weekdays_service_hours; ?> Horas</a></td>
              
              <td><?php echo $tec=$c->woc_tecnicos; ?></td>
              <td><?php echo $fac=$c->woc_productivity_factor; ?></td>
              <td><?php echo (($hor * $tec) * $fac); ?></td>
              <td><a href="<?php echo base_url();?>capacidad/editar/<?php echo $c->woc_idcapability; ?>" class="btn btn-xs btn-default"> <i class="fa fa-edit"></i> Editar</a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  
  
   <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-filter"></i>Factores de Capacidad | Sabados </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
              <th> <b>Taller</b> </th>
             
              <th> <b>Hrs. Sabado</b> </th>
              <th> <b>Técnicos</b> </th>
              <th> <b>Factor</b> </th>
              <th> <b>Horas Totales</b> </th>
              <th> <b>Control</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($capacidades as $c) { ?>
            <tr class="odd gradeX" style="text-align:center">
              <td style="text-align:left"><?php echo $c->wor_city; ?></td>
              
              <td><a href="<?php echo base_url();?>capacidad/horaswe/<?php echo $c->wor_idWorkshop;?>"> <?php echo $hor=$c->woc_weekends_service_hours; ?> Horas</a></td>
              <td><?php echo $tec=$c->woc_tecnicos; ?></td>
              <td><?php echo $fac=$c->woc_productivity_factor; ?></td>
              <td><?php echo (($hor * $tec) * $fac); ?></td>
              <td><a href="<?php echo base_url();?>capacidad/editar/<?php echo $c->woc_idcapability; ?>" class="btn btn-xs btn-default"> <i class="fa fa-edit"></i> Editar</a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('produccion/js/script');?>
