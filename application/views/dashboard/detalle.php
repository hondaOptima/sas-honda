<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>
 <?php
$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
?>
<form name="form1" method="get" action="<?php echo base_url();?>dashboard">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-gears"></i> <a href="#">Detalle Dashboard <?php echo $_GET['cd']?></a> <i class="fa fa-angle-right"></i> </li>
  
 
</ul>
</form>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET--> 
    
          <div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>VENTA X DIA X ASESOR | <?php echo $_GET['cd'];?> </div>
    </div>
    <div class="portlet-body"   >
    <div id="gradoce"></div>
    </div></div>
    </div>
    
    </div> 
    
    
    <div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>VENTA ACUMULADA | <?php echo $_GET['cd'];?></div>
    </div>
    <div class="portlet-body"   >
    <div id="gratrece"></div>
    </div></div>
    </div>
    
    </div>
    
    <div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>HORAS X OS | <?php echo $_GET['cd'];?></div>
    </div>
    <div class="portlet-body"   >
    <div id="gracatorce"></div>
    </div></div>
    </div>
    
    </div> 
    
    

    
    
        
    


<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('productividad/js/script');?>
<?php
if($_GET['cd']=='Ensenada'){$taller=1;}
if($_GET['cd']=='Mexicali'){$taller=2;}
if($_GET['cd']=='Tijuana'){$taller=3;}
if($taller==1){$serie=1;}
if($taller==2){$serie=6;}
if($taller==3){$serie=7;}
if($taller==1){$pmes=69456.66;}
if($taller==2){$pmes=641977.37;}
if($taller==3){$pmes=718717.11;}
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>    
 <script type="text/javascript">
     
google.load("visualization", "1.1", {packages:["bar","corechart"]});
//venta por dia por asesor
google.setOnLoadCallback(drawVisualizationxabcd);
function drawVisualizationxabcd() {
  // Some raw data (not necessarily accurate)
  var dataxabcd = google.visualization.arrayToDataTable([
  
<?php 
//cabeceraz de dias y asesores
echo '["Dia",';
foreach($usuario as $us){if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
echo '"'.$us->sus_name.'",';
}}
echo '"Objetivo"],';
   
  //lectura de dias y resultados por asesor (agente) 
for($x=1; $x <=$lista;  $x++){	
$nam=$this->Dashboardmodel->nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{

echo'["'.$x.'",	';
foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;//numero de agente

echo ''.$this->Dashboardmodel->verdia($desglocem,$age,$x,$mes,$anio).',';

 }}//end if taller y usuarios

if($nam=='Sab'){$obj=5095.5;}else{$obj=10191;}
echo ''.$obj.'],';
	}
}
?>
  ]);

  var options = {
		   
		   chartArea:{left:'5%',top:'10%',width:'90%',height:'70%'},
    seriesType: "bars",
	legend: { position: 'top', maxLines: 3 },
    series: {<?php echo $serie;?>: {type: "line"}}
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ComboChart(document.getElementById('gradoce'));
  chart.draw(dataxabcd, options);
}	
	
//VENTA ACUMULADA
google.setOnLoadCallback(drawVisualizationxabcde);
function drawVisualizationxabcde() {
  // Some raw data (not necessarily accurate)
  var dataxabcde = google.visualization.arrayToDataTable([
  
<?php 
//cabeceraz de dias y asesores
echo '["Dia",';
foreach($usuario as $us){if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
echo '"'.$us->sus_name.'",';
}}
echo '"Objetivo"],';
   
  //lectura de dias y resultados por asesor (agente) acumulado

$sodiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=$this->Dashboardmodel->nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{

echo'["'.$x.'",	';
$sverdia=0;
$conase=0; 
foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;//numero de agente
$conase++;


if($x<=date('d')){
$sverdia=$this->Dashboardmodel->verdiaacu($desglocem,$age,$x,$mes,$anio);}
else{$sverdia=0;}

echo ''.$sverdia.',';

 }}//end if taller y usuarios

if($nam=='Sab'){ 
$div=number_format($pmes / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($pmes / $diastra, 2, '.', '');}
$odiv=$div / 3;
$sodiv+=$odiv;

echo ''.$sodiv.'],';
	}
}
?>
  ]);

  var options = {
		   
		   chartArea:{left:'5%',top:'10%',width:'90%',height:'70%'},
    seriesType: "bars",
	legend: { position: 'top', maxLines: 3 },
    series: {<?php echo $serie;?>: {type: "line"}}
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ComboChart(document.getElementById('gratrece'));
  chart.draw(dataxabcde, options);
}



//VENTA HORAS POR os mexicali
google.setOnLoadCallback(drawVisualizationxabcdef);
function drawVisualizationxabcdef() {
  // Some raw data (not necessarily accurate)
  var dataxabcdef = google.visualization.arrayToDataTable([
  
<?php 
//cabeceraz de dias y asesores
echo '["Dia",';
foreach($usuario as $us){if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
echo '"'.$us->sus_name.'",';
}}
echo '"Objetivo"],';
   
  //lectura de dias y resultados por asesor (agente) acumulado

$sodiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=$this->Dashboardmodel->nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{

echo'["'.$x.'",	';
$sverdia=0; 
foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;//numero de agente
if($x<=date('d')){
$sverdia=$this->Dashboardmodel->horasase($desglocem,$age,$x,$mes,$anio);}
else{$sverdia=0;}
echo ''.$sverdia.',';

 }}//end if taller y usuarios



echo '3.5],';
	}
}
?>
  ]);

  var options = {
		   
		   chartArea:{left:'5%',top:'10%',width:'90%',height:'70%'},
    seriesType: "bars",
	legend: { position: 'top', maxLines: 3 },
    series: {<?php echo $serie;?>: {type: "line"}}
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ComboChart(document.getElementById('gracatorce'));
  chart.draw(dataxabcdef, options);
}

	  
    </script>
