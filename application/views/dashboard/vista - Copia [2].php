<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>

<form name="form1" method="get" action="<?php echo base_url();?>dashboard">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-gears"></i> <a href="#">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  
 <li class="pull-right" style="margin-top:-8px;">
    <input type="submit" value="Consultar" class="btn">
    </li>
 

  <li class="pull-right" style=" margin-left:8px; margin-top:1px;">
   
  <select name="dia">
<?php for($x=1; $x<= 31; $x++){

if($dia==$x){$dsel='selected';}else{$dsel='';}
echo '<option value="'.$x.'" '.$dsel.'>'.$x.'</option>';	
	}?>
  </select>
   </li><li class="pull-right" style=" margin-left:8px; margin-top:1px;">
  
  <select name="mes">
 
  <?php
  foreach($meses as $numm=> $nommes){
if($numm==$mes){$sel='selected="selected"';}else{$sel='';}	  
echo'<option value="'.$numm.'"  '.$sel.'>'.$nommes.'</option>';	  
	  }
  
  ?>
  </select>
   </li>
   
</ul>
</form>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<?php
//$acumulado

//ENSENADA 2015	
$VMOE=$ppto[0]->res;
$VRFE=$ppto[1]->res;
$VMTE=$ppto[2]->res;
//ENSENADA 2014
$AVMOE=$ppto[3]->res;
$AVRFE=$ppto[4]->res;
$AVMTE=$ppto[5]->res;
//MEXCIALI 2015
$VMO=$ppto[6]->res;
$VRF=$ppto[7]->res;
$VMT=$ppto[8]->res;
//MEXICLAI 2014	
$AVMO=$ppto[9]->res;
$AVRF=$ppto[10]->res;
$AVMT=$ppto[11]->res;
//TIJUANA 2015	
$VMOT=$ppto[12]->res;
$VRFT=$ppto[13]->res;
$VMTT=$ppto[14]->res;
//TIJUANA 2014
$AVMOT=$ppto[15]->res;
$AVRFT=$ppto[16]->res;
$AVMTT=$ppto[17]->res;
// INFORMACION REAL DEL DESGLOCE DE VENTAS
$real=$this->Dashboardmodel->realpormes($desglocem,'TIJUANA');
$realm=$this->Dashboardmodel->realpormes($desglocem,'Mexicali');
$reale=$this->Dashboardmodel->realpormes($desglocem,'Ensenada');


//HORAS POR ORDEN DE SERVICIO	
$hosr=array();
for($r=1; $r<=3; $r++){
$hosr[]=$this->Dashboardmodel->hos($usuario,$lista,$desglocem,$r,'03',$anio);	
	}
	//print_r($hosr);
//HORAS POR ORDEN DE SERVICIO ACUMULADO
$ahosr=array();
for($r=1; $r<=3; $r++){
$ahosr[]=$this->Dashboardmodel->hos($usuario,$lista,$desgloce,$r,'03',$anio);	
	}	

//CITAS
$ci=$this->Dashboardmodel->citas($citasm,$mes,$dia);
$cit=$this->Dashboardmodel->citas($citast,$mes,$dia);
$cie=$this->Dashboardmodel->citas($citase,$mes,$dia);	

?>



<div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>VENTAS TOTALES DEL MES POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="grauno"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>VENTAS ACUMULADO POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="grados"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
    
    
<div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>MEZCLA DE VENTA REAL | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="gratres"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>MEZCLA DE VENTA REAL | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="gracuatro"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>MEZCLA DE VENTA REAL | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="gracinco"></div>
    </div></div>
    
    
    </div>
    
    
    </div>

       <div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle-o"></i>OS | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="granueve"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle-o"></i>OS | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="gradiez"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle-o"></i>OS | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="graonce"></div>
    </div></div>
    
    
    </div>
    
    
    </div>    
    
    
    <div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CITAS | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="graseis"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CITAS | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="grasiete"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CITAS | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="graocho"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
    
    
    
    
    
       <div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CAMPAÑAS | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="campTij"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CAMPAÑAS | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="campMex"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CAMPAÑAS | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="campEns"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
    
    
<!--<div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>HORAS POR OS DEL MES POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="gradoce"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>HORAS POR OS ACUMULADO POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="gratrece"></div>
    </div></div>
    
    
    </div>
    
    
    </div>-->
    
    
    
<!--    <div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>OCUPACION DEL TALLER DEL DIA </div>
    </div>
    <div class="portlet-body"   >
    <div id="gracatorce"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>OCUPACION DE TALLER DEL MES </div>
    </div>
    <div class="portlet-body"   >
    <div id="graquince"></div>
    </div></div>
    
    
    </div>-->
    
    <?php
	

	
	
    
	//utlizacion por dia
/*$suma=0;	
for($d=1; $d<=date('d'); $d++){
if($d<10){$d='0'.$d;}	
//$res=$this->Productividadmodel->capacidadtaller('2015-03-'.$d.'',3,$capacidad);	
//$res=$this->Productividadmodel->utilizacion('2015-03-'.$d.'',3);	
echo $res.'-2015/03/'.$d.'<br>';
$suma+=$res;	
	}
echo $suma;	*/
	?>
    </div>
  

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('productividad/js/script');?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

        
     <script type="text/javascript">
     google.load("visualization", "1", {packages:["corechart"]});
	  //google.load("visualization", "1.1", {packages:["bar","corechart"]});

	  google.setOnLoadCallback(drawVisualization);
function drawVisualization() {
	
  // Lista de valores del mes
  var mes = google.visualization.arrayToDataTable([
    ['Descripcion', 'Ventas', 'Presupuesto', 'Ventas 2014'],
    ['Tijuana',  
	<?php echo $resa=$real['most'] + $real['mano'] + $real['refac']+ $real['tot'];?>,
	 <?php echo $pptoTotal[4]->total;?>,   
	<?php echo $pptoTotal[5]->total;?>],
    ['Mexicali', 
	<?php echo $resb=$realm['most'] + $realm['mano'] + $realm['refac']+ $realm['tot'];?>,
	<?php echo $pptoTotal[2]->total;?>,        
	<?php echo $pptoTotal[3]->total;?>],
    ['Ensenada', 
	<?php echo $resc=$reale['most'] + $reale['mano'] + $reale['refac']+ $reale['tot'];?> ,  
	 <?php echo $pptoTotal[0]->total;?>,      
	 <?php echo $pptoTotal[1]->total;?>]
  ]);
  
  //Lista de valores acumulados
   var acumulado = google.visualization.arrayToDataTable([
    ['Descripcion', 'Ventas', 'Presupuesto', 'Ventas 2014'],
   ['Tijuana',  
	<?php  echo $resb=$desglocea[0]->mostrador + $desglocea[0]->mano + $desglocea[0]->refacciones;?>,
	<?php echo $pptoa[4]->total;?>,
	<?php echo $pptoa[5]->total;?>],
    ['Mexicali', 
	<?php  echo $resb=$desglocea[1]->mostrador + $desglocea[1]->mano + $desglocea[1]->refacciones;?>,
	<?php echo $pptoa[2]->total;?>,
	<?php echo $pptoa[3]->total;?>],
    ['Ensenada', 
	<?php  echo $resb=$desglocea[2]->mostrador + $desglocea[2]->mano + $desglocea[2]->refacciones;?>,
	<?php echo $pptoa[0]->total;?>,
	<?php echo $pptoa[1]->total;?>]
  ]);
  

  var optionsM = {
      hAxis: {title: 'Meses', titleTextStyle: {color: 'red'}},
    legend: { position: 'top', maxLines: 3 },
	chartArea:{ width:'90%'},
	bar: {groupWidth: "50"},
  };
  
    var optionsA = {
    hAxis: {title: 'Meses', titleTextStyle: {color: 'red'}},
    legend: { position: 'top', maxLines: 3 },
	chartArea:{ width:'90%'},
	bar: {groupWidth: "50"},
  };

  var chartM = new google.visualization.ColumnChart(document.getElementById('grauno'));
  var chartA = new google.visualization.ColumnChart(document.getElementById('grados'));  
  //var chartM = new google.charts.Bar(document.getElementById('grauno'));
  //var chartA = new google.charts.Bar(document.getElementById('grados'));
  function selectHandler() {
          var selectedItem = chartM.getSelection()[0];
          if (selectedItem) {
            var topping = mes.getValue(selectedItem.row,0);
			 window.location="<?php echo base_url();?>dashboard/detalle/?cd="+topping+"";
			}
        }

        google.visualization.events.addListener(chartM, 'select', selectHandler); 
  
  
  chartM.draw(mes, optionsM);
  chartA.draw(acumulado, optionsA);
}

//############################################## mezcal de venta #########################



//mezcla de venta ensenada
google.setOnLoadCallback(drawVisualizationxabccc);
function drawVisualizationxabccc() {
  // Some raw data (not necessarily accurate)
  var dataxabccc = google.visualization.arrayToDataTable([
    ['Ensenada', 'Real','PPTO','2014'],
    ['Mano de Obra', <?php echo $reale['mano'];?>,<?php echo $VMOE;?>,<?php echo $AVMOE;?>],
    ['Refacciones', <?php echo $reale['refac'];?>,<?php echo $VRFE;?>,<?php echo $AVRFE;?>],
    ['Mostrador',<?php echo $reale['most'];?>,<?php echo $VMTE;?>,<?php echo $AVMTE;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gracinco'));
  chart.draw(dataxabccc, options);
}


//mezcla de venta tijuana
google.setOnLoadCallback(drawVisualizationxa);
function drawVisualizationxa() {
  // Some raw data (not necessarily accurate)
  var dataxa = google.visualization.arrayToDataTable([
    ['Tijuana', 'Real','PPTO','2014'],
    ['Mano de Obra', <?php echo $real['mano'];?>,<?php echo $VMOT;?>,<?php echo $AVMOT;?>],
    ['Refacciones', <?php echo $real['refac'];?>,<?php echo $VRFT;?>,<?php echo $AVRFT;?>],
    ['Mostrador',<?php echo $real['most'];?>,<?php echo $VMTT;?>,<?php echo $AVMTT;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gratres'));
  chart.draw(dataxa, options);
}	


//mezcla de venta mexicali
google.setOnLoadCallback(drawVisualizationxab);
function drawVisualizationxab() {
  // Some raw data (not necessarily accurate)
  var dataxab = google.visualization.arrayToDataTable([
    ['Mexicali', 'Real','PPTO','2014'],
    ['Mano de Obra', <?php echo $realm['mano'];?>,<?php echo $VMO;?>,<?php echo $AVMO;?>],
    ['Refacciones', <?php echo $realm['refac'];?>,<?php echo $VRF;?>,<?php echo $AVRF;?>],
    ['Mostrador',<?php echo $realm['most'];?>,<?php echo $VMT;?>,<?php echo $AVMT;?>]
  ]);


//var formatter = new google.visualization.NumberFormat({prefix: '$'});
//formatter.format(dataxab, 1);

  var options = {
	 // vAxis: [0: {format: '#,###'}, 1: {format: '#%'}],
    //vAxis: {title: 'VALUES', format: '\u00A4 #,###0.00'},
	// pieSliceText: 'value',
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gracuatro'));
  chart.draw(dataxab, options);
}

//###################################### ORDENES DE SERVICIO##########################
google.load("visualization", "1", {packages:["corechart"]});	
//ocupacion del dia
google.setOnLoadCallback(drawVisualizationxabc);
function drawVisualizationxabc() {
  // Some raw data (not necessarily accurate)
  var dataxabc = google.visualization.arrayToDataTable([
    ['Ocupacion del dia', 'Real','Objetivo',],
    ['Tijuana', <?php echo $rutij;?>,<?php echo $tctaller;?>],
    ['Mexicali', <?php echo $rumex;?>,<?php echo $mctaller;?>],
    ['Ensenada',<?php echo $ruens;?>,<?php echo $ectaller;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gracatorce'));
  chart.draw(dataxabc, options);
}


google.load("visualization", "1", {packages:["corechart"]});	
//ocupacion del dia
google.setOnLoadCallback(drawVisualizationxabcc);
function drawVisualizationxabcc() {
  // Some raw data (not necessarily accurate)
  var dataxabcc = google.visualization.arrayToDataTable([
    ['Ocupacion del dia', 'Real','Objetivo',],
    ['Tijuana', <?php echo $hosr[2];?>,<?php echo $hos;?>],
    ['Mexicali', <?php echo $hosr[1];?>,<?php echo $hos;?>],
    ['Ensenada',<?php echo $hosr[0];?>,<?php echo $hos;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   series: {1: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gradoce'));
  chart.draw(dataxabcc, options);
}


	 
	 // HORAS POR os DEL DIA


	  
    </script>
    
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]}); 
	 
	 
	  

 google.setOnLoadCallback(drawChartc);
      function drawChartc() {

        var datac = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Asistieron',  <?php echo $ci['r'];?>],
          ['Faltaron',      <?php echo $ci['f'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartc= new google.visualization.PieChart(document.getElementById('grasiete'));

        chartc.draw(datac, options);
      }
	  
	  google.setOnLoadCallback(drawChartca);
      function drawChartca() {

        var dataca = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Asistieron',  <?php echo $cit['r'];?>],
          ['Faltaron',      <?php echo $cit['f'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartca= new google.visualization.PieChart(document.getElementById('graseis'));

        chartca.draw(dataca, options);
      }
      
	  
	  google.setOnLoadCallback(drawChartcb);
      function drawChartcb() {

        var datacb = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Asistieron',  <?php echo $cie['r'];?>],
          ['Faltaron',      <?php echo $cie['f'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcb= new google.visualization.PieChart(document.getElementById('graocho'));

        chartcb.draw(datacb, options);
      }
	  
	  
	  google.setOnLoadCallback(drawChartcba);
      function drawChartcba() {

        var datacba = google.visualization.arrayToDataTable([
          ['Internas y Seguros', 'Mexicali'],
          ['Internas',  <?php echo $realm['inte'];?>],
          ['Seguros',     <?php echo $realm['segu'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcba= new google.visualization.PieChart(document.getElementById('gradiez'));

        chartcba.draw(datacba, options);
      }
	  
	  
	  google.setOnLoadCallback(drawChartcbat);
      function drawChartcbat() {

        var datacbat = google.visualization.arrayToDataTable([
          ['Internas y Seguros', 'Tijuana'],
          ['Internas',  <?php echo $real['inte'];?>],
          ['Seguros',     <?php echo $real['segu'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcbat= new google.visualization.PieChart(document.getElementById('granueve'));

        chartcbat.draw(datacbat, options);
      }
	  
	   google.setOnLoadCallback(drawChartcbate);
      function drawChartcbate() {

        var datacbate = google.visualization.arrayToDataTable([
          ['Internas y Seguros', 'Ensenada'],
          ['Internas',  <?php echo $reale['inte'];?>],
          ['Seguros',     <?php echo $reale['segu'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcbate= new google.visualization.PieChart(document.getElementById('graonce'));

        chartcbate.draw(datacbate, options);
      }
	  
	  
	  
	   google.setOnLoadCallback(drawChartcbaww);
      function drawChartcbaww() {

        var datacbaww = google.visualization.arrayToDataTable([
          ['Descripcion', 'Total'],
          ['Pendientes [<?= $campanias[0]->pendientes?>]',     <?= $campanias[0]->pendientes?>],
          ['Realizadas [<?= $campanias[0]->realizadas?>]',     <?= $campanias[0]->realizadas?>],
		  ['Canceladas [<?= $campanias[0]->canceladas?>]',     <?= $campanias[0]->canceladas?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcbaww= new google.visualization.PieChart(document.getElementById('campTij'));

        chartcbaww.draw(datacbaww, options);
      }
	  


      google.setOnLoadCallback(drawChartcbaqq);
      function drawChartcbaqq() {

        var datacbaqq = google.visualization.arrayToDataTable([
          ['Descripcion', 'Total'],
           ['Pendientes[<?= $campanias[2]->pendientes?>]',     <?= $campanias[2]->pendientes?>],
          ['Realizadas [<?= $campanias[2]->realizadas?>]',     <?= $campanias[2]->realizadas?>],
		  ['Canceladas [<?= $campanias[2]->canceladas?>]',     <?= $campanias[2]->canceladas?>]	  
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	
        };

        var chartcbaqq= new google.visualization.PieChart(document.getElementById('campMex'));

        chartcbaqq.draw(datacbaqq, options);
      }
      
	  
	   google.setOnLoadCallback(drawChartcbaee);
      function drawChartcbaee() {

        var datacbaee = google.visualization.arrayToDataTable([
          ['Descripcion', 'Total'],
          ['Pendientes [<?= $campanias[1]->pendientes?>]',     <?= $campanias[1]->pendientes?>],
          ['Realizadas [<?= $campanias[1]->realizadas?>]',     <?= $campanias[1]->realizadas?>],
		  ['Canceladas [<?= $campanias[1]->canceladas?>]',     <?= $campanias[1]->canceladas?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcbaee= new google.visualization.PieChart(document.getElementById('campEns'));

        chartcbaee.draw(datacbaee, options);
      }

    </script>