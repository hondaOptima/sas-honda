<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>
 <?php
$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
?>
<form name="form1" method="get" action="<?php echo base_url();?>dashboard">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-gears"></i> <a href="#">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  
 <li class="pull-right" style="margin-top:-8px;">
    <input type="submit" value="Consultar" class="btn">
    </li>
  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" >
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
  </li>

  <li class="pull-right" style=" margin-left:8px; margin-top:1px;">
   
  <select name="dia">
<?php for($x=1; $x<= $dias; $x++){

if($dia==$x){$dsel='selected';}else{$dsel='';}
echo '<option value="'.$x.'" '.$dsel.'>'.$x.'</option>';	
	}?>
  </select>
   </li><li class="pull-right" style=" margin-left:8px; margin-top:1px;">
  
  <select name="mes">
 
  <?php
  foreach($meses as $numm=> $nommes){
if($numm==$mes){$sel='selected="selected"';}else{$sel='';}	  
echo'<option value="'.$numm.'"  '.$sel.'>'.$nommes.'</option>';	  
	  }
  
  ?>
  </select>
   </li>
   
</ul>
</form>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<?php
//VARIABLES DE PRESUPUESTOS
//ENSENADA 2015	
$VMOE=$ppto[0]->res;
$VRFE=$ppto[1]->res;
$VRTE=$ppto[2]->res;
$VRGE=$ppto[3]->res;
$VRSE=$ppto[4]->res;
$VMTE=$ppto[5]->res;
$CVMOE=$ppto[6]->res;
$CVRFE=$ppto[7]->res;
$CVMTE=$ppto[8]->res;
$CUMOE=$ppto[9]->res;
//ENSENADA 2014
$AVMOE=$ppto[10]->res;
$AVRFE=$ppto[11]->res;
$AVRTE=$ppto[12]->res;
$AVRGE=$ppto[13]->res;
$AVRSE=$ppto[14]->res;
$AVMTE=$ppto[15]->res;
$ACVMOE=$ppto[16]->res;
$ACVRFE=$ppto[17]->res;
$ACVMTE=$ppto[18]->res;
$ACUMOE=$ppto[19]->res;
//MEXCIALI 2015
$VMO=$ppto[20]->res;
$VRF=$ppto[21]->res;
$VRT=$ppto[22]->res;
$VRG=$ppto[23]->res;
$VRS=$ppto[24]->res;
$VMT=$ppto[25]->res;
$CVMO=$ppto[26]->res;
$CVRF=$ppto[27]->res;
$CVMT=$ppto[28]->res;
$CUMO=$ppto[29]->res;	
//MEXICLAI 2014	
$AVMO=$ppto[30]->res;
$AVRF=$ppto[31]->res;
$AVRT=$ppto[2]->res;
$AVRG=$ppto[33]->res;
$AVRS=$ppto[34]->res;
$AVMT=$ppto[35]->res;
$ACVMO=$ppto[36]->res;
$ACVRF=$ppto[37]->res;
$ACVMT=$ppto[38]->res;
$ACUMO=$ppto[39]->res;

//TIJUANA 2015	
$VMOT=$ppto[40]->res;
$VRFT=$ppto[41]->res;
$VRTT=$ppto[42]->res;
$VRGT=$ppto[43]->res;
$VRST=$ppto[44]->res;
$VMTT=$ppto[45]->res;
$CVMOT=$ppto[46]->res;
$CVRFT=$ppto[47]->res;
$CVMTT=$ppto[48]->res;
$CUMOT=$ppto[49]->res;
//TIJUANA 2014
$AVMOT=$ppto[50]->res;
$AVRFT=$ppto[51]->res;
$AVRTT=$ppto[52]->res;
$AVRGT=$ppto[53]->res;
$AVRST=$ppto[54]->res;
$AVMTT=$ppto[55]->res;
$ACVMOT=$ppto[56]->res;
$ACVRFT=$ppto[57]->res;
$ACVMTT=$ppto[58]->res;
$ACUMOT=$ppto[59]->res;	

################################### PRESUPUESTOS ACUMULADOS######################################

//ENSENADA 2015	
$KVMOE=$pptoa[0]->total;
$KVRFE=$pptoa[1]->total;
$KVRTE=$pptoa[2]->total;
$KVRGE=$pptoa[3]->total;
$KVRSE=$pptoa[4]->total;
$KVMTE=$pptoa[5]->total;
$KCVMOE=$pptoa[6]->total;
$KCVRFE=$pptoa[7]->total;
$KCVMTE=$pptoa[8]->total;
$KCUMOE=$pptoa[9]->total;
//ENSENADA 2014
$KAVMOE=$pptoa[10]->total;
$KAVRFE=$pptoa[11]->total;
$KAVRTE=$pptoa[12]->total;
$KAVRGE=$pptoa[13]->total;
$KAVRSE=$pptoa[14]->total;
$KAVMTE=$pptoa[15]->total;
$KACVMOE=$pptoa[16]->total;
$KACVRFE=$pptoa[17]->total;
$KACVMTE=$pptoa[18]->total;
$KACUMOE=$pptoa[19]->total;
//MEXCIALI 2015
$KVMO=$pptoa[20]->total;
$KVRF=$pptoa[21]->total;
$KVRT=$pptoa[22]->total;
$KVRG=$pptoa[23]->total;
$KVRS=$pptoa[24]->total;
$KVMT=$pptoa[25]->total;
$KCVMO=$pptoa[26]->total;
$KCVRF=$pptoa[27]->total;
$KCVMT=$pptoa[28]->total;
$KCUMO=$pptoa[29]->total;	
//MEXICLAI 2014	
$KAVMO=$pptoa[30]->total;
$KAVRF=$pptoa[31]->total;
$KAVRT=$pptoa[2]->total;
$KAVRG=$pptoa[33]->total;
$KAVRS=$pptoa[34]->total;
$KAVMT=$pptoa[35]->total;
$KACVMO=$pptoa[36]->total;
$KACVRF=$pptoa[37]->total;
$KACVMT=$pptoa[38]->total;
$KACUMO=$pptoa[39]->total;

//TIJUANA 2015	
$KVMOT=$pptoa[40]->total;
$KVRFT=$pptoa[41]->total;
$KVRTT=$pptoa[42]->total;
$KVRGT=$pptoa[43]->total;
$KVRST=$pptoa[44]->total;
$KVMTT=$pptoa[45]->total;
$KCVMOT=$pptoa[46]->total;
$KCVRFT=$pptoa[47]->total;
$KCVMTT=$pptoa[48]->total;
$KCUMOT=$pptoa[49]->total;
//TIJUANA 2014
$KAVMOT=$pptoa[50]->total;
$KAVRFT=$pptoa[51]->total;
$KAVRTT=$pptoa[52]->total;
$KAVRGT=$pptoa[53]->total;
$KAVRST=$pptoa[54]->total;
$KAVMTT=$pptoa[55]->total;
$KACVMOT=$pptoa[56]->total;
$KACVRFT=$pptoa[57]->total;
$KACVMTT=$pptoa[58]->total;
$KACUMOT=$pptoa[59]->total;

// INFORMACION REAL DEL DESGLOCE DE VENTAS


//HORAS POR ORDEN DE SERVICIO	
$hosmex=$this->Dashboardmodel->hos($usuario,$lista,$desgloce,2,$mes,$anio);
$hosens=$this->Dashboardmodel->hos($usuario,$lista,$desgloce,1,$mes,$anio);
$hostij=$this->Dashboardmodel->hos($usuario,$lista,$desgloce,3,$mes,$anio);

//CITAS
$ci=$this->Dashboardmodel->citas($citasm,$mes,$dia);
$cit=$this->Dashboardmodel->citas($citast,$mes,$dia);
$cie=$this->Dashboardmodel->citas($citase,$mes,$dia);	

///internas y seguros

$lev=$this->Dashboardmodel->getTipodeOrden('Mexicali','INTERNAS','OTROS',$desgloce);
$segm=$this->Dashboardmodel->getTipodeOrden('Mexicali','CIA DE SEGURO','INTERNAS',$desgloce);
$intt=$this->Dashboardmodel->getTipodeOrden('TIJUANA','INTERNAS','OTROS',$desgloce);
$segt=$this->Dashboardmodel->getTipodeOrden('TIJUANA','CIA DE SEGURO','INTERNAS',$desgloce);
$inte=$this->Dashboardmodel->getTipodeOrden('Ensenada','INTERNAS','OTROS',$desgloce);
$sege=$this->Dashboardmodel->getTipodeOrden('Ensenada','INTERNAS','OTROS',$desgloce);

////
$real=$this->Dashboardmodel->realpormes($desgloce,date('m'));
$acumulado=$this->Dashboardmodel->realacumulado($desgloce);

?>



<div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>VENTAS TOTALES DEL MES POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="grauno"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>VENTAS ACUMULADO POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="grados"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
    
    
<div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>MEZCLA DE VENTA REAL | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="gratres"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>MEZCLA DE VENTA REAL | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="gracuatro"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>MEZCLA DE VENTA REAL | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="gracinco"></div>
    </div></div>
    
    
    </div>
    
    
    </div>

       <div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle-o"></i>OS | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="granueve"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle-o"></i>OS | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="gradiez"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle-o"></i>OS | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="graonce"></div>
    </div></div>
    
    
    </div>
    
    
    </div>    
    
    
    <div class="row" style="background-color:white">
<div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CITAS | TIJUANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="graseis"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CITAS | MEXICALI </div>
    </div>
    <div class="portlet-body"   >
    <div id="grasiete"></div>
    </div></div>
    </div>
    <div class="col-md-4 col-sm-4" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-circle"></i>CITAS | ENSENADA </div>
    </div>
    <div class="portlet-body"   >
    <div id="graocho"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
    
    
    
<div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>HORAS POR OS DEL MES POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="gradoce"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>HORAS POR OS ACUMULADO POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="gratrece"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
    
    
    
    <div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>PRODUCTIVIDAD POR TALLER DE LA SEMANA </div>
    </div>
    <div class="portlet-body"   >
    <div id="gracatorce"></div>
    </div></div>
    </div>
    
    
    <div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-bar-chart-o"></i>PRODUCTIVIDAD DEL MES0 POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div id="graquince"></div>
    </div></div>
    
    
    </div>
    
    
    </div>
  

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('productividad/js/script');?>

    
 <script type="text/javascript">
     
	  google.load("visualization", "1.1", {packages:["bar"]});
google.setOnLoadCallback(drawVisualization);

function drawVisualization() {
  // Some raw data (not necessarily accurate)
  var data = google.visualization.arrayToDataTable([
    ['Descripcion', 'Ventas', 'Presupuesto', 'Ventas 2014'],
    ['Tijuana',  
	<?php echo $resa=$real['tij'] + $real['tijr'] + $real['tijm'];?>,
	 <?php echo $VMOT + $VMTT + $VRFT + $VRGT + $VRST + $VRTT;?>,   
	<?php echo $AVMOT + $AVMTT + $AVRFT + $AVRGT + $AVRST + $AVRTT;?>],
    ['Mexicali', 
	<?php echo $resb=$real['mex'] + $real['mexr'] + $real['mexm'];?>,
	<?php echo $VMO + $VMT + $VRF + $VRG + $VRS + $VRT;?>,        
	<?php echo $AVMO + $AVMT + $AVRF + $AVRG + $AVRS + $AVRT;?>],
    ['Ensenada', 
	<?php echo $resc=$real['ens'] + $real['ensr'] + $real['ensm'];?> ,  
	 <?php echo $VMOE + $VMTE + $VRFE + $VRGE + $VRSE + $VRTE;?>,      
	 <?php echo $AVMOE + $AVMTE + $AVRFE + $AVRGE + $AVRSE + $AVRTE;?>]
  ]);

  var options = {
    chartArea:{left:'10%',top:'10%',width:'90%',height:'75%'},
	legend: { position: 'top', maxLines: 3 },
   // series: {5: {type: "line"}}
  };

  var chart = new google.charts.Bar(document.getElementById('grauno'));
  chart.draw(data, options);
}
    

google.setOnLoadCallback(drawVisualizationx);

function drawVisualizationx() {
  // Some raw data (not necessarily accurate)
  var datax = google.visualization.arrayToDataTable([
    ['Acumulado 2015', 'Ventas', 'Presupuesto', 'Ventas 2014'],
    ['Tijuana',  
	<?php 
    echo $resb=$acumulado['tij'] + $acumulado['tijr'] + $acumulado['tijm'];?>,
	<?php echo $KVMOT + $KVMTT + $KVRFT + $KVRGT + $KVRST + $KVRTT;?>,
	<?php echo $KAVMOT + $KAVMTT + $KAVRFT + $KAVRGT + $KAVRST + $KAVRTT;?>],
    ['Mexicali', 
	0,
	<?php echo $KVMO + $KVMT + $KVRF + $KVRG + $KVRS + $KVRT;?>,
	<?php echo $KAVMO + $KAVMT + $KAVRF + $KAVRG + $KAVRS + $KAVRT;?>],
    ['Ensenada', 
	0,
	<?php echo $KVMOE + $KVMTE + $KVRFE + $KVRGE + $KVRSE + $KVRTE;?>,
	<?php echo $KAVMOE + $KAVMTE + $KAVRFE + $KAVRGE + $KAVRSE + $KAVRTE;?>]
  ]);

  var options = {
     chartArea:{left:'10%',top:'10%',width:'90%',height:'75%'},
	legend: { position: 'top', maxLines: 3 },
   // series: {5: {type: "line"}}
  };
  
  

  var chart = new google.charts.Bar(document.getElementById('grados'));
  chart.draw(datax, options);
}	


//mezcla de venta ensenada
google.setOnLoadCallback(drawVisualizationxabccc);
function drawVisualizationxabccc() {
  // Some raw data (not necessarily accurate)
  var dataxabccc = google.visualization.arrayToDataTable([
    ['Ensenada', 'Real','PPTO','2014'],
    ['Mano de Obra', <?php echo $real['ens'];?>,<?php echo $VMOE;?>,<?php echo $AVMOE;?>],
    ['Refacciones', <?php echo $real['ensr'];?>,<?php echo $VRFE;?>,<?php echo $AVRFE;?>],
    ['Mostrador',<?php echo $real['ensm'];?>,<?php echo $VMTE;?>,<?php echo $AVMTE;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.charts.Bar(document.getElementById('gracinco'));
  chart.draw(dataxabccc, options);
}


//mezcla de venta tijuana
google.setOnLoadCallback(drawVisualizationxa);
function drawVisualizationxa() {
  // Some raw data (not necessarily accurate)
  var dataxa = google.visualization.arrayToDataTable([
    ['Tijuana', 'Real','PPTO','2014'],
    ['Mano de Obra', <?php echo $real['tij'];?>,<?php echo $VMOT;?>,<?php echo $AVMOT;?>],
    ['Refacciones', <?php echo $real['tijr'];?>,<?php echo $VRFT;?>,<?php echo $AVRFT;?>],
    ['Mostrador',<?php echo $real['tijm'];?>,<?php echo $VMTT;?>,<?php echo $AVMTT;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.charts.Bar(document.getElementById('gratres'));
  chart.draw(dataxa, options);
}	


//mezcla de venta mexicali
google.setOnLoadCallback(drawVisualizationxab);
function drawVisualizationxab() {
  // Some raw data (not necessarily accurate)
  var dataxab = google.visualization.arrayToDataTable([
    ['Mexicali', 'Real','PPTO','2014'],
    ['Mano de Obra', <?php echo $real['mex'];?>,<?php echo $VMO;?>,<?php echo $AVMO;?>],
    ['Refacciones', <?php echo $real['mexr'];?>,<?php echo $VRF;?>,<?php echo $AVRF;?>],
    ['Mostrador',<?php echo $real['mexm'];?>,<?php echo $VMT;?>,<?php echo $AVMT;?>]
  ]);


//var formatter = new google.visualization.NumberFormat({prefix: '$'});
//formatter.format(dataxab, 1);

  var options = {
	 // vAxis: [0: {format: '#,###'}, 1: {format: '#%'}],
    //vAxis: {title: 'VALUES', format: '\u00A4 #,###0.00'},
	// pieSliceText: 'value',
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.charts.Bar(document.getElementById('gracuatro'));
  chart.draw(dataxab, options);
}


 google.load("visualization", "1", {packages:["corechart"]});	
//ocupacion del dia
google.setOnLoadCallback(drawVisualizationxabc);
function drawVisualizationxabc() {
  // Some raw data (not necessarily accurate)
  var dataxabc = google.visualization.arrayToDataTable([
    ['Ocupacion del dia', 'Real','Objetivo',],
    ['Tijuana', <?php echo $rutij;?>,<?php echo $tctaller;?>],
    ['Mexicali', <?php echo $rumex;?>,<?php echo $mctaller;?>],
    ['Ensenada',<?php echo $ruens;?>,<?php echo $ectaller;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   // series: {5: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gracatorce'));
  chart.draw(dataxabc, options);
}


google.load("visualization", "1", {packages:["corechart"]});	
//ocupacion del dia
google.setOnLoadCallback(drawVisualizationxabcc);
function drawVisualizationxabcc() {
  // Some raw data (not necessarily accurate)
  var dataxabcc = google.visualization.arrayToDataTable([
    ['Ocupacion del dia', 'Real','Objetivo',],
    ['Tijuana', <?php echo $hostij;?>,<?php echo $hos;?>],
    ['Mexicali', <?php echo $hosmex;?>,<?php echo $hos;?>],
    ['Ensenada',<?php echo $hosens;?>,<?php echo $hos;?>]
  ]);

  var options = {
    
    seriesType: "bars",
   series: {1: {type: "line"}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('gradoce'));
  chart.draw(dataxabcc, options);
}


	 
	 // HORAS POR os DEL DIA


	  
    </script>
    
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]}); 
	 
	 
	  

 google.setOnLoadCallback(drawChartc);
      function drawChartc() {

        var datac = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Asistieron',  <?php echo $ci['r'];?>],
          ['Faltaron',      <?php echo $ci['f'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartc= new google.visualization.PieChart(document.getElementById('grasiete'));

        chartc.draw(datac, options);
      }
	  
	  google.setOnLoadCallback(drawChartca);
      function drawChartca() {

        var dataca = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Asistieron',  <?php echo $cit['r'];?>],
          ['Faltaron',      <?php echo $cit['f'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartca= new google.visualization.PieChart(document.getElementById('graseis'));

        chartca.draw(dataca, options);
      }
      
	  
	  google.setOnLoadCallback(drawChartcb);
      function drawChartcb() {

        var datacb = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Asistieron',  <?php echo $cie['r'];?>],
          ['Faltaron',      <?php echo $cie['f'];?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcb= new google.visualization.PieChart(document.getElementById('graocho'));

        chartcb.draw(datacb, options);
      }
	  
	  
	  google.setOnLoadCallback(drawChartcba);
      function drawChartcba() {

        var datacba = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Internas',  <?php echo $lev -2;?>],
          ['Seguros',     <?php echo $segm -2;?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcba= new google.visualization.PieChart(document.getElementById('gradiez'));

        chartcba.draw(datacba, options);
      }
	  
	  
	  google.setOnLoadCallback(drawChartcbat);
      function drawChartcbat() {

        var datacbat = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Internas',  <?php echo $intt -2;?>],
          ['Seguros',     <?php echo $segt -2;?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcbat= new google.visualization.PieChart(document.getElementById('granueve'));

        chartcbat.draw(datacbat, options);
      }
	  
	   google.setOnLoadCallback(drawChartcbate);
      function drawChartcbate() {

        var datacbate = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Internas',  <?php echo $inte -2;?>],
          ['Seguros',     <?php echo $sege -2;?>]
        ]);

        var options = {
			 chartArea:{left:'1%',top:'10%',width:'90%',height:'95%'},
	legend: { position: 'top', maxLines: 3 },
        };

        var chartcbate= new google.visualization.PieChart(document.getElementById('graonce'));

        chartcbate.draw(datacbate, options);
      }
	  
	  
    </script>