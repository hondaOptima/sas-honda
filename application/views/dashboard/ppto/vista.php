  <?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>dashboard/ppto/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-gears"></i> <a href="#">Presupuestos</a> <i class="fa fa-angle-right"></i> </li>
  
  
   <li class="pull-right">
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option> 
    <option value="4" <?php if($taller==4){echo'selected';}?>>&Oacute;ptima</option>        
    </select>
  </li>
  
   <li class="pull-right">
    <select name="anio" onchange='this.form.submit()'>
    <option value="2015" <?php if($anio=='2015'){echo'selected';}?>>2015</option>
    <option  value="2014" <?php if($anio=='2014'){echo'selected';}?>>2014</option>       
    </select>
  </li>
</ul>
</form>
<!-- END PAGE TITLE & BREADCRUMB--> 

<?php

function formaton($num){
return $num;
	}
	
	
function verdia($desgloce,$age,$dia,$mes,$anio){
$res=0;
$suma=0;
foreach($desgloce as $des){
if( $des->des_a==''.$dia.'/'.$mes.'/'.$anio.'' && $des->des_d==$age){
if($des->des_l==''){$res=0;}
else{$res=formaton($des->des_l);}
$suma+=$res;
}}

return $suma;
}

function horasase($desgloce,$age,$dia,$mes,$anio){
$dec='';
$bul='si';
$smtot=0;
$nor=0;
foreach($desgloce as $des){
if($des->des_e=='MOSTRADOR/REFACCIONES' || $des->des_e=='SERVICIO' || $des->des_e=='OTROS'){ $bul='si';}{}
	
if($des->des_e=='INTERNAS'){$bul='no';}

if($bul=='si'){
if( $des->des_a==''.$dia.'/'.$mes.'/'.$anio.'' && $des->des_d==$age){
	 $nor++;
	 $smtot+=$des->des_k.'<br>';
	}
}


}
if($smtot==0){return 0;}
else{
return $smtot / $nor;}
}


function horasaseos($desgloce,$age,$dia,$mes,$anio){
$dec='';
$bul='si';
$smtot=0;
$nor=0;
foreach($desgloce as $des){

	
if($des->des_e=='MOSTRADOR/REFACCIONES' || $des->des_e=='SERVICIO' || $des->des_e=='OTROS'){ $bul='si';}{}
	
if($des->des_e=='INTERNAS'){$bul='no';}

if($bul=='si'){
if( $des->des_a==''.$dia.'/'.$mes.'/'.$anio.'' && $des->des_d==$age){
	 $nor++;
	 $smtot+=$des->des_k.'<br>';
	}
}


}
if($smtot==0){return 0;}
else{
return $nor;}
}

?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-money"></i><?php if($taller==1){echo 'Ensenada';}
	  if($taller==3){echo 'Tijuana';}
	  if($taller==2){echo 'Mexicali';}
	  if($taller==4){echo '&Oacute;ptima';}
	  echo ' '.$anio;?> </div>
    </div>
    <?php
    setlocale(LC_MONETARY, 'en_US');
	
	function formato($num,$pos){
		if($pos=='n'){return $num;}
		else{
			return  money_format('%(#10n', $num);
			}
		}


	?>
    <div class="portlet-body"   >
      <table class="table table-striped table-bordered table-hover" id="sample_cotizaciones" >
        <thead>
          <tr style="font-weight:bold">
            <th width="190px;" style="text-align:center"><b>Descripci&oacute;n</b></th>
            <th style="text-align:center"><b>Ene</b></th>
            <th style="text-align:center"><b>Feb</b></th>
            <th style="text-align:center"><b>Mar</b></th>
            <th style="text-align:center"><b>Abr</b></th>
            <th style="text-align:center"><b>May</b></th>
            <th style="text-align:center"><b>Jun</b></th>
            <th style="text-align:center"><b>Jul</b></th>
            <th style="text-align:center"><b>Ago</b></th>
            <th style="text-align:center"><b>Sep</b></th>
            <th style="text-align:center"><b>Oct</b></th>
            <th style="text-align:center"><b>Nov</b></th>
            <th style="text-align:center"><b>Dic</b></th>
            <th style="text-align:center"><b>Total</b></th>
          </tr>
        </thead>
        <tbody id="table-body">
        <?php 
		
		    $h=1;
			$sm1=0;
			$sm2=0;
			$sm3=0;
			$sm4=0;
			$sm5=0;
			$sm6=0;
			$sm7=0;
			$sm8=0;
			$sm9=0;
			$sm10=0;
			$sm11=0;
			$sm12=0;
			$res1=0;
			$res2=0;
			$res3=0;
			$res4=0;
			$res5=0;
			$res6=0;
			$res7=0;
			$res8=0;
			$res9=0;
			$res10=0;
			$res11=0;
			$res12=0;
			$vu=0;
			
			
			
		    foreach($ppto as $pp){
			
			
			?>
        
          <tr style="font-weight:bold">
            <th style="text-align:left"><b><a href="<?php echo base_url();?>dashboard/editppto/?cd=<?php echo $pp->ppt_ID;?>"><?php echo $h.'-'.$pp->pre_desc;?></a></b></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_01,$pp->pre_formato);if($h>2 && $h <=8){ $sm1+=$pp->ppt_01;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_02,$pp->pre_formato); if($h>2){$sm2+=$pp->ppt_02;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_03,$pp->pre_formato); if($h>2){$sm3+=$pp->ppt_03;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_04,$pp->pre_formato); if($h>2){$sm4+=$pp->ppt_04;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_05,$pp->pre_formato); if($h>2){$sm5+=$pp->ppt_05;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_06,$pp->pre_formato); if($h>2){$sm6+=$pp->ppt_06;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_07,$pp->pre_formato); if($h>2){$sm7+=$pp->ppt_07;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_08,$pp->pre_formato); if($h>2){$sm8+=$pp->ppt_08;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_09,$pp->pre_formato); if($h>2){$sm9+=$pp->ppt_09;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_10,$pp->pre_formato); if($h>2){$sm10+=$pp->ppt_10;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_11,$pp->pre_formato); if($h>2){$sm11+=$pp->ppt_11;}?></th>
            <th style="text-align:center"><?php echo formato($pp->ppt_12,$pp->pre_formato); if($h>2){$sm12+=$pp->ppt_12;}?></th>
            <th style="text-align:center"><?php  $sm=($pp->ppt_01 + $pp->ppt_02 + $pp->ppt_03 + $pp->ppt_04 + $pp->ppt_05 + $pp->ppt_06 + $pp->ppt_07 + $pp->ppt_08 + $pp->ppt_09 + + $pp->ppt_10 + $pp->ppt_11 + $pp->ppt_12 ); echo formato($sm,$pp->pre_formato);?></th>
          </tr>
          <?php if($h==8) {?>
          <tr style="font-weight:bold">
            <th style="text-align:left"><b>Total Ventas</b></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm1); $um1=$sm1; ?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm2); $um2=$sm2;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm3); $um3=$sm3;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm4); $um4=$sm4;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm5); $um5=$sm5;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm6); $um6=$sm6;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm7); $um7=$sm7;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm8); $um8=$sm8;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm9); $um9=$sm9;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm10); $um10=$sm10;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm11); $um11=$sm11;?></th>
            <th style="text-align:center"><?php echo money_format('%(#10n',$sm12); $um12=$sm12;?></th>
            <th style="text-align:center"><?php  $sm13=($sm1 + $sm2 + $sm3 + $sm4 + $sm5 + $sm6 + $sm7 + $sm8 + $sm9 + $sm10 + $sm11 + $sm12 ); echo  money_format('%(#10n',$sm13);?></th>
          </tr>
          <?php }
		  if($h==15) {
			  
        $co1=$pp->ppt_01;  
		$co2=$pp->ppt_02;  
		$co3=$pp->ppt_03;  
		$co4=$pp->ppt_04;
		$co5=$pp->ppt_05;  
		$co6=$pp->ppt_06;  
		$co7=$pp->ppt_07;  
		$co8=$pp->ppt_08;
		$co9=$pp->ppt_09;  
		$co10=$pp->ppt_10;  
		$co11=$pp->ppt_11;  
		$co12=$pp->ppt_12;  
		
        }
		  
		  
		  
		  
		   $h++;
		  
		  if($h>8){
		  	$sm1+=$pp->ppt_01;  
			  }
		  
		  }  ?>
          
          <tr style="font-weight:bold">
             <th style="text-align:left"><b>Utilidad</b></th>
            <th style="text-align:center"><?php $res1=$um1 - $co1; echo  money_format('%(#10n',$res1); ?></th>
            <th style="text-align:center"><?php $res2=$um2 - $co2; echo  money_format('%(#10n',$res2);?></th>
            <th style="text-align:center"><?php $res3=$um3 - $co3; echo  money_format('%(#10n',$res3);?></th>
            <th style="text-align:center"><?php $res4=$um4 - $co4; echo  money_format('%(#10n',$res4);?></th>
            <th style="text-align:center"><?php $res5=$um5 - $co5; echo  money_format('%(#10n',$res5);?></th>
            <th style="text-align:center"><?php $res6=$um6 - $co6; echo  money_format('%(#10n',$res6);?></th>
            <th style="text-align:center"><?php $res7=$um7 - $co7; echo  money_format('%(#10n',$res7);?></th>
            <th style="text-align:center"><?php $res8=$um8 - $co8; echo  money_format('%(#10n',$res8);?></th>
            <th style="text-align:center"><?php $res9=$um9 - $co9; echo  money_format('%(#10n',$res9);?></th>
            <th style="text-align:center"><?php $res10=$um10 - $co10; echo  money_format('%(#10n',$res10);?></th>
            <th style="text-align:center"><?php $res11=$um11 - $co11; echo  money_format('%(#10n',$res11);?></th>
            <th style="text-align:center"><?php $res12=$um12 - $co12; echo  money_format('%(#10n',$res12);?></th>
            <th style="text-align:center"><?php  $res13=($res1 + $res2 + $res3 + $res4 + $res5 + $res6 + $res7 + $res8 + $res9 + $res10 + $res11 + $res12 ); echo  money_format('%(#10n',$res13);?></th>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  
  
  
</div>
</div>





<?php

function nameDay($date){
$day = date('l', strtotime($date));
if($day=='Sunday'){return 'Dom';}
if($day=='Monday'){return 'Lun';}
if($day=='Tuesday'){return 'Mar';}
if($day=='Wednesday'){return 'Mie';}
if($day=='Thursday'){return 'Jue';}
if($day=='Friday'){return 'Vie';}
if($day=='Saturday'){return 'Sab';}
}


function getMonthDays($Month, $Year)
{
   //Si la extensión que mencioné está instalada, usamos esa.
   if( is_callable("cal_days_in_month"))
   {
      return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
   }
   else
   {
      //Lo hacemos a mi manera.
      return date("d",mktime(0,0,0,$Month+1,0,$Year));
   }
}
//Obtenemos la cantidad de días que tiene septiembre del 2008
 $lista=getMonthDays($mes, $anio);
?>

<div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-money"></i> <?php if($taller==1){echo 'Ensenada';}
	  if($taller==3){echo 'Tijuana';}
	  if($taller==2){echo 'Mexicali';}
	  if($taller==4){echo '&Oacute;ptima';}
	  echo ' '.$anio;?></div>
    </div>

    <div class="portlet-body"  style=" overflow:scroll" >
<table  class="table table-striped table-bordered table-hover" width="100%" border="1px">
<tr style="text-align:center">
<td></td>
<td colspan="3">PRESUPUESTO DEL MES</td>
<td colspan="2"><?php echo money_format('%(#10n',$ppto[2]->ppt_04);?></td>

<td colspan="2"><a href="<?php echo base_url()?>dashboard/dias_tra/?cd=<?php echo $taller;?>&mes=<?php echo $mes;?>&anio=<?php echo $anio;?>&iddt=<?php echo $diasa=$dias_tra[0]->dit_ID;?>">DIAS HABS.</a></td>
<td><?php echo $diasa=$dias_tra[0]->dit_dias;?></td>

<td>SABADOS</td>
<td><?php echo $sab=$dias_tra[0]->dit_sabados;?></td>

<td colspan="2">DIAS TRAB.</td>
<td><?php 
if($dias_tra[0]->dit_sabados==0){
echo $diastra=$dias_tra[0]->dit_dias;	
	}
else{
echo $diastra=$dias_tra[0]->dit_dias - ($dias_tra[0]->dit_sabados /2);}?></td>
<?php
for($s=0; $s<13; $s++){
echo'<td></td>';	
	}
?>
</tr>

<tr>
<td></td>
<?php
$odiv=0;
$sdiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$nam.'</td>';}	
	}
	echo '</tr><tr><td>Dia</td>';
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$x.'</td>';}	
	}
	echo '</tr><tr><td>PPTO X DIA</td>';
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}	
echo'<td>'.$div.'</td>';}	
	}
	
	echo '</tr><tr><td>ACUMULADO</td>';
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}
else{$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}	
$sdiv+=$div;	
echo'<td>'.$sdiv.'</td>';}	
	}
?>
</tr>
</table>
<br>
<table class="table table-striped table-bordered table-hover" border="1px" width="100%">
<tr style="text-align:center"><td>Dia</td>
<?php
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$x.'</td>';}	
	}
?>
</tr>
<tr><td>OBJETIVO</td>
<?php
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
$odiv=$div / 3;	
echo'<td>'.number_format($odiv, 2, '.', '').'</td>';}	
	}
?>

</tr>

<?php
foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;
	 
echo'<tr><td>'.$us->sus_name.'</td>';	 
for($x=1; $x <=$lista;  $x++){	

$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
	
echo'<td>'.verdia($desgloce,$age,$x,$mes,$anio).'</td>';}	
	}	 
	 
	 }	
	}
?>

</table>



<br>
<table class="table table-striped table-bordered table-hover" border="1px" width="100%">
<tr style="text-align:center"><td>ACUMULADO</td>
<?php
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$x.'</td>';}	
	}
?>
</tr>
<tr><td>OBJETIVO X ASM</td>
<?php
$sodiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
$odiv=$div / 3;
$sodiv+=$odiv;	
echo'<td>'.number_format($sodiv, 2, '.', '').'</td>';}	
	}
?>

</tr>

<?php

foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;
	 
echo'<tr><td>'.$us->sus_name.'</td>';
$sverdia=0;
$verdia=0;	 
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
if($x<=date('d')){
$verdia=verdia($desgloce,$age,$x,$mes,$anio);
$sverdia+=$verdia;	}
else{
$sverdia=0;	
	}
echo'<td>'.$sverdia.'</td>';}	
	}	 
	 
	 }	
	}
?>

</table>



<br>
<table class="table table-striped table-bordered table-hover" border="1px" width="100%">
<tr style="text-align:center"><td></td>
<?php
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$x.'</td>';}	
	}
?>
</tr>
<tr><td>OBJETIVO HR X OS</td>
<?php
$sodiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
$odiv=$div / 3;
$sodiv+=$odiv;	
echo'<td>3.5</td>';}	
	}
?>

</tr>

<?php

foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;
	 
echo'<tr><td>'.$us->sus_name.'</td>';
$sverdia=0;
$verdia=0;	 
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
if($x<=date('d')){
$verdia=horasase($desgloce,$age,$x,$mes,$anio);
$sverdia=$verdia;	}
else{
$sverdia=0;	
	}
echo'<td>'.number_format($sverdia, 2, '.', '').'</td>';}	
	}	 
	 
	 }	
	}
?>

</table>



<br>
<table class="table table-striped table-bordered table-hover" border="1px" width="100%">
<tr style="text-align:center"><td></td>
<?php
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$x.'</td>';}	
	}
?>
</tr>
<tr><td>OS CLIENTE</td>
<?php
$sodiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
$odiv=$div / 3;
$sodiv+=$odiv;	
echo'<td>3.5</td>';}	
	}
?>

</tr>

<?php

foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;
	 
echo'<tr><td>'.$us->sus_name.'</td>';
$sverdia=0;
$verdia=0;	 
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
if($x<=date('d')){
$verdia=horasaseos($desgloce,$age,$x,$mes,$anio);
$sverdia=$verdia;	}
else{
$sverdia=0;	
	}
echo'<td>'.number_format($sverdia, 2, '.', '').'</td>';}	
	}	 
	 
	 }	
	}
?>

</table>



<br>
<table class="table table-striped table-bordered table-hover" border="1px" width="100%">
<tr style="text-align:center"><td></td>
<?php
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
echo'<td>'.$x.'</td>';}	
	}
?>
</tr>
<tr><td>MO X OS</td>
<?php



///HORAS TRABAJADAS
$sodiv=0;
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
$odiv=$div / 3;
$sodiv+=$odiv;	
echo'<td>'.number_format($sodiv, 2, '.', '').'</td>';}	
	}
?>

</tr>

<?php

foreach($usuario as $us){
 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) {
$age=$us->sus_adviserNumber;
	 
echo'<tr><td>'.$us->sus_name.'</td>';
$sverdia=0;
$verdia=0;
$dinero=0;
$doper=0;	 
for($x=1; $x <=$lista;  $x++){	
$nam=nameDay($anio.'/'.$mes.'/'.$x);
if($x<10){$x='0'.$x;}
if($nam=='Dom'){}else{
if($nam=='Sab'){ 
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', ''); $div=number_format($div / 2, 2, '.', '');}else{	
$div=number_format($ppto[2]->ppt_04 / $diastra, 2, '.', '');}
if($x<=date('d')){
$dinero=verdia($desgloce,$age,$x,$mes,$anio);
$verdia=horasaseos($desgloce,$age,$x,$mes,$anio);
$sverdia=$verdia;	

}
else{
$sverdia=0;
$dinero=0;	
$doper=0;
	}

if($dinero==0 || $sverdia==0){$doper=0;}else{
$doper=$dinero / $sverdia;	
	}	
	
	
echo'<td>'.number_format($doper, 2, '.', '').'</td>';}	
	}	 
	 
	 }	
	}
?>

</table>


    </div>
  </div>
  
  
  
</div>










<?php 
$data['taller']=$taller;
$data['nomina']=$nomina;
$data['meses']=$meses; 
$this->load->view('dashboard/ppto/editarNomina',$data); 
?>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('productividad/js/script');?>

<script>
jQuery(document).ready(function() {
	
$('.modalEdit').live('click',function(){ 
var id=$(this).attr('id');
$('#wide').modal('show');    
});

$('.updateMonto').live('focusout',function(){ 
var id=$(this).attr('id');
var val=$(this).attr('value');
$.ajax({
	  url:"<?php echo base_url();?>dashboard/editarMonto/?id="+id+"&campo="+val+"",
	  success:function(result){}
      });
});


});
</script>