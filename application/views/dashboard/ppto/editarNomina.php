<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Modificar Informaci&oacute;n</h4>
      </div>
      <div class="modal-body"> <?php echo form_open('../dashboard/ppto/?taller='.$taller.'','class=""'); ?>
        
      <?php foreach($nomina as $nom){?>  
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-4"><?php echo $meses[$nom->nom_mes];?></label>
              <div class="col-md-6">
                <input name="" id="<?php echo $nom->nom_ID;?>" value="<?php echo $nom->nom_monto;?>" class="updateMonto" <?php if($taller==4){echo'readonly';}?>>
              </div>
            </div> 
          </div>
         
        </div>
        <?php } ?>
       
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <input type="submit" class="btn btn-info" value="Guardar">
      </div>
    </div>
    </form>
    <!-- /.modal-content --> 
  </div>
</div>