<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-gears"></i> <a href="#">Presupuestos</a> <i class="fa fa-angle-right"></i> </li>
  
  
   
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-money"></i>Modificar Información de <?php if($_GET['cd']==1){echo'Ensenada';} if($_GET['cd']==2){echo'Mexicali';} if($_GET['cd']==3){echo'Tijuana';}?></div>
    </div>
    <div class="portlet-body"   >
     <?php echo  form_open_multipart('dashboard/editppto/','class="form-horizontal"'); ?>
      <table class="table table-striped table-bordered table-hover" id="sample_cotizaciones" >
        <thead>
          <tr style="font-weight:bold">
            <th width="190px;" style="text-align:center"><b>Descripci&oacute;n</b></th>
            <th style="text-align:center"><b>Ene</b></th>
            <th style="text-align:center"><b>Feb</b></th>
            <th style="text-align:center"><b>Mar</b></th>
            <th style="text-align:center"><b>Abr</b></th>
            <th style="text-align:center"><b>May</b></th>
            <th style="text-align:center"><b>Jun</b></th>
            <th style="text-align:center"><b>Jul</b></th>
            <th style="text-align:center"><b>Ago</b></th>
            <th style="text-align:center"><b>Sep</b></th>
            <th style="text-align:center"><b>Oct</b></th>
            <th style="text-align:center"><b>Nov</b></th>
            <th style="text-align:center"><b>Dic</b></th>
          </tr>
        </thead>
        <tbody id="table-body">
        <?php foreach($ppto as $pp){?>
        <input type="hidden" name="idpp" value="<?php echo $pp->ppt_ID;?>">
          <tr style="font-weight:bold">
            <th style="text-align:center"><b><a href="<?php echo base_url();?>dashboard/editppto/?cd=<?php echo $pp->wor_idWorkshop;?>"><?php echo $pp->pre_desc;?></a></b></th>
            <th style="text-align:center"><input style="width:80px;"  type="tex" name="ppt_a" value="<?php echo $pp->ppt_01;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;"type="tex" name="ppt_b" value="<?php echo $pp->ppt_02;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_c" value="<?php echo $pp->ppt_03;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_d" value="<?php echo $pp->ppt_04;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_e" value="<?php echo $pp->ppt_05;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_f" value="<?php echo $pp->ppt_06;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_g" value="<?php echo $pp->ppt_07;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_h" value="<?php echo $pp->ppt_08;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_i" value="<?php echo $pp->ppt_09;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_j" value="<?php echo $pp->ppt_10;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_k" value="<?php echo $pp->ppt_11;?>"  /></th>
            <th style="text-align:center"><input style="width:80px;" type="tex" name="ppt_l" value="<?php echo $pp->ppt_12;?>"  /></th>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <input type="submit" value="Guardar Información">
      </form>
    </div>
  </div>
</div>


<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('productividad/js/script');?>

<script>
jQuery(document).ready(function() {
	
$('.modalEdit').live('click',function(){ 
var id=$(this).attr('id');
$('#wide').modal('show');    
});

$('.updateMonto').live('focusout',function(){ 
var id=$(this).attr('id');
var val=$(this).attr('value');
$.ajax({
	  url:"<?php echo base_url();?>dashboard/editarMonto/?id="+id+"&campo="+val+"",
	  success:function(result){}
      });
});


});
</script>