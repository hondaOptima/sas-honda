<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>
 <?php
$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
?>
<form name="form1" method="get" action="<?php echo base_url();?>dashboard">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-gears"></i> <a href="#">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  
 <li class="pull-right" style="margin-top:-8px;">
    <input type="submit" value="Consultar" class="btn">
    </li>
  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" >
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
  </li>

  <li class="pull-right" style=" margin-left:8px; margin-top:1px;">
   
  <select name="dia">
<?php for($x=1; $x<= $dias; $x++){

if($dia==$x){$dsel='selected';}else{$dsel='';}
echo '<option value="'.$x.'" '.$dsel.'>'.$x.'</option>';	
	}?>
  </select>
   </li><li class="pull-right" style=" margin-left:8px; margin-top:1px;">
  
  <select name="mes">
 
  <?php
  foreach($meses as $numm=> $nommes){
if($numm==$mes){$sel='selected="selected"';}else{$sel='';}	  
echo'<option value="'.$numm.'"  '.$sel.'>'.$nommes.'</option>';	  
	  }
  
  ?>
  </select>
   </li>
   
</ul>
</form>
<!-- END PAGE TITLE & BREADCRUMB--> 

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<?php
date_default_timezone_set('America/Tijuana');
date('Hi');
$x=date("w", strtotime('2014-11-15'));
foreach($app as $ap){
	$this->Dashboardmodel->appedit($ap->seo_IDapp);
	}
	
$val=0;	
$tij=0;
$mex=0;
$ens=0;

$valr=0;	
$tijr=0;
$mexr=0;
$ensr=0;

$valm=0;	
$tijm=0;
$mexm=0;
$ensm=0;

$valc=0;	
$tijc=0;
$mexc=0;
$ensc=0;

$tijcc=0;
$mexcc=0;
$enscc=0;

foreach($ppto as $pp){
	if($pp->ppt_desc==2){$VMO=$pp->res;}
	if($pp->ppt_desc==3){$VRF=$pp->res;}
	if($pp->ppt_desc==9){$CVRF=$pp->res;}
	if($pp->ppt_desc==7){$VMT=$pp->res;}
	if($pp->ppt_desc==13){$CVMT=$pp->res;}
	if($pp->ppt_desc==16){$CUMO=$pp->res;}
	if($pp->ppt_desc==8){$CVMO=$pp->res;}
	}	

echo '<table>';	
$f=0;
foreach($desgloce as $des){
if(strpos($des->des_j, 'Total Sucursal:')!==false){
	if($des->des_j== 'Total Sucursal:'){
		//venta de mano de obra
		$val=$des->des_l;
		//venta de refacciones
		$valr=$des->des_n;
		//venta refacciones costo
		$valc=$des->des_o;
		//venta de mostrador
		$valm=$des->des_n;
		}

$f++;	

if($f==2){$tij=$val; $tijr=$valr; $tijc=$valc;}
if($f==4){$mex=$val; $mexr=$valr; $mexc=$valc;}
if($f==6){$ens=$val; $ensr=$valr; $ensc=$valc;}
if($f==1){$tijm=$valm; $tijcc=$valc;}
if($f==3){$mexm=$valm; $mexcc=$valc;}
if($f==5){$ensm=$valm; $enscc=$valc;}	
//echo '<tr><td>'.$des->des_c.'-'.$des->des_j.'-'.$valm.'</td></tr>';	
	
	}	
	}
echo '</table>';	
?>


<div class="row" style="background-color:white">
<div class="col-md-6 col-sm-6" >
  <div class="portlet" >
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-money"></i>VENTAS POR MES POR TALLER </div>
    </div>
    <div class="portlet-body"   >
    <div class="grauno"></div>
    </div></div></div>
    
    
    

<div class="row" style="background-color:white">
  <table width="100%" style="text-align:center">
    <tr height="22px;">
      <td width="16.6%"><table width="100%" >
          <tr>
            <td></td>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> VENTA MANO DE OBRA DEL MES </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> <?php echo $VMO; ?></td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> REAL </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> <?php echo $mex;?> </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; "></td>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
            <div id="chart1" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; "></td>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00% </td>
          </tr>
          
        </table></td>
        <td width="1%"></td>
      <td width="16.6%">
      <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> VENTA DE REFACCIONES DEL MES </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> <?php echo $VRF; ?> </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333">  <?php echo $mexr;?> </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
            <div id="chart2" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table></td>
      <td width="16.6%"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> VTA. REF. MOSTRADOR DEL MES </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> <?php echo $VMT; ?></td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> <?php echo $mexm;?> </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart3" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table></td>
      <td width="16.6%"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> UTILIDAD MANO DE OBRA</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> <?php echo $VMO - $CVMO; ?> </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> <?php
			$opu=0;
			$mmex=substr($mex, 1);
			list($n1,$n2)=explode(',',$mmex);
			$mmex=$n1.$n2;
			 $opu=$mmex;
			 echo ($opu - $CUMO);?> </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart4" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table></td>
      <td width="16.6%"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> UTILIDAD REFACCIONES DEL MES</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> <?php echo $VRF - $CVRF; ?></td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> <?php
			$cre=0;
			$scre=substr($mexr, 1);
			list($rn1,$rn2)=explode(',',$scre);
			$scre=$rn1.$rn2;
			$cre=$scre;
			
			$crec=0;
			$scre=substr($mexc, 1);
			list($rn1,$rn2)=explode(',',$scre);
			$scre=$rn1.$rn2;
			$crec=$scre;
			 
			 echo $cre - $crec;?> </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart5" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table></td>
      <td width="16.6%"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> UTILIDAD DE REF. POR MOSTRADOR</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> <?php echo $VMT - $CVMT; ?></td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> <?php
			$cre=0;
			$scre=substr($mexm, 1);
			list($rn1,$rn2)=explode(',',$scre);
			$scre=$rn1.$rn2;
			$cre=$scre;
			
			$crec=0;
			$scre=substr($mexcc, 1);
			list($rn1,$rn2)=explode(',',$scre);
			$scre=$rn1.$rn2;
			$crec=$scre;
			 
			 echo $cre - $crec;?></td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart6" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>

<div class="row" style="background-color:white">
  <table width="100%" style="text-align:center">
    <tr height="22px;">
       <td width="29%"> <table width="100%" >
          <tr>
            <td  style="background-color:#00b050; font-size:11px; color:white;"> INDICADOR </td>
             <td  style="background-color:#00b050; font-size:11px; color:white;"> ACUMULADO AL MES </td>
              <td  style="background-color:#00b050; font-size:11px; color:white;"> ALCANCE DEL OBJETIVO </td>
          </tr>
         
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> VENTA MO PPTO </td>
          <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"><?php echo $VMO;?></td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> VENTA MO REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"><?php echo $mex;?> </td>
          </tr>
          
           <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> VENTA REF PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"><?php echo $VRF;?></td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> VENTA REF REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"><?php echo $mexr;?> </td>
          </tr>
          
           <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> VENTA REFS MOST. PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"><?php echo $VRF;?></td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> VENTA REFS MOTS. REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
          </tr>
         <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> UTILIDAD MO PPTO</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> UTILIDAD MO REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
          </tr>
          
           <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> UTILIDAD REF 	PPTO</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> UTILIDAD REF REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
          </tr>
          
           <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> OS PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> OS REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
          </tr>
          
           <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> HORAS X OS PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> HORAS X OS REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
          </tr>
           <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> HORAS PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
            <td rowspan="2" style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">88.60 %</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> HORAS REAL</td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333">$ 000,000.00 </td>
          </tr>
        </table></td>
        
      <td width="16.6%" valign="top"><table width="100%" >
          <tr>
            <td></td>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;">  ORDENES TOTALES DE SERVICIO</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> REAL </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; "></td>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
            <div id="chart1" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; "></td>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        <table style="margin-top:4px;" width="100%" >
          <tr>
            <td></td>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;">Y</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> PPTO </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> REAL </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; "></td>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
            <div id="chart1" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; "></td>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        </td>
     
     
      <td width="16.6%" valign="top"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> HORAS DE MANO DE OBRA </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart4" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        
        <table style="margin-top:4px;" width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> VENTA MANO DE OBRA DEL MES </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart4" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        </td>
      <td width="16.6%" valign="top"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> HORAS POR ORDEN DE SERVICIO</td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart5" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        <table style="margin-top:4px;" width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> REFACCIONES DEL MES </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart4" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        </td>
      <td width="16.6%" valign="top"> <table width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> REFACCIONES ACUMULADO	 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart6" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        <table style="margin-top:4px;" width="100%" >
          <tr>
            <td colspan="2" style="background-color:#00b050; font-size:11px; color:white;"> Ref. X Mostrador Acumulado </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:#3ab0f0; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> $ </td>
            <td style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333"> 000,000.00 </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:red; border:1px solid #333; height:80px;">
             <div id="chart4" style="height:80px;">
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style=" font-size:12px; font-weight:bold; color:black; background-color:red; border:1px solid #333; "> 0.00 % </td>
          </tr>
        </table>
        </td>
    </tr>
  </table>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('productividad/js/script');?>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: '',
		  axisTitlesPosition:'none',
		  legend:'none',
		  colors:['#00b0f0','#ff0000'],
		  chartArea:{left:'1%',top:'1%',width:'100%',height:'100%'},
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart1'));

        chart.draw(data, options);
      }
	  
	  
	  google.setOnLoadCallback(drawChart2);
      function drawChart2() {
        var data2 = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: '',
		  axisTitlesPosition:'none',
		  legend:'none',
		  colors:['#00b0f0','#ff0000'],
		  chartArea:{left:'1%',top:'1%',width:'100%',height:'100%'},
        };

        var chart2 = new google.visualization.LineChart(document.getElementById('chart2'));

        chart2.draw(data2, options);
      }
	  
	  google.setOnLoadCallback(drawChart3);
      function drawChart3() {
        var data3 = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: '',
		  axisTitlesPosition:'none',
		  legend:'none',
		  colors:['#00b0f0','#ff0000'],
		  chartArea:{left:'1%',top:'1%',width:'100%',height:'100%'},
        };

        var chart3 = new google.visualization.LineChart(document.getElementById('chart3'));

        chart3.draw(data3, options);
      }
	  
	   google.setOnLoadCallback(drawChart4);
      function drawChart4() {
        var data4 = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: '',
		  axisTitlesPosition:'none',
		  legend:'none',
		  colors:['#00b0f0','#ff0000'],
		  chartArea:{left:'1%',top:'1%',width:'100%',height:'100%'},
        };

        var chart4 = new google.visualization.LineChart(document.getElementById('chart4'));

        chart4.draw(data4, options);
      }
	  
	  google.setOnLoadCallback(drawChart5);
      function drawChart5() {
        var data5 = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: '',
		  axisTitlesPosition:'none',
		  legend:'none',
		  colors:['#00b0f0','#ff0000'],
		  chartArea:{left:'1%',top:'1%',width:'100%',height:'100%'},
        };

        var chart5 = new google.visualization.LineChart(document.getElementById('chart5'));

        chart5.draw(data5, options);
      }
	  
	  google.setOnLoadCallback(drawChart6);
      function drawChart6() {
        var data6 = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: '',
		  axisTitlesPosition:'none',
		  legend:'none',
		  colors:['#00b0f0','#ff0000'],
		  chartArea:{left:'1%',top:'1%',width:'100%',height:'100%'},
        };

        var chart6 = new google.visualization.LineChart(document.getElementById('chart6'));

        chart6.draw(data6, options);
      }
    </script>
    
    <script type="text/javascript">
      google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses', 'Profit'],
          ['2014', 0, 400, 200],
          ['2015', 1170, 460, 250],
          ['2016', 660, 1120, 300],
          ['2017', 1030, 540, 350]
        ]);

        var options = {
          chart: {
            title: 'Company Performance',
            subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('grauno'));

        chart.draw(data, options);
      }
    </script>