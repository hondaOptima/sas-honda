<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>

<div class="div-takata">
  <div class="takata-first-column">
      <div class="takata-r1 tkt-th">
        REALIZADAS
      </div>
      <div class="takata-r1 tkt-tr">
        <?php echo $realizadasNum; ?>
      </div>
      <div class="takata-r1 tkt-tr">
        <?php echo number_format($realizadasPor, 2, '.', ',').'%'; ?>
      </div>
  </div>
  <div class="takata-second-column">
      <div class="takata-r1 tkt-th">
        PENDIENTES
      </div>
      <div class="takata-r1 tkt-tr">
        <?php echo $pendientesNum; ?>
      </div>
      <div class="takata-r1 tkt-tr">
        <?php echo number_format($pendientesPor, 2, '.', ',').'%'; ?>
      </div>
  </div>
  <div class="takata-third-column">
      <div class="takata-r1 more-padding tkt-th">
        TOTAL
      </div>
      <div class="takata-r1 tkt-tr">
        <?php echo $pendientesNum+$realizadasNum; ?>
      </div>
      <div class="takata-r1 tkt-tr">
         <?php echo number_format(($pendientesPor+$realizadasPor), 2, '.', ',').'%'; ?>
      </div>
  </div>
</div>

<h2 style="margin-left: 45%;">Realizadas</h2>
<div class="th-fx">
  <div style="width: 3%">
    #
  </div>
  <div style="width: 13%">
    VIN
  </div>
  <div style="width: 25%">
    CAMPANIA
  </div>
  <div style="width: 26%">
    CLIENTE
  </div>
  <div style="width: 21%">
    TELEFONO
  </div>
</div>
<div id="columnchart_material" style="width: 80%; height: 500px;margin-left:0.5%;margin-top:20px;overflow-y:scroll;"></div>
<h2 style="margin-left: 45%;">Pendientes</h2>
<div class="th-fx">
  <div style="width: 3%">
    #
  </div>
  <div style="width: 13%">
    VIN
  </div>
  <div style="width: 25%">
    CAMPANIA
  </div>
  <div style="width: 22%">
    CLIENTE
  </div>
  <div style="width: 14%">
    TELEFONO
  </div>
  <div style="width: 9%">
    ATENDIDO
  </div>
</div>
<div id="columnchart_pendientes" style="width: 80%; height: 550px;margin-left:0.5%;overflow-y:scroll;"></div>


<style type="text/css">

  .div-takata{
    top:100px;
    right: 10px;
    width: 275px;
    height: 130px;
    position: fixed;
    z-index: 1;
    border-radius: 4px;
  }
 
  .tkt-th{
    background: rgba(122, 119, 114, 1); 
  }

  .tkt-tr{
    background: rgba(15, 78, 125, 1); 
  }
  .takata-r1{
    width: 100%;
    height: 40px;
    margin-bottom: 3px;
    padding: 4px;
    color: white;
    font-size: 1.1em;
  }

  .more-padding{
    padding-left: 6px;

  }

  .takata-first-column, .takata-second-column, .takata-third-column{
    width: 88px;
    margin-top: 3px;
    margin-left: 3px;
    height: 126px;
    float: left;
  }

  .takata-first-column, .takata-second-column{
    width: 92px;
  }

  .takata-third-column{
    width: 80px;
  }

 .th-fx{
  margin-left: 3%;
  width: 85%;
 }

.th-fx div{
  float: left;
  margin: 3px;
  font-size: 1.2em;
  text-align: center;
  background: rgba(62, 65, 61, 1); 
  color: white;
}


tr {
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.2s ease-in-out;
}

tr:hover {
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}


.tblp tr {
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.2s ease-in-out;
}

.tblp tr:hover {
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}

</style>

<?php  $this->load->view('globales/footer'); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript" src="../../../assets/plugins/jquery-1.10.2.min.js"></script>
 <script type="text/javascript">


$(document).ready(function(){
  addTableCamp();
  addTableCampPendientes();
});

    <?php $option = 0; $end = 29; $passOption = 'False';  $counter = 0; ?>
     
      function atendido(e){
        var tst = e.checked;
        var status = 0;
        if(e.checked){
          status = 1;
        }
        $.ajax({
                                  url:"http://hsas.gpoptima.net/ajax/dashboard/atendido.php?id="+e.id+"&city=<?php echo $ciudadNum; ?>&status="+status,
                                  success:function(result){
                                    if(result == 1){
                                        alert('CAMBIO DE STATUS');
                                    }else{
                                      alert('**** ERROR ****');
                                    }
                                      
                                  }
                                     }).fail(function(data){

                                          alert('****ERROR AL ENVIAR EMAIL****');
                                          $('#wide').modal('hide');
                            }); 
      }

      function changeColor(){
          this.style.backgroundColor = 'rgba(46, 138, 138, 0.7)';
      }

      function changeColorB(){
          this.style.backgroundColor = 'rgba(46, 138, 138, 0.7)';
      }

      function changeColorOut(){
          this.style.backgroundColor = this.changeColor;
      }

      function addTableCamp(){
          var tabla = document.createElement('table');
          var divParent = document.createElement('div');
          divParent.id = 'div-table-mcamp';
          divParent.style.width = '95%';
          divParent.style.marginTop = '5px';
          divParent.style.marginLeft = '3%';
          divParent.classList.add('table-responsive');
          tabla.style.marginTop = '15px';
          tabla.classList.add('table');
          tabla.classList.add('table-condensed');
          tabla.classList.add('table-striped');
          var colorCount = 1;
          var countid = 0;
          var countAplicadas = 0;
          var countPendientes = 0;
          <?php foreach ($realizadas as $elem){ ?>
              var inRow = document.createElement('tr');
              inRow.style.marginTop = '1px';
              inRow.style.marginBottom = '1px';
              inRow.style.height = '25px';
              inRow.style.paddingTop = '3px';
              inRow.style.marginTop = '2px';
              inRow.style.marginBottom = '2px';
              inRow.style.fontSize = '1.2em';
              inRow.id = 'tr_'+countid;
              inRow.addEventListener("mouseover", changeColor);
              inRow.addEventListener("mouseout", changeColorOut);
              if(colorCount == 1){
                  inRow.changeColor = 'rgba(14, 144, 250, 0.2)';
                  inRow.style.backgroundColor = 'rgba(14, 144, 250, 0.2)';
                  colorCount = 0;
              }else{
                  inRow.changeColor = '#fff';
                  inRow.style.backgroundColor = '#fff';
                  colorCount = 1;
              }
              countid++;
              var tdCont = document.createElement('td');
              tdCont.style.textAlign = 'center';
              tdCont.style.paddingLeft = '12px';
              tdCont.style.paddingRight = '12px';
              tdCont.innerHTML = countid;
              var tdVin = document.createElement('td');
              tdVin.style.textAlign = 'center';
              tdVin.innerHTML = '<?php echo $elem->cus_serie; ?>';
              var tdCamp = document.createElement('td');
              tdCamp.style.textAlign = 'center';
              tdCamp.style.width = '29%';
              tdCamp.innerHTML = "<?php echo $elem->camp; ?>";
              var tdName = document.createElement('td');
              tdName.style.textAlign = 'center';
              tdName.innerHTML = '<?php echo $elem->cus_name; ?>';
              var tdTel = document.createElement('td');
              tdTel.style.textAlign = 'center';
              tdTel.innerHTML = '<?php echo $elem->cus_telephone ?>';

             
              inRow.appendChild(tdCont);
              inRow.appendChild(tdVin);
              inRow.appendChild(tdCamp);
              inRow.appendChild(tdName);
              inRow.appendChild(tdTel); 
              tabla.appendChild(inRow); 
              
          <?php } ?>

          divParent.appendChild(tabla);
          document.getElementById('columnchart_material').appendChild(divParent);
          

        }

        function addTableCampPendientes(){
          var tabla = document.createElement('table');
          var divParent = document.createElement('div');
          divParent.id = 'div-table-mcamp';
          divParent.style.width = '95%';
          divParent.style.marginTop = '5px';
          divParent.style.marginLeft = '3%';
          divParent.classList.add('table-responsive');
          tabla.classList.add('table');
          tabla.classList.add('table-condensed');
          tabla.classList.add('table-striped');
          
          var colorCount = 1;
          var countid = 0;
          var countAplicadas = 0;
          var countPendientes = 0;
          <?php foreach ($pendientes as $elem){ ?>
              var inRow = document.createElement('tr');
              inRow.style.marginTop = '1px';
              inRow.style.marginBottom = '1px';
              inRow.style.height = '25px';
              inRow.style.paddingTop = '3px';
              inRow.style.marginTop = '2px';
              inRow.style.marginBottom = '2px';
              inRow.style.fontSize = '1.2em';

              inRow.id = 'tr_'+countid;
              inRow.addEventListener("mouseover", changeColor);
              inRow.addEventListener("mouseout", changeColorOut);
              if(colorCount == 1){
                  inRow.style.color = 'white';
                  inRow.changeColor = 'rgba(224, 65, 61, 1)';
                  inRow.style.backgroundColor = 'rgba(224, 65, 61, 1)';
                  colorCount = 0;
              }else{
                  inRow.changeColor = '#fff';
                  inRow.style.backgroundColor = '#fff';
                  colorCount = 1;
              }
              countid++;
              var tdCont = document.createElement('td');
              tdCont.style.textAlign = 'center';
              tdCont.style.paddingLeft = '12px';
              tdCont.style.paddingRight = '12px';
              tdCont.innerHTML = countid;
              var tdVin = document.createElement('td');
              tdVin.style.textAlign = 'center';
              tdVin.innerHTML = '<?php echo $elem->cus_serie; ?>';
              var tdCamp = document.createElement('td');
              tdCamp.style.textAlign = 'center';
              tdCamp.innerHTML = "<?php echo $elem->camp; ?>";
              var tdName = document.createElement('td');
              tdName.style.textAlign = 'center';
              tdName.innerHTML = '<?php echo $elem->cus_name; ?>';
              var tdTel = document.createElement('td');
              tdTel.style.textAlign = 'center';
              tdTel.innerHTML = '<?php echo $elem->cus_telephone ?>';
              var tdCheck = document.createElement('td');
              tdCheck.style.textAlign = 'center';
              tdCheck.style.width = '90px';
              <?php if(empty($elem->realizado)){?>
                tdCheck.innerHTML = '<input type="checkbox" id="<?php echo $elem->cus_serie; ?>" inp-check="0" style="width:25px;height:25px;" onclick="atendido(this)" />';
              <?php }else{ ?>
                tdCheck.innerHTML = '<input type="checkbox" id="<?php echo $elem->cus_serie; ?>" inp-check="1" style="width:25px;height:25px;" checked onclick="atendido(this)"/>';
              <?php } ?>

             
              inRow.appendChild(tdCont);
              inRow.appendChild(tdVin);
              inRow.appendChild(tdCamp);
              inRow.appendChild(tdName);
              inRow.appendChild(tdTel); 
              inRow.appendChild(tdCheck);
              tabla.appendChild(inRow); 
              
          <?php } ?>

          divParent.appendChild(tabla);
          document.getElementById('columnchart_pendientes').appendChild(divParent);
          

        }
    </script>

    