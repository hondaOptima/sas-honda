<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .div-print{
            margin:50px 0 0 0;
        }
        .div-print a{
            margin:0 0 40px 0;
        }
    </style>
</head>
<body>
    <div class="container"><br><br>

        <div class="div-print">
            <a  class="btn btn-primary" role="button" href="#">Imprimir</a>
             <div class='col-sm-3'>
                <div class='input-group date'>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type='date' id='fecha' class="form-control" value="<?= $fecha?>" />
                </div>
             </div>
        </div>

        <table>
            <thead>
                <tr>
                    <th>Folio: TIJ<?= date("y")."000".date("d")."00".date("m") ?></th>
                </tr>
            </thead>
        </table>
        <?php
        if(count($entregas) > 0) {
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th colspan="9">Entregas <?= $fecha_completa ?></th>
                </tr>
                <tr>
                    <td>#</td>
                    <td>Hora</td>
                    <td>Auto</td>
                    <td>Vin</td>
                    <td>Cliente</td>
                    <td>Sala</td>
                    <td>Asesor</td>
                    <td>Responsable</td>
                </tr>
                <?php
                    $i = 1;
                    foreach($entregas AS $r)
                    {
                        $s = '<tr>
                            <td>'.$i++.'</td>
                            <td>'.$r->hora.'</td>
                            <td>'.$r->auto.'</td>
                            <td>'.$r->vin.'</td>
                            <td>'.$r->cliente.'</td>
                            <td>'.$r->sala.'</td>
                            <td>'.$r->nombre.'</td>
                            <td></td>
                        </tr>';
                        echo $s;
                    }
                ?>
            </thead>
        </table>

        <?php
        }
        if(count($traslados) > 0)
        {
            ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th colspan="9">Traslados <?= $fecha_completa ?></th>
                    </tr>
                    <tr>
                        <td>#</td>
                        <td>Hora</td>
                        <td>Auto</td>
                        <td>Trasladista</td>
                        <td>Vin</td>
                        <td>Almacén</td>
                    </tr>
                    <?php
                        $i = 1;
                        foreach($traslados AS $r)
                        {
                            $al = (empty($r->almacen)) ? "Sin Asignar Almacén" : $almacenes[$r->almacen];
                            $s = '<tr>
                                <td>'.$i++.'</td>
                                <td>'.$r->hora.'</td>
                                <td>'.$r->auto.'</td>
                                <td>'.$r->nombre.'</td>
                                <td>'.$r->vin.'</td>
                                <td>'.$al.'</td>
                            </tr>';
                            echo $s;
                        }
                    ?>
                </thead>
            </table>
            <?php
        }
        ?>

    </div>
</body>
</html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
        console.log("Ready");
        $("#fecha").change(function(){
            var fechaactual = $(this).val();
            window.location.href = '<?= base_url(); ?>traslados-entregas/<?= (!empty($_SESSION['sfworkshop'])) ? $_SESSION['sfworkshop'] : $_GET["ciudad"] ;?>?fecha='+ fechaactual;
        });

        $("a").click(function(e){

            $(".div-print").hide();
             window.print();

            setTimeout(function()
            { $(".div-print").show(); }, 100);
            e.preventDefault();
        });
    });


</script>
