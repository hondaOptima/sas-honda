<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?= base_url();?>reportes/saveVentaPerdida/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-table"></i> <a href="#">Auditoria Citas</a><i class="fa fa-angle-right"></i>  </li>


  <li class="pull-right" style="margin-left:8px; margin-right:8px;">

  </li>
<li class="pull-right" style=" margin-left:8px; margin-top:1px;">

   </li>

</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->


	<div class="row" style="background-color:white">
		<div style="width: 80%; margin-left: auto; margin-right: auto;">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-calendar-o"></i>Auditoria Citas
					</div>
				</div>
				<div  class="portlet-body">
				<form action="auditoria-citas" method="post">
					<table  style="width: 80%; margin-left: auto; margin-right: auto;">
						<tr>
							<td>Fecha inicio: <input name="fechai" value="<?php if(isset($fechai)){echo $fechai;} ?>" type="date" min="2015-06-23" required/></td>
							<td>Fecha fin: <input name="fechaf" value="<?php if(isset($fechaf)){echo $fechaf;} ?>" type="date" min="2015-06-23" required/></td>
							<td>Sucursal:	<select name="sucursal" required> <option></option>
								<?php
									foreach($sucursales as $sucursal)
									{
										if($sucursal->wor_idWorkshop == $idSucursal){ $sel = "selected"; }
										else {$sel = "";}
											echo '<option value="'.$sucursal->wor_idWorkshop.'" '.$sel.'>'.$sucursal->wor_city.'</option>';
									}
									?>
								</select>
							</td>
							<td><input type="submit" value="Consultar"/></td>
						</tr>
					</table>
				</form>
				<h3>Historial De Citas</h3>
				<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%" border="1px" style="margin-top:5px; border-style: solid; border-color: #666;">
					<thead>
						<tr>
							<th>#</th>
                            <th>IDAPP</th>
                            <th>Status</th>
							<th>Ciudad</th>
							<th>Asesor</th>
							<th>Fecha</th>
							<th>Cliente</th>
							<th>Correo</th>
							<th>Telefono</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(isset($citas)){ //valida si esta definida la variable
							$cont = 1;
							foreach($citas as $cita) // recorre los renglones del arreglo
							{
								if($cita->app_status==0) $status='En Espera';
								if($cita->app_status==1) $status='Atendida';
								if($cita->app_status==2) $status='Eliminada';
								echo'<tr>';
									echo '<td>'.$cont.'</td>';
									echo '<td>'.$cita->app_idAppointment.'</td>';
									echo '<td>'.$status.'</td>';
									echo '<td>'.$cita->wor_city.'</td>';
									echo '<td>'.$cita->sus_name.' '.$cita->sus_lastName.'</td>';
									echo '<td>'.$cita->aud_fecha.'</td>';
									echo '<td>'.$cita->app_customerName.' '.$cita->app_customerLastName.'</td>';
									echo '<td>'.$cita->app_customerEmail.'</td>';
									echo '<td>'.$cita->app_customerTelephone.'</td>';
								echo'</tr>';
									$cont++;
							}
						}
						?>
					</tbody>
				</table>
				</div>
			</div>

		</div>
	 </div>
	<script>
		$("#tblHistorial").dataTable();
	</script>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
