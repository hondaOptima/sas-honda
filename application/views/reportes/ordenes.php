<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" >
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-table"></i> <a href="#">Reportes</a> <i class="fa fa-angle-right"></i> </li>
  
 
  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" onchange="this.form.submit()">
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
  </li>

        
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->


<div class="row" style="background-color:white">
	<div class="col-md-6 col-sm-6">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Lista de Archivos por Ciudad
                </div>
            </div>
            <div class="portlet-body">
 <table width="100%" style="font-size:10px; text-align:center" ><tr>
 <td style="border:1px solid #666">Ciudad</td>
  <td style="border:1px solid #666">Nombre de Archivo</td>
   <td style="border:1px solid #666">Acciones</td>
   </tr>
  <?php foreach($archivos as $ar) {?>
  <tr>
  <td><?php 
   if($ar->cin_ciudad=='3'){echo 'Tijuana';}
   if($ar->cin_ciudad=='2'){echo 'Mexicali';}
   if($ar->cin_ciudad=='1'){echo 'Ensenada';}
   ?></td>
  <td><?php echo $ar->cin_nombre;?></td>  
 <td><a href="<?php echo base_url();?>cronjobs/recibiranalisis_cadahora.php?file=<?php echo $ar->cin_file;?>">Descargar</a>
 <a href="<?php echo base_url();?>reportes/deletefile/?file=<?php echo $ar->cin_file;?>">Eliminar</a>
 </td>    
  </tr>
  <?php } ?> 
   
   
   </table>

        </div>
	</div>
 </div>
 	<div class="col-md-6 col-sm-6">
    
   <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Subir Archivos
                </div>
            </div>
            <div class="portlet-body">
            <?php echo  form_open_multipart('reportes/ordenes','class="form-horizontal"'); ?>
            Ciudad <select name="ciudad">
            <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option> 
            </select>
            Seleccione el archivo  <input type="file" class="form-control"id="userfile" name="userfile[]" required>
            Nombre del Archivo<input type="text" name="nombredel" ><br>
            <input type="submit" value="Subir Achivo">
           <?php echo form_close(); ?>
            </div>
        </div>
	</div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
