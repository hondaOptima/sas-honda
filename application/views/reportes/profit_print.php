<?php header('Content-Type: text/html; charset=utf-8');?>
<?php echo  form_open_multipart('reportes/profit_print/?id='.$id.'','class="form-horizontal"'); 

foreach($resumen as $res){
	if($res->rss_ID_desc==2){
		$partes_a=$res->rss_partes;
		$piezas_a=$res->rss_piezas;
		$valor_a=$res->rss_valor;
		}
		
		if($res->rss_ID_desc==3){
		$partes_n=$res->rss_partes;
		$piezas_n=$res->rss_piezas;
		$valor_n=$res->rss_valor;
		}
		
		if($res->rss_ID_desc==4){
		$partes_7=$res->rss_partes;
		$piezas_7=$res->rss_piezas;
		$valor_7=$res->rss_valor;
		}
		
		if($res->rss_ID_desc==5){
		$partes_12=$res->rss_partes;
		$piezas_12=$res->rss_piezas;
		$valor_12=$res->rss_valor;
		}
		
		if($res->rss_ID_desc==6){
		$partes_sn=$res->rss_partes;
		$piezas_sn=$res->rss_piezas;
		$valor_sn=$res->rss_valor;
		}
	
	 } ?>

<table cellspacing="0" width="600px" border="1" style="font-family:arial; font-size:11px;">
<tr>
<td style="border:1px solid #999">INVENTARIO $ (Valor Actual en $)</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_inv_va?>" name="pro_inv_va"></td>
<td style="border:1px solid #999">2B) VENTA DE ACEITE ($ Mes)</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_v_aceite?>" name="pro_v_aceite"></td>
</tr>

<tr>
<td style="border:1px solid #999">VENTAS TOTAL DE PARTES ($ Mes)</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_v_partes?>" name="pro_v_partes"></td>
<td style="border:1px solid #999">UTILIDAD BRUTA</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_u_partes?>" name="pro_u_partes"></td>
</tr>

<tr>
<td style="border:1px solid #999">2A) VENTAS  DE ACCESORIOS ($ Mes)</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_v_acc?>" name="pro_v_acc"></td>
<td style="border:1px solid #999">UTILIDAD BRUTA</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_u_acc?>" name="pro_u_acc"></td>
</tr>

<tr>
<td style="border:1px solid #999">UTILIDAD BRUTA  ($ Mes)</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_ub?>" name="pro_ub"></td>
<td style="border:1px solid #999">GASTOS TOTALES DE LA AGENCIA </td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_gastos_a?>" name="pro_gastos_a"></td>
</tr>

<tr>
<td style="border:1px solid #999">UTILIDAD OPERACIONAL ($ Mes)</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_uo?>" name="pro_uo"></td>
<td style="border:1px solid #999">GASTOS VARIABLES </td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_gastos_v?>" name="pro_gastos_v"></td>
</tr>
</table>
<br>


<table cellspacing="0" width="500px" border="1" style="font-family:arial; font-size:11px;">
<tr bgcolor="#339900">
<td style="border:1px solid #999">DEL SISTEMA</td>
<td style="border:1px solid #999"># PARTE</td>
<td style="border:1px solid #999"># PIEZAS</td>
<td style="border:1px solid #999"># VALOR(redondeado)</td>
</tr>

<tr>
<td style="border:1px solid #999">INVENTARIO TOTAL (Valor Costo Promedio)</td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
</tr>

<tr>
<td style="border:1px solid #999">INVENTARIO ALMACENABLE</td>
<td style="border:1px solid #999"><?php echo $partes_a?></td>
<td style="border:1px solid #999"><?php echo $piezas_a?></td>
<td style="border:1px solid #999"><?php echo $valor_a?></td>
</tr>


<tr>
<td style="border:1px solid #999">INVENTARIO NO ALMACENABLE (Entrada)</td>
<td style="border:1px solid #999"><?php echo $partes_n?></td>
<td style="border:1px solid #999"><?php echo $piezas_n?></td>
<td style="border:1px solid #999"><?php echo $valor_n?></td>
</tr>


<tr>
<td style="border:1px solid #999">PEDIDOS ESPECIALES</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_ped_parte?>" name="pro_ped_parte"></td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_ped_pieza?>" name="pro_ped_pieza"></td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_ped_valor?>" name="pro_ped_valor"></td>
</tr>


<tr>
<td style="border:1px solid #999">PARTES SIN VENTA  7 - 12 MESES</td>
<td style="border:1px solid #999"><?php echo $partes_7?></td>
<td style="border:1px solid #999"><?php echo $piezas_7?></td>
<td style="border:1px solid #999"><?php echo $valor_7?></td>
</tr>


<tr>
<td style="border:1px solid #999">PARTES SIN VENTA > 12 MESES</td>
<td style="border:1px solid #999"><?php echo $partes_12?></td>
<td style="border:1px solid #999"><?php echo $piezas_12?></td>
<td style="border:1px solid #999"><?php echo $valor_12?></td>
</tr>

<tr>
<td style="border:1px solid #999">PARTES NUEVAS, SIN VENTAS</td>
<td style="border:1px solid #999"><?php echo $partes_sn?></td>
<td style="border:1px solid #999"><?php echo $piezas_sn?></td>
<td style="border:1px solid #999"><?php echo $valor_sn?></td>
</tr>


<tr>
<td style="border:1px solid #999">COMPRAS MENSUALES PARA INVENTARIO</td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_com_parte?>" name="pro_com_parte"></td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_com_pieza?>" name="pro_com_pieza"></td>
<td style="border:1px solid #999"><input type="text" value="<?= $profit[0]->pro_com_valor?>" name="pro_com_valor"></td>
</tr>


<tr>
<td style="border:1px solid #999">COMPRAS DE EMERGENCIA</td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
</tr>

<tr>
<td style="border:1px solid #999">VENTAS A COSTO</td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
</tr>

<tr>
<td style="border:1px solid #999">VENTAS PERDIDAS</td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
<td style="border:1px solid #999"></td>
</tr>

</table>
<input type="submit" value="Guardar Información">
</form>
