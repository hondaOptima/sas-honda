<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
 <?php
$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/citas">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-table"></i> <a href="#">Comportamiento de las Citas</a> <i class="fa fa-angle-right"></i> </li>
  
 
 <li class="pull-right" style="margin-top:-8px;">
    <input type="submit" value="Consultar" class="btn">
    </li>
  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" >
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
  </li>

  <li class="pull-right" style=" margin-left:8px; margin-top:1px;">
   
  <select name="year">
 <option value="2014" <?php if($year=='2014'){echo 'selected="selected"';}?>>2014</option>
  <option value="2015" <?php if($year=='2015'){echo 'selected="selected"';}?>>2015</option>
    <option value="2016" <?php if($year=='2016'){echo 'selected="selected"';}?>>2016</option>
  </select>
   </li><li class="pull-right" style=" margin-left:8px; margin-top:1px;">
   
  <select name="mes">
 
  <?php
  foreach($meses as $numm=> $nommes){
if($numm==$mes){$sel='selected="selected"';}else{$sel='';}	  
echo'<option value="'.$numm.'"  '.$sel.'>'.$nommes.'</option>';	  
	  }
  
  ?>
  </select>
   </li>
   
  
    
        
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->

<?php

function citas($array,$fecha){
$n=0;
$r=0;	
$f=0;
$s=0;
$c=0;
$i=0;
$in='';
$fo='';
$p=0;
foreach($array as $ar){
	list($in,$fo)=explode('-',$ar->app_folio);
	if($ar->app_day==$fecha){$n++;}
	if($ar->app_day==$fecha && $ar->app_status=='1'){$r++;}
	if($ar->app_day==$fecha && $ar->app_status=='0'){$f++;}
	if($ar->app_day==$fecha && $ar->app_referencia=='2'){$s++;}	
	if($ar->app_day==$fecha && $ar->app_status=='1' && $in=='T'){$c++;}
	if($ar->app_day==$fecha && $ar->app_status=='1' && $in=='I'){$i++;}	
	if($ar->app_day==$fecha && $ar->app_customerArrival=='on'){$p++;}		
	
	}
	
	return array('n'=>$n,'r'=>$r,'f'=>$f,'s'=>$s,'c'=>$c,'i'=>$i,'p'=>$p);	
	}


function citasanio($array,$fecha){
$n=0;
$r=0;	
$f=0;
$s=0;
$c=0;
$i=0;
$in='';
$fo='';
$p=0;
foreach($array as $ar){
	list($in,$fo)=explode('-',$ar->app_folio);
	list($an,$ms,$di)=explode('-',$ar->app_day);
	
	if($ms==$fecha){$n++;}
	if($ms==$fecha && $ar->app_status=='1'){$r++;}
	if($ms==$fecha && $ar->app_status=='0'){$f++;}
	if($ms==$fecha && $ar->app_referencia=='2'){$s++;}	
	if($ms==$fecha && $ar->app_status=='1' && $in=='T'){$c++;}
	if($ms==$fecha && $ar->app_status=='1' && $in=='I'){$i++;}	
	if($ms==$fecha && $ar->app_customerArrival=='on'){$p++;}	
	
	}
	
	return array('n'=>$n,'r'=>$r,'f'=>$f,'s'=>$s,'c'=>$c,'i'=>$i,'p'=>$p);	
	}
	
	

function citassin($array,$fecha){
$n=0;
$r=0;	
$f=0;
$s=0;
$c=0;
$i=0;
foreach($array as $ar){
	if($ar->app_day==$fecha){$n++;}
	if($ar->app_day==$fecha && $ar->app_status=='1'){$r++;}
	if($ar->app_day==$fecha && $ar->app_status=='0'){$f++;}
	if($ar->app_day==$fecha && $ar->app_referencia=='2'){$s++;}	
	if($ar->app_day==$fecha && $ar->app_referencia=='1'){$c++;}
	if($ar->app_day==$fecha && $ar->app_referencia=='3'){$i++;}		
	
	}
	
	return array('n'=>$n,'r'=>$r,'f'=>$f,'s'=>$s,'c'=>$c,'i'=>$i);	
	}


function citassinanio($array,$fecha){
$n=0;
$r=0;	
$f=0;
$s=0;
$c=0;
$i=0;



foreach($array as $ar){
	list($an,$ms,$di)=explode('-',$ar->app_day);
	if($ms==$fecha){$n++;}
	if($ms==$fecha && $ar->app_status=='1'){$r++;}
	if($ms==$fecha && $ar->app_status=='0'){$f++;}
	if($ms==$fecha && $ar->app_referencia=='2'){$s++;}	
	if($ms==$fecha && $ar->app_referencia=='1'){$c++;}
	if($ms==$fecha && $ar->app_referencia=='3'){$i++;}		
	
	}
	
	return array('n'=>$n,'r'=>$r,'f'=>$f,'s'=>$s,'c'=>$c,'i'=>$i);	
	}

	
?>
<div class="row" style="background-color:white">
	<div class="col-md-12 col-sm-12">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table"></i><?php echo $meses[$mes];?>
                </div>
            </div>
            <div class="portlet-body">
 <table width="100%" style="font-size:12px;"  class="table table-striped table-bordered table-hover" >
 <tr>
 <td style="border:1px solid #666; " width="160px;"></td>
 <?php 
 for($x=1; $x<=$ndias; $x++){
	 
echo '<td style="border:1px solid #666; font-weight:bold;text-align:center; ">'.$x.'</td>';	 
	 
	 }
 ?>
 <td colspan="2" style="border:1px solid #666;font-weight:bold;text-align:center; " width="150px;">Resumen</td>
 </tr>
 <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Citas</td>
 <?php 
 $tc=0;
 $tcd=0;
 for($x=1; $x<=$ndias; $x++){
	 if($x < 10){$x='0'.$x;}
	 $cval=citas($citas,$year.'-'.$mes.'-'.$x);
	 
echo '<td style="border:1px solid #666;text-align:center;">'.($cval['n']).'</td>';	 
	 $tc +=$cval['n'];
	 
	 if($x<=date('d')){$tcd +=$cval['n'];}
	 }
 ?>
  <td style="border:1px solid #666;text-align:left; width:140px; font-weight:bold">Citas</td>
    <td style="border:1px solid #666;text-align:center; width:20px;"><?php echo $tc;?></td>
 </tr>
 
 <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Asistieron</td>
 <?php 
 $ta=0;
 $tad=0;
 for($x=1; $x<=$ndias; $x++){
	 if($x < 10){$x='0'.$x;}
	$cval=citas($citas,$year.'-'.$mes.'-'.$x);
	
echo '<td style="border:1px solid #666;text-align:center;">'.($cval['r']).'</td>';	 
	 $ta+=$cval['r'];
	 if($x<=date('d')){$tad +=$cval['r'];}
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Asistieron</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $ta;?></td>
 </tr>
 
 
  <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Faltaron</td>
 <?php 
 $tf=0;
 $tfd=0;
 for($x=1; $x<=$ndias; $x++){
	  if($x < 10){$x='0'.$x;}
	$cval=citas($citas,$year.'-'.$mes.'-'.$x); 
echo '<td style="border:1px solid #666;text-align:center;">'.$cval['f'].'</td>';

	 $tf+=$cval['f'];
	 if($x<=date('d')){$tfd +=$cval['f'];}
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Faltaron</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $tf;?></td>
 </tr>
 
  <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Puntuales</td>
 <?php 
 $tp=0;
 for($x=1; $x<=$ndias; $x++){
	 if($x <10){$x='0'.$x;}
$cval=citas($citas,$year.'-'.$mes.'-'.$x); 
echo '<td style="border:1px solid #666;text-align:center;">'.$cval['p'].'</td>';

	 $tp+=$cval['p']; 
	 
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Puntuales</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $tp;?></td>
 </tr>
 
  <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">% Puntualidad</td>
 <?php 
 $punt=0;
 $asis=0;
 $spor=0;
 for($x=1; $x<=$ndias; $x++){
	  if($x <10){$x='0'.$x;}
$cval=citas($citas,$year.'-'.$mes.'-'.$x);	 

$puntuales=$cval['p'];
if($puntuales==0){$por=0;}
else{
$asistieron=$cval['r'];
$por=($puntuales / $asistieron)*100;}
echo '<td style="border:1px solid #666;text-align:center;">'. number_format($por, 2, '.', '').'</td>';	

$punt+=$cval['p']; 
$asis+=$cval['r'];
if($punt==0){$spor=0;}
else{
	$spor=($punt / $tad) * 100;
	$gpun=$spor;
	}	 
	 
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">% Puntualidad</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo number_format($spor, 2, '.', '');?></td>
 </tr>
 
  <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">% Impuntualidad</td>
  <?php 
 $punt=0;
 $asis=0;
 $spor=0;
 for($x=1; $x<=$ndias; $x++){
 if($x <10){$x='0'.$x;}	 
$cval=citas($citas,$year.'-'.$mes.'-'.$x);
$cvalsin=citassin($citassin,$year.'-'.$mes.'-'.$x); 	 

$puntuales=$cval['p'];
if($puntuales==0){$por=0;}
else{
$asistieron=$cval['r'];
$por=($puntuales / $asistieron)*100;
$por=100 - $por;}
echo '<td style="border:1px solid #666;text-align:center;">'. number_format($por, 2, '.', '').'</td>';	

$punt+=$cval['p']; 
$asis+=$cval['r'];
if($punt==0){$spor=0;}
else{
	$spor=($punt / $tad) * 100;
	$spor=100 - $spor;
	$gpor=$spor;
	}	 
	 
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">% Impuntualidad</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo number_format($spor, 2, '.', '')?></td>
 </tr>
 
 
   <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Recibidos sin Citas</td>
 <?php 
 $ts=0;
 for($x=1; $x<=$ndias; $x++){
	 
 if($x < 10){$x='0'.$x;}
	$cval=citas($citas,$year.'-'.$mes.'-'.$x); 
	 $cvalsin=citassin($citassin,$year.'-'.$mes.'-'.$x);
echo '<td style="border:1px solid #666;text-align:center;">'.($cvalsin['n']).'</td>'; 
	 $ts+=$cvalsin['n'];
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Recibidos sin Citas</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $ts;?></td>
 </tr>

  <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Con Citas por Telefono</td>
 <?php 
 $tt=0;
 $ttd=0;
 for($x=1; $x<=$ndias; $x++){
	  if($x < 10){$x='0'.$x;}
	$cval=citas($citas,$year.'-'.$mes.'-'.$x); 
	
echo '<td style="border:1px solid #666;text-align:center;">'.$cval['c'].'</td>'; 
	 $tt+=$cval['c'];
	 if($x<=date('d')){$ttd +=$cval['c'];}
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Con Citas por Telefono</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $tt;?></td>
 </tr>
  
   <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Con Cita por Internet</td>
 <?php 
 $ti=0;
 $tid=0;
 for($x=1; $x<=$ndias; $x++){
	  if($x < 10){$x='0'.$x;}
	$cval=citas($citas,$year.'-'.$mes.'-'.$x); 
	
echo '<td style="border:1px solid #666;text-align:center;">'.$cval['i'].'</td>';  
$ti+=$cval['i'];	 
 if($x<=date('d')){$tid +=$cval['i'];}
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Con Cita por Internet</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $ti;?></td>
 </tr> 
 
   <tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Recepcion Total</td>
 <?php 
 $to=0;
 for($x=1; $x<=$ndias; $x++){
	 if($x < 10){$x='0'.$x;}
	$cval=citas($citas,$year.'-'.$mes.'-'.$x);
	 $cvalsin=citassin($citassin,$year.'-'.$mes.'-'.$x); 
echo '<td style="border:1px solid #666;text-align:center;">'.($cval['r'] + $cvalsin['n']).'</td>';	 
	 $to+=$cval['r'] + $cvalsin['n'];
	 }
 ?>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Recepción Total</td>
    <td style="border:1px solid #666;text-align:center;"><?php echo $to;?></td>
 </tr>
  
  
   </table>

        </div>
	</div>
 </div>
 	
</div>

<div class="row" style="background-color:white">
	<div class="col-md-12 col-sm-12">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table"></i>Resumen Anual
                </div>
            </div>
            <div class="portlet-body">
            <table width="100%"  class="table table-striped table-bordered table-hover">
            <tr>
            <td style="border:1px solid #999; font-weight:bold" width="150px;"></td>
            <?php
			foreach($meses as $nms=>$nomm){
				echo '<td width="6.1%" align="center" style="border:1px solid #999;text-align:center; font-weight:bold">'.$nomm.'</td>';
				}
            
			?>
            <td style="border:1px solid #999; font-weight:bold; width:160px;"  colspan="2" align="center">Resumen</td>
            </tr>
            
<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">Citas</td>
<?php
$tca=0;
			foreach($meses as $nms=>$nomm){
				$cvalanio=citasanio($citasy,$nms); 
	            
				
				echo '<td style="border:1px solid #999;text-align:center;">'.($cvalanio['n']).'</td>';
				$tca+=$cvalanio['n'];
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; width:140px; font-weight:bold">Citas</td>
 <td style="border:1px solid #666;text-align:center; width:20px;"><?php echo $tca;?></td>
</tr>

<tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Asistieron</td>
<?php
$tay=0;
			foreach($meses as $nms=>$nomm){
				
				$cvalanio=citasanio($citasy,$nms); 
echo '<td style="border:1px solid #666;;text-align:center;">'.($cvalanio['r']).'</td>';	 
	 $tay+=$cvalanio['r'];
	 
				
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Asistieron</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $tay;?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">Faltaron</td>
<?php
$tfy=0;
			foreach($meses as $nms=>$nomm){
			$cvalanio=citasanio($citasy,$nms); 
echo '<td style="border:1px solid #666;;text-align:center;">'.$cvalanio['f'].'</td>';

	 $tfy+=$cvalanio['f'];
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Faltaron</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $tfy;?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">Puntuales</td>
<?php
$tpy=0;
			foreach($meses as $nms=>$nomm){
$cvalanio=citasanio($citasy,$nms); 
echo '<td style="border:1px solid #666;text-align:center;">'.$cvalanio['p'].'</td>';

	 $tpy+=$cvalanio['p']; 
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Puntuales</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $tpy;?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">% Puntualidad</td>
<?php
$punt=0;
 $asis=0;
 $spor=0;
			foreach($meses as $nms=>$nomm){
$cvalanio=citasanio($citasy,$nms);
$cvalsinanio=citassinanio($citassiny,$nms); 	 

$puntuales=$cvalanio['p'];
if($puntuales==0){$por=0;}
else{
$asistieron=$cvalanio['r'] + $cvalsinanio['n'];
$por=($puntuales / $asistieron)*100;}
echo '<td style="border:1px solid #666;text-align:center;">'. number_format($por, 2, '.', '').'</td>';	

$punt+=$cvalanio['p']; 
$asis+=$cvalanio['r'] + $cvalsinanio['n'];
if($punt==0){$spor=0;}
else{
	$spor=($punt / $asis) * 100;
	}	 				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">% Puntualidad</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo number_format($spor, 2, '.', '');?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">% Impuntualidad</td>
<?php
$punt=0;
 $asis=0;
 $spor=0;
			foreach($meses as $nms=>$nomm){
$cvalanio=citasanio($citasy,$nms);
$cvalsinanio=citassinanio($citassiny,$nms); 	 

$puntuales=$cvalanio['p'];
if($puntuales==0){$por=0;}
else{
$asistieron=$cvalanio['r'] + $cvalsinanio['n'];
$por=($puntuales / $asistieron)*100;
$por=100 - $por;
}
echo '<td style="border:1px solid #666;text-align:center;">'. number_format($por, 2, '.', '').'</td>';	

$punt+=$cvalanio['p']; 
$asis+=$cvalanio['r'] + $cvalsinanio['n'];
if($punt==0){$spor=0;}
else{
	$spor=($punt / $asis) * 100;
	$spor=100 - $spor;
	}	 				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">% Impuntualidad</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo  number_format($spor, 2, '.', '');?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">Recibidos sin Citas</td>
<?php
$tsy=0;
			foreach($meses as $nms=>$nomm){
				$cvalanio=citasanio($citasy,$nms); 
	 $cvalsinanio=citassinanio($citassiny,$nms);
echo '<td style="border:1px solid #666;text-align:center;">'.($cvalsinanio['n']).'</td>'; 
	 $tsy+=$cvalsinanio['n'];
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Recibidos sin Citas</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $tsy;?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">Con Citas por Telefono</td>
<?php
$tty=0;
			foreach($meses as $nms=>$nomm){
				$cvalanio=citasanio($citasy,$nms); 
	
echo '<td style="border:1px solid #666;text-align:center;">'.$cvalanio['c'].'</td>'; 
	 $tty+=$cvalanio['c'];
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Con Citas por Telefono</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $tty;?></td>
</tr>

<tr>
 <td style="border:1px solid #666; text-align:left; font-weight:bold">Con Citas por Internet</td>
<?php
$tiy=0;
			foreach($meses as $nms=>$nomm){
				$cvalanio=citasanio($citasy,$nms); 
	
echo '<td style="border:1px solid #666;text-align:center;">'.$cvalanio['i'].'</td>';  
$tiy+=$cvalanio['i'];	
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Con Citas por Internet</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $tiy;?></td>
</tr>

<tr>
 <td style="border:1px solid #666;text-align:left; font-weight:bold">Recepcion Total</td>
<?php
$toy=0;
			foreach($meses as $nms=>$nomm){
	 $cvalanio=citasanio($citasy,$nms);
	 $cvalsinanio=citassinanio($citassiny,$nms); 
echo '<td style="border:1px solid #666;text-align:center;">'.($cvalanio['r'] + $cvalsinanio['n']).'</td>';	 
	 $toy+=$cvalanio['r'] + $cvalsinanio['n'];
				}
            
			?>

 <td style="border:1px solid #666; text-align:left; font-weight:bold">Recepcion Total</td>
 <td style="border:1px solid #666;text-align:center;"><?php echo $toy;?></td>
</tr>

            
</table>

</div>
	</div>
 </div>
 	
</div>

<div class="row" style="background-color:white">

<div class="col-md-4 col-sm-4">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-adjust"></i>Citas de Mes al Dia
                </div>
            </div>
            <div class="portlet-body">
            <div id="piechart"></div>
</div></div></div>


<div class="col-md-4 col-sm-4">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-adjust"></i>Puntualidad en el Mes al Dia.
                </div>
            </div>
            <div class="portlet-body"><div id="piechartc"></div>
</div></div></div>

<div class="col-md-4 col-sm-4">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-adjust"></i>Mezcla de Recepción al Dia.
                </div>
            </div>
            <div class="portlet-body"><div id="piechartb"></div>
</div></div></div>





<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>

<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Asistieron', 'Faltaron'],
          ['Asistieron',   <?php echo $tad;?>],
		  ['Faltaron',     <?php echo $tfd;?>]
        ]);

        var options = {
         
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
	  
	  google.setOnLoadCallback(drawChartb);
      function drawChartb() {

        var datab = google.visualization.arrayToDataTable([
          ['Asistieron', 'Faltaron'],
          ['Sin Cita',   <?php echo $ts;?>],
		  ['Cita por Telefono',     <?php echo $ttd;?>],
		  ['Cita por Internet',     <?php echo $tid;?>]
        ]);

        var options = {
         
        };

        var chartb = new google.visualization.PieChart(document.getElementById('piechartb'));

        chartb.draw(datab, options);
      }
	  
	  google.setOnLoadCallback(drawChartc);
      function drawChartc() {

        var datac = google.visualization.arrayToDataTable([
          ['Asistieron', 'Faltaron'],
		  ['% Puntualidad',    <?php echo $ta;?>],
		  ['% Impuntualidad',   <?php echo $ta - $tf;?>]
        ]);

        var options = {
         
        };

        var chartc = new google.visualization.PieChart(document.getElementById('piechartc'));

        chartc.draw(datac, options);
      }
    </script>
