<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data);
$name=array('Monday'=>'Lun','Tuesday'=>'Mar','Wednesday'=>'Mie','','Thursday'=>'Jue','Friday'=>'Vie','Saturday'=>'Sab','Sunday'=>'Dom');
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$horas=array('10:00 a.m.','12:00 a.m.','02:00 a.m.','04:00 a.m.');
if(empty($_GET['fecha'])) $fecha= date('m/d/Y'); else $fecha=$_GET['fecha'];
 ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/">
<ul class="page-breadcrumb breadcrumb">
    <li><i class="fa fa-calendar"></i> <a href="#">Calendario de Entregas</a> <i class="fa fa-angle-right"></i> <a href="<?= base_url()."traslados-entregas/".$_SESSION['sfworkshop'] ?>" style="color:#389dc7;">Imprimir Reporte</a></li>

<li class="pull-right" >
    <div  class="input-group input-medium date " id="date-pickerbb" data-date-format="mm/dd/yyyy" style="text-align:right;">
        <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
        Fecha: <?php echo $fecha;?> <i style="cursor:pointer; float:right; margin-left:5px;" class="fa fa-calendar  input-group-btn"></i>
    </div>
</li>



</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->


<div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"><i class="fa fa-calendar"></i>Calendario de Entregas <?= $_SESSION['sfworkshop']?></div>
      </div>
      <div class="portlet-body">
      <div id="calendario_entregas"></div>
           </div>
    </div>






  <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="portlet">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-clock-o"></i><b>Entregas Urgentes <i class="fa fa-calendar"></i> </div>
          </div>
          <div class="portlet-body">

              <div id="tabla_urgentes"></div>

            <a class="btn btn-primary btn-block agendarsin" id="">Añadir Entrega Urgente</a> </div>
        </div>
      </div>
    </div>
  </div>

  </div>
  </div>

  <div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Apartado Logistica:</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="referencia" value="2">


        <div class="row"  >
          <div class="col-md-6">
            <div class="form-group">
              Fecha:
              <br>
              <input type="text" name="r_fecha" disabled="disabled">
              <br>
              Hora:
              <br>
              <input type="text" name="r_hora" disabled="disabled"><br>
              Sala:
              <br>
              <input type="text" name="r_sala" disabled="disabled"><br>
              Ciudad:
              <br>
              <input type="text" name="r_ciudad" disabled="disabled">
              <br>
              Descrici&oacute;n:
              <br>

                <input type="text" name="r_desc" value="Apartado Logistica" readonly="readonly">
                <div class="btn-xs btn btn-info guardarInfo_lg">Guardar</div>
            </div>

          </div>

          </div>
          <!--/-->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

      </div>
    </div>

     <!-- /.modal-content -->
  </div>
</div>
</div>
<div class="modal fade" id="wideSin" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Registro de Cita Urgente:</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="referencia" value="2">


        <div class="row"  >
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-1">No. Vin:</label>
              <div class="col-md-6">
                <input type="text" name="r_vin">

              </div>
                <div class="btn-xs btn btn-info buscar">OK</div>
            </div>
            <div class="loading" ></div>
          </div>
          <div class="infoCrm"></div>
          </div>
          <!--/-->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <input type="submit" class="btn btn-info guardarInfo" value="Guardar">
      </div>
    </div>

     <!-- /.modal-content -->
  </div>
</div>




<!-- /.modal-dialog -->

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>

   <script>
$(document).ready(function(){

lista_citas_urgentes();

var sas='1';
var agencia="<?= $_SESSION['sfworkshop']?>";
var ciudad="<?= $_SESSION['sfworkshop']?>";
var fecha="<?= $fecha?>";
$.post("http://hcrm.gpoptima.net/querys/inventario/calendario_entregas.php",{agencia:agencia,fecha:fecha,ciudad:ciudad,sas:sas}, function(data)
{
	$('#calendario_entregas').html(data);
});

$('.agendarsin').live('click',function(){

$('#wideSin').modal('show');
	});


$('.buscar').live('click',function(){
var vin=$('input[name=r_vin]').val();
$.post("http://hcrm.gpoptima.net/querys/inventario/buscar_en_proceso.php",{vin:vin}, function(data) {

$('.infoCrm').html(data);
});

	});

$('.eliminarcita').live('click',function(){
var vin=$(this).attr('id');
$.post("http://hcrm.gpoptima.net/querys/inventario/deleteCita.php",{vin:vin}, function(data) {
lista_citas_urgentes();

});
	});


$('.guardarInfo').live('click',function(){

	$('.loading').html('Guardando Informacion...');

	var fecha=$('input[name=r_fecha]').val();
	var hora=$('input[name=r_hora]').val();
	var minu=$('input[name=r_min]').val();
	var thora=""+hora+":"+minu+"";
	var sala=$('select[name="r_sala"]').val();

	var agencia=$('input[name=r_ciudad]').val();
	var vin=$('input[name=r_vin]').val();
	var auto=$('input[name=r_auto]').val();
	var asesor=$('input[name=idasesor]').val();
	var cliente=$('input[name=r_cliente]').val();
	var tipo=1;

	$.post("http://hcrm.gpoptima.net/querys/inventario/savecita.php",{fecha:fecha,hora:thora,sala:sala,agencia:agencia,vin:vin,auto:auto,asesor:asesor,cliente:cliente,tipo:tipo}, function(data) {
	alert('Cita Agregada con Exito');
	$('#wideSin').hide();

	lista_citas_urgentes();
	});

});

$('.guardarInfo_lg').live('click',function(){

	$('.loading').html('Guardando Informacion...');

	var fecha=$('input[name=r_fecha]').val();
	var hora=$('input[name=r_hora]').val();
	var minu=$('input[name=r_min]').val();
	var thora=""+hora+"";
	var sala=$('input[name="r_sala"]').val();

	var agencia=$('input[name=r_ciudad]').val();
	var vin=$('input[name=r_desc]').val();
	var auto=$('input[name=r_auto]').val();
	var asesor=$('input[name=idasesor]').val();
	var cliente=$('input[name=r_cliente]').val();
	var tipo=0;

	$.post("http://hcrm.gpoptima.net/querys/inventario/savecita.php",{fecha:fecha,hora:thora,sala:sala,agencia:agencia,vin:vin,auto:auto,asesor:asesor,cliente:cliente,tipo:tipo}, function(data) {
	alert('Cita Agregada con Exito');
	$('#wide').hide();
	lista_citas_urgentes();
	});

});


$(function () {
	 $('#date-pickerbb').datepicker({
	language: 'es',
	isRTL: false,
         autoclose:true

	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });


	});


	function lista_citas_urgentes()
	    {
		agencia="<?= $_SESSION['sfworkshop']?>";
		$.post("http://hcrm.gpoptima.net/querys/inventario/lista_citas_urgentes.php",{agencia:agencia}, function(data) {
$('#tabla_urgentes').html(data);
         });

		}

});
</script>
