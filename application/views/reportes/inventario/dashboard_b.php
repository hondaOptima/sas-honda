<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboardmodel');
		$this->load->model('Reportesmodel');
		$this->load->model('Permisomodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Productividadmodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		
		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
		 ini_set('memory_limit', '1024M');
	}
	public function index()
	{
		if(empty($_GET['year'])){
				$year=date('Y');}
			else{
				$year=$_GET['year'];
				}
				
		if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}
			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
			if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
			
			if(empty($_GET['dia'])){
			$dia=date('d');
			}else{
				
				$dia=$_GET['dia'];
				}	
				
	$mesi=$year.'-'.$mes.'-'.'01';
	$mesf=$year.'-'.$mes.'-31';				
	$data['fecha']=$date;		
	$data['taller']=$taller;		
	$data['mes']=$mes;
	$data['anio']=$year;
	$data['dia']=$dia;
	$data['dias']=cal_days_in_month(CAL_GREGORIAN, $mes, date('Y'));
	$data['meses']=$this->Dashboardmodel->meses();
	$data['app']=$this->Dashboardmodel->app();
	$data['ppto']=$this->Dashboardmodel->pptomes($mes);
	$data['pptoa']=$this->Dashboardmodel->pptomesA();
	$data['desgloce']=$this->Dashboardmodel->desgloce();
	$data['citasm']=$this->Reportesmodel->getCitas('2',$mesi,$mesf);
	$data['citast']=$this->Reportesmodel->getCitas('3',$mesi,$mesf);
	$data['citase']=$this->Reportesmodel->getCitas('1',$mesi,$mesf);
	//Obtenemos la cantidad de días que tiene septiembre del 2008
    $data['lista']=$this->Dashboardmodel->getMonthDays($mes, $year);
	$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
	$data['pmes']=641977.37;
	$data['diastra']=23;
	
	// Utilizacion del taller.
	 $data['capacidad'] = $this->Productividadmodel->capacidad($taller);
	// utilizacion en horas
	 $ectaller=$this->Productividadmodel->capacidadtaller($date,1,$data['capacidad']);
     $mctaller=$this->Productividadmodel->capacidadtaller($date,2,$data['capacidad']);
     $tctaller=$this->Productividadmodel->capacidadtaller($date,3,$data['capacidad']);
	
	
	$this->load->view('dashboard/vista',$data);
	}
/*	public function index()
	{
		if(empty($_GET['year'])){
				$year=date('Y');}
			else{
				$year=$_GET['year'];
				}
				
		if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}
			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
			if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
			
			if(empty($_GET['dia'])){
			$dia=date('d');
			}else{
				
				$dia=$_GET['dia'];
				}	
				
	$mesi=$year.'-'.$mes.'-'.'01';
	$mesf=$year.'-'.$mes.'-31';				
	$data['fecha']=$date;		
	$data['taller']=$taller;		
	$data['mes']=$mes;
	$data['anio']=$year;
	$data['dia']=$dia;
	$data['dias']=cal_days_in_month(CAL_GREGORIAN, $mes, date('Y'));
	$data['meses']=$this->Dashboardmodel->meses();
	$data['ppto']=$this->Dashboardmodel->pptomes($mes,$dia);
	$data['pptoTotal']=$this->Dashboardmodel->pptomesTotal($mes);
	$data['pptoa']=$this->Dashboardmodel->pptomesA($mes);
	$data['desgloce']=$this->Dashboardmodel->desgloce();
	$data['desglocem']=$this->Dashboardmodel->desglocemes($mes,$dia);
	$data['desglocea']=$this->Dashboardmodel->desgloceAcumulado($mes);
	
	//CITAS
	$data['citasm']=$this->Reportesmodel->getCitas('2',$mesi,$mesf);
	$data['citast']=$this->Reportesmodel->getCitas('3',$mesi,$mesf);
	$data['citase']=$this->Reportesmodel->getCitas('1',$mesi,$mesf);
	//Obtenemos la cantidad de días que tiene septiembre del 2008
    $data['lista']=$this->Dashboardmodel->getMonthDays($mes, $year);
	$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
	//meta
	$data['hos']=3.5;
	
	// Utilizacion del taller.
	 $data['capacidad'] = $this->Productividadmodel->capacidad($taller);
	// utilizacion en horas
	 $data['ectaller']=$this->Productividadmodel->capacidadtaller($date,1,$data['capacidad']);
     $data['mctaller']=$this->Productividadmodel->capacidadtaller($date,2,$data['capacidad']);
     $data['tctaller']=$this->Productividadmodel->capacidadtaller($date,3,$data['capacidad']);
	 //real de utilizacion por dia
	  $data['ruens']=$this->Productividadmodel->utilizacion($date,1);
      $data['rutij']=$this->Productividadmodel->utilizacion($date,2);
      $data['rumex']=$this->Productividadmodel->utilizacion($date,3);	
	
	
	$this->load->view('dashboard/vista',$data);
	}*/
	
	
	
	function detalle()
	{
		if(empty($_GET['year'])){
				$year=date('Y');}
			else{
				$year=$_GET['year'];
				}
				
		if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}
			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
			if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
			
			if(empty($_GET['dia'])){
			$dia=date('d');
			}else{
				
				$dia=$_GET['dia'];
				}	
				
	$mesi=$year.'-'.$mes.'-'.'01';
	$mesf=$year.'-'.$mes.'-31';				
	$data['fecha']=$date;		
	$data['taller']=$taller;		
	$data['mes']=$mes;
	$data['anio']=$year;
	$data['dia']=$dia;
	$data['dias']=cal_days_in_month(CAL_GREGORIAN, $mes, date('Y'));
	$data['meses']=$this->Dashboardmodel->meses();

	$data['desglocem']=$this->Dashboardmodel->desglocemes($mes,date('d'));
    $data['lista']=$this->Dashboardmodel->getMonthDays($mes, $year);
	$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
	$data['pmes']=641977.37;
	$data['diastra']=23;
	$data['hos']=3.5;

	
	$this->load->view('dashboard/detalle',$data);
	}
	
	
	
	
	
	
	
	
	
	
	function citasPorConfirmar(){
		echo 100;
		}
	
	
	function ppto()
	{
	if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}
			
	if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}
		else{
			$taller=$_GET['taller'];
			}
			
	if(empty($_GET['anio'])){
			$anio='2015';
			}
		else{
			$anio=$_GET['anio'];
			}
	$data['anio']=$anio;		
	$data['taller']=$taller;	
	$data['mes']=$mes;			
	$data['desgloce']=$this->Dashboardmodel->desglocemes($mes,date('d'));			
	$data['meses']=$this->Dashboardmodel->meses();
	$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
	$data['nomina']=$this->Dashboardmodel->nomina($taller,$anio,$mes);
	$data['ppto']=$this->Dashboardmodel->ppto($taller,$anio);
	$data['dias_tra']=$this->Dashboardmodel->dias_tra($taller,$anio,$mes);
	
	$this->load->view('dashboard/ppto/vista',$data);
	
	}
	
	function editppto()
	{
		$this->form_validation->set_rules('idpp', 'IDPPTO', '');
 
        if ($this->form_validation->run())
        {
			$ppto = array(
						'ppt_01'=>$this->input->post('ppt_a'),
						'ppt_02'=>$this->input->post('ppt_b'),
						'ppt_03'=>$this->input->post('ppt_c'),
						'ppt_04'=>$this->input->post('ppt_d'),
						'ppt_05'=>$this->input->post('ppt_e'),
						'ppt_06'=>$this->input->post('ppt_f'),
						'ppt_07'=>$this->input->post('ppt_g'),
						'ppt_08'=>$this->input->post('ppt_h'),
						'ppt_09'=>$this->input->post('ppt_i'),
						'ppt_10'=>$this->input->post('ppt_j'),
						'ppt_11'=>$this->input->post('ppt_k'),
						'ppt_12'=>$this->input->post('ppt_l'),
						);
					$this->Dashboardmodel->updateppto($ppto,$this->input->post('idpp'));
					redirect('dashboard/ppto/');		
		}
		else{
	$data['ppto']=$this->Dashboardmodel->getPpto($_GET['cd']);	
	$this->load->view('dashboard/ppto/editppto',$data);
		}
	}
	
	function editarMonto()
	{
	$this->Dashboardmodel->editarMonto($_GET['id'],$_GET['campo']);	
	}
	
	function dias_tra()
	{
		$this->form_validation->set_rules('dias', 'Dias', '');
 
        if ($this->form_validation->run())
        {
			$info = array(
						'dit_dias'=>$this->input->post('dias'),
						'dit_sabados'=>$this->input->post('sabados'),
						);
					$this->Dashboardmodel->updateDiasTra($info,$this->input->post('iddt'));
			
			redirect('dashboard/ppto');
		}
		else{
	$data['info']=$this->Dashboardmodel->getDiasTra($_GET['iddt']);	
	$this->load->view('dashboard/ppto/edit_dias_tra',$data);
	}}
	
/*	public function datad()
	{
	
	$clientes=$this->Dashboardmodel->clientesTijuana();
	
	
	foreach($clientes as $cli){
		$info=array(
		'cus_name'=>$cli->nombre,
		'cus_email'=>$cli->email,
		'cus_telephone'=>$cli->tel,
		'cus_celular'=>$cli->celular,
		'cus_calle'=>$cli->calle,								
		'cus_num'=>$cli->num,								
		'cus_colonia'=>$cli->colonia,								
		'cus_cp'=>$cli->cp,								
		'cus_ciudad'=>$cli->ciudad,								
		'cus_estado'=>$cli->estado,
		'cus_rfc'=>$cli->rfc,								
		'cus_marca'=>$cli->marca,
		'cus_submarca'=>$cli->submarca,								
		'cus_version'=>$cli->version,								
		'cus_color'=>$cli->color,								
		'cus_modelo'=>$cli->modelo,								
		'cus_serie'=>$cli->serie,								
		'cus_capacidad'=>$cli->capacidad,								
		'cus_km_recepcion'=>$cli->kms,
		'cus_placas'=>$cli->placas,
		'cus_ultimo_servicio'=>$cli->ultimafecha,
		'cus_orden'=>$cli->orden,
		'cus_fecha_venta'=>$cli->fvta,
		'cus_cd'=>'TJ',																																																														
		);
		$this->Dashboardmodel->insertClientes($info);
		}
	}*/
	
	
}