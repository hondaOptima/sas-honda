<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/">
    <ul class="page-breadcrumb breadcrumb">
        <li> <i class="fa fa-book"></i> <a href="#">Status Inventario de Vehículos</a> <i class="fa fa-angle-right"></i></li>
        <li class="pull-right" style="margin-left:8px; margin-right:8px;">
            <select name="taller" onchange='this.form.submit()'>
                <option value="3" >Tijuana</option>
                <option  value="2" >Mexicali</option>
                <option value="1" >Ensenada</option>
            </select>
        </li>
    </ul>
</form>

<div class="row" style="background-color:white">
    <div class="col-md-4 col-sm-4">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-eye"></i>Lavado de Verificacion</div>
        </div>
        <div class="portlet-body" >
            <table class="table  table-hover" id="recibidos" style="text-align:center">
                <thead>
                    <tr style="font-weight:bold">
                        <td>Fecha</td>
                        <td>Torre</td>
                        <td>VIN</td>
                        <td>Auto</td>
                        <td>Dañado</td>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    </div>
    <div class="col-md-4 col-sm-4">
        <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-exclamation-triangle"></i>Vehículos Dañados
                </div>
            </div>
            <div class="portlet-body form">
                <table class="table table-hover" id="daniados" style="text-align:center">
                    <thead>
                        <tr style="font-weight:bold">
                            <td>Fotos</td>
                            <td>Torre</td>
                            <td>VIN</td>
                            <td>Auto</td>
                            <td>Carroceria</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
    </div>



  </div>


  <div class="col-md-4 col-sm-4">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->

      <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-exclamation-triangle"></i>Vehiculos En Carroceria
                </div>

            </div>
            <div class="portlet-body form"   >

        <table class="table table-hover" id="carroceria" style="text-align:center">
         <thead>
        <tr style="font-weight:bold">
        <td>Fotos</td>
        <td>Torre</td>
        <td>Entrega</td>
        <td>VIN</td>
        <td>Auto</td>
        <td>Reparado</td>
        </tr>
        </thead>
        <tbody></tbody>
        </table>
             </div>
    </div>



  </div>
   </div>


  <div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12">
  	<div class="portlet">
      <div class="portlet-title">
        <div class="caption"><i class="fa  fa-thumbs-up"></i>Vehículos con Previa</div>
      </div>
      <div class="portlet-body" >
<table class="table table-hover" id="sample_carroceria" style="text-align:center">
<thead>
        <tr style="font-weight:bold">
        <td>LLega a Piso</td>
        <td>VIN</td>
        <td>Auto</td>
        <td>Status</td>
		<td>No. Torre</td>
        <td>Ubicación</td>
        </tr>
        </thead>
        <tbody></tbody>
        </table>

 </div>
    </div>
	 </div>
    </div>


<div class="modal fade" id="wideCom" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Fecha de Salida de Carroceria:</h4>
      </div>
      <div class="modal-body">
     <input type="text" name="vinfecha" readonly="readonly">
        <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
												<input type="text" name="fechano" class="form-control" readonly>
												<span class="input-group-btn">
													<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>

<b>Comentarios</b>
<textarea name="comen" class="comen">
</textarea>

<div class="btn btn-primary btn-sm savefechano">Guardar Informaci&oacute;n</div>
<div class="loading"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

      </div>
    </div>

    <!-- /.modal-content -->
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('reportes/inventario/fotos_autos_daniados');?>

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>

	$(document).ready(function() {
    recibidos();
	daniados();
	carroceria();
	rampa_previa();


$('.savefechano').live('click',function(){
$('.loading').html('');
var vin = $('input[name=vinfecha]').val();
var fecha=$('input[name=fechano]').val();
var comen=$('.comen').val();
agregarFechaVin(vin,fecha,comen);
});


	$('.wide').live('click',function(){
		var vin=$(this).attr('id');
		$('#wide').modal('show');
		$('#MyUploadForm').addClass(vin);
		$('input[name=vin]').val('');
		$('input[name=vin]').val(vin);
		lista_fotos_daniados(vin);
		});

	$('.agregar_fecha').live('click',function(){
var vin=$(this).attr('id');
 $('input[name=vinfecha]').val(vin);
		$('#wideCom').modal('show');


		});

	$('.selectVIN').live('click',function(){
		var id=$(this).attr('id');

		$('#sample_2x_wrapper').hide();
		$('#forma').show();
		$('input[name="vin"]').val(id);


		});

	$('.dana').live('click',function(){
		var vin=$(this).attr('id');
		saveinfo(vin,'','',3);
		recibidos();
	daniados();
	carroceria();
	rampa_previa();
		});

	$('.carroceria').live('click',function(){
		var vin=$(this).attr('id');
		saveinfo(vin,'','',4);
		recibidos();
	daniados();
	carroceria();
	rampa_previa();
		});

	$('.enviar_pizarron').live('click',function(){
		var vin=$(this).attr('id');
		var torre=$(this).attr('title');
		var r = confirm("Enviar Unidad a Pizarron Electrónico?");
if (r == true) {

		//agregar auto pizarro y despues modificar el estatus del vehiculo a (status:5) es: previado
		$.post(
                  "<?= base_url()?>orden/agregar_inventario_pizarron",
                  { vin: vin,torre:torre},
                  function(data) {
					//actualizar datos del auto en inventario
					$.post("<?= base_url()?>ajax/inventario/inv_status_update.php",{ vin: vin,torre:torre,status:5},
                  function(data) {
		alert('Auto Transferido a Pizarrón Electrónico');
		//actualizar datos de las 3 tablas(verificacion, daniados,en carroceria)
		recibidos();
	    daniados();
	    carroceria();
	    rampa_previa();
				  } );


                  });
}

		});

	$('.nuevacarroceria').live('click',function(){
	var vin=$(this).attr('id');
	saveinfo(vin,'','',9);
	carroceria();
	rampa_previa();
		});

	$('.previado').live('click',function(){
	var vin=$(this).attr('id');
	saveinfo(vin,'','',8);
	carroceria();
	rampa_previa();
		});

	$('#sample_2x').dataTable( {

		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
						"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista.php",


	} );} );
	function recibidos(){
		$("#recibidos tbody").empty();
		$.getJSON("<?= base_url()?>ajax/inventario/autos_recibidos.php?status=2",
function (data) {
    var tr;

    for (var i = 0; i < data.length; i++) {
        if(data[i].api_danios==1){color='red';}else{color='blue;font-weight:bold';}
		if(data[i].api_ubicacion==99){color="rgba(205, 71, 70, 1);  font-weight:bold;";}
		tr = $('<tr/>');
        tr.append("<td ub="+data[i].api_ubicacion+" style='color:"+color+"'>" + data[i].api_fecha + "</td>");
         tr.append("<td style='color:"+color+"'>" + data[i].api_torre + "</td>");
        tr.append("<td class='enviar_pizarron' title='"+data[i].api_torre+"' id='" + data[i].api_vin + "'  style=' cursor:pointer; color:"+color+"'>" + data[i].api_vin + "</td>");
        tr.append("<td style='color:"+color+"'>" + data[i].api_modelo + "</td>");
		tr.append("<td title='Enviar a Vehículos Dañados' style='cursor:pointer; font-size:25px;' id='"+data[i].api_vin+"' class='dana'><i style='font-size:30px;color:red' class='fa fa-arrow-circle-o-right'></i></td>");
        $('#recibidos').append(tr);
    }
});
		}


		function daniados(){
		$("#daniados tbody").empty();
		$.getJSON("<?= base_url()?>ajax/inventario/autos_recibidos.php?status=3",
function (data) {
    var tr;
	var color="";
    for (var i = 0; i < data.length; i++) {
        if(data[i].api_danios==1){color='red';}else{color='';}
		tr = $('<tr/>');
		tr.append("<td class='wide' id='"+data[i].api_vin+"' style=' font-size:18px;cursor:pointer;color:"+color+";'><i style='font-size:25px;' class='fa fa-camera'></i></td>");
         tr.append("<td style='color:"+color+";font-weight:bold'>" + data[i].api_torre + "</td>");
		tr.append("<td class='enviar_pizarron' title='"+data[i].api_torre+"' id='" + data[i].api_vin + "' style='cursor:pointer;color:"+color+";font-weight:bold'>" + data[i].api_vin + "</td>");
         tr.append("<td style='color:"+color+";font-weight:bold' >" + data[i].api_modelo + "</td>");
		tr.append("<td title='Enviar A Carrocería' style='cursor:pointer;font-weight:bold' id='"+data[i].api_vin+"' class='carroceria'><i style='font-size:30px;color:red' class='fa fa-arrow-circle-o-right'></i></td>");
        $('#daniados').append(tr);
    }
});
		}

		function carroceria(){
		$("#carroceria tbody").empty();
		$.getJSON("<?= base_url()?>ajax/inventario/autos_recibidos.php?status=4",
function (data) {
    var tr;
	var color="red";
    for (var i = 0; i < data.length; i++) {

		tr = $('<tr/>');
		tr.append("<td class='wide' id='"+data[i].api_vin+"' style=' font-size:18px;cursor:pointer;color:"+color+"; font-weight:bold'><i style='font-size:25px;' class='fa fa-camera'></i></td>");
         tr.append("<td style='color:"+color+"; font-weight:bold'>" + data[i].api_torre + "</td>");
        tr.append("<td title='"+data[i].api_danios_desc+"' class='agregar_fecha' id='"+data[i].api_vin+"' style='cursor:pointer; color:"+color+"; font-weight:bold'>" + data[i].api_salida_carroceria + "</td>");
        tr.append("<td class='previado' id='"+data[i].api_vin+"' title='Enviar de Carroceria a Previado' style='color:"+color+";font-weight:bold; cursor:pointer' >" + data[i].api_vin + "</td>");
        tr.append("<td style='color:"+color+";font-weight:bold'>" + data[i].api_modelo + "</td>");

		if(data[i].api_status==4){

		tr.append("<td title='"+data[i].api_torre+"' style='cursor:pointer;font-weight:bold' id='"+data[i].api_vin+"' class='enviar_pizarron'><i style='font-size:30px;color:green' class='fa fa-arrow-circle-o-right'></i></td>");
		}
		else
		{

			tr.append("<td title='"+data[i].api_torre+"' style='cursor:pointer;font-weight:bold' id='"+data[i].api_vin+"' class='previado'><i style='font-size:30px;color:green' class='fa fa-arrow-circle-o-right'></i></td>");

			}


        $('#carroceria').append(tr);
    }
});
		}

		function rampa_previa(){
		$("#sample_carroceria tbody").empty();
		$.getJSON("<?= base_url()?>ajax/inventario/autos_recibidos.php?status=8",
function (data) {
    var tr;
	var color="green; font-weight:bold;";
	var estado="";
	var almacen='';
    for (var i = 0; i < data.length; i++) {

		tr = $('<tr/>');
		if(data[i].api_status=='9'){
		estado="Previado y en Carroceria";
		color="#f89406; font-weight:bold;";
		}else{
			estado="Previado";
			color="green; font-weight:bold;";
		}

		if(data[i].api_ubicacion==9){ almacen='Almacen 1';}
		if(data[i].api_ubicacion==10){ almacen='Almacen 2';}
		if(data[i].api_ubicacion==11){ almacen='Almacen 3';}
		if(data[i].api_ubicacion==99){
			almacen='Almacen 99';
			color = "rgba(205, 71, 70, 1);  font-weight:bold;";
			}

        tr.append("<td style='color:"+color+"'>" + data[i].api_fecha + "</td>");
        tr.append("<td class='nuevacarroceria' title='Enviar a Carroceria' id='" + data[i].api_vin + "' style='color:"+color+";cursor:pointer' >" + data[i].api_vin + "</td>");
        tr.append("<td style='color:"+color+"'>" + data[i].api_modelo + " " + data[i].api_color + "</td>");
		tr.append("<td style='color:"+color+" cursor:pointer'  >"+  estado +"</td>");
        tr.append("<td style='color:"+color+" cursor:pointer'  >"+  data[i].api_torre +"</td>");
		tr.append("<td style='color:"+color+" cursor:pointer'  >"+  almacen +"</td>");
        $('#sample_carroceria').append(tr);

    }
	var table=$('#sample_carroceria').DataTable();
	 table.destroy();
	$('#sample_carroceria').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });
});
		}


		function actualizarTabla(){
	$("#sample_2x").dataTable().fnDestroy();
	$('#sample_2x').dataTable( {


		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
						"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista.php",


	} );
			}

	function limpiarCampos(){
		var vin=$('input[name=vin]').val("");
		var des=$('input[name=desc]').val("");

		$('input[name=vinfecha]').val("");
		$('input[name=fechano]').val("");
		}
	function saveinfo(vin,dan,des,sta){
	$.post(
                  "<?= base_url()?>ajax/inventario/inv_update.php",
                  { vin: vin, dan:dan,des:des,status:sta},
                  function(data) {
					$('#sample_2x_wrapper').show();
		            $('#forma').hide();
					limpiarCampos();
					actualizarTabla();
                    recibidos();
                  }
               );
		}

	function agregarFechaVin(vin,fecha,comen){
	$.post(
                  "<?= base_url()?>ajax/inventario/inv_update_fecha.php",
                  { vin: vin, fecha:fecha,comen:comen},
                  function(data) {

					limpiarCampos();
					carroceria();

                  }
               );
		}
	</script>





    <script type="text/javascript">
$(document).ready(function() {
	var options = {
			target: '#output',   // target element(s) to be updated with server response
			beforeSubmit: beforeSubmit,  // pre-submit callback
			success: afterSuccess,  // post-submit callback
			resetForm: true  // reset the form after successful submit
		};

	 $('#MyUploadForm').submit(function() {
			$(this).ajaxSubmit(options);
			var vin=$(this).attr('class');

			lista_fotos_daniados(vin);

			// always return false to prevent standard browser submit and page navigation
			return false;
		});
});

function afterSuccess()
{



	//$('#submit-btn').show(); //hide submit button
	//$('#loading-img').hide(); //hide submit button

}
function lista_fotos_daniados(vin){
	$.post("<?= base_url()?>ajax/inventario/lista_fotos_daniados.php",
                  { vin: vin},
                  function(data) {
					  //alert(data);
					$('#lista_fotos_daniados').html(data);
                  }
               );
	}
//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{

		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Are you kidding me?");
			return false
		}

		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type


		//allow only valid image file types
		switch(ftype)
        {
            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }

		//Allowed file size is less than 1 MB (1048576)
		if(fsize>1048576)
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
			return false
		}

		//$('#submit-btn').hide(); //hide submit button
		//$('#loading-img').show(); //hide submit button
		$("#output").html("");
	}
	else
	{
		//Output error to older browsers that do not support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function confirmar(vin){
var answer = confirm ("Enviar a Pizarron Elctronico?")
if (answer)
 return true;
else
 return false;
	}

</script>
