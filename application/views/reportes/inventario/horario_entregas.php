<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-book"></i> 
  <a href="#">Horarios de Entrega de Vehículos</a> 
  <i class="fa fa-angle-right"></i> </li>       
</ul>



<div class="row" style="background-color:white">
  <div class="col-md-4 col-sm-4"> 
  <?php 
  $data['agencia']='Tijuana';
  $data['id']='tlv'; 
  $data['tipo']='Lunes a Viernes';
  $this->load->view('reportes/inventario/lista_horas.php',$data); ?> 
  </div>
  
   <div class="col-md-4 col-sm-4"> 
   <?php 
   $data['agencia']='Mexicali'; 
   $data['id']='mlv';
   $this->load->view('reportes/inventario/lista_horas.php',$data); ?>
   </div>
  
  
  <div class="col-md-4 col-sm-4"> 
   <?php 
   $data['agencia']='Ensenada'; 
   $data['id']='elv';
   $this->load->view('reportes/inventario/lista_horas.php',$data); ?>
   </div>      
 </div>
 
 
 
 <div class="row" style="background-color:white">
  <div class="col-md-4 col-sm-4"> 
  <?php 
  $data['agencia']='Tijuana'; 
  $data['tipo']='Sabado';
  $data['id']='tsa';
  $this->load->view('reportes/inventario/lista_horas.php',$data); ?> 
  </div>
  
   <div class="col-md-4 col-sm-4"> 
   <?php 
   $data['agencia']='Mexicali'; 
   $data['tipo']='Sabado';
  $data['id']='msa';
   $this->load->view('reportes/inventario/lista_horas.php',$data); ?>
   </div>
  
  
  <div class="col-md-4 col-sm-4"> 
   <?php 
   $data['agencia']='Ensenada'; 
   $data['tipo']='Sabado';
   $data['id']='esa';
   $this->load->view('reportes/inventario/lista_horas.php',$data); ?>
   </div>      
 </div>


<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>
var info = $.ajax({type: "GET", url: "<?= base_url()?>ajax/inventario/horario_calendarios.php", async: false}).responseText;

horas(info,'Tijuana','tlv','lv');
horas(info,'Mexicali','mlv','lv');
horas(info,'Ensenada','elv','lv');

horas(info,'Tijuana','tsa','s');
horas(info,'Mexicali','msa','s');
horas(info,'Ensenada','esa','s');


$('.eliminar').live('click',function(){
	
    $('.loading').html('Eliminando...');	
    var id=$(this).attr('id');
    var title=$(this).attr('title');

	$.post("<?= base_url()?>ajax/inventario/eliminar_hora.php",
          { id: id},function(data) {
			  $("#"+title+" tbody").empty();
			  info_a = $.ajax({type: "GET", url: "<?= base_url()?>ajax/inventario/horario_calendarios.php", async: false}).responseText;

horas(info_a,title);
			  $('.loading').html('');
			  
			 
	});
});

$('.agregar').live('click',function(){
	
    $('.loading').html('Guardando Infomacion...');	
    var cd=$(this).attr('id');
    var hora=$('select[name=ag'+cd+']').val();

	$.post("<?= base_url()?>ajax/inventario/agregar_hora.php",
          { agencia: cd, hora:hora},function(data) {
			  $("#"+cd+" tbody").empty();
			  info_a = $.ajax({type: "GET", url: "<?= base_url()?>ajax/inventario/horario_calendarios.php", async: false}).responseText;

horas(info_a,cd);
			  $('.loading').html('');
			  
			  
	});
});
	

function horas(info,cd,id,secc)
{ 
 $('.loading').html('Cargando...');
      
	  data=JSON.parse(info);
	  var clase='';
      for (var i = 0; i < data.length; i++) 
	  {
		  if(data[i].hen_agencia==cd)
		  {
			  if(data[i].hen_agencia=="<?= $_SESSION['sfworkshop']?>")
			  {
				  clase='fa-trash-o eliminar';
			  }
			  else
			  {
				  clase='fa-ban ';
			  }
			  if(data[i].hen_seccion==secc)
			  {

				 tr = $('<tr/>');
				 tr.append("<td >" + data[i].hen_hora + " "+tipo_hora(data[i].hen_hora)+"</td>");
				 tr.append("<td style='cursor:pointer' ><i style='font-size:24px;' class='fa "+clase+"' title='"+cd+"' id='"+data[i].hen_ID+"'></i></td>");				
				$('#'+id+'').append(tr);
			  }
		  }
      }
	  
$('.loading').html('');
 }

function tipo_hora(hora){
    var tipo='p.m.';
	var arr = hora.split(':');
	if(arr[0]<12)
	{
	tipo='a.m.';	
	}
	return tipo;
}

</script>