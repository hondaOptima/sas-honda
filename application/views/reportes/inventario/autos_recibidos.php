<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>

<form name="form1" method="get" action="<?php echo base_url();?>reportes/autos_recibidos/">
    <ul class="page-breadcrumb breadcrumb">
        <li><i class="fa fa-book"></i> <a href="#"></a> <i class="fa fa-angle-right"></i> </li>
        <li class="pull-right" style="margin-left:8px; margin-right:8px;">
        <select name="taller" onchange='this.form.submit()'>
        <option value="3">Tijuana</option>
        <option  value="2">Mexicali</option>
        <option value="1">Ensenada</option>
        </select>
        </li>
    </ul>
</form>

<div class="row" style="background-color:white">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa  fa-thumbs-up"></i>Inventario Fisico</div>
            </div>
            <div class="portlet-body" >
                <table class="table table-hover" id="sample_carroceria" style="text-align:center">
                    <thead>
                        <tr style="font-weight:bold">
                        <td>D</td>
                        <td>LLega a Piso</td>
                        <td>VIN</td>
                        <td>Auto</td>
                        <td>Status</td>
                        <td>No. Torre</td>
                        <td>Ubicación</td>
                        <td></td>
                        <td>Regresar</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="wide" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Cambiar Almacen, Status,Torre</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_vin">Vin</label>
                            <input type="text" class="form-control" id="txt_vin" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_agencia">Agencia</label>
                            <input type="text" class="form-control" id="txt_agencia" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>Seleciona un almacén:</p>
                        <label class="radio-inline">
                            <input type="radio" name="rad_almacen" value="9">Almacén 1
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="rad_almacen" value="10">Almacén 2
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="rad_almacen" value="11">Almacén 3
                        </label>
                    </div>
                    <div class="col-md-6">
                        <p>Seleciona un status:</p>
                        <label class="radio-inline">
                            <input type="radio" name="rad_status" value="8" data-label="Previado">Previado
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="rad_status" value="2" data-label="Lavado de Verificación">Lavado de Verificación
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary cambiar_status">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-torre" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambiar Torre</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_torre">Torre Actual</label>
                            <input type="text" class="form-control" id="txt_torre" readonly />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sel_torre">Torre Nueva</label>
                            <select class="form-control" id="sel_torre">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary actualiza-torre">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!--<link rel="stylesheet" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<script type="text/javascript" src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>-->

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>

<script type='text/javascript'>

$(document).ready(function() {
    torres();
    rampa_previa();

	$('.modal-torre').live('click',function() {
        data = $(this).closest("tr").get(0);
        vin = data.childNodes[2].innerHTML.trim();

        var t = $(this).html();

        $("#txt_torre").val(t);
        $.ajax({
            url: base_url + "get_torres",
            type : "GET",
            dataType: "json",
            //data : { agencia : "Tijuana" },
            data : { agencia : "<?= $taller ?>" },
            success: function(json) {
                $("#sel_torre").html("");
                $.each(json, function(i, item) {
                    var option = document.createElement("option");
                    option.value = item.id;
                    option.innerHTML = item.torre;
                    $("#sel_torre").append(option);
                });

                /*$.each($("#sel_torre option"), function(i, item) {
                    if(t != "0" && t == item.innerHTML) $("#sel_torre").select2("val", item.value);
                });*/
            }
        });
        $('#modal-torre').data('vin',vin);
        $('#modal-torre').data('torre',t);
        $('#modal-torre').modal('show');
	});

    $(".modalalmacen_trigger").live('click',function() {
        var data_id = $(this).attr("id");//aqui
        $("[data-id=alm_"+data_id+"]").trigger("click");
    });

	$('.modalalmacen').live('click',function() {
		var vin=$(this).attr('id');
        //var ubicacion = $(this).data("ubicacion");
        //aqui
        var ubicacion = $(this).get(0).getAttribute("data-ubicacion");
        //var s = $(this).data("status");
        var s = $(this).get(0).getAttribute("data-status");
        almacenes = document.getElementsByName("rad_almacen");
        for(j=0;j<almacenes.length;j++) if(almacenes[j].value == ubicacion) almacenes[j].checked = true;
        var status = document.getElementsByName("rad_status");
        for(j=0;j<status.length;j++) if(status[j].value == s) status[j].checked = true;
        $("#txt_vin").val(vin);
		var cd = $(this).attr('title');
        $("#txt_agencia").val(cd);
        var celda = $(this).get(0);
        $('#wide').data("status",celda.getAttribute("data-status"));
        $('#wide').data("ubicacion",celda.getAttribute("data-ubicacion"));
        $('#wide').data("vin",celda.getAttribute("data-vin"));
        $('#wide').data("agencia",celda.getAttribute("data-agencia"));
        $('#wide').data("almacen",celda.innerHTML);
        $('#wide').data("elemento",celda.getAttribute("data-id"));

        $('#wide').modal('show');
	});

    $('.cambiar_status').click(function(event)
    {
        event.preventDefault();
        $("button").prop("disabled",true);
        var vin = $("#txt_vin").val();
        var agencia = $("#txt_agencia").val();
        var almacen=$("input[name='rad_almacen']:checked").val();
        var status =$("input[name='rad_status']:checked").val();
        var status_descripcion =$("input[name='rad_status']:checked").attr("data-label");
        $.post(
            "<?= base_url()?>ajax/inventario/update_almacen.php",
            { vin: vin, almacen:almacen },
            function(data) {
                $.post(
                    "<?= base_url()?>ajax/inventario/update_cambiaesta.php",
                    { vin: vin,cambiaresta:status},
                    function(data) {

                    }
                );
            }
        ).done(function() {
            id = $("#wide").data("elemento");
            ele = $("td[data-id='" + id + "']" ).get(0);
            almac = parseInt(almacen);
            ele.setAttribute("data-ubicacion",almac);//aqui
            ele.setAttribute("data-status",status);

            if(almac == 9) ele.innerHTML = "Almacén 1";
            else if(almac == 10) ele.innerHTML = "Almacén 2";
            else if(almac == 11) ele.innerHTML = "Almacén 3";

            ele_s = $("[data-id=ubi_" + vin + "]").get(0);
            ele_s.innerHTML = status_descripcion;
            ele_s.setAttribute("data-status",status);

            if(status=="8") var color = "green"; else if(status == "2") var color = "blue";

            var tr = $("td[data-id='" + id + "']" ).closest("tr").get(0);
            for(i=0;i<tr.childNodes.length;i++) tr.childNodes[i].style.color = color;

            $('#wide').modal('toggle');
            $("button").prop("disabled",false);
            swal('¡Registro Actualizado!','Has actualizado el almacén y el status para el auto con VIN "' + vin + '".','success');
        });
	});

    $('.actualiza-torre').click(function(event)
    {
        event.preventDefault();
        vin = $("#modal-torre").data("vin");
        torre = $("#sel_torre option:selected").text();
        $("button").prop("disabled",true);
        $.ajax({
            url: base_url + "update_torre",
            type : "GET",
            dataType: "json",
            data : { agencia : "<?= $_SESSION["sfworkshop"] ?>", torre : torre, vin : vin },
            success: function(json) {
                if(json.error) swal("Se ha encontrado un error",json.error,'error');
                else
                {
                    el = document.getElementById("tor_" + vin);
                    el.innerHTML = torre;
                    swal("Se ha actualizado correctamente la torre \"" + torre + "\"",json.success,'success');
                }
                //$('#sample_carroceria').ajax.url(base_url+"get_autos_piso").load();//
            },
            complete: function() {
                $("button").prop("disabled",false);
                $('#modal-torre').modal('toggle');
            }
        });
	});

	$('.regresar').live('click',function() {
        //aqui
		var datos=$(this).attr('id');
		var arr = datos.split('&');
        console.log(arr);

        $.ajax({
            url: base_url + "get_traslado",
            type : "GET",
            dataType: "json",
            data : { vin : arr[0] },
            success: function(json) {
                console.log(json);
                if(json.error) swal('Error',json.error,'error');
                else
                {
                    if(json.status && (json.status == "0" || json.status == "1"))
                    {
                        swal({
                            title: '¡Auto en traslado!',
                            text: "¿Desea modificar este registro?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Actualizar',
                            cancelButtonText: 'Cancelar',
                            confirmButtonClass: 'btn btn-primary',
                            cancelButtonClass: 'btn btn-danger',
                            buttonsStyling: false
                        }).then(function () {
                            window.location.href = base_url + "reportes/crear_hoja_traslado/" + json.id;
                            console.log("Redireccionamiento...");
                        }, function (dismiss) {
                            // dismiss can be 'cancel', 'overlay',
                            // 'close', and 'timer'
                            if (dismiss === 'cancel') {
                                console.log("Aqui cancelamos");
                            }
                        });
                    }
                    else
                    {
                        var r = confirm("Liberar Torre: "+ arr[1] +"!");

                        if (r == true)
                		{
                            //aqui
                		    saveinfo(arr[0],'','',0,0,0,0,0);
                		    //window.location="<?= base_url()?>/reportes/autos_recibidos";
                        }
                    }
                }
            }
        });
	});

	function rampa_previa() {
		var cd="<?= $taller?>";
		$.getJSON("<?= base_url()?>ajax/inventario/autos_recibidos.php?status=1000&cd="+cd+"",
        function (data) {

            var tr;
        	var color="";
        	var estado="";
        	var almacen='';
        	var msn="";
            for (var i = 0; i < data.length; i++) {
		        tr = $('<tr/>');
	            if(data[i].api_status=='9') {
	                estado="Previado y en Carroceria";
	                color="red; font-weight:bold;";
	                msn='';
	            }
		        if(data[i].api_status=='2') {
        			estado="Lavado de Verificacion";
        			color="blue; font-weight:bold;";
        			msn='';
		        }

        		if(data[i].api_status=='3' || data[i].api_status=='4') {
        			estado="Carroceria";
        			color="red; font-weight:bold;";
        			msn='';
        		}

        		if(data[i].api_status=='5' || data[i].api_status=='6') {
        			estado="En Previa";
        			color="#f89406; font-weight:bold;";
        			msn='';
        		}

        		if(data[i].api_status=='8') {
        			estado="Previado";
        			color="green; font-weight:bold;";
        			msn='';
        		}

        		if(data[i].api_status=='10') {
        			estado="Previado";
        			color="black; font-weight:bold;";
        			msn='<br>Debes recibirlo en la seccion de traslados.';
        		}

        		if(data[i].api_ubicacion==9) almacen='Almacén 1';
        		else if(data[i].api_ubicacion==10) almacen='Almacén 2';
        		else if(data[i].api_ubicacion==11) almacen='Almacén 3';
        		else if(data[i].api_ubicacion==12) almacen='Almacén 4';
        		else if(data[i].api_ubicacion==13) almacen='Almacén 5';
        		else if(data[i].api_ubicacion==14) almacen='Almacén 6';
        		tr.append('<td style="color:'+color+'">D</td>');
                tr.append('<td style="color:'+color+'">' + data[i].api_fecha + '</td>');
                tr.append('<td id="' + data[i].api_vin + '" style="color:'+color+';cursor:pointer" >' + data[i].api_vin +' '+msn+'</td>');
                tr.append('<td style="color:'+color+'">' + data[i].api_modelo + ' ' + data[i].api_color + '</td>');
        		tr.append('<td data-status="' + data[i].api_status + '" data-ubicacion="' + data[i].api_ubicacion + '" data-id="ubi_' + data[i].api_vin + '" class="modalalmacen_trigger" id="'+data[i].api_vin+'" title="'+data[i].api_cd+'" style="color:'+color+' cursor:pointer"  >'+  estado +'</td>');
        	    tr.append('<td style="color:'+color+' cursor:pointer" class="modal-torre" id="tor_' + data[i].api_vin + '">'+  data[i].api_torre +'</td>');
        		//tr.append("<td data-status='" + data[i].api_status + "' data-ubicacion='"+ data[i].api_ubicacion +"' class='modalalmacen' data-id='alm_" + data[i].api_vin + "' id='"+data[i].api_vin+"' title='"+data[i].api_cd+"' style='color:"+color+" cursor:pointer'  >"+  almacen +"</td>");
        		tr.append('<td data-status="' + data[i].api_status + '" data-ubicacion="'+ data[i].api_ubicacion +'" class="modalalmacen" data-id="alm_'+data[i].api_vin+'" title="'+data[i].api_cd+'" style="color:'+color+' cursor:pointer" id="' + data[i].api_vin + '">'+ almacen +'</td>');
        		tr.append('<td style="cursor:pointer" style="color:'+color+'"  id="'+data[i].api_vin+'&'+data[i].api_torre+'" ></td>');
        		tr.append('<td style="cursor:pointer" style="color:'+color+'" id="'+data[i].api_vin+'&'+data[i].api_torre+'" class="regresar"><span class="badge badge-important"><i class="fa fa-arrow-circle-o-left"></i></span></td>');
                $('#sample_carroceria').append(tr);
            }

            table_carroceria = $('#sample_carroceria').DataTable({
                "bProcessing": true,
                "bDestroy": true,
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                }]
            });
        });
	}

    $('#wide').on('hidden.bs.modal', function () {
        $("input[type=text]").val("");
        $("input[type=radio]").prop("checked",false);
    });

    $("#sel_torre").select2();
});

function updateTorre(torre,nf) {
	var agencia="<?= $_SESSION['sfworkshop']?>";
    $.post(
        "<?= base_url()?>ajax/inventario/update_torre.php",
        { agencia:agencia ,torre:torre,nf:nf},
        function(data) {}
    );
}

function saveinfo(vin,dan,des,sta,anio,modelo,color,torre,status,almacen) {
    $.post(
        "<?= base_url()?>ajax/inventario/inv_update.php",
        { vin: vin, dan:dan,des:des,status:sta,anio:anio,modelo:modelo,color:color,torre:torre,statusb:status,almacen:almacen},
        function(data) {
            window.location="<?= base_url()?>/reportes/autos_recibidos";
        }
    );
}

function torres() {
    var agencia="<?= $_SESSION['sfworkshop']?>";
    $.getJSON('<?php echo base_url(); ?>ajax/inventario/torre.php?agencia='+agencia+'', function(data) {
        var html = '';
        var len = data.length;
        for (var i = 0; i< len; i++) {
            html += '<option value="' + data[i]+ '">' + data[i] + '</option>';
        }
        $('.torre')
        .find('option')
        .remove()
        .end();
        $('.torre').append(html);
    });
}
</script>

<script> var table_carroceria = "";</script>
