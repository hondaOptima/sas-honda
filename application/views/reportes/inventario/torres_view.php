<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-book"></i> <a href="#">Status Inventario de Vehículos</a> <i class="fa fa-angle-right"></i> </li>   
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->


  <style>
table tr td{ height:10%; font-size:30px; border:#ddd 3px solid; text-align:center; background-color:#f9f9f9; }
.amarillo{ background-color:#5DA423; color:white; font-weight:bold; text-align:center}
.green{ background-color:red; color:white; cursor:pointer;}
.texto{ font-size:65px;}
.texto2{ font-size:45px; border:1px solid #CCC; font-weight:bold; color:#5DA423}
</style>
  
  <div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12"> 
  	<div class="portlet">
      <div class="portlet-title">
        <div class="caption"><i class="fa  fa-thumbs-up"></i>Torres Ocupadas y Disponibles</div>
      </div>
 <div class="portlet-body" >
<div id="tabtorre"></div>
 </div>
    </div>
	 </div>
    </div>
    <!-- /.modal-content --> 
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>
    $(document).ready(function() {
    var agencia="<?= $agencia?>";		
 $.post( 
                 "<?= base_url()?>ajax/inventario/tablatorre.php",
                 {agencia:agencia},
                 function(data) 
                 {					  
					$('#tabtorre').html(data);	
               });
});                
	</script>