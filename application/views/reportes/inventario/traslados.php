<?php
    $this->load->view('globales/head');
    $this->load->view('globales/topbar');
    $data['menu'] = 'repor';
?>

<style>
    table.table-striped > tbody > tr > td:first-child{
        font-weight: bold !important;
        font-size: 12pt;
    }

    table.table-striped td:nth-child(6){
        font-weight: bold !important;
        font-size: 10pt;
    }

	.links a {
		padding-left: 10px;
		margin-left: 10px;
		border-left: 1px solid #000;
		text-decoration: none;
		color: #999;
	}

	.links a:first-child {
		padding-left: 0;
		margin-left: 0;
		border-left: none;
	}

	.links a:hover {text-decoration: underline;}

	.column-left {
		display: inline;
		float: left;
	}

    .column-right {
		display: inline;
		float: right;
	}

    td a {
        text-decoration: underline;
        color:#1E90FF;
    }

</style>

<form name="form1" method="get" action="<?= base_url();?>reportes/">
    <ul class="page-breadcrumb breadcrumb">
        <li> <i class="fa fa-book"></i> <a href="#">Traslado de Vehículos</a> <i class="fa fa-angle-right"></i> </li>
    </ul>
</form>


<div class="row" style="background-color:white">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa  fa-thumbs-up"></i>SALIDAS</div>
            </div>
            <div class="portlet-body" >
                <table class="table  table-hover table-striped" id="salidas" style="text-align:center">
                <thead>
                    <tr style="font-weight:bold">
                        <td>#</td>
                        <td>Origen</td>
                        <td>Destino</td>
                        <td>Trasladista</td>
                        <td>VIN</td>
                        <td>Auto</td>
                        <td>Salida</td>
                        <td>Salida Real</td>
                        <td>Asist. Gerencia</td>
                        <td>F&I </td>
                        <td>Logistica</td>
                        <td>Status</td>
                        <td>Imprimir</td>
                    </tr>
                </thead>
                <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row" style="background-color:white">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa  fa-thumbs-up"></i>LLEGADAS</div>
            </div>
            <div class="portlet-body" >
                <table class="table  table-hover table-striped" id="llegadas" style="text-align:center">
                    <thead>
                        <tr style="font-weight:bold">
                            <td>#</td>
                            <td>Origen</td>
                            <td>Destino</td>
                            <td>Trasladista</td>
                            <td>VIN</td>
                            <td>Auto</td>
                            <td>Salida</td>
                            <td>Salida Real</td>
                            <td>Asist. Gerencia</td>
                            <td>F&I </td>
                            <td>Logistica</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php if($_SESSION['sfworkshop'] != 'Mexicali') { ?>
<div class="row" style="background-color:white">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa  fa-thumbs-up"></i>TRASLADOS MEXICALI - ENSENADA</div>
            </div>
            <div class="portlet-body" >
                <table class="table  table-hover table-striped" id="llegadasmex" style="text-align:center">
                    <thead>
                        <tr style="font-weight:bold">
                            <td>#</td>
                            <td>Origen</td>
                            <td>Destino</td>
                            <td>Trasladista</td>
                            <td>VIN</td>
                            <td>Auto</td>
                            <td>Salida</td>
                            <td>Salida Real</td>
                            <td>Asist. Gerencia</td>
                            <td>F&I </td>
                            <td>Logistica</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

    <?php } ?>
  </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fecha Real Salida </h4>
            </div>
            <div class="modal-body">
                <form id="form_mymodal">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="txt_fecha_salida_real">Tipo</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_salida_real" placeholder="Fecha Real Salida" tabindex="1">
                              </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_guardar">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>
    $(document).ready(function()
	{
		salidas();
		llegadas();
		mexens();

        $(".btn-fecha_salida_real").live("click",function(e){
            $('#myModal').data("id",$(this).data("id"));
            $('#myModal').modal('show');
            e.preventDefault();
        });

        $("#btn_guardar").live("click",function(e){

            var fecha = $("#txt_fecha_salida_real").val();
            var id = $('#myModal').data("id");

            if(fecha != "" && id != "" && id != undefined)
            {
                $("button").prop("disabled",true);
                $.ajax({
                    url: base_url + "update_fecha_salida_real",
                    dataType : "json",
                    type : "POST",
                    data : { id : id, fecha_salida_real : fecha },
                    success: function(json,textStatus, jqXHR) {
                        if(json.error)
                            swal('Error',json.error,'error');
                        else if(json.success)
                        {
                            $("a[data-id='" + id + "']").text(json.fecha_nueva);
                            swal({
                                title: 'Actualizado',
                                text: json.success,
                                timer: 1000,
                                type : 'success'
                            }).then(
                                function () {},

                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                    }
                                }
                            );
                        }
                        else swal('Error','Error desconocido','error');

                    },
                    complete : function(jqXHR , textStatus) {
                        $("button").prop("disabled",false);
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                    },
                    statusCode: {
                        404: function() {
                            console.error( "page not found" );
                        },
                        500: function(){
                            console.log("Internal server error");
                        }
                    }
                });
            }
            else swal('Fecha vacía','Por favor seleccione una fecha para actualizar','error');

            $('#myModal').modal('hide');
            $("input").val("");
            e.preventDefault();
        });

        $("#txt_fecha_salida_real").datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            daysOfWeekDisabled: "0",
            todayBtn: true,
            autoclose : true
        });

	});

    function salidas()
    {
        var sta='';
        var ge='';
        var fi='';
        var se='';
        var ve='';
        var lo='';

        $.getJSON(base_url + "get_lista_traslados?tipo=salida",
            function (data)
            {

        		for (var i = 0; i < data.length; i++)
                {
            		if(data[i].tra_status=='0') {sta='proceso';     st='default';}
            		if(data[i].tra_status=='1')    {sta='En Traslado'; st='warning';}
            		if(data[i].tra_status=='2')    {sta='Recibido';	   st='success';}
            		if(data[i].tra_ventas=='0')    {ger='proceso';     sv='warning';} else { ger='aprobado'; sv='success';}
            		if(data[i].tra_fandi=='0')     {fan='proceso';     sf='warning';} else { fan='aprobado'; sf='success';}
            		if(data[i].tra_servicio=='0')  {ser='proceso';     ss='warning';} else { ser='aprobado'; ss='success';}
            		if(data[i].tra_age=='0')       {age='proceso';     sg='warning';} else { age='aprobado'; sg='success';}
            		if(data[i].tra_logistica=='0') {lgi='proceso';     sl='warning';} else { lgi='aprobado'; sl='success';}

            		tr = $('<tr/>');
                    tr.append("<td>" + (i+1) + "</td>");
                    tr.append("<td>" + data[i].tra_origen + "</td>");
                    tr.append("<td>" + data[i].tra_destino + "</td>");
                    tr.append("<td>" + data[i].tra_trasladista + "</td>");
                    tr.append('<td><a href="' + base_url + 'crear-hoja-traslado/' + data[i].tra_ID + '" target="_blank">' + data[i].tra_vin + '</a></td>');
                    tr.append("<td>" + data[i].tra_marca + " " + data[i].tra_modelo + " " + data[i].tra_anio + " " + data[i].tra_color + " </td>");
                    tr.append("<td>" + data[i].fecha_salida_formateada+"</td>");
                    tr.append("<td><a data-id='" + data[i].tra_ID + "' href='#' class='btn-fecha_salida_real'>" + ((data[i].mostrar_real == true) ? data[i].fecha_salida_real_formateada : "Actualizar") + "</a></td>");//aqui
                    tr.append("<td><span class='label label-sm label-"+sg+"'>" + age + "</span></td>");
            		tr.append("<td><span class='label label-sm label-"+sf+"'>" + fan + "</span></td>");
            		tr.append("<td><span class='label label-sm label-"+sl+"'>" + lgi + "</span></td>");
            		tr.append("<td><span class='label label-sm label-"+st+"'>" + sta + "</span></td>");
            		tr.append("<td><a style='font-size:24px;' target='_blank' href='http://hcrm.gpoptima.net/index.php/formatos/formato_de_traslado/"+data[i].tra_ID+"?autoriza=1'><i class='fa fa-paste'></i></a></td>");

                    $('#salidas').append(tr);
                }

                i_hhtml = $("#salidas").closest(".portlet").find("div.caption"); str = i_hhtml.html() + " - TOTAL: " +data.length; i_hhtml.html(str);
            }
        );
    }


    function llegadas()
    {
        var sta='';
        var ge='';
        var fi='';
        var se='';
        var ve='';
        var lo='';

        $.getJSON(base_url + "get_lista_traslados?tipo=entrada",
        function (data)
        {
            for (var i = 0; i < data.length; i++)
            {
                if(data[i].tra_status=='0')    {sta='proceso';     st='default';}
                if(data[i].tra_status=='1')    {sta='En Traslado'; st='warning';}
                if(data[i].tra_status=='2')    {sta='Recibido';	   st='success';}
                if(data[i].tra_ventas=='0')    {ger='proceso';     sv='warning';} else { ger='aprobado'; sv='success';}
                if(data[i].tra_fandi=='0')     {fan='proceso';     sf='warning';} else { fan='aprobado'; sf='success';}
                if(data[i].tra_servicio=='0')  {ser='proceso';     ss='warning';} else { ser='aprobado'; ss='success';}
                if(data[i].tra_age=='0')       {age='proceso';     sg='warning';} else { age='aprobado'; sg='success';}
                if(data[i].tra_logistica=='0') {lgi='proceso';     sl='warning';} else { lgi='aprobado'; sl='success';}

                tr = $('<tr/>');
                tr.append("<td >" + (i + 1) + "</td>");
                tr.append("<td >" + data[i].tra_origen + "</td>");
                tr.append("<td >" + data[i].tra_destino + "</td>");
                tr.append("<td >" + data[i].tra_trasladista + "</td>");
                tr.append('<td ><a href="' + base_url + 'recibir-hoja-traslado/' +data[i].tra_ID + '" target="_blank">' + data[i].tra_vin + '</a></td>');
                tr.append("<td >" + data[i].tra_marca + " " + data[i].tra_modelo + " " + data[i].tra_anio + " " + data[i].tra_color + " </td>");
                tr.append("<td >" + data[i].fecha_salida_formateada + "</td>");
                tr.append("<td ><a data-id='" + data[i].tra_ID + "' href='#' class='btn-fecha_salida_real'>" + ((data[i].mostrar_real == true) ? data[i].fecha_salida_real_formateada : "Actualizar") + "</a></td>");//aqui
                tr.append("<td ><span class='label label-sm label-"+sg+"'>" + age + "</span></td>");
                tr.append("<td ><span class='label label-sm label-"+sf+"'>" + fan + "</span></td>");
                tr.append("<td ><span class='label label-sm label-"+sl+"'>" + lgi + "</span></td>");
                tr.append("<td ><span class='label label-sm label-"+st+"'>" + sta + "</span></td>");

                $('#llegadas').append(tr);
            }

            i_hhtml = $("#llegadas").closest(".portlet").find("div.caption"); str = i_hhtml.html() + " - TOTAL: " +data.length; i_hhtml.html(str);

        });
    }

    function mexens()
    {
        var sta='';
        var ge='';
        var fi='';
        var se='';
        var ve='';
        var lo='';

        $.getJSON(base_url + "get_traslados_origen_y_destino",
            function (data)
            {
                for (var i = 0; i < data.length; i++)
                {
                    if(data[i].tra_status=='0')    {sta='proceso';     st='default';}
                    if(data[i].tra_status=='1')    {sta='En Traslado'; st='warning';}
                    if(data[i].tra_status=='2')    {sta='Recibido';	   st='success';}
                    if(data[i].tra_ventas=='0')    {ger='proceso';     sv='warning';} else { ger='aprobado'; sv='success';}
                    if(data[i].tra_fandi=='0')     {fan='proceso';     sf='warning';} else { fan='aprobado'; sf='success';}
                    if(data[i].tra_servicio=='0')  {ser='proceso';     ss='warning';} else { ser='aprobado'; ss='success';}
                    if(data[i].tra_age=='0')       {age='proceso';     sg='warning';} else { age='aprobado'; sg='success';}
                    if(data[i].tra_logistica=='0') {lgi='proceso';     sl='warning';} else { lgi='aprobado'; sl='success';}

                    tr = $('<tr/>');
                    tr.append("<td >" + (i + 1) + "</td>");
                    tr.append("<td >" + data[i].tra_origen + "</td>");
                    tr.append("<td >" + data[i].tra_destino + "</td>");
                    tr.append("<td >" + data[i].tra_trasladista + "</td>");
                    tr.append('<td ><a href="' + base_url + 'recibir-hoja-traslado/' + data[i].tra_ID + '" target="_blank">' + data[i].tra_vin + '</a></td>');
                    tr.append("<td >" + data[i].tra_marca + " " + data[i].tra_modelo + " " + data[i].tra_anio + " " + data[i].tra_color + " </td>");
                    tr.append("<td >" + data[i].tra_fecha_salida + " " + data[i].tra_hora_salida + "</td>");
                    tr.append("<td ><a data-id='" + data[i].tra_ID + "' href='#' class='btn-fecha_salida_real'>" + ((data[i].mostrar_real == true) ? data[i].fecha_salida_real_formateada : "Actualizar") + "</a></td>");//aqui
                    tr.append("<td ><span class='label label-sm label-"+sg+"'>" + age + "</span></td>");
                    tr.append("<td ><span class='label label-sm label-"+sf+"'>" + fan + "</span></td>");
                    tr.append("<td ><span class='label label-sm label-"+sl+"'>" + lgi + "</span></td>");
                    tr.append("<td ><span class='label label-sm label-"+st+"'>" + sta + "</span></td>");

                    $('#llegadasmex').append(tr);
                }

                i_hhtml = $("#llegadasmex").closest(".portlet").find("div.caption"); str = i_hhtml.html() + " - TOTAL: " +data.length; i_hhtml.html(str);

            }
        );
    }
</script>
