<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<ul class="page-breadcrumb breadcrumb">
					
</ul>
 
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Nueva Hoja de Traslado
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
                                        
                     					<?php echo  form_open_multipart('repo/crear','class="form-horizontal"'); ?>
                                        
<input type="hidden" name="idt" value="<?= $idt?>">
<input type="hidden" name="torre" >
<input type="hidden" name="agencia" value="<?= $_SESSION['sfworkshop']?>" >                                        
											<div class="form-body">
											
	<h3 class="form-section">Firma de Aprobacion</h3>   
    <div class="row">
<!--													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label col-md-3">
                                                            <div class="servicio"></div>
                                                            </label>
															<div class="col-md-9">
<input type="checkbox" name="f_servicio" id="servicio" class="firma"  disabled="disabled">	
															</div>
														</div>
													</div>-->
                                                    
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label col-md-3">
                                                            <div class="logistica"></div>
                                                            </label>
															<div class="col-md-9">
<input type="checkbox" name="f_logistica" id="logistica" class="firma" disabled="disabled">	
															</div>
														</div>
													</div>
                                                    
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label col-md-3">
                                                            
                                                            </label>
															<div class="col-md-9">
<div class="btn btn-success" onclick="enviarAuto(<?= $idt;?>)">Enviar Auto en Traslado</div>
															</div>
														</div>
													</div>
                                                    
                                                    
   </div>                                                    
                                             
                                            
                                            	<h3 class="form-section">Datos del Traslado</h3>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">De:</label>
															<div class="col-md-9">
<input type="text" name="origen" class="form-control" readonly="readonly">	
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Para:</label>
															<div class="col-md-9">
<input type="text" name="destino" class="form-control" readonly="readonly">
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Dia de Traslado:</label>
															<div class="col-md-9">
<input type="text" name="dia_traslado" class="form-control" readonly="readonly">
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
<label class="control-label col-md-3">Hora de Traslado:</label>
															<div class="col-md-9">
<input type="text" name="hora" class="form-control" readonly="readonly">
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">
                                                            Trasladista:</label>
															<div class="col-md-9">
<input type="text" name="trasladista" class="form-control" readonly="readonly">
																
															</div>
														</div>
													</div>
													<!--/span-->
													
												</div>
												<!--/row-->
												
												</div>
                                                
                                                
                                                
                                                
												<h3 class="form-section">Datos del Auto</h3>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">VIN:</label>
															<div class="col-md-9">
<input type="text" name="vin" class="form-control" readonly="readonly" readonly="readonly">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Marca:</label>
															<div class="col-md-9">
<input type="text" name="marca" class="form-control" readonly="readonly">
<input type="hidden" name="torre" class="form-control" readonly="readonly">																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Modelo:</label>
															<div class="col-md-9">
<input type="text" name="modelo" class="form-control" readonly="readonly">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Año:</label>
															<div class="col-md-9">
<input type="text" name="anio" class="form-control" readonly="readonly">
															</div>
														</div>
													</div>
												</div>
                                                
                                                <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Color:</label>
															<div class="col-md-9">
<input type="text" name="color" class="form-control" readonly="readonly">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">No. Motor:</label>
															<div class="col-md-9">
<input type="text" name="motor" class="form-control" readonly="readonly">
															</div>
														</div>
													</div>
												</div>
                                                
                                                
                                                <div class="row">
													<div class="col-md-6">
														<div class="form-group">
<label class="control-label col-md-3">Previa</label>
<div class="col-md-1"><div class="input-group">
<span class="input-group-addon"><input name="c_previa" class="c_previa" value="1" type="checkbox"></span>
</div></div>

<label class="control-label col-md-3">Herramientas</label>
<div class="col-md-1"><div class="input-group">
<span class="input-group-addon"><input name="c_herramientas" value="1" type="checkbox"></span>
</div></div>

<label class="control-label col-md-3">Manuales</label>
<div class="col-md-1"><div class="input-group">
<span class="input-group-addon"><input name="c_manuales" value="1" type="checkbox"></span>
</div></div>
<label class="control-label col-md-3">Tapetes</label>
<div class="col-md-1"><div class="input-group">
<span class="input-group-addon"><input name="c_tapetes" value="1" type="checkbox"></span>
</div></div>
<label class="control-label col-md-3">Desconectado</label>
<div class="col-md-1"><div class="input-group">
<span class="input-group-addon"><input name="c_desconectado" value="1" type="checkbox"></span>
</div></div>
<label class="control-label col-md-3">Gasolina</label>
<div class="col-md-1"><div class="input-group">
<span class="input-group-addon"><input name="c_gasolina" value="1" type="checkbox"></span>
</div></div>

<br><br><br>
<label class="control-label col-md-3">Observaciones:</label>
<div class="col-md-9"><div class="input-group">
<br>
<textarea cols="83" rows="7" class="observaciones" name="observaciones"></textarea>

</div></div>
														</div>
													</div>
													
                                                    <div class="col-md-6">
                                                    <label class="control-label col-md-3">Detalles:</label>
														<div class="form-group">
															
<canvas id="test" style=" width:452px; height:232px"></canvas>    
<div  onclick="limpiar()" class="btn btn-primary">Limpiar</div>                                                            
                                                            
														</div>
													</div>
												</div>
                                                
                                               
                                                
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
										<div class="btn btn-success" onclick="save()">Guardar Información</div>
															
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
                                            
										<?php echo form_close(); ?>
										<!-- END FORM-->
									</div>
								</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				
<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('reportes/js/script');?> 

<script type='text/javascript'>
var imagebg='';
get_hoja_traslado("<?= $idt?>");
get_personas_firma("<?= $_SESSION['sfworkshop']?>");
firma_aprobacion();


function save()
		{
			$("#test").data("jqScribble").save(function(imageData)
			{
				
				var form=$('.form-horizontal');
				var forma=form.serialize();		
				$('.savechasis').html('Guardando Imagen ...');
					$.post('<?= base_url()?>/ajax/inventario/image_save_detalle.php?'+forma+'', {imagedata: imageData}, function(response)
					{
						alert('Informacion Actulizada con Exito!');
						window.location="<?= base_url()?>reportes/traslados";
					});	
				
			});
		}
	
		
		function limpiar(){
			$("#test").jqScribble();
			$("#test").data("jqScribble").update({width:"450",height:"229",backgroundImage:"<?= base_url()?>images/autos_daniados/chasis.png",backgroundImageX:'1',backgroundImageY:'1'});
			}
		
function get_hoja_traslado(idt)
  { 
	  $.getJSON("<?= base_url()?>ajax/inventario/detalle_traslado.php?idt="+idt+"",
function (data)  {
		$('input[name=origen]').val(data[0].tra_origen);
		$('input[name=destino]').val(data[0].tra_destino);
		$('input[name=dia_traslado]').val(data[0].tra_fecha_salida);
		$('input[name=hora]').val(data[0].tra_hora_salida);
		$('input[name=trasladista]').val(data[0].tra_trasladista);
		$('input[name=vin]').val(data[0].tra_vin);
		$('input[name=marca]').val(data[0].tra_marca);
		$('input[name=modelo]').val(data[0].tra_modelo);
		$('input[name=anio]').val(data[0].tra_anio);
		$('input[name=color]').val(data[0].tra_color);
		$('input[name=motor]').val(data[0].tra_motor);
		$('.observaciones').val(data[0].tra_desc);
		if(data[0].dta_previa==1){$('input[name=c_previa]').prop( "checked", true );}
		if(data[0].dta_herramientas==1){$('input[name=c_herramientas]').prop( "checked", true );}
		if(data[0].dta_manuales==1){$('input[name=c_manuales]').prop( "checked", true );}
		if(data[0].dta_tapetes==1){$('input[name=c_tapetes]').prop( "checked", true );}
		if(data[0].dta_desconectado==1){$('input[name=c_desconectado]').prop( "checked", true );}
		if(data[0].dta_gasolina==1){$('input[name=c_gasolina]').prop( "checked", true );}
		if(data[0].tra_servicio==1){$('input[name=f_servicio]').prop( "checked", true );}
		if(data[0].tra_logistica==1){$('input[name=f_logistica]').prop( "checked", true );}		
	
		
		imagebg=""+data[0].tra_imagen+"";
		
		
		$("#test").jqScribble();
		$("#test").data("jqScribble").update({width:"450",height:"229",backgroundImage:"<?= base_url()?>images/autos_daniados/"+imagebg+"",backgroundImageX:'0',backgroundImageY:'0'});
		
		torreActual(data[0].tra_vin);
		
        });
  }	


function get_personas_firma(cd)
  { 
	  $.getJSON("<?= base_url()?>ajax/inventario/get_personas_aprobacion.php?cd="+cd+"",
function (data) 
        {
			 for (var i = 0; i < data.length; i++) 
			 {
if(data[i].atr_puesto=='GS')
                 {
    $('.servicio').html(data[i].hus_nombre+" "+data[i].hus_apellido);
    if(data[i].hus_correo=='<?= $_SESSION['sfemail']?>') $('input[name=f_servicio]').removeAttr('disable');
	              }
if(data[i].atr_puesto=='LO')
                  { 
    $('.logistica').html(data[i].hus_nombre+" "+data[i].hus_apellido);
	
	
	if(data[i].hus_correo=="<?= $_SESSION['sfemail']?>"){$('input[name=f_logistica]').prop("disabled", false);}
                  }
			 }
		});
		
		
  }
   function firma_aprobacion()
      {
		  
		  $('.firma').live('click',function()
		  { 
		    var columna=$(this).attr('id');
		    var mcCbxCheck = $(this);
			if(mcCbxCheck.is(':checked')) 
			{
			//actualizar firmar
			var r = confirm("Confirmar orden de Traslado ?"); 
            if (r == true) var res=firma(<?= $idt?>,columna,1); else $(this).prop('checked',false);
			}
			else
			{
			//actualizar desfirmar
			var r = confirm("Des aprobar orden de Traslado ?"); 
            if (r == true) firma(<?= $idt?>,columna,0); else $(this).prop('checked',true);
			}  
			  
		  });
	  
	  
	  }
	  
   function firma(idt,columna,tipo)
   {
	 $.post('<?= base_url()?>/ajax/inventario/firma_traslado.php', {idt:idt,columna:columna,tipo:tipo}, function(response){});	  
   }	
   
   function enviarAuto(idt)
   {
	 //1)verificar firmas ycambiar estatus traslado
	 //2)cambiar estatus y torre auto en inventario y en autos en piso
	 //3)liberar torre
	 
	 $.post('<?= base_url()?>/ajax/inventario/firma_verificar.php', {idt:idt}, function(response){
	 if(response!=0)
	     {//traslado aprobado
		alert('aqui');
		cambiarStatusAuto(); 
	     }
	else
	    {
		alert('Para Enviar es necesario contar con todas las firmas de aprobacion');	
		}	 
	});
	 	  
   }	  
 
  function cambiarStatusAuto()
  {
	    var vin=$('input[name=vin]').val();
		var torre=$('input[name=torre]').val();
		var agencia=$('input[name=agencia]').val();			  
		$.post("<?= base_url()?>ajax/inventario/update_torre_traslado.php",{ torre: torre,agencia,agencia},function(data) {
			
		$.post("<?= base_url()?>ajax/inventario/update_auto_traslado.php",{ vin: vin},function(data){});	
			
			});	
			   
			      	 
  }
  
  function torreActual(vin)
  {
	  $.getJSON("<?= base_url()?>ajax/inventario/get_datos_vin.php?vin="+vin+"",
function (data)  {
		  $('input[name=torre]').val(data[0].api_torre);
		  });
  }
		
</script>
