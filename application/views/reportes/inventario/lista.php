<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/">
    <ul class="page-breadcrumb breadcrumb">
        <li><i class="fa fa-table"></i> <a href="#">Vehículos Comprados</a> <i class="fa fa-angle-right"></i></li>
    </ul>
</form>

<div class="row" style="background-color:white">
    <div class="col-md-6 col-sm-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-truck"></i>Lista de Vehículos en Embarque</div>
            </div>
            <div class="portlet-body">
                <table align="center" class="table table-striped table-bordered table-hover" id="sample_2x">
			        <thead>
				        <tr style="font-weight:bold">
				            <th>
				                <b>Check</b>
				            </th>
				            <th>
				                <b># VIN</b>
				            </th>
                            <th>
				                <b>ALMACEN</b>
				            </th>
				            <th>
				                <b>MODELO</b>
				            </th>
				            <th>
				                <b>AÑO</b>
				            </th>
				            <th>
				                <b>COLOR</b>
				            </th>
				        </tr>
			        </thead>
			        <tbody>
                    </tbody>
                </table>

                <table id="forma" style=" display:none">
                    <tr>
                        <td>VIN</td>
                        <td><input type="text" name="vin" id="vin" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td>Año</td><td><input type="text" name="anio" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td>Modelo</td><td><input type="text" name="modelo" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td>Color</td><td><input type="text" name="color" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td>Motor</td><td><input type="text" name="motor" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td>Daños</td><td><Input type="radio" name="danios" value="1"> SI <input type="radio" name="danios" value="0" checked>NO</td>
                    </tr>
                    <tr>
                        <td>Torre</td>
                        <td>
                            <select name="torre" class="torre">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <tr>
                            <td>Status</td>
                            <td>
                                <select name="statusb" class="statusb">
                                    <option value="2">Lavado de Verificacion</option>
     <!-- <option value="4">Carroceria</option>
     <option value="9">Carroceria y Previado</option>
     -->
                                    <option value="8">Previado</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Almacen</td>
                            <td>
                            <?php if($_SESSION['sfworkshop'] == "Tijuana"){ ?>
                                <select name="almacen" class="almacen">
                                    <option value="9">1</option>
                                    <option value="10">2</option>
                                    <option value="11">3</option>
                                    <option value="12">4</option>
                                    <option value="13">5</option>
                                    <option value="14">6</option>
                                </select>
                                   <?php }else{ ?>
                                <select name="almacen" class="almacen">
                                    <option value="9">1</option>
                                    <option value="10">2</option>
                                    <option value="11">3</option>
                                    <option value="12">4</option>
                                </select>
                                <?php } ?>
	                        </td>
                        </tr>
                        <tr>
                            <td>Descripcion</td>
                            <td><input type="text" name="desc"></td>
                        </tr>
                        <tr>
                            <td></td><td><input type="submit" class="saveinfo" value="Siguiente"></td>
                        </tr>
                    </table>
                    <br><br><br>
                </div>

                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-truck"></i>Lista de Vehículos en Embarque y Facturados</div>
                </div>
                <div class="portlet-body">
                    <table align="center" class="table table-striped table-bordered table-hover" id="sample_3x">
			            <thead>
				            <tr style="font-weight:bold">
				                <th>
				                    <b>Check</b>
				                </th>
				                <th>
				                    <b># VIN</b>
				                </th>
                                <th>
				                    <b>ALMACEN</b>
				                </th>
				                <th>
				                    <b>MODELO</b>
				                </th>
				                <th>
				                    <b>AÑO</b>
				                </th>
				                <th>
				                    <b>COLOR</b>
				                </th>
				            </tr>
			            </thead>
			            <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6">
            <div class="portlet" >
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-home"></i>Vehiculos Recibidos
                    </div>
                </div>
                <div class="portlet-body form">
                    <table class="table" id="recibidos" style="text-align:center">
                        <thead>
                            <tr>
                                <td>Fecha</td>
                                <td>VIN</td>
                                <td>Auto</td>
                                <td>Daños?</td>
                                <td>Torre</td>
                                <td>Regresar</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>
$(document).ready(function() {
    recibidos();
	torres();
	var vinGlobal = "";

	$('#sample_3x').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
		"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista_facturados.php",
	});

    $('.selectVIN').live('click',function() {
        var string = $(this).attr('id');
        arr = string.split('&');
        id = arr[0];
        anio = arr[1];
        modelo = arr[2];
        color = arr[3];
        motor = arr[4];

        vinGlobal = id;
        $('#sample_2x_wrapper').hide();
        $('#forma').show();
        $('input[name="vin"]').val(id);
        $('input[name="anio"]').val(anio);
        $('input[name="modelo"]').val(modelo);
        $('input[name="motor"]').val(motor);
        $('input[name="color"]').val(color);
        torres();
    });

    $('.regresar').live('click',function() {
        var datos=$(this).attr('id');
        var arr = datos.split('&');
        saveinfo(arr[0],'','',0,0,0,0,0);//aqui
    });

    $('.saveinfo').live('click',function() {
        //Checar si está en traslado.
        var vin = $('input[name=vin]').val();
        $.ajax({
            url: base_url + "get_traslado",
            type : "GET",
            dataType: "json",
            data : { vin : vin },
            success: function(json) {
                if(json.error) swal('Error',json.error,'error');
                else
                {
                    if(json.status && (json.status == "0" || json.status == "1"))
                    {
                        /*swal({
                            title: '¡Auto en traslado!',
                            text: "¿Desea modificar este registro?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Actualizar'
                        }).then(function () {
                            window.location.replace(base_url + "reportes/crear_hoja_traslado/" + json.id);
                        });*/

                        swal({
                            title: '¡Auto en traslado!',
                            text: "¿Desea modificar este registro?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Actualizar',
                            cancelButtonText: 'Cancelar',
                            confirmButtonClass: 'btn btn-primary',
                            cancelButtonClass: 'btn btn-danger',
                            buttonsStyling: false
                        }).then(function () {
                            window.location.href = base_url + "reportes/crear_hoja_traslado/" + json.id;
                        }, function (dismiss) {
                            // dismiss can be 'cancel', 'overlay',
                            // 'close', and 'timer'
                            if (dismiss === 'cancel') {
                                $('#sample_2x_wrapper').show();
                                $('#forma').hide();
                            }
                        });
                    }
                    else
                    {
                        var dan=$('input[name=danios]:checked').val();
                        var des=$('input[name=desc]').val();
                        var ani=$('input[name=anio]').val();
                        var mod=$('input[name=modelo]').val();
                        var col=$('input[name=color]').val();
                        var mot=$('input[name=motor]').val();
                        var tor=$('select.torre').val();
                        var sta=$('select.statusb').val();
                        var alma=$('select.almacen').val();
                        var stab=0;
                        saveinfo(vin,dan,des,sta,anio,mod,col,tor,stab,alma,mot);
                        torres();
                    }
                }
            }
        });
    });

    var table_sample_2x = $('#sample_2x').DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bRetrieve": true,
        "bDestroy": true,
        "sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista.php",
    });
});

function recibidos()
{
    $("#recibidos tbody").empty();
    $.getJSON("<?= base_url()?>ajax/inventario/autos_recibidos.php?status=2",
        function(data)
        {
            var tr;
            var color="";
            var x="";
            for (var i = 0; i < data.length; i++) {
                tr = $('<tr/>');
                if(data[i].api_danios_desc!=''){x="X";}else{x="0";}
                if(data[i].api_danios==1){color="red";}else{color="";}
                tr.append("<td style='color:"+color+"'>" + data[i].api_fecha + "</td>");
                tr.append("<td style='color:"+color+"'>" + data[i].api_vin + "</td>");
                tr.append("<td style='color:"+color+"'>" + data[i].api_modelo + "</td>");
                tr.append("<td style='color:"+color+"' title='" + data[i].api_danios_desc + "' >"+ x +"</td>");
                tr.append("<td style='color:"+color+" cursor:pointer'>" + data[i].api_torre + "</td>");
                tr.append("<td style='cursor:pointer' style='color:"+color+"' id='"+data[i].api_vin+"&"+data[i].api_torre+"' class='regresar'><span class='badge badge-important'><i class='fa fa-arrow-circle-o-left'></i></span></td>");
                $('#recibidos').append(tr);
            }
        }
    );
}

function actualizarTabla()
{
    $("#sample_2x").dataTable().fnDestroy();
    $('#sample_2x').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bRetrieve": true,
        "bDestroy": true,
        "sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista.php",
    });
}

function limpiarCampos()
{
    var vin=$('input[name=vin]').val("");
    var des=$('input[name=desc]').val("");
}

function saveinfo(vin,dan,des,sta,anio,modelo,color,torre,status,almacen,motor)
{
    console.log(vin);
    $.ajax({
        //url: base_url + "get_auto_piso",
        url: base_url + "get_traslado",
        type : "GET",
        dataType: "json",
        data : { vin : vin },
        success: function(json) {
            if(json.status && (json.status == "0" || json.status == "1"))
            {
                swal({
                    title: '¡Auto en traslado!',
                    text: "¿Desea modificar este registro?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Actualizar',
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function () {
                    window.location.href = base_url + "reportes/crear_hoja_traslado/" + json.id;
                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        console.log("Cancelada");
                    }
                });
            }
            else
            {
                //aqui
                $.post(
                    "<?= base_url()?>ajax/inventario/inv_update.php",
                    { vin: vin, dan:dan,des:des,status:sta,anio:anio,modelo:modelo,color:color,torre:torre,statusb:status,almacen:almacen,motor:motor},
                    function(data) {
                        if(data==1000)
                        {
                            //actualizar torre en crm en la tabla inventario
                            $.post(
                            "<?= base_url()?>ajax/inventario/inv_update_torre.php",
                            { vin: vin,torre:torre,ubicacion:almacen},
                            function(data)
                            {
                                $('#sample_2x_wrapper').show();
                                $('#forma').hide();
                                limpiarCampos();
                                actualizarTabla();
                                recibidos();
                            });
                        }
                        else console.log("La variable data fue diferente de 1000");
                    }
                );
            }
        }
    });
}

function torres()
{
    var agencia="<?= $_SESSION['sfworkshop']?>";
        $.getJSON('<?php echo base_url(); ?>ajax/inventario/torre.php?agencia='+agencia+'', function(data){
        var html = '';
        var len = data.length;
        for (var i = 0; i< len; i++) {
            html += '<option value="' + data[i]+ '">' + data[i] + '</option>';
        }
        $('.torre')
        .find('option')
        .remove()
        .end();
        $('.torre').append(html);
    });
}
</script>
