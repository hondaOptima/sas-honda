<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
</ul>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
<div class="portlet-title">
  <div class="caption"> <i class="fa fa-reorder"></i>Recibir Hoja Traslado </div>
</div>
<div class="portlet-body form"> 
  <!-- BEGIN FORM--> 
  
  <?php echo  form_open_multipart('','class="form-horizontal"'); ?>
  <input type="hidden" name="idt" value="<?= $idt?>">
  <div class="form-body">
    <h3 class="form-section">Agregar a un Almacen</h3>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-3">VIN:</label>
          <div class="col-md-9">
            <input type="text" name="vin" class="form-control" readonly="readonly">
          </div>
        </div>
      </div>
      <!--/span-->
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-3">Auto:</label>
          <div class="col-md-9">
            <input type="text" name="auto" class="form-control" readonly="readonly">
          </div>
        </div>
      </div>
    </div>
    <!--/row-->
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-3">Status:</label>
          <div class="col-md-9">
            <select name="status" class="form-control status" >
            <option value="8">Previado</option>
            <option value="2">Lavado de Verificacion</option>            
            </select>
          </div>
        </div>
      </div>
      <!--/span-->
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-3">Torre:</label>
          <div class="col-md-9">
            <select class="torre" name="hora" class="form-control">
            </select>
          </div>
        </div>
      </div>
    </div>
    <!--/row-->
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-3"> Almacen:</label>
          <div class="col-md-9">
            <select name="almacen" class="form-control ubicacion" >
            <option value="9">Almacen 1</option>
            <option value="10">Almacen 2</option> 
            <option value="11">Almacen 3</option>           
            </select>
          </div>
        </div>
      </div>
      <!--/span--> 
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-3"> </label>
          <div class="col-md-9">
<div class="btn btn-success" onclick="updateAutoTraslado()">Agregar a Inventario</div>
          </div>
        </div>
      </div>
    </div>
    <!--/row-->
    
    <h3 class="form-section">Hoja de Traslado</h3>
    <!--/row-->
    <div class="row">
    <div class="col-md-12">
      <iframe width="100%" id="pdf" height="400px;" src="http://hcrm.gpoptima.net/index.php/formatos/formato_de_traslado/<?= $idt?>/?autoriza=1" frameborder="0">
      </iframe>
      </div>
    </div>
    <div class="form-actions fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="col-md-offset-3 col-md-9"> </div>
        </div>
        <div class="col-md-6"> </div>
      </div>
    </div>
    <?php echo form_close(); ?> 
    <!-- END FORM--> 
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->

<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>
torres();

var imagebg='';
get_hoja_traslado("<?= $idt?>");
get_personas_firma("<?= $_SESSION['sfworkshop']?>");
firma_aprobacion();



function save()
		{
			$("#test").data("jqScribble").save(function(imageData)
			{
				
				var form=$('.form-horizontal');
				var forma=form.serialize();		
				$('.savechasis').html('Guardando Imagen ...');
					$.post('<?= base_url()?>/ajax/inventario/image_save_detalle.php?'+forma+'', {imagedata: imageData}, function(response)
					{
						$('.savechasis').html('Imagen Guardada !');
					});	
				
			});
		}
	
		
		function limpiar(){
			$("#test").jqScribble();
			$("#test").data("jqScribble").update({width:"450",height:"229",backgroundImage:"<?= base_url()?>images/autos_daniados/chasis.png",backgroundImageX:'1',backgroundImageY:'1'});
			}
		
function get_hoja_traslado(idt)
  { 
	  $.getJSON("<?= base_url()?>ajax/inventario/detalle_traslado.php?idt="+idt+"",
function (data)  {
	
	
	    $('input[name=auto]').val(data[0].tra_marca+" "+data[0].tra_modelo+" "+data[0].tra_anio+" "+data[0].tra_color);
		$('input[name=origen]').val(data[0].tra_origen);
		$('input[name=destino]').val(data[0].tra_destino);
		$('input[name=dia_traslado]').val(data[0].tra_fecha_salida);
		$('input[name=hora]').val(data[0].tra_hora_salida);
		$('input[name=trasladista]').val(data[0].tra_trasladista);
		$('input[name=vin]').val(data[0].tra_vin);
		$('input[name=marca]').val(data[0].tra_marca);
		$('input[name=modelo]').val(data[0].tra_modelo);
		$('input[name=anio]').val(data[0].tra_anio);
		$('input[name=color]').val(data[0].tra_color);
		$('input[name=motor]').val(data[0].tra_motor);
		$('.observaciones').val(data[0].tra_desc);
		if(data[0].dta_previa==1){$('input[name=c_previa]').prop( "checked", true );}
		if(data[0].dta_herramientas==1){$('input[name=c_herramientas]').prop( "checked", true );}
		if(data[0].dta_manuales==1){$('input[name=c_manuales]').prop( "checked", true );}
		if(data[0].dta_tapetes==1){$('input[name=c_tapetes]').prop( "checked", true );}
		if(data[0].dta_desconectado==1){$('input[name=c_desconectado]').prop( "checked", true );}
		if(data[0].dta_gasolina==1){$('input[name=c_gasolina]').prop( "checked", true );}
		if(data[0].tra_servicio==1){$('input[name=f_servicio]').prop( "checked", true );}
		if(data[0].tra_logistica==1){$('input[name=f_logistica]').prop( "checked", true );}		
	
		
		imagebg=""+data[0].tra_imagen+"";
		
		
		$("#test").jqScribble();
		$("#test").data("jqScribble").update({width:"450",height:"229",backgroundImage:"<?= base_url()?>images/autos_daniados/"+imagebg+"",backgroundImageX:'0',backgroundImageY:'0'});
		
        });
  }	


function get_personas_firma(cd)
  { 
	  $.getJSON("<?= base_url()?>ajax/inventario/get_personas_aprobacion.php?cd="+cd+"",
function (data) 
        {
			 for (var i = 0; i < data.length; i++) 
			 {
if(data[i].atr_puesto=='GS')
                 {
    $('.servicio').html(data[i].hus_nombre+" "+data[i].hus_apellido);
    if(data[i].hus_correo=='<?= $_SESSION['sfemail']?>') $('input[name=f_servicio]').removeAttr('disable');
	              }
if(data[i].atr_puesto=='LO')
                  { 
    $('.logistica').html(data[i].hus_nombre+" "+data[i].hus_apellido);
	if(data[i].hus_correo=="<?= $_SESSION['sfemail']?>"){$('input[name=f_logistica]').prop("disabled", false);}
                  }
			 }
		});
		
		
  }
   function firma_aprobacion()
      {
		  
		  $('.firma').live('click',function()
		  { 
		    var columna=$(this).attr('id');
		    var mcCbxCheck = $(this);
			if(mcCbxCheck.is(':checked')) 
			{
			//actualizar firmar
			var r = confirm("Confirmar orden de Traslado ?"); 
            if (r == true) var res=firma(<?= $idt?>,columna,1); else $(this).prop('checked',false);
			}
			else
			{
			//actualizar desfirmar
			var r = confirm("Des aprobar orden de Traslado ?"); 
            if (r == true) firma(<?= $idt?>,columna,0); else $(this).prop('checked',true);
			}  
			  
		  });
	  
	  
	  }
	  
   function firma(idt,columna,tipo)
   {
	 $.post('<?= base_url()?>/ajax/inventario/firma_traslado.php', {idt:idt,columna:columna,tipo:tipo}, function(response){});	  
   }	
   
   function enviarAuto(idt)
   {
	 //1)verificar firmas
	 //2)cambiar estatus traslado
	 //3)cambiar estatus y torre auto en inventario y en autos en piso
	 //4)liberar torre
	 
	 $.post('<?= base_url()?>/ajax/inventario/firma_verificar.php', {idt:idt}, function(response){
	 if(response!=0)
	     {//traslado aprobado
		 alert('Transferido');
		 
	     }
	else
	    {
		alert('Para Enviar es necesario contar con todas las firmas de aprobacion');	
		}	 
	});
	 	  
   }	  
 
  function torres(){
	var agencia="<?= $_SESSION['sfworkshop']?>";
	$.getJSON('<?php echo base_url(); ?>ajax/inventario/torre.php?agencia='+agencia+'', function(data){
    var html = '';
    var len = data.length;
    for (var i = 0; i< len; i++) {
        html += '<option value="' + data[i]+ '">' + data[i] + '</option>';
    }
	$('.torre')
	.find('option')
    .remove()
    .end();
	$('.torre').append(html);
	});	
		}
		
	function updateAutoTraslado()
	{
		var vin=$('input[name=vin]').val();
		var sta=$('.status').val();
		var tor=$('.torre').val();
		var age="<?= $_SESSION['sfworkshop']?>";
		var nf='off';
		var ubi=$('.ubicacion').val();
		
		$.post('<?= base_url()?>/ajax/inventario/update_traslado_recibir.php', 
		{vin:vin,status:sta,torre:tor,age:age,ubi:ubi}, function(response){
	
	 window.location="<?= base_url();?>reportes/traslados/";
	});
	
	$.post('<?= base_url()?>/ajax/inventario/update_torre.php', {torre:tor,agencia:age,nf:nf}, function(response){
	
	
	});
		
	}	
		
</script> 
