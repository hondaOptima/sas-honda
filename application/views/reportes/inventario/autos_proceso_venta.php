<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-book"></i> <a href="#"></a> <i class="fa fa-angle-right"></i> </li>
  
  
  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" >Tijuana</option>
    <option  value="2" >Mexicali</option>
    <option value="1" >Ensenada</option>        
    </select>
  </li>
  
        
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->



  <div class="row" style="background-color:white">
<div class="col-md-12 col-sm-12"> 
  	<div class="portlet">
      <div class="portlet-title">
        <div class="caption"><i class="fa  fa-thumbs-up"></i>Vehículos Recibidos</div>
      </div>
      <div class="portlet-body" >
<table class="table table-hover" id="autos_proceso" style="text-align:center">
<thead>
        <tr style="font-weight:bold">
        <td>Ciudad</td>
        <td>Mes</td>
        <td>Contacto</td>
        <td>Cliente</td>
		<td>Asesor</td>
        <td>Auto</td>
        <td>VIN</td>
        <td>F&I </td>
        <td>Gerente</td>
        <td>Facturacion</td>
        <td>Asesor</td>
        <td>Contabilidad</td>
        </tr>
        </thead>
        <tbody></tbody>
        </table>
 </div>
    </div>
	 </div>
    </div>



<!-- END EXAMPLE TABLE PORTLT-->

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script type='text/javascript'>

	$(document).ready(function() {
    proceso_de_venta();
	
	});

function proceso_de_venta()
       {
	var cla_f='';
	var cla_g='';
	var cla_fc='';
	var cla_a='';
	var cla_c='';
	var color='black';
			
	$("#autos_proceso tbody").empty();
		$.getJSON("<?= base_url()?>ajax/ventas/autos_en_proceso.php",
function (data) {
    var tr;
    for (var i = 0; i < data.length; i++) {
       
	   if(data[i].prcv_status_fi=='aprobado') cla_f='label-success'; else cla_f='label-warning';
	   if(data[i].prcv_status_gerente=='aprobado') cla_g='label-success'; else cla_g='label-warning';
if(data[i].prcv_prcv_status_credito_contado=='aprobado') cla_fc='label-success'; else cla_fc='label-warning';

if(data[i].prcv_director_gral=='aprobado') cla_a='label-success'; else cla_a='label-warning';
if(data[i].prcv_status_contabilidad =='aprobado') cla_c='label-success'; else cla_c='label-warning';	   
if(data[i].api_status==1 || data[i].api_status==2) color='blue';
if(data[i].api_status==5) color='#f89406';
if(data[i].api_status==8 ) color='green';
if(data[i].api_status==3 || data[i].api_status==4 || data[i].api_status==9) color='red';
if(data[i].api_status==null) color='black';	

   
		tr = $('<tr/>');			
        tr.append("<td >" + data[i].hus_ciudad + "</td>");
		tr.append("<td >" + data[i].dat_fecha_facturacion + "</td>");
		tr.append("<td >" + data[i].con_nombre + " " + data[i].con_apellido + "</td>");
		tr.append("<td >" + data[i].hus_ciudad + "</td>");
		tr.append("<td >" + data[i].hus_nombre + " " + data[i].hus_apellido + "</td>");
		tr.append("<td >" + data[i].data_auto + "</td>");
		tr.append("<td style='color:"+color+"' >" + data[i].data_vin + "</td>");
tr.append("<td ><span class='label label-sm "+cla_f+"'>" + data[i].prcv_status_fi + "</span></td>");
		tr.append("<td ><span class='label label-sm "+cla_g+"'>" + data[i].prcv_status_gerente + "</span></td>");
tr.append("<td ><span class='label label-sm "+cla_fc+"'>" + data[i].prcv_prcv_status_credito_contado + "</span></td>");
tr.append("<td ><span class='label label-sm "+cla_a+"'>" + data[i].prcv_director_gral + "</span></td>");
tr.append("<td ><span class='label label-sm "+cla_c+"'>" + data[i].prcv_status_contabilidad + "</span></td>");
		
        
        $('#autos_proceso').append(tr);
		
    }
	var table=$('#autos_proceso').DataTable();
	 table.destroy();
	$('#sample_carroceria').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });
});
		}
		
		
		</script>