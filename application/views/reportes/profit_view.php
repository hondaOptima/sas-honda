<?php header('Content-Type: text/html; charset=utf-8');?>
<table cellspacing="0" width="500px" border="1" style="font-family:arial; font-size:11px;">
<tr>
<td style="border:1px solid #999">INVENTARIO $ (Valor Actual en $)</td>
<td style="border:1px solid #999">$ 000</td>
<td style="border:1px solid #999">2B) VENTA DE ACEITE ($ Mes)</td>
<td style="border:1px solid #999">$ 000</td>
</tr>

<tr>
<td style="border:1px solid #999">VENTAS TOTAL DE PARTES ($ Mes)</td>
<td style="border:1px solid #999">$ 000</td>
<td style="border:1px solid #999">UTILIDAD BRUTA</td>
<td style="border:1px solid #999">$ 000</td>
</tr>

<tr>
<td style="border:1px solid #999">2A) VENTAS  DE ACCESORIOS ($ Mes)</td>
<td style="border:1px solid #999">$ 000</td>
<td style="border:1px solid #999">UTILIDAD BRUTA</td>
<td style="border:1px solid #999">$ 000</td>
</tr>

<tr>
<td style="border:1px solid #999">UTILIDAD BRUTA  ($ Mes)</td>
<td style="border:1px solid #999">$ 000</td>
<td style="border:1px solid #999">GASTOS TOTALES DE </td>
<td style="border:1px solid #999">$ 000</td>
</tr>

<tr>
<td style="border:1px solid #999">UTILIDAD OPERACIONAL ($ Mes)</td>
<td style="border:1px solid #999">$ 000</td>
<td style="border:1px solid #999">GASTOS VARIABLES </td>
<td style="border:1px solid #999">$ 000</td>
</tr>
</table>
<br>
<table cellspacing="0" width="500px" border="1" style="font-family:arial; font-size:11px;">
<tr bgcolor="#339900">
<td style="border:1px solid #999">DEL SISTEMA</td>
<td style="border:1px solid #999"># PARTE</td>
<td style="border:1px solid #999"># PIEZAS</td>
<td style="border:1px solid #999"># VALOR(redondeado)</td>
</tr>

<tr>
<td style="border:1px solid #999">INVENTARIO TOTAL (Valor Costo Promedio)</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>

<tr>
<td style="border:1px solid #999">INVENTARIO ALMACENABLE</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>


<tr>
<td style="border:1px solid #999">INVENTARIO NO ALMACENABLE (Entrada)</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>


<tr>
<td style="border:1px solid #999">PEDIDOS ESPECIALES</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>


<tr>
<td style="border:1px solid #999">PARTES SIN VENTA  7 - 12 MESES</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>


<tr>
<td style="border:1px solid #999">PARTES SIN VENTA > 12 MESES</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>

<tr>
<td style="border:1px solid #999">PARTES NUEVAS, SIN VENTAS</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>


<tr>
<td style="border:1px solid #999">COMPRAS MENSUALES PARA INVENTARIO</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>


<tr>
<td style="border:1px solid #999">COMPRAS DE EMERGENCIA</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>

<tr>
<td style="border:1px solid #999">VENTAS A COSTO</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>

<tr>
<td style="border:1px solid #999">VENTAS PERDIDAS</td>
<td style="border:1px solid #999">2358</td>
<td style="border:1px solid #999">11502</td>
<td style="border:1px solid #999">2698884</td>
</tr>

</table>