<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-table"></i> <a href="#">Profit Watch</a> <i class="fa fa-angle-right"></i> </li>


  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" onchange='<?php echo base_url();?>reportes/ordenes/'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>
    </select>
  </li>


</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->


<div class="row" style="background-color:white">
	<div class="col-md-6 col-sm-6">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Profit Watch
                </div>
            </div>
            <div class="portlet-body">
 <table width="100%" style="font-size:10px; text-align:center" >
 <tr>
 <td style="border:1px solid #666">Fecha</td>
 <td style="border:1px solid #666">Ciudad</td>
  <td style="border:1px solid #666">Descripción</td>
  <td style="border:1px solid #666">Semaforo</td>
  <td style="border:1px solid #666">Vta. Perdida.</td>
  <td style="border:1px solid #666">Inv. CEC.</td>
  <td style="border:1px solid #666">Profit Watch</td>
  <td style="border:1px solid #666">Mes Pibote</td>
   <td style="border:1px solid #666">Eliminar</td>
   </tr>
 <?php foreach($file as $info){

	if($info->iec_mes<=9){
	$mes='0'.$info->iec_mes;
	}
	else{
		$mes=$info->iec_mes;
		}
	 $mes;
	 ?>
 <input type="hidden" value="<?= $info->iec_cd?>" name="cd<?= $info->iec_ID?>">
 <input type="hidden" value="<?= $info->iec_file_semaforo?>" name="file<?= $info->iec_ID?>">
  <input type="hidden" value="<?= $info->iec_file_inv_cec?>" name="filec<?= $info->iec_ID?>">
    <tr>
      <td style="border:1px solid #666"><?= $info->iec_fecha?></td>
 <td style="border:1px solid #666"><?= $info->iec_cd?></td>
 <td style="border:1px solid #666"><?= $info->iec_desc?></td>

  <td style="border:1px solid #666;" >
<div class="loadingS<?= $info->iec_ID?>" style="display:none; color:green">Procesando...</div>
<div class="detalleS<?= $info->iec_ID?>" style="display:none;">
<a target="_blank" href="<?= base_url()?>reportes/semaforo/?cd=<?= $info->iec_cd?>&file=<?= $info->iec_file_semaforo?>&mes=<?= $mes?>"> Detalle </a></div>


<?php if($info->iec_procesado>=1){

if($mes==12) $mes='01'; else $mes=$mes +1;
?>
<div class="detalleS<?= $info->iec_ID?>"><a target="_blank" href="<?= base_url()?>reportes/semaforo/?cd=<?= $info->iec_cd?>&file=<?= $info->iec_file_semaforo?>&mes=<?= $mes;?>"> Detalle </a></div>
<?php }else{?>
<div class="procesar_semaforo showS<?= $info->iec_ID?>" style="color:red; cursor:pointer"  id="<?= $info->iec_ID?>" > Procesar <i class="fa fa-gears"></i></div>
<?php }?>
</td>

  <td style="border:1px solid #666">
  <div class="loadingE<?= $info->iec_ID?>" style="display:none; color:green">Procesando...</div>
 <div class="detalleE<?= $info->iec_ID?>" style="display:none;">
  <a target="_blank" href="<?= base_url()?>reportes/no_parte_mes/?cd=<?= $info->iec_cd?>&mes=02"> Detalle </a>
  </div>
   <?php if($info->iec_procesado>=2){?>
  <a target="_blank" href="<?= base_url()?>reportes/no_parte_mes/?cd=<?= $info->iec_cd?>&mes=02"> Detalle </a>
  <?php } elseif($info->iec_procesado==1){?>
  <div class="procesar_sec showE<?= $info->iec_ID?>"  id="<?= $info->iec_ID?>" style="color:red; cursor:pointer" > Procesar <i class="fa fa-gears"></i></div>
  <?php }elseif($info->iec_procesado==0){ ?>
   <i class="fa fa-clock-o"></i>
  <?php } ?>
  </td>


   <td style="border:1px solid #666;" >
<div class="loadingC<?= $info->iec_ID?>" style="display:none; color:green">Procesando...</div>
<div class="detalleC<?= $info->iec_ID?>" style="display:none;"><a target="_blank" href="<?= base_url()?>reportes/sin_exi_cero/?cd=<?= $info->iec_cd;?>&file=<?= $info->iec_file_inv_cec;?>"> Detalle </a></div>

<?php if($info->iec_procesado==3){?>
<div class="detalleC<?= $info->iec_ID?>"><a target="_blank" href="<?= base_url()?>reportes/sin_exi_cero/?cd=<?= $info->iec_cd;?>&file=<?= $info->iec_file_inv_cec;?>"> Detalle </a></div>
<?php }elseif($info->iec_procesado==2){?>
<div class="procesar_cec showC<?= $info->iec_ID?>"  id="<?= $info->iec_ID?>" style="color:red; cursor:pointer" > Procesar <i class="fa fa-gears"></i></div>
<?php }else{?>
  <i class="fa fa-clock-o"></i>
 <?php }?>
 </td>

 <td style="border:1px solid #666">

 <?php if($info->iec_procesado==3){?>
<table width="100%" align="center"><tr><td>
<div class="detalleC<?= $info->iec_ID?>"><a target="_blank" href="<?php echo base_url()?>reportes/profit_print/?id=<?= $info->iec_ID?>"><i class="fa fa-pencil-square-o"></i> </a></div>
</td><td>
<div class="detalleC<?= $info->iec_ID?>"><a target="_blank" href="<?php echo base_url()?>reportes/profit_view/?id=<?= $info->iec_ID?>"><i class="fa  fa-paste "></i> </a></div>

</td></tr></table>
<?php }else{?>
 <i class="fa fa-clock-o"></i>
<?php }?>
 </td>
<?php
if($info->iec_cd=='Mexicali'){$tallercd='mex';}
if($info->iec_cd=='Tijuana'){$tallercd='tij';}
if($info->iec_cd=='Ensenada'){$tallercd='ens';}
if($info->iec_mes<=9){
	$mes='0'.$info->iec_mes;
	}
	else{
		$mes=$info->iec_mes;
		}
	$mes;
?>
  <td style="border:1px solid #666"><?php echo $meses[$mes];?></td>
   <td style="border:1px solid #666; width:25px;"><a href="<?php echo base_url()?>reportes/deleteArchivosProfit/<?= $tallercd?>/<?= $mes?>/<?= $info->iec_ID?>/"><i class="fa fa-trash-o"></i></a></td>

   </tr>
 <?php } ?>


   </table>

        </div>
	</div>
 </div>
 <?= $flash_message;?>
 <?= validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">×</button>','</div>');?>
 	<div class="col-md-6 col-sm-6">

   <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Subir Archivos
                </div>
            </div>
            <div class="portlet-body">
            <?php echo  form_open_multipart('reportes/profit','class="form-horizontal"'); ?>
            <center>
            <input type="radio" name="cd"  value="Tijuana" <?php if($taller==3){echo'checked';}?>>Tijuana <input type="radio" name="cd"  value="Mexicali"  <?php if($taller==2){echo'checked';}?>> Mexicali <input type="radio" name="cd"  value="Ensenada"  <?php if($taller==1){echo'checked';}?>> Ensenada</center>
            <br>
          <table>
          <tr><td>
            Descripción (*)</td><td><input type="text"  name="desc" required></td></tr>
          <tr><td>
            Inventario  SEC (Semaforo) (*)</td><td><input type="file" style="width:140px;color:#446655;display: inline;"  class="form-control"id="userfile" name="userfile[]" required></td></tr>
            <tr><td>Inventario con CEC (Cto. Vta) (*)</td><td><input type="file" style="width:140px;color:#446655;display: inline;"  class="form-control"id="userfileb" name="userfile[]" ></td></tr>

            <tr><td>Mes Pibote (*)</td><td><select name="mes">
            <?php
            foreach($meses as $ms=>$msn){
				if($ms==date('m')){$sel='selected="selected"';}else{$sel='';}
			echo '<option value="'.$ms.'" '.$sel.'>'.$msn.'</option>';
				}
			?>
            </select></td></tr>

            </table>


            <br>
           <center> <input type="submit" value="Subir Achivos" class="subir"></center>
           <?php echo form_close(); ?>
            </div>
        </div>
	</div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
<script>
jQuery(document).ready(function() {

$('.subir').live('click',function(){
$(this).hide();
	});

$('.procesar_semaforo').live('click',function(){
var id=$(this).attr('id');
$('.showS'+id+'').hide();
$('.loadingS'+id+'').show();
var cd=$('input[name=cd'+id+']').val();
var file=$('input[name=file'+id+']').val();
$.ajax({
  method: "GET",
  url: "<?= base_url();?>ajax/reportes/subir_inv_cero.php",
  data: {id:id,cd:cd, file:file}
})
  .done(function( msg ) {
if(msg){
//$('.loadingS'+id+'').hide();
//$('.detalleS'+id+'').show();
location.reload();
}
  });

	});


	$('.procesar_cec').live('click',function(){
var id=$(this).attr('id');
$('.showC'+id+'').hide();
$('.loadingC'+id+'').show();
var cd=$('input[name=cd'+id+']').val();
var file=$('input[name=filec'+id+']').val();
$.ajax({
  method: "GET",
  url: "<?= base_url();?>ajax/reportes/subir_inv_sin_cero.php",
  data: {id:id,cd:cd, file:file}
})
  .done(function( msg ) {
if(msg){
//$('.loadingC'+id+'').hide();
 //$('.detalleC'+id+'').show();
 location.reload();
}
  });

	});


	$('.procesar_sec').live('click',function(){
var id=$(this).attr('id');
$('.showE'+id+'').hide();
$('.loadingE'+id+'').show();
var cd=$('input[name=cd'+id+']').val();
$.ajax({
  method: "GET",
  url: "<?= base_url()?>reportes/no_parte_mes/?mes=<?= (date('m')-1)?>",
  data: {id:id,cd:cd}
})
  .done(function( msg ) {
if(msg){
$('.loadingE'+id+'').hide();
$('.detalleE'+id+'').show();
location.reload();
}
  });

	});





});
</script>
