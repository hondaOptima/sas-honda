

<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/moment.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"
type="text/javascript" ></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"
type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/moment.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"
type="text/javascript"></script>



<script src="<?php echo base_url();?>assets/plugins/select2/select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/data-tables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/fuelux/js/spinner.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"
type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/moment.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.input-ip-address-control-1.0.min.js"
type="text/javascript" ></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript" ></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-markdown/lib/markdown.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js"
type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/scripts/table-managed.js"></script>
<script src="<?php echo base_url();?>assets/scripts/index.js"></script>
<script src="<?php echo base_url();?>assets/scripts/form-components.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.form.min.js"></script>
<script src="<?php echo base_url();?>assets/scripts/form-samples.js"></script>


<!-- Scrip para dibujar en un canvas-->
<script src="<?php echo base_url();?>assets/scripts/jquery.jqscribble.js"></script>
<script src="<?php echo base_url();?>assets/scripts/jqscribble.extrabrushes.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/sweet/sweetalert2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/select2/select2.css">
<script src="<?php echo base_url();?>assets/plugins/sweet/sweetalert2.js"></script>


<script>
jQuery(document).ready(function() {
   Index.init();
   Index.initCalendar();
   Index.initDashboardDaterange();
   TableManaged.init();
   FormComponents.init();
	 FormSamples.init();

   $('#inventario').dataTable( {
        "bInfo":false,
        "bProcessing": true,
        "bServerSide": true,
        "bRetrieve": true,
        "bDestroy": true,
        "sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista.php",
	});

	$('#inventario_en_piso').dataTable( {
		"lengthMenu": [2],
		"bInfo":false,
		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
						"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>ajax/inventario/lista_en_piso.php",


	} );

	$('.llego').live('click',function(){
		var id=$(this).val();
		var val=$(this).attr('name');


    $.ajax({url: "<?= base_url();?>ajax/inventario/inv_update.php?vin="+id+"&status="+val+"", success: function(result){
        window.location="<?= base_url()?>reportes/inventario";
      }});



		});



});
</script>
