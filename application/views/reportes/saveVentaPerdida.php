<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='repor';$this->load->view('globales/menu',$data); ?>
<form name="form1" method="get" action="<?php echo base_url();?>reportes/saveVentaPerdida/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-table"></i> <a href="#">Profit Watch</a> <i class="fa fa-angle-right"></i> </li>
  
 <li class="pull-right"> 
 <select name="anio" onchange='this.form.submit()'>
 <option value="" <?php if($anio=='') echo 'selected="selected"';?> >Seleccione Una Opcion</option>	
 <option value='2016' <?php if($anio=='2016') echo 'selected="selected"';?>>2016</option>
 <option value='2017' <?php if($anio=='2017') echo 'selected="selected"';?>>2017</option>
 </select>
 </li>
  <li class="pull-right" style="margin-left:8px; margin-right:8px;">
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
  </li>
<li class="pull-right" style=" margin-left:8px; margin-top:1px;">
  
  <select name="mes" onchange="this.form.submit()">
 
  <?php
  foreach($meses as $numm=> $nommes){
if($numm==$mes){$sel='selected="selected"';}else{$sel='';}	  
echo'<option value="'.$numm.'"  '.$sel.'>'.$nommes.'</option>';	  
	  }
  
  ?>
  </select>
   </li>
        
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 
</form>
<!-- BEGIN EXAMPLE TABLE PORTLET-->


<div class="row" style="background-color:white">
	<div class="col-md-6 col-sm-6">
    <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Venta Perdida del Mes
                </div>
            </div>
            <div class="portlet-body">

 <table  align="" width="100%" style="font-size:10px; text-align:center" >
 <tr>
 <td style="border:1px solid #666">Fecha</td>
 <td style="border:1px solid #666">Numero de Parte</td>
  <td style="border:1px solid #666">Piezas</td>
  <td style="border:1px solid #666">Costo Unitario</td>
  <td style="border:1px solid #666">Eliminar</td>  
   </tr>      
 <?php  
 
 foreach($info as $inf){ 
echo '<tr>';
	 ?>  
      <td style="border:1px solid #666"><?php echo $inf->hit_hora?></td>
  <td style="border:1px solid #666"><?php echo $inf->hit_noparte?></td>
 <td style="border:1px solid #666"><?= $inf->hit_piezas?></td>
  <td style="border:1px solid #666"><?php echo $inf->hit_costo?></td>
    <td style="border:1px solid #666">
    <a href="<?= base_url()?>reportes/eliminarHit/<?= $inf->hit_IDhits?>">
    <i class="fa fa-trash-o"></i>
    </a>
    </td>
 <?php echo '</tr>';

 } ?>
 </table>
 


        </div>
	</div>
 </div>
 
 
 
  	<div class="col-md-6 col-sm-6">
    
   <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Guardar Numero de Parte
                </div>
            </div>
            <div class="portlet-body">
            <?php echo  form_open_multipart('reportes/saveVentaPerdida','class="form-horizontal"'); ?>
            <center>
           
          <table>
          <tr><td>  # Parte</td><td><input type="text"  name="parte" required></td></tr>
          <tr><td>  Piezas</td><td><input type="text"  name="pieza" required></td></tr>
          <tr><td>  Unitario </td><td><input type="text"  name="unitario" required></td></tr>
          
            
            </table>            
                                    
            
            <br>
           <center> <input type="submit" value="Guardar Información"></center>
           <?php echo form_close(); ?>
            </div>
        </div>
	</div>
    </div>
 	
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('reportes/js/script');?>
