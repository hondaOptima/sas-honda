<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='cotizaciones';$this->load->view('globales/menu',$menu); ?>
<form name="form1" method="get" action="<?php echo base_url();?>cotizaciones">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="fa fa-money"></i>
				<a href="#">Cotizaciones</a>
			<i class="fa fa-angle-right"></i>
		</li>

		<li class="pull-right" style="text-align:right; margin-left:15px;">
			<Select name="asesor" onchange='this.form.submit()'>
				<option value="0" <?php if($asesor==0){echo 'selected="selected"';}?>>Todos</option>
				<?php foreach($usuario as $us) {
				if($us->rol_idRol=='3' && $us->sus_workshop==$taller && $us->sus_isActive==1) {
				?>
				<option value="<?php echo $us->sus_idUser?>" <?php if($asesor==$us->sus_idUser){echo 'selected="selected"';}?>><?php echo $us->sus_name.' '.$us->sus_lastName;?></option>
				<?php }} ?>
			</select>
		</li>
        <li class="pull-right" style="text-align:right; margin-left:15px;">
    <?php if($_SESSION['sfrol']<=2) {?>

    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>
    </select>
    <?php } ?>
    </li>
		<li class="pull-right" >
            <div  class="input-group input-medium date " id="date-pickerbb" data-date-format="yyyy-mm-dd" style="text-align:right;">
            <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
            Fecha: <?php echo $fecha;?> <i style="cursor:pointer; float:right; margin-left:5px;" class="fa fa-calendar  input-group-btn"></i>
            </div>
		</li>
	</ul>
</form>
<!-- END OVERVIEW STATISTIC BARS-->

<?php echo $flash_message;?>

<div class="row">
	<div class="col-md-6 col-sm-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Ordenes del día
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover"  >
                    <thead>
                        <tr style="font-weight:bold">
                        <th style="text-align:center"></th>
                            <th style="text-align:center"><b>O.S.</b></th>
                            <th style="text-align:center"><b>Asesor</b></th>
                            <th style="text-align:center"><b>Cliente</b></th>
                            <th style="text-align:center"><b>VIN</b></th>
                            <th style="text-align:center"><b>Torre</b></th>
                            <th style="text-align:center"><b>Acciones</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $y = 1; foreach($ordenes as $row) { ?>
                        <?php if($row->cotiza == 1){ ?>
                            <tr style="background-color:rgba(46, 119, 118, 0.5); ">
                        <?php }else{ ?>
                            <tr>
                        <?php } ?>
                        <td><?php echo $y;?></td>
                            <td style="text-align:center"><?php echo $row->seo_serviceOrder; ?></td>
                            <td style=" text-align:center; cursor:help;"
                            title="<?php echo $row->sus_name.' '.$row->sus_lastName; ?>">
								<?php echo $row->sus_adviserNumber; ?>
							</td>
                            <td style="text-align:center"><?php echo substr($row->sec_nombre,0,25) ?></td>
                            <?php if($row->camp==1){ ?>
                            <?php if($row->campGreen == 1){?>
                                <td style="text-align:center; color:red; font-size:1.1em;"  ><?php echo $row->sev_vin; ?></td>
                            <?php }else{ ?>
                                <td style="text-align:center; color:blue; font-size:1.1em;"  ><?php echo $row->sev_vin; ?></td>
                            <?php } ?>

                            <?php }else{ ?>
                             <td style="text-align:center; "  ><?php echo $row->sev_vin; ?></td>
                            <?php } ?>
                             <td style="text-align:center"><?= $row->seo_tower; ?></td>
                            <td width="100px;" style="text-align:center">
                                <a target="_blank" href="<?= base_url().'orden/llenar1/'.$row->seo_idServiceOrder;?>"
                                class="btn btn-warning btn-xs" title="Editar Orden de Servicio"><i class="fa fa-clipboard"></i></a>
                                <a   class="btn btn-success btn-xs show-estimates"  href="<?= base_url()?>orden/printtestDuplicadoBueno/<?php echo $row->sev_idVehicleInformation;?>" title="Ver Cotizaciones"
                                data-order="<?= $row->seo_serviceOrder;?>" id="<?= $row->seo_idServiceOrder;?>" >
                                <i class="fa fa-eye"></i></a>

                                    <i
                                        class="fa fa-tags"
                                        style="color:get_traslados_origen_y_destino; font-size:1.5em;"
                                        onclick="clickGarantia(this)"
                                        orden="<?= $row->sev_vin;?>">
                                    </i>


                                <? if($row->sev_km_recepcion > 50000 && $row->sev_km_recepcion < 60000){ ?>
                                        <i
                                            class="fa fa-certificate"
                                            style="color:red;"
                                            onclick=""
                                            title="Este auto tiene <?= number_format($row->sev_km_recepcion); ?> KM"
                                            orden="<?= $row->seo_idServiceOrder;?>">
                                        </i>
                                <? }else{ ?>
                                        <i
                                            class="fa fa-certificate"
                                            style="color:rgba(240, 239, 240, 1);"
                                            onclick=""
                                            orden="<?= $row->seo_idServiceOrder;?>">
                                        </i>
                                <? } ?>
                            </td>
                        </tr>
                        <?php $y++;} ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>

         <style>
        .cover-garantia{
          position: fixed;
          top: 0;
          left: 0;
          background: rgba(149, 149, 149, 0.7);
          z-index: 1;
          width: 100%;
          height: 100%;
        }

        .modal-garantia{
          position: fixed;
          width: 50%;
          height: 300px;
          box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
          top: 25%;
          background: white;
          color:rgba(53, 116, 222, 1);
          left: 25%;
          text-align: center;
        }
        .modal-garantia h2{
          color:gray;
        }

        .alert{
          margin: 0 !important;
          border-radius: 0 !important;
        }

        .middle-garantia{
          width:50%;
          height:300px;
          float:left;

        }

        .middle-garantia div{
          color:black !important;
          font-weight:bold !important;
          background: rgba(247, 248, 252, 1) !important;
          font-size:1.2em !important;
        }
      </style>



	<div class="col-md-6 col-sm-6" >
        <!-- Cotizaciones -->
		<!-- BEGIN EXAMPLE TABLE PORTLET-->

         <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Porcentaje de Utilización de Taller
                </div>

            </div>
            <div class="portlet-body form"   >



            <form role="form" class="form-horizontal form-bordered">
               <div class="form-group last">
										<div class="col-md-12">
											<input id="range_333" type="text" name="range_3" value="0;100"/>


										</div>
                                        </div>

                </form>

            </div>
        </div>



        <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Cotizaciones de Orden
                </div>
                <div class="actions">
                    <a href="<?php echo base_url().'cotizaciones/crear/';?><?php if($ordenes) { echo $ordenes[0]->seo_idServiceOrder; } ?>" class="btn btn-default" id="new_estimate">
                        <i class="fa fa-pencil"></i> Crear Cotizaci&oacute;n
                    </a>
                </div>
            </div>
            <div class="portlet-body"   >
                <table class="table table-striped table-bordered table-hover" id="sample_cotizaciones" >
                    <thead>
                        <tr style="font-weight:bold">
                        	<td style="text-align:center" colspan="4"  id="order_title"><b>Orden - <?php if($ordenes) { echo $ordenes[0]->seo_serviceOrder; } ?> </b></td>
                        </tr>
                        <tr style="font-weight:bold">
                            <th style="text-align:center"><b>Fecha</b></th>
                            <th style="text-align:center"><b>Estatus</b></th>
                            <th style="text-align:center"><b>Acciones</b></th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    	<?php foreach($cotizaciones as $row) { ?>
                            <tr>
                                <td style="text-align:center"><?php echo $row->date; ?></td>
                                <td style="text-align:center">
                                	<?php if($row->est_confirmed == 1) { ?>
                                    <a href="<?php echo base_url().'cotizaciones/desconfirmar/'.$row->est_idEstimate; ?>"
									onClick="return confirm('Esta seguro que desea cambiar el estatus de esta cotización?')">
                                    <span class="label label-sm label-success"> Confirmado </span> </a>
                                    <?php } else { ?>
                                    <a href="<?php echo base_url().'cotizaciones/confirmar/'.$row->est_idEstimate; ?>"
									onClick="return confirm('Esta seguro que desea cambiar el estatus de esta cotización?')">
                                    <span class="label label-sm label-warning"> Sin Confirmar </span> </a>
                                    <?php } ?>
                                </td>
                                <td width="160px;" style="text-align:center">
                                    <a target="_blank" href="<?php echo base_url().'cotizaciones/prueba_documento/'.$row->est_idEstimate; ?>"
                                    class="btn btn-warning btn-xs"  title="Ver PDF" ><i class="fa fa-file-o "></i></a>
                                    <a href="<?php echo base_url().'cotizaciones/sendDocument/'.$row->est_idEstimate; ?>" class="btn btn-info btn-xs" title="Enviar PDF"><i class="fa fa-envelope-o"></i></a>
                                    <a target="_blank" href="<?php echo base_url().'cotizaciones/editar/'.$row->est_idEstimate; ?>"
                                    class="btn btn-success btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url().'cotizaciones/borrar/'.$row->est_idEstimate; ?>"
                                    onClick="return confirm('Está seguro que desea borrar esta cotización?');"
                                    class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>




        <div class="portlet">
      <div class="portlet-title" style="background-color:#999">
        <div class="caption"> <i class="fa fa-users"></i>Vista Previa de Orden de Servicio Seleccionada </div>

          <input type="hidden" name="idseleccionada" value="0">

        <?php if($_SESSION['sfrol']==2 || $_SESSION['sfrol']==1){?>
         <div id="getAsesores" class="tools"><div class="btn btn-xs btn-info">Cambiar de Asesor</div></div>
        <!--<div id="eliminaros" class="tools"><div class="btn btn-xs btn-danger">Eliminar</div></div>-->
      <?php }?>

      </div>
      <div class="portlet-body">
       <iframe id="framesrc" src="" width="100%" frameborder="0" height="500px;"></iframe>
      </div>
    </div>
    </div>




</div>
   <div style="display:none" class="cover-garantia">
            <div class="modal-garantia">

              <div class="middle-garantia">
                <h2>Detalle de la garant&iacute;a:<h3 id="h2-vin"></h3></h2><hr>
                <div class="tipo-gar alert alert-info"></div>
                <div class="tiempo-gar alert alert-info"></div>
								<div  class=" alert alert-info"><h4><b>Km vencimiento:</b>60,000 km</h4></div>
              </div>

              <div class="middle-garantia">
                <h2>KM actual:<h3 id="h2-km"></h3></h2><hr>
								<div class="tipo-ext alert alert-info"></div>
								<div class="tiempo-ext alert alert-info"></div>
                <div class="kilometros-ext alert alert-info"></div>
              </div>



            </div>
      </div>

<script id="estimate-row" type="text/x-handlebars-template">
<tr>
	<td style="text-align:center">{{date}}</td>
	<td style="text-align:center">
		{{#if est_confirmed}}
		<a href="<?php echo base_url().'cotizaciones/desconfirmar/'; ?>{{est_idEstimate}}"
		onClick="return confirm('Esta seguro que desea cambiar el estatus de esta cotización?')">
		<span class="label label-sm label-success"> Confirmado </span> </a>
		{{else}}
		<a href="<?php echo base_url().'cotizaciones/confirmar/'; ?>{{est_idEstimate}}"
		onClick="return confirm('Esta seguro que desea cambiar el estatus de esta cotización?')">
		<span class="label label-sm label-warning"> Sin Confirmar </span> </a>
		{{/if}}
	</td>
	<td width="160px;" style="text-align:center">
		<a target="_blank" href="<?php echo base_url().'cotizaciones/prueba_documento/'; ?>{{est_idEstimate}}"
		class="btn btn-warning btn-xs" title="Ver PDF" id="new_estimate"><i class="fa fa-file-o "></i></a>
		<a href="<?php echo base_url().'cotizaciones/sendDocument/'; ?>{{est_idEstimate}}"  class="btn btn-info btn-xs" title="Enviar PDF"><i class="fa fa-envelope-o"></i></a>
		<a target="_blank" href="<?php echo base_url().'cotizaciones/editar/'; ?>{{est_idEstimate}}"
		class="btn btn-success btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
		<a href="<?php echo base_url().'cotizaciones/borrar/'; ?>{{est_idEstimate}}"
		onClick="return confirm('Está seguro que desea borrar esta cotización?');"
		class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"></i></a>
	</td>
</tr>
</script>

<script src="<?php echo base_url(); ?>assets/scripts/handlebars-v1.3.0.js" type="text/javascript"></script>
<?php $this->load->view('orden/captura/asesores');?>
<?php $this->load->view('globales/footer');?>
<?php
$data['utilizacion']=$utilizacion;
$this->load->view('cotizaciones/js/script',$data);?>
<script type='text/javascript'>

function clickGarantia(e){
         var orden = $(e).attr('orden');


         $.get('<?= base_url();?>ajax/ordendeservicio/getNewGarantia.php?orden='+orden, function(data){
          $('.tipo-gar').empty();
          $('.tiempo-gar').empty();
          $('.kilometros-gar').empty();
          $('#h2-vin').empty();

          $('.tipo-ext').empty();
          $('.tiempo-ext').empty();
          $('.kilometros-ext').empty();
          $('#h2-km').empty();

            var json = JSON.parse(data);

            $('#h2-vin').append(json.Base.vin);
            $('.tipo-gar').append('<h4><b>GARANTÍA BASE</b></h4>');
            $('.tiempo-gar').append('<h4><b>Fecha de vencimiento:</b> '+json.Base.facturacion+'</h4>');

            $('#h2-km').append(json.Base.km);
            $('.tipo-ext').append('<h4><b>EXTENSIÓN DE GARANTÍA</b></h4>');
            $('.tiempo-ext').append('<h4><b>Fecha vencimiento:</b> '+json.Extension.km+'</h4>');
            $('.kilometros-ext').append('<h4><b>Km vencimiento:</b> '+json.Extension.facturacion+'</h4>');
            $('.cover-garantia').fadeIn(100);
         });
        };
        $('.cover-garantia').click(function(e){
          var container = $('.cover-garantia');
          var div = $('.modal-garantia')
          if (!div.is(e.target) // if the target of the click isn't the container...
              && div.has(e.target).length === 0) // ... nor a descendant of the container
          {
              container.fadeOut(100);
          }
        });


Handlebars.registerHelper("if", function(est_confirmed, options) {
 if(est_confirmed == 1){
    return options.fn(this);
  } else {
    return options.inverse(this);
 }
});
	var tableBody = $("#table-body");

	var estimateRowHtml = $("#estimate-row").html();
	var estimateRow = Handlebars.compile(estimateRowHtml);

	function showEstimate(estimates) {
		tableBody.empty();
		//console.log(incomes);
		$.each(estimates, function (index, estimate) {
			tableBody.append(estimateRow(estimate));
		});
	}


	$(".confirm").on('click', function(e){
									   e.preventDefault();
									   alert('mensaje');
									   });


	$newEstimate = $("#new_estimate");
	$orderTitle = $("#order_title");
	$(document).ready(
		function() {
			$(".show-estimates").on('click',
							   function(e) {
								   e.preventDefault();
								   $orden = $(this).attr("data-order");
								   $id = $(this).attr("id");

								    $('input[name=idseleccionada]').val($id);
								   $new_url = 'cotizaciones/crear/'+$id
								   $orderTitle.html('Orden - ' + $orden);
								   $newEstimate.attr("href", $new_url);
								   var href=$(this).attr("href")
								   		$.ajax({
											url:"<?php echo base_url(); ?>cotizaciones/obtener_cotizaciones_ajax/"+$id,
											type:"GET",
											dataType : "json",
											success:function(result){
													showEstimate(result);

													$("#framesrc").attr("src", href);
												}
										});

								   });
		} );
</script>
<script>
jQuery(document).ready(function() {
	$(function () {

		$(".changease").live('click',function() {

       var id=$('input[name=idseleccionada]').val();
       var value=$(this).val();

       $.ajax({
	  url:"<?php echo base_url();?>orden/updateOrdenAsesor?id="+id+"&valor="+value+"",
	  success:function(result){
		 window.location="<?= base_url();?>cotizaciones?fecha=<?= $fecha;?>";
		         }
         });


    });

	 $('#date-pickerbb').datepicker({
	language: 'es',
	isRTL: false,
         autoclose:true

	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });


	});



	$(".file").live('click',function() {

       var tit=$(this).attr('id');

       $("#framesrc").attr("src", $(this).attr("id"));
       $('input[name=idseleccionada]').val(tit);

    });

	$("#getAsesores").live('click',function() {
		var id=$('input[name=idseleccionada]').val();
		$('#wide').modal('show');
		var el = id.split('/');
		un=el[0];
		ds=el[1];
		tr=el[2];
		cu=el[3];
		cn=el[4];
		se=el[5];
		si=el[6];

		 $.ajax({
	  url:"<?php echo base_url();?>ajax/ordendeservicio/getAsesores.php?id="+si+"&taller=<?php echo $taller;?>",
	  success:function(result){
		   $('#infoAsesor').html(result);
		         }
         });


		});






});
</script>
