<!DOCTYPE html>
<html lang="esp" class="no-js">
    <head>
        <?php header('Content-Type: text/html; charset=UTF-8'); ?>
        <title>Honda Optima | SAS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.css" media="screen"/>

        <!-- BEGIN THEME STYLES -->
        <link href="<?= base_url();?>assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/jquery-ui-slider-pips.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <!-- PLUGINS -->
        <link href="<?= base_url() ?>assets-new/css/carroceria.css" rel="stylesheet" />

        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/datatables/responsive.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/sweet-alert-2/sweetalert2.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/amaran/amaran.min.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/select2/select2.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/select2/select2_conquer.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.css"/>
        <!-- /PLUGINS -->

        <!-- Para los iconitos de la derecha -->
        <link href="<?= base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">

            td img{
                display: block;
                margin-left: auto;
                margin-right: auto;
                height: 40px !important;
            }

            input[type=checkbox] {
              transform: scale(3);
            }

            #printable { display: none; }

            @media print {
            	#non-printable { display: none; }
            	#printable { display: block; }
            }

            .client-m {
                width:160px !important;
            }

            .page-content{
                width:100%; margin:0px;
            }

            .hovereffect {
                width:100%;
                height:100%;
                float:left;
                overflow:hidden;
                position:relative;
                text-align:center;
                cursor:default;
                }

                .hovereffect .overlay {
                width:100%;
                height:100%;
                position:absolute;
                overflow:hidden;
                top:0;
                left:0;
                opacity:0;
                background-color:rgba(0,0,0,0.5);
                -webkit-transition:all .4s ease-in-out;
                transition:all .4s ease-in-out
                }

                .hovereffect img {
                display:block;
                position:relative;
                -webkit-transition:all .4s linear;
                transition:all .4s linear;
                }

                .hovereffect h2 {
                text-transform:uppercase;
                color:#fff;
                text-align:center;
                position:relative;
                font-size:17px;
                background:rgba(0,0,0,0.6);
                -webkit-transform:translatey(-100px);
                -ms-transform:translatey(-100px);
                transform:translatey(-100px);
                -webkit-transition:all .2s ease-in-out;
                transition:all .2s ease-in-out;
                padding:10px;
                }

                .hovereffect a.info {
                text-decoration:none;
                display:inline-block;
                text-transform:uppercase;
                color:#fff;
                border:1px solid #fff;
                background-color:transparent;
                opacity:0;
                filter:alpha(opacity=0);
                -webkit-transition:all .2s ease-in-out;
                transition:all .2s ease-in-out;
                margin:50px 0 0;
                padding:7px 14px;
                }

                .hovereffect a.info:hover {
                box-shadow:0 0 5px #fff;
                }

                .hovereffect:hover img {
                -ms-transform:scale(1.2);
                -webkit-transform:scale(1.2);
                transform:scale(1.2);
                }

                .hovereffect:hover .overlay {
                opacity:1;
                filter:alpha(opacity=100);
                }

                .hovereffect:hover h2,.hovereffect:hover a.info {
                opacity:1;
                filter:alpha(opacity=100);
                -ms-transform:translatey(0);
                -webkit-transform:translatey(0);
                transform:translatey(0);
                }

                .hovereffect:hover a.info {
                -webkit-transition-delay:.2s;
                transition-delay:.2s;
                }
        </style>
    </head>
    <body>
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>Crear Cotización
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form>
                                <div class="form-body">
                                    <h4 class="form-section">Datos del cliente</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="txt_nombre" value="Juan Perez" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Fecha</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="txt_fecha" value="<?= $fecha ?>" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="form-section">Datos del vehículo</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Modelo</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="txt_modelo" value="Civic" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Año</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="txt_anio" value="2016" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Color</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="txt_color" value="Azul" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="form-section">Servicios</h4>
                                    <div class="form-actions fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3 pull-right">
                                                    <button type="button" class="btn btn-default btn-cancelar">Cancelar</button>
                                                    <button class="btn btn-primary btn-guardar">Guardar</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-12">
                                            <table class="table table-striped table-bordered table-hover" id="" >
                                                <thead>
                                                    <tr style="font-weight:bold">
                                                        <th>#</th>
                                                        <th><b>Descripción</b></th>
                                                        <th><b>Precio</b></th>
                                                        <th><b>Cantidad</b></th>
                                                        <th><b>Total</b></th>
                                                        <th><b>Foto</b></th>
                                                        <th><b>¿Autorizar?</b></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Rotación de llantas</td>
                                                        <td>$1,000.00</td>
                                                        <td>1</td>
                                                        <td>$1,000.00</td>
                                                        <td><a href="https://i5.walmartimages.com/dfw/4ff9c6c9-cffb/k2-_217824fd-df7b-432f-bede-7ae7792ccc4d.v1.jpg" target="_blank"><img class="img-service" src="https://i5.walmartimages.com/dfw/4ff9c6c9-cffb/k2-_217824fd-df7b-432f-bede-7ae7792ccc4d.v1.jpg" alt="Servicio"></a></td>
                                                        <!-- <td>
                                                            <div class="hovereffect">
                                                                <img class="img-responsive" src="https://i5.walmartimages.com/dfw/4ff9c6c9-cffb/k2-_217824fd-df7b-432f-bede-7ae7792ccc4d.v1.jpg" alt="">
                                                                <div class="overlay">
                                                                    <h2>Hover effect 1</h2>
                                                                    <a class="info" target="_blank" href="https://i5.walmartimages.com/dfw/4ff9c6c9-cffb/k2-_217824fd-df7b-432f-bede-7ae7792ccc4d.v1.jpg">link here</a>
                                                                </div>
                                                            </div>
                                                        </td> -->
                                                        <td>
                                                            <!-- <div class="checkbox">
                                                                <input type="checkbox" value="">
                                                            </div> -->

                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox2" type="checkbox">
                                                                <!-- <label for="checkbox2">
                                                                    Autorizar
                                                                </label> -->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3 pull-right">
                                                <button type="button" class="btn btn-default btn-cancelar">Cancelar</button>
                                                <button class="btn btn-primary btn-guardar">Guardar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>



















                </div>
            </div>
        </div>
    </body>
</html>

<script type="text/javascript">
    window.url_base = "<?= base_url() ?>";
</script>
<script src="<?= base_url() ?>assets-new/plugins/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>

<!-- <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/moment/moment.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/sweet-alert-2/sweetalert2.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datatables/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/amaran/jquery.amaran.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/select2/select2.js" type="text/javascript"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/js/cotizaciones-autorizacion-cliente.js"></script>
