<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<script type="text/javascript">

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

</script>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="<?= base_url();?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-money"></i>
        <a href="<?= base_url();?>cotizaciones">Cotizaciones</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-edit"></i>
        Crear
        <i class="fa fa-angle-right"></i>
    </li>
</ul>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-reorder"></i>Crear Cotización
            </div>
        </div>
        <div class="portlet-body form">
			<?= $flash_message; ?>
            <!-- BEGIN FORM-->

            <?=  form_open_multipart('cotizaciones/crear/'.$info[0]->seo_idServiceOrder,'class="form-horizontal forma"'); ?>
                <div class="form-body">
                    <h4 class="form-section">Datos del cliente</h4>
                    <div class="row">
                    	<input type="hidden" name="cliente_id" value="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nombre</label>
                                <div class="col-md-9">
                                    <?= form_input('nombre', $info[0]->sec_nombre,
                                            'id="nombre"  maxlength="50" class="form-control"
                                            placeholder="Nombre" readonly required');?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3">Fecha</label>
                                <div class="col-md-4">
									<?= form_input('fecha', date("Y-m-d"),
                                    'id=""  class="form-control"
                                    placeholder="Fecha" readonly required');	?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <h4 class="form-section">Datos del vehículo</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3">Modelo</label>
                                <div class="col-md-9">
                                    <?= form_input('modelo', $info[0]->sev_marca,
                                            'id="modelo"  maxlength="25" class="form-control"
                                            placeholder="Modelo" readonly required');?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3">Año</label>
                                <div class="col-md-9">
                                    <?= form_input('ano', $info[0]->sev_modelo,
                                            'id="ano"  maxlength="25" class="form-control"
                                            placeholder="Año" readonly required');?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3">Color</label>
                                <div class="col-md-9">
                                    <?= form_input('color', $info[0]->sev_color,
                                            'id="color"  maxlength="25" class="form-control"
                                            placeholder="Color" readonly required');?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <!--/row-->
                    <h4 class="form-section">Servicios</h4>
                    <div class="row">
                    	<div class="col col-md-12">
                            <table class="table table-striped table-bordered table-hover" id="" >
                                <thead>
                                    <tr style="font-weight:bold">
                                        <th style="text-align:center; width:50px;"></th>
                                        <th style="text-align:center; padding-bottom:6px; width:50px;"><b>Cantidad</b></th>
                                        <th style="text-align:center; padding-bottom:6px; width:250px;"><b>Servicio</b></th>
                                        <th style="text-align:center"><b>Descripción</b></th>
                                        <th style="text-align:center; width:100px;"><b>Total Refacciones</b></th>
                                        <th style="text-align:center; width:100px;"><b>Total <br />Mano de Obra</b></th>
                                        <th style="text-align:center; padding-bottom:6px; width:100px;"><b>Total</b></th>
                                        <th style="text-align:center; padding-bottom:6px; width:50px;"><b>Autorizo?</b></th>
                                        <th style="text-align:center; padding-bottom:6px; width:50px;"><b>Existencia?</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php for($i = 0; $i < 15; $i++) { ?>
                                    <tr>
                                        <td style="text-align:center; padding-top:10px; width:50px;">
                                    		<span><?= (1+$i);?></span>
                                        </td>
                                        <td>
                                    		<?= form_input('cantidad[]', '',
                                            'id="cantidad"  maxlength="3" class="form-control" ');?>
                                        </td>
                                        <td>
											<?= form_dropdown('servicio[]', $servicios, '0',
													'id="selectError" class="select2_category form-control"
													data-rel="chosen"');	?>
										</td>
                                        <td>
                                    		<?= form_input('descripcion[]', '',
                                            'id="descripcion" class="form-control"
                                            placeholder="Descripción"');?>
                                        </td>
                                        <td>
                                    		<?= form_input('refacciones[]', '',
                                            'id="refacciones" class="form-control calculate"');?>
                                        </td>
                                        <td>
                                    		<?= form_input('mano_obra[]', '',
                                            'id="mano_obra" class="form-control calculate"');?>
                                        </td>
                                        <td>
                                    		<?= form_input('total[]', '',
                                            'id="total"  class="form-control" readonly');?>
                                        </td>
                                        <td style="text-align:center; width:50px;">
                                            <label class="checkbox-inline">
                                            <input type="checkbox" name="autorizo[]" value="<?php echo $i; ?>-1">
                                            </label>
                                        </td>

                                        <td style="text-align:center; width:50px;">
                                            <label class="checkbox-inline">
                                            <input type="checkbox" name="existencia[]" value="<?php echo $i; ?>-1">
                                            </label>
                                        </td>

                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                    </div>

                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-success">Crear</button>
                                <a href="<?= base_url(); ?>cotizaciones">
                                <button type="button" class="btn btn-default">Cancelar</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>

            <?php echo form_close(); ?>
            <!-- END FORM-->
        </div>
    </div>
<!-- END EXAMPLE TABLE PORTLET-->

<?php $this->load->view('globales/footer'); ?>
<?php $this->load->view('usuario/js/script');?>
<script type='text/javascript'>

	$(document).ready(
		function() {
			$(".calculate").on('focusout',
							   function() {
								    $parent = $(this).parent().parent();
									$refacciones = $parent.find("#refacciones").val();
									$mano_obra = $parent.find("#mano_obra").val();
									$total = $parent.find("#total");
									if(isNaN(parseFloat($refacciones))) $refacciones = 0;
									else $refacciones = parseFloat($refacciones);
									if(isNaN(parseFloat($mano_obra))) $mano_obra = 0;
									else $mano_obra = parseFloat($mano_obra);
									sum = $refacciones + $mano_obra;

									$total.val(sum);
								   });



$('.forma').keyup(function(e) {
  return e.which !== 13
});
		} );
</script>
