<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='control';$this->load->view('globales/menu',$menu); ?>
<form name="form1" method="get" action="<?php echo base_url();?>control/">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="fa fa-money"></i>
				<a href="#">Cotizaciones</a>
			<i class="fa fa-angle-right"></i>
		</li>
        
		<li class="pull-right" style="text-align:right; margin-left:15px;">
			<Select name="asesor" onchange='this.form.submit()'>
				<option value="0" <?php if($asesor==0){echo 'selected="selected"';}?>>Todos</option>
				<?php foreach($usuario as $us) {
				if($us->rol_idRol=='3' && $us->sus_workshop==$taller) {
				?>
				<option value="<?php echo $us->sus_idUser?>" <?php if($asesor==$us->sus_idUser){echo 'selected="selected"';}?>><?php echo $us->sus_name.' '.$us->sus_lastName;?></option>
				<?php }} ?>
			</select>
		</li>
        <li class="pull-right" style="text-align:right; margin-left:15px;">
    <?php if($_SESSION['sfrol']<=2) {?>
    
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    <?php } ?>
    </li>
		<li class="pull-right" >
            <div  class="input-group input-medium date " id="date-pickerbb" data-date-format="yyyy-mm-dd" style="text-align:right;">
            <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
            Fecha: <?php echo $fecha;?> <i style="cursor:pointer; float:right; margin-left:5px;" class="fa fa-calendar  input-group-btn"></i>
            </div>
		</li> 
	</ul>
</form>
<!-- END PAGE TITLE & BREADCRUMB-->
<?php
 $name=array('Monday'=>'Lunes','Tuesday'=>'Martes','Wednesday'=>'Miercoles','','Thursday'=>'Jueves','Friday'=>'Viernes','Saturday'=>'Sabado');
	
date_default_timezone_set('America/Tijuana');
$hor=date('H:i');
list($h,$m)=explode(':',$hor);
if($h<12){$ty='AM';}else{$ty='PM';}
$hora=$hor.' '.$ty;	
		
function tecnico($array,$id){
	$tec='';
	foreach($array as $ar){
		if($ar->sus_idUser==$id){
			$nom = substr($ar->sus_name,0, 1); 
			$ape = substr($ar->sus_lastName,0, 1);
			$tec=$nom.$ape;
			}
		}
		
		return $tec;
	}
	
	function decimal( $value ) {
    if ( strpos( $value, "." ) !== false ) {
        return true;
    }
    return false;
}
	
	function numCarriOver($array,$date){
$sum=0;
$arre=array();
	foreach($array as $ar){
		if($ar->seo_date==$date.' 00:00:00'){
	
			}
			else{
			$sum++;	
			$arre[]=$ar->seo_idServiceOrder;
				}
		}
		
		return array($sum,$arre);
	}

		function revision($array,$id){
$ser='';
$suma=0;
$lser='';
$como='';
$comp='';
foreach($array as $ar){
	if($ar->ssr_IDNULL==$id){
		if($ar->ssr_tipo=='master'){
		$ser=$ar->set_name;}
		$suma+=$ar->ser_approximate_duration;
		$lser.=$ar->set_name.', ';
		}
	}	
	return $lista=array($lser,$ser,$suma,$como,$comp);
	}
	
	function suma($ve,$mo,$array){
$sum=0;
foreach($array as $ar){if($ar->ser_vehicleModel==$ve && $ar->ser_serviceType==$mo){$sum=$ar->ser_approximate_duration;}}
return $sum;}

	
	function horasCitas($lista,$serv){
$suma=0;	
foreach($lista as $ar){
  $suma+=suma($ar->app_vehicleModel,$ar->app_service,$serv).',';
 
	}	
	
	return $suma;
	
	}
	
	
	
	
		function publicodia($arr,$ido){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->ssr_IDNULL==$ido && ($ar->ssr_venta=='venta' || $ar->ssr_venta=='adicional'))$sum+=$ar->ser_approximate_duration;
			
			}
		return $sum;
		}
		
			 function listaOrden($arr,$da,$usuario){
		$cat=array();	
		foreach($arr as $ar){
			
			$nom = substr($ar->sus_name,0, 1); 
			$ape = substr($ar->sus_lastName,0, 1);
			$tecnico = tecnico($usuario,$ar->seo_technicianTeam);
			list($dia,$entrada)=explode(' ',$ar->seo_timeReceived);
			    if($ar->piz_fecha==$da and $ar->ssr_venta=='venta'){
				$cat[]=array(
				'tipo'=>$ar->ssr_tipo,
				'servicio'=>$ar->set_name,
				'codigo'=>$ar->set_codigo,
				'duracion'=>$ar->ser_approximate_duration,
				'torre'=>$ar->seo_tower,
				'venta'=>$ar->ssr_venta,
				'ido'=>$ar->ssr_IDNULL,
				'orden'=>$ar->seo_serviceOrder,
				'cliente'=>$ar->sec_nombre,
				'telefono'=>$ar->sec_cel,
			    'marca'=>$ar->sev_marca.' '.$ar->sev_sub_marca.' '.$ar->sev_version.' '.$ar->sev_modelo.' '.$ar->sev_color,
			    'entrada'=>$entrada,
				'tiempo'=>$ar->seo_promisedTime,
				'asesor'=>$nom.$ape,
				'notas'=>$ar->seo_comments,
				'idorden'=>$ar->seo_idServiceOrder,
				'tecnico'=>$tecnico,
				'status'=>$ar->seo_control_sta,
				'hora'=>$ar->seo_control_hora,
				'fecha'=>$ar->seo_date,
				'tipoCita'=>$ar->app_referencia,
				'entregado'=>$ar->seo_control_entregado);	
				}
			
			}
		return $cat;
		}
	
	function numeros($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da and $ar->ssr_venta=='venta')$sum++;
			
			}
		return $sum;
		}
		
		function pro($arr){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->ser_tipo=='pro')$sum++;
			
			}
		return $sum;
		}
		
		function citas($lista){
			$num=0;
			$ccitas=count($lista);

								for($i=0; $i <$ccitas; $i++){
									$num++;
									}
									
			return $num;						
			}
			 
			 ?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row">
  <div class="col-md-12"> 
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet" style="min-width:1024px;  overflow: auto;">
      <div class="portlet-title">
        <div class="caption">
          <center>
            CONTROL DE FLUJO DE TRABAJO DIARIO
          </center>
        </div>
        <div class="actions"><i class="fa fa-print"></i><a target="_blank" style="color:white; text-align:right" href="<?php echo base_url();?>control/pdfAzul"> Imprimir</a> </div>
      </div>
      <div class="portlet-body">
        <table  width="100%;" style="min-width:1024px;" >
          <thead style="color:#3ca1ca;">
            <tr>
              <th  ></th>
              <th colspan="6" style="
border-bottom:1px solid #41abdd;
border-right:1px solid #41abdd;
border-top:2px solid #41abdd;
border-left:2px solid #41abdd;
text-align:center"> INFORMACI&Oacute;N DE LA ORDEN DE REPARACI&Oacute;N </th>
              <th colspan="4" style="border-bottom:1px solid #41abdd;border-right:1px solid #41abdd;border-top:2px solid #41abdd; text-align:center">AVALUO INICIAL </th>
              <th colspan="2" style="border-bottom:1px solid #41abdd;border-right:1px solid #41abdd;border-top:2px solid #41abdd; text-align:center">RECORDATORIO </th>
              <th style="border-bottom:1px solid #41abdd;border-right:1px solid #41abdd;border-top:2px solid #41abdd; text-align:center"> FALLA PRINCIPAL</th>
              <th rowspan="2" style="text-align:center;border-top:2px solid #41abdd; border-left:1px solid #41abdd;background-color:#b9f3ff; border-bottom:2px solid #41abdd; font-size:8px; ">TOTAL<br>
                HORAS<br>
                FACTURADAS </th>
              <th rowspan="2" style="border-bottom:1px solid #41abdd;border-left:1px solid #41abdd; border-top:2px solid #41abdd; border-right:2px solid #41abdd; text-align:center; font-size:8px;"> M<br>
                E<br>
                C<br>
                A<br>
                N<br>
                I<br>
                C<br>
                O </th>
              <th rowspan="2" style="border-bottom:1px solid #41abdd; border-top:2px solid #41abdd; border-right:2px solid #41abdd; text-align:center;font-size:8px;"> TRABAJO <br>
                TERMINADO<br>
                CONTACTO<br>
                C/CLIENTE </th>
            </tr>
            <tr style="text-align:center; font-size:11px;">
              <th style="width:10px; border-bottom:2px solid #41abdd;    "> </th>
              <th class="highlight"  style=" text-align:center;  border-bottom:2px solid #41abdd;  border-left:2px solid #41abdd;  background-color:#b9f3ff;"> No. O.R. </th>
              <th width="15px;" style="text-align:center; border-bottom:2px solid #41abdd;  border-left:1px solid #41abdd; "> R </th>
              <th style="text-align:center; border-bottom:2px solid #41abdd;  border-left:1px solid #41abdd; "> No. TORRE </th>
              <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff; border-bottom:2px solid #41abdd"> CLIENTE </th>
              <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd""> TEL. DEL CLIENTE </th>
              <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff; border-bottom:2px solid #41abdd"> MARCA/MOD </th>
              <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd; font-size:9px;"> TIEMPO <br>
                DE<br>
                ENTRADA </th>
              <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff; border-bottom:2px solid #41abdd; font-size:9px; "> TIEMPO <br>
                PROMETIDO </th>
              <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd"> HORAS<BR>
                EN O.R. </th>
              <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff;border-bottom:2px solid #41abdd "> PQT-MENU </th>
              <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd"> STATUS </th>
              <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff;border-bottom:2px solid #41abdd ">ESCRITO<br>
                POR </th>
              <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd"> NOTAS DEL TRABAJO </th>
            </tr>
          </thead>
          <tbody>
            <?php 
								$hrsrev=0;
								$rr=0;
								$cc=0;
								$sum=0;
								$i=0;
								$noOS=0;
								$color='';
								$oarray=array();
								$totalHrsFacturadas=0;
								$numb=numeros($os,$fecha);
								for($dd=0; $dd< $numb; $dd++){
								$orden=listaOrden($os,$fecha,$usuario);
								$uu=0;	
								if($orden[$dd]['tipo']=='pro'){$cod=$orden[$dd]['servicio'];}else{$cod=$orden[$dd]['codigo'];}
								$sum+=$orden[$dd]['duracion'];	
								
								$cc=$dd - 1;
								if($dd==0){
									$torre=$orden[$dd]['torre'];
									$rr++;
									}
								elseif($orden[$cc]['torre']==$orden[$dd]['torre']){
								$torre='';
								}else{
									$torre=$orden[$dd]['torre'];
									$rr++;
									}
								
								if($torre==''){}else{
									
									$revision=revision($os,$orden[$dd]['idorden']);	
									$publicodia=publicodia($os,$orden[$dd]['idorden']);	
								if($orden[$dd]['fecha']==$fecha.' 00:00:00'){
									if($orden[$dd]['tipoCita']==2){$color='black';}
								if($orden[$dd]['tipoCita']!=2){$color='blue';}
									}else{$color='red';}
								if($orden[$dd]['entregado']==1){$carri='hilay';}else{$carri='';}								 	
								$i++;
								?>
            <tr  style=" color:<?php echo $color; ?>;">
              <?php if($i==5 || $i==10 || $i==15 || $i==20 || $i==25 || $i==30 || $i==35 || $i==40 || $i==45 || $i==50){ $sty='border-bottom:2px solid #41abdd; ';}else{$sty='';}?>
              <td style="width:10px;   <?php echo $sty;?> "><?php
                                if($i==5 || $i==10){echo $i;}
								?></td>
              <td class="highlight dbso" id="<?php echo $orden[$dd]['idorden'];?>"  style=" cursor:pointer; text-align:center; background-color:#b9f3ff; <?php if($sty==''){echo 'border-left:2px solid #41abdd; border-bottom:1px solid #41abdd;';}else{echo 'border-left:2px solid #41abdd; border-bottom:2px solid #41abdd;';}?>"><p id="mi" class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['orden']; 
?> </p></td>
              <td style="border-left:1px solid #41abdd; text-align:center; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > P </p></td>
              <td style="border-left:1px solid #41abdd; text-align:center; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" >
                  <?php 
echo $orden[$dd]['torre'];
?>
                </p></td>
              <td style="border-left:1px solid #41abdd; background-color:#b9f3ff; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['cliente'];?> </p></td>
              <td style="border-left:1px solid #41abdd; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['telefono'];?> </p></td>
              <td style="border-left:1px solid #41abdd; background-color:#b9f3ff; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['marca'];?> </p></td>
              <td style="border-left:1px solid #41abdd; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['entrada'];?> </p></td>
              <td style="border-left:1px solid #41abdd; background-color:#b9f3ff;  <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['tiempo'];?> </p></td>
              <td style="border-left:1px solid #41abdd; text-align:center; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $revision[2]; $hrsTrab+=$revision[2]; 

$hrsrev=round($revision[2], 1);
						$decimal=decimal($hrsrev);
						if($decimal==true){
							list($hn,$hd)=explode('.',$hrsrev);
							if($hd > 5){$hrsrev=$hn + 1;}
							if($hd == 5){$hrsrev; }
							if($hd < 5){$hrsrev=$hn + 0.5; }
							
							}
						else{ }


if($orden[$dd]['orden']==''){$osorden='...';}else{$osorden=$orden[$dd]['orden'];}
$oarray[$osorden]=$hrsrev;
?> </p></td>
              <td style="border-left:1px solid #41abdd; background-color:#b9f3ff;  <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['servicio'];?> </p></td>
              <td style=" text-align:center; border-left:1px solid #41abdd; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><div class="loading<?php echo $orden[$dd]['idorden'];?>" style="color:green; display:none">Guardando...</div>
                <div  class="btn-group" style="width:30px; height:19px;">
                  <button class="btn  btn-xs dropdown-toggle" style=" width:100%;height:19px; background-color:white"  type="button" data-toggle="dropdown">
                  <div class="respuesta<?php echo $orden[$dd]['idorden'];?>">
                    <p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" >
                      <?php

if($orden[$dd]['status']==1 || $orden[$dd]['status']==0){echo ' / ';}
if($orden[$dd]['status']==2){echo ' X ';}
if($orden[$dd]['status']==3){echo '(X)';}
?>
                    </p>
                  </div>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li style="cursor:pointer" id="<?php echo $orden[$dd]['idorden'];?>_1" class="saveStatus" > / </li>
                    <li style="cursor:pointer" id="<?php echo $orden[$dd]['idorden'];?>_2" class="saveStatus" > X </li>
                    <li style="cursor:pointer" id="<?php echo $orden[$dd]['idorden'];?>_3" class="saveStatus"> (X) </li>
                  </ul>
                </div></td>
              <td style="border-left:1px solid #41abdd; text-align:center; background-color:#b9f3ff;  <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['asesor'];?></p></td>
              <td style="border-left:1px solid #41abdd; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['notas'];?></p></td>
              <td style="border-left:1px solid #41abdd;background-color:#b9f3ff; text-align:center;  <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $publicodia; $totalHrsFacturadas+=$publicodia;?></p></td>
              <td style="border-left:1px solid #41abdd; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" > <?php echo $orden[$dd]['tecnico'];?> </p></td>
              <td class="savehora" id="<?php echo $orden[$dd]['idorden'];?>" style=" cursor:pointer;border-left:1px solid #41abdd; background-color:#b9f3ff;  border-right:2px solid #41abdd; <?php if($sty==''){echo 'border-bottom:1px solid #41abdd;';}else{echo $sty;}?>"><div class="loadingH<?php echo $orden[$dd]['idorden'];?>" style="color:green; display:none">Guardando...</div>
                <div id="hora<?php echo $orden[$dd]['idorden'];?>">
                  <p class="mi<?php echo $orden[$dd]['idorden'];?> <?php echo $carri;?>" >
                    <?php if($orden[$dd]['hora']){echo $orden[$dd]['hora'];}
									else{
                                    echo 'Cargar Hora...';} ?>
                  </p>
                </div></td>
            </tr>
            <?php $noOS++; }} ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php  
						if($hrsTrab>0){ $hrsTrab=$hrsTrab + 0.5; 
						$hrsTrab=round($hrsTrab, 1);
						$decimal=decimal($hrsTrab);
						if($decimal==true){
							list($hn,$hd)=explode('.',$hrsTrab);
							if($hd > 5){$hrsTrab=$hn + 1;}
							if($hd == 5){$hrsTrab;}
							if($hd < 5){$hrsTrab=$hn + 0.5;}
							
							}
						else{ }
						
						
						}?>
  <div class="col-md-12"> 
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet">
      <div class="portlet-body">
        <div style=" font-size:20px; margin-bottom:5px; color:#41abdd;">
          <center>
            VENTAS DIARIAS DE MANO DE OBRA Y CONTROL DE TIEMPO GENERAL
          </center>
        </div>
        <div class="table-responsive">
          <table width="100%" style="color:#41abdd; " cellspacing="0" cellpadding="0" class="">
            <tbody>
              <tr height="25px;">
                <td class="highlight"  style="font-weight:bold">Dia:</td>
                <td class="hidden-xs" style="border-bottom:1px solid #41abdd; text-align:center; color:black"><?php 
$dname = date('l', strtotime($fecha));
echo $name[$dname];?></td>
                <td width="10px" style="font-weight:bold">Fecha:</td>
                <td  style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php echo $fecha;?></td>
              </tr>
              <tr height="35px;">
                <td class="highlight" width="20px" style="font-weight:bold">Equipo:</td>
                <td class="hidden-xs" style="border-bottom:1px solid #41abdd; text-align:center; color:black"><?php if($asesor==0){echo'Todos';} ?></td>
                <td width="20px" style="font-weight:bold">Asesor:</td>
                <td  style="border-bottom:1px solid #41abdd; text-align:center; color:black"><?php if($asesor==0){echo'Todos';}else{echo $nasesor;} ?></td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="10px" style="font-weight:bold">Mecanicos<br>
                  Presentes:</td>
                <td class="hidden-xs" style="border-bottom:1px solid #41abdd; text-align:center; color:black;"><?php echo $cmec=count($mecanicos)?></td>
                <td width="10px" style="font-weight:bold;font-size:18px;">X:</td>
                <td  style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php echo $capacidad[0]->woc_weekdays_service_hours;?></td>
                <td width="10px" style="font-weight:bold">Horas <br>
                  Reloj</td>
                <td width="10px" style="font-weight:bold; font-size:18px;">=</td>
                <td width="105px" style="font-weight:bold">Hrs. Totales<br>
                  de Trabajo:</td>
                <td  style="border-bottom:1px solid #41abdd; color:black; text-align:center;"><?php echo $multi=$cmec * $capacidad[0]->woc_weekdays_service_hours;?></td>
                <td width="10px" style="font-weight:bold; font-size:18px;">X:</td>
                <td  style="border-bottom:1px solid #41abdd; text-align:center; color:black"><?php echo $capacidad[0]->woc_productivity_factor;?></td>
                <td width="10px" style="font-weight:bold; font-size:18px;">%</td>
                <td width="15px" style="font-weight:bold">Productividad<br>
                  Objetivo</td>
                <td width="10px" style="font-weight:bold; font-size:18px;">=</td>
                <td  style="border-bottom:1px solid #41abdd; color:black; text-align:center;"><?php echo $multi * $capacidad[0]->woc_productivity_factor;?></td>
                <td width="110px" style="font-weight:bold">Horas<br>
                  disponibles hoy</td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight"  style="font-weight:bold">O.R. Pendientes que se deben de trabajar hoy :</td>
                <td width="25%"  style="border-bottom:1px solid #41abdd; color:black; text-align:center;"><?php $carriOver=numCarriOver($numCarriOver,$fecha);echo $carriOver[0];?></td>
                <td  style="font-weight:bold">Hrs. para Trabajar:</td>
                <td   width="20%"style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php 
$conCO=count($carriOver[1]);
$sumCar=0;
$sumCarT=0;
for($e=0; $e<$conCO; $e++){
	$sumCar=revision($os,$carriOver[1][$e]);
	$sumCarT+=$sumCar[2];
	}
echo $sumCarT;
?></td>
                <td  style="font-weight:bold">Aprox.</td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="190px" style="font-weight:bold">Citas Confirmadas para Hoy:</td>
                <td width="40%"  style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php echo citas($lista);?></td>
                <td  style="font-weight:bold">Hrs. para Trabajar:</td>
                <td width="20%"   style="border-bottom:1px solid #41abdd; color:black; text-align:center;"><?php echo $horas=horasCitas($lista,$servicios)?></td>
                <td  style="font-weight:bold">Aprox.</td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="320px" style="font-weight:bold">El objetivo de venta de paquetes para hoy es de:</td>
                <td width="40%"  class="hidden-xs" style="border-bottom:1px solid #41abdd;"><input type="text" style="border:1px solid white; width:300px;" name="descripcion" class="saveControl" 
value="<?php if($res)echo $res[0]->mno_descripcion;?>" id="ase-fecha" ></td>
                <td  style="font-weight:bold">Paquete completo</td>
              </tr>
            </tbody>
          </table>
          <br>
          <hr>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="160px" style="font-weight:bold">Hrs. Fact. Producidas hoy:</td>
                <td  class="hidden-xs" style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php echo $totalHrsFacturadas;?></td>
                <td width="25px" class="hidden-xs" style="border-bottom:1px solid #41abdd;">/</td>
                <td  class="hidden-xs" style="border-bottom:1px solid #41abdd; text-align:center; color:black;"><?php echo $resOS=$noOS - $carriOver[0];?></td>
                <td width="90px" style="font-weight:bold">O.R. Cerradas=</td>
                <td  class="hidden-xs" style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php  if($resOS==0)echo 0; else echo  number_format( $totalHrsFacturadas / $resOS, 1, '.', '');?></td>
                <td width="110px" style="font-weight:bold">Prom. Hrs./O.R.</td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="130px" style="font-weight:bold">Citas cumplidas hoy:</td>
                <td  class="hidden-xs" style="border-bottom:1px solid #41abdd; text-align:center; color:black"><?php echo citas($lista);?></td>
                <td width="145px;"  style="font-weight:bold">Ventas de Paquetes hoy:</td>
                <td   style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php echo pro($os);?></td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="140px" style="font-weight:bold">No. de Autos recibidos:</td>
                <td  class="hidden-xs" style="border-bottom:1px solid #41abdd; color:black; text-align:center"><?php echo $noautos[0]->numero;?></td>
                <td width="110px" style="font-weight:bold">No. de pendientes</td>
                <td   style="border-bottom:1px solid #41abdd; text-align:center; color:black"><?php echo $carriOver[0];?></td>
                <td width="110px" style="font-weight:bold">con m&aacute;s de 3 dias</td>
                <td  width="25px" style="border-bottom:1px solid #41abdd;"></td>
              </tr>
            </tbody>
          </table>
          <table width="100%" style="color:#41abdd; margin-top:12px;" class="">
            <tbody>
              <tr>
                <td class="highlight" width="50px" style="font-weight:bold">Conteo de O.R. Publico:</td>
                <td width="25px" class="hidden-xs" style="border-bottom:1px solid #41abdd; color:black; text-align:center;"><?php echo $rr;?></td>
                <td width="50px" style="font-weight:bold">Garantia:</td>
                <td  width="25px" style="border-bottom:1px solid #41abdd; text-align:center; color:black"></td>
                <td width="50px" style="font-weight:bold">Interno:</td>
                <td  width="25px" style="border-bottom:1px solid #41abdd;"></td>
                <td width="50px" style="font-weight:bold">Total:</td>
                <td  width="25px" style="border-bottom:1px solid #41abdd;"></td>
              </tr>
            </tbody>
          </table>
          <br>
          <br>
        </div>
      </div>
    </div>
  </div>
  <?php $hrsTrab;?>
  <div class="col-md-12">
    <div class="portlet">
      <div class="portlet-body">
        <div style=" font-size:20px; margin-bottom:5px; color:#41abdd;">
          <center>
            BANCO DE TIEMPO
          </center>
        </div>
        <div class="table-responsive">
          <table width="100%" class="" style="border-left:1px solid #41abdd;">
            <tbody>
              <tr>
                <?php
function checar($tot,$array){
$osd='';	
foreach($array as $ar=>$res){
	if($tot==$res){$osd=$ar;}
	
	}
	return $osd;	
	}
$x=0;
$do=0;
$y=.5;
$vuelta=.5;

if($asesor==0){
$max=100.50;
$limite=301;
$tr=12;
$vuno=6;
$vdos=11;
$vtre=5;
$vcua=3;
}else{
$max=33.5;
$tr=4;
$limite=101;
$vuno=2;
$vdos=3;
$vtre=4;
$vcua=3;
}

$suma=0;
foreach($oarray as $arr=>$res){
	
	
		$cvinisuma=round($res, 1);	
		$decimal=decimal($cvinisuma);
						if($decimal==true){
							list($hn,$hd)=explode('.',$cvinisuma);
							if($hd > 5){$cvinisuma=$hn + 1;}
							if($hd == 5){$cvinisuma;}
							if($hd < 5){$cvinisuma=$hn + 0.5;}
							
							}
						else{ }		
			

	$suma+=$cvinisuma;
	}
$suma;
$inicio=$max - $suma;

$ddx=$hrsTrab + $inicio;
$cvinicio=$inicio;
$datarray=array();
$cvuelta=0;
$cvinisuma=0;

foreach($oarray as $arr=>$res){
	
	
		$cvinisuma=round($res, 1);	
		$decimal=decimal($cvinisuma);
						if($decimal==true){
							list($hn,$hd)=explode('.',$cvinisuma);
							if($hd > 5){$cvinisuma=$hn + 1;}
							if($hd == 5){$cvinisuma;}
							if($hd < 5){$cvinisuma=$hn + 0.5;}
							
							}
						else{ }		
			
	$datarray[$arr]=$cvinicio+=$cvinisuma;
	$suma+=$cvinisuma;
		
	$cvuelta++;
	}
$suma;
$styTop='border-top:1px solid #41abdd;';
$styRig='border-right:1px solid #41abdd;';
$bordertop='';
$borderbottom='';
$styBot='';
$check='';
for ($i=1; $i<$limite; $i++){ 
							 
$val=$y; 
$tot=$val + $do;
if($tot >=$inicio){
	
if($tot <= $ddx){
    $bgColor='background-color:#b9f3ff;';
        }else{ $bgColor='';}
}else{ $bgColor='';}
$check=checar($tot,$datarray);
if($check!==''){$borderbottom='border-bottom:1px solid #41abdd;';}else{
if($tot==150){$borderbottom='border-bottom:1px solid #41abdd;';}else{$borderbottom='';}
	
}
if($tot==$inicio){$bordertop='border-top:1px solid #41abdd;';}else{$bordertop='';}

?>
                <td width="29px;"  style="border-left:1px solid #41abdd; text-align:right; <?php echo $bgColor.' '.$styTop.' '.$styRig.' '.$styBot.' '.$bordertop.' '.$borderbottom; ?>"><?php
if($tot >=$inicio){echo '<div style=" font-weight:bold; float:left; color:green; ">'.checar($tot,$datarray).'</div>';
if($check!==''){echo'-';}
}
echo  '<div style=" float:right">'.number_format($tot, 1, '.', '').'</div>';
?></td>
                <?php 								
if($x==$tr){
if($tr==$vcua){echo '<td style="border-right:1px solid #41abdd; '.$styBot.'"></td>';}	
echo '</tr><tr>';
$x=0; $do=0; 
$vuelta=$vuelta+.5;
$y=$vuelta;
if($vuelta>$vuno){$tr=$vdos;$styRig='border-right:1px solid #41abdd;';}
if($vuelta<$vtre){$styRig='border-right:1px solid #41abdd;';}
if($vuelta==1){$styTop='';$styRig='border-right:1px solid #41abdd;';}
if($vuelta==12){$styBot='border-bottom:1px solid #41abdd;';}




}else{echo '';$x++; $do=$do +11; $y=$y+1;}
}




?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="portlet">
      <div style="font-size:11px; text-align:center; margin-top:5px; margin-bottom:5px;" class="caption"> CITAS PARA HOY</div>
      <div class="portlet-body">
        <div class="table-responsive">
          <table width="100%" class="table table-hover ">
            <tbody>
              <tr style="color:#41abdd; ">
                <td colspan="4" style=" font-size:22px; padding-left:10px; border-left:2px solid #41abdd;border-top:2px solid #41abdd;">CITAS PARA HOY</td>
                <td colspan="2" style="border-top:2px solid #41abdd;"> ORDENES DE REPARACI&Oacute;N<br>
                  ESCRITAS CON ANTERIORIDAD<br>
                  PARA CITAS CONFIRMADAS </td>
                <td style="border:2px solid #41abdd; border-bottom:none">HORAS</td>
              </tr>
              <tr style="color:#41abdd; font-weight:bold; ">
                <td width="150px" style="border-left:2px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd;" > Cliente </td>
                <td width="100px;" style="border-left:1px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd;" class="hidden-xs"> Tel. </td>
                <td width="100px;" style="border-left:1px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd;"  > A&ntilde;o/Mod </td>
                <td style="border-left:1px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd;" > Trabajo </td>
                <td style="border-left:1px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd;" > Horas </td>
                <td style="border-left:1px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd;" > Descripci&oacute;n. </td>
                <td style="border-left:1px solid #41abdd;border-top:1px solid #41abdd;border-bottom:1px solid #41abdd; border-right:2px solid #41abdd;" > Confirmada </td>
              </tr>
              <?php 
								$ccitas=count($lista);

								for($i=0; $i <$ccitas; $i++){
								$revision=revision($os,$lista[$i]->seo_idServiceOrder);	
									?>
              <tr>
                <td style="border-left:2px solid #41abdd;border-bottom:1px solid #41abdd;" class="highlight"><?php echo $lista[$i]->app_customerName.' '.$lista[$i]->app_customerLastName;?></td>
                <td style="border-left:1px solid #41abdd;border-bottom:1px solid #41abdd;" class="hidden-xs"><?php echo $lista[$i]->app_customerTelephone;?></td>
                <td style="border-left:1px solid #41abdd;border-bottom:1px solid #41abdd;" ><?php echo $lista[$i]->vem_name.' '.$lista[$i]->app_vehicleYear;?></td>
                <td style="border-left:1px solid #41abdd;border-bottom:1px solid #41abdd;" ><?php echo $revision[1]; ?></td>
                <td style="border-left:1px solid #41abdd;border-bottom:1px solid #41abdd; text-align:center" class="highlight"><?php echo $revision[2]; ?></td>
                <td style="border-left:1px solid #41abdd;border-bottom:1px solid #41abdd;"  class="hidden-xs"><?php echo $lista[$i]->seo_comments;?></td>
                <td style="border-left:1px solid #41abdd;border-bottom:1px solid #41abdd; text-align:center; border-right:2px solid #41abdd;" ><?php if ($lista[$i]->app_confirmed==0){echo'No';}else{echo'Si';};?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  
  <!-- END SAMPLE TABLE PORTLET--> 
</div>
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('control/js/script');?>
<script>
jQuery(document).ready(function() {
	
	$('.saveStatus').live('click',function(){
		var data=$(this).attr('id');
		var elem = data.split('_');
		ido=elem[0];
		idu=elem[1];
		$('.loading'+ido+'').show();
		
		$.ajax({
	  url:"<?php echo base_url();?>orden/updateControl/?data="+data+"",
	  success:function(result){
		    $('.respuesta'+ido+'').html(result);
           $('.loading'+ido+'').hide();
		   
                              }
         });
		});
	
	
	$('.dbso').live('dblclick',function(){
		var ido=$(this).attr('id');
		$.ajax({
	  url:"<?php echo base_url();?>orden/updateEntregado/?valor=1&id="+ido+"",
	  success:function(result){$('.mi'+ido+'').addClass('hilay');}
         });
		 });

	
	$(function () {
	 $('#date-pickerbb').datepicker({ 
	language: 'es',
	isRTL: false,
         autoclose:true
                
	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });

	 
	});
	
	
	$('.savehora').on('click',function(){
		
var id=$(this).attr('id');
$('.loadingH'+id+'').show();
var valor="<?php echo $hora?>";

 $.ajax({
	  url:"<?php echo base_url();?>orden/updateControlHora?id="+id+"&value="+valor+"",
	  success:function(result){
		  $('.loadingH'+id+'').hide();
         $('#hora'+id+'').html('<?php echo $hora?>');     
                          
						      }
         });

});



	$('.saveControl').live('keyup',function(){
		
var id=$(this).val();
$('.loadingText').show();
var asesor="<?php echo $asesor?>";

 $.ajax({
	  url:"<?php echo base_url();?>control/updateManodeObra?valor="+id+"&asesor="+asesor+"&fecha=<?php echo $fecha;?>&taller=<?php echo $taller;?>",
	  success:function(result){
		  $('.loadingText').hide();    
                          
						      }
         });

});


});
</script>