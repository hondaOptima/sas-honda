<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Honda Optima</title>
		<style type="text/css">
		body{margin:0;padding:0;}
		.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
		table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
		div{line-height:18px;color:#202020;}
		img{display:block;}
		td,tr{padding:0;}
		ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;}
		li{background-image: url(iconlist.jpg);
		background-repeat: no-repeat;
		background-position: 0px 5px;
		padding-left: 20px; line-height:24px; }
		a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
		.headertbl{border-bottom:1px solid #E1E1E1;}
		.footertbl{border-top:1px solid #E1E1E1;}
		.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
		.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
		.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
		.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
		.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
		.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
		.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
		.precio { font-size:14px; color:#0f0f0f;}
		.precio strong { color:#333; font-weight:800;}
		.invert div, .invert .h{color:#F4F4F4;}
		.invert div a{color:#FFFFFF;}
		.line{border-top:1px dotted #D1D1D1;}
		.logo{border-right:1px dotted #D1D1D1;}
		.small div{font-size:10px; line-height:16px;}
		.btn{margin-top:10px;display:block;}
		.btn img,.social img{display:inline;}
		div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
		.templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
		.templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
		*{font-size:12pt;}
	</style>
	</head>
	<body bgcolor="#F1F1F1">
		<table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
			<tr>
				<td align="center">
					<table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1" height="auto">
						<tr>
							<td valign="top" align="center">
								<img src="http://hcrm.gpoptima.net/img/encabezadohonda.jpg" width="100%">
							</td>
						</tr>
					</table>
					<table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td valign="top" align="center">
								<table width="570" cellpadding="0" cellspacing="0">
									<tr>
										<td width="" valign="top" align="left">
											<br>
											<table width="100%">
												<tr>
													<td width="40%">
														<div style="text-align: justify;"><b><?= $nombre ?></b></div>
													</td>
													<td width="60%" style="text-align:right">
														<div><?= $ciudad ?>  B.C. <?= $hoy ?><div>
													</td>
												</tr>
											</table>
											<br><br>
											<div class="msg">
												Saludos <b><?= $nombre ?></b>. Le avisamos que su cliente <b><?= $nombre_cliente ?></b> se encuentra en estos momentos en el área de servicio.<br><br><br>¡Por favor, considere regalarle un agradable saludo!
                                                <br><br><br><br><b>Auto: </b><?= $auto ?><br><br><b>Fecha de entrega de auto:</b> <?= $fecha_auto_entrega ?>
											</div>
										</td>
									</tr>
								</table>
								<table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
									<tr>
										<td valign="top" align="center">
											<a href="https://www.facebook.com/hondaoptima">
												<img src="http://hcrm.gpoptima.net/img/piehonda.jpg" width="100%">
											</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
