<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>servicio">Servicios</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>tiposervicio">Tipos de Servicio</a> <i class="fa fa-angle-right"></i> </li>
</ul>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-user"></i>Tipos de Servicios </div>
        <div class="actions"> <a href="<?php echo base_url();?>tiposervicio/crear" class="btn btn-success"><i class="fa fa-pencil"></i> Añadir Tipo de Servicio</a> </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
              <th> <b>#</b> </th>
              <th> <b>Orden</b> </th>
              <th> <b>Nombre</b> </th>
              <th> <b>Codigo</b> </th>
              <th> <b>Estatus</b> </th>
              <th> <b>Control</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php if($tipos): ?>
            <?php
			$i=1;
                            foreach($tipos as $tipo){ ?>
            <tr class="odd gradeX">
            <td><?php echo $i;?></td>
            <td><?php echo $tipo->set_orden; ?></td>
              <td><?php echo $tipo->set_name; ?></td>
              <td><?php echo $tipo->set_codigo; ?></td>
              <td><?php if($tipo->set_isActive==1){ $status = 'Activo'; $label='success'; $action='desactivar/'; }
                                                   else { $status = 'Inactivo'; $label='danger'; $action='activar/'; }?>
                <a href="<?php echo base_url().'tiposervicio/'.$action.$tipo->set_idServiceType ?>"
                                            onClick=""> <span class="label label-sm label-<?php echo $label?>"> <?php echo $status?> </span> </a></td>
              <td><a class="btn btn-default btn-xs" href="<?php echo base_url().'tiposervicio/editar/'.$tipo->set_idServiceType; ?>"   > Editar </a></td>
            </tr>
            <?php $i++;} ?>
            <?php endif ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('tiposervicio/js/script');?>
