<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-wrench"></i> <a href="<?php echo base_url();?>tiposervicio">Tipos de Servicio</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-edit"></i> <a href="<?php echo base_url();?>tiposervicio/crear">Nuevo</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-reorder"></i>Nuevo Tipo de Servicio </div>
  </div>
  <div class="portlet-body form"> 
    <!-- BEGIN FORM--> 
    
    <?php echo form_open('tiposervicio/crear','class="form-horizontal"'); ?>
    <div class="form-body">
      <div class="row">
       <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Orden</label>
            <div class="col-md-9">
              <?php  echo form_input('orden',set_value('orden'), 
																		'id="orden" class="form-control"
																		placeholder="Orden" ');?>
            </div>
          </div>
        </div>
      
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Nombre</label>
            <div class="col-md-9">
              <?php  echo form_input('nombre',set_value('nombre'), 
																		'id="nombre" class="form-control"
																		placeholder="Nombre" required');?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Codigo</label>
            <div class="col-md-9">
              <?php  echo form_input('codigo',set_value('codigo'), 
																		'id="codigo" class="form-control"
																		placeholder="Codigo" required');?>
            </div>
          </div>
        </div>
        
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Tipo de Servicio</label>
            <div class="col-md-9">
              <select name="tipo">
               <option value="pro">Programados</option>
               <option value="nopro">No Programados</option>               
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-success">Añadir</button>
            <a href="<?php echo base_url();?>pizarron">
            <button type="button" class="btn btn-default">Cancelar</button>
            </a> </div>
        </div>
        <div class="col-md-6"> </div>
      </div>
    </div>
    <?php echo form_close(); ?> 
    <!-- END FORM--> 
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('tiposervicio/js/script');?>