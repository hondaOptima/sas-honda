<style>
		.logo{
				width:180px;
				height:100px;
				background-image:url("http://bsimport.com/wp-content/uploads/2014/03/bsi_brand_logo1.png");
				background-image-size:70%;
				background-repeat:no-repeat;
		}
		.page {
			font-family:Verdana, Geneva, sans-serif;
		}
		.row{
			width:100%;
			margin-bottom:10px;
			float:left;
		}
		.row:first-child{
			margin-bottom:0px;
		}
		.floated{
			float:left;
		}
		.col-12 {
			width:100%;
		}
		.col-10{
			width:83.33%;
		}
		.col-9 {
			width:75%;
		}
		.col-8{
			width:66.66%;
		}
		.col-7{
			width:58.33%;
		}
		.col-6 {
			width:50%;
		}
		.col-5{
			width:41.66%;
		}
		.col-4 {
			width:33.33%;
		}
		.col-3 {
			width:25%;
		}
		.col-2 {
			width:16.5%;
		}
		.col-1{
			width:8.33%;
		}
		.first-row {
			border:1px solid rgb(0,0,0);
		}
		.title {
			color:rgb(226,0,43);
		}
		.right{
			text-align:right;
			padding-right:3px;
			border-right:1px solid rgb(0,0,0);
		}
		.left{
			text-align:left;
			padding-left:3px;
		}
		.radius {
			border: 1px solid rgb(51,51,51);
			border-radius: 10px;
			border-spacing:0;
			overflow:hidden;
		}
		</style>
		<div class="page" style="width:700px;">
			<div class="row">
				<div class="col-6 floated" style="text-align:center;"><b>HOJA DE TRABAJO DE IMPORTACIONES MARITIMAS</b></div>
				<div class="col-6 floated" style="text-align:right;">
					Cliente/Modalidad/Despacho
					<br><b style="text-align:center;"></b>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-6 floated">
					<div class="row">
						Contenedor: <b></b>
					</div>
					<div class="row">
						BL: <b></b>
					</div>
				</div>
				<div class="col-6 floated">
					<div class="row" style="text-align:right;">
						# de Pedimento: <b></b>
					</div>
					<div class="row" style="text-align:right;">
						Origen: <b></b>
					</div>
					<div class="row" style="text-align:right;">
						Destino: <b></b>
					</div>
				</div>
			</div>
			<br><br>
			<br>
			<div class="row">
				<div class="col-12 floated" style="text-align:center;">INFORME</div><br>
				<div class="row">
					<div class="col-3 floated" style="text-align:left;">HORA</div>
					<div class="col-3 floated" style="text-align:left;">FECHA</div>
					<div class="col-6 floated" style="text-align:center;">OBSERVACIONES</div>
				</div>
			</div>
			<br><br>
			<br><br>
			<br><br>
			<br><br>
			<br><br>
			<br><br>
			<br><br>
			<div class="row">
				<div class="col-12 floated"><u>DATOS DE PROVEEDOR</u>: <b></b></div><br>
				<div class="col-12 floated"><u>NUMERO DE FACTURA</u>: <b></b></div><br>
				<div class="col-12 floated"><u>COMERCIALIZADORA</u>: <b></b></div><br>
			</div>
			<br><br>
			<div class="row">
				<div class="col-12 floated" style="text-align:center;">Otra informaci&oacute;n</div><br>
				<div class="row">
				<div class="col-12 floated">Notas especiales:</div>
				<div class="col-12 floated"><b></b></div>
				<div class="col-12 floated"><b></b></div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="logo" style="float:right;"></div>
				</div>
			</div>
		</div>