<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>pizarron">Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-group"></i>
							<a href="<?php echo base_url();?>usuario">Usuarios</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-edit"></i>
							<a href="#">Editar</a>
							<i class="fa fa-angle-right"></i>
						</li>
</ul>
                    
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Editar Usuario
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
                                        
                                        <?php if($usuario):?>
                                        
                          				<?php foreach($usuario as $u): ?>
                     					<?php echo form_open_multipart('usuario/actualizar/'.$u->sus_idUser,'class="form-horizontal"'); ?>
                                        <input type="hidden" name="imagen" value="<?php echo $u->sus_foto;?>">
											<div class="form-body">
												<h3 class="form-section">Foto</h3>
                                                	<div class="row">
													<div class="col-md-6">
                                                    <img src="<?php echo base_url();?>images/empleados/<?php echo $u->sus_foto;?>" width="150px;">
														<div class="form-group">
															<label class="control-label col-md-3">Cambiar Foto</label>
															<div class="col-md-9">
                                   								<input type="file" name="userfile" id="userfile" class="form-control">
																
															</div>
														</div>
													</div>
                                                  </div>
                                                <h3 class="form-section">Información Personal</h3>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Nombre</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('nombre',$u->sus_name, 
																		'id="nombre" class="form-control" 
																		placeholder="Nombre" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Apellido</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('apellido',$u->sus_lastName,
																		'id="apellido" class="form-control"
																		placeholder="Apellido" required');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Teléfono</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('telefono',$u->sus_telephone, 
																		'id="mask_phone" class="form-control"
																		placeholder="Teléfono" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Número de Empleado</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('numEmpleado',$u->sus_employeeNumber, 
																		'id="numEmpleado" class="form-control"
																		placeholder="Número de empleado"');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Número de Asesor</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('numAsesor',$u->sus_adviserNumber, 
																		'id="numAsesor" class="form-control"
																		placeholder="Número de asesor"');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">DTPS</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('dtps',$u->sus_dtps, 
																		'id="dtps" class="form-control"
																		placeholder="dtps"');?>
																
															</div>
														</div>
													</div>
													
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Usuario Intelisis</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('uin',$u->sus_usuario_intelisis, 
																		'id="uin" class="form-control"
																		placeholder="Usuario Intelsis"');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Taller</label>
															<div class="col-md-9">
                                                               	<?php 
																 echo form_dropdown('taller', $talleres, $u->sus_workshop,
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen"');
																?>
															</div>
														</div>
													</div>
                                                    
                                                    
                                                    <div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Orden</label>
															<div class="col-md-9">
                                                               	<?php 
													echo form_input('orden', $u->sus_orden,'id="" class="form-control"');
																?>
															</div>
														</div>
													</div>
                                                    
												</div>
												<h3 class="form-section">Cuenta</h3>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Correo Electrónico</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('correo',$u->sus_email, 
																		'id="correo" class="form-control"
																		placeholder="Correo Electrónico" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Contraseña</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('nuevaContrasena','', 
																		'id="contrasena" class="form-control"
																		placeholder="Nueva Contraseña"');?>
                                                                        <span class="help-block">
                                                                        	Puedes escribir una nueva contraseña o deja vacio el campo para mantener la contraseña actual.
                                                                        </span>
																<input type="hidden" name="contrasena" value="<?php echo $u->sus_password ?>">
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Tipo de Usuario</label>
															<div class="col-md-9">
                                                               	<?php 
																 echo form_dropdown('tipoUsuario', $tiposUsuario, 
																					$u->sus_userType,
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen"');
																?>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Rol</label>
															<div class="col-md-9">
                                                               	<?php 
																 echo form_dropdown('rol', $roles, $u->sus_rol,
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen"');
																?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-success">Guardar</button>
															<a href="<?php echo base_url();?>usuario"><button type="button" class="btn btn-default">Cancelar</button></a>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
                                            
										<?php echo form_close(); ?>
						   				<?php endforeach ?>
	                 					<?php else: ?>None<?php endif ?>
										<!-- END FORM-->
									</div>
								</div>
					<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('usuario/js/script');?> 