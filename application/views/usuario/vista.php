<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url();?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-group"></i>
        <a href="<?php echo base_url();?>usuario">Usuarios</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>

<?php echo $flash_message;?>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
    <div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i>Usuarios
		</div>
		<div class="actions">
			<a href="<?php echo base_url();?>usuario/crear" class="btn btn-success">
				<i class="fa fa-pencil"></i> Añadir Nuevo Usuario
			</a>
		</div>
    </div>
    <div class="portlet-body">
		<table class="table table-striped table-bordered table-hover" id="sample_2" >
			<thead>
				<tr style="font-weight:bold">
				
				<th>
				<b># Empleado</b>
				</th>
                <th>
				<b>Orden</b>
				</th>
				<th>
				<b>Nombre</b>
				</th>
				<th>
				<b>Correo Electrónico</b>
				</th>
				<th>
				<b>Teléfono</b>
				</th>
				<th>
				<b>Rol</b>
				</th>
				<th>
				<b>Tipo de Usuario</b>
				</th>
				<th>
				<b>Taller</b>
				</th>
				<th>
				<b>Estatus</b>
				</th>
				<th>
				<b>Control</b>
				</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($usuarios as $usuario){ ?>
				<tr class="odd gradeX">
					<td>
						<?php echo $usuario->sus_adviserNumber; ?>
					</td>
                    <td>
						<?php echo $usuario->sus_orden; ?>
					</td>
					<td>
						<?php echo $usuario->sus_name.' '.$usuario->sus_lastName; ?>
					</td>
					<td>
						<a href="mailto:<?php echo $usuario->sus_email; ?>"><?php echo $usuario->sus_email; ?></a>
					</td>
					<td>
						<?php echo $usuario->sus_telephone; ?>
					</td>
					<td>
						<?php echo $usuario->rol_name; ?>
					</td>
					<td>
						<?php echo $usuario->ust_name; ?>
					</td>
					<td>
						<?php echo $usuario->wor_city; ?>
					</td>
					<td>
						<?php  if($usuario->sus_isActive==1){ $status = 'Activo'; $label='success'; $action='desactivar/'; }
						else { $status = 'Inactivo'; $label='danger'; $action='activar/'; }?>
						
						
						<a href="<?php echo base_url().'usuario/'.$action.$usuario->sus_idUser ?>" 
						onClick="return confirm('Está seguro que desea desactivar a <?php echo $usuario->sus_name.' '.$usuario->sus_lastName; ?>?');"> 
							<span class="label label-sm label-<?php echo $label?>">
							<?php echo $status?>
							</span>
						</a>
					</td>
                    <td>
                        <a href="<?php echo base_url().'usuario/editar/'.$usuario->sus_idUser ?>" 
                        class="btn btn-default">
                            <i class="fa fa-edit" ></i>
                        </a>
                        <a href="<?php echo base_url().'usuario/borrar/'.$usuario->sus_idUser ?>" 
                        class="btn btn-default" onClick="return confirm('Está seguro que desea borrar a <?php echo $usuario->sus_name.' '.$usuario->sus_lastName; ?>?');">
                            <i class="fa fa-trash-o" ></i>
                        </a>
                    </td>
				</tr>
				
				<?php }
				?>
			</tbody>
		</table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('usuario/js/script');?> 