<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>pizarron">Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-group"></i>
							<a href="<?php echo base_url();?>usuario">Usuarios</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-edit"></i>
							<a href="<?php echo base_url();?>usuario/crear">Nuevo</a>
							<i class="fa fa-angle-right"></i>
						</li>
</ul>
 
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Nuevo Usuario
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
                                        
                     					<?php echo  form_open_multipart('usuario/crear','class="form-horizontal"'); ?>
											<div class="form-body">
												<h3 class="form-section">Información Personal</h3>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Nombre</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('nombre',set_value('nombre'), 
																		'id="nombre"  maxlength="25" class="form-control" 
																		placeholder="Nombre" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Apellido</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('apellido',set_value('apellido'), 
																		'id="apellido"  maxlength="25" class="form-control"
																		placeholder="Apellido" required');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Teléfono</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('telefono',set_value('telefono'), 
																		'class="form-control" id="mask_phone" placeholder="Teléfono"
																		 required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Número de Empleado</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('numEmpleado',set_value('numEmpleado'), 
																		'id="numEmpleado" class="form-control"
																		placeholder="Número de empleado"');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Número de Asesor</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('numAsesor',set_value('numAsesor'), 
																		'id="numAsesor" class="form-control"
																		placeholder="Número de asesor"');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">DTPS</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('dtps',set_value('dtps'), 
																		'id="dtps" class="form-control"
																		placeholder="DPTS"');?>
																
															</div>
														</div>
													</div>
													
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Usuario Intelisis</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('uin',set_value('uin'), 
																		'id="uin" class="form-control"
																		placeholder="Usuario Intelsis"');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Taller</label>
															<div class="col-md-9">
                                                               	<?php 
																 echo form_dropdown('taller', $talleres, '1',
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen"');
																?>
															</div>
														</div>
													</div>
                                                    
                                                    <div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Foto</label>
															<div class="col-md-9">
                                                               <input type="file" class="form-control"id="userfile" name="userfile" required>
															</div>
														</div>
													</div>
												</div>
                                                
                                                
                                                <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Orden</label>
															<div class="col-md-9">
                                                               	<?php 
					 echo form_input('orden',set_value('orden'),' class="form-control"');
																?>
															</div>
														</div>
													</div>
												</div>
                                                
                                                
                                                
                                                
												<h3 class="form-section">Cuenta</h3>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Correo Electrónico</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('correo',set_value('correo'), 
																		'id="correo" class="form-control"
																		placeholder="Correo Electrónico" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Contraseña</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('contrasena',set_value('contrasena'), 
																		'id="contrasena" class="form-control"
																		placeholder="Contraseña" required');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Tipo de Usuario</label>
															<div class="col-md-9">
                                                               	<?php 
																 echo form_dropdown('tipoUsuario', $tiposUsuario, 
																					'1',
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen"');
																?>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Rol</label>
															<div class="col-md-9">
                                                               	<?php 
																 echo form_dropdown('rol', $roles, '1',
																					'id="selectError" 
																					class="select2_category form-control"
																					data-rel="chosen"');
																?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-success">Añadir</button>
															<a href="<?php echo base_url();?>usuario"><button type="button" class="btn btn-default">Cancelar</button></a>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
                                            
										<?php echo form_close(); ?>
										<!-- END FORM-->
									</div>
								</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				
<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('usuario/js/script');?> 