<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-home"></i> <a href="<?php echo base_url();?>pizarron">Dashboard</a> <i class="fa fa-angle-right"></i> </li>
  <li> <i class="fa fa-group"></i> <a href="<?php echo base_url();?>usuario">Usuarios</a> <i class="fa fa-angle-right"></i> </li>
</ul>
<?php echo $flash_message;?>
<div class="row"> 
  <!-- BEGIN EXAMPLE TABLE PORTLET-->
  
  <?php $i=0;
          foreach($usuario as $us) {
	       if($us->rol_idRol<'4' && $us->sus_isActive=='1') {
			   $i++;
?>
  <div class="col-md-3 "> 
    <!-- BEGIN Portlet PORTLET-->
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-reorder"></i>Equipo <?php echo $i;?> </div>
        <div class="actions"> <a href="<?php echo base_url()?>usuario/equiposEditar/<?php echo $us->sus_idUser;?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Editar</a> </div>
      </div>
      <div class="portlet-body">
        <div class="scroller" style="height:200px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd"> 
          
          <table class="table table-striped table-bordered table-hover"  >
            <thead>
              <tr style="font-weight:bold">
                <th> <b>Asesor de Ventas</b> </th>
              </tr>
            </thead>
            <tbody>
            <td><?php echo $us->sus_name.' '.$us->sus_lastName;?></td>
                </tbody>
          </table>
          <table class="table table-striped table-bordered table-hover"  >
            <thead>
              <tr style="font-weight:bold">
                <th> <b>Tecnicos</b> </th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($equipos as $eq){
				if($eq->equ_asesor==$us->sus_idUser){
				?>
            <tr>
            <td><?php echo $eq->sus_name.' '.$eq->sus_lastName;?></td>
            </tr>
            <?php }} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END Portlet PORTLET--> 
  </div>
  <?php }} ?>
</div>
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('usuario/js/script');?>
