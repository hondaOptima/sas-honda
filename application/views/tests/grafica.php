<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
     <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
</head>
<body>
    <div id="contenedor">
    
    </div>
</body>
<script>
    $(document).ready(function() {
    var options = {
        chart: {
            renderTo: 'contenedor',
            type: 'spline'
        },
        series: [{}]
    };
    
    var url =  "http://hsas.gpoptima.net/seminuevos/GetLista";
    $.getJSON(url,  function(data) {
        options.series[0].data = data;
        var chart = new Highcharts.Chart(options);
    });
});
</script>
</html>