<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='citas';$this->load->view('globales/menu',$menu); ?>
<form method="get" action="<?php echo base_url();?>cita/porconfirmar">
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url();?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-calendar"></i>
        <a href="<?php echo base_url();?>cita">Citas</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-calendar"></i>
        <a href="<?php echo base_url();?>cita/porconfirmar">Online</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li class="pull-right"><input style="margin-top:-7px; margin-left:-5px; "  type="submit" class="btn btn-info" value="Consultar"></li>
    <li class="pull-right">

    <div style="margin-top:-7px; width:200px;"  class="input-group  
    date date-picker tooltips" data-date="<?php echo $date;?>" 
    data-date-format="yyyy-mm-dd" data-date-viewmode="years" 
    data-original-title="Cambiar fecha de Citas">
		<span class="input-group-btn">
            <button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button>
        </span><input  type="text" name="date" value="<?php echo $date;?>" class="form-control" readonly>
												
	</div>
 <!--<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
            <i class="fa fa-calendar"></i>
            <span>
            </span>
            <i class="fa fa-angle-down"></i>
        </div>-->
    </li>
</ul>
</form>

<div class="clearfix">
</div>

<div class="row">
	<div class="col-md-9 col-sm-12">
    	<div class="portlet">
        	<div class="portlet-title">
            	<div class="caption">Citas Por Confirmar</div>
                <div class="actions">
								
				</div>
            </div>
            
            
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr style="font-weight:bold; font-size:11px;">
                            
                            <th>
                                 <b>Hora</b>
                            </th>		
                            <th>
                                 <b>Asesor</b>
                            </th>
                            <th>
                                 <b>Cliente</b>
                            </th>
                            <th>
                                 <b>Tel&eacute;fono</b>
                            </th>
                            <th>
                                 <b>Veh&iacute;culo</b>
                            </th>		
                            <th>
                                 <b>Servicio</b>
                            </th>		
                            <th>
                                 <b>Recordatorio</b>
                            </th>	
                            <th>
                                 <b>Confirmado?</b>
                            </th>
                            <th>
                                 <b></b>
                            </th>									
                        </tr>
                    </thead>
                    <tbody>
                    
                    
						<?php if($citas): ?>
                        <?php foreach($citas as $cita){ ?>
                            <tr class="odd gradeX">
                            
                            <td>
                                <?php echo date('g:i A', strtotime($cita->app_hour)); ?>
                            </td>
                            <td>
                                <?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>
                            </td>
                            <td>
                                 <?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?>
                            </td>
                            <td>
                                 <?php echo $cita->app_customerTelephone ?>
                            </td>
                            <td>
                                <?php echo $cita->vem_name.' - '.$cita->app_vehicleYear; ?>
                            </td>
                            <td>
                                <?php echo $cita->set_name; ?>
                            </td>
                            <td>
                                <?php echo $cita->rem_name; ?>
                            </td>
                            <td>
                                    <a href="<?php 
                                    echo base_url().'cita/confirmaronline/'.$cita->app_idAppointment ?>" 
                                    onClick="return confirm('Está seguro que desea confirmar esta cita online?');">
                                        <span class="label label-sm label-warning">
                                            Por confirmar
                                        </span>
                                    </a>
                            </td>
                            <td>
                            
                            <div class="task-config-btn btn-group">
                                <a class="btn btn-xs btn-default dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
                                <i class="fa fa-cog"></i>
                                <i class="fa fa-angle-down"></i>
                                
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo base_url().'cita/editar/'.$cita->app_idAppointment ?>">
                                    <i class="fa fa-edit"></i>
                                    Editar</a>
                                    </li>
                                    
                                </ul>
                            </div>
                                
                            </td>
                        </tr>
                            
                            <?php } ?>
                            <?php endif ?>
                                
                                
                    </tbody>
                </table>
			</div>
        </div>
    </div>
	<div class="col-md-3 col-sm-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-reorder"></i>Disponibilidad de Taller
                </div>
            </div>
            <div class="portlet-body">
        
                <div id="graficaCitas" class="chart">
                </div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN EXAMPLE TABLE PORTLET-->
<!-- END EXAMPLE TABLE PORTLET-->


<?php $this->load->view('globales/footer');?>
<?php $this->load->view('cita/js/script');?> 
 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Ocupado',   80],
          ['Disponible',      20],
      
        
        ]);

        var options = {
           height:'1000px',
		  width:'1000px',
		   colors:['#d12610','#63bd2c'],
		   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
		   legend:{position: 'top'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graficaCitas'));
        chart.draw(data, options);
      }
    </script>