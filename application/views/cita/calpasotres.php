<?php $this->load->view('globales/head'); ?>

<div class="row" style="background-color:white">


	<div class="col-md-3 col-sm-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i><b>Paso 3 de 3.</b> Información de Contacto
                </div>
               
            </div>
            <div class="portlet-body">
        
             <?php echo form_open('cita/crearonline','class=""'); ?> 
                    <input type="hidden" name="fecha" value="<?php echo $date;?>">
                    <input type="hidden" name="hora" value="<?php echo $hora;?>">
                    <input type="hidden" name="idh" value="<?php echo $idh;?>">
                    <input type="hidden" name="taller" value="<?php echo $taller;?>">
                    <input type="hidden" name="modelo" value="<?php echo $modelo;?>">
                     <input type="hidden" name="servicio" value="<?php echo $servicio;?>">
                    <input type="hidden" name="anio" value="<?php echo $anio;?>">                                                               
               
                  <div class="row"  >
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteNombre',set_value('clienteNombre'), 
                                        'id="clienteNombre"  maxlength="25" class="form-control" 
                                        placeholder="Nombre" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteApellido',set_value('clienteApellido'), 
                                        'id="clienteApellido"  maxlength="25" class="form-control"
                                        placeholder="Apellido" required');	?>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteTelefono',set_value('clienteTelefono'), 
                                        'id="mask_phone"  class="form-control" 
                                        placeholder="Teléfono" required');	?>
                            </div>
                        </div>
                    </div>
                    
                </div>
				<!--/row-->
                <br>
                <div class="row">
                   
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteCorreo',set_value('clienteCorreo'), 
                                        '  class="form-control" 
                                        placeholder="Email" required');	?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Asesor:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('asesor', $asesores, '29',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-5">Valet?:</label>
                            <div class="checkbox-list col-md-3" style="padding-top:7px">
                                <input type="checkbox" id="necesitaValet" name="necesitaValet" value="1" >
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <br><div class="row">
                <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Recordatorio:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('tipoRecordatorio', $recordatorios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                      <div class="col-md-8">
                        <div class="form-group">
                           	<?php	echo form_textarea('mensaje',set_value('mensaje'),
									'id="mensaje" class="form-control" placeholder="Comentario..." 
									style="resize: none; height:80px;"');	?>
                        </div>
                    </div>
                    
                </div>
                <input class="btn btn-success" type="submit" value="Guardar Informaci&oacute;n">
            </div>
        </div>
        
        
</div>
<script src="<?php echo base_url();?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/scripts/app.js"></script>

<script>
jQuery(document).ready(function() {    
 App.init();
 
});


</script>