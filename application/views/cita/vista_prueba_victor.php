<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='citas';$this->load->view('globales/menu',$menu); ?>

<link href="<?php echo base_url(); ?>assets/assets-cita/css/style.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-1.10.2.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets-cita/js/script.js"></script>

 <style type="text/css">

    #printable { display: none; }

    @media print
    {
    	#non-printable { display: none; }
    	#printable { display: block; }
    }
    </style>
    <div style="display: none;" id="modal-cita" class="div-back-cita">
        <div class="div-modal-cita" >
            <div class="title-delete">
                <div>
                    <div id="div-result-manage">
                        <div style="" id="clock" class="light">
                            <div class="display">
                                <div class="weekdays"></div>
                                <div class="ampm"></div>
                                <div class="alarm"></div>
                                <div class="digits"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li> <i class="fa fa-calendar" id="hrefPrint"></i> <a href="<?php echo base_url();?>cita/tv_sas">Citas <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?></a> <i class="fa fa-angle-right"></i> </li>

        <li class="pull-right">
            <?php if($_SESSION['sfrol']<=2) {?>
            <select  name="taller" onchange='this.form.submit()'>
                <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
                <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
                <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>
            </select>
            <?php } ?>
        </li>
    </ul>
<style>
    .client-m{
        width:160px !important;
    }
</style>
<div class="clearfix"> </div>
<?php echo $flash_message;?>
<div class="row">
    <div class="col-md-9 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-clock-o"></i><b>Citas <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?> </b> </div>
                <div class="tools"> <?php echo $di.' '.$meses[date($ms)].' del '.$an;?> <i class="fa fa-calendar"></i>&nbsp;<a style="color:white" href="<?php echo base_url()?>cita/imprimir/?date=<?php echo $date;?>&taller=<?php echo $taller;?>" target="_blank"><i style="cursor:pointer" title="Imprimir" class="fa fa-print" ></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="respuesta"></div>
                <table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="imprimir" >
                    <thead>
                        <tr style="font-weight:bold; font-size:11px;">
                            <th width="15px;" > <b>#</b> </th>
                            <th style="width:40px;"> <b>Hora</b> </th>
                            <th width="30px;" style="" title="¿Puntual?"> <b>P</b> </th>
                            <th width="30px;"><b>Timer</b></th>
                            <th width="60px;"> <b>Folio</b> </th>
                            <th style="width:45px;"> <b>Asesor</b> </th>
                            <th > <b>Cliente</b> </th>
                            <th > <b>Auto</b> </th>
                            <th width="100px;"> <b>Tel&eacute;fono</b> </th>
                            <th style="width:50px;"> <b>Servicio</b> </th>
                            <th style="width:60px;"> <b>Recordatorio</b> </th>
                            <th style="width:60px;" id="non-printable"> <b>¿Confirmada?</b> </th>
                            <th style="width:50px;" id="non-printable"> <b>Acciones</b> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $xx=1; $tt=1; $ii=1; $mm = 1; foreach ($lista as $cita) {
							if($cita->cal_status=='2')//1)Disponible; 2)Ocupada; 3)Eliminada;
                            {
								$clas='warning';
								$txt='ban';
								$url='';
							}else
                            {
								$clas='info';
								$txt='edit';
                                $url=base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."#tabcon";
							}
							if($cita->app_status==1) $bg='#3cc051';
							else $bg='';
						?>
                        <tr class="">
                            <td>
                                <?php
                                if(!empty($cita->sus_name))
                                {
                                    echo $xx; $xx++;
                                    list($iden,$nidnum) = explode('-',$cita->app_folio);
                                    //T = Telefono, I = Internet, M = Movile del APP, P = Previas.
			                        /*if($iden=='I') $ii++;
			                        if($iden=='T') $tt++;
                                    if($iden=='M') $mm++;*/

                                    switch ($iden) {
                                        case "I":
                                            $ii++;
                                            break;
                                        case "T":
                                            $tt++;
                                            break;
                                        case "M":
                                            $mm++;
                                            break;
                                    }
			                    }?>
                            </td>
                            <td>
                                <?php
                                if(empty($cita->sus_name)) {
                                    $url='#wide';
                                    $idc=$date.' '.$cita->cal_hora.':00/'.$cita->cal_ID;
                                    $classs='agendar';
                                }
                                else
                                {
                                    $url='';
                                    $idc='';
                                    $classs='';
                                }
                                ?>
                                <a href="<?php echo $url?>"  class="<?php echo $classs?>" id="<?php echo $idc?>" >
                                    <?php
							            echo $cita->cal_hora;
							            list($hora,$min)=explode(':',$cita->cal_hora);
							        ?>
                                </a>
                            </td>
                            <td>
                                <!-- $cita->app_customerArrival: (NULL), on, off, NUL -->
                                <input type="checkbox" facita="fa-cita-clock-<?php echo $cita->app_idAppointment;?>"  name="ch<?php echo $cita->app_idAppointment;?>" id="<?php echo $cita->app_idAppointment;?>" class="puntual <?php if($cita->app_customerArrival=='on'){ echo ' checktime';} ?>"<?php echo $cita->app_customerArrival; ?> <?php if($cita->app_customerArrival=='on'){echo 'checked time="'.$cita->app_timeAttended.'"';}?>>
                            </td>
                            <td>
                                <div id="div-in-clock-<?php echo $cita->app_idAppointment; ?>" style="width:20px;">
                                    <?php if($cita->app_customerArrival=='on')
                                    {
                                        if($cita->app_timeToOrder == "0000-00-00 00:00:00"){ ?>
                                            <i style="font-size:2.5em; padding-top:8px;" id="fa-cita-clock-<?php echo $cita->app_idAppointment;?>" cita="<?php echo $cita->app_idAppointment;?>"status="0" stop="false" onclick="update_time('<?php echo $cita->app_timeAttended; ?>', this)" class="fa fa-clock-o "></i>
                                        <?php }
                                        else { ?>
                                <i style="font-size:2.5em; padding-top:8px;" id="fa-cita-clock-<?php echo $cita->app_idAppointment;?>" cita="<?php echo $cita->app_idAppointment;?>" status="1" stop="<?php echo $cita->app_timeToOrder;?>" stop="false" onclick="update_time('<?php echo $cita->app_timeAttended; ?>', this)" class="fa fa-clock-o "></i>
                                        <?php }?>
                                    <?php } ?>
                                </div>
                            </td>
                            <td><?php echo $cita->app_folio;?></td>
                            <td title="<?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>" style="text-align:center; cursor:help">
			                    <?= ucwords(strtolower($cita->sus_name)); ?>
                            </td>
                            <td style="cursor:help" title="Comentario:<?php echo $cita->app_message;?>">
			                    <?= ucwords(strtolower($cita->app_customerName.' '.$cita->app_customerLastName)); ?>
                            </td>
                            <td title="<?php echo $cita->vem_name.' '.$cita->app_vehicleYear; ?>">
                                <?= ucwords(strtolower(substr($cita->vem_name.' '.$cita->app_vehicleYear,0,15))); ?>
                            </td>
                            <td style="cursor:help" title="<?php echo $cita->app_tipo_telefono;?>"><?php echo $cita->app_customerTelephone ?></td>
                            <td title="<?php echo $cita->set_name; ?>" style="cursor:help; text-align:center"><?php echo $cita->set_codigo; ?></td>
              <td><?php echo $cita->rem_name; ?></td>
              <td style="text-align:center" id="non-printable"><?php
								 if(empty($cita->sus_name)){}else{
								  if($cita->app_confirmed==1){ $status = '&nbsp;&nbsp;Confirmada&nbsp;&nbsp;&nbsp;'; $label='success';
								 $action='#'; $onclick = '';}
								 else if($cita->app_confirmed==2){ $status = 'Cancelada'; $label='danger';
								 $action='#'; $onclick = '';}
								 else if(empty($cita->app_confirmed)){ $status = 'Por Confirmar'; $label='warning';
								 $action= base_url().'cita/confirmar/'.$cita->app_idAppointment.'/?date='.$date.'&taller='.$taller.'';
								 $onclick = "return confirm('Est&aacute; seguro que desea confirmar esta cita?');";}?>
                <a href="<?php
                                    echo $action ?>"
                                    onClick="<?php echo $onclick?>"> <span class="label label-sm label-<?php echo $label?>"> <?php echo $status?> </span> </a>
                <?php } ?></td>
              <td id="non-printable" style="text-align:center"><?php if(empty($cita->sus_name)) {?>
                <?php } elseif($cita->app_status==1) {} else{ ?>
                <div class="task-config-btn btn-group"> <a class="btn btn-xs btn-default dropdown-toggle btn-primary" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#"> <i class="fa fa-cog"></i> <i class="fa fa-angle-down"></i> </a>
                  <ul class="dropdown-menu pull-right" style="text-align:left">

                   <?php if($permiso){}else{?>
                    <li><a href="<?php echo base_url().'orden/saveespera?idapp='.$cita->app_idAppointment.''; ?>"> <i class="fa fa-paste"></i> Crear Orden de Servicio </a> </li>
                    <?php } ?>

                    <li><a href="<?php echo base_url().'cita/reagendar/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"> <i class="fa fa-calendar-o"></i> Reagendar Cita </a> </li>
                    <?php
						if($taller == 2)
						{
							if($_SESSION['sfemail'] == 'mmartinez@hondaoptima.com' or $_SESSION['sfemail']== 'igonzalez@hondaoptima.com' or $_SESSION['sfemail']=='marina@hondaoptima.com')
							{
								$t = $taller;
								?>
								<li><a href="<?php echo base_url().'cita/editar/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>">
								<i class="fa fa-edit"></i> Editar </a> </li>
								<?php
							}
						}
						else
						{
							$t = $taller;
							?>
							<li><a href="<?php echo base_url().'cita/editar/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>">
							<i class="fa fa-edit"></i> Editar </a> </li>
							<?php
						}
					?>
					<!-- <li><a href="<?php #echo base_url().'cita/editar/'.$cita->app_idAppointment ?>/?taller=<?php #echo $taller;?>">
					<i class="fa fa-edit"></i> Editar </a> </li>  -->
					<li><a href="<?php echo base_url().'cita/cancelar/'.$cita->app_idAppointment.'/'.$cita->cal_ID ?>/?taller=<?php echo $taller;?>&date=<?php echo $date;?>"

					onClick="return confirm('Est&aacute; seguro que desea cancelar esta cita?');"> <i class="fa fa-ban"></i> Cancelar Cita </a> </li>
                  </ul>
                </div>
                <?php } ?></td>
            </tr>
            <?php  } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="portlet">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-clock-o"></i><b>Sin Citas <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?> </b> </div>
            <div class="tools"> <?php echo $di.' '.$meses[date($ms)].' del '.$an;?> <i class="fa fa-calendar"></i> </div>
          </div>
          <div class="portlet-body">
            <table style="font-size:12px;" class="table table-striped table-bordered table-hover" >
              <thead>
                <tr style="font-weight:bold; font-size:11px;">
                <th > <b>#</b> </th>
                  <th style="width:60px;"> <b>Hora</b> </th>
                  <th><b>Folio</b></th>
                  <th style="width:45px;"> <b>Asesor</b> </th>
                  <th> <b>Cliente</b> </th>
                  <th> <b>Tel&eacute;fono</b> </th>
                  <th> <b>Veh&iacute;culo</b> </th>
                  <th style="width:50px;"> <b>Servicio</b> </th>
                  <th style="width:60px;" id="non-printable"> <b>Acciones</b> </th>
                </tr>
              </thead>
              <tbody>
                <?php $pp=1;  foreach ($sinCita as $cita) {

				      if($cita->app_status==1){$bg='#3cc051';}
											else{ $bg='';}

									?>
                <tr class="odd gradeX" >
                <td><?php echo $pp; $pp++;?></td>
                  <td><a href="<?php echo $url; ?>"  >
                    <?php

							list($fec,$hora)=explode(' ',$cita->app_hour);
							echo $hora;
							list($hora,$min)=explode(':',$hora);

							if($hora <=11){echo 'a.m.';}else{echo 'p.m.';}
							?>
                    </a></td>
                   <td><?php echo $cita->app_folio;?></td>
                  <td title="<?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>" style="text-align:center; cursor:help"><?php echo $cita->sus_idUser; ?></td>
                  <td><?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?></td>
                  <td><?php echo $cita->app_customerTelephone ?></td>
                  <td><?php echo $cita->vem_name.' '.$cita->app_vehicleYear; ?></td>
                  <td style="text-align:center; cursor:pointer" title="<?php echo $cita->set_name; ?>" ><?php echo $cita->set_codigo; ?></td>
                  <td style="text-align:center" id="non-printable">
                  <?php if(empty($cita->sus_name)) {?>
                <?php } elseif($cita->app_status==1) {} else{ ?>
                  <div class="task-config-btn btn-group"> <a class="btn btn-xs btn-default dropdown-toggle btn-primary" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#"> <i class="fa fa-cog"></i> <i class="fa fa-angle-down"></i> </a>
                      <ul class="dropdown-menu pull-right" style="text-align:left">
                      <?php if($cita->set_codigo == 'ava'){ ?>
                            <li><a href="<?php echo base_url().'orden/saveespera?idapp='.$cita->app_idAppointment.''; ?>"> <i class="fa fa-paste"></i> Crear Orden de Servicio </a> </li>
                      <?php }else{if($permiso){}else{?>
                        <li><a href="<?php echo base_url().'orden/saveespera?idapp='.$cita->app_idAppointment.''; ?>"> <i class="fa fa-paste"></i> Crear Orden de Servicio </a> </li>
                        <?php }?>
                        <li><a href="<?php echo base_url().'cita/editarSin/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"> <i class="fa fa-edit"></i> Editar </a> </li>
                        <li><a href="<?php echo base_url().'cita/cancelarSin/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"
                                    onClick="return confirm('Est&aacute; seguro que desea cancelar esta cita?');"> <i class="fa fa-ban"></i> Cancelar Cita </a> </li>
                      </ul>
                    </div>

                    <?php }} ?></td>
                </tr>
                <?php  } ?>
              </tbody>
            </table>
            <a class="btn btn-primary btn-block agendarsin" id="<?php date_default_timezone_set('America/Tijuana'); echo  date('H:i').':00';?>"  href="#wideSin">A&ntildeadir registro sin Cita</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6" id="non-printable">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-calendar"></i>Calendario </div>
        <div class="tools">
          <div class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on">
            <div class="fc-text-arrow"><a style="color:white" title="Anterior" href="<?php echo base_url()?>cita/Prueba?date=<?php echo $ant;?>&taller=<?php echo $taller;?>"><i class="fa fa-toggle-left"></i></a></div>
          </div>
          <span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a style="color:white" title="Siguiente" href="<?php echo base_url()?>cita/Prueba?date=<?php echo $sig;?>&taller=<?php echo $taller;?>"><i class="fa  fa-toggle-right"></i></a></span></span> </div>
      </div>
      <div class="portlet-body">
        <div id="calendarB"> </div>
      </div>
    </div>
<!-- Pay de Citas -->
   <?php $this->load->view('cita/payCitas'); ?>
   <div class="portlet" id="non-printable">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-circle-o"></i>Total de Citas </div>
      </div>
      <div class="portlet-body">
      <table style="text-align:center" class="table table-striped table-bordered table-hover" >
      <tr>
      <td style="font-weight:bold">Citas Por Telefono</td>
      <td>
      <?php echo $tt-1; ?>
      </td>
      </tr>
      <tr>
      <td style="font-weight:bold">Citas Por Internet</td>
      <td>
      <?php echo $ii-1;?>
      </td>
      </tr>
      <tr>
      <td style="font-weight:bold">Citas Presenciales</td>
      <td><?php echo $pp-1;?></td>
      </tr>
      </tr>
      <tr>
      <td style="font-weight:bold">Citas App Movil</td>
      <td><?php echo $mm-1;?></td>
      </tr>
      </table>

      </div>
    </div>


  </div>
</div>
<div class="row"> </div>
<!-- Nueva Cita -->
<?php
$data['servicios']=$servicios;
$data['taller']=$taller;
$data['modelos']=$modelos;
$data['recordatorios']=$recordatorios;
$this->load->view('cita/nuevaCitaCopia',$data);
?>

<!-- Nueva Cita Sin Cita -->
<?php
$data['servicios']=$servicios;
$data['taller']=$taller;
$data['modelos']=$modelos;
$data['recordatorios']=$recordatorios;
$this->load->view('cita/nuevaCitaSin',$data);
?>
<!-- Factor de produccion 100 % del taller en numero de horas-->
<?php
$tot=0;
$tot1=0;
$tot2=0;
$tot1=$total[0]->tot;
$tot2=$totSin[0]->tot;
$tot=$tot1 + $tot2;
$porcentaje;
$verde=$porcentaje - $tot;

?>
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('cita/js/script');?>
<script type="text/javascript">


    //   google.load("visualization", "1", {packages:["corechart"]});
    //   google.setOnLoadCallback(drawChart);
    //   function drawChart() {
    //     var data = google.visualization.arrayToDataTable([
    //       ['Task', 'Hours per Day'],
    //       ['Ocupado',   <?= $tot;?>],
    //       ['Disponible',    <?= $verde;?>],


    //     ]);

    //     var options = {
    //        height:'1000px',
	// 	  width:'1000px',
	// 	   colors:['#cc0000','#3cc051'],
	// 	   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
	// 	   legend:{position: 'top'}
    //     };

    //     var chart = new google.visualization.PieChart(document.getElementById('graficaCitas'));
    //     chart.draw(data, options);
    //   }
    </script>
<script type="text/javascript">

jQuery(document).ready(function() {
    var actuales = [];
    checkStop();
    setInterval(checkTimes,3000);
    function checkTimes(){
        var elems = $('.checktime');
        elems.each(function(){
            var cita = $(this).attr('id');
            var time = $(this).attr('time');
            var idclock = $(this).attr('facita');
            if(time.toString().length >6){
                time = time.replace(/:/g,"");
                var section = (time .length-6);
                time = time.substr(section,6);
            }

            var color = 'green';

            var nowtime = moment().format("HHmmss");
            var hin = ((time[0]+time[1])*60)*60;
            var min = (time[2]+time[3])*60;
            var sin = (time[4]+time[5])*1;

            var totalin = hin+min+sin;

            var hn = ((nowtime[0]+nowtime[1])*60)*60;
            var mn = (nowtime[2]+nowtime[3])*60;
            var sn = (nowtime[4]+nowtime[5])*1;

			var nowTemp = hn+mn+sn;
            var timePlusFive = totalin+300;
            var timePlusTen = totalin+600;

            if(nowTemp >= timePlusFive){
                color = '#f5a31f';
            }

            if(nowTemp>=timePlusTen){
                color = 'red';
            }

            if($('#'+idclock).attr('status') == 1){
                color = 'black';
            }
            $('#'+idclock).css('color',color);
            checkStop();
            checkNewArrivals();
        });

    }

    function checkNewArrivals(){
        $.ajax({
              url:"<?php echo base_url(); ?>ajax/citas/getNewArrivals.php?taller=<?php echo $taller; ?>",
              success:function(data){
                  var dataJson = JSON.parse(data);
                        for(var i = 0; i< dataJson.length; i++){
                            var pass = false;
                            for(var u = 0; u<actuales.length; u++){
                                if(dataJson[i].id == actuales[u]){
                                    pass = true;
                                }
                            }
                            if(pass==false){
                                var clockToIn = '';
                                var id = dataJson[i].id;
                                var time = dataJson[i].time;
                                var timeAttended = dataJson[i].timeAttended;
                                var checkbox = $('#'+id);
                                var divClock = $('#div-in-clock-'+id);

                                clockToIn = " <i style=\"font-size:1.5em;\" id=\"fa-cita-clock-"+id+"\" cita=\""+id+"\"status=\"0\" stop=\""+time+"\" onclick=\"update_time('"+timeAttended+"', this)\" class=\"fa fa-clock-o \"></i>";

                                checkbox.addClass('puntual');
                                checkbox.addClass('checktime');
                                checkbox.attr('time',time);
                                checkbox.prop('checked',true)

                                divClock.html(clockToIn);
                            }
                        }
                    }
                 });
    }

    function checkStop(){
        $('.checktime').each(function(){
            var cita = $(this).attr('id');
            actuales.push(cita);
            $.ajax({
              url:"<?php echo base_url(); ?>ajax/citas/checkstop.php?cita="+cita+"",
              success:function(data){
                    if(data.status == 'STOP'){
                        $('#fa-cita-clock-'+cita).css('color','black');
                        $('#'+cita).removeClass('checktime');
                        $('#fa-cita-clock-'+cita).attr('status','1');
                        $('#fa-cita-clock-'+cita).attr('stop',data.data);
                    }
                                      }
                 });

        });


    }



var data = {items: []};

$('.puntual').live('click',function(){
$('.respuesta').html('Conectando... !');
var mcCbxCheck = $(this);
var id=$(this).attr('id');
if(mcCbxCheck.is(':checked')) {
$.post("<?php echo base_url(); ?>ajax/citas/updateCita.php",{id:id,valor:'on'}, function(data) { $('.respuesta').html('Cliente Puntual !');
            $(this).addClass('checktime');
});
    }
    else{
$.post("<?php echo base_url(); ?>ajax/citas/updateCita.php",{id:id,valor:'off'}, function(data) { $('.respuesta').html('Cliente Inpuntual !'); });
    }

});


$('.agendar').live('click',function(){

	//obtener asesores
	var asesores = '';
	$.ajax({
		type: "POST",
		url: "obtenerAsesores",
		data:  {"fecha": 'd'} ,
		success: function (data)
		{
			var json = JSON.parse(data);
			var c = json.length;
			asesores = json;
		},
		cache: false
	});//termina AJAX

	var id=$(this).attr('id');

	$.ajax({
		url: "<?php echo base_url()?>ajax/calendarioClientes/verdisponibilidad.php?id="+id+"",
		type: "post",
		data: $('.citassas').serialize(),
		success: function(info)
		{
			if(info=='ok')
			{
				 alert('Cita Ocupada se actualizara la lista de Citas');
				// window.location="http://www.hondaoptima.com/sas/cita/prueba";
			}
			else
			{
				$('#wide').modal('show');
				$('input[name=fecha]').val(id);
				var a = $('input[name=fecha]').val();
				var b = a.split(" ");
				var c = b[1].split(':00/');
				$('#asesor').empty();
				console.log(b[0]);
				//cargar asesores disponibles
				$.ajax
				({
					type: "POST",
					url: "obtenerDatos",
					data:  {"fecha": b[0],"hora" : c[0]} ,
					success: function (data)
					{
						var json = JSON.parse(data);
						var njson = json.length;
						var nasesores = asesores.length;
						//Agregar asesores al select
						for(var i = 0; i < nasesores; i++)
						{
							var as = asesores[i];
							if(as.sus_idUser == 67){$('#asesor').append('<option value="'+as.sus_idUser+'" selected="selected">'+as.sus_name+' '+as.sus_lastName+'</option>');}
							else {$('#asesor').append('<option value="'+as.sus_idUser+'">'+as.sus_name+' '+as.sus_lastName+'</option>');}
						}

						// quitar asesores del select no disponibles
						for(var j = 0; j < njson; j++)
						{
							var id = json[j].sus_idUser;
							$("select#asesor option[value='"+id+"']").remove();
						}
						$("select#asesor option[value='35']").remove();
						$("select#asesor option[value='100']").remove();
					},
						cache: false
				});//termina AJAX
			}
// alert(d);
                        }
         });


});

$('.agendarsin').live('click',function(){
var id=$(this).attr('id');
$('#wideSin').modal('show');
$('input[name=fechaSin]').val(id);
});


  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar

            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
   dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita/porcentajeCita?fecha=<?php echo $date;?>&taller=<?php echo $taller;?>', // use the `url` property

        }


    ],
	eventRender: function(event, element, monthView) {
     if (event.color == "#1ec138") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('orange');
           }
if (event.color == "#cc0000") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('ocupado');
           }
if (event.color == "#fcb322") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('festivo');
           }
if (event.color == "#333") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('black');
           }
     }
            });


$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');


	$("#hrefPrint").click(function() {
// Print the DIV.
document.getElementById('imprimir').print();
});
});
</script>
