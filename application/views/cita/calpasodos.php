<?php $this->load->view('globales/head'); ?>




<div class="row" style="background-color:white">
	
	<div class="col-md-3 col-sm-6">
    
         <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><b>
                 Paso 2 de 3.
                </b> Seleccionar Fecha de Cita</div>
                
            </div>
           
        </div>
    
    
    
    
    
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>Calendario
                </div>
                 <div class="tools"><a href="" style="margin-top:-15px;" class="expand"></a></div>
                            <div class="tools">
                              <span class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita/calpasodos?date=<?php echo $ant;?>&taller=<?php echo $taller?>&servicio=<?php echo $servicio;?>&modeloVehiculo=<?php echo $modelo?>&anoVehiculo=<?php echo $anio?>">‹</a></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita/calpasodos?date=<?php echo $sig;?>&taller=<?php echo $taller?>&servicio=<?php echo $servicio;?>&modeloVehiculo=<?php echo $modelo?>&anoVehiculo=<?php echo $anio?>">›</a></span></span>
                            </div>
            </div>
            <div class="portlet-body">
        
             <div id="calendarB">
							</div>
            </div>
        </div>
        
         <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-circle-o"></i>Seleccione una de las siguientes opciones
                </div>
            </div>
            <div class="portlet-body">
        
                  <?php  foreach ($lista as $cita) { 
					
						 
									if(!empty($cita->sus_name)) {
										$clas='danger';
										$txt='ban';
										$url='';
										$tit='Cita Ocupada';
										}else{
										$tit='Cita Disponible';
											$clas='success';
											$txt='edit';
$url="".base_url()."cita/calpasotres?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."&modelo=".$modelo."&servicio=".$servicio."&anio=".$anio."";
											}
									
									?>
											
											
											
                                            
                              
                             <a title="<?php echo $tit;?>" class="btn btn-<?php echo $clas;?>  " style="margin-bottom:4px; margin-left:4px;" href="<?php echo $url; ?>"  >
							
							
							<?php echo $cita->cal_hora?> a.m.
							
							
							</a>
                          
										
                                    <?php  } ?>   
                    
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog modal-wide">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Nueva Cita:</h4>
										</div>
										<div class="modal-body">
											
                	<?php echo form_open('cita/crear','class=""'); ?> 
                    <input type="hidden" name="fecha" value="">                       
                <div class="row">
                  <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Servicio:</label>
                            <div class="col-md-9">
                            
                                <?php	echo form_dropdown('servicio', $servicios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Modelo:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('modeloVehiculo', $modelos, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Año:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('anoVehiculo',set_value('anoVehiculo'), 
										'id="mask_number" maxlenght="4" class="form-control"
                                        placeholder="Año de Vehículo" required');	?>
                                
                            </div>
                        </div>
                  
                </div>
                
                
                
                </div>
                <br>
                  <div class="row"  >
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteNombre',set_value('clienteNombre'), 
                                        'id="clienteNombre"  maxlength="25" class="form-control" 
                                        placeholder="Nombre" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteApellido',set_value('clienteApellido'), 
                                        'id="clienteApellido"  maxlength="25" class="form-control"
                                        placeholder="Apellido" required');	?>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteTelefono',set_value('clienteTelefono'), 
                                        'id="mask_phone"  class="form-control" 
                                        placeholder="Teléfono" required');	?>
                            </div>
                        </div>
                    </div>
                    
                </div>
				<!--/row-->
                <br>
                <div class="row">
                   
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteCorreo',set_value('clienteCorreo'), 
                                        '  class="form-control" 
                                        placeholder="Email" required');	?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Asesor:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('asesor', $asesores, '29',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-5">Valet?:</label>
                            <div class="checkbox-list col-md-3" style="padding-top:7px">
                                <input type="checkbox" id="necesitaValet" name="necesitaValet" value="1" >
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <br><div class="row">
                <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Recordatorio:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('tipoRecordatorio', $recordatorios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                      <div class="col-md-8">
                        <div class="form-group">
                           	<?php	echo form_textarea('mensaje',set_value('mensaje'),
									'id="mensaje" class="form-control" placeholder="Comentario..." 
									style="resize: none; height:80px;"');	?>
                        </div>
                    </div>
                    
                </div>
                
                
                
                
                                            
                                            
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
											<input type="submit" class="btn btn-info" value="Guardar">
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<!-- END EXAMPLE TABLE PORTLET-->

<!-- Factor de produccion 100 % del taller en numero de horas-->

<!-- Factor de produccion 100 % del taller en numero de horas gastadas-->

<?php
$porcentaje;

$verde=$porcentaje - $tot[0]->tot;

?>
<script src="<?php echo base_url();?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/scripts/app.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
jQuery(document).ready(function() {    
 App.init();
 
});

$(function() { citas.citasOnline(); });
 
citas= {
        citasOnline: function() {

  $.ajax({
	  url:"<?php echo base_url();?>dashboard/citasPorConfirmar",
	  success:function(result){
             $("#citasOnline").html(result);
                              }
         });

}}
setInterval("citas.citasOnline()", 10000000); // Update every 10 seconds 
</script>
<?php $this->load->view('cita/js/script');?> 
 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Ocupado',   <?php echo $tot[0]->tot;?>],
          ['Disponible',     <?php echo $verde;?>],
      
        
        ]);

        var options = {
           height:'1000px',
		  width:'1000px',
		   colors:['#cc0000','#008000'],
		   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
		   legend:{position: 'top'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graficaCitas'));
        chart.draw(data, options);
      }
    </script>
    
    <script>
jQuery(document).ready(function() {
	
$('.agendar').live('click',function(){ 
var id=$(this).attr('id');
$('#wide').modal('show');    
$('input[name=fecha]').val(id);
});	
	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita/porcentajeCitaCliente?fecha=<?php echo $date;?>&servicio=<?php echo $servicio;?>&modeloVehiculo=<?php echo $modelo;?>&anoVehiculo=<?php echo $anio;?>', // use the `url` property
           
        }


    ],
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
			
			
});
</script>