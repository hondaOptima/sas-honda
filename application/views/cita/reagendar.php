<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='citas';$this->load->view('globales/menu',$menu); ?>
<form method="get" action="<?php echo base_url();?>cita">
<ul class="page-breadcrumb breadcrumb">
    
     <li>
        <i class="fa fa-calendar"></i>
        <a href="<?php echo base_url();?>cita/?taller=<?php echo $_GET['taller']?>">Calendario de Citas de <?php if($_GET['taller']==1){echo 'Ensenada';}if($_GET['taller']==2){echo 'Mexicali';}if($_GET['taller']==3){echo 'Tijuana';}?></a>
          <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-calendar"></i>
        <a href="">Reagendar Cita</a>
        
    </li>
  
    <li class="pull-right">

    </li>
</ul>
</form>

<div class="clearfix">
</div>

<div class="row">

	<div class="col-md-4 col-sm-4">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>Calendario
                </div>
                 <div class="tools"><a href="" style="margin-top:-15px;" class="expand"></a></div>
                            <div class="tools">
                              <span class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita/reagendar/<?php echo $idc;?>/?date=<?php echo $ant;?>&taller=<?php echo $taller;?>">‹</a></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita/reagendar/<?php echo $idc;?>/?date=<?php echo $sig;?>&taller=<?php echo $taller;?>">›</a></span></span>
                            </div>
            </div>
            <div class="portlet-body">
        
             <div id="calendarB">
							</div>
            </div>
        </div>
        
        
    </div>
    
    
    
    <div class="col-md-4 col-sm-4">
    	<div class="portlet">
        	<div class="portlet-title">
            	<div class="caption"><i class="fa fa-clock-o"></i><b>Lista de Horas del <?php echo $di.' '.$meses[date($ms)].' del '.$an;?>  </b> </div>
             
               
            </div>
            
            
            <div class="portlet-body">
          
                <table class="table table-striped table-bordered table-hover" >
                    <thead>
                        <tr style="font-weight:bold; font-size:11px;">
                            
                            <th>
                                 
                            </th>
                            <th>
                                 <b>Hora</b>
                            </th>		
                            <th>
                                 <b>Cliente</b>
                            </th>
                            <th>
                                 <b>Servicio</b>
                            </th>
                           				
                        </tr>
                    </thead>
                     <?php  foreach ($lista as $cita) { 
					
						 
									if($cita->cal_status=='2'){
										$clas='warning';
										$txt='ban';
										$url='';
										}else{
											$clas='info';
											$txt='edit';
$url="".base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."#tabcon";
											}
											if($cita->app_status==1){$bg='#3cc051';}
											else{ $bg='';}
									
									?>
											
											
											
                                            
                                           
                            
                            
                             <tr class="odd gradeX" style="background-color:<?php echo $bg;?>">
                            <td>
                            <?php if(empty($cita->set_name)) {?>
                            <input type="checkbox" class="cradio" id="<?php echo $date.'/'.$cita->cal_hora.'/'.$cita->cal_ID.'/'.$recita[0]->cal_ID.'/'.$recita[0]->app_idAppointment;?> ">
                            <?php }?>
                            </td>
                            <td>
                             <a href="<?php echo $url; ?>"  >
							
							
							<?php echo $cita->cal_hora?>
							
							
							</a>
                            </td>
                            <td>
                                <?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?>
                            </td>
                            <td>
                                <?php echo $cita->set_name ?>
                            </td>
                            <td>
                            </td>
                        </tr>
										
                                    <?php  } ?>   
                    
					
                                
                                
                    </tbody>
                </table>
			</div>
        </div>
    </div>
    
  
  
  
 	<div class="col-md-4 col-sm-4">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>Reagendar Cita
                </div>
                
            </div>
            <div class="portlet-body">
            <input type="hidden" value="0" id="nuevacita" name="nuevacita">
        <b>Cliente:</b><?php echo $recita[0]->app_customerName.' '.$recita[0]->app_customerLastName;?><br>
        <b>Auto:</b><?php echo $recita[0]->app_vehicleModel.' '.$recita[0]->app_vehicleYear;?><br>
        <b>Servicio:</b><?php echo $recita[0]->set_name.' '.$recita[0]->app_customerLastName;?><br>
        <b>Comentario:</b><?php echo $recita[0]->app_message;?><br>        
         <br><br>
         
         <div class="btn btn-success btn-xs guardar">Guardar</div>
         
            </div>
        </div>
        
        
    </div> 
  
    
</div>




							</div>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<!-- END EXAMPLE TABLE PORTLET-->

<!-- Factor de produccion 100 % del taller en numero de horas-->

<!-- Factor de produccion 100 % del taller en numero de horas gastadas-->

<?php $this->load->view('globales/footer');?>
<?php $this->load->view('cita/js/script');?> 

    
    <script>
	
jQuery(document).ready(function() {

	
$('.guardar').live('click',function(){ 
var id=$('input[name=nuevacita]').attr('value');
if(id==0){alert('Selccione una hora');}   
else{
window.location="<?php echo base_url()?>cita/registrareagendada?idc=<?php echo $idc?>&idncita="+id+"&taller=<?php echo $taller;?>";	
	}
});	

$('.cradio').live('click',function(){ 
var id=$(this).attr('id');
$('input[name=nuevacita]').val(id);
});	
	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita/porcentajeCitaReagendar?fecha=<?php echo $date;?>&idc=<?php echo $idc;?>&taller=<?php echo $taller;?>', // use the `url` property
           
        }


    ],
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
			
			
});
</script>