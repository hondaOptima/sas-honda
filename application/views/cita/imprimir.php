<?php
header('Content-Type: text/html; charset=UTF-8'); 
?>
 <style type="text/css">

    #printable { display: none; }

    @media print
    {
    	#non-printable { display: none; }
    	#printable { display: block; }
    }
    </style>
<form name="form" >
<input name="date" value="<?php echo $_GET['date']?>" type="hidden">
<input name="taller" value="<?php echo $_GET['taller']?>" type="hidden">
<select name="ase"  onchange="this.form.submit();">
<option value="0">Todos</option>
<?php
foreach($usuarios as $us){
if($us->sus_idUser==$user){$ch='selected="selected" ';}else{$ch='';}
	echo '<option value="'.$us->sus_idUser.'" '.$ch.'>'.$us->sus_name.' '.$us->sus_lastName.'</option>';
	}
?>
</select>
</form>
        <table border="1" class="table table-striped table-bordered table-hover" id="imprimir" >
          <thead>
            <tr style="font-weight:bold; font-size:11px;">
              <th style="width:60px;"> <b>Hora</b> </th>
              <th style=""> <b>Folio</b> </th>              
              <th style="width:45px;"> <b>Asesor</b> </th>
              <th> <b>Cliente</b> </th>
              <th> <b>Tel&eacute;fono</b> </th>
              <th> <b>Veh&iacute;culo</b> </th>
              <th style="width:50px;"> <b>Servicio</b> </th>
              <th style="width:60px;"> <b>Recordatorio</b> </th>
             
            </tr>
          </thead>
          <tbody>
            <?php  foreach ($lista as $cita) { 
				
						 
									if($cita->cal_status=='2'){
										$clas='warning';
										$txt='ban';
										$url='';
										}else{
											$clas='info';
											$txt='edit';
$url="";
											}
											if($cita->app_status==1){$bg='#3cc051';}
											else{ $bg='';}
									
									?>
            <tr class="" >
              <td><?php if(empty($cita->sus_name)) { 
							          $url='#wide';
									   $idc=' '.$cita->cal_hora.':00/'.$cita->cal_ID.'';
									    $classs='agendar';
							       }else{
									      $url='';
									       $idc='';
									        $classs='';
								 
								        }?>
                <a href="<?php echo $url?>"  class="<?php echo $classs?>" id="<?php echo $idc?>" >
                <?php 
							echo $cita->cal_hora;
							list($hora,$min)=explode(':',$cita->cal_hora); 
							if($hora <=11){echo 'a.m.';}else{echo 'p.m.';}
							?>
                </a></td>
                <td><?php echo $cita->app_folio;?></td>
              <td title="<?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>" style="text-align:center; cursor:help"><?php echo $cita->sus_idUser; ?></td>
              <td style="cursor:help" title="Comentario:<?php echo $cita->app_message;?>"><?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?></td>
              <td style="cursor:help" title="<?php echo $cita->app_tipo_telefono;?>"><?php echo $cita->app_customerTelephone ?></td>
              <td><?php echo $cita->vem_name.' '.$cita->app_vehicleYear; ?></td>
              <td title="<?php echo $cita->set_name; ?>" style="cursor:help; text-align:center"><?php echo $cita->set_codigo; ?></td>
              <td><?php echo $cita->rem_name; ?></td>
              
            </tr>
            <?php  }  ?>
          </tbody>
        </table>
  
           