<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='citas';$this->load->view('globales/menu',$menu); ?>
 <style type="text/css">

    #printable { display: none; }

    @media print
    {
    	#non-printable { display: none; }
    	#printable { display: block; }
    }
    </style>

  <ul class="page-breadcrumb breadcrumb">
   
    <li> <i class="fa fa-calendar" id="hrefPrint"></i> <a href="<?php echo base_url();?>cita/tv_sas/?date=<?= $date;?>">Citas <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?></a> <i class="fa fa-angle-right"></i> </li>
    <li class="pull-right">
    <?php if($_SESSION['sfrol']<=2) {?>
    
    <select  name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    <?php } ?>
    </li>
    
  </ul>

<div class="clearfix"> </div>
<?php echo $flash_message;?>
<div class="row">
  <div class="col-md-9 col-sm-12">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"><i class="fa fa-clock-o"></i><b>Citas <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?> </b> </div>
        <div class="tools"> <?php echo $di.' '.$meses[date($ms)].' del '.$an;?> <i class="fa fa-calendar"></i>&nbsp;<a style="color:white" href="<?php echo base_url()?>cita/imprimir/?date=<?php echo $date;?>&taller=<?php echo $taller;?>" target="_blank"><i style="cursor:pointer" title="Imprimir" class="fa fa-print" ></i></a>
        </div>
      </div>
      <div class="portlet-body">
      <div class="respuesta"></div>
        <table class="table table-striped table-bordered table-hover" id="imprimir" >
          <thead>
            <tr style="font-weight:bold; font-size:11px;">
             <th > <b>#</b> </th>
              <th style="width:60px;"> <b>Hora</b> </th>
              <th style=""> <b>P</b> </th>
              <th style=""> <b>Folio</b> </th>              
              <th style="width:45px;"> <b>Asesor</b> </th>
              <th> <b>Cliente</b> </th>
              <th> <b>Tel&eacute;fono</b> </th>
              <th> <b>Veh&iacute;culo</b> </th>
              <th style="width:50px;"> <b>Servicio</b> </th>
              <th style="width:60px;"> <b>Recordatorio</b> </th>
              <th style="width:60px;" id="non-printable"> <b>Confirmada?</b> </th>
              <th style="width:50px;" id="non-printable"> <b>Acciones</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php $xx=1; $tt=1; $ii=1; foreach ($lista as $cita) { 
					
						 
									if($cita->cal_status=='2'){
										$clas='warning';
										$txt='ban';
										$url='';
										}else{
											$clas='info';
											$txt='edit';
$url="".base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."#tabcon";
											}
											if($cita->app_status==1){$bg='#3cc051';}
											else{ $bg='';}
									
									?>
            <tr class="" >
            <td><?php if(!empty($cita->sus_name)) { echo $xx; $xx++;
			list($iden,$nidnum)=explode('-',$cita->app_folio);
			if($iden=='I'){$ii++;}
			if($iden=='T'){$tt++;}
			}?></td>
              <td><?php if(empty($cita->sus_name)) { 
							          $url='#wide';
									   $idc=''.$date.' '.$cita->cal_hora.':00/'.$cita->cal_ID.'';
									    $classs='agendar';
							       }else{
									      $url='';
									       $idc='';
									        $classs='';
								 
								        }?>
                <a href="<?php echo $url?>"  class="<?php echo $classs?>" id="<?php echo $idc?>" >
                <?php 
							echo $cita->cal_hora;
							list($hora,$min)=explode(':',$cita->cal_hora); 
							if($hora <=11){echo 'a.m.';}else{echo 'p.m.';}
							?>
                </a></td>
                <td><input type="checkbox" name="ch<?php echo $cita->app_idAppointment;?>" id="<?php echo $cita->app_idAppointment;?>" class="puntual" <?php if($cita->app_customerArrival=='on'){echo 'checked';}?>></td>
                <td><?php echo $cita->app_folio;?></td>
              <td title="<?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>" style="text-align:center; cursor:help"><?php echo $cita->sus_name; ?></td>
              <td style="cursor:help" title="Comentario:<?php echo $cita->app_message;?>"><?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?></td>
              <td style="cursor:help" title="<?php echo $cita->app_tipo_telefono;?>"><?php echo $cita->app_customerTelephone ?></td>
              <td><?php echo $cita->vem_name.' '.$cita->app_vehicleYear; ?></td>
              <td title="<?php echo $cita->set_name; ?>" style="cursor:help; text-align:center"><?php echo $cita->set_codigo; ?></td>
              <td><?php echo $cita->rem_name; ?></td>
              <td style="text-align:center" id="non-printable"><?php
								 if(empty($cita->sus_name)){}else{
								  if($cita->app_confirmed==1){ $status = '&nbsp;&nbsp;Confirmada&nbsp;&nbsp;&nbsp;'; $label='success'; 
								 $action='#'; $onclick = '';}
								 else if($cita->app_confirmed==2){ $status = 'Cancelada'; $label='danger'; 
								 $action='#'; $onclick = '';}
								 else if(empty($cita->app_confirmed)){ $status = 'Por Confirmar'; $label='warning'; 
								 $action= base_url().'cita/confirmar/'.$cita->app_idAppointment.'/?date='.$date.'&taller='.$taller.''; 
								 $onclick = "return confirm('Est&aacute; seguro que desea confirmar esta cita?');";}?>
                <a href="<?php 
                                    echo $action ?>" 
                                    onClick="<?php echo $onclick?>"> <span class="label label-sm label-<?php echo $label?>"> <?php echo $status?> </span> </a>
                <?php } ?></td>
              <td id="non-printable" style="text-align:center"><?php if(empty($cita->sus_name)) {?>
                <?php } elseif($cita->app_status==1) {} else{ ?>
                <div class="task-config-btn btn-group"> <a class="btn btn-xs btn-default dropdown-toggle btn-primary" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#"> <i class="fa fa-cog"></i> <i class="fa fa-angle-down"></i> </a>
                  <ul class="dropdown-menu pull-right" style="text-align:left">
                   
                   <?php if($permiso){}else{?>
                    <li><a href="<?php echo base_url().'orden/saveespera?idapp='.$cita->app_idAppointment.''; ?>"> <i class="fa fa-paste"></i> Crear Orden de Servicio </a> </li>
                    <?php } ?>
                    
                    <li><a href="<?php echo base_url().'cita/reagendar/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"> <i class="fa fa-calendar-o"></i> Reagendar Cita </a> </li>
                    <?php 
						if($taller == 2)
						{
							if($_SESSION['sfemail'] == 'mmartinez@hondaoptima.com' or $_SESSION['sfemail']== 'igonzalez@hondaoptima.com' or $_SESSION['sfemail']=='marina@hondaoptima.com')
							{
								$t = $taller;
								?>
								<li><a href="<?php echo base_url().'cita/editar/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"> 
								<i class="fa fa-edit"></i> Editar </a> </li> 
								<?php
							}
						}
						else
						{
							$t = $taller;
							?>
							<li><a href="<?php echo base_url().'cita/editar/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"> 
							<i class="fa fa-edit"></i> Editar </a> </li> 
							<?php
						}
					?>
					<!-- <li><a href="<?php #echo base_url().'cita/editar/'.$cita->app_idAppointment ?>/?taller=<?php #echo $taller;?>"> 
					<i class="fa fa-edit"></i> Editar </a> </li>  -->                  
					<li><a href="<?php echo base_url().'cita/cancelar/'.$cita->app_idAppointment.'/'.$cita->cal_ID ?>/?taller=<?php echo $taller;?>&date=<?php echo $date;?>" 
					
					onClick="return confirm('Est&aacute; seguro que desea cancelar esta cita?');"> <i class="fa fa-ban"></i> Cancelar Cita </a> </li>
                  </ul>
                </div>
                <?php } ?></td>
            </tr>
            <?php  } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="portlet">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-clock-o"></i><b>Sin Citas <?php if($taller==1){echo 'Ensenada';}if($taller==2){echo 'Mexicali';}if($taller==3){echo 'Tijuana';}?> </b> </div>
            <div class="tools"> <?php echo $di.' '.$meses[date($ms)].' del '.$an;?> <i class="fa fa-calendar"></i> </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr style="font-weight:bold; font-size:11px;">
                <th > <b>#</b> </th>
                  <th style="width:60px;"> <b>Hora</b> </th>
                  <th><b>Folio</b></th>
                  <th style="width:45px;"> <b>Asesor</b> </th>
                  <th> <b>Cliente</b> </th>
                  <th> <b>Tel&eacute;fono</b> </th>
                  <th> <b>Veh&iacute;culo</b> </th>
                  <th style="width:50px;"> <b>Servicio</b> </th>
                  <th style="width:60px;" id="non-printable"> <b>Acciones</b> </th>
                </tr>
              </thead>
              <tbody>
                <?php $pp=1;  foreach ($sinCita as $cita) { 
					
				      if($cita->app_status==1){$bg='#3cc051';}
											else{ $bg='';}
									
									?>
                <tr class="odd gradeX" >
                <td><?php echo $pp; $pp++;?></td>
                  <td><a href="<?php echo $url; ?>"  >
                    <?php 
							
							list($fec,$hora)=explode(' ',$cita->app_hour);
							echo $hora;
							list($hora,$min)=explode(':',$hora);
							 
							if($hora <=11){echo 'a.m.';}else{echo 'p.m.';}
							?>
                    </a></td>
                   <td><?php echo $cita->app_folio;?></td>
                  <td title="<?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>" style="text-align:center; cursor:help"><?php echo $cita->sus_idUser; ?></td>
                  <td><?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?></td>
                  <td><?php echo $cita->app_customerTelephone ?></td>
                  <td><?php echo $cita->vem_name.' '.$cita->app_vehicleYear; ?></td>
                  <td style="text-align:center; cursor:pointer" title="<?php echo $cita->set_name; ?>" ><?php echo $cita->set_codigo; ?></td>
                  <td style="text-align:center" id="non-printable">
                  <?php if(empty($cita->sus_name)) {?>
                <?php } elseif($cita->app_status==1) {} else{ ?>
                  <div class="task-config-btn btn-group"> <a class="btn btn-xs btn-default dropdown-toggle btn-primary" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#"> <i class="fa fa-cog"></i> <i class="fa fa-angle-down"></i> </a>
                      <ul class="dropdown-menu pull-right" style="text-align:left">
                      <?php if($permiso){}else{?>
                        <li><a href="<?php echo base_url().'orden/saveespera?idapp='.$cita->app_idAppointment.''; ?>"> <i class="fa fa-paste"></i> Crear Orden de Servicio </a> </li>
                        <?php }?>
                        <li><a href="<?php echo base_url().'cita/editarSin/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>"> <i class="fa fa-edit"></i> Editar </a> </li>
                        <li><a href="<?php echo base_url().'cita/cancelarSin/'.$cita->app_idAppointment ?>/?taller=<?php echo $taller;?>" 
                                    onClick="return confirm('Est&aacute; seguro que desea cancelar esta cita?');"> <i class="fa fa-ban"></i> Cancelar Cita </a> </li>
                      </ul>
                    </div>
                    
                    <?php } ?></td>
                </tr>
                <?php  } ?>
              </tbody>
            </table>
            <a class="btn btn-primary btn-block agendarsin" id="<?php date_default_timezone_set('America/Tijuana'); echo  date('H:i').':00';?>"  href="#wideSin">A&ntildeadir registro sin Cita</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6" id="non-printable">
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-calendar"></i>Calendario </div>
        <div class="tools">
          <div class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on">
            <div class="fc-text-arrow"><a style="color:white" title="Anterior" href="<?php echo base_url()?>cita/Prueba?date=<?php echo $ant;?>&taller=<?php echo $taller;?>"><i class="fa fa-toggle-left"></i></a></div>
          </div>
          <span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a style="color:white" title="Siguiente" href="<?php echo base_url()?>cita/Prueba?date=<?php echo $sig;?>&taller=<?php echo $taller;?>"><i class="fa  fa-toggle-right"></i></a></span></span> </div>
      </div>
      <div class="portlet-body">
        <div id="calendarB"> </div>
      </div>
    </div>
<!-- Pay de Citas -->   
   <?php $this->load->view('cita/payCitas'); ?> 
   <div class="portlet" id="non-printable">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-circle-o"></i>Total de Citas </div>
      </div>
      <div class="portlet-body">
      <table style="text-align:center" class="table table-striped table-bordered table-hover" >
      <tr>
      <td style="font-weight:bold">Citas Por Telefono</td>
      <td>
      <?php echo $tt-1; ?>
      </td>
      </tr>
      <tr>
      <td style="font-weight:bold">Citas Por Internet</td>
      <td>
      <?php echo $ii-1;?>
      </td>
      </tr>
      <tr>
      <td style="font-weight:bold">Citas Presenciales</td>
      <td><?php echo $pp-1;?></td>
      </tr>
      </table>
        
      </div>
    </div>
    
    
  </div>
</div>
<div class="row"> </div>
<!-- Nueva Cita -->
<?php 
$data['servicios']=$servicios; 
$data['taller']=$taller; 
$data['modelos']=$modelos;
$data['recordatorios']=$recordatorios;
$this->load->view('cita/nuevaCitaCopia',$data); 
?>

<!-- Nueva Cita Sin Cita -->
<?php 
$data['servicios']=$servicios; 
$data['taller']=$taller; 
$data['modelos']=$modelos;
$data['recordatorios']=$recordatorios;
$this->load->view('cita/nuevaCitaSin',$data); 
?>
<!-- Factor de produccion 100 % del taller en numero de horas--> 
<?php
$tot=0;
$tot1=0;
$tot2=0;
$tot1=$total[0]->tot;
$tot2=$totSin[0]->tot;
$tot=$tot1 + $tot2;
$porcentaje;
$verde=$porcentaje - $tot;

?>
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('cita/js/script');?>
<script type="text/javascript">


      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Ocupado',   <?= $tot;?>],
          ['Disponible',    <?= $verde;?>],
      
        
        ]);

        var options = {
           height:'1000px',
		  width:'1000px',
		   colors:['#cc0000','#3cc051'],
		   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
		   legend:{position: 'top'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graficaCitas'));
        chart.draw(data, options);
      }
    </script> 
<script type="text/javascript">
	
jQuery(document).ready(function() {

	
$('.puntual').live('click',function(){
$('.respuesta').html('Conectando... !');	
var mcCbxCheck = $(this);
var id=$(this).attr('id');
if(mcCbxCheck.is(':checked')) {
$.post("<?php echo base_url(); ?>ajax/citas/updateCita.php",{id:id,valor:'on'}, function(data) { $('.respuesta').html('Cliente Puntual !');});
    }
    else{
$.post("<?php echo base_url(); ?>ajax/citas/updateCita.php",{id:id,valor:'off'}, function(data) { $('.respuesta').html('Cliente Inpuntual !'); });
    }
	
});	
	
	
$('.agendar').live('click',function(){
	
	//obtener asesores
	var asesores = '';
	$.ajax({
		type: "POST",
		url: "obtenerAsesores",
		data:  {"fecha": 'd'} ,
		success: function (data)
		{
			var json = JSON.parse(data);
			var c = json.length;    
			asesores = json;			
		},
		cache: false
	});//termina AJAX
	
	var id=$(this).attr('id');
	
	$.ajax({
		url: "<?php echo base_url()?>ajax/calendarioClientes/verdisponibilidad.php?id="+id+"",
		type: "post",
		data: $('.citassas').serialize(),
		success: function(info) 
		{
			if(info=='ok')
			{
				 alert('Cita Ocupada se actualizara la lista de Citas');
				// window.location="http://www.hondaoptima.com/sas/cita/prueba";
			}
			else
			{
				$('#wide').modal('show');    
				$('input[name=fecha]').val(id);   
				var a = $('input[name=fecha]').val(); 
				var b = a.split(" ");
				var c = b[1].split(':00/');
				$('#asesor').empty();
				console.log(b[0]);
				//cargar asesores disponibles
				/*$.ajax
				({
					type: "POST",
					url: "obtenerDatos",
					data:  {"fecha": b[0],"hora" : c[0]} ,
					success: function (data)
					{	
						var json = JSON.parse(data);						
						var njson = json.length;					
						var nasesores = asesores.length;																		
						//Agregar asesores al select
						for(var i = 0; i < nasesores; i++)
						{
							var as = asesores[i];*/
							/*if(as.sus_idUser == 67)
							{
								$('#asesor').append('<option value="'+as.sus_idUser+'" selected="selected">'+as.sus_name+' '+as.sus_lastName+'</option>');
							}
					   else {*/
							//	$('#asesor').append('<option value="'+as.sus_idUser+'">'+as.sus_name+' '+as.sus_lastName+'</option>');
							//}							
						//}
						
						// quitar asesores del select no disponibles
						//for(var j = 0; j < njson; j++)
						//{
						//	var id = json[j].sus_idUser;
						//	$("select#asesor option[value='"+id+"']").remove();
						//}			
						//$("select#asesor option[value='35']").remove();						
						//$("select#asesor option[value='100']").remove();						
					//},
						//cache: false
				//});//termina AJAX
			}
// alert(d);
                        }
         });
		
		 	
});	

$('.agendarsin').live('click',function(){ 
var id=$(this).attr('id');
$('#wideSin').modal('show');    
$('input[name=fechaSin]').val(id);
});	
	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }
		
            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'],
   dayNamesShort: ['Dom','Lun','Mar','Mi�','Jue','Vie','S�b'],
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita/porcentajeCita?fecha=<?php echo $date;?>&taller=<?php echo $taller;?>', // use the `url` property
           
        }


    ],
	eventRender: function(event, element, monthView) {
     if (event.color == "#1ec138") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('orange');
           }
if (event.color == "#cc0000") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('ocupado');
           }
if (event.color == "#fcb322") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('festivo');
           }
if (event.color == "#333") {
           var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('td[data-date="' + dateString + '"]').addClass('black');
           }


     }
	 
	 
		
		


     
		
				
            });
			
		
$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
	
	$("#hrefPrint").click(function() {
// Print the DIV.
document.getElementById('imprimir').print();	
});			
});
</script>