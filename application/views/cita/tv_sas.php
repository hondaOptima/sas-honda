<?php $this->load->view('globales/head');
$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
header("refresh:3000; url=$self"); //Refrescamos cada 300 segundos

?>
<?php
$info['di']=$di;
$info['ms']=$ms;
$info['an']=$an;
$info['meses']=$meses;
$this->load->view('globales/topbar_tv',$info); ?>
<div class="row" style="font-size:20px; width:98%; margin-left:10px; margin-top:30px;">
<div class="col-md-12 col-sm-12" style="background-color:white">
<div class="portlet" style=" margin-top:15px;">
  <div class="portlet-title" style="text-align:center">
    <div class="caption" style="font-size:25px; text-align:center; width:100%">
    <i style="font-size:35px;" class="fa fa-clock-o"></i><center><b>HOY EN SERVICIO ESPERAMOS A:</b></center>
    </div>

  </div>
  <div class="portlet-body">
    <table style="margin-top:-5px;" class="table table-striped table-bordered table-hover" id="imprimir" >
      <thead>
        <tr style="font-weight:bold; font-size:11px; text-align:center;">
          <th > <b>#</b> </th>
          <th style="width:70px; font-size:12px;"><center> <b>Hora (A.M.)</b></center></th>
          <th style=""><center> <b>P</b></center></th>
          <th style="width:70px;  font-size:20px; padding-bottom:5px;"><center> <b>Asesor</b></center></th>
          <th style="font-size:25px;"><center> <b>Cliente</b></center></th>
          <th style="font-size:20px; padding-bottom:5px;"><center> <b>Veh&iacute;culo</b></center></th>
          <th style="width:50px; padding-bottom:10px;"><center> <b>Servicio</b></center></th>
        </tr>
      </thead>
      <tbody>
        <?php
			$xx=1; $tt=1; $ii=1;
			foreach ($lista as $cita)
			{
				if(!empty($cita->app_folio))
				{
                    if($cita->app_status==1) $bg='#3cc051';	else  $bg='';
                    if($cita->cal_status=='2')
                    {
                        $clas='warning';
                        $txt='ban';
                        $url='';
                    }
                    else
                    {
                        $clas='info';
                        $txt='edit';
                        $url="".base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."#tabcon";
                    }
				   ?>
        <tr class="" >
          <td>
		  <?php
			if(!empty($cita->sus_name))
			{
                echo $xx; $xx++;
                list($iden,$nidnum)=explode('-',$cita->app_folio);
                if($iden=='I'){$ii++;}
                if($iden=='T'){$tt++;}
			}
		   ?></td>
          <td><?php
			if(empty($cita->sus_name))
			{
                $url='#wide';
                $idc=''.$date.' '.$cita->cal_hora.':00/'.$cita->cal_ID.'';
                $classs='agendar';
			}
			else
			{
                $url='';
                $idc='';
                $classs='';
			}?>
            <a href="<?php echo $url?>"  class="<?php echo $classs?>" id="<?php echo $idc?>" >
            <?php
    			list($hora,$min)=explode(':',$cita->cal_hora);
    			echo $cita->cal_hora;
		    ?>
            </a></td>
            <td><input type="checkbox" name="ch<?php echo $cita->app_idAppointment;?>" id="<?php echo $cita->app_idAppointment;?>" class="puntual" <?php if($cita->app_customerArrival=='on'){echo 'checked';}?>></td>
            <td title="<?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>" style=" cursor:help; "><?php echo $cita->sus_name; ?></td>
            <td style="cursor:help; font-size:22px;font-weight:bold" title="Comentario:<?php echo $cita->app_message;?>"><?php echo strtoupper($cita->app_customerName.' '.$cita->app_customerLastName); ?></td>
            <td><?php echo $cita->vem_name.'  '.$cita->app_vehicleYear; ?></td>
            <td title="<?php echo $cita->set_name; ?>" style="cursor:help; text-align:center"><?php echo strtoupper($cita->set_codigo); ?></td>
        </tr>
        <?php } } ?>
      </tbody>
    </table>
  </div>
</div>
<br><br><br>
<br><br><br>
<br><br><br>
<br><br>
<br><br><br>
<br><br><br>
<br><br><br>
<br><br>
<br><br><br>
<br><br><br>
<br><br><br>
<br><br>

<!---quitar la primer hora para que queden 6 espacios libres!>
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('cita/js/script');?>
