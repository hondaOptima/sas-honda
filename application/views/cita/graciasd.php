<?php $this->load->view('globales/head'); ?>

    <li class="pull-right">
 <!--
    <div style="margin-top:-7px; width:200px;"  class="input-group  date date-picker tooltips" data-date="<?ph //echo $date;?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-original-title="Cambiar fecha de Citas">
<span class="input-group-btn">
													<button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button>
												</span><input  type="text" name="date" value="<?ph //echo $date;?>" class="form-control" readonly>
												
                                                </div>
<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
            <i class="fa fa-calendar"></i>
            <span>
            </span>
            <i class="fa fa-angle-down"></i>
        </div>-->
    </li>
</ul>
</form>

<div class="clearfix">
</div>

<div class="row">
	<div class="col-md-9 col-sm-12">
    	<div class="portlet">
        	<div class="portlet-title">
            	<div class="caption"><i class="fa fa-clock-o"></i><b>Citas <?php echo $_SESSION['sfworkshop'];?> </b> </div>
                <div class="tools">
                <?php echo $di.' '.$meses[date($ms)].' del '.$an;?> <i class="fa fa-calendar"></i>
                </div>
               
            </div>
            
            
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" >
                    <thead>
                        <tr style="font-weight:bold; font-size:11px;">
                            
                            <th>
                                 <b>Hora</b>
                            </th>		
                            <th>
                                 <b>Asesor</b>
                            </th>
                            <th>
                                 <b>Cliente</b>
                            </th>
                            <th>
                                 <b>Tel&eacute;fono</b>
                            </th>
                            <th>
                                 <b>Veh&iacute;culo</b>
                            </th>		
                            <th>
                                 <b>Servicio</b>
                            </th>		
                            <th>
                                 <b>Recordatorio</b>
                            </th>	
                            <th>
                                 <b>Confirmada?</b>
                            </th>
                            <th>
                                 <b>Acciones</b>
                            </th>									
                        </tr>
                    </thead>
                    <tbody>
                     <?php  foreach ($lista as $cita) { 
					
						 
									if($cita->cal_status=='2'){
										$clas='warning';
										$txt='ban';
										$url='';
										}else{
											$clas='info';
											$txt='edit';
$url="".base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$cita->cal_hora."&idh=".$cita->cal_ID."#tabcon";
											}
									
									?>
											
											
											
                                            
                                           
                            
                            
                             <tr class="odd gradeX">
                            
                            <td>
                             <a href="<?php echo $url; ?>"  >
							
							
							<?php echo $cita->cal_hora?> a.m.
							
							
							</a>
                            </td>
                            <td>
                                <?php echo $cita->sus_name.' '.$cita->sus_lastName; ?>
                            </td>
                            <td>
                                 <?php echo $cita->app_customerName.' '.$cita->app_customerLastName; ?>
                            </td>
                            <td>
                                 <?php echo $cita->app_customerTelephone ?>
                            </td>
                            <td>
                                <?php echo $cita->vem_name.' '.$cita->app_vehicleYear; ?>
                            </td>
                            <td>
                                <?php echo $cita->set_name; ?>
                            </td>
                             <td>
                                <?php echo $cita->rem_name; ?>
                            </td>
                            <td>
								 <?php if($cita->app_confirmed==1){ $status = 'Confirmada'; $label='success'; 
								 $action='#'; $onclick = '';}
								 else if($cita->app_confirmed==2){ $status = 'Cancelada'; $label='danger'; 
								 $action='#'; $onclick = '';}
								 else if($cita->app_confirmed==0){ $status = 'Por Confirmar'; $label='warning'; 
								 $action= base_url().'cita/confirmar/'.$cita->app_idAppointment; 
								 $onclick = "return confirm('Est&aacute; seguro que desea confirmar esta cita?');";}?>
                                    
                                    <a href="<?php 
                                    echo $action ?>" 
                                    onClick="<?php echo $onclick?>">
                                        <span class="label label-sm label-<?php echo $label?>">
                                            <?php echo $status?>
                                        </span>
                                    </a>
                            </td>
                            <td>
                           <?php if(empty($cita->sus_name)) {?> 
                           <a   class="btn btn-xs btn-default agendar" id="<?php echo $date?> <?php echo  $cita->cal_hora;?>:00/<?php echo $cita->cal_ID;?>"  href="#wide">
                           Agendar Cita
                        </a>
                                <?php } else { ?>
                                   <div class="task-config-btn btn-group">
                                <a class="btn btn-xs btn-default dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
                                <i class="fa fa-cog"></i>
                                <i class="fa fa-angle-down"></i>
                                
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    
                                    <li><a href="<?php echo base_url().'orden/crear/'.$cita->app_idAppointment ?>">
                                    <i class="fa fa-paste"></i>
                                    Crear Orden de Servicio </a>
                                    </li>
                                    
                                    <li><a href="<?php echo base_url().'cita/reagendar/'.$cita->app_idAppointment ?>">
                                    <i class="fa fa-calendar-o"></i>
                                    Reagendar Cita  </a>
                                    </li>

                                    <li><a href="<?php echo base_url().'cita/editar/'.$cita->app_idAppointment ?>">
                                    <i class="fa fa-edit"></i>
                                    Editar </a>
                                    </li>
                                                         
                                    <li><a href="<?php echo base_url().'cita/cancelar/'.$cita->app_idAppointment ?>" 
                                    onClick="return confirm('Est&aacute; seguro que desea cancelar esta cita?');">
                                    <i class="fa fa-ban"></i>
                                    Cancelar Cita </a>
                                    </li>
                                    
                                </ul>
                            </div>
                                <?php } ?>
                            </td>
                        </tr>
										
                                    <?php  } ?>   
                    
					
                                
                                
                    </tbody>
                </table>
			</div>
        </div>
    </div>
	<div class="col-md-3 col-sm-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>Calendario
                </div>
                 <div class="tools"><a href="" style="margin-top:-15px;" class="expand"></a></div>
                            <div class="tools">
                              <span class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita?date=<?php echo $ant;?>">‹</a></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita?date=<?php echo $sig;?>">›</a></span></span>
                            </div>
            </div>
            <div class="portlet-body">
        
             <div id="calendarB">
							</div>
            </div>
        </div>
        
         <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-circle-o"></i>Disponibilidad de Taller
                </div>
            </div>
            <div class="portlet-body">
        
                <div id="graficaCitas" class="chart">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog modal-wide">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Nueva Cita:</h4>
										</div>
										<div class="modal-body">
											
                	<?php echo form_open('cita/crear','class=""'); ?> 
                    <input type="hidden" name="fecha" value="">                       
                <div class="row">
                  <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Servicio:</label>
                            <div class="col-md-9">
                            
                                <?php	echo form_dropdown('servicio', $servicios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Modelo:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('modeloVehiculo', $modelos, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Año:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('anoVehiculo',set_value('anoVehiculo'), 
										'id="mask_number" maxlenght="4" class="form-control"
                                        placeholder="Año de Vehículo" required');	?>
                                
                            </div>
                        </div>
                  
                </div>
                
                
                
                </div>
                <br>
                  <div class="row"  >
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteNombre',set_value('clienteNombre'), 
                                        'id="clienteNombre"  maxlength="25" class="form-control" 
                                        placeholder="Nombre" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteApellido',set_value('clienteApellido'), 
                                        'id="clienteApellido"  maxlength="25" class="form-control"
                                        placeholder="Apellido" required');	?>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteTelefono',set_value('clienteTelefono'), 
                                        'id="mask_phone"  class="form-control" 
                                        placeholder="Teléfono" required');	?>
                            </div>
                        </div>
                    </div>
                    
                </div>
				<!--/row-->
                <br>
                <div class="row">
                   
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteCorreo',set_value('clienteCorreo'), 
                                        '  class="form-control" 
                                        placeholder="Email" required');	?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Asesor:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('asesor', $asesores, '29',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-5">Valet?:</label>
                            <div class="checkbox-list col-md-3" style="padding-top:7px">
                                <input type="checkbox" id="necesitaValet" name="necesitaValet" value="1" >
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <br><div class="row">
                <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Recordatorio:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('tipoRecordatorio', $recordatorios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                      <div class="col-md-8">
                        <div class="form-group">
                           	<?php	echo form_textarea('mensaje',set_value('mensaje'),
									'id="mensaje" class="form-control" placeholder="Comentario..." 
									style="resize: none; height:80px;"');	?>
                        </div>
                    </div>
                    
                </div>
                
                
                
                
                                            
                                            
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
											<input type="submit" class="btn btn-info" value="Guardar">
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<!-- END EXAMPLE TABLE PORTLET-->

<!-- Factor de produccion 100 % del taller en numero de horas-->

<!-- Factor de produccion 100 % del taller en numero de horas gastadas-->

<?php
$porcentaje;

$verde=$porcentaje - $tot[0]->tot;

?>
<script src="<?php echo base_url();?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/scripts/app.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
jQuery(document).ready(function() {    
 App.init();
 
});

$(function() { citas.citasOnline(); });
 
citas= {
        citasOnline: function() {

  $.ajax({
	  url:"<?php echo base_url();?>dashboard/citasPorConfirmar",
	  success:function(result){
             $("#citasOnline").html(result);
                              }
         });

}}
setInterval("citas.citasOnline()", 10000000); // Update every 10 seconds 
</script>
<?php $this->load->view('cita/js/script');?> 
 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Ocupado',   <?php echo $tot[0]->tot;?>],
          ['Disponible',     <?php echo $verde;?>],
      
        
        ]);

        var options = {
           height:'1000px',
		  width:'1000px',
		   colors:['#cc0000','#008000'],
		   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
		   legend:{position: 'top'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graficaCitas'));
        chart.draw(data, options);
      }
    </script>
    
    <script>
jQuery(document).ready(function() {
	
$('.agendar').live('click',function(){ 
var id=$(this).attr('id');
$('#wide').modal('show');    
$('input[name=fecha]').val(id);
});	
	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita//porcentajeCita?fecha=<?php echo $date;?>', // use the `url` property
           
        }


    ],
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
			
			
});
</script>