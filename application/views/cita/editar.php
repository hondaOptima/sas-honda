<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
  
    <li>
        <i class="fa fa-calendar"></i>
        <a href="<?php echo base_url(); ?>cita/?taller=<?php echo $_GET['taller'];?>">Citas</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-edit"></i>
        <a href="#">Editar</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>


<!-- BEGIN EXAMPLE TABLE PORTLET-->

<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i>Editar Cita
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        
        <?php echo form_open('cita/actualizar/'.$cita['0']->app_idAppointment.'/?taller='.$_GET['taller'].'','class="form-horizontal"'); ?>
        
            <div class="form-body">
               
                <div class="row">
                     	<?php echo form_open('cita/crear','class=""'); ?> 
                    <input type="hidden" name="fecha" value="">
                    <input type="hidden" name="referencia" value="1">                                           
                <div class="row">
                  <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Servicio:</label>
                            <div class="col-md-9">
                            
                                <?php	echo form_dropdown('servicio', $servicios, $cita['0']->app_service,
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Modelo:</label>
                            <div class="col-md-9">
<?php	echo form_dropdown('modeloVehiculo',$modelos, $cita['0']->app_vehicleModel,'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Año:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('anoVehiculo',$cita['0']->app_vehicleYear, 
										'id="mask_number" maxlenght="4" class="form-control"
                                        placeholder="Año de Vehículo" required');	?>
                                
                            </div>
                        </div>
                  
                </div>
                
                
                
                </div>
                <br>
                  <div class="row"  >
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteNombre',$cita['0']->app_customerName, 
                                        'id="clienteNombre"  maxlength="25" class="form-control" 
                                        placeholder="Nombre" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteApellido',$cita['0']->app_customerLastName, 
                                        'id="clienteApellido"  maxlength="25" class="form-control"
                                        placeholder="Apellido" required');	?>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteTelefono',$cita['0']->app_customerTelephone, 
                                        'id="mask_phone"  class="form-control" 
                                        placeholder="Teléfono" required');	?>
                            </div>
                        </div>
                    </div>
                    
                </div>
				<!--/row-->
                <br>
                <div class="row">
                   
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteCorreo',$cita['0']->app_customerEmail, 
                                        '  class="form-control" 
                                        placeholder="Email" required');	?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Asesor:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('asesor', $asesores, $cita['0']->app_desiredAdviser,
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-5">Valet?:</label>
                            <div class="checkbox-list col-md-3" style="padding-top:7px">
                                <input type="checkbox" id="necesitaValet" name="necesitaValet" value="1"  <?php if($cita[0]->app_needValet=='1'){echo 'checked="checked"';}?>>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <br><div class="row">
                <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Recordatorio:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('tipoRecordatorio', $recordatorios, $cita[0]->app_reminderType,
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                      <div class="col-md-7">
                        <div class="form-group">
                           	<?php	echo form_textarea('mensaje',$cita[0]->app_message,
									'id="mensaje" class="form-control" placeholder="Comentario..." 
									style="resize: none; height:80px;"');	?>
                        </div>
                    </div>
                    
                </div>
                
                
                
                
                                            
                                            
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
											<input type="submit" class="btn btn-info" value="Guardar">
										</div>
									</div>
                                    </form>
        <!-- END FORM-->
    </div>
</div>   
<!-- END EXAMPLE TABLE PORTLET-->


<?php $this->load->view('globales/footer'); ?>
<?php $this->load->view('cita/js/script'); ?> 