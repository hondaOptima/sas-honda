<?php $this->load->view('globales/head'); ?>

   

<div class="row" style="background-color:white">
	<div class="col-md-9 col-sm-12">
    	<div class="portlet">
        	<div class="portlet-title">
            	<div class="caption"><i class="fa fa-clock-o"></i><b>Paso 1 de 3</b> </div>
               
               
            </div>
            
            
            <div class="portlet-body">
            <form name="forma"  action="<?php echo base_url()?>cita/calpasodos" method="get">
           <div class="row">
           <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Taller de Servicio:</label>
                            <div class="col-md-9">
                            
                                <?php
								$taller= array(
								'1'=>'Ensenada',
								'2'=>'Mexicali',
								'3'=>'Tijuana',
								);
									echo form_dropdown('taller', $taller, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Servicio:</label>
                            <div class="col-md-9">
                            
                                <?php	echo form_dropdown('servicio', $servicios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Modelo:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('modeloVehiculo', $modelos, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Año:</label>
                            <div class="col-md-9">
                            <input name="anoVehiculo" id="mask_number" class="form-control"  placeholder="Año de Vehículo" maxlength="4">
                               
                                
                            </div>
                        </div>
                  
                </div>
                
                
                
                </div>
                <br>
                 <input type="submit" value="Consultar Calendario" class="btn btn-success">
                </form>
			</div>
        </div>
    </div>
	

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<!-- END EXAMPLE TABLE PORTLET-->

<!-- Factor de produccion 100 % del taller en numero de horas-->

<!-- Factor de produccion 100 % del taller en numero de horas gastadas-->

<?php
$porcentaje;

$verde=$porcentaje - $tot[0]->tot;

?>
<script src="<?php echo base_url();?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/scripts/app.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
jQuery(document).ready(function() {    
 App.init();
 
});

$(function() { citas.citasOnline(); });
 
citas= {
        citasOnline: function() {

  $.ajax({
	  url:"<?php echo base_url();?>dashboard/citasPorConfirmar",
	  success:function(result){
             $("#citasOnline").html(result);
                              }
         });

}}
setInterval("citas.citasOnline()", 10000000); // Update every 10 seconds 
</script>
<?php $this->load->view('cita/js/script');?> 
 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Ocupado',   <?php echo $tot[0]->tot;?>],
          ['Disponible',     <?php echo $verde;?>],
      
        
        ]);

        var options = {
           height:'1000px',
		  width:'1000px',
		   colors:['#cc0000','#008000'],
		   chartArea:{left:'1%',top:'5%',width:'97%',height:'100%'},
		   legend:{position: 'top'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graficaCitas'));
        chart.draw(data, options);
      }
    </script>
    
    <script>
jQuery(document).ready(function() {
	
$('.agendar').live('click',function(){ 
var id=$(this).attr('id');
$('#wide').modal('show');    
$('input[name=fecha]').val(id);
});	
	
	
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				
				 height: 250,
                editable: false,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita//porcentajeCita?fecha=<?php echo $date;?>', // use the `url` property
           
        }


    ],
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
			
			
});
</script>