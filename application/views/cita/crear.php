<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url(); ?>pizarron">Dashboard</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-calendar"></i>
        <a href="<?php echo base_url(); ?>cita">Citas</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <i class="fa fa-edit"></i>
        <a href="<?php echo base_url(); ?>cita/crear">Crear</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>


<!-- BEGIN EXAMPLE TABLE PORTLET-->

<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i>Nuevo Cita
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        
        <?php echo form_open('cita/crear','class="form-horizontal"'); ?>
        
            <div class="form-body">
        
            
               
                  <div class="row ">
				<div class="col-md-6 col-sm-6">
					<div class="portlet calendar">
						<div class="portlet-title">
							<div class="caption">
								<span class="badge badge-success">
										Paso 1
									</span>
                                    Calendario de Citas
							</div>
                            <div class="tools"><a href="" style="margin-top:-15px;" class="expand"></a></div>
                            <div class="tools">
                              <span class="fc-button fc-button-prev fc-state-default fc-corner-left" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita/crear/?date=<?php echo $ant;?>">‹</a></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class=""><?php echo $meses[date($ms)].' '.$an;?></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" unselectable="on"><span class="fc-text-arrow"><a href="<?php echo base_url()?>cita/crear/?date=<?php echo $sig;?>">›</a></span></span>
                            </div>
                            
                        
                          
                           
                           
                           
                            
                            
						</div>
						<div class="portlet-body" id="potal1" >
							<div id="calendarB">
							</div>
						</div>
					</div>
					</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="portlet tasks-widget">
						<div class="portlet-title">
							<div class="caption">
								<span class="badge badge-success">
										Paso 2
									</span>
0
							</div>
							<div class="tools">
                            <a href="" class="expand"></a>
                            </div>
							
						</div>
						<div class="portlet-body"  >
							<div class="task-content">
								<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
									<!-- START TASK LIST -->
								<div class="clearfix">
                                <?php $i=0; foreach ($lista as $dato) {
									if($dato->cal_status=='2'){
										$clas='warning';
										$txt='ban';
										$url='';
										}else{
											$clas='info';
											$txt='edit';
$url="".base_url()."cita/crear?date=".$date."&taller=".$taller."&hora=".$dato->cal_hora."&idh=".$dato->cal_ID."#tabcon";
											}
									
									?>
											
											
											
                                            
                                             <a href="<?php echo $url; ?>"  class="icon-btn btn btn-xs btn-<?php echo $clas?>" >
							<i class="fa fa-<?php echo $txt;?>" ></i>
							<div>
							<?php echo $dato->cal_hora?> a.m.
							</div>
							
							</a>
										
                                    <?php $i++;  } ?>    
                                        </div>
                                        
							</div>
							<div class="task-footer">
							
							</div>
						</div>
					</div>
				</div>
                </div>
			</div>
                       

                
				<!--/row-->
              
            <?php if(!empty($_GET['idh'])) {?>
            <div class="top-news" id="tabcon">
								<a href="#" class="btn btn-success">
								<span>
								Reservando Cita
								</span>
                                
								<em><i class="fa  fa-calendar"></i> Fecha: <?php echo $di.' de '.$meses[date($ms)].' del '.$an;?> a las <?php echo $_GET['hora'];?></em>
								<em>
								<i class="fa fa-tags"></i>
								Servicio: </em>
								<i class="fa fa-book top-news-icon"></i>
								</a>
								
								
							</div>
            
       
                            <?php }?>
                            
                    
                
                
                            <div   <?php if(empty($_GET['idh'])) {?> style="display:none" <?php } ?>>
                            <div class="portlet-body form">
                             <div class="form-body">
                            <div class="portlet calendarxxxx">
                            <div class="portlet-title">
							<div class="caption">
						<span class="badge <?php if(empty($_GET['idh'])) {?>badge-danger<?php }else{ ?>badge-success<?php } ?>">
										Paso 3
									</span>
                                    Información del contacto
							</div>
                            </div>
                            	<div class="portlet-body"  >
                <div class="row"  >
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteNombre',set_value('clienteNombre'), 
                                        'id="clienteNombre"  maxlength="25" class="form-control" 
                                        placeholder="Nombre" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteApellido',set_value('clienteApellido'), 
                                        'id="clienteApellido"  maxlength="25" class="form-control"
                                        placeholder="Apellido" required');	?>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteTelefono',set_value('clienteTelefono'), 
                                        'id="mask_phone"  class="form-control" 
                                        placeholder="Teléfono" required');	?>
                            </div>
                        </div>
                    </div>
                    
                </div>
				<!--/row-->
                <div class="row">
                   
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('clienteCorreo',set_value('clienteCorreo'), 
                                        '  class="form-control" 
                                        placeholder="Email" required');	?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Asesor:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('asesor', $asesores, '29',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-5">Necesita Valet:</label>
                            <div class="checkbox-list col-md-3" style="padding-top:7px">
                                <input type="checkbox" id="necesitaValet" name="necesitaValet" value="1" >
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                 <h4 class="form-section">Vehículo</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Modelo:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('modeloVehiculo', $modelos, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Año:</label>
                            <div class="col-md-9">
                                <?php	echo form_input('anoVehiculo',set_value('anoVehiculo'), 
										'id="mask_number" class="form-control"
                                        placeholder="Año de Vehículo" required');	?>
                                
                            </div>
                        </div>
                    </div><div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Recordatorio:</label>
                            <div class="col-md-9">
                                <?php	echo form_dropdown('tipoRecordatorio', $recordatorios, '1',
										'id="selectError" class="select2_category form-control" 
										data-rel="chosen" required');	?>
                            </div>
                        </div>
                    </div>
                </div>
        </hr>
                
				<!--/row-->
                <div class="row">
                   
                    <!--/-->
                    
              
                    <div class="col-md-12">
                        <div class="form-group">
                         
                            <div class="col-md-12">
							<?php	echo form_textarea('mensaje',set_value('mensaje'),
									'id="mensaje" class="form-control" placeholder="Mensaje..." 
									style="resize: none; height:80px;"');	?>
                            </div>
                        </div>
                    </div>
                    <!--/-->
                    
                </div>
                </div>
				<!--/row-->
                 <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                          
                           <?php if(!empty($_GET['idh'])) {?>
                            <button type="submit" class="btn btn-success">Añadir</button>
                            <a href="<?php echo base_url(); ?>cita">
                            	<button type="button" class="btn btn-default">Cancelar</button>
                            </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
                
				<!--/row-->
				<!--/section-->
               
				<!--/row-->
            </div>
           
            
        <?php echo form_close(); ?>
        
        <!-- END FORM-->
    </div>
</div>   
<!-- END EXAMPLE TABLE PORTLET-->


<?php $this->load->view('globales/footer'); ?>
<?php $this->load->view('cita/js/script'); ?> 
<script>
jQuery(document).ready(function() {
  var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if ($('#calendarB').width() <= 400) {
                $('#calendarB').addClass("mobile");
                h = {
                    left: '',
                    center: '',
                    right: ''
                };
            } else {
                $('#calendarB').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: '',
                        center: '',
                        left: ''
                    };
                } else {
                    h = {
                        left: '',
                        center: '',
                        right: ''
                    };
                }               
            }

            $('#calendarB').fullCalendar('destroy'); // destroy the calendar
			
            $('#calendarB').fullCalendar({ //re-initialize the calendar
                disableDragging: true,
                header: h,
				
				 height: 295,
                editable: true,
                 eventSources: [

        // your event source
        {
            url: '<?php echo base_url()?>cita//porcentajeCita?fecha=<?php echo $date;?>', // use the `url` property
           
        }


    ],
				
            });
		
			$('#calendarB').fullCalendar('gotoDate', '<?php echo $an;?>','<?php echo $ms;?>','<?php echo $di;?>');
			
			
			
});
</script>