<div class="modal fade" id="wideSin" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><b>Nueva Cita (Sin Cita)</b></h4>
      </div>
      <div class="modal-body"> <?php echo form_open('cita/crear?taller='.$taller.'','class=""'); ?>
        <input type="hidden" name="referencia" value="2">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Fecha:</label>
              <div class="col-md-9">
                <?= form_input('fsin', $date, 'id="" class="form-control" placeholder="Fecha" readonly="readonly" required');	?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Hora:</label>
              <div class="col-md-9">
                <input name="fechaSin" class="form-control" readonly="readonly">
              </div>
            </div>
          </div>
          <!--/-->

        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Servicio:</label>
              <div class="col-md-9">
                <?php	echo form_dropdown('servicio', $servicios, '1',
										'id="selectError" class="select2_category form-control"
										data-rel="chosen" required');	?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Modelo:</label>
              <div class="col-md-9">
                <?php	echo form_dropdown('modeloVehiculo', $modelos, '0',
										'id="selectError" class="select2_category form-control"
										data-rel="chosen" required');	?>
              </div>
            </div>
          </div>
          <!--/-->
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Año:</label>
              <div class="col-md-9">
                <?php	echo form_input('anoVehiculo',set_value('anoVehiculo'),
										'id="mask_number" maxlenght="4" class="form-control"
                                        placeholder="Año de Vehículo" required');	?>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row"  >
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Nombre:</label>
              <div class="col-md-9">
                <?php	echo form_input('clienteNombre',set_value('clienteNombre'),
                                        'id="clienteNombre"  maxlength="25" class="form-control"
                                        placeholder="Nombre" required');	?>
              </div>
            </div>
          </div>
          <!--/-->
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Apellido:</label>
              <div class="col-md-9">
                <?php	echo form_input('clienteApellido',set_value('clienteApellido'),
                                        'id="clienteApellido"  maxlength="25" class="form-control"
                                        placeholder="Apellido" required');	?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Teléfono:</label>
              <div class="col-md-9">
               <input type="radio" name="tipotel" value="celular">Celular <input type="radio" value="casa" name="tipotel">Casa
                <?php	echo form_input('clienteTelefono',set_value('clienteTelefono'),
                                        'id="mask_phone"  class="form-control"
                                        placeholder="Teléfono" required');	?>
              </div>
            </div>
          </div>
        </div>
        <!--/row-->
        <br>
        <div class="row">

          <!--/-->
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Email:</label>
              <div class="col-md-9">
                <?php	echo form_input('clienteCorreo',set_value('clienteCorreo'),
                                        '  class="form-control emailc"
                                        placeholder="Email" required');	?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Asesor:</label>
              <div class="col-md-9">
                <?php	echo form_dropdown('asesor', $asesores, '67',
										'id="selectError" class="select2_category form-control"
										data-rel="chosen" required');	?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <div class="checkbox-list col-md-9" style="padding-top:7px">
                Servicio de Transporte<input style="margin-left:3px;"  type="checkbox" id="necesitaValet" name="necesitaValet" value="0" >
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-3">Recordatorio:</label>
              <div class="col-md-9">
                <?php	echo form_dropdown('tipoRecordatorio', $recordatorios, '1',
										'id="selectError" class="select2_category form-control"
										data-rel="chosen" required');	?>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="form-group">
              <?php	echo form_textarea('mensaje',set_value('mensaje'),
									'id="mensaje" class="form-control" placeholder="Comentario..."
									style="resize: none; height:80px;"');	?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <input type="submit" class="btn btn-info" value="Guardar">
      </div>
    </div>
    </form>
     <!-- /.modal-content -->
  </div>
</div>
<!-- /.modal-dialog -->
