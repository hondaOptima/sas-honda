<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='pizarron';$this->load->view('globales/menu',$menu);
//$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
//header("refresh:300; url=$self"); //Refrescamos cada 300 segundos
 ?>
<script src="<? echo base_url();?>/assets/scripts/jquery-1.12.1.min.js"></script>
<script src="<? echo base_url();?>/assets/scripts/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>/assets/css/managec-body.css">
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>/assets/css/jquery-ui.css">
<input type="hidden" id="inp-hws" value="<?php echo $wshop; ?>" />
<h1 class="description">
  Monitoreo de las campa&ntilde;as.
</h1>
<style>
    #yearMonthInput{
        width: 20% !important;
        margin-left: 10px;
    }
</style>
<div class="divs-mngc-color1 divs-mngc-color">POSIBLES</div>
<div class="divs-mngc-color2 divs-mngc-color">EFECTUADAS</div>
<div class="divs-mngc-color3 divs-mngc-color">PENDIENTES</div>
 <select class="form-control" id="yearMonthInput"></select>
<div class="box-table">
<table>
    <thead>
          <tr> 
              <td>Dias</td>
          <?php foreach($asesores as $element) {?>
              <td><?php echo $element->sus_name ?></td>
              <?php } ?>
          </tr>
    </thead>
    <tbody id="managec-tbody"> 
        <?php for($i = 1; $i < count($datos); $i++){ ?>
        <tr>
            <?php 
                $yf = date("Y");
                $mf = date('M');
                $df = date('Y-m-01', strtotime($mf.' 21, '.$yf));
                $dateaddf = strtotime("+".($i-1)." days", strtotime($df));
                $datetosearchf = date("Y-m-d", $dateaddf);
                $timestamp = strtotime($datetosearchf);
                $dayf = date('D', $timestamp);
                $dayEsp = '';
                switch ($dayf) {
                    case 'Mon':
                        $dayEsp = 'Lunes';
                        break;
                    case 'Tue':
                        $dayEsp = 'Martes';
                        break;
                    case 'Wed':
                        $dayEsp = 'Miercoles';
                        break;
                    case 'Thu':
                        $dayEsp = 'Jueves';
                        break;
                    case 'Fri':
                        $dayEsp = 'Viernes';
                        break;
                    case 'Sat':
                        $dayEsp = 'Sabado';
                        break;
                    case 'Sun':
                        $dayEsp = 'Domingo';
                        break;
                }
            ?>
            <td><?php echo $dayEsp.' - '.$i; ?></td>
            <?php if($datos[$i-1] == 'null'){ ?>
            
            <?php }else{ foreach($datos[$i-1] as $row){ 
                $y = date("Y");
                $m = date('M');
                $d = date('Y-m-01', strtotime($m.' 21, '.$y));
                $dateadd = strtotime("+".($i-1)." days", strtotime($d));
                $datetosearch = date("Y-m-d", $dateadd);
            ?>
            <td>
                <div class="posibles table-mngc col-3" title="POSIBLES" >
                    <?php echo $row->posibles ?> 
                </div>
                <div class="efectuadas table-mngc col-3" title="EFECTUADAS" >
                    <?php echo ($row->posibles - $row->pendientes) ?> 
                </div>
                <div user="<?php echo $row->sus_idUser ?>" date="<?php echo $datetosearch ?>" class="pendientes table-mngc col-3" title="PENDIENTES" >
                    <?php echo $row->pendientes ?> 
                </div>
            </td>
            
            <?php }} ?>
        </tr>
        <?php } ?>
    </tbody>
</table> 
</div>

<div style="display: none;" id="modal-managec" class="div-back-delete">
    <div class="div-modal-delete">
        <div class="title-delete">
            
            <div>
                Campa&ntilde;as pendientes
                <hr color="black">
                <div id="div-result-manage">
                    
                </div>
            </div>
            <div> 
            	
            </div>
        </div>
        <div>
         
        </div>
    </div>
</div>

<div style="display:none" id="div-spin" class="back-spin" >
    
</div>

<style>
.ui-datepicker-calendar {
    display: none;
    }
</style>

<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('pizarron/js/script');?>


<script>
    $(document).on('click','.pendientes',function(){
        $.ajax({
          url:"http://hsas.gpoptima.net/ajax/managec/get_pendientes.php?asesor="+$(this).attr('user')+"&date="+$(this).attr('date'),
          success:function(result){
              if(result != ''){
                  $('#div-result-manage').html(result);
              }
            }
             });
        $('#modal-managec').fadeIn();
    });
    
    

$(document).ready(function()
{
    var mouse_is_inside = false;
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    for (i = new Date().getFullYear() ; i > 2008; i--) {
        var nowdate = new Date();
        var year = nowdate.getFullYear();
        var month = nowdate.getMonth();
        $.each(months, function (index, value) {
            if(index==month&&i==year){
                $('#yearMonthInput').append($('<option selected>').val(("0" + (index+1)).slice(-2)+"_"+value + "_" + i).html(value + " " + i));
            }else{
                $('#yearMonthInput').append($('<option >').val(("0" + (index+1)).slice(-2)+"_"+value + "_" + i).html(value + " " + i));
            }
            
        });                
    }
    
    
    $('.div-modal-delete').hover(function(){ 
        mouse_is_inside=true; 
    }, function(){ 
        mouse_is_inside=false; 
    });

    $("body").mouseup(function(){ 
        if(! mouse_is_inside) $('#modal-managec').fadeOut();
    });
});
    
    $('#yearMonthInput').change(function(){
        var spinner = '<div class="spin-manage-div" ><image class="spin-manage" src="http://hsas.gpoptima.net/assets/images/descarga.svg" /> Consultando reporte...</div>';
        
        $('#div-spin').html(spinner);
        $('#div-spin').fadeIn();
        var num = $('#yearMonthInput option:selected').val().substr(0,2);
        var month = $('#yearMonthInput option:selected').val().substr(3,3);
        var year = $('#yearMonthInput option:selected').val().substr(7,4);
        $.ajax({
          url:"http://hsas.gpoptima.net/ajax/managec/getByNewMonth.php?month="+month+"&year="+year+"&wshop="+$('#inp-hws').val()+'&num='+num,
            dataType: "json",
          success:function(result){
              if(result.status != "success"){
                  $('#managec-tbody').empty();
                  $('#managec-tbody').html(result.data);
                  $('#div-spin').fadeOut();
              }
            }
             });
    });
    
    
</script>