<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='clientes';$this->load->view('globales/menu',$menu); ?>
<ul class="page-breadcrumb breadcrumb">
   
    <li>
        <i class="fa fa-male"></i>
        <a href="<?php echo base_url();?>estadisticas">Clientes</a>
        <i class="fa fa-angle-right"></i>
    </li>
     <li>
        <i class="fa fa-book"></i>
        <a href="<?php echo base_url();?>estadisticas">Historia</a>
        <i class="fa fa-angle-right"></i>
    </li>
    
    
    
</ul>


<!-- BEGIN OVERVIEW STATISTIC BARS-->


                            
                            
<div class="row stats-overview-cont">

    <div class="col-md-6 col-sm-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa  fa-file-text"></i>Ordenes de Servicio
							</div>
							
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1_1">
									<div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible="0">
										<ul class="feeds">
										<?php 
										date_default_timezone_set('America/Mexico_City');
											
											if(ISSET($servicios)){
											foreach($servicios as $servicio){
												$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
                                                $date = new DateTime($servicio->seo_date);												                                              
												
												echo '											
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-success">
																	<i class="fa fa-check"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	'.$servicio->set_name.'
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															 '.$date->format('d')."/".$meses[$date->format('n')-1]. "/".$date->format('Y').'
														</div>
													</div>
												</li>';
												
											}																					
											}
											foreach($serviciosf as $servicio){												
												echo '											
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-warning">
																	<i class="fa fa-warning"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	'.$servicio->set_name.'
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">															 
														</div>
													</div>
												</li>';
												
											}											
										
										?>												
                                            
                                            
										</ul>
									
								
								
								</div>
							</div>
							<!--END TABS-->
						</div>
					</div>
				</div></div>
                
                
                
                <div class="col-md-6 col-sm-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa  fa-money"></i>Presupuestos
							</div>
						</div>
						<div class="portlet-body" style="height:271px;">
							<!--BEGIN TABS-->
							<div class="tab-content">
								<table class="table">
									<thead>
										<tr>
											<th>Fecha</th>
											<th>Nombre</th>
											<th>Sub-Total</th>
											<th>Mano de Obra</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									if(ISSET($presupuestos)){
										if(count($presupuestos) > 0)
										{
											foreach($presupuestos as $presupuesto){
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
											$date = new DateTime($presupuesto->seo_date);
											
											echo '
											<tr>
											<td>'.$date->format('d')."/".$meses[$date->format('n')-1]. "/".$date->format('Y').'</td>
											<td>'.$presupuesto->ess_description .'</td>
											<td>'.$presupuesto->ess_total_price .'</td>
											<td>'.$presupuesto->ess_handwork .'</td>
											<td>'.$presupuesto->ess_total_price .'</td>
											
											</tr>
											';
											}
										}
										else{
											
											echo '<tr><td style="    text-align: center;
    font-size: 32pt;" colspan="5">NO EXISTEN PRESUPUESTOS</td></tr>';
										}
									}
									else{
											
											echo '<tr><td style="    text-align: center;
    font-size: 32pt;" colspan="5">NO EXISTEN PRESUPUESTOS</td></tr>';
										}
									?>
									</tbody>
								</table>							
							</div>
							<!--END TABS-->
						</div>
					</div>
				</div>
                
                
                
                
                <div class="col-md-12 col-sm-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-camera"></i>Banco de Fotos
							</div>
							
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							
							<div class="tab-content">
								
								<div class="tab-pane active" id="tab_1_3">
									<div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible1="1">
									<?php 
									if(isset($imagenes))
									{
										foreach($imagenes as $imagen){
											
											echo '<div class="col-md-3 user-info">
													<img alt="" src="'.$imagen.'" class="img-responsive"/>
													<div class="details">												
													</div>
												</div>';
										}	
									}									
									
									?>										
									</div>
								</div>
							</div>
							<!--END TABS-->
						</div>
					</div>
				</div>
                
               
              <!--  <div class="col-md-6 col-sm-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell"></i>Banco de Archivos PDF
							</div>
							<div class="tools">
							
							</div>
						</div>
						<div class="portlet-body">
							
						
							<div class="tab-content">
								
								<div class="tab-pane active" id="tab_1_3">
									<div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
											<div class="col-md-6 user-info">
												<img alt="" width="70px;"  src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 29 Jan 2013 10:45AM
													</div>
												</div>
											</div>
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 10:45AM
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 12:45PM
													</div>
												</div>
											</div>
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 11:55PM
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 user-info">
												<img alt=""  width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 12:45PM
													</div>
												</div>
											</div>
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 11:55PM
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 12:45PM
													</div>
												</div>
											</div>
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 11:55PM
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 12:45PM
													</div>
												</div>
											</div>
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 11:55PM
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 12:45PM
													</div>
												</div>
											</div>
											<div class="col-md-6 user-info">
												<img alt="" width="70px;" src="http://www.clker.com/cliparts/R/X/6/k/w/S/pdf-file-icon-hi.png" class="img-responsive"/>
												<div class="details">
													
													<div>
														 19 Jan 2013 11:55PM
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>							
						</div>
					</div>
				</div>-->
				</div>                                                                                                             
                
</div>
<!-- END OVERVIEW STATISTIC BARS-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('clientes/historia/js/script');?>