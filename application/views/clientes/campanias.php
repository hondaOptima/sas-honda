<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='clientes';$this->load->view('globales/menu',$menu); ?>
<ul class="page-breadcrumb breadcrumb">
   
    <li>
        <i class="fa fa-group"></i>
        <a href="<?php echo base_url();?>clientes">Clientes</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>

<?php echo $flash_message;?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
    <div class="portlet-title">
		<div class="caption">
			<i class="fa fa-users"></i>Buscar Campañas
		</div>
        
    </div>
    <div class="portlet-body">
		<table class="table table-striped table-bordered table-hover" id="sample_2x">
			<thead>
				<tr style="font-weight:bold">
                <th>
				<b>CD</b>
				</th>
				<th>
				<b>No. VIN</b>
				</th>
				<th>
				<b>Modelo</b>
				</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
        <br><br><br>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('usuario/js/script');?> 
<script type='text/javascript'>

	$(document).ready(function() {
	$('#sample_2x').dataTable( {
		
			
		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
						"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>ajax/clientes/campanias.php",
		
                    
	} );} );
	</script>