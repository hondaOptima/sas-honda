<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='clientes';$this->load->view('globales/menu',$menu); ?>
<ul class="page-breadcrumb breadcrumb">

    <li>
        <i class="fa fa-group"></i>
        <a href="<?php echo base_url();?>clientes">Clientes</a>
        <i class="fa fa-angle-right"></i>
    </li>
</ul>

<?php echo $flash_message;?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet">
    <div class="portlet-title">
		<div class="caption">
			<i class="fa fa-users"></i>Lista de Clientes
		</div>
        <div class="actions">
         <a href="<?php echo base_url();?>orden/cancelarOrden/" class="btn btn-small btn-warning">Cancelar Orden</a>
        <a href="<?php echo base_url();?>orden/clienteNuevo/" class="btn btn-small btn-success">El Cliente no exite en la base de datos</a>
        </div>
    </div>
    <div class="portlet-body">
		<table class="table table-striped table-bordered table-hover" id="sample_2x">
			<thead>
				<tr style="font-weight:bold">
                <th>
				<b>CD</b>
				</th>
				<th>
				<b>No. Cliente</b>
				</th>
				<th>
				<b>Cliente</b>
				</th>
				<th>
				<b>Email</b>
				</th>
				<th>
				<b>Teléfono</b>
				</th>
				<th>
				<b>Vehículo</b>
				</th>

				<th>
				<b>Fecha de Venta</b>
				</th>

				<th>
				<b>Acciones</b>
				</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
        <br><br><br>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('globales/footer');?>
<?php $this->load->view('usuario/js/script');?>
<script type='text/javascript'>

	$(document).ready(function() {
	$('#sample_2x').dataTable( {


		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
						"bDestroy": true,
		"sAjaxSource": "<?php echo base_url();?>ajax/clientes/clientes.php",


	} );} );
	</script>
