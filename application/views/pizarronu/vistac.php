<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='pizarron';$this->load->view('globales/menu',$menu);
//$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
//header("refresh:300; url=$self"); //Refrescamos cada 300 segundos
 ?> 
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>/assets/css/pizarron-style.css">

<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-desktop"></i> <a href="#">Pizarr&oacute;n Electr&oacute;nico</a> </li>
  <li class="pull-right">
    <div class="actions" style="margin-top:-3px;">
      <div class="btn" style="background:#C00;color:white; border:2px solid #000;width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">En Espera</div>
      <div class="btn" style=" background:#fcb322; color:white; border:2px solid #000;width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;" >En Rampa</div>
      <div class="btn"  style="background-color:#FF0; border:2px solid #000;color:#333; width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">Pend. X Auto</div>
      <div class="btn"  style=" background:#3cc051; color:white; border:2px solid #000; width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">En Prueba</div>
      <div class="btn" style="background-color:green;  color:white; border:2px solid #000;width:85px; height:25px; font-size:12px; padding: 3px 6px 3px 6px;">Terminado</div>
    </div>
  </li>
  <li class="pull-right"> </li>
</ul>
<?php
$estatus=array('5'=>'Sin Previa','6'=>'Previado');
function contar($array,$id){
$res=0;
foreach($array as $ar){
	if($ar->seo_technicianTeam==$id)
	$res++;
	}	
	return $res;
	}
function contar_piz_inv($array,$id){
$res=0;
foreach($array as $ar){
	if($ar->pii_tecnico==$id)
	$res++;
	}	
	return $res;
	}	
?>
<?php
date_default_timezone_set('America/Tijuana');
$hor=date('H:i');
list($h,$m)=explode(':',$hor);
if($h<12){$ty='AM';}else{$ty='PM';}
$hora=$hor.' '.$ty;

function servicios($id,$array){
$ser='';
$sertipo = 0;
$suma=0;
$lser='';
$como='';
$comp='';
foreach($array as $ar){
	if($ar->ssr_IDNULL==$id){
		if($ar->ssr_tipo=='master'){
		$ser=$ar->set_name;}
		$suma+=$ar->ser_approximate_duration;
		$lser.=$ar->set_name.', ';
		$como=$ar->seo_comments;
		$comp=$ar->app_message;
    $sertipo = $ar->app_service;
		}
	}	
	
	return $lista=array($lser,$ser,$suma,$como,$comp,$sertipo);
	}
	
function serviciossv($id,$array){
$suma=0;
$lser='';
foreach($array as $ar){
	if($ar->sev_id_orden==$id){
		$suma+=$ar->sev_tiempo;
		$lser.=$ar->sev_nombre.', ';
		}
	}	
return $lista=array($lser,$suma);
	}	
	
	
function asesi($array,$id){
$ok='no';	
foreach($array as $ar){
if($ar->seo_technicianTeam==$id)
$ok='ok';	
}	

return $ok;
	}
	
function asesi_inv($array,$id){
$ok='no';	
foreach($array as $ar){
if($ar->pii_tecnico==$id)
$ok='ok';	
}	

return $ok;
	}	

function selecGar($array,$orden)
{
    $cat = array();
    foreach($array as $gar){
        if($gar->sei_ID == $orden){
            $cat[]= array('garantia'=>$gar->sev_vin);
        }
    }
    return $cat;
}
	
	
function getpizuser($array,$id){
$cat=array();	
foreach($array as $ar){
if($ar->seo_technicianTeam==$id)
$cat[]=array(  'extension' => $ar->extension, 'garexist'=>$ar->garexist,'campania'=>$ar->camp,'torre'=>$ar->seo_tower,'idorden'=>$ar->seo_idServiceOrder,'prometido'=>$ar->seo_promisedTime,
'id'=>$ar->piz_ID,'lavado'=>$ar->piz_status_lavado,'mecanico'=>$ar->piz_status_mecanico,'hlavado'=>$ar->piz_entrega_lavado,'surtido'=>$ar->piz_status_surtido,'vin'=>$ar->seo_vehicle);	
}	

return $cat;
	}	

function getpizuserinv($array,$id){
$cat=array();	
foreach($array as $ar){
if($ar->pii_tecnico==$id)
$cat[]=array('id'=>$ar->pii_ID,'vin'=>$ar->pii_vin,'torre'=>$ar->pii_torre,'estado'=>$ar->pii_estado,'ubicacion'=>$ar->pii_ubicacion,'tiempo'=>$ar->pii_servicio);	
}	

return $cat;
	}		
	
?>
<!-- BEGIN CONTENIDO--> 
<?php echo $flash_message;
//print_r($pizarron);
?>
<div class="row">
<div class="col-md-12">
<div class="portlet">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-desktop"></i>Pizarr&oacute;n Electr&oacute;nico </div>
    <div class="tools">
    <form method="get" action="<?php echo base_url();?>pizarron">
      <div class="col-md-6">
        <div  class="input-group input-small date " id="date-pickerbb" data-date-format="dd-mm-yyyy" >
          <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
          Fecha:
          <?php echo $fecha;?>
          <i style="cursor:pointer" class="fa fa-calendar  input-group-btn"></i> 
          </div>
      </div>
      <div class="col-md-1">
       <?php if($_SESSION['sfrol']<=2) {?>
    
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    <?php } ?>
      </div>
      </form>
      

      <style>
        .cover-garantia{
          position: fixed;
          top: 0;
          left: 0;
          background: rgba(149, 149, 149, 0.7); 
          z-index: 1;
          width: 100%;
          height: 100%;
        }

        .modal-garantia{
          position: fixed;
          width: 50%;
          height: 300px;
          box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
          top: 25%;
          background: white;
          color:rgba(53, 116, 222, 1); 
          left: 25%;
          text-align: center;
        }
        .modal-garantia h2{
          color:gray;
        }

        .alert{
          margin: 0 !important;
          border-radius: 0 !important;
        }

        .middle-garantia{
          width:50%;
          height:300px;
          float:left;
          
        }

        .middle-garantia div{
          color:black !important;
          font-weight:bold !important;
          background: rgba(247, 248, 252, 1) !important;
          font-size:1.2em !important;
        }
      </style>
      
      <div style="display:none" class="cover-garantia">
            <div class="modal-garantia">

              <div class="middle-garantia">
                <h2>Detalle de la garant&iacute;a:<h3 id="h2-vin"></h3></h2><hr>
                <div class="tipo-gar alert alert-info"></div>
                <div class="tiempo-gar alert alert-info"></div>
                <div class="kilometros-gar alert alert-info"></div>
              </div>

              <div class="middle-garantia">
                <h2>KM actual:<h3 id="h2-km"></h3></h2><hr>
                <div class="tipo-ext alert alert-info"></div>
                <div class="tiempo-ext alert alert-info"></div>
                <div class="kilometros-ext alert alert-info"></div>
              </div>

            </div>
      </div>

    </div>

    </div>
    </div>
    <div class="portlet-body" style=" overflow:scroll">
      <table class="table table-striped table-bordered  table-hover "  id="sample_2" >
        <thead>
          <tr style="font-weight:bold">
            <th width="110px;" style="border-top:2px solid #333; border-right:2px solid #333; border-left:2px solid #333; text-align:center "> <b>T&eacute;cnicos</b> </th>
            <th width="130px;" style="border-top:2px solid #333; border-bottom:2px solid #333; border-right:2px solid #333; text-align:center  " > <b>Descripci&oacute;n</b> </th>
            <?php
                                for($z=1; $z<10; $z++){
									echo'<th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Asignaci&oacute;n '.$z.'</b></th>';
									} 
								?>
                                <th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Acumulado</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($usuarios as $us) {
					if($us->wor_idWorkshop==$taller and $us->rol_idRol==4 and $us->sus_isActive==1 and $us->sus_email!='NA@GMAIL.COM'){
							  ?>
          <tr class="odd gradeX"  >
            <td rowspan="8"  width="110px"  style="border: 2px solid #000; text-align:center"><img src="<?php echo base_url()?>ajax/asesores/thumb.php?file=../../images/empleados/<?php echo $us->sus_foto;?>&size=110" ><br><?php 
			
			
			echo $us->sus_name.' '.$us->sus_lastName;?>
            </td>
            <td style="border-right:2px solid #333; text-align:right"><b> Torre </b></td>
            <?php 
			$sum=0;
			$asesi=asesi($pizarron,$us->sus_idUser);
			if('ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									//echo 'si';
									 for($i=0; $i<$totuno; $i++){
										 
									$pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);
									 $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv);	 
									$sum+=$ser[2] + $sersv[1];	 
									?>
            <?php if($pizuser[$i]['garexist'] == 1){ ?>
                <td title="<?php echo $pizuser[$i]['vin'];?>" id="<?php echo $pizuser[$i]['idorden'];?>" style="font-weight:bold; cursor:pointer; font-size:16px;  border-right: 2px solid #333;background-color: rgba(255, 222, 52, 0.5);padding-left:5px; " >
                   <i id="<?php echo $pizuser[$i]['idorden'];?>" class="<?php if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 ){ echo 'cambiardia';} ?>"> <?php echo $pizuser[$i]['torre'];?></i><i  class="fa fa-tags tag-garantia" orden="<?php echo $pizuser[$i]['idorden'];?>" onclick="clickGarantia(this)" style="color:rgba(53, 116, 222, 1);margin-left:30px;font-size:1.4em;margin-top:8px;"></i>
                </td>
            <?php }else{ if($pizuser[$i]['extension'] == 1){?>

                <td title="<?php echo $pizuser[$i]['vin'];?>" id="<?php echo $pizuser[$i]['idorden'];?>" style="font-weight:bold; cursor:pointer; font-size:16px;  border-right: 2px solid #333;background-color: rgba(255, 222, 52, 0.5);padding-left:5px; " >
                   <i id="<?php echo $pizuser[$i]['idorden'];?>" class="<?php if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 ){ echo 'cambiardia';} ?>"> <?php echo $pizuser[$i]['torre'];?></i><i  class="fa fa-tags tag-garantia" orden="<?php echo $pizuser[$i]['idorden'];?>" onclick="clickGarantia(this)" style="color:green;margin-left:30px;font-size:1.4em;margin-top:8px;"></i>
                </td>
                
              <?php }else{ ?>
                <td title="<?php echo $pizuser[$i]['vin'];?>" align="center" class="<?php if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 ){ echo 'cambiardia';} ?>" id="<?php echo $pizuser[$i]['idorden'];?>" style="font-weight:bold; cursor:pointer; font-size:16px;  border-right: 2px solid #333;" >
                <?php echo $pizuser[$i]['torre'];?>
            </td>
            <?php } }?>

            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php } 
			echo ' <td align="center" rowspan="8"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }
						echo ' <td align="center" rowspan="8"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';

			
			} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Servicio</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										$pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);	
									  $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv); 
                                    
										 ?>
								
            <td title="<?php echo $ser[0].$sersv[0];?>" style=" cursor:help;border-right: 2px solid #333; text-align:center">
			<?php echo $ser[1];
			
			if($ser[3]=='' && $ser[4]==''){
                if($pizuser[$i]['campania']){ ?>
            <br/>
                <u style="color:#f50e16"><h4 orden="<?php echo $pizuser[$i]['idorden']; ?>" class="boletin" > CAMPA&Ntilde;A <i class="fa fa-comments-o " ></i></h4></u>

                <?php } 
            } 
			else{
			?>
            <?php if($ser[5] == 51){ ?>
                </br>
                <a target="_blank" href="http://hsas.gpoptima.net/seminuevos?id=<?php echo $pizuser[$i]['idorden']; ?>"><u style="color:rgba(212, 127, 58, 1);"><h4 orden="<?php echo $pizuser[$i]['idorden']; ?>" class="" > EVALUACION <i class="fa fa-check-circle " ></i></h4></u></a>
            <?php }else{ ?>
            <i style="background-color:yellow" id="<?php echo $pizuser[$i]['idorden']; ?>" class="fa fa-comments-o getComentarios"></i>
                
                <?php if($pizuser[$i]['campania']){ ?>
            <br/>
                <u style="color:#f50e16"><h4 orden="<?php echo $pizuser[$i]['idorden']; ?>" class="boletin" > CAMPA&Ntilde;A <i class="fa fa-comments-o " ></i></h4></u>

                <?php } ?>
                </br>
                <a target="_blank" href="http://hsas.gpoptima.net/multipuntos/index2/<?php echo $pizuser[$i]['idorden']; ?>"><u style="color:rgba(26, 151, 224, 1); "><h4 orden="<?php echo $pizuser[$i]['idorden']; ?>" class="" > MULTIPUNTOS <i class="fa fa-check-circle " ></i></h4></u></a>
            </td>
              <?php }} ?>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="border-right: 2px solid #333; text-align:center"></td>
            <?php } 
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center"></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Tiempo Tabulado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser);
									//print_r($pizuser);	 
									 $ser=servicios($pizuser[$i]['idorden'],$servicios);
									 $sersv=serviciossv($pizuser[$i]['idorden'],$serviciossv); 
									?>
            <td style=" border-right: 2px solid #333; text-align:center"><?php echo $ser[2] + $sersv[1];?></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Hora Promesa</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 
										  $pizuser=getpizuser($pizarron,$us->sus_idUser);
									
									?>
            <td style=" border-right: 2px solid #333; text-align:center">
			<?php 
			echo $pizuser[$i]['prometido'];
			if($pizuser[$i]['vin']!=""){ echo 'N/A';}
			?>
            </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Entregado a Lavado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										   $pizuser=getpizuser($pizarron,$us->sus_idUser);
									?>
            <td style=" border-right: 2px solid #333; text-align:center">
            <?php 
			if($pizuser[$i]['vin']!=""){ echo 'N/A';}
			else{
			if($pizuser[$i]['hlavado']==''){
				echo'<div class="savehora" style="cursor:pointer;"  id="'.$pizuser[$i]['id'].'">Cargar hora...</div>';
				}
			else{
			echo'<div class="savehorano" style="cursor:pointer;"  id="'.$pizuser[$i]['id'].'">'.$pizuser[$i]['hlavado'].'</div>';		
				}}?>
          </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Estatus Mec&aacute;nico</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
							
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										$pizuser=getpizuser($pizarron,$us->sus_idUser); 
									?>
            <td style="font-weight:bold; border-right: 2px solid #333; "><div  class="btn-group" style="width:100px; height:19px;">
                <button class="btn  btn-xs dropdown-toggle" style=" width:100%;height:19px; background:<?php echo $arraystatus[1][ $pizuser[$i]['mecanico']];?>; color:<?php if($arraystatus[1][ $pizuser[$i]['mecanico']]=='#FF0'){echo'#333';}else{echo 'white';} ?> " type="button" data-toggle="dropdown"> <?php echo $arraystatus[0][ $pizuser[$i]['mecanico']];?><i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background:#C00; color:white;"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=1&fecha=<?php echo $fecha;?>">En Espera</a> </li>
                  <li style="background-color:#fcb322;"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=2&fecha=<?php echo $fecha;?>">En Rampa</a> </li>
                  <li style="background-color:#FF0;" > <a href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=3&fecha=<?php echo $fecha;?>">Pend. X Auto</a> </li>
                  <li style="background-color:#3cc051"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=4&fecha=<?php echo $fecha;?>">En Prueba</a> </li>
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=5&fecha=<?php echo $fecha;?>">Terminado</a> </li>
                    
                    <li style="background-color:green"> <a style="color:white" onclick="previarConRadio(<?php echo $pizuser[$i]['id'];?>,'piz_status_mecanico',8,'<?php echo $fecha;?>',<?php echo $pizuser[$i]['idorden']; ?>, '<?php echo $pizuser[$i]['vin']; ?>')">Previado</a> </li>
                    <!--onclick="previarConRadio(<?php echo $pizuser[$i]['id'];?>,'piz_status_mecanico',8,<?php echo $fecha;?>)"-->
                    <!--href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_mecanico&value=5&fecha=<?php echo $fecha;?>&vin=<?= $pizuser[$i]['vin']?>"-->
                </ul>
              </div></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }
			
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td  style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"><b> Estatus Lavado</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser); 
									?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "><div class="btn-group" style="width:100px;height:19px;">
                <button class="btn  btn-xs dropdown-toggle" style=" width:100%; height:19px; background:<?php echo $arraystatus[1][ $pizuser[$i]['lavado']];?>; color:<?php if($arraystatus[1][$pizuser[$i]['lavado']]==3){echo'#333';}else{echo 'white';}?>" type="button" data-toggle="dropdown"> <?php echo $arraystatus[0][$pizuser[$i]['lavado']];?><i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background:#C00; color:white;"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=1&fecha=<?php echo $fecha;?>">En Espera</a> </li>
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=5&fecha=<?php echo $fecha;?>">Terminado</a> </li>
                  
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=9&fecha=<?php echo $fecha;?>&vin=<?= $pizuser[$i]['vin']?>">Almacen 1</a> </li>
                  
                  
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=10&fecha=<?php echo $fecha;?>&vin=<?= $pizuser[$i]['vin']?>">Almacen 2</a> </li>
                  
                                    <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_lavado&value=11&fecha=<?php echo $fecha;?>&vin=<?= $pizuser[$i]['vin']?>">Almacen 3</a> </li>
                  
                                    
                </ul>
              </div></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "></td>
            <?php }
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333;  border-bottom: 2px solid #333;">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333;"></td>
            <?php }} ?>
          </tr>
          
          <!-- status surtido de pieza !-->
          
          <tr class="odd gradeX">
            <td  style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"><b> Surtido de Piezas</b></td>
            <?php $asesi=asesi($pizarron,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar($pizarron,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $pizuser=getpizuser($pizarron,$us->sus_idUser); 
									?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "><div class="btn-group" style="width:100px;height:19px;">
                <button class="btn  btn-xs dropdown-toggle" 
                style=" width:100%; height:19px;  background:<?php echo $arraystatus[1][ $pizuser[$i]['surtido']];?>; color:white " type="button" data-toggle="dropdown">
				<?php echo $arraystatus[0][$pizuser[$i]['surtido']];?><i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background:#C00; color:white;">
                  <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_surtido&value=6&fecha=<?php echo $fecha;?>">No Surtido </a>
                  </li>
                  <li style="background-color:green"> <a style="color:white" href="<?php echo base_url()?>pizarron/update?id=<?php echo $pizuser[$i]['id'];?>&campo=piz_status_surtido&value=7&fecha=<?php echo $fecha;?>">Surtido</a> </li>
                </ul>
              </div></td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; "></td>
            <?php }
			 } else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333;"></td>
            <?php }} ?>
          </tr>
          
           <!-- fin surtido de pieza !-->          
          
          
          <?php }}?>
        </tbody>
      </table>
      <br><br>
	  
	  <!-- 
	  tabla para autos que de inventario que se reflejan en el pizarron
	  -->
	  
      <table class="table table-striped table-bordered  table-hover "  id="sample_2" >
        <thead>
          <tr style="font-weight:bold">
            <th width="110px;" style="border-top:2px solid #333; border-right:2px solid #333; border-left:2px solid #333; text-align:center "> <b>T&eacute;cnicos</b> </th>
            <th width="130px;" style="border-top:2px solid #333; border-bottom:2px solid #333; border-right:2px solid #333; text-align:center  " > <b>Descripci&oacute;n</b> </th>
            <?php
                                for($z=1; $z<10; $z++){
									echo'<th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Asignaci&oacute;n '.$z.'</b></th>';
									} 
								?>
                                <th  style="border-top:2px solid #333;border-bottom:2px solid #333; border-right:2px solid #333; width:100px; text-align:center; "><b>Acumulado</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($usuarios as $us) {
					if($us->wor_idWorkshop==$taller and $us->rol_idRol==4 and $us->sus_isActive==1 and $us->sus_email=='NA@GMAIL.COM'){
					
				    $sum=0;
					 ?>
          <tr class="odd gradeX"  >
            <td rowspan="5"  width="110px"  style="border: 2px solid #000; text-align:center">
			<img src="<?php echo base_url()?>ajax/asesores/thumb.php?file=../../images/empleados/<?php echo $us->sus_foto;?>&size=110" ><br><?php 
			
			
			echo $us->sus_name.' '.$us->sus_lastName;?>
            </td>
            <td style="border-right:2px solid #333; text-align:right"><b> Torre</b></td>
            <?php $asesi=asesi_inv($pizarron_inventario,$us->sus_idUser);
			if( 'ok'==$asesi){
			   $sum=0;
			   $totuno=contar_piz_inv($pizarron_inventario,$us->sus_idUser);
			   $totdos=9 - $totuno;
			   for($i=0; $i<$totuno; $i++){
			  $piz_Datos=getpizuserinv($pizarron_inventario,$us->sus_idUser);
			  $sum+=$piz_Datos[$i]['tiempo'];
			       ?>
            <td align="center"  style="font-weight:bold; cursor:pointer; font-size:16px;  border-right: 2px solid #333;" >
			<?=$piz_Datos[$i]['torre']?>
			</td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="font-weight:bold; border-right: 2px solid #333; ">
            
            </td>
            <?php }
			echo ' <td align="center" rowspan="5"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style="font-weight:bold; border-right: 2px solid #333; "></td>
            <?php }
						echo ' <td align="center" rowspan="5"  style="vertical-align:middle; font-size:24px;  font-weight:bold; border-right: 2px solid #333; border-bottom: 2px solid #333; ">'.number_format($sum, 2, '.', '').'</td>';

			
			} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> VIN</b></td>
            <?php $asesi=asesi_inv($pizarron_inventario,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar_piz_inv($pizarron_inventario,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
									$piz_Datos=getpizuserinv($pizarron_inventario,$us->sus_idUser);
										 ?>
								
            <td title="<?php echo $piz_Datos[$i]['vin'];?>" style=" cursor:help;border-right: 2px solid #333; text-align:center">
			<?php echo $piz_Datos[$i]['vin']; ?>
			
			
            </td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="border-right: 2px solid #333; text-align:center"></td>
            <?php } 
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center"></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Accion</b></td>
            <?php $asesi=asesi_inv($pizarron_inventario,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar_piz_inv($pizarron_inventario,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										 $piz_Datos=getpizuserinv($pizarron_inventario,$us->sus_idUser);
									?>
            <td class="asignar" title="<?= $piz_Datos[$i]['torre']?>/<?= $piz_Datos[$i]['vin']?>/<?= $us->sus_idUser?>" id="<?= $piz_Datos[$i]['vin']?>" style="cursor:pointer; border-right: 2px solid #333; text-align:center">
			
			Asignar
			</td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333; text-align:center ">
			
			</td>
            <?php } 
						//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			 } else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333; text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-right:2px solid #333; text-align:right"><b> Ubicaci&oacute;n</b></td>
            <?php $asesi=asesi_inv($pizarron_inventario,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar_piz_inv($pizarron_inventario,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
									 $piz_Datos=getpizuserinv($pizarron_inventario,$us->sus_idUser);
										
									
									?>
            <td style=" border-right: 2px solid #333; text-align:center">
			<?= $piz_Datos[$i]['ubicacion']?>
			</td>
            <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style=" border-right: 2px solid #333;text-align:center "></td>
            <?php }} ?>
          </tr>
          <tr class="odd gradeX">
            <td style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"><b>Tiempo Tabulado</b></td>
            <?php $asesi=asesi_inv($pizarron_inventario,$us->sus_idUser);
			if( 'ok'==$asesi){
									
									$totuno=contar_piz_inv($pizarron_inventario,$us->sus_idUser);
									$totdos=9 - $totuno;
									 for($i=0; $i<$totuno; $i++){
										  $piz_Datos=getpizuserinv($pizarron_inventario,$us->sus_idUser);
									?>
            <td style=" border-bottom: 2px solid #333; border-right:2px solid #333; text-align:center">
           <?= $piz_Datos[$i]['tiempo']?>
          </td>
									 <?php  }for($x=0; $x < $totdos; $x++){ ?>
            <td style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"></td>
            <?php } 
			//echo ' <td style="font-weight:bold; border-right: 2px solid #333; ">xx</td>';
			} else { for($i=0; $i<9; $i++){?>
            <td style="border-bottom: 2px solid #333; border-right:2px solid #333; text-align:right"></td>
            <?php }} ?>
          </tr>
         
          
           <!-- fin surtido de pieza !-->          
          
          
          <?php }}?>
        </tbody>
      </table>
      
      
      
      
      
    </div>
  </div>
ó</div>

<!-- Modal cambiar fecha-->

<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Mover al Siguiente dia:</h4>
      </div>
      <div class="modal-body"> <?php echo form_open('pizarron/cambiardia/','class=""'); ?>
        <input type="hidden" name="ido" value="">
        <div class="form-group">
										
										<div class="col-md-3">
											
											<input type="text" name="fecha" class="form-control" value="<?php echo $end = date("Y-m-d",strtotime("+1 days"));?>" <?php if($_SESSION['sftype']==2){}else{echo 'readonly';}?>>
												
											</div>
											<!-- /input-group -->
											
										</div>
									
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <input type="submit" class="btn btn-info" value="Guardar">
      </div>
    </div>
    </form>
    <!-- /.modal-content --> 
  </div>
</div>

<!-- Modal cambiar fecha-->

<div class="modal fade" id="wideCom" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Comentarios:</h4>
      </div>
      <div class="modal-body"> 
     <div id="comentarios"></div>							
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
   
      </div>
    </div>
   
    <!-- /.modal-content --> 
  </div>
</div>


<!-- Modal agregar previa a Tecnico-->

<div class="modal fade" id="wideInv" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Agregar Previa a Tecnico:</h4>
      </div>
      <div class="modal-body"> 
	  <div class="loading" ></div>
      <input type="hidden" name="torre" value="">
      <table class="table">
	  <?php
	  foreach($usuarios as $us) {
					if($us->wor_idWorkshop==$taller and $us->rol_idRol==4 and $us->sus_isActive==1 and $us->sus_email!='NA@GMAIL.COM'){
		echo '<tr>';		
		echo '<td><input type="radio" name="tecnico" id="'.$us->sus_idUser.'" class=""></td><td>'.$us->sus_name.' '.$us->sus_lastName.'</td>';  
		echo '</tr>'; 
	  }}
	  ?>
      </table>	  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
   
      </div>
    </div>
	</div>
    </div>
    <style>
      .cover-radio{
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(138, 138, 138, 0.4); ;
        text-align: center;
      }

      .main-radio{
        position: fixed;
        z-index: 1;
        margin-left: 30%;
        padding: 10px;
        text-align: center;
        margin-top: 100px;
        width: 40%;
        background: white;
      }

      .inp-clave-radio{
        margin-left: 25%;
        width: 50% !important;
        margin-bottom: 20px;
      }
    </style>
    <div class="cover-radio" style="display:none">
      <div class="main-radio">
        <h3>Ingresar clave de radio del vehiculo</h3>
        <br>
        <input type="text" id="inp-radio" class="inp-clave-radio form-control">
        <br>
        <div style="margin-right:10px" id="btn-previar-radio" url="" onclick="previarFinal(this)" class="btn btn-primary btn-lg">PREVIAR</div>
        <div class="btn btn-danger btn-lg" onclick="cerrarClave()" id="cerrar-clave">CANCELAR</div>
        <br>
      </div>
    </div>
   
    <!-- /.modal-content --> 

<!-- END CONTENIDO-->
<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('pizarron/js/script');?>

<script>

function previarConRadio(piz,campo,status,fecha,orden,vin){
    var url = '<?php echo base_url()?>pizarron/update?id='+piz+'&campo='+campo+'&value='+status+'&fecha='+fecha+'&vin='+vin+'&orden='+orden;
    var boton = $('#btn-previar-radio');
    boton.attr('url',url)
    $('.cover-radio').fadeIn(100);
}

function previarFinal(e){
  var l = $('#inp-radio').val().length;
  var clave = $('#inp-radio').val();
  // if(l == 0){
  //   alert('Debes ingresar la clave de radio');
  // }else{
    var url = $(e).attr('url');
    $.get(url+'&clave='+clave, function(data){
      // alert(data);
      window.location.reload(); 
    });
    cerrarClave();
    
  // }
}

function cerrarClave(){
  $('.cover-radio').fadeOut(100);
}

jQuery(document).ready(function() {
    
 //Ocultamos el menú al cargar la página
             
            /* mostramos el menú si hacemos click derecho
            con el ratón */
            $(document).on('contextmenu','.boletines',function(e){
                $('.div-rclick').fadeOut();
                var y = e.pageY;
                var x = e.pageX-100;
                  $("#menu-"+$(this).attr('orden')+'-'+$(this).attr('campania')).css({ 'left':x, 'top':50});
                $("#menu-"+$(this).attr('orden')+'-'+$(this).attr('campania')).fadeIn();
                  return false;
            });
             
             
            //cuando hagamos click, el menú desaparecerá
            $(document).click(function(e){
                  if(e.button == 0){
                        $(".div-rclick").css("display", "none");
                  }
            });
             
            //si pulsamos escape, el menú desaparecerá
            $(document).keydown(function(e){
                  if(e.keyCode == 27){
                        $("#menu").fadeOut();
                  }
            });
             
            //controlamos los botones del menú
            $(document).on('click','.li-rclick',function(e){
                   console.log($(this).attr('campania'));
                  // El switch utiliza los IDs de los <li> del menú
                  switch($(this).attr('id')){
                        case "realizado":
                              $('#gar-'+$(this).attr('orden')+'-'+$(this).attr('campania')).css({'color': 'green'});
                          console.log($(this).attr('orden')+'    '+$(this).attr('campania'));
                          
                            $.ajax({
                              url:"<?php echo base_url();?>ajax/pizarron/update_garantia_status.php?status=0&orden="+$(this).attr('orden')+"&campania="+$(this).attr('campania'),
                              success:function(result){ 
                              } 
                            });
                          var fa = $('#i-'+$(this).attr('orden')+'-'+$(this).attr('campania'));
                          fa.removeClass();
                          fa.addClass('fa fa-check fa-2x');

                          $(this).parent().parent().fadeOut();
                              break;      
                        case "norealizado":
                              $('#gar-'+$(this).attr('orden')+'-'+$(this).attr('campania')).css({'color': 'red'});
                              console.log($(this).attr('orden')+'    '+$(this).attr('campania'));

                                $.ajax({
                                  url:"<?php echo base_url();?>ajax/pizarron/update_garantia_status.php?status=1&orden="+$(this).attr('orden')+"&campania="+$(this).attr('campania'),
                                  success:function(result){ 
                                  } 
                                });
                              var fa = $('#i-'+$(this).attr('orden')+'-'+$(this).attr('campania'));
                              fa.removeClass();
                              fa.addClass('fa fa-times fa-2x');
                              $(this).parent().parent().fadeOut();
                              break;
                  }
                   
            });
             
    
$('.asignar').live('click',function(){
var valor=$(this).attr('id'); 
var torre=$(this).attr('title');     
    
$('#wideInv').modal('show');    
$('input[name=tecnico]').addClass(valor); 
$('input[name=torre]').val(torre); 
});    
    
    
$('input[name=tecnico]').live('click',function(){
    
$('.loading').html('Guardando Informacion...');
          //1)crear una cita traer idcita
          //2)crear un cliente
          //3)Crear una orden de servicio
          //4)Crear agregar al pizarron
          //5)Modificar Pizarron de Inventario
          //6)
                      
          //alert('La Informacion fue modificada con Exito, El Pizarron se actualizara !.');    
    
    
var tecnico=$(this).attr('id');	
var vin=$(this).attr('class');
var torre=$('input[name=torre]').val();
    
$.post("<?= base_url()?>clientes/agregarNuevoCliente/"+vin+"",{ },function(data) {/*Cliente Agregado*/
var idCliente=data;
// crear cita
$.post("<?= base_url()?>cita/enviarInventarioOrden/"+vin+"",{ },function(data) { /*Cita Agregada*/   
var idCita=data;
//crea crear orden de servicio y mandar al pizarron
$.post("<?= base_url()?>orden/crearOrdenPrevias/"+idCita+"/"+idCliente+"/"+tecnico+"/"+torre+"",{ },function(data) {/*Cliente Agregado*/
//mover auto de pizarron Inventario
$.post("<?= base_url()?>pizarron/pasaraPreviado/"+tecnico+"/"+vin+"",{ },function(data) {/*Cliente Agregado*/
    
alert('Orden de Servicio Asignada');
window.location="<?php echo base_url()?>/pizarron";    
});    
});       
//fin crear orden y enviar ala pizarron
} ); // fin crear cita   
});   //fin Crear Cliente
    
//crea cliente
//$.post("<= base_url()?>clientes/agregarNuevoCliente/"+vin+"",{ },function(data) {/*Cliente Agregado*/
//var idCliente=data;});
    
//crear cita    
//$.post("<= base_url()?>cita/enviarInventarioOrden/"+vin+"",{ },function(data) {/*Cita Agregada*/
//var idCita=data;} );
    
    
    
//actualiza la inforacion de pizarron, como previado. 
//$.ajax({
//	  url:"echo base_url();?>pizarron/pasaraPreviado?&tecnico="+tecnico+"&estatusId="+estatusIdVin+"",
//	  success:function(result){ /*actualizar CRM Inventario*/} });

});

$('.torre').on('change',function(){
var valor=$(this).val();

$.ajax({
	  url:"<?php echo base_url();?>pizarron/updatePizarronInventario?&valor="+valor+"",
	  success:function(result){  } });
});	




	
	$(function () {
	 $('#date-pickerbb').datepicker({ 
	language: 'es',
	isRTL: false,
         autoclose:true
                
	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });

	 
	});
	
$('.cambiardia').on('click',function(){
var id=$(this).attr('id');
$('#wide').modal('show');    
$('input[name=ido]').val(id); 
	
	});	
	
	$('.getComentarios').on('click',function(){
var id=$(this).attr('id');
$('#wideCom').modal('show');
 $.ajax({
	  url:"<?php echo base_url();?>ajax/pizarron/comentarios.php?ido="+id+"",
	  success:function(result){
         $('#comentarios').html(result);
		       
                              }
         });    

	
	});	
	
$('.savehora').on('click',function(){
var campo='piz_entrega_lavado';
var id=$(this).attr('id');
var valor="<?php echo $hora?>";

 $.ajax({
	  url:"<?php echo base_url();?>pizarron/update?id="+id+"&campo="+campo+"&value="+valor+"",
	  success:function(result){
         $('#'+id+'').html('<?php echo $hora?>');
		 $('#'+id+'').removeClass();
		 $('#'+id+'').addClass('savehorano');        
                              }
         });

});

$('.savehorano').on('click',function(){
var campo='piz_entrega_lavado';
var id=$(this).attr('id');
var valor="";

 $.ajax({
	  url:"<?php echo base_url();?>pizarron/update?id="+id+"&campo="+campo+"&value="+valor+"",
	  success:function(result){
         $('#'+id+'').html('Cargar hora...');
		 $('#'+id+'').removeClass();
		 $('#'+id+'').addClass('savehora');    
                              }
         });

});
    

});
     $('.boletin').on('click',function(){
         $('#comentarios').html('<image src="http://hsas.gpoptima.net//assets/images/spinnersvg.svg" />');
         console.log($(this).attr('orden'));
        $('#wideCom').modal('show');
         $.ajax({
              url:"http://hsas.gpoptima.net//ajax/pizarron/getBoletines.php?orden="+$(this).attr('orden'),
              success:function(result){
                  $('#comentarios').empty();
                 $('#comentarios').html(result);
                  
                  
                                      },
             error:function(data){
                 console.log(data);
             }
                 });    

	
	});
  
  
        function clickGarantia(e){
         var orden = $(e).attr('orden');
         
         
         $.get('http://hsas.gpoptima.net/ajax/ordendeservicio/getNewGarantia.php?orden='+orden, function(data){
           $('.tipo-gar').empty();
          $('.tiempo-gar').empty();
          $('.kilometros-gar').empty(); 
          $('#h2-vin').empty();

          $('.tipo-ext').empty();
          $('.tiempo-ext').empty();
          $('.kilometros-ext').empty(); 
          $('#h2-km').empty();

            var json = JSON.parse(data);

            $('#h2-vin').append(json.Base.vin);
            $('.tipo-gar').append('<h4>Garantia base: '+json.Base.status+'</h4>');
            $('.tiempo-gar').append('<h4>Vigencia tiempo: '+json.Base.tiempo+'</h4>');
            $('.kilometros-gar').append('<h4>Vigencia km: 60,000</h4>');

            $('#h2-km').append(json.Base.km);
            $('.tipo-ext').append('<h4>Extension de garantia: '+json.Extension.status+'</h4>');
            $('.tiempo-ext').append('<h4>Vigencia tiempo: '+json.Extension.tiempo+'</h4>');
            $('.kilometros-ext').append('<h4>Vigencia km: </h4>');
            $('.cover-garantia').fadeIn(100);
         });
        };
        $('.cover-garantia').click(function(e){
          var container = $('.cover-garantia');
          var div = $('.modal-garantia')
          if (!div.is(e.target) // if the target of the click isn't the container...
              && div.has(e.target).length === 0) // ... nor a descendant of the container
          {
              container.fadeOut(100);
          }
        });
    
    	
</script>