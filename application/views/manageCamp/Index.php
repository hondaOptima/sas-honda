<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $data['menu']='dash';$this->load->view('globales/menu',$data); ?>


<div id="columnchart_material" style="width: 105%; height: 500px;margin-left:0.5%;margin-top:20px;"></div>


<style type="text/css">
 

tr {
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.2s ease-in-out;
}

tr:hover {
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}

</style>

<?php  $this->load->view('globales/footer'); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript" src="../../../assets/plugins/jquery-1.10.2.min.js"></script>
 <script type="text/javascript">




    <?php $option = 0; $end = 29; $passOption = 'False';  $counter = 0; ?>
      google.charts.load('current', {'packages':['bar','table']});
      google.charts.setOnLoadCallback(function(){ drawChart() });
      function drawChart() {

        
          // now add the rows.

          <?php
            while($passOption == 'False'){?>

              var dataTable = new google.visualization.DataTable();

        var titles = ['Campañas', 'Posibles', 'Realizadas', 'Pendientes'];
        var numCols = titles.length;
        dataTable.addColumn('string', titles[0]);  
         for (var i = 1; i < numCols; i++)
            dataTable.addColumn('number', titles[i]);           


            <?php for ($i = $option; $i < $end; $i++) { 
            $elem = $campanias[$i];
            $campaniatxt = (string)$elem->campania;
            $posibles = (string)$elem->posibles;
            $realizadas = (string)$elem->realizadas;
            $pendientes = (string)$elem->pendientes;
            ?>
            var arr = ['<?php echo $campaniatxt; ?>',<?php echo $posibles; ?>,<?php echo $realizadas; ?>,<?php echo $pendientes; ?>];
            dataTable.addRow(arr);
          <?php }
             $option = count($campanias) - ($end+1);
             if($option > 0){
              $option = $end +1; 
              if((count($campanias) - ($end+1)) > 30){
                $end = $end + 29;
              }
              else{
                $end = $end + (count($campanias) - ($end));
              }
              
             }else{
              $option = 0;
              $passOption = 'True';
             }
           ?>

        var options = {
          chart: {
            title: 'Campañas',
            subtitle: 'Posibles, Realizadas, Pendientes',
          },
          vAxis: {
            format: 'decimal',
            viewWindowMode: 'explicit',
            maxValue: 2000, 
            minValue:0,
            
          },
          bar: {groupWidth: '70%'},
        };

        if(<?php echo $counter; ?> > 0){
          var newChart = document.createElement('div');
          newChart.id = 'newChart_'+<?php echo $option ?>;
          newChart.style.width = '105%';
          newChart.style.height = '500px';
          newChart.style.marginLeft = '0.5%';
          newChart.style.marginTop = '20px';
          document.getElementsByClassName('page-content')[0].appendChild(newChart);
          var newGChart = new google.charts.Bar(document.getElementById('newChart_'+<?php echo $option ?>));
          newGChart.draw(dataTable, google.charts.Bar.convertOptions(options));
        }else{
            if('<?php echo $passOption; ?>' != 'True'){
                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
                chart.draw(dataTable, google.charts.Bar.convertOptions(options));

            }
            
        }
        <?php $counter++; ?>
        <?php } ?>

        addTableCamp();

      }





      function changeColor(){
          this.style.backgroundColor = 'rgba(46, 138, 138, 0.7)';
      }

      function changeColorOut(){
          this.style.backgroundColor = this.changeColor;
      }

      function addTableCamp(){
          var tabla = document.createElement('table');
          var divParent = document.createElement('div');
          divParent.id = 'div-table-mcamp';
          divParent.style.width = '80%';
          divParent.style.marginTop = '40px';
          divParent.style.marginLeft = '10%';
          divParent.classList.add('table-responsive');
          tabla.style.marginTop = '15px';
          tabla.classList.add('table');
          tabla.classList.add('table-condensed');
          tabla.classList.add('table-striped');
          
          var colCampania = document.createElement('th');
          colCampania.style.textAlign  = 'center';
          colCampania.innerHTML = 'Campania';
          var colAplicadas = document.createElement('th');
          colAplicadas.style.textAlign  = 'center';
          colAplicadas.innerHTML = 'Aplicadas';
          var colPendientes = document.createElement('th');
          colPendientes.style.textAlign  = 'center';
          colPendientes.innerHTML = 'Pendientes';
          var colAvance = document.createElement('th');
          colAvance.style.textAlign  = 'center';
          colAvance.innerHTML = 'Avance';
          var rhead = document.createElement('tr');
          rhead.style.backgroundColor = '#373a3c';
          rhead.style.color = '#FFF';
          rhead.style.height = '30px';
          rhead.style.fontSize = '1.5em';
          rhead.style.paddingTop = '2px';
          rhead.appendChild(colCampania);
          rhead.appendChild(colAplicadas);
          rhead.appendChild(colPendientes);
          rhead.appendChild(colAvance);
          tabla.appendChild(rhead);
          var colorCount = 1;
          var countid = 0;
          var countAplicadas = 0;
          var countPendientes = 0;
          <?php foreach ($campanias as $elem){ ?>
              var inRow = document.createElement('tr');
              inRow.style.marginTop = '1px';
              inRow.style.marginBottom = '1px';
              inRow.style.height = '25px';
              inRow.style.paddingTop = '3px';
              inRow.style.marginTop = '2px';
              inRow.style.marginBottom = '2px';
              inRow.style.fontSize = '1.2em';
              inRow.id = 'tr_'+countid;
              inRow.addEventListener("mouseover", changeColor);
              inRow.addEventListener("mouseout", changeColorOut);
              if(colorCount == 1){
                  inRow.changeColor = 'rgba(14, 144, 250, 0.2)';
                  inRow.style.backgroundColor = 'rgba(14, 144, 250, 0.2)';
                  colorCount = 0;
              }else{
                  inRow.changeColor = '#fff';
                  inRow.style.backgroundColor = '#fff';
                  colorCount = 1;
              }
              countid++;
              var tdCampania = document.createElement('td');
              tdCampania.style.textAlign = 'center';
              tdCampania.innerHTML = '<?php echo $elem->campania; ?>';
              var tdAplicadas = document.createElement('td');
              tdAplicadas.style.textAlign = 'center';
              tdAplicadas.innerHTML = <?php echo $elem->realizadas; ?>;
              var tdPendientes = document.createElement('td');
              tdPendientes.style.textAlign = 'center';
              tdPendientes.innerHTML = <?php echo $elem->pendientes; ?>;
              var tdAvance = document.createElement('td');
              tdAvance.style.textAlign = 'center';
              var fixed = <?php echo ($elem->realizadas*100)/ $elem->posibles; ?>;
              tdAvance.innerHTML = fixed.toFixed(2)+' %';

              countAplicadas = countAplicadas+<?php echo $elem->realizadas; ?>;
              countPendientes = countPendientes+<?php echo $elem->pendientes; ?>;

              inRow.appendChild(tdCampania);
              inRow.appendChild(tdAplicadas);
              inRow.appendChild(tdPendientes);
              inRow.appendChild(tdAvance); 
              tabla.appendChild(inRow); 
              
          <?php } ?>
          var inRow = document.createElement('tr');
              inRow.style.marginTop = '1px';
              inRow.style.marginBottom = '1px';
              inRow.style.height = '35px';
              inRow.style.paddingTop = '4px';
              inRow.style.marginTop = '2px';
              inRow.style.marginBottom = '2px';
              inRow.style.fontSize = '1.5em';
              inRow.id = 'tr_'+(countid+1);
              inRow.addEventListener("mouseover", changeColor);
              inRow.addEventListener("mouseout", changeColorOut);
              inRow.changeColor = 'rgba(42, 214, 86, 1)';
              inRow.style.backgroundColor = 'rgba(42, 214, 86, 1)';
              var tdCampania = document.createElement('td');
              tdCampania.style.textAlign = 'center';
              tdCampania.innerHTML = '';
              var tdAplicadas = document.createElement('td');
              tdAplicadas.style.textAlign = 'center';
              tdAplicadas.innerHTML = countAplicadas;
              var tdPendientes = document.createElement('td');
              tdPendientes.style.textAlign = 'center';
              tdPendientes.innerHTML = countPendientes;
              var tdAvance = document.createElement('td');
              tdAvance.style.textAlign = 'center';
              var fixed = (countAplicadas*100)/ countPendientes;
              tdAvance.innerHTML = fixed.toFixed(2)+' %';
              inRow.appendChild(tdCampania);
              inRow.appendChild(tdAplicadas);
              inRow.appendChild(tdPendientes);
              inRow.appendChild(tdAvance); 
              tabla.appendChild(inRow); 



          divParent.appendChild(tabla);
          document.getElementsByClassName('page-content')[0].appendChild(divParent);
          

        }
    </script>

    