<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>pizarron">Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="#">Producción</a>
							<i class="fa fa-angle-right"></i>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
                    
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Producción
							</div>
							
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr style="font-weight:bold">
								
								<th>
									 <b>Taller</b>
								</th>
								<th>
									 <b>Hrs de Lun-Vier</b>
								</th>
								<th>
									 <b>Hrs. Sabado</b>
								</th>
								<th>
									 <b>Empleados</b>
								</th>
								<th>
									 <b>Producción</b>
								</th>
                                <th>
									 <b>Control</b>
								</th>
								
							</tr>
							</thead>
							<tbody>
                          
								<tr class="odd gradeX">
								
								<td>
                                	
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
                                <td>
                                	
                                </td>
                                <td>
                                	
                                </td>
                                
                               
									
                                <td>
                                
                                <div class="task-config-btn btn-group">
<a class="btn btn-xs btn-default dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
<i class="fa fa-cog"></i>Acción
<i class="fa fa-angle-down"></i>

</a>
<ul class="dropdown-menu pull-right">

<li><a href="<?php echo base_url();?>produccion/editar">
<i class="fa fa-edit"></i>
Editar</a>

</li>


</ul>
</div>
                                	
                                	
                                </td>
							</tr>
                            
                            <tr class="odd gradeX">
								
								<td>
                                	
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
                                <td>
                                	
                                </td>
                                <td>
                                	
                                </td>
                                
                               
									
                                <td>
                                
                                <div class="task-config-btn btn-group">
<a class="btn btn-xs btn-default dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
<i class="fa fa-cog"></i>Acción
<i class="fa fa-angle-down"></i>

</a>
<ul class="dropdown-menu pull-right">

<li><a href="">
<i class="fa fa-edit"></i>
Editar</a>

</li>


</ul>
</div>
                                	
                                	
                                </td>
							</tr>
                            
                            <tr class="odd gradeX">
								
								<td>
                                	
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
                                <td>
                                	
                                </td>
                                <td>
                                	
                                </td>
                                
                               
									
                                <td>
                                
                                <div class="task-config-btn btn-group">
<a class="btn btn-xs btn-default dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
<i class="fa fa-cog"></i>Acción
<i class="fa fa-angle-down"></i>

</a>
<ul class="dropdown-menu pull-right">

<li><a href="">
<i class="fa fa-edit"></i>
Editar</a>

</li>


</ul>
</div>
                                	
                                	
                                </td>
							</tr>
								
							
							
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
	<?php $this->load->view('globales/footer');?>
<?php $this->load->view('produccion/js/script');?> 