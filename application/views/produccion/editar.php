<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>
<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>pizarron">Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="<?php echo base_url();?>produccion">Producción</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-edit"></i>
							<a href="#">Editar</a>
							<i class="fa fa-angle-right"></i>
						</li>
</ul>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Editar Produccion
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
                                        
                                      
                     					<?php echo form_open('usuario/actualizar/','class="form-horizontal"'); ?>
												<br>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Taller</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('nombre','', 
																		'id="nombre" class="form-control" 
																		placeholder="Taller" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Horas de Lunes a Viernes</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('apellido','',
																		'id="apellido" class="form-control"
																		placeholder="Horas" required');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Horas Sabado</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('telefono','', 
																		'id="telefono" class="form-control"
																		placeholder="Horas" required');?>
																
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Producción</label>
															<div class="col-md-9">
                                   								<?php  echo form_input('numEmpleado','', 
																		'id="numEmpleado" class="form-control"
																		placeholder="Porcentaje"');?>
																
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
												
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-success">Guardar</button>
															<a href="<?php echo base_url();?>produccion"><button type="button" class="btn btn-default">Cancelar</button></a>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
                                            
										
										<!-- END FORM-->
									</div>
								</div>
					<!-- END EXAMPLE TABLE PORTLET-->
	<?php $this->load->view('globales/footer');?>
<?php $this->load->view('produccion/js/script');?> 