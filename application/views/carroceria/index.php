<?php //$this->load->view('globales/head'); ?>
<!DOCTYPE html>
<html lang="esp" class="no-js">
    <head>
        <?php header('Content-Type: text/html; charset=UTF-8'); ?>
        <title>Honda Optima | SAS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.css" media="screen"/>

        <!-- BEGIN THEME STYLES -->
        <link href="<?= base_url();?>assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/css/jquery-ui-slider-pips.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <!-- PLUGINS -->
        <link href="<?= base_url() ?>assets-new/css/carroceria.css" rel="stylesheet" />

        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets-new/plugins/datatables/responsive.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/sweet-alert-2/sweetalert2.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/amaran/amaran.min.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/select2/select2.css"></link>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/select2/select2_conquer.css"/>
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.min.css"/> -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.css"/>
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker-standalone.css"/> -->
        <!-- /PLUGINS -->

        <!-- Para los iconitos de la derecha -->
        <link href="<?= base_url() ?>assets-new/plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <?php $this->load->view('globales/topbar'); ?>
    </head>

<?php $menu['menu']='carroceria';$this->load->view('globales/menu',$menu); ?>

<style type="text/css">
    #printable { display: none; }

    @media print
    {
    	#non-printable { display: none; }
    	#printable { display: block; }
    }
</style>
<div style="display: none;" id="modal-cita" class="div-back-cita">
    <div class="div-modal-cita" >
        <div class="title-delete">
            <div>
                <div id="div-result-manage">
                    <div >
                        <div class="display">
                            <div class="weekdays"></div>
                            <div class="ampm"></div>
                            <div class="alarm"></div>
                            <div class="digits"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="page-breadcrumb breadcrumb">
    <li> <i class="fa fa-calendar" id="hrefPrint"></i> <a href="<?php echo base_url();?>cita/tv_sas">Carrocería <?= $taller->city ?></a><i class="fa fa-angle-right"></i></li>
    <li class="pull-right"></li>
</ul>
<style>
    .client-m{
        width:160px !important;
    }
</style>
<div class="clearfix"></div>
<?= $flash_message;?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-clock-o"></i><b>Autorizados por Seguro <?= $taller->city ?> </b> </div>
                <div class="tools"> <?= $fecha_completa ?> <i class="fa fa-calendar"></i>&nbsp;<a style="color:white" href="<?php echo base_url()?>cita/imprimir/?date=<?= $fecha ?>&taller=<?= $taller->id ?>" target="_blank"><i style="cursor:pointer" title="Imprimir" class="fa fa-print" ></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="table-carroceria-autorizado">
                    <thead>
                        <tr>
                            <!-- 0 --><th><b>#</b></th>
                            <!-- 1 --><th><b>ID</b></th>
                            <!-- 2 --><th><b>ID Tipo</b></th>
                            <!-- 3 --><th><b>Tipo</b></th>
                            <!-- 4 --><th title="Fecha Autorizado"><b>Autorizado</b></th><!-- Fecha Autorizado Seguro -->
                            <!-- 5 --><th><b>Vin</b></th>
                            <!-- 6 --><th><b>Auto</b></th>
                            <!-- 7 --><th><b>Cliente</b></th>
                            <!-- 8 --><th><b>Poliza</b></th>
                            <!-- 9 --><th><b>Siniestro</b></th><!-- AQUI -->

                            <!-- 10 --><th title="Fecha Recibido"><b>Fecha Recibido</b></th>
                            <!-- 11 --><th><b>Date Recibido</b></th>
                            <!-- 12 --><th><b>Hora Recibido</b></th>

                            <!-- 13 --><th title="Fecha Envío Auto Seguro"><b>Fecha Envío</b></th>
                            <!-- 14 --><th><b>Date Envío</b></th>
                            <!-- 15 --><th><b>Hora Envío</b></th>

                            <!-- 16 --><th title="Fecha Envío"><b>Fecha tentativa</b></th>
                            <!-- 17 --><th><b>Tentativa Date Envío</b></th>
                            <!-- 18 --><th><b>Hora Tentativa</b></th>

                            <!-- 19 --><th><b>ID REFACCION</b></th>
                            <!-- 20 --><th title="Refacciones"><b>Refacciones</b></th>


                            <!-- 21 --><th><b>ID Taller</b></th>
                            <!-- 22 --><th><b>Taller</b></th>

                            <!-- 23 --><th title="¿Paga deducible?"><b>Deducible</b></th>

                            <!-- 24 --><th title="Fecha de la cita"><b>Cita</b></th>
                            <!-- 25 --><th><b>Finalizar</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <tr>
                            <td>1</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_1" data-value="">Interno
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd_1">
                                        <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                        <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                        <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>12/09</td>
                            <td><a href="#" class="vin-link" data-id="dd_1">S5SSF6E9N78WAQ541</a></td>
                            <td>EJE</td>
                            <td>Juan Pérez</td>
                            <td>854715647</td>
                            <td>021587415892a</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_r1" data-value="">Recibidas
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd_r1">
                                        <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                        <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                        <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><button type="button" class="btn btn-primary btn-xs btn-crear-cita">Crear Cita</button></td>
                            <td>Marquez</td>
                            <td>Si</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_2" data-value="">Interno
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd_2">
                                        <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                        <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                        <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>12/09</td>
                            <td><a href="#" class="vin-link" data-id="dd_2">02AD6N8QPT87DEGV5</a></td>
                            <td>Civic Hibrido</td>
                            <td>Rubén Marroquín</td>
                            <td>236587418</td>
                            <td>002180905617</td>
                            <td><div class="dropdown">
                                <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_r2" data-value="">Recibidas
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd_r2">
                                    <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                    <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                    <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                </ul>
                            </div></td>
                            <td>12/13</td>
                            <td>Baja</td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-warning dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_3" data-value="">Particulares
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd_3">
                                        <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                        <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                        <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>12/09</td>
                            <td><a href="#" class="vin-link">2A5EWQJG484DFERT5</a></td>
                            <td>City (MT) 2014-2016</td>
                            <td>Luisa Álvarez</td>
                            <td>151874857</td>
                            <td>000210847580</td>
                            <td><div class="dropdown">
                                <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_r3" data-value="">Recibidas
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd_r3">
                                    <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                    <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                    <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                </ul>
                            </div></td>
                            <td>12/13</td>
                            <td>Confianza</td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_4" data-value="">Por Seguro
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd_4">
                                        <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                        <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                        <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>12/09</td>
                            <td><a href="#" class="vin-link">Q4D5RFE87RF51CDW8</a></td>
                            <td>CIVIC TA/TM 2016</td>
                            <td>José López</td>
                            <td>987456123</td>
                            <td>000000000001</td>
                            <td><div class="dropdown">
                                <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_r4" data-value="">Recibidas
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd_r4">
                                    <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                    <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                    <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                </ul>
                            </div></td>
                            <td><button type="button" class="btn btn-primary btn-xs btn-crear-cita">Crear Cita</button></td>
                            <td>Honda</td>
                            <td>Si</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_5" data-value="">Interno
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd_5">
                                        <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                        <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                        <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>12/09</td>
                            <td><a href="#" class="vin-link">12F5E4WER5D8RF78R</a></td>
                            <td>RIDGELINE 2016-2017</td>
                            <td>Ajelet Vargas</td>
                            <td>151849175</td>
                            <td>000006258741</td>
                            <td><div class="dropdown">
                                <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dd_r5" data-value="">Recibidas
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd_r5">
                                    <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                    <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                    <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                </ul>
                            </div></td>
                            <td>12/13</td>
                            <td>Honda</td>
                            <td>Si</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-clock-o"></i><b>Espera Autorización Seguro <?= $taller->city ?></b></div>
                        <div class="tools"> <?= $fecha_completa ?> <i class="fa fa-calendar"></i> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="table-carroceria-espera">
                            <thead>
                                <tr>
                                    <!-- 0 --><th><b>#</b></th>
                                    <!-- 1 --><th><b>ID CARROCERIA</b></th>
                                    <!-- 2 --><th><b>ID TIPO</b></th>
                                    <!-- 3 --><th><b>Tipos</b></th>
                                    <!-- 4 --><th><b>Vin</b></th>
                                    <!-- 5 --><th><b>Auto</b></th>
                                    <!-- 6 --><th><b>Cliente</b></th>
                                    <!-- 7 --><th><b>Póliza</b></th>
                                    <!-- 8 --><th><b>Siniestros</b></th><!-- AQUI -->

                                    <!-- 9 --><th title="Fecha Recibido"><b>Fecha Recibido</b></th>
                                    <!-- 10 --><th><b>Date Recibido</b></th>
                                    <!-- 11 --><th><b>Hora Recibido</b></th>

                                    <!-- 12 --><th title="Fecha Envío Auto Seguro"><b>Fecha Envío</b></th>
                                    <!-- 13 --><th><b>Date Envío</b></th>
                                    <!-- 14 --><th><b>Hora Envío</b></th>

                                    <!-- 15 --><th title="Fecha Tentativa"><b>Fecha Tentativa</b></th>
                                    <!-- 16 --><th><b>Date Tentativa</b></th>
                                    <!-- 17 --><th><b>Hora Tentativa</b></th>

                                    <!-- 18 --><th><b>ID REFACCION</b></th>
                                    <!-- 19 --><th title="Refacciones"><b>Refacciones</b></th>
                                    <!-- 20 --><th><b>Comentarios</b></th>
                                    <!-- 21 --><th><b>Autorizar</b></th>
                                    <!-- 22 --><th><b>Editar</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td>1</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-success dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dde_1" data-value="">Interno
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dde_1">
                                            <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                            <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                            <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                        </ul>
                                    </div></td>
                                    <td><a href="#" class="vin-link">1G2NW12E25M108434</a></td>
                                    <td>Crosstour</td>
                                    <td>Laura Díaz</td>
                                    <td>015425898</td>
                                    <td>000000012354</td>
                                    <td>12/09</td>
                                    <td>12/10</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-warning dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dde_r1" data-value="">Pendientes
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dde_r1">
                                            <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                            <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                            <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                        </ul>
                                    </div></td>
                                    <td>Solo tomará un día</td>
                                    <td><button type="button" class="btn btn-success btn-xs btn-autorizar">Autorizar</button></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dde_2" data-value="">Por Seguro
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dde_2">
                                            <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                            <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                            <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                        </ul>
                                    </div></td>
                                    <td><a href="#" class="vin-link">D52G4V5RT89ED521S</a></td>
                                    <td>ACCORD CVT 2013-2016</td>
                                    <td>Fernanda Rodríguez</td>
                                    <td>001790542</td>
                                    <td>000000001000</td>
                                    <td>12/08</td>
                                    <td>12/09</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dde_r2" data-value="">En Proceso
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dde_r2">
                                            <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                            <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                            <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                        </ul>
                                    </div></td>
                                    <td>Falta un documento de recibir</td>
                                    <td><button type="button" class="btn btn-success btn-xs btn-autorizar">Autorizar</button></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-warning dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dde_3" data-value="">Particulares
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dde_3">
                                            <li><a href="#" data-value="1" data-color-class="btn-primary">Por Seguro</a></li>
                                            <li><a href="#" data-value="2" data-color-class="btn-success">Interno</a></li>
                                            <li><a href="#" data-value="3" data-color-class="btn-warning">Particulares</a></li>
                                        </ul>
                                    </div></td>
                                    <td><a href="#" class="vin-link">SD6Q3MR889SR5D2SW</a></td>
                                    <td>CIVIC TA/TM 2016</td>
                                    <td>Adrián González</td>
                                    <td>102089703</td>
                                    <td>000025147801</td>
                                    <td>12/09</td>
                                    <td>12/12</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-warning dropdown-toggle btn-xs" type="button" data-toggle="dropdown" id="dde_r3" data-value="">Pendiente
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dde_r3">
                                            <li><a href="#" data-value="1" data-color-class="btn-primary">En Proceso</a></li>
                                            <li><a href="#" data-value="2" data-color-class="btn-warning">Pendientes</a></li>
                                            <li><a href="#" data-value="3" data-color-class="btn-success">Recibidas</a></li>
                                        </ul>
                                    </div></td>
                                    <td>Solo falta comfirmación</td>
                                    <td><button type="button" class="btn btn-success btn-xs btn-autorizar">Autorizar</button></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //$this->load->view('globales/footer');?>
<!-- FOOTER-->
</div>
	</div>
</div>
</div>
</body>
</html>
<script> window.base_url = "<?= base_url() ?>"; window.color_success = "#5cb85c"; window.color_error = '#EA1934'; window.color_warning = "#F28D3A"</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar <span id="spa_vin"></span></h4>
            </div>
            <div class="modal-body">
                <form id="form_mymodal">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_tipo">Tipo</label>
                                <select class="form-control" id="sel_tipo" tabindex="1">
                                    <option></option>
                                    <option value="1">Interno</option>
                                    <option value="2">Particulares</option>
                                    <option value="3">Por seguro</option>
                                </select>
                              </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fecha_recibido">Fecha recibido</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_recibido" placeholder="Fecha Recibido" tabindex="2">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fecha_enviado">Fecha Enviado</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_enviado" placeholder="Fecha Enviado" tabindex="3">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fecha_tentativa">Fecha Tentativa</label>
                                <input type="text" class="form-control datepicker" id="txt_fecha_tentativa" placeholder="Fecha Tentativa" tabindex="4">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_poliza">Póliza</label>
                                <input type="text" class="form-control" id="txt_poliza" placeholder="Póliza" tabindex="5">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_siniestro">Siniestro</label>
                                <input type="text" class="form-control" id="txt_siniestro" placeholder="Siniestro" tabindex="6">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_deducible">Deducible</label>
                                <select class="form-control" id="sel_deducible" tabindex="7">
                                    <option></option>
                                    <option value=""></option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                              </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_guardar">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-comment" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b><span id="span_comment_name"></b></span></h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="txt_comment">Comentario</label>
                                <textarea class="form-control" rows="5" id="txt_comment"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- aqui -->
                <table class="table table-striped table-bordered table-hover" id="table-comments">
                    <thead>
                        <tr>
                            <!-- 0 --><th><b>#</b></th>
                            <!-- 0 --><th><b>ID</b></th>
                            <!-- 1 --><th><b>ID CARROCERIA</b></th>
                            <!-- 2 --><th><b>Bitácora</b></th>
                            <!-- 3 --><th><b>fecha</b></th>
                            <!-- 4 --><th><b>Usuario</b></th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_guardar_comentario">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->

<!-- <div id="modal-add" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agregar Registro</h4>
            </div>
            <div class="modal-body">
                <form id="form-modal-add">

                    <div class="row" id="div-modal-add">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_modal_cliente">Cliente</label>
                                <select class="form-control" id="sel_modal_cliente">
                                    <option value="1">Jose Luis</option>
                                    <option value="2">German</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_vin">Vin</label>
                                <input type="text" id="txt_modal_vin" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_auto">Auto</label>
                                <input type="text" id="txt_modal_auto" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_vin">Vin</label>
                                <input type="text" id="txt_modal_vin" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_fecha_recibido">Fecha Recibido</label>
                                <input type="text" id="txt_modal_fecha_recibido" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_modal_tipo">Tipo</label>
                                <select class="form-control select2" id="sel_modal_tipo">
                                    <option value="1">Interno</option>
                                    <option value="2">Por Seguro</option>
                                    <option value="3">Particulares</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_siniestro">Siniestro</label>
                                <input type="text" id="txt_modal_siniestro" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_poliza">Poliza</label>
                                <input type="text" id="txt_modal_poliza" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_modal_siniestro">Envío</label>
                                <input type="text" id="txt_modal_siniestro" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sel_modal_refacciones">Refacciones</label>
                                <select class="form-control" id="sel_modal_refacciones">
                                    <option value="1">En espera de piezas</option>
                                    <option value="2">Complementos </option>
                                    <option value="3">Recibidas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="txt_modal_comentarios" >Comentarios</label>
                                <textarea class="form-control" rows="2" id="txt_modal_comentarios"></textarea>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="button-save" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div> -->

<script type="text/javascript">
    window.url_base = "<?= base_url() ?>";
</script>
<script src="<?= base_url() ?>assets-new/plugins/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>

<!-- <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/moment/moment.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/bootstrap/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/sweet-alert-2/sweetalert2.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datatables/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/amaran/jquery.amaran.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?= base_url() ?>assets-new/plugins/select2/select2.js" type="text/javascript"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets-new/js/carroceria.js"></script>
