<!DOCTYPE html>
<html lang="es-ES"  class="body-error"><head>
   <?php
header('Content-Type: text/html; charset=UTF-8'); 
?>

    <title>Honda Optima | SAS - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url();?>assets/css/login.css" rel="stylesheet">
	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

  
  </head>

  <body>


        <div id="wrapper">
        
               
                <?php echo form_open('acceso','class="form-login"'); ?>
  
                        <div class="header">
                            <a href="http://webmarketingnetworks.com/soft/acceso/login" class="link-head-left"><i class="icon-home"></i></a>
                            <span>Inicio de sesión</span>
                        </div>
                        
                        <div class="avatar"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></div>
                        
                        <div class="inputs">
                            <input  name="user" type="text"  placeholder="Usuario" />
                            <input  name="pass" type="password"  placeholder="Contraseña" />
                
                        
                        <div class="link-1">
                            <input type="checkbox" id="c2" name="cc" checked="checked" />
                            <label for="c2"><span></span> Recordarme</label>
                        </div>
                        <div class="link-2"><a href="<?php echo base_url();?>acceso/recuperar">Olvidaste tu contraseña?</a></div>
                        <div class="clear"></div>
                        
                        <div class="button-login"><input type="submit" value="Iniciar Sesión"></div>
                    </div>
                    
                    <div class="footer-login">
                       <?php echo validation_errors('<div class="alert alert-error">','</div>');?>
                        <?php echo $flash_message; ?>
                    </div>
                    
                   
            	<?php echo form_close(); ?>

     <div class="clear"></div>   
    </div>
	<script type="text/javascript">
		(function (exports) {
		function valOrFunction(val, ctx, args) {
			if (typeof val == "function") {
				return val.apply(ctx, args);
			} else {
				return val;
			}
		}
	
		function InvalidInputHelper(input, options) {
			input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));
	
			function changeOrInput() {
				if (input.value == "") {
					input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
				} else {
					input.setCustomValidity("");
				}
			}
	
			function invalid() {
				if (input.value == "") {
					input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
				} else {
				   console.log("INVALID!"); input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
				}
			}
	
			input.addEventListener("change", changeOrInput);
			input.addEventListener("input", changeOrInput);
			//input.addEventListener("invalid", invalid);
		}
		exports.InvalidInputHelper = InvalidInputHelper;
	})(window);
		
	InvalidInputHelper(document.getElementById("usuario"), {
		defaultText: "Por favor ingresa tu nombre de usuario!",
		emptyText: "Por favor ingresa tu nombre de usuario!",
		invalidText: function (input) {
			return 'El usuario "' + input.value + '" no es válido!';
		}
	});
	
		InvalidInputHelper(document.getElementById("contrasena"), {
		defaultText: "Por favor ingresa tu contraseña!",
		emptyText: "Por favor ingresa tu contraseña!",
		invalidText: function (input) {
			return 'La contraseña ingresada no es válida!';
		}
	});
    
    </script>
  </body>
</html>
