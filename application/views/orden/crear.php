<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<!-- BEGIN CONTENIDO-->

<div class="row">
  <div class="col-md-12">
    <div class="portlet" id="form_wizard_1">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-reorder"></i> Nueva Orden de Servicio - <span class="step-title"> Paso 1 de 4 </span> </div>
      </div>
      <div class="portlet-body form"> <?php echo form_open_multipart('orden/crear','class="form-horizontal" id="submit_form"'); ?>
        <div class="form-wizard">
          <div class="form-body">
            <ul class="nav nav-pills nav-justified steps">
              <li style="width:350px; "> <a href="#tab1" data-toggle="tab" class="step"> <span class="number" style="float:left"> 1 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Datos del Cliente </span> </a> </li>
              <li style="width:350px;"> <a href="#tab2" data-toggle="tab" class="step"> <span class="number" style="float:left"> 2 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Revisión del Veh&iacute;culo </span> </a> </li>
              <li style="width:350px;"> <a href="#tab3" data-toggle="tab" class="step active"> <span class="number" style="float:left"> 3 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Fotos del Veh&iacute;culo </span> </a> </li>
              <li style="width:350px;"> <a href="#tab4" data-toggle="tab" class="step"> <span class="number" style="float:left"> 4 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Promociones </span> </a> </li>
            </ul>
            
            <?php //print_r($orden);?>
            <div class="tab-content" style=" margin-top:10px;">
              <div class="alert alert-danger display-none">
                <button class="close" data-close="alert"></button>
                Alguna informaci&oacute;n del formulario es incorrecta. </div>
              <div class="alert alert-success display-none">
                <button class="close" data-close="alert"></button>
                Los datos est&aacute;n correctos! </div>
              <div class="tab-pane active" id="tab1">
               
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Nombre <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteNombre',$orden[0]->sec_nombre,
                                                            'id="sec_nombre" class="form-control saveorden"
                                                            placeholder="Nombre" required');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">R.F.C. <span class="required"> * </span> </label>
                      <div class="col-md-9">
                         <?php  echo form_input('clienteRfc',
															$orden[0]->sec_rfc, 
                                                            'id="sec_rfc" class="form-control saveorden"
                                                            placeholder="RFC" maxlenght="20" required');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Email <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteDireccion',
															$orden[0]->sec_email, 
                                                            'id="sec_email" class="form-control saveorden" 
                                                            placeholder="Email"  required');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Calle <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteCiudad',
															$orden[0]->sec_calle, 
                                                            'id="sec_calle" class="form-control saveorden"
                                                            placeholder="Calle" required');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">No. Int. o Ext. <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteTelefono',
															$orden[0]->sec_num_ext, 
                                                            'id="sec_num_ext" class="form-control saveorden"
                                                            placeholder="No. Int. o Ext." ');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Colonia <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteRfc',
															$orden[0]->sec_colonia, 
                                                            'id="sec_colonia" class="form-control saveorden"
                                                            placeholder="Colonia" maxlenght="20" required');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row--> 
                
                <!--/row-->
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">C&oacute;digo Postal <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteAtencionA',
															$orden[0]->sec_cp, 
                                                            'id="sec_cp" class="form-control saveorden"
                                                            placeholder="Codigo Postal" ');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Delegaci&oacute;n o Municipio <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteAtTel',
															$orden[0]->sec_ciudad, 
                                                            'id="sec_ciudad" class="form-control saveorden"
                                                            placeholder="Delegacion o Municipio"  ');?>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Estado <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteAtTel',
															$orden[0]->sec_estado, 
                                                            ' id="sec_estado" class="form-control saveorden"
                                                            placeholder="Estado"  ');?>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Tel&eacute;fonos<span class="required"> * </span> </label>
                      <div class="col-md-9">
                         <?php  echo form_input('telcliente',
															$orden[0]->sec_tel, 
                                                            'id="sec_tel" class="form-control saveorden"
                                                            placeholder="Tel&eacute;fonos"  ');?>
                      </div>
                    </div>
                  </div>
                  </div>
                  
                                  <div class="row">
                                  
 <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Si es la misma persona click en la siguiente casilla </label>
                      <div class="col-md-9">
                        <input type="checkbox"  name="seo_recoge_check" id="seo_recoge_check" class="checkConfirmarOtros" value="1" <?php if($orden[0]->seo_recoge_check=='1'){echo 'checked';}?>>
                      </div>
                    </div>
                  </div>                                  

                   <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Persona autorizada para recoger Unidad </label>
                      <div class="col-md-9">
                         <?php  echo form_input('persona',
															$orden[0]->seo_persona_recoge, 
                                                            'id="seo_persona_recoge" class="form-control saveorden"
                                                            placeholder="Nombre"  ');?>
                      </div>
                    </div>
                  </div>
                  
                  
                   <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Tel&eacute;fono persona autorizada</label>
                      <div class="col-md-9">
                         <?php  echo form_input('tel',
															$orden[0]->seo_telefono_recoge, 
                                                            'id="seo_telefono_recoge" class="form-control saveorden"
                                                            placeholder="Tel&eacute;fono"  ');?>
                      </div>
                    </div>
                  </div>
                  
                  
                  </div>
                
                <!--/row-->
                
                <div style="font-weight:bold; font-size:18px; margin-left:48px; margin-top:15px;"><span class="number"></span>Datos del Veh&iacute;culo</div>
                <div class="row" style=" padding-top:15px;">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Marca<span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <input type="text" name="fechaVenta" id="sev_marca" 
                                                        class="form-control saveorden" value="<?php echo $orden[0]->sev_marca;?>"  >
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-6">Submarca <span class="required"> * </span> </label>
                      <div class="col-md-6">
                        <?php  echo form_input('vehiculoUltimoSer',$orden[0]->sev_sub_marca, 
                                                            'id="sev_sub_marca" class="form-control saveorden"
                                                            placeholder="Sub Marca" ');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Año - Modelo<span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoVin',$orden[0]->sev_modelo, 
                                                            'id="sev_modelo" class="form-control saveorden"
                                                            placeholder="Modelo" ');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Tipo o versi&oacute;n <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoModelo',$orden[0]->sev_version,
                                                            'id="sev_version" class="form-control saveorden"
                                                            placeholder="Tipo o Version" ');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Capacidad <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoAno',$orden[0]->sev_capacidad, 
                                                            'id="sev_capacidad" class="form-control saveorden"
                                                            placeholder="Capacidad" ');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">N&uacute;mero de Placas <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoColor',$orden[0]->sev_placas,
                                                            'id="sev_placas" class="form-control saveorden"
                                                            placeholder="Placas" ');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Color <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoPlacas',$orden[0]->sev_color, 
                                                            'id="sev_color" class="form-control saveorden"
                                                            placeholder="Color" ');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                 
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">VIN <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoMotor',$orden[0]->sev_vin, 
                                                            'id="sev_vin" class="form-control saveorden"
                                                            placeholder="VIN" ');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">No. Km. Recorridos <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoTrans',$orden[0]->sev_km_recepcion,
                                                            'id="sev_km_recepcion" class="form-control saveorden"
                                                            placeholder="No. Km. Recorridos" ');?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div class="tab-pane" id="tab2">
                
                <div class="row">
                  <div class="col-md-6">
                    <div class="col-md-5" >
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-md-2">Torre<span class="required">*</span> </label>
                          <div class="col-md-7">
                            <select name="torre"  id="seo_tower" class="torre select2_category form-control" required>
                            <option value="">Seleccione una Opción</option>
                              <?php
                                                        for($torre=1; $torre<200; $torre++){
															?>
                              <option class="saveordenc  "  value="<?php echo $torre;?>" <?php if($torre==$orden[0]->seo_tower){echo 'selected="selected"';}?>><?php echo $torre;?></option>
                              <?php
															}
														?>
                            </select>
                          </div>
                          
                           
                          
                        </div>
                       
                          
                           <div class="form-group" >
                          <table width="140px;"  style="background-color:green; margin-left:55px; color:white"><tr><td>Se entregan las partes o refacciones reemplazadas al consumidor?
                          </td><td valign="middle" align="center" style="padding-right:7px;"><input type="checkbox" 
                          name="seo_se_entregan" id="seo_se_entregan" class="checkConfirmarOtros" value="1" <?php if($orden[0]->seo_se_entregan=='1'){echo 'checked';}?>></td></tr></table>
                          </div>
                           <div class="form-group" >
                          <table width="140px;"  style="background-color:green; margin-left:55px; color:white"><tr><td>Servicio en domicilio del consumidor?
                          </td><td valign="middle" align="center" style="padding-right:7px;"><input type="checkbox" 
                          name="seo_ser_domi" id="seo_ser_domi" class="checkConfirmarOtros" value="1" 
						  <?php if($orden[0]->seo_ser_domi=='1'){echo 'checked';}?>></td></tr></table>
                          </div>
                          
                          
                          
                          
                          
                          
                      </div>
                      <div class="row"> </div>
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-md-3">T&eacute;cnico </label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group">
                          <div class="col-md-10" style="padding-left:50px;" >
                            <?php
                                                   foreach($tecnicos as $tec){
													   if($tec->sus_idUser==$orden[0]->seo_technicianTeam){$check='checked="checked"';}else{$check='';}
	echo '<input type="radio" class="savetec" required id="seo_technicianTeam" name="tec" value="'.$tec->sus_idUser.'" '.$check.'>'.$tec->sus_name.' '.$tec->sus_lastName.'<br>';  
													   }
												   ?>
                          </div>
                        </div>
                      </div>
                      <!--/--> 
                      
                      <!--/--> 
                      
                      <!--/-->
                      <div class="row"> </div>
                    </div>
                    <div class="col-md-6">
                     
                      <div class="form-group">
                        <label class="control-label col-md-5 ">Vehículo:</label>
                        <div class="col-md-7">
                         
                          
                           <?php	echo form_dropdown('vehiculo', $vehiculos, $orden[0]->seo_vehiculo,
										'id="app_vehicleModel" class="vehiculo select2_category form-control" 
										data-rel="chosen" required');	?>
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-5">Hora Prometida <?php echo $orden[0]->seo_promisedTime; ?></label>
                        <div class="col-md-7">
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" name="horapro" id="seo_promisedTime" value="<?php echo $orden[0]->seo_promisedTime; ?>" class="form-control timepicker-default" required>
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                            </span> </div>
                        </div>
                      </div>
                     
                       
                      <div class="form-group">
                        <label class="col-md-5 control-label">Combustible</label>
                        <div class="col-md-7">
                          <div id="slider2"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-md-5">Comentarios </label>
                          <div class="col-md-12">
                            <?php  echo form_textarea('comentarios',
															$orden[0]->seo_comments, 
                                                            'id="seo_comments" class="form-control saveorden" 
                                                            placeholder="Comentarios" 
                                                            style="resize: none; height:60px;"');?>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-md-8">Posibles Consecuencias </label>
                          <div class="col-md-12">
                            <?php  echo form_textarea('consecuencias',
															$orden[0]->seo_consecuencias, 
                                                            'id="seo_consecuencias" class="form-control saveorden" 
                                                            placeholder="Posibles Consecuencias" 
                                                            style="resize: none; height:60px;"');?>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-md-5">Tipo de Pago<span class="required">*</span> </label>
                          <div class="col-md-7">
                            <select name="pago"  id="seo_pago" class="pago select2_category form-control">
                              <option value="1" <?php if($orden[0]->seo_pago==1){echo 'selected';}?>>Efectivo</option>
                               <option value="2" <?php if($orden[0]->seo_pago==2){echo 'selected';}?>>Cheque</option>
                                <option value="3" <?php if($orden[0]->seo_pago==3){echo 'selected';}?>>Tarjeta de Cr&eacute;dito</option>
                                 <option value="4" <?php if($orden[0]->seo_pago==4){echo 'selected';}?>>Otro</option>
                            </select>
                          </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  
                  <!--/--> 
                  <!--/-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-2">Articulos</label>
                      <br />
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_tapetes" name="seo_tapetes" class="checkConfirmar" value="2" <?php if($orden[0]->seo_tapetes==2){echo 'checked="checked"';}?>>
                          Tapetes </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_espejo" name="seo_espejo" class="checkConfirmar" value="2" <?php if($orden[0]->seo_espejo==2){echo 'checked="checked"';}?>>
                          Espejo </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_antena" name="seo_antena" class="checkConfirmar" value="2" <?php if($orden[0]->seo_espejo==2){echo 'checked="checked"';}?>>
                          Antena </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_tapones" name="seo_tapones" class="checkConfirmar" value="2" <?php if($orden[0]->seo_tapones==2){echo 'checked="checked"';}?>>
                          Tapones </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_radio" name="seo_radio" class="checkConfirmar" value="2" <?php if($orden[0]->seo_radio==2){echo 'checked="checked"';}?>>
                          Radio </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_encendedor" name="seo_encendedor" class="checkConfirmar" value="2" <?php if($orden[0]->seo_encendedor==2){echo 'checked="checked"';}?>>
                          Encendedor </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_herramienta" name="seo_herramienta" class="checkConfirmar" value="2" <?php if($orden[0]->seo_herramienta==2){echo 'checked="checked"';}?>>
                          Herramienta </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_llantarefaccion" name="seo_llantarefaccion" class="checkConfirmar" value="2" <?php if($orden[0]->seo_llantarefaccion==2){echo 'checked="checked"';}?>>
                          Llanta Ref. </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_limpiadores" name="seo_limpiadores" class="checkConfirmar" value="2" <?php if($orden[0]->seo_limpiadores==2){echo 'checked="checked"';}?>>
                          Limpiadores </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_reflejantes" name="seo_reflejantes" class="checkConfirmar" value="2" <?php if($orden[0]->seo_reflejantes==2){echo 'checked="checked"';}?>>
                          Reflejantes </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_extinguidor" name="seo_extinguidor" class="checkConfirmar" value="2" <?php if($orden[0]->seo_extinguidor==2){echo 'checked="checked"';}?>>
                          Extinguidor </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_cables" name="seo_cables" class="checkConfirmar" value="2" <?php if($orden[0]->seo_cables==2){echo 'checked="checked"';}?>>
                          Cables P/C </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_ecualizador" name="seo_ecualizador" class="checkConfirmar" value="2" <?php if($orden[0]->seo_ecualizador==2){echo 'checked="checked"';}?>>
                          Ecualizador </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_gato" name="seo_gato" class="checkConfirmar" value="2" <?php if($orden[0]->seo_gato==2){echo 'checked="checked"';}?>>
                          Gato </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_estereo" name="seo_estereo" class="checkConfirmar" value="2" <?php if($orden[0]->seo_estereo==2){echo 'checked="checked"';}?>>
                          Estereo </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_tapon" name="seo_tapon" class="checkConfirmar" value="2" <?php if($orden[0]->seo_tapon==2){echo 'checked="checked"';}?>>
                          Tapon Gas </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_carnet" name="seo_carnet" class="checkConfirmar" value="2" <?php if($orden[0]->seo_carnet==2){echo 'checked="checked"';}?>>
                          Carnet </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_carroceria" name="seo_antena" class="checkConfirmar" value="2" <?php if($orden[0]->seo_carroceria==2){echo 'checked="checked"';}?>>
                          Carroceria </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_ventanas" name="seo_ventanas" class="checkConfirmar" value="2" <?php if($orden[0]->seo_ventanas==2){echo 'checked="checked"';}?>>
                          Ventanas </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_vestidura" name="seo_vestidura" class="checkConfirmar" value="2" <?php if($orden[0]->seo_vestidura==2){echo 'checked="checked"';}?>>
                          Vestidura </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_parabrisas" name="seo_parabrisas" class="checkConfirmar" value="2" <?php if($orden[0]->seo_parabrisas==2){echo 'checked="checked"';}?>>
                          Parabrisas </label>
                      </div>
                    </div>
                  
                  </div>
                  
                  
                 
                  <!--/--> 
                </div>
                <!--/row--> 
                 <div class="row">
                  <div class="col-md-2" style="padding-right:10px;">
                      <div class="form-group">
                        
                      </div>
                    </div>
                  
                    <div class="col-md-8" style="padding-left:145px;">
                      <div class="form-group">
                        <div class="loadservicio"></div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab3">
              
                 <div style=" " > 
         <div style="overflow: hidden; margin-bottom: 5px; width:180px;">
			
		</div>     
		<canvas id="test" style=" float:border: 1px solid red;"></canvas>
		<div class="links" >
			<strong><a href="#" onclick='limpiar();'>Limpiar</a></strong>
			<!--<a href="#" onclick='$("#test").data("jqScribble").save();'>Guardar</a>-->
			<a href="#" onclick='save();'>Guardar</a>
		</div>
        <div class="savechasis"></div>
       </div>
              <iframe src="<?php echo base_url();?>calendario/ajax/index.php?carpeta=<?php echo $orden[0]->sei_carpeta;?>&ido=<?php echo $orden[0]->seo_idServiceOrder;?>" width="80%" frameborder="0" height="380px;"></iframe>
		
        
                
                <!--/row--> 
              </div>
              <div class="tab-pane" id="tab4">
               
                <div class="row">
                  <div class="col-md-12">
                    <table style="font-size:18px; font-weight:bold;" class="table table-striped table-bordered table-hover" id="sample_2">
                      <thead>
                        <tr style="font-weight:bold">
                          <th> <b>Imagen</b> </th>
                          <th> <b>Nombre</b> </th>
                          <th> <b>Descripción</b> </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if($promociones): ?>
                        <?php
                            foreach($promociones as $promocion){ ?>
                        <tr class="odd gradeX">
                          <td><img src="<?php echo base_url().'images/promotions/'.$promocion->ima_name; ?>" 
                                    alt class="img-responsive" width="150px" ></td>
                          <td><?php echo $promocion->pro_name; ?></td>
                          <td><?php echo $promocion->pro_description; ?></td>
                        </tr>
                        <?php } ?>
                        <?php endif ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--/row--> 
              </div>
            </div>
          </div>
          <div class="form-actions fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-offset-3 col-md-9"> <a href="javascript:;" class="btn btn-default button-previous"> <i class="m-icon-swapleft"></i> Regresar </a> <a href="javascript:;" class="btn btn-info button-next"> Continuar <i class="m-icon-swapright m-icon-white"></i> </a>
                

<div class="btn btn-info  button-print firma"> Firma <i class="m-icon-swapright m-icon-white"></i> </div>                   
                
<a style="display:none"  target="_blank" href="<?php echo base_url()?>orden/printtestDuplicadoBueno/<?php echo $orden[0]->seo_idServiceOrder;?>" class="btn btn-info  button-print"> Imprimir Nueva Orden <i class="m-icon-swapright m-icon-white"></i> </a>                 

<?php if($orden[0]->enp_status==1){?>                
                <a href="<?php echo base_url()?>orden/enviarCaptura/<?php echo $orden[0]->seo_idServiceOrder;?>/<?php echo $orden[0]->ser_approximate_duration;?>-<?php echo $orden[0]->seo_technicianTeam;?>" class="btn btn-success button-submit"> Enviar a Captura <i class="m-icon-swapright m-icon-white"></i> </a>
<?php }?>                
                 </div>
              </div>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?> </div>
    </div>
  </div>
</div>

<!-- END CONTENIDO--> 


<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Firma</h4>
      </div>
      <div class="modal-body"> 
        
          Firma
          <canvas id="test_b" style=" border:1px solid #CCC; width:452px; height:232px"></canvas>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <input type="submit" class="btn btn-info" value="Guardar">
      </div>
    </div>
    </form>
    <!-- /.modal-content --> 
  </div>
</div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) --> 
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <p class="size">{%=o.formatFileSize(file.size)%}</p>
            {% if (!o.files.error) { %}
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
            {% } %}
        </td>
        <td>
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                <button class="btn btn-info start">
                    <i class="fa fa-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-danger cancel">
                    <i class="fa fa fa-ban"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script> 
<!-- The template to display files available for download --> 
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="fa fa-trash-o"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="fa fa fa-ban"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('orden/js/script');?>
 <script src="<?php echo base_url();?>signature/jquery.jqscribble.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>signature/jqscribble.extrabrushes.js" type="text/javascript"></script>
		<script type="text/javascript">
		function save()
		{
			$("#test").data("jqScribble").save(function(imageData)
			{
				$('.savechasis').html('Guardando Imagen ...');
					$.post('<?php echo base_url();?>signature/image_save.php?ido=<?php echo $orden[0]->seo_idServiceOrder;?>', {imagedata: imageData}, function(response)
					{
						$('.savechasis').html('Imagen Guardada !');
					});	
				
			});
		}
		function addImage()
		{
			var img = prompt("Enter the URL of the image.");
			alert(img);
			if(img !== '')$("#test").data("jqScribble").update({backgroundImage: img});
		}
		function limpiar(){
			$("#test").jqScribble();
			$("#test").data("jqScribble").update({width:"150",backgroundImage:"../../documents/chasis.png",backgroundImageX:'25',backgroundImageY:'10'});
			}
		$(document).ready(function()
		{
			
			$("#test_b").jqScribble();
			$("#test_b").data("jqScribble").update({width:"452",backgroundImageX:'232',backgroundImageY:'10'});
			$("#test").jqScribble();
			$("#test").data("jqScribble").update({width:"150",backgroundImage:"../../documents/chasis.png",backgroundImageX:'25',backgroundImageY:'10'});
			
			
		});
		</script> 
<script>
var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    username: {
                        minlength: 5,
                        required: true
                    },
                    password: {
                        minlength: 5,
                        required: true
                    },
                    rpassword: {
                        minlength: 5,
                        required: true,
                        equalTo: "#submit_form_password"
                    },
                    //profile
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    //payment
                    card_name: {
                        required: true
                    },
                    card_number: {
                        minlength: 16,
                        maxlength: 16,
                        required: true
                    },
                    card_cvc: {
                        digits: true,
                        required: true,
                        minlength: 3,
                        maxlength: 4
                    },
                    card_expiry_date: {
                        required: true
                    },
                    'payment[]': {
                        required: true,
                        minlength: 1
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]').each(function(){
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }
			
			var verfotos= function() {
			var result='';	

		 
		 
			
			}
		

            var handleTitle = function(tab, navigation, index) {
				
                var total = navigation.find('li').length;
                var current = index + 1;
				
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Paso ' + (index + 1) + ' de ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }
				

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
					$('#form_wizard_1').find('.button-print').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
					$('#form_wizard_1').find('.button-print').hide();
                }
                App.scrollTo($('.page-title'));
				
				
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
					
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }
					if(index==1){
						
						 $.ajax({
	  url:"<?php echo base_url();?>ajax/ordendeservicio/servicios.php?id=<?php echo $orden[0]->seo_idServiceOrder;?>&modelo="+<?php echo $orden[0]->app_vehicleModel;?>+"",
	  success:function(result){
           $('.loadservicio').html(''+result+'');  
                              }
         });	
						}
						if(index==2){
						

var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$('#seo_consecuencias').val();

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_consecuencias&valor="+valor+"",
	  success:function(result){
             
                              }
         });


						}
					if(index==3){
						$.ajax({
	  url:"<?php echo base_url();?>orden/checkFotos?id=<?php echo $orden[0]->seo_idServiceOrder;?>",
	  success:function(resp){
             if(resp==0){
					alert('Para avanzar es necesario tener las Primeras 5 Fotos del Vehículo');	
					return false;
					}
					else{
						  handleTitle(tab, navigation, index);
						}
                              }
         });
					}else{
						 handleTitle(tab, navigation, index);
						}

                  
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();
					
					

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    
					
             var total = navigation.find('li').length;
                    var current = index + 1;
						  var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
						
                   
					
					
					
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                
            }).hide();
        }

    };

}();


</script> 
<script>
jQuery(document).ready(function() {
	
	
 

// first we need a slider to work with
var months = ["E","1/8", "1/4", "3/8", "1/2", "5/8", "3/4", "F"];
<?php
if($orden[0]->seo_combustible=='E' || $orden[0]->seo_combustible==''){$val='0';}
if($orden[0]->seo_combustible=='1/8'){$val='1';}
if($orden[0]->seo_combustible=='1/4'){$val='2';}
if($orden[0]->seo_combustible=='3/8'){$val='3';}
if($orden[0]->seo_combustible=='1/2'){$val='4';}
if($orden[0]->seo_combustible=='5/8'){$val='5';}
if($orden[0]->seo_combustible=='3/4'){$val='6';}
if($orden[0]->seo_combustible=='F'){$val='7';}
?>
$("#slider2").slider({ min: 0, max: 7, value: <?php echo $val;?>,orientation: "horizontal"  });
$("#slider2").slider("pips" , { rest: "label", labels: months })
$("#slider2").on("slidechange", function(e,ui) {

  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id=<?php echo $orden[0]->seo_idServiceOrder;?>&campo=seo_combustible&valor="+months[ui.value]+"",
	  success:function(result){
             
                              }
         });
});


$('.firma').live('click',function(){

	$('#wide').modal('show'); 
	});

$('.saveorden').on('focusout',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});


$('.savetec').on('click',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});


 $('.bootstrap-timepicker').on('click',function(){
var campo=$('input[name=horapro]').attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;

var hr=$('input[name=hour]').val();
var mi=$('input[name=minute]').val();
var me=$('input[name=meridian]').val();
var valor=""+hr+":"+mi+" "+me+""

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});

 $('#savehora').on('click',function(){
var campo=$('input[name=horapro]').attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var hr=$('input[name=hour]').val();
var mi=$('input[name=minute]').val();
var me=$('input[name=meridian]').val();
var valor=""+hr+":"+mi+" "+me+""

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});

$('.torre').on('change',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){  } });
});

$('.vehiculo').on('change',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_IDapp;?>;
var valor=$(this).val();
$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){  
	  
	  $.ajax({
	  url:"<?php echo base_url();?>ajax/ordendeservicio/updateVehiculo.php?id="+<?php echo $orden[0]->seo_idServiceOrder;?>+"&valor="+valor+"",
	  success:function(result){  
	  
	  } });
	  
	  
	  } });
});


$('.pago').on('change',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){  } });
});



$('.checkConfirmar').on('click',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
var mcCbxCheck = $(this);
if(mcCbxCheck.is(':checked')) {
 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });
}
else{
  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor=1",
	  success:function(result){
             
                              }
         });
	}
});


$('.checkConfirmarOtros').on('click',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
var persona=$('input[name=clienteNombre]').val();
var tel=$('input[name=telcliente]').val();
var mcCbxCheck = $(this);
if(mcCbxCheck.is(':checked')) {
 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
		  if(id=='seo_num_poliza'){
			  $('#idseo_num_poliza').show();
			  }
		
		/*insertar nombre y telfono de la persona que recoge al seleccionar la casilla*/
		if(campo=='seo_recoge_check'){	
			$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_persona_recoge&valor="+persona+"",
	  success:function(result){
		  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_telefono_recoge&valor="+tel+"",
	  success:function(result){}});
		  
		  }
			});
		$('input[name=persona]').val(persona);
		$('input[name=tel]').val(tel);		
			
			}	  
		  /*fin*/
                              }
         });
}
else{
  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor=0",
	  success:function(result){
		    if(id=='seo_num_poliza'){
			  $('#seo_num_poliza').hide();
			  }
			  
			  if(campo=='seo_recoge_check'){
				  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_persona_recoge&valor=",
	  success:function(result){
		  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_telefono_recoge&valor=",
	  success:function(result){}});
		  
		  }
			});
		$('input[name=persona]').val('');
		$('input[name=tel]').val('');	
				  }
             
                              }
         });
	}
});





});
</script> 
<script>



	function remove()
{


$(":checkbox:checked").each(
function() {
var ch= $(this).val();


$('#'+ch).remove();
var t=0;

}
); 



} 
$(document).ready(function(){ 


$('.removelinea').live('click',function(){
	
	remove();	
	
});

$('.elimi').on('click',function(){
	
	dele();	
	
});


$('#chall').live('click',function(){
var chk=$('#chall').attr('checked');
if(chk==true){
// Seleccionar un checkbox
$('input:checkbox').attr('checked', true);
}
else{
// Deseleccionar un checkbox
$('input:checkbox').attr('checked', false);
}
});





		
}); 
</script> 
