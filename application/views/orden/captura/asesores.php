<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      <h4 class="modal-title">Cambiar de Asesor</h4>
      </div>
      <div class="modal-body"> 
        <div class="row">
          <div class="col-md-4">
           <div id="infoAsesor"></div>
          </div>
         
        </div>
        
       
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  
    <!-- /.modal-content --> 
  </div>
</div>