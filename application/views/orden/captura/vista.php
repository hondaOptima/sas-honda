<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $menu['menu']='captura';$this->load->view('globales/menu',$menu); ?>
<form name="form1" method="get" action="<?php echo base_url();?>orden/captura/">
<ul class="page-breadcrumb breadcrumb">
  <li> <i class="fa fa-keyboard-o"></i> <a href="<?php echo base_url();?>clientes">Captura de Ordenes de Servicio</a> <i class="fa fa-angle-right"></i> </li>
  
<li class="pull-right" style="text-align:right; margin-left:15px;">
     <?php if($_SESSION['sfrol']<=2) {?>
    
    <select name="taller" onchange='this.form.submit()'>
    <option value="3" <?php if($taller==3){echo'selected';}?>>Tijuana</option>
    <option  value="2" <?php if($taller==2){echo'selected';}?>>Mexicali</option>
    <option value="1" <?php if($taller==1){echo'selected';}?>>Ensenada</option>        
    </select>
    <?php } ?>
    </li> 
   <li class="pull-right" >
            <div  class="input-group input-medium date " id="date-pickerbb" data-date-format="yyyy-mm-dd" style="text-align:right;">
            <input  type="hidden" name="fecha" value="<?php echo $fecha;?>" class="form-control" readonly>
            Fecha: <?php echo $fecha;?> <i style="cursor:pointer; float:right; margin-left:5px;" class="fa fa-calendar  input-group-btn"></i>
            </div>
		</li> 
                       
  
</ul>
</form>
<?php echo $flash_message;?>
<div class="row">
  <div class="col-md-4 col-sm-4"> 
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-users"></i>Ordenes de Servicio por Capturar </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
            
              <th> <b>VIN</b> </th>
              
               <th> <b>O.S. Intelisis</b> </th>
                <th> <b>Acción</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php $tt=1; foreach($ordenes as $dato) {?>
            <tr class="odd gradeX" style="cursor:pointer">
              <td class="file" id="<?= base_url()?>orden/printtestDuplicadoBueno/<?php echo $dato->sev_idVehicleInformation;?>"><?php echo $dato->sev_vin;?></td>
           
              <td><input type="text" name="<?php echo $dato->enp_ID_proceso; ?>" style="border:1px solid white" id=""></td>
              <td align="center"><div class="btn btn-xs btn-success savevin" id="<?php echo $dato->enp_ID_proceso; ?>">Guardar</div></td>
            </tr>
            <?php $tt++;} ?>
          </tbody>
        </table>
      </div>
    </div>
    
    
    
    
     <div class="portlet">
      <div class="portlet-title" style="background-color:green">
        <div class="caption"> <i class="fa fa-users"></i>Ordenes de Servicio Capturadas</div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
            <th>#</th>
           
              <th> <b>VIN</b> </th>
              
               <th> <b>O.S. Intelisis</b> </th> <th>Torre</th>
                <th> <b>Acción</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php $rr=1; foreach($ordenes_cap as $dato) {?>
            <tr class="odd gradeX" style="cursor:pointer">
            <td><?php echo $rr;?></td>
            
              <td class="file" id="<?= base_url()?>orden/printTest/<?php echo $dato->sev_idVehicleInformation;?>"><?php echo $dato->sev_vin;?></td>
           
              <td><input type="text" name="<?php echo $dato->enp_ID_proceso; ?>" style="border:1px solid white;width:50px;" value="<?php echo $dato->seo_serviceOrder;?>" id=""></td>
              <td style="text-align:center;"><b><?php echo $dato->seo_tower;?></b></td>
              <td>
              <a target="_blank" href="<?php echo base_url()?>ajax/imprimir/vin.php?vin=<?php echo $dato->sev_vin;?>">
              <div class="btn btn-xs btn-warning" title="Imprimir Etiqueta QR"><i class="fa fa-qrcode"></i></div></a><div id="<?php echo $dato->enp_ID_proceso; ?>" class="btn btn-xs btn-success updatevin">Actualizar</div></td>
            </tr>
            <?php $rr++; } ?>
          </tbody>
        </table>
      </div>
    </div>
    
    
    <div class="portlet">
      <div class="portlet-title" style="background-color:#ed9c28">
        <div class="caption"> <i class="fa fa-users"></i>Imprimir Etiquetas QR en Par</div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
              <th> <b>VIN 1</b> </th>
              
               <th> <b>VIN 2</b> </th>
                <th> <b>Acción</b> </th>
            </tr>
          </thead>
          <tbody>
            <?php
			$num=count($ordenes_cap);
			$y=0;
			 for($x=0; $x <$num; $x++) {?>
             <?php if($y==0) { echo ' <tr class="odd gradeX" style="cursor:pointer">';}?>
           
              <td><?php echo $ordenes_cap[$x]->sev_vin;?></td>
            
 
              
            <?php 
			if($x==1){
			if($y==1){ $y=0;
			echo '<td>
<a target="_blank" href="'.base_url().'/clientes/printQRD/'.$ordenes_cap[$x-1]->sev_vin.'/'.$ordenes_cap[$x]->sev_vin.'">
            <div class="btn btn-xs btn-warning" title="Imprimir Etiqueta QR"><i class="fa fa-qrcode"></i></div></a></td></tr><tr>';
			}}
			else{
				
				if($y==2){ $y=0;
			echo '<td>
<a target="_blank" href="'.base_url().'/clientes/printQRD/'.$ordenes_cap[$x-1]->sev_vin.'/'.$ordenes_cap[$x]->sev_vin.'">
            <div class="btn btn-xs btn-warning" title="Imprimir Etiqueta QR"><i class="fa fa-qrcode"></i></div></a></td></tr><tr>';
			}
				}?>
            
            <?php $y++; } ?>
            
          </tbody>
        </table>
      </div>
    </div>
    
    
    
    
     <div class="portlet">
      <div class="portlet-title" style="background-color:#46b8da">
        <div class="caption"> <i class="fa fa-users"></i>Ordenes de Servicio en Llenado</div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" >
          <thead>
            <tr style="font-weight:bold">
              <th> <b>VIN</b> </th>
              
               <th> <b>Asesor de Ventas</b> </th>
               <th> <b>Acciones</b> </th>
               
            </tr>
          </thead>
          <tbody>
            <?php foreach($ordenesPen as $dato) {?>
            <tr class="odd gradeX" style="cursor:pointer">
              <td class="file" id="<?= base_url()?>orden/printTest/<?php echo $dato->sev_idVehicleInformation;?>">              <?php echo $dato->sev_vin;?>
              </td>
           
              <td>
              <?php echo $dato->sus_name.' '.$dato->sus_lastName;?>
              </td>
              <td>
              <a target="_blank" href="<?php echo base_url()?>orden/llenar/<?php echo $dato->seo_idServiceOrder;?>">
              <div  class="btn btn-xs btn-success">Ver</div>
              </a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    
    
    
  </div>
  
   <div class="col-md-8 col-sm-8"> 
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    
      <div class="portlet" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Porcentaje de Utilización de Taller
                </div>
               
            </div>
            <div class="portlet-body form"   >
            
            
            
            <form role="form" class="form-horizontal form-bordered">
               <div class="form-group last">
										<div class="col-md-12">
											<input id="range_333" type="text" name="range_3" value="0;100"/>
											
											
										</div>
                                        </div>
                
                </form>
                
            </div>
        </div> 
    
    
   
    <div class="portlet">
      <div class="portlet-title" style="background-color:#999">
        <div class="caption"> <i class="fa fa-users"></i>Vista Previa de Orden de Servicio Seleccionada </div>
        <input type="hidden" name="idseleccionada" value="0">
        
        <?php if($_SESSION['sfrol']==2 || $_SESSION['sfrol']==1){?>
         <div id="getAsesores" class="tools"><div class="btn btn-xs btn-info">Cambiar de Asesor</div></div>
        <div id="eliminaros" class="tools"><div class="btn btn-xs btn-danger">Eliminar</div></div>
      <?php }?>
      </div>
      <div class="portlet-body">
       <iframe id="framesrc" src="" width="100%" frameborder="0" height="500px;"></iframe> 
      </div>
    </div>
  </div>
</div>

<!-- END EXAMPLE TABLE PORTLET-->
<?php $this->load->view('orden/captura/asesores');?>
<?php $this->load->view('globales/footer');?>
<?php 
$data['utilizacion']=$utilizacion;
$this->load->view('cotizaciones/js/script',$data);?>
<script>
jQuery(document).ready(function() {
	
	$(".changease").live('click',function() {

       var id=$(this).attr('id');
       var value=$(this).val();
	   
       $.ajax({
	  url:"<?php echo base_url();?>orden/updateOrdenAsesor?id="+id+"&valor="+value+"",
	  success:function(result){
		 window.location="<?php echo base_url();?>orden/captura?fecha=<?php echo  $fecha;?>";  
		         }
         });
       
       
    });
	
	
	
	
	$(function () {
	 $('#date-pickerbb').datepicker({ 
	language: 'es',
	isRTL: false,
         autoclose:true
                
	 }).on('changeDate', function(ev){
            window.location.href = "?fecha=" + ev.format();
        });

	 
	});
	
	
	$(".file").live('click',function() {

       var tit=$(this).attr('id');

       $("#framesrc").attr("src", $(this).attr("id"));
       $('input[name=idseleccionada]').val(tit);
       
    });
	
	$("#eliminaros").live('click',function() {
		var id=$('input[name=idseleccionada]').val();
		
		var el = id.split('/');
		un=el[0];
		ds=el[1];
		tr=el[2];
		cu=el[3];
		cn=el[4];
		se=el[5];
		si=el[6];
		if (confirm('Realmente desea Eliminar Esta Orden de Servicio?')) {
			
   window.location="<?php echo base_url()?>orden/eliminarOrden/"+se+"";
} else {

}
		});
		
		
		$("#getAsesores").live('click',function() {
		var id=$('input[name=idseleccionada]').val();
		$('#wide').modal('show');
		var el = id.split('/');
		un=el[0];
		ds=el[1];
		tr=el[2];
		cu=el[3];
		cn=el[4];
		se=el[5];
		si=el[6];
		
		 $.ajax({
	  url:"<?php echo base_url();?>ajax/ordendeservicio/getAsesores.php?id="+si+"&taller=<?php echo $taller;?>",
	  success:function(result){
		   $('#infoAsesor').html(result);
		         }
         });
		
		
		});
		
		
	
	$(".savevin").live('click',function() {

       var id=$(this).attr('id');
       var value=$('input[name='+id+']').val();
	   
       $.ajax({
	  url:"<?php echo base_url();?>orden/updateOrdenCaptura?id="+id+"&valor="+value+"",
	  success:function(result){
		   window.location="<?php echo base_url();?>orden/captura?fecha=<?php echo  $fecha;?>";
		         }
         });
       
       
    });
	
	
	$(".updatevin").live('click',function() {

       var id=$(this).attr('id');
       var value=$('input[name='+id+']').val();
	   
       $.ajax({
	  url:"<?php echo base_url();?>orden/updateOrdenCaptura?id="+id+"&valor="+value+"",
	  success:function(result){
		   window.location="<?php echo base_url();?>orden/captura?fecha=<?php echo  $fecha;?>";
		   
		   
		         }
         });
       
       
    });


});
</script>