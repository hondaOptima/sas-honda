<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orden de Servicio</title>
</head>

<body>


<div style="width:850px;height:1100px;font-family:Verdana, Geneva, sans-serif;" >
	<div style="width:100%;height:180px;margin-bottom:10px;float:left;margin-bottom:0px;">
    	<div style="width:55%;height:100%;float:left;">
        	<div style="width:100%;height:70px;background-image:url('<?php echo base_url(); ?>documents/hlogo.png');background-size: 80%;background-repeat:no-repeat;"></div>
            <div>
            	<table style="width:100%;height:100%;border:none;border-collapse:collapse;">
                    	<tr style="font-size:9px;">
                        	<th style="text-align:right;padding-right:3px;border-right:1px solid rgb(0,0,0);color:rgb(226,0,43);">TIJUANA:</th>
                        	<th style="text-align:left;padding-left:3px;color:rgb(226,0,43);">MEXICALI:</th>
                        </tr>
                    	<tr style="font-size:8px;">
                        	<td style="text-align:right;padding-right:3px;border-right:1px solid rgb(0,0,0);">Av. Padre Kino 4300 Zona Río, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td style="text-align:left;padding-left:3px;">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr style="font-size:8px;">
                        	<th style="text-align:right;padding-right:3px;color:rgb(226,0,43);">ENSENADA:</th>
                        	<td style="font-size:10px;text-align:center;">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr style="font-size:8px;">
                        	<td tyle="text-align:right;
padding-right:3px;">Ensenada Av. Balboa 146 esq.<br />
							López Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td style="font-size:10px;text-align:center;">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            Sábado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>
            </div>
        </div>
        <div style="width:45%;height:100%;float:left;">
        	<div style="width:100%;height:25px;padding-top:20px;font-size:12px;font-weight:bold;text-align:right;">
        		GARANTÍA PAGO CLIENTE INTERNO
        </div>
        <div style="width:100%;height:120px;font-size:9px;">
        	<div style="width:60%;height:60px;float:left;">
                <p style="width:98%;margin:auto;margin-top:20px;padding:5px;border:0px solid rgb(51,51,51);border-radius:15px;background-color:rgb(226,0,43);color:rgb(255,255,255);font-size:8px;text-align:justify;line-height:180%;">SÓLO A LA PRESENTACIÓN DE ESTA CONTRASEÑA SE HARÁ ENTREGA DEL VEHÍCULO, SUPLICAMOS LA CONSERVE 
                PUES NO NOS HACEMOS RESPONSABLES DEL MAL USO QUE SE HAGA DE ELLA.</p>
			</div>
            	<div style="width:40%;height:100%;float:left;">
                	<table style="width:80%;margin:auto;margin-top:20px;font-size:8px;border:2px solid rgb(102,102,102);border-spacing:0;text-align:center;border: 1px solid rgb(51,51,51);border-radius: 10px;border-spacing:0;overflow:hidden;">
                        	<tr style="background-color:rgb(102,102,102);color:rgb(255,255,255);font-size:8.4px;font-weight:900;">
                            	<td>ORDEN DE SERVICIO</td>
                            </tr>
                            <tr style="height:27px;font-size:8.4px;font-weight:900;vertical-align:text-top;text-align:left;">
                            	<td>AAAA</td>
                            </tr>
                            <tr style="background-color:rgb(102,102,102);color:rgb(255,255,255);font-size:8.4px;font-weight:900;">
                            	<td>FECHA</td>
                            </tr>
                            <tr style="height:27px;font-size:8.4px;font-weight:900;vertical-align:text-top;text-align:left;">
                            	<td>AAAA</td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
    	<div class="customer-info">
   	  <table class="radius">
                    <tr class="first-row">
                        <th colspan="2" class="title">DATOS DEL CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-3">Nombre: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Dirección: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Ciudad: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">R.F.C.: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Teléfono: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Atención a: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Teléfono: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
            </table>
        </div>
        <div class="vehicle-info">
        	<table class="radius">
                    <tr>
                        <th colspan="4" class="title">DATOS DEL VEHÍCULO</th>
                    </tr>
                    <tr>
                        <td class="col-3">Modelo: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Placas: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Año: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Último Servicio: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Fecha de Venta: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Seguro: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Color: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Km. de Entrada: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">No. de  Serie: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">No. de Motor: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Transmisión: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
            </table>
        </div>
    </div>
	<div class="row">
   	  <div class="reception-info">
  			<table class="radius">
                	<tr>
                    	<td class="col-4">Fecha y hora de recepción: </td>
                    	<td class="col-8">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Asesor: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Técnico: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-4">Fecha y hora prometida: </td>
                    	<td class="col-8">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-4">Monto orden de servicio: </td>
                    	<td class="col-8">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Torre: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Firma del cliente: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
            </table>
        </div>
      <div class="confirmation" style="width:100%;border: 1px solid rgb(51,51,51);border-radius: 10px;border-spacing:0;overflow:hidden;">
        	<p class="confirm">GARANTIZO Y ASEGURO QUE SOY DUEÑO O ESTOY AUTORIZADO PARA ORDENAR ESTA REPARACIÓN CON LA 
            PRESENTE AUTORIZO EL TRABAJO DESCRITO JUNTO CON LAS PIEZAS DE REPUESTO Y/O MATARIALES NECESARIOS PARA EFECTUARLOS.</p><br /><br />

			<p>FIRMA DEL CLIENTE: __________________________________</p>
        </div>
    </div>
	<div class="row">
    	<div class="col-10" style="float:left;">
            <div class="comments">
            	<div class="text" style="width:100%;border: 1px solid rgb(51,51,51);border-radius: 10px;border-spacing:0;overflow:hidden;">
                	<span class="title">COMENTARIOS:</span>
                </div>
            </div>
            <div class="items">
                <table>
                        <tr>
                            <td>TAPETES</td>
                            <td>TAPONES</td>
                            <td>HERRAMIENTA</td>
                            <td>REFLEJANTES</td>
                            <td>ECUALIZADOR</td>
                            <td>TAPÓN GAS</td>
                            <td>VENTANAS</td>
                        </tr>
                        <tr>
                            <td>ESPEJO</td>
                            <td>RADIO</td>
                            <td>LLANTA REF.</td>
                            <td>EXTINGUIDOR</td>
                            <td>GATO</td>
                            <td>CARNET</td>
                            <td>VESTIDURA</td>
                        </tr>
                        <tr>
                            <td>ANTENA</td>
                            <td>ENCENDEDOR</td>
                            <td>LIMPIADORES</td>
                            <td>CABLES P/C</td>
                            <td>ESTEREO</td>
                            <td>CARROCERÍA</td>
                            <td>PARABRISAS</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>CANTIDAD COMB.</td>
                        </tr>
                </table>
            </div>
            <div class="tecs">
                <table class="radius" >
                        <tr>
                            <td>MEC.</td>
                            <td>MEC.</td>
                            <td>MEC.</td>
                            <td>MEC.</td>
                        </tr>
                        <tr>
                            <td>HR.</td>
                            <td>HR.</td>
                            <td>HR.</td>
                            <td>HR.</td>
                        </tr>
                </table>
            </div>
        </div>
        <div class="col-2" style="float:left;">
        	<div class="chasis">
            </div>
        </div>
    </div>
	<div class="row">
    	<table class="services-head" style="width:100%;border: 1px solid rgb(51,51,51);border-radius: 10px;border-spacing:0;overflow:hidden;">
        	<tr>
            	<th style="border-right:1px solid rgb(226,0,43);" class="col-2">CLAVE</th>
            	<th class="col-5">DESCRIPCIÓN</th>
            	<th class="col-2">P.U.</th>
            	<th class="col-2">CANTIDAD</th>
            	<th class="col-1">P. TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
            	<tr>
                	<td  style="border-right:1px solid rgb(226,0,43);"  class="col-2">AAAA</td>
                	<td class="col-5">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-1">AAAA</td>
                </tr>
            	<tr>
                	<td style="border-right:1px solid rgb(226,0,43);">AAAA</td>
                	<td class="col-5">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-1">AAAA</td>
                </tr>
            	<tr>
                	<td style="border-right:1px solid rgb(226,0,43);">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
            	<tr>
                	<td style="border-right:1px solid rgb(226,0,43);">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
            	<tr>
                	<td style="border-right:1px solid rgb(226,0,43);">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
				<tr>
                  	<td style="border-right:1px solid rgb(226,0,43);">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
            	<tr>
                  	<td style="border-right:1px solid rgb(226,0,43);"></td>
                	<td colspan="3" style="
text-align:right;">Precio total de refacciones y mano de obra</td>
                	<td><p style="height:15px;margin:5px;vertical-align:center-top;border:1px solid rgb(0,0,0);border-radius:5px;">AAAA</p></td>
                </tr>
        </table>
    </div>
</div>
</body>
</html>