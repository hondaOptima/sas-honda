<?php $this->load->view('globales/head'); ?>
<?php $this->load->view('globales/topbar'); ?>
<?php $this->load->view('globales/menu'); ?>

<!-- BEGIN CONTENIDO-->

<div class="row">
  <div class="col-md-12">
    <div class="portlet" id="form_wizard_1">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-reorder"></i> Nueva Orden de Servicio - <span class="step-title"> Paso 1 de 4 </span> </div>
      </div>
      <div class="portlet-body form"> <?php echo form_open_multipart('orden/crear','class="form-horizontal" id="submit_form"'); ?>
      
      
        <div class="form-wizard">
          <div class="form-body">
            <ul class="nav nav-pills nav-justified steps">
              <li style="width:350px; "> <a href="#tab1" data-toggle="tab" class="step"> <span class="number" style="float:left"> 1 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Datos del Cliente </span> </a> </li>
              <li style="width:350px;"> <a href="#tab2" data-toggle="tab" class="step"> <span class="number" style="float:left"> 2 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Revisión del Veh&iacute;culo </span> </a> </li>
              <li style="width:350px;"> <a href="#tab3" data-toggle="tab" class="step active"> <span class="number" style="float:left"> 3 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Fotos del Veh&iacute;culo </span> </a> </li>
              <li style="width:350px;"> <a href="#tab4" data-toggle="tab" class="step"> <span class="number" style="float:left"> 4 </span> <span class="desc" style="float:left"> <i class="fa fa-check"></i> Promociones </span> </a> </li>
            </ul>
            
            <?php //print_r($orden);?>
            <div class="tab-content" style=" margin-top:10px;">
              <div class="alert alert-danger display-none">
                <button class="close" data-close="alert"></button>
                Alguna informaci&oacute;n del formulario es incorrecta. </div>
              <div class="alert alert-success display-none">
                <button class="close" data-close="alert"></button>
                Los datos est&aacute;n correctos! </div>
              <div class="tab-pane active" id="tab1">
             
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Nombre <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteNombre',$orden[0]->sec_nombre,
                                                            'id="sec_nombre" class="form-control saveorden"
                                                            placeholder="Nombre" required');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">R.F.C. <span class="required"> * </span> </label>
                      <div class="col-md-9">
                         <?php  echo form_input('clienteRfc',
															$orden[0]->sec_rfc, 
                                                            'id="sec_rfc" class="form-control saveorden"
                                                            placeholder="RFC" maxlenght="20" required');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Email <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteDireccion',
															$orden[0]->sec_email, 
                                                            'id="sec_email" class="form-control saveorden" 
                                                            placeholder="Email"  required');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Calle <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteCiudad',
															$orden[0]->sec_calle, 
                                                            'id="sec_calle" class="form-control saveorden"
                                                            placeholder="Calle" required');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">No. Int. o Ext. <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteTelefono',
															$orden[0]->sec_num_ext, 
                                                            'id="sec_num_ext" class="form-control saveorden"
                                                            placeholder="No. Int. o Ext." ');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Colonia <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteRfc',
															$orden[0]->sec_colonia, 
                                                            'id="sec_colonia" class="form-control saveorden"
                                                            placeholder="Colonia" maxlenght="20" required');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row--> 
                
                <!--/row-->
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">C&oacute;digo Postal <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteAtencionA',
															$orden[0]->sec_cp, 
                                                            'id="sec_cp" class="form-control saveorden"
                                                            placeholder="Codigo Postal" ');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Delegaci&oacute;n o Municipio <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteAtTel',
															$orden[0]->sec_ciudad, 
                                                            'id="sec_ciudad" class="form-control saveorden"
                                                            placeholder="Delegacion o Municipio"  ');?>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Estado <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('clienteAtTel',
															$orden[0]->sec_estado, 
                                                            ' id="sec_estado" class="form-control saveorden"
                                                            placeholder="Estado"  ');?>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Tel&eacute;fonos<span class="required"> * </span> </label>
                      <div class="col-md-9">
                         <?php  echo form_input('telcliente',
															$orden[0]->sec_tel, 
                                                            'id="sec_tel" class="form-control saveorden"
                                                            placeholder="Tel&eacute;fonos"  ');?>
                      </div>
                    </div>
                  </div>
                  </div>
                  
                                  <div class="row">
                                  
 <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-7">Si es la misma persona click en la siguiente casilla </label>
                      <div class="col-md-5">
                        <input type="checkbox" style="font-size:1.5em;width:1.5em;height:1.5em;" name="seo_recoge_check" id="seo_recoge_check" class="checkConfirmarOtros" value="1" <?php if($orden[0]->seo_recoge_check=='1'){echo 'checked';}?>>
                      </div>
                    </div>
                  </div>                                  

                   <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Persona autorizada para recoger Unidad </label>
                      <div class="col-md-9">
                         <?php  echo form_input('persona',
															$orden[0]->seo_persona_recoge, 
                                                            'id="seo_persona_recoge" class="form-control saveorden"
                                                            placeholder="Nombre"  ');?>
                      </div>
                    </div>
                  </div>
                  
                  
                   <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Tel&eacute;fono persona autorizada</label>
                      <div class="col-md-9">
                         <?php  echo form_input('tel',
															$orden[0]->seo_telefono_recoge, 
                                                            'id="seo_telefono_recoge" class="form-control saveorden"
                                                            placeholder="Tel&eacute;fono"  ');?>
                      </div>
                    </div>
                  </div>
                  
                  
                  </div>
                
                <!--/row-->
                
                <div style="font-weight:bold; font-size:18px; margin-left:48px; margin-top:15px;"><span class="number"></span>Datos del Veh&iacute;culo</div>
                <div class="row" style=" padding-top:15px;">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Marca<span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <input type="text" name="fechaVenta" id="sev_marca" 
                                                        class="form-control saveorden" value="<?php echo $orden[0]->sev_marca;?>"  >
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-6">Submarca <span class="required"> * </span> </label>
                      <div class="col-md-6">
                        <?php  echo form_input('vehiculoUltimoSer',$orden[0]->sev_sub_marca, 
                                                            'id="sev_sub_marca" class="form-control saveorden"
                                                            placeholder="Sub Marca" ');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Año - Modelo<span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoVin',$orden[0]->sev_modelo, 
                                                            'id="sev_modelo" class="form-control saveorden"
                                                            placeholder="Modelo" ');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Tipo o versi&oacute;n <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoModelo',$orden[0]->sev_version,
                                                            'id="sev_version" class="form-control saveorden"
                                                            placeholder="Tipo o Version" ');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Capacidad <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoAno',$orden[0]->sev_capacidad, 
                                                            'id="sev_capacidad" class="form-control saveorden"
                                                            placeholder="Capacidad" ');?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">N&uacute;mero de Placas <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoColor',$orden[0]->sev_placas,
                                                            'id="sev_placas" class="form-control saveorden"
                                                            placeholder="Placas" ');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">Color <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoPlacas',$orden[0]->sev_color, 
                                                            'id="sev_color" class="form-control saveorden"
                                                            placeholder="Color" ');?>
                      </div>
                    </div>
                  </div>
                  <!--/-->
                 
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">VIN <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoMotor',$orden[0]->sev_vin, 
                                                            'id="sev_vin" class="form-control saveorden"
                                                            placeholder="VIN" ');?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/row-->
                <div class="row"> 
                  
                  <!--/-->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3">No. Km. Recorridos <span class="required"> * </span> </label>
                      <div class="col-md-9">
                        <?php  echo form_input('vehiculoTrans',$orden[0]->sev_km_recepcion,
                                                            'id="sev_km_recepcion" class="form-control saveorden"
                                                            placeholder="No. Km. Recorridos" ');?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div class="tab-pane" id="tab2">
                
                <div class="row">
                  <div class="col-md-6">
                    <div class="col-md-5" >
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-md-2">Torre<span class="required">*</span> </label>
                          <div class="col-md-7">
                            <select name="torre"  id="seo_tower" class="torre select2_category form-control" required>
                            <option value="">Seleccione una Opción</option>
                              <?php
                                                        for($torre=1; $torre<191; $torre++){
															?>
                              <option class="saveordenc  "  value="<?php echo $torre;?>" <?php if($torre==$orden[0]->seo_tower){echo 'selected="selected"';}?>><?php echo $torre;?></option>
                              <?php
															}
														?>
                            </select>
                          </div>
                          
                           
                          
                        </div>
                       
                          
                          <div class="form-group" >
                          <table width="140px;"  style="background-color:green; margin-left:55px; color:white; border-radius:3px;"><tr><td style="padding:10px;">Se entregan las partes o refacciones reemplazadas al consumidor?
                          </td><td valign="middle" align="center" style="padding-right:7px;"><input type="checkbox" 
                          name="seo_se_entregan" id="seo_se_entregan" class="checkConfirmarOtros" value="1" <?php if($orden[0]->seo_se_entregan=='1'){echo 'checked';}?>></td></tr></table>
                          </div>
                           <div class="form-group" >
                          <table width="140px;"  style="background-color:green; margin-left:55px; color:white; border-radius:3px;"><tr><td style="padding:10px;">Servicio en domicilio del consumidor?
                          </td><td valign="middle" align="center" style="padding-right:7px;"><input type="checkbox" 
                          name="seo_ser_domi" id="seo_ser_domi" class="checkConfirmarOtros" value="1" 
						  <?php if($orden[0]->seo_ser_domi=='1'){echo 'checked';}?>></td></tr></table>
                          </div>
                          
                          
                          
                          
                          
                          
                      </div>
                      <div class="row"> </div>
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-md-3">T&eacute;cnico </label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group">
                          <div class="col-md-10" style="padding-left:50px;" >
                            <?php
                                                   foreach($tecnicos as $tec){
													   if($tec->sus_idUser==$orden[0]->seo_technicianTeam){$check='checked="checked"';}else{$check='';}
	echo '<input type="radio" class="savetec" required id="seo_technicianTeam" name="tec" value="'.$tec->sus_idUser.'" '.$check.'>'.$tec->sus_name.' '.$tec->sus_lastName.'<br>';  
													   }
												   ?>
                          </div>
                        </div>
                      </div>
                      <!--/--> 
                      
                      <!--/--> 
                      
                      <!--/-->
                      <div class="row"> </div>
                    </div>
                    <div class="col-md-6">
                     
                      <div class="form-group">
                        <label class="control-label col-md-5 ">Vehículo:</label>
                        <div class="col-md-7">
                         
                          
                           <?php	echo form_dropdown('vehiculo', $vehiculos, $orden[0]->app_vehicleModel,
										'id="app_vehicleModel" class="vehiculo select2_category form-control" 
										data-rel="chosen" required');	?>
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-5">Hora Prometida <?php echo $orden[0]->seo_promisedTime; ?></label>
                        <div class="col-md-7">
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" name="horapro" id="seo_promisedTime" value="<?php echo $orden[0]->seo_promisedTime; ?>" class="form-control timepicker-default" required>
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                            </span> </div>
                        </div>
                      </div>
                     
                       
                      <div class="form-group">
                        <label class="col-md-5 control-label">Combustible</label>
                        <div class="col-md-7">
                          <div id="slider2"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-md-5">Comentarios </label>
                          <div class="col-md-12">
                            <?php  echo form_textarea('comentarios',
															$orden[0]->seo_comments, 
                                                            'id="seo_comments" class="form-control saveorden" 
                                                            placeholder="Comentarios" 
                                                            style="resize: none; height:60px;"');?>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-md-8">Posibles Consecuencias </label>
                          <div class="col-md-12">
                            <?php  echo form_textarea('consecuencias',
															$orden[0]->seo_consecuencias, 
                                                            'id="seo_consecuencias" class="form-control saveorden" 
                                                            placeholder="Posibles Consecuencias" 
                                                            style="resize: none; height:60px;"');?>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-md-5">Tipo de Pago<span class="required">*</span> </label>
                          <div class="col-md-7">
                            <select name="pago"  id="seo_pago" class="pago select2_category form-control">
                              <option value="1" <?php if($orden[0]->seo_pago==1){echo 'selected';}?>>Efectivo</option>
                               <option value="2" <?php if($orden[0]->seo_pago==2){echo 'selected';}?>>Cheque</option>
                                <option value="3" <?php if($orden[0]->seo_pago==3){echo 'selected';}?>>Tarjeta de Cr&eacute;dito</option>
                                 <option value="4" <?php if($orden[0]->seo_pago==4){echo 'selected';}?>>Otro</option>
                            </select>
                          </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  
                  <!--/--> 
                  <!--/-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-2">Articulos</label>
                      <br />
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_tapetes" name="seo_tapetes" class="checkConfirmar" value="2" <?php if($orden[0]->seo_tapetes==2){echo 'checked="checked"';}?>>
                          Tapetes </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_espejo" name="seo_espejo" class="checkConfirmar" value="2" <?php if($orden[0]->seo_espejo==2){echo 'checked="checked"';}?>>
                          Espejo </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_antena" name="seo_antena" class="checkConfirmar" value="2" <?php if($orden[0]->seo_espejo==2){echo 'checked="checked"';}?>>
                          Antena </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_tapones" name="seo_tapones" class="checkConfirmar" value="2" <?php if($orden[0]->seo_tapones==2){echo 'checked="checked"';}?>>
                          Tapones </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_radio" name="seo_radio" class="checkConfirmar" value="2" <?php if($orden[0]->seo_radio==2){echo 'checked="checked"';}?>>
                          Radio </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_encendedor" name="seo_encendedor" class="checkConfirmar" value="2" <?php if($orden[0]->seo_encendedor==2){echo 'checked="checked"';}?>>
                          Encendedor </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_herramienta" name="seo_herramienta" class="checkConfirmar" value="2" <?php if($orden[0]->seo_herramienta==2){echo 'checked="checked"';}?>>
                          Herramienta </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_llantarefaccion" name="seo_llantarefaccion" class="checkConfirmar" value="2" <?php if($orden[0]->seo_llantarefaccion==2){echo 'checked="checked"';}?>>
                          Llanta Ref. </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_limpiadores" name="seo_limpiadores" class="checkConfirmar" value="2" <?php if($orden[0]->seo_limpiadores==2){echo 'checked="checked"';}?>>
                          Limpiadores </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_reflejantes" name="seo_reflejantes" class="checkConfirmar" value="2" <?php if($orden[0]->seo_reflejantes==2){echo 'checked="checked"';}?>>
                          Reflejantes </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_extinguidor" name="seo_extinguidor" class="checkConfirmar" value="2" <?php if($orden[0]->seo_extinguidor==2){echo 'checked="checked"';}?>>
                          Extinguidor </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_cables" name="seo_cables" class="checkConfirmar" value="2" <?php if($orden[0]->seo_cables==2){echo 'checked="checked"';}?>>
                          Cables P/C </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_ecualizador" name="seo_ecualizador" class="checkConfirmar" value="2" <?php if($orden[0]->seo_ecualizador==2){echo 'checked="checked"';}?>>
                          Ecualizador </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_gato" name="seo_gato" class="checkConfirmar" value="2" <?php if($orden[0]->seo_gato==2){echo 'checked="checked"';}?>>
                          Gato </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_estereo" name="seo_estereo" class="checkConfirmar" value="2" <?php if($orden[0]->seo_estereo==2){echo 'checked="checked"';}?>>
                          Estereo </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_tapon" name="seo_tapon" class="checkConfirmar" value="2" <?php if($orden[0]->seo_tapon==2){echo 'checked="checked"';}?>>
                          Tapon Gas </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_carnet" name="seo_carnet" class="checkConfirmar" value="2" <?php if($orden[0]->seo_carnet==2){echo 'checked="checked"';}?>>
                          Carnet </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_carroceria" name="seo_antena" class="checkConfirmar" value="2" <?php if($orden[0]->seo_carroceria==2){echo 'checked="checked"';}?>>
                          Carroceria </label>
                      </div>
                      <label class="control-label col-md-2"></label>
                      <div class="checkbox-list col-md-10">
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_ventanas" name="seo_ventanas" class="checkConfirmar" value="2" <?php if($orden[0]->seo_ventanas==2){echo 'checked="checked"';}?>>
                          Ventanas </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_vestidura" name="seo_vestidura" class="checkConfirmar" value="2" <?php if($orden[0]->seo_vestidura==2){echo 'checked="checked"';}?>>
                          Vestidura </label>
                        <label class="checkbox-inline">
                          <input type="checkbox"  id="seo_parabrisas" name="seo_parabrisas" class="checkConfirmar" value="2" <?php if($orden[0]->seo_parabrisas==2){echo 'checked="checked"';}?>>
                          Parabrisas </label>
                      </div>
                    </div>
                  
                  </div>
                  
                  
                 
                  <!--/--> 
                </div>
                <!--/row--> 
                 <div class="row">
                  <div class="col-md-2" style="padding-right:10px;">
                      <div class="form-group">
                        
                      </div>
                    </div>
                  
                    <div class="col-md-8" style="padding-left:145px;">
                      <div class="form-group">
                        <div class="loadservicio">

                        </div>
                        <div class="display-none" id="camp"  padding="15px">
                          <?php
                            if (empty($campanias)){
                                   echo '<div style="color:red">No se encontraron campañas para este vehiculo</div>'; 
                                  }
                            else { ?>
                              <table class="table" width="100%" style="width:100%; border-bottom:1px solid #999; border-right:1px solid #999;  text-align:center">
                               <thead>
                                      <tr style="background-color:red; color:yellow; text-align:center;">
                                        <th style="border-bottom:1px solid #999; border-right:1px solid #999; width:243px; text-align:center" >Realizar</th>
                                        <th style="border-bottom:1px solid #999; border-right:1px solid #999; width:243px; text-align:center" ># de garantia</th>                                     
                                      </tr>
                                    </thead>
                                <tbody>
                                 <?php foreach($campanias as $c) { 
                                   $number = substr($c->cam_campania, 0, 6); 
                                $checked = '';
                                if($c->cam_status==2){
                                    $checked = '';
                                }
                                else{
                                    $checked = 'checked';  
                                }
                                     


                                  echo '
                                  <tr>
                                   <td style="border-bottom:1px solid #999; border-right:1px solid #999"> <input id="chk-campaign-'.$number.'"
                                   garantia="'.$number.'" order="'.$order.'"
                                   autofocus type="checkbox" '.$checked.' class="chk-campaign" /> </td>
                                   <td > <u title="Boletin PDF"><a style="color:blue" href="'.base_url().'assets/img/boletines/'.$number.'.PDF" target="_blank">'.$number.'</a></u> </td>
                                  </tr>';
                                 } ?>
                                </tbody>
                              </table>
                            
                         
                            
                           <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab3">
              
                 <div style=" " > 
         <div style="overflow: hidden; margin-bottom: 5px; width:180px;">
			
		</div>     
		<canvas id="test" style=" float:border: 1px solid red;"></canvas>
		<div class="links" >
			<strong><a href="#" onclick='limpiar();'>Limpiar</a></strong>
			<!--<a href="#" onclick='$("#test").data("jqScribble").save();'>Guardar</a>-->
			<a href="#" onclick='save();'>Guardar</a>
		</div>
        <div class="savechasis"></div>
       </div>
              <iframe src="<?php echo base_url();?>calendario/ajax/index.php?carpeta=<?php echo $orden[0]->sei_carpeta;?>&ido=<?php echo $orden[0]->seo_idServiceOrder;?>" width="80%" frameborder="0" height="380px;"></iframe>
                <!--/row--> 
              </div>
              <div class="tab-pane" id="tab4">
               
                <div class="row">
                  <div class="col-md-12">
                    <table style="font-size:18px; font-weight:bold;" class="table table-striped table-bordered table-hover" id="sample_2">
                      <thead>
                        <tr style="font-weight:bold">
                          <th> <b>Imagen</b> </th>
                          <th> <b>Nombre</b> </th>
                          <th> <b>Descripción</b> </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if($promociones): ?>
                        <?php
                            foreach($promociones as $promocion){ ?>
                        <tr class="odd gradeX">
                          <td><img src="<?php echo base_url().'images/promotions/'.$promocion->ima_name; ?>" 
                                    alt class="img-responsive" width="150px" ></td>
                          <td><?php echo $promocion->pro_name; ?></td>
                          <td><?php echo $promocion->pro_description; ?></td>
                        </tr>
                        <?php } ?>
                        <?php endif ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--/row--> 
              </div>
            </div>
          </div>
          <div class="form-actions fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-offset-3 col-md-9"> 
                    <a href="javascript:;" class="btn btn-warning btn-lg button-previous" style="float:left !important;margin-left:10%; width:200px !important; height:80px !important;"> 
                        <i class="fa fa-arrow-left" style="font-size:3em;margin-top:18px !important;"></i> 
                    </a> 
                    <a href="javascript:;" class="btn btn-info btn-lg button-next" style="margin-left:20%; width:200px !important; height:80px !important;"> 
                        <i class="fa fa-arrow-right" style="font-size:3em;margin-top:18px !important;"></i> 
                    </a>
                

<div class="btn btn-info   button-print firma" style="margin-left:8px; margin-right:8px;width:131px !important; display:none"> Firma <i class="m-icon-swapright m-icon-white"></i> </div>                   
                
<a style=" display:none; width:131px;"  target="_blank" href="<?php echo base_url()?>orden/printtestDuplicadoBueno/<?php echo $orden[0]->seo_idServiceOrder;?>" class="btn btn-success  button-print"> Imprimir Orden <i class="m-icon-swapright m-icon-white"></i> </a> 
                    
<a style="display:none; margin-top:8px; margin-left:8px;"  target="_blank" href="<?php echo base_url()?>orden/printFirma/<?php echo $orden[0]->seo_idServiceOrder;?>" class="btn btn-primary  button-print"> Imprimir Orden Con Aviso De privacidad <i class="m-icon-swapright m-icon-white"></i> </a> 

<?php if($orden[0]->enp_status==1){?>                
                <a href="<?php echo base_url()?>orden/enviarCaptura/<?php echo $orden[0]->seo_idServiceOrder;?>/<?php echo $orden[0]->ser_approximate_duration;?>-<?php echo $orden[0]->seo_technicianTeam;?>" class="btn btn-success button-submit"> Enviar a Captura <i class="m-icon-swapright m-icon-white"></i> </a>
<?php }?>                
                 </div>
              </div>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?> </div>
    </div>
  </div>
</div>

<!-- END CONTENIDO--> 

<style>
    .modal-dialog{
        width: 95% !important;
        margin-top: 20px;
        margin-bottom: 20px;
    }

    .modal-content{
      height: 500px !important;
    }

    .modal{
        padding-bottom: 0;
    }
    .aviso-cont{
        margin-top: 5px;
      float: left;
       width: 50%;
       text-align: center;;
    }
    .scroll {
    background-color: rgba(224, 224, 224, 0.78);
    
    width: 100%;
    height: 240px;
    margin-bottom: 10px;
    margin-right: 10px;
    margin-left: 10px;
    padding: 10px;
    border-radius: 5px;
    overflow: scroll;
    }
    
    .f-elem{
        float: left;
    }
    
     #btnCancelFirma, #save-firma{
            width: 140px;
            height: 60px;
            margin-top: 15px;
        }
    #save-firma{
        margin-right: 18px;
    }
    
    .modal-footer{
        margin-right: 25px !important;
    }
    @media only screen and (max-width: 1200px){ 
        .aviso-cont{
            width: 35%;
        }
    }
    
    @media only screen and (max-width: 900px){ 
        .modal-content{
            height: 700px !important;
        }
        
        .aviso-cont{
            width: 77%;
        }
        
        #btnCancelFirma, #save-firma{
            width: 13%;
            height: 230px;
            padding-top: 5%;
        }
        
        #save-firma{
            margin-right: 0;
        }
        
        #btnCancelFirma{
            margin-bottom: 15px;
            margin-top: -11px;
        }
        
        .modal-footer{
            margin-top: 0 !important;
        }

    }
    
    
}
    
input[type="checkbox"]
{
  width: 30px; /*Desired width*/
  height: 30px; /*Desired height*/
}
</style>

<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-wide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Firma</h4>
      </div>
      <div class="modal-body"> 
        <div class="f-elem">
          <canvas id="test_b" style="border-radius:5px; margin-top: 20px; border:1px solid #CCC; width:452px; height:250px"></canvas>
          <div id="btn-lf" class="btn btn-success" onclick="limpiarFirma()" style="margin-top:-240px;width:90px;height:250px; padding-top:120px; padding-bottom:95px;">
          
          Limpiar
          </div>
            </div>
          <div class="aviso-cont">
          <div class="aviso" style="font-size:1.5em;">
              Aviso de privacidad
          </div>
          <div class="scroll" style="font-size:1.2em;">
              AVISO DE PRIVACIDAD<br>
<br>
En Óptima Automotriz SA. De CV. (Honda Óptima), con domicilio matriz en Av. Padre Kino 4300, Tijuana, B.C., Ia información de nuestros clientes, clientes potenciales, visitantes, proveedores y empleados es tratada de forma estrictamente confidencial y es tan importante como su seguridad al conducir nuestros vehículos, por lo que hacemos un esfuerzo permanente para salvaguardarla, utilizándolos única y exclusivamente para Ias finalidades establecidas.<br>
<br>
Finalidad
Más que una política, en Honda Óptima tenemos Ia filosofía de mantener una relación estrecha y activa con nuestros clientes y clientes potenciales. <br>
<br>
<br>
Transmisión de datos personales <br>
<br>
Al proporcionar sus datos personales, tales como nombre, domicilio, correo
electrónico, teléfono y datos de contacto, así como los siguientes datos sensibles: comprobantes de ingresos y estados de cuenta bancarios, situación
financiera y patrimonial, consiente su manejo tanto dentro como fuera de
los Estados Unidos Mexicanos, y entiende que podrán ser tratados directa o
indirectamente por Honda Óptima, sus sociedades subsidiarias, afiliadas o relacionadas, sus distribuidores autorizados y/o sus terceros proveedores de
servicios con quienes tiene una relación jurídica, así como, en su caso, autoridades competentes, con las siguientes finalidades: <br>
<br>
Primarias para el caso de clientes:<br>
• Proceso de compra-venta de automotores, refacciones, accesorios y servicios.<br>
• Proveerle un bien y/o servicio.<br>
• Actualizar Ia base de datos.<br>
• La cobranza y procuración de pago, en caso que se le otorgue crédito de casa.<br>
<br>
Secundarias:<br>
• Realizar actividades de mercadeo y promoción en general.<br>
• Ofrecerle nuestros productos y servicios e información de nuestros socios de negocios.<br>
• Realizar análisis estadísticos y de mercado.<br>
• Mantener actualizados nuestros registros para poder responder a sus consultas, invitarle a eventos, hacer válida Ia garantía de su vehículo, informarle acerca de llamados a revisión de su vehículo, hacer de su conocimiento nuestras promociones y lanzamientos y mantener comunicación en general, así como dar seguimiento a nuestra relación comercial. <br>
<br>
Transferencia de los datos personales <br>
<br>
Para llevar a cabo Ias finalidades previamente establecidas, será necesario remitir sus datos a Honda de México, con la finalidad de reportar los procesos de compra-venta de automotores, refacciones, accesorios y servicios, sin requerir el consentimiento expreso. <br>
<br>
Así como transferirlos a: Honda de México con Ia finalidad de realizar análisis estadísticos y de mercado bajo consentimiento expreso del titular.<br>
<br>
Baja Grupo Digital, SA. De CV., con Ia finalidad de mantener actualizados nuestros registros para poder responder a sus consultas, invitarle a eventos, hacer válida Ia garantía de su vehículo, informarle acerca de llamados a revisión de su vehículo, hacer de su conocimiento nuestras promociones y lanzamientos y mantener comunicación en general, así como dar seguimiento a nuestra relación comercial, bajo consentimiento expreso del titular.<br>
<br>
Le informamos que para Ias transferencias indicadas, requerimos obtener su consentimiento, por lo que si usted no manifiesta su negativa dentro del término de 5 días hábiles, para dichas transferencias, entenderemos que Ias transferencias, han sido aceptadas. <br>
<br>
• No autorizo que mis datos personales sean transferidos con los siguientes terceros:<br>
<br>
Solicitud de acceso, rectificación, cancelación u oposición de datos personales y revocación del consentimiento. (Solicitud ARCO). 
Para prevenir el acceso no autorizado a sus datos personales, y con el fin de asegurar que Ia información sea utilizada para los fines establecidos en este Aviso de Privacidad, hemos establecido diversos procedimientos físicos y administrativos con Ia seguridad de que sus datos estarán debidamente protegidos.  
Todos sus datos personales son tratados de acuerdo con Ia legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento el derecho de acceder, rectificar, cancelar u oponerse al tratamiento que le damos a sus datos personales, así como revocar el consentimiento otorgado para el tratamiento de los mismos; derecho que podrá hacer valer a través de nuestro número de Atención a Clientes de Honda Óptima, en Tijuana (664) 900.9000 ext. 101, en Mexicali (686) 900.9000 ext. 120, o por medio de nuestros correos electrónicos: derechosarcoti@hondaoptima.com ó derecho-sarcomxli@hondaoptima.com. A través de estos canales usted podrá actualizar sus datos y especificar el medio por el cual desea recibir información, ya que en caso de no contar con esta especificación de su parte, Honda Óptima establecerá el canal que considere pertinente para enviarle información. Si usted desea dejar de recibir mensajes promocionales, puede solicitarlo a través de nuestro número de Atención a Clientes de Honda Óptima, en Tijuana (664) 900.9000 ext. 101, en Mexicali (686) 900.9000 ext. 120, o por medio de nuestros correos electrónicos: derechosarcoti@hondaoptima.com ó derecho-sarcomxli@hondaoptima.com.
Para el ejercicio de cualquiera de los derechos ARCO, usted deberá presentarla solicitud de Ia siguiente manera: 
Deberá enviar un escrito libre dirigido al Departamento de Atención al Cliente mediante correo electrónico o presentarse en Av. Padre Kino 4300, Zona Río, Tijuana, B.C. ó en Justo Sierra 1233, Los Pinos, Mexicali, B.C. en un horario de 8 am a 6pm de lunes a viernes, el cual debe contener nombre completo, correo electrónico, domicilio para notificarle Ia respuesta, anexando los documentos que acrediten su identidad, si envía a un representante legal, deberá presentar el original del poder notarial, Ia descripción clara y precisa de los datos personales respecto de los que busca ejercer algunos de los derechos ARCO y cualquier otro documento que facilite Ia localización de sus datos personales. En caso de enviar su solicitud vía correo electrónico, se le recuerda que deberá presentarse posteriormente en el domicilio indicado a fin de acreditar su identidad. 
Usted puede revocar el consentimiento que, en su caso nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal, o bien por subsistir Ia relación jurídica, requerimos seguir tratando sus datos personales. 
Asimismo, usted deberá considerar que para ciertos fines, Ia revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó o la conclusión de su relación con nosotros.  
Para limitar el uso o divulgación de sus datos personales, podrá acceder al portal de los Registros Público de PROFECO y CONDUSEF y realizar su registro, a fin de que quede debidamente limitado el uso y divulgación de datos personales. 
Le informamos que en nuestra página de internet utilizamos cookies, web beacons y otras tecnologías, a través de Ias cuales es posible monitorear su comportamiento como usuario de internet brindarle un mejor servicio y experiencia de usuario al navegar en nuestra página, así como ofrecerle nuevos productos  servicios basados en sus preferencias.<br>
<br>
En Ias siguientes ligas podrá conocer el mecanismo para deshabilitar cookies, web beacon y otras tecnologías:<br>
<br>
Explorer.<br>
http://windows.microsoft.com/es-419/windows-vista/block-or-allow-cookies<br>
<br>
Link de soporte de Safari. <br>
http://www.apple.com/es/support/mac-apps/safari/<br>
<br>
Safari iPhone: <br>
http://support.apple.com/kb/HT1677?viewlocale=es_ES&locale=es_ES<br>
<br>
Chrome:<br>
https://support.google.com/chrome/answer/95647?hI=es<br>
<br>
<br>
Modificaciones al Aviso de Privacidad<br>
<br>
Este aviso de privacidad podrá ser modificado de tiempo en tiempo por el Departamento de Servicio al Cliente de Honda Óptima. Dichas modificaciones serán oportunamente informadas a través de nuestra página de Internet www.hondaoptima.com, o cualquier otro medio de comunicación oral, impreso o electrónico que Honda Óptima determine para tal efecto.<br>
<br>
<br>
Fecha de actualización: 04 de julio de 2013<br>
<br>
Otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el presente aviso de privacidad. <br>
<br>

          </div>
          <div style="font-size:1.5em;color:red;">
              Acepto que lei el aviso de privacidad  
              <input type="checkbox" id="chk-aviso" />
              <br>
              Desea recibir publicidad? 
              <input type="checkbox" id="chk-publicidad" />

          </div>
          </div>
      </div>
      <div class="modal-footer">
        <button id="btnCancelFirma" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-4x" style="font-size: 2em !important;"></i></button>
          <button disabled="disabled" id="save-firma"  class="btn btn-primary" onclick="saveFirma()" value="Guardar"><i class="fa fa-check" style="font-size:2em !important;"></i></button>
      </div>
    </div>
    </form>
    <!-- /.modal-content --> 
  </div>
</div>



<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) --> 
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <p class="size">{%=o.formatFileSize(file.size)%}</p>
            {% if (!o.files.error) { %}
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
            {% } %}
        </td>
        <td>
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                <button class="btn btn-info start">
                    <i class="fa fa-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-danger cancel">
                    <i class="fa fa fa-ban"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script> 
<!-- The template to display files available for download --> 
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="fa fa-trash-o"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="fa fa fa-ban"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<?php  $this->load->view('globales/footer'); ?>
<?php $this->load->view('orden/js/script');?>
 <script src="<?php echo base_url();?>signature/jquery.jqscribble.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>signature/jqscribble.extrabrushes.js" type="text/javascript"></script>
		<script type="text/javascript">
		function save()
		{
			$("#test").data("jqScribble").save(function(imageData)
			{
				$('.savechasis').html('Guardando Imagen ...');
					$.post('<?php echo base_url();?>signature/image_save.php?ido=<?php echo $orden[0]->seo_idServiceOrder;?>', {imagedata: imageData}, function(response)
					{
                        console.log('probando ruta');
						$('.savechasis').html('Imagen Guardada !');
					});	
				
			});
		}
            
            
        function continueFirma(){
            var pass = 'error';
			$("#test_b").data("jqScribble").save(function(imageData)
			{
                var publicity = false;
             if($('#chk-publicidad').is(':checked')){
                 publicity = true;                
             }   
				//$('.savechasis').html('Guardando Imagen ...');
					$.post('<?php echo base_url();?>firmas/image_save_firma.php?ido=<?php echo $orden[0]->seo_idServiceOrder;?>', {imagedata: imageData}, function(response)
					{
						//$('.savechasis').html('Imagen Guardada !');
                        if(response == 'success'){
                            console.log('pass is success');
                            $.ajax({
                                  url:"http://hsas.gpoptima.net/cotizacionesc/enviar_documento/<?php echo $orden[0]->seo_idServiceOrder;?>/"+publicity+"",
                                  success:function(result){
                                    if(result == 'success'){
                                        alert('----EMAIL EXITOSO----');
                                        $('#wide').modal('hide');
                                    }else{
                                      alert('****ERROR AL ENVIAR EMAIL****');
                                      $('#wide').modal('hide');
                                    }
                                      
                                  }
                                     }).fail(function(data){

                                          alert('****ERROR AL ENVIAR EMAIL****');
                                          $('#wide').modal('hide');
                            });	
                        }
                        
					});	
                
                
				
			});
            
        }
            
            function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};
		
		function saveFirma()
		{
            var email = $('#sec_email').val();
            
            if(email != "0" && email != "" && email != '.'){
                if( isValidEmailAddress( email ) ) { 
                    continueFirma();
                }else{
                    alert('El email es incorrecto');
                }
            }else{
                var txt;
                var r = confirm("El cliente no cuenta con EMAIL");
                if (r == true) {
                    continueFirma();
                } else {
                    
                }
            }
            
		}
		
             $(document).on('click','#chk-aviso', function(){
      if($(this).is(':checked')){
          $('#save-firma').prop('disabled',false);
      }
      else{
        $('#save-firma').prop('disabled',true);
      }
    });
		
		
		function addImage()
		{
			var img = prompt("Enter the URL of the image.");
			alert(img);
			if(img !== '')$("#test").data("jqScribble").update({backgroundImage: img});
		}
		function limpiar(){
			$("#test").jqScribble();
			$("#test").data("jqScribble").update({width:"150",backgroundImage:"../../documents/chasis.png",backgroundImageX:'25',backgroundImageY:'10'});
			}
			
		function limpiarFirma(){
			$("#test_b").jqScribble();
			$("#test_b").data("jqScribble").update({width:"452",backgroundImageX:'232',backgroundImageY:'10'});
			}
		$(document).ready(function()
		{
			
			$("#test_b").jqScribble();
			$("#test_b").data("jqScribble").update({width:"452",backgroundImageX:'232',backgroundImageY:'10'});
			$("#test").jqScribble();
			$("#test").data("jqScribble").update({width:"150",backgroundImage:"../../documents/chasis.png",backgroundImageX:'25',backgroundImageY:'10'});
			
			
		});
		</script> 


<script>
var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    username: {
                        minlength: 5,
                        required: true
                    },
                    password: {
                        minlength: 5,
                        required: true
                    },
                    rpassword: {
                        minlength: 5,
                        required: true,
                        equalTo: "#submit_form_password"
                    },
                    //profile
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    //payment
                    card_name: {
                        required: true
                    },
                    card_number: {
                        minlength: 16,
                        maxlength: 16,
                        required: true
                    },
                    card_cvc: {
                        digits: true,
                        required: true,
                        minlength: 3,
                        maxlength: 4
                    },
                    card_expiry_date: {
                        required: true
                    },
                    'payment[]': {
                        required: true,
                        minlength: 1
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]').each(function(){
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }
			
			var verfotos= function() {
			var result='';	

		 
		 
			
			}
		

            var handleTitle = function(tab, navigation, index) {
				
                var total = navigation.find('li').length;
                var current = index + 1;
				
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Paso ' + (index + 1) + ' de ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }
				

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
					$('#form_wizard_1').find('.button-print').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
					$('#form_wizard_1').find('.button-print').hide();
                }
                App.scrollTo($('.page-title'));
				
				
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
					
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }
					if(index==1){
						
						 $.ajax({
	  url:"<?php echo base_url();?>ajax/ordendeservicio/serviciosc.php?id=<?php echo $orden[0]->seo_idServiceOrder;?>&modelo="+<?php echo $orden[0]->app_vehicleModel;?>+"&camp_exists="+<?php echo $exists ?>+"&vin=<?php echo $vin; ?>",
	  success:function(result){
           $('.loadservicio').html(''+result+''); 

            $('#camp').clone().appendTo($('#campsin'));
            $('#camp').fadeIn();
                
             }

          
         });	
						}
						if(index==2){
						

var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$('#seo_consecuencias').val();

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_consecuencias&valor="+valor+"",
	  success:function(result){
             
                              }
         });


						}
					if(index==3){
						$.ajax({
	  url:"<?php echo base_url();?>orden/checkFotos?id=<?php echo $orden[0]->seo_idServiceOrder;?>",
	  success:function(resp){
             if(resp==0){
					alert('Para avanzar es necesario tener las Primeras 5 Fotos del Vehículo');	
					return false;
					}
					else{
						  handleTitle(tab, navigation, index);
						}
                              }
         });
					}else{
						 handleTitle(tab, navigation, index);
						}

                  
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();
					
					

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    
					
             var total = navigation.find('li').length;
                    var current = index + 1;
						  var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
						
                   
					
					
					
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                
            }).hide();
        }

    };

}();


</script> 
<script >


  
</script>

<script>
jQuery(document).ready(function() {
	
	 $('[data-toggle="tooltip"]').tooltip(); 
 $('#camp').clone().appendTo($('#campsin'));


$(document).on('click','.chk-campaign', function(e){
    
  if(!$(this).is(':checked'))
  {
      e.preventDefault();
      $('#textarea-desc').val('');
     $('#mod-descripcion').fadeIn();
      $('#btn-solicitud').attr('chk-element',$(this).attr('id'));
      $('#btn-solicitud').attr('orden',$(this).attr('order'));
      $('#btn-solicitud').attr('garantia',$(this).attr('garantia'));
  }else{
      $(this).prop('checked', true);
      $.ajax({
                                  url:"http://hsas.gpoptima.net//ajax/pizarron/update_garantia_status.php?status=1&orden="+$(this).attr('order')+"&campania="+$(this).attr('garantia'),
                                  success:function(result){ 
                                    
                                  } 
                                });
  }
   

});
    var interval = null;
    $(document).on('click','#btn-solicitud',function(){
        $('#mod-descripcion').fadeOut();
         var value = $('#inp_session');
        var txt = $('#textarea-desc').val();
      $('#div-result-notify').empty();
    $('#myModal').fadeIn();
       $.ajax({
	  url:"http://hsas.gpoptima.net//ajax/ordendeservicio/create_notification_order.php?asesor="+value.val()+"&orden="+$(this).attr('orden')+"&txt="+txt+"&garantia="+$(this).attr('garantia'),
	  success:function(result){
          console.log(result);
            },
           error:function(result){
               console.log(result);
           }
         });
      interval = setInterval(getResultNotify,3000,$(this).attr('orden'), $(this).attr('chk-element'));
    });
    
function getResultNotify(orden,chk){
    $.ajax({
	  url:"http://hsas.gpoptima.net//ajax/ordendeservicio/get_result_notify.php?orden="+orden,
	  success:function(result){
                if(result == 'success'){
                    $('#div-result-notify').empty();     
                    $('#div-result-notify').append('<i class="fa fa-thumbs-o-up"></i> Solicitud aceptada');
                    clearInterval(interval);
                    $('#myModal').delay(2500).fadeOut();
                    $('#'+chk).prop('checked', false);
                     
                }else if(result == 'decline'){
                    $('#div-result-notify').empty();
                    $('#div-result-notify').append('<i class="fa fa-thumbs-o-down"></i> Solicitud rechazada');
                    clearInterval(interval);
                    $('#myModal').delay(2500).fadeOut();
                    $('#'+chk).prop('checked', true);
                    
                } 
            }
         });
}


// first we need a slider to work with
var months = ["E","1/8", "1/4", "3/8", "1/2", "5/8", "3/4", "F"];
<?php
if($orden[0]->seo_combustible=='E' || $orden[0]->seo_combustible==''){$val='0';}
if($orden[0]->seo_combustible=='1/8'){$val='1';}
if($orden[0]->seo_combustible=='1/4'){$val='2';}
if($orden[0]->seo_combustible=='3/8'){$val='3';}
if($orden[0]->seo_combustible=='1/2'){$val='4';}
if($orden[0]->seo_combustible=='5/8'){$val='5';}
if($orden[0]->seo_combustible=='3/4'){$val='6';}
if($orden[0]->seo_combustible=='F'){$val='7';}
?>
$("#slider2").slider({ min: 0, max: 7, value: <?php echo $val;?>,orientation: "horizontal"  });
$("#slider2").slider("pips" , { rest: "label", labels: months })
$("#slider2").on("slidechange", function(e,ui) {

  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id=<?php echo $orden[0]->seo_idServiceOrder;?>&campo=seo_combustible&valor="+months[ui.value]+"",
	  success:function(result){
             
                              }
         });
});


$('.firma').live('click',function(){

	$('#wide').modal('show'); 
	});

$('.saveorden').on('focusout',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});


$('.savetec').on('click',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});


 $('.bootstrap-timepicker').on('click',function(){
var campo=$('input[name=horapro]').attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;

var hr=$('input[name=hour]').val();
var mi=$('input[name=minute]').val();
var me=$('input[name=meridian]').val();
var valor=""+hr+":"+mi+" "+me+""

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});

 $('#savehora').on('click',function(){
var campo=$('input[name=horapro]').attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var hr=$('input[name=hour]').val();
var mi=$('input[name=minute]').val();
var me=$('input[name=meridian]').val();
var valor=""+hr+":"+mi+" "+me+""

 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });

});

$('.torre').on('change',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){  } });
});

$('.vehiculo').on('change',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_IDapp;?>;
var valor=$(this).val();
$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){  
	  
	  $.ajax({
	  url:"<?php echo base_url();?>ajax/ordendeservicio/updateVehiculo.php?id="+<?php echo $orden[0]->seo_idServiceOrder;?>+"&valor="+valor+"",
	  success:function(result){  
	  
	  } });
	  
	  
	  } });
});


$('.pago').on('change',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){  } });
});



$('.checkConfirmar').on('click',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
var mcCbxCheck = $(this);
if(mcCbxCheck.is(':checked')) {
 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
             
                              }
         });
}
else{
  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor=1",
	  success:function(result){
             
                              }
         });
	}
});


$('.checkConfirmarOtros').on('click',function(){
var campo=$(this).attr('id');
var id=<?php echo $orden[0]->seo_idServiceOrder;?>;
var valor=$(this).val();
var persona=$('input[name=clienteNombre]').val();
var tel=$('input[name=telcliente]').val();
var mcCbxCheck = $(this);
if(mcCbxCheck.is(':checked')) {
 $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor="+valor+"",
	  success:function(result){
		  if(id=='seo_num_poliza'){
			  $('#idseo_num_poliza').show();
			  }
		
		/*insertar nombre y telfono de la persona que recoge al seleccionar la casilla*/
		if(campo=='seo_recoge_check'){	
			$.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_persona_recoge&valor="+persona+"",
	  success:function(result){
		  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_telefono_recoge&valor="+tel+"",
	  success:function(result){}});
		  
		  }
			});
		$('input[name=persona]').val(persona);
		$('input[name=tel]').val(tel);		
			
			}	  
		  /*fin*/
                              }
         });
}
else{
  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo="+campo+"&valor=0",
	  success:function(result){
		    if(id=='seo_num_poliza'){
			  $('#seo_num_poliza').hide();
			  }
			  
			  if(campo=='seo_recoge_check'){
				  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_persona_recoge&valor=",
	  success:function(result){
		  $.ajax({
	  url:"<?php echo base_url();?>orden/saveAjaxOrden?id="+id+"&campo=seo_telefono_recoge&valor=",
	  success:function(result){}});
		  
		  }
			});
		$('input[name=persona]').val('');
		$('input[name=tel]').val('');	
				  }
             
                              }
         });
	}
});





});
</script> 
<script>



	function remove()
{


$(":checkbox:checked").each(
function() {
var ch= $(this).val();


$('#'+ch).remove();
var t=0;

}
); 



} 
$(document).ready(function(){ 


$('.removelinea').live('click',function(){
	
	remove();	
	
});

$('.elimi').on('click',function(){
	
	dele();	
	
});


$('#chall').live('click',function(){
var chk=$('#chall').attr('checked');
if(chk==true){
// Seleccionar un checkbox
$('input:checkbox').attr('checked', true);
}
else{
// Deseleccionar un checkbox
$('input:checkbox').attr('checked', false);
}
});





		
}); 
</script> 
