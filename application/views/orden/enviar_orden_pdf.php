<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Orden de Servicio</title>
    <style>

    @page {

        margin: 5mm;
    }

    html, body{
        width:100%; padding: 0px; margin: 0px;
        font-size: 12px;
    }
    body{
        font-family: Sans-Serif;
    }

    #table-cliente td,#table-info td, #table-order td{
        font-size: 8px;
        margin: 0;
        padding: 0;
    }

    #table-importe tr td{
        /*height:5px;*/
    }

    #table-info td{
        width:50%;
    }

    .branches-text{
        font-size: 10px;
    }
    .text-7{
        font-size: 7px;
    }
    .text-8{
        font-size: 8px;
    }
    .text-9{
        font-size: 9px;
    }
    .text-10{
        font-size: 10px;
    }
    .text-11{
        font-size: 11px;
    }
    .text-12{
        font-size: 12px;
    }
    .text-13{
        font-size: 13px;
    }
    .text-14{
        font-size: 14px;
    }
    .text-15{
        font-size: 15px;
    }
    .text-16{
        font-size: 16px;
    }
    .branch-header{
        font-size: 12px;
    }
    .nombre-empresa{
        font-size: 10px;
        margin: 0;
        padding: 0;
    }
    .reception-info table td,.tecs table td
    {
        padding-left:5px;
    }
    .chasis,.logo{
        background-repeat:no-repeat;
    }
    .radius,.services{
        border-spacing:0;
        overflow:hidden;
    }
    .page{
        /*width:1024px;height:1000px;font-family:Arial;*/
        width: 100%; /*height: 1000px;*/
    }
    .head{
        width:100%;
        /*height:180px;*/
    }
    .contact{
        /*width:40%;
        height:100%;
        float:left;*/
    }
    .logo{
        width:100%;height:70px;background-size:80%;
    }
    .address{
        width:790px;height:100%;border:none;border-collapse:collapse;
    }
    .address th{
        font-size:16px;
    }
    .address td{
        font-size:12px;
    }
    .address .hours{
        font-size:16px;text-align:center;
    }
    .folio{
        /*width:60%;*/
        height:100%;
        float:left;
    }
    .info,.os-type{
        width:100%;font-size:16px;
    }
    .os-type{
        height:25px;
        padding-top:20px;
        font-weight:700;
        text-align:right
    }
    .info{
        height:120px
    }
    .message{
        width:60%;height:60px;float:left
    }
    .message p{
        width:98%;margin:10px;
        auto auto;
        padding:5px;
        border:0 solid;
        border-radius:15px;
        background-color:#e2002b;
        color:#fff;
        font-size:11px;
        text-align:justify;
        line-height:180%;
    }
    .customer-info table,.reception-info table,.vehicle-info table{
        line-height:110%;
        margin:auto;
    }
    .service-order{
        width:40%;height:100%;float:left;
    }
    .service-order table{
        width:80%;
        margin:10px auto auto 50px;
        font-size:16px;
        border:2px solid #666;
        border-spacing:0;
        text-align:center;
    }
    .order-date .gray{
        background-color:#666;
        color:#fff;
        font-size:12px;
        font-weight:900;
    }
    h .order-date .white{
        height:27px;
        font-size:8.4px;
        font-weight:900;
        vertical-align:text-top;
        text-align:left;
    }
    .customer-info,.reception-info{
        float:left;
        /*font-size:16px;*/
    }
    .order-date td{
        border:1px solid #666;
    }
    .customer-info{
        width:50%;
    }
    .customer-info table{
        width:100%;
    }
    .customer-info table th{
        border-bottom:1px solid #000;
    }
    .vehicle-info{
        width:50%;
    }
    .vehicle-info table{
        width:100%;
    }
    .vehicle-info table th{
        border-bottom:1px solid #333;
    }
    .reception-info{
        width:50%;
        display: block;
    }
    .reception-info table{
        width:100%;
        height:120px;
    }
    .comments,.confirmation{
        /*height:135px;*/
        float:left;
        font-size:16px;
    }
    .confirmation{
        width:48%;
        display: block;
    }
    .details{
        width:70%;
    }
    .comments{
        width:100%;
        text-align:justify;
        vertical-align:top;
    }
    .comments .text{
        width:97%;
        height:100%;
        margin:auto;
    }
    .items,.itemsb{
        height:65px;
        float:left
    }
    .text .title{
        font-size:12px;
    }
    .itemsb{
        width:80%;
        font-size:14px
    }
    .items,.services,.services-head,.tecs{
        font-size:16px;
    }
    .itemsb table{
        width:100%;margin:auto;border:none;}
    .items{
        width:100%;margin-top:10px;
    }
    .items table{
        width:95%;margin:auto;border:none;
    }
    .tecs{
        width:82%;margin-top:10px;float:left;}
    .chasis,.col-12,.row{
        width:100%;
    }
    .tecs table{
        width:100%;margin:auto;
    }
    .tecs table td{
        height:25px;vertical-align:text-top;border-bottom:1px solid #000;border-right:1px solid #000;
    }
    .tecs table td:last-child{
        border-right:none;
    }
    .chasis{
        height:205px;
        float:left;
        background-image:url();
        background-size:100%
    }
    .services-head{
        background-color:#e2002b;
        color:#fff;
        margin-bottom:10px;
    }
    .services-head th{
        height:22px;
    }
    .services{
        border:1px solid #e2002b;
        border-radius:10px;
        text-align:center;
    }
    .first-cell{
        border-right:1px solid #e2002b;
    }
    .total{
        height:15px;
        margin:5px;
        vertical-align:center-top;
        border:1px solid #000;
        border-radius:5px;
    }
    .total-text{
        text-align:right;
    }
    .row{
        margin-bottom:10px;
        float:left;
    }
    .row:first-child{
        margin-bottom:0;
    }
    .floated{
        float:left;
    }
    .col-10{
        width:83.33%;
    }
    .col-9{
        width:75%;
    }
    .col-8{
        width:66.66%;
    }
    .col-7{
        width:58.33%;
    }
    .col-6{
        width:50%;
    }
    .col-5{
        width:41.66%;
    }
    .col-4{
        width:33.33%;
    }
    .col-3{
        width:25%;
    }
    .col-2{
        width:20.5%;
    }
    .col-1{
        width:8.33%;
    }
    .col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11{
        display: block !important;
        float: left !important;
    }
    .first-row{
        border:1px solid #000;
    }
    .title{
        color:#e2002b;
    }
    .right{
        text-align:center;
        padding-right:3px;
        border-right:1px solid #000;
    }
    .left{
        text-align:left;
        padding-left:3px;
    }
    .radius{
        border:1px solid #333;
        border-radius:10px !important;
    }
    .dark-cell{
        text-align:center;
        font-weight:bold;
        background-color:black;
        color:white;
    }





    #table-order {
        color: #333; /* Lighten up font color */
        font-family: Helvetica, Arial, sans-serif; /* Nicer font */
        width: 640px;
        border-collapse:
        collapse; border-spacing: 0;
    }

    #table-order td, #table-order th { border: 1px solid #CCC; height: 20px; } /* Make cells a bit taller */

    #table-order th {
        background: #F3F3F3; /* Light grey background */
        font-weight: bold; /* Make sure they're bold */
    }

    #table-order td {
        background: #FFFFFF; /* Lighter grey background */
        text-align: center; /* Center our text */
    }
    
    </style>
</head>

<body>
    <div class="page">
        <div class="row head">
            <div>
                <div class="col-6">
                    <img src="<?php echo base_url(); ?>documents/hlogo.png" width="250px">
                </div>
                <div class="col-6">
                    <table class="order-date radius" style="width:300px;" id="table-order">
                        <tr>
                            <td class="white"><span class="text-9">ORDEN DE SERVICIO</span></td>
                            <td class="white"><span class="text-9"><?php if($orden[0]->seo_serviceOrder==''){echo $orden[0]->seo_tower;}else{ echo $orden[0]->seo_serviceOrder;}?></span></td>
                        </tr>
                        <tr class="white">
                            <td><span class="text-9">FECHA</span></td>
                            <td><span class="text-9"><?php list($fec,$hor)=explode(" ",$orden[0]->seo_date); echo $fec;?></span></td>
                        </tr>
                        <tr class="white">
                            <td><span class="text-9">LOCALIDAD</span></td>
                            <td><span class="text-9"><?= $_SESSION['sfworkshop'];?></span></td>
                        </tr>
                    </table>
                </div>

                <!-- <div style=" padding-left:45px; margin-bottom:10px; margin-top:-5px; font-size:16px; "> -->
                <div style="">
                    <div style="font-weight:bold;"><span class="nombre-empresa">OPTIMA AUTOMOTRIZ S.A. DE C.V.</span></div>
                    <div><span class="nombre-empresa">RFC: OAU990325G88</span></div>
                </div>

                <table cellspacing="0"  class="address" style="width:827px;">
                    <tr>
                        <th width="25%" class="title right"><span class="branch-header">TIJUANA B.C.:</span></th>
                        <th width="25%" class="title right"><span class="branch-header">MEXICALI B.C.:</span></th>
                        <th width="22%" class="title right"><span class="branch-header">ENSENADA B.C.:</span></th>
                        <th width="30%" class="title"><span class="branch-header">HORARIO DE ATENCI&Oacute;N</span><th>
                    </tr>
                    <tr style="font-size: 100px;">
                        <td class="right"><span class="branches-text">Av. Padre Kino 4300 Zona Río,<br> CP. 22320,<br />
                        Tel. (664) 900-9010</span></td>
                        <td class="right"><span class="branches-text">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                        Tel. (686) 900-9010</span></td>
                        <td class="right" style="text-align:center"><span class="branches-text">Av. Balboa 146 Esq.<br />
                            López Mateos Fracc. Granados. CP.  228402,<br />
                            Tel. (646) 900-9010</span></td>
                        <td style="text-align:center"><span class="branches-text">Lunes a Viernes de 8:00 A.M. a 6:00 P.M.<br />
                            Sábado de 8:00 A.M. a 1:30 P.M.</span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <!-- <div class="folio">
            <div class="info">
                <div class="message">
                    <p style="background-color:white"></p>
                </div>
                <tr class="gray">
                    <td>ORDEN DE SERVICIO</td>
                </tr>
                <tr class="white" style="text-align:center; height:15px;">
                    <td><?php //if($orden[0]->seo_serviceOrder==''){echo $orden[0]->seo_tower;}else{ echo $orden[0]->seo_serviceOrder;}?></td>
                </tr>
                <tr class="gray">
                    <td>FECHA</td>
                </tr>
                <tr class="white" style="text-align:center;height:15px;" >
                    <td><?php //list($fec,$hor)=explode(" ",$orden[0]->seo_date); echo $fec;?></td>
                </tr>
                <tr class="gray">
                    <td>LOCALIDAD</td>
                </tr>
                <tr class="white" style="text-align:center; height:15px;" >
                    <td><?php //echo $_SESSION['sfworkshop'];?></td>
                </tr>
                </table>
            </div>
        </div>  -->
    </div>

    <div class="row" style="margin: 0 0 12px 0; padding:0;">
        <div class="customer-info" style="margin:0;">
            <table class="radius" id="table-cliente">
                <tr class="first-row">
                    <th colspan="3" class="title"><span class="text-9">DATOS DEL CLIENTE (CONSUMIDOR)</th>
                </tr>
                <tr>
                    <td colspan="3" class="text-8"><b>Nombre:</b>
                        <?= substr(ucwords(strtolower($orden[0]->sec_nombre)),0,50);?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="text-8"><b>Dirección: </b><?php echo substr(ucwords(strtolower($orden[0]->sec_calle)),0,23);?><?php echo ucwords(strtolower($orden[0]->sec_num_ext));?></td>
                </tr>
                <tr>
                    <td class="text-8"><b>Ciudad:</b> <?php echo ucwords(strtolower($orden[0]->sec_ciudad));?></td>
                    <td class="text-8" ><b>Estado:</b> <?php echo ucwords(strtolower($orden[0]->sec_estado));?></td>
                    <td class="text-8"><b>CP:</b><?php echo ucwords(strtolower($orden[0]->sec_cp));?></td>
                </tr>
                <tr>
                    <td class="col-5" class="text-8"><b>R.F.C.:</b><?php echo strtoupper($orden[0]->sec_rfc);?>
                    </td>
                    <td class="col-7" class="text-8"><b>Email:</b> <?php $eema=$orden[0]->sec_email.';xxx'; list($email,$emailb)=explode(';',$eema);
                        echo substr(ucwords(strtolower($email)),0,25);?>
                    </td>
                    <td><b>Teléfono:</b><?php echo ucwords(strtolower($orden[0]->sec_tel));?></td>
                </tr>
                <tr>
                    <td  colspan="3" ><b>Persona autorizada para recoger la unidad:</b> &nbsp;</td>
                </tr>
                <tr>
                    <td><b>Nombre:</b> <?= substr(ucwords(strtolower($orden[0]->seo_persona_recoge)),0,50);?></td>
                    <td><b>Teléfono:</b><?= ucwords(strtolower($orden[0]->seo_telefono_recoge));?></td>
                </tr>
            </table>
        </div>
        <div class="vehicle-info" style="margin:0;">
            <table class="radius" id="table-info"><!-- aqui table info -->
                <tr>
                    <th colspan="2" class="title"><span class="text-9">DATOS DEL VEHÍCULO</span></th><!--aqui-->
                </tr>
                <tr height="36px;">
                    <td><b>Marca:</b><?php echo ucwords(strtolower($orden[0]->sev_marca));?></td>
                    <td><b>Sub Marca:</b> <?php echo ucwords(strtolower($orden[0]->sev_sub_marca));?></td>
                </tr>
                <tr style="height:36px;">
                    <td><b>Año-Modelo:</b><?php echo ucwords(strtolower($orden[0]->sev_modelo));?></td>
                    <td><b>Placas:</b><?php echo strtoupper($orden[0]->sev_placas);?></td>
                </tr>
                <tr style="height:36px;">
                    <td><b>Fecha de Venta:</b><?php echo $orden[0]->sec_venta?></td>
                    <td><b>Último Servicio:</b><?php echo $orden[0]->sec_ultimo_servicio?></td>
                </tr>
                <tr style="height:36px;">
                    <td class="col-6"><b>Color:</b>  <?php echo ucwords(strtolower($orden[0]->sev_color));?></td>
                    <td class="col-6"><b>Seguro:</b> &nbsp;</td>
                </tr>
                <tr style="height:36px;">
                    <td class="col-6"><b>No. de  VIN:</b> <?php echo strtoupper ($orden[0]->sev_vin);?></td>
                    <td class="col-6"><b>Km. de Entrada:</b><?php echo ucwords(strtolower($orden[0]->sev_km_recepcion));?></td>
                </tr>
                <tr>
                    <td class="col-6"><b>No. de  Motor:</b> &nbsp;</td>
                    <td class="col-6"><b>Capacidad:</b> <?php echo ucwords(strtolower($orden[0]->sev_capacidad));?></td>
                </tr>
            </table>
        </div>
    </div>




    <div class="row" style="margin-bottom: 2px; position:relative;" >
        <div class="reception-info">
            <table  class="radius" width="100%">
                <tr>
                    <td colspan="2" class="text-8"><b>Asesor:</b> <?= $orden[0]->sus_name.' '.$orden[0]->sus_lastName ?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-8"><b>Técnico:</b> <?= $tecnico[0]->sus_name.' '.$tecnico[0]->sus_lastName?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-8"><b>Fecha y hora de recepción:</b><?= ucwords(strtolower($orden[0]->seo_timeReceived)); ?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-8"><b>Fecha de entrega del vehículo:</b> <?= date('Y-m-d'); ?></td>
                </tr>
                <tr >
                    <td style="padding-top:4px;" class="text-8 dark-cell">NUMERO DE TORRE: </td>
                    <td width="20%" style="" class="text-10 dark-cell"><?= $orden[0]->seo_tower;?></td>
                </tr>
            </table>
        </div>
        <div class="confirmation" style="padding-left:10px; font-size:7.8px; font-weight:bold; color:black;">
            Se entregan las partes o refacciones reemplazadas al consumidor SI &nbsp;&nbsp;( <?php if($orden[0]->seo_se_entregan==1){echo '&radic;';}?> ) &nbsp;&nbsp;&nbsp;&nbsp; NO &nbsp;&nbsp;( <?php if($orden[0]->seo_se_entregan==0){echo '&radic;';}?> )
            <br>
            <b>NOTA:</b> Las partes y/o refacciones no se entregar&aacute;n al consumidor cuando:<br>
            a) Sean cambiadas por garantía<br>
            b) Se trate de residuos peligrosos deacuerdo con las disposiciones legales aplicables.<br>
            Servicio  en el domicilio del consumidor &nbsp;( <?php if($orden[0]->seo_ser_domi==1){echo '&radic;';}?> ) &nbsp; SI  &nbsp;( <?php if($orden[0]->seo_ser_domi==0){echo '&radic;';}?> ) &nbsp; NO<br>
            Poliza de seguros para cubrir al consumidor los daños o extravios de <br> bienes: SI ( <?php if($orden[0]->seo_poliza==1){echo '&radic;';}?> ) Numero:<?php if($orden[0]->seo_poliza==''){echo '______________';}else{echo $orden[0]->seo_poliza;}?>.  ( <?php if($orden[0]->seo_poliza==0){echo '&radic;';}?> )NO
        </div>
    </div>




    <div class="row">
        <div class="floated">
            <div style="margin-bottom: 73px;">
                <div class="" style="height:28px; padding-left:15px;">
                    <span class="text-9" style="color:#e2002b;">COMENTARIOS:</span>
                    <br>
                    <?= $orden[0]->seo_comments;?>
                </div>
            </div>
            <div class="itemsb"  style="margin-top:-70px;">
                <div class="text-9" style="font-weight:bold; width:100%; text-align:center; color:rgb(226,0,43);">INVENTARIO DEL VEHICULO</div>
                <table>
                    <tr style="height:10px">
                        <td width="15px;" class="text-9"><?php if($orden[0]->seo_tapetes==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style="font-weight:bold" class="text-9">TAPETES</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_tapones==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">TAPONES</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_herramienta==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">HERRAMIENTA</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_reflejantes==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">REFLEJANTES</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_ecualizador==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">ECUALIZADOR</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_tapon==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">TAPÓN GAS</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_ventanas==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">VENTANAS</td>
                    </tr>
                    <tr style="height:10px">
                        <td class="text-9"><?php if($orden[0]->seo_espejo==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">ESPEJO</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_radio==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">RADIO</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_llantarefaccion==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">LLANTA REF.</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_extinguidor==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">EXTINGUIDOR</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_gato==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">GATO</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_carnet==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">CARNET</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_vestidura==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">VESTIDURA</td>
                    </tr>
                    <tr style="height:10px">
                        <td class="text-9"><?php if($orden[0]->seo_antena==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">ANTENA</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_encendedor==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">ENCENDEDOR</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_limpiadores==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">LIMPIADORES</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_cables==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">CABLES P/C</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_estereo==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">ESTEREO</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_carroceria==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">CARROCERÍA</td>
                        <td width="15px;" style="text-align:right" class="text-9"><?php if($orden[0]->seo_parabrisas==2){echo'&radic;';}else{echo'X';}?></td>
                        <td style=" font-weight:bold" class="text-9">PARABRISAS</td>
                    </tr>
                    <tr>
                        <td height="15px;"></td>
                        <td ></td>
                        <td ></td>
                        <td></td>
                        <td></td> <td colspan="4" style="font-weight:bold; font-family:14px; background-color:black; color:white; text-align:center" class="text-8" > CANTIDAD DE COMBUSTIBLE: </td>
                        <td class="text-8" style="background-color:black; color:white; text-align:center; font-weight:bold"><?php echo ' '.$orden[0]->seo_combustible;?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="tecs" style="padding-right:0px; margin-top:0px;">
                <table style="text-align:center; margin-top:0px; font-weight:bold;">
                    <tr>
                        <td valign="middle" style="padding-top:3px;" class="text-9">Hora de Recepción</td>
                        <td class="text-9">Hora de Inicio de Trabajo</td>
                        <td class="text-9">Hora de Trabajo Terminado</td>
                    </tr>
                    <tr>
                        <td class="text-9"><?php list($fech,$hora)=explode(" ",$orden[0]->seo_timeReceived); echo ucwords(strtolower($hora));?>
                        </td>
                        <td class="text-9"></td>
                        <td class="text-9"><?php echo ucwords(strtolower($orden[0]->seo_promisedTime));?></td>
                    </tr>
                </table>
            </div>

            <div class="floated" style="margin-top:-73px; padding-right:10px;">
                <img src="<?php echo base_url(); ?>signature/<?php if(empty($orden[0]->sei_cha_uno))echo 'chasis.png'; else echo $orden[0]->sei_cha_uno;?>" height="280px;" width="143px;">
                <table style="margin-top:-160px;" width="80px" height="130px;">
                    <tr>
                        <td width="20px;" align="center"><?php if($orden[0]->sei_cha_uno=='ok'){echo ' ';}?></td>
                        <td width="95px;"></td>
                        <td><?php if($orden[0]->sei_cha_dos=='ok'){echo '';}?></td>
                    </tr>
                    <tr height="43px;">
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="20px;" align="center"><?php if($orden[0]->sei_cha_tres=='ok'){echo '';}?></td>
                        <td></td>
                        <td><?php if($orden[0]->sei_cha_cuatro=='ok'){echo '';}?></td>
                    </tr>
                </table>
            </div>

            <div class="comments" style="margin-top:30px; height: 85px; width:1022px; padding-left:5px; margin-left:-10px;">
                <div class="text " style=" width:1022px; height:90px; margin-top:-20px; ">
                    <table width="1024px" style="text-align:center; " cellspacing="0"  border="0" id="table-importe"><!-- Aqui -->
                        <tr>
                            <td width="29%" height="12px" style=" border-bottom:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;" class="text-8">OPERACIONES A EFECTUAR</td>
                            <td width="29%" height="12px" style=" border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;" class="text-8">PARTES Y/O REFACCIONES</td>
                            <td width="29%" height="12px" style="  border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;" class="text-8">PRECIOS UNITARIOS</td>
                            <td width="10%" height="12px" style=" border-bottom:1px solid #000; border-top:1px solid #000;"></td>
                        </tr>
                        <?php
                            setlocale(LC_MONETARY, 'en_US');
                            $num = count($servicios);
                            $numsv = count($serviciossv);
                            //$limite = 11 - $num - $numsv;
                            $limite = 4 - $num - $numsv;
                            $monto = 0;
                            $tiposer = '';
                            foreach($servicios as $ser)
                            {
                                if($ser->ser_tipo=='pro')//True
                                    $comercial=$ser->ser_comercial / 1.16;//1410/1.16
                                else
                                    $comercial=$ser->ser_comercial;//1410
                                $tiposer = $ser->ssr_venta;//venta
                                $monto += $comercial;//1215.51
                                echo '
                                    <tr>
                                    <td height="12px" width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;" class="text-9">'.$ser->set_name.'</td>
                                    <td height="12px" width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
                                    <td height="12px" width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
                                    <td height="12px" width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
                                    <table width="100%"><tr>
                                    <td height="12px" width="20%">$</td>
                                    <td height="12px" width="79%" align="right" class="text-9">
                                    '.number_format($comercial, 2, '.', '').'
                                    </td>
                                    <td height="12px" width="1%"></td>
                                    </tr></table>
                                    </td>
                                    </tr>';
                            }

                            foreach($serviciossv as $sersv)
                            {
                                echo '
                                    <tr>
                                    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;" class="text-8">'.$sersv->sev_nombre.'</td>
                                    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
                                    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
                                    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
                                    <table width="100%"><tr>
                                    <td width="20%">$</td>
                                    <td width="79%" align="right" class="text-8">
                                    '.number_format(0, 2, '.', '').'
                                    </td>
                                    <td width="1%"></td>
                                    </tr></table>
                                    </td>
                                    </tr>';
                            }
                            for($x=0; $x<$limite; $x++)
                            {
                        ?>
                        <tr>
                            <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
                            <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
                            <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
                            <td width="10%" style="border-bottom:1px solid #999; text-align:left; ">$</td>
                        </tr>
                        <?php
                            }
                        ?>
                        <tr>
                            <td width="30%" style="border-right:1px solid #000;">&nbsp;</td>
                            <td width="30%" style="border-right:1px solid #000;"></td>
                            <td width="30%" style="border-right:1px solid #000;"><!--MONTO DE LA OPERACI&Oacute;N--></td>
                            <td width="10%" style=" text-align:left;border-top:1px solid #000;">
                            <?php
                                echo '<table width="100%"><tr>
                                    <td width="20%">$</td>
                                    <td width="79%" align="right">
                                    '.number_format($monto, 2, '.', '').'
                                    </td>
                                    <td width="1%"></td>
                                    </tr></table>';
                            ?>
                            </td>
                        </tr>
                        <!-- <tr>
                            <td width="30%" style="border-right:1px solid #000;"></td>
                            <td width="30%" style="border-right:1px solid #000;"></td>
                            <td width="30%" style="border-right:1px solid #000;"></td>
                            <td width="10%" style=" text-align:left;  ">&nbsp;</td>
                        </tr> -->
                    </table>
                </div>
            </div>

            <div class="comments text-8" style="margin-top:0px; height:75px; width:1020px; margin-left:-10px;">
                <div class="text" style="height:0px;">
                    <table width="100%">
                        <tr>
                            <td width="15%" class="text-8">FORMA DE PAGO</td>
                            <td width="50%" class="text-8">EFECTIVO ( <?php if($orden[0]->seo_pago==1){echo '&radic;';}?>) CHEQUE (<?php if($orden[0]->seo_pago==2){echo '&radic;';}?>) TARJETA DE CREDITO (<?php if($orden[0]->seo_pago==3){echo '&radic;';}?>) OTRO (<?php if($orden[0]->seo_pago==4){echo '&radic;';}?> )</td>
                            <td width="25" style="text-8 text-align:right">SUBTOTAL</td>
                            <td width="10%" class="text-8">$ <?php echo number_format($monto, 2, '.', '');?></td>
                        </tr>
                        <tr>
                            <td width="15%"></td>
                            <td width="50%"></td>
                            <td width="25" style="text-align:right" class="text-8">IVA</td>
                            <td width="10%" class="text-8">
                            <?php if($tiposer=='interna'){$iva=0;}
                            else{$iva=0.16;}?>
                            $ <?php echo number_format($monto * $iva, 2, '.', '');?></td>
                        </tr>
                        <tr>
                            <td width="15%" class="text-8"></td>
                            <td width="50%" class="text-8"></td>
                            <td width="25"  class="text-8"style="text-align:right"> TOTAL</td>
                            <td width="10%" class="text-8">
                            <?php if($tiposer=='interna'){$miva=0;}
                            else{$miva=1.16;}?>
                            $ <?php echo number_format($monto * $miva, 2, '.', '');?></td>
                        </tr>
                        <!-- <tr>
                            <td width="15%" class="text-8">&nbsp;</td>
                            <td width="50%" class="text-8"></td>
                            <td width="25" class="text-8" style="text-align:right"></td>
                            <td width="10%" class="text-8"></td>
                        </tr> -->
                        <tr>
                            <td colspan="4" style="font-weight:bold" width="100%" class="text-8">
                            *EL TOTAL NO INCLUYE SERVICIOS ADICIONALES. EN CASO DE SER NECESARIOS, SE SOLICITARA AUTORIZACION AL CLIENTE VIA TELEFONICA
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div style="margin-bottom: 0px;">
                <div class="text" style="height:15px;  padding-left:15px;"> <span class="text-9">POSIBLES CONSECUENCIAS:</span> <br>
                    <!-- <div style="font-size:16px; ">
                    </div> -->
                </div>
            </div>
        </div>
    </div>


    <div class="row" style="margin-top:0px;">
        <b style="margin-left:5px; margin-top:-26px;" class="text-9">Imagenes del Vehículo ( Condiciones de Recepción )</b><br>
        <table class="radius services-head" style="width:100%; text-align:center">
            <tr>
                <th width="20%" class="first-cell col-2" class="text-8">Costado Derecho</th>
                <th width="20%" class="text-8">Costado Izquierdo</th>
                <th width="20%" class="text-8">Parte Frontal</th>
                <th width="20%" class="text-8">Parte Trasera</th>
                <th width="20%" class="text-8">Tablero</th>
            </tr>
        </table>
        <table class="services" align="center" width="100%" style="text-align:center">
            <tr>
                <td>
                    <?php
                        $ider=$orden[0]->sei_inf_der;
                        if($ider=='noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url;?>/<?= $ider;?>" target="_blank">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider;?>" width="100px;">
                    </a>
                </td>
                <td>
                    <?php
                        $ider = $orden[0]->sei_inf_izq;
                        if($ider == 'noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target="_blank">
                        <img style="border:1px solid #CCC" src="<?= $url;?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
                <td>
                    <?php
                        $ider = $orden[0]->sei_pos_der;
                        if($ider == 'noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target="_blank">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
                <td>
                <?php
                    $ider = $orden[0]->sei_pos_izq;
                    if($ider == 'noimagen.png')
                        $url = base_url().'assets/img';
                    else
                        $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target="_blank">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
                <td>
                    <?php
                        $ider=$orden[0]->sei_tablero;
                        if($ider=='noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target = "_blank">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
            </tr>
        </table>

        <b class="text-9">Imagenes Extras</b><br>
        <table class="radius services-head" style="width:100%; text-align:center">
            <tr>
                <th width="20%" class="first-cell col-2 text-8">Extra 1</th>
                <th width="20%" class="text-8">Extra 2</th>
                <th width="20%" class="text-8">Extra 3</th>
                <th width="40%" class="text-8"></th>
            </tr>
        </table>
        <table class="services" align="center" width="100%" style="text-align:center">
            <tr>
                <td style="width:20%">
                    <?php
                        $ider=$orden[0]->sei_extra_uno;
                        if($ider == 'noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target="_blank">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
                <td style="width:20%">
                    <?php
                        $ider = $orden[0]->sei_extra_dos;
                        if($ider == 'noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target="">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
                <td style="width:20%">
                    <?php
                        $ider = $orden[0]->sei_extra_tres;
                        if($ider == 'noimagen.png')
                            $url = base_url().'assets/img';
                        else
                            $url = base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta;
                    ?>
                    <a href="<?= $url; ?>/<?= $ider; ?>" target="_blank">
                        <img style="border:1px solid #CCC" src="<?= $url; ?>/<?= $ider; ?>" width="100px;">
                    </a>
                </td>
            <td width="40%">
                <img alt="testing" src="<?= base_url()?>ajax/imprimir/barcode.php?text=<?= $orden[0]->sev_vin;?>&print=true" />
            </td>
            </tr>
        </table>
    </div>
</div>

</body>
</html>
<!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="crossorigin="anonymous"></script> -->

<script type="text/javascript">
<!--
//window.print();
//-->
</script>
