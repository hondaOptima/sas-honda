<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
header('Content-Type: text/html; charset=UTF-8'); 
?>
<title>Orden de Servicio</title>
<style>
.page {
	width:1024px;
	height:1100px;
	font-family:Arial;
	
}
.head {
	width:100%;
	height:180px;
}
.contact {
	width:30%;
	height:100%;
	float:left;
}
.logo {
	width:100%;
	height:70px;
	
	background-size: 80%;
	background-repeat:no-repeat;
}
.address {
	width:1024px;
	height:100%;
	border:none;
	border-collapse:collapse;
	margin-top:30px;
}
.address th {
	font-size:16px;
}
.address td {
	font-size:14px;
}
.address .hours {
	font-size:16px;
	text-align:center;
}
.folio {
	width:70%;
	height:100%;
	float:left;
}
.os-type {
	width:100%;
	height:25px;
	padding-top:20px;
	font-size:16px;
	font-weight:bold;
	text-align:right;
}
.info {
	width:100%;
	height:120px;
	font-size:16px;
}
.message {
	width:55%;
	height:60px;
	float:left;
}
.message p {
	width:98%;
	margin:auto;
	margin-top:10px;
	padding:5px;
	border:0px solid rgb(51,51,51);
	border-radius:15px;
	text-align:center;
	
	font-size:11px;
	text-align:justify;
	line-height:180%;
}
.service-order {
	width:45%;
	height:100%;
	float:left;
}
.service-order table {
	width:100%;
	margin:auto;
	margin-top:10px;
	font-size:14px;
	border:2px solid black;
	border-spacing:0;
	text-align:center;
}
.order-date .gray {
	background-color:black;
	color:rgb(255,255,255);
	font-size:11px;
	font-weight:900;
}h
.order-date .white {
	height:27px;
	font-size:8.4px;
	font-weight:900;
	vertical-align:text-top;
	text-align:left;
}
.order-date td {

}
.customer-info {
	width:45%;
	float:left;
	font-size:16px;
}
.customer-info table {
	width:100%;
	margin:auto;
	line-height:150%;
}
.customer-info table th {
	border-bottom:1px solid rgb(0,0,0);
}
.customer-info table td {
	padding-left:5px;
}
.vehicle-info {
	width:55%;
	float:left;
	font-size:16px;
}
.vehicle-info table {
	width:100%;
	margin:auto;
	line-height:150%;
}
.vehicle-info table th {
	border-bottom:1px solid rgb(51,51,51);
}
.vehicle-info table td {
	padding-left:5px;
}
.reception-info {
	width:45%;
	float:left;
	font-size:16px;
}
.reception-info table {
	width:100%;
	height:120px;
	margin:auto;
	line-height:150%;
}
.reception-info table td {
	padding-left:5px;
}
.confirmation {
	width:54%;
	height:145px;
	margin:auto;
	float:left;
	font-size:16px;
	
	line-height:150%;
}
.confirmation p {
	
}
.details {
	width:70%;
}
.comments {
	width:100%;
	height:135px;
	float:left;
	font-size:16px;
	text-align:justify;
	vertical-align:top;
}
.comments .text {
	width:1020px;
	height:100%;
	margin-left:10px;
}
.text .title {
	font-size:12px;
	font-weight:bold;
	margin-left:5px;
}
.itemsb {
	width:830px;
	padding-left:5px;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:14px;
}
.itemsb table {
	width:100%;
	margin:auto;
	border:none;
}
.items {
	width:100%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.items table {
	width:95%;
	margin:auto;
	border:none;
}
.tecs {
	width:800px;
	margin-top:10px;
	
	float:left;
	font-size:16px;
}
.tecs table {
	width:99%;
	margin:auto;
}
.tecs table td {
	height:25px;
	padding-left:5px;
	vertical-align:text-top;
	border-bottom:1px solid rgb(0,0,0);
	border-right:1px solid rgb(0,0,0);
}
.tecs table td:last-child {
	border-right:none;
}
.chasis {
	width:100%;
	height:205px;
	float:left;
	background-image:url('');
	background-size: 100%;
	background-repeat:no-repeat;
}
.services-head {
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:16px;
	margin-bottom:10px;
}
.services-head th {
	height:22px;
}
.services {
	font-size:16px;
	border:1px solid rgb(226,0,43);
	border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}
.first-cell {
	border-right:1px solid rgb(226,0,43);
}
.total {
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(0,0,0);
	border-radius:5px;
}
.total-text {
	text-align:right;
}
.row {
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child {
	margin-bottom:0px;
}
.floated {
	float:left;
}
.col-12 {
	width:100%;
}
.col-10 {
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8 {
	width:66.66%;
}
.col-7 {
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5 {
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:20.5%;
}
.col-1 {
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right {
	text-align:center;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left {
	text-align:left;
	padding-left:3px;
}
.radius {
	border: 1px solid rgb(51,51,51);
	border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>
</head>

<body>
<div class="page">
  <div class="row head">
    <div class="contact">
      <img src="<?php echo base_url(); ?>documents/hlogo.png" width="350px">
      <div>
       
        <table cellspacing="0"  class="address">
          <tr>
            <th width="25%" class="title right">TIJUANA:</th>
            <th width="25%" class="title right">MEXICALI:</th>
            <th width="22%" class="title ">ENSENADA:</th>
            <td  width="28%" rowspan="2"  style="border-left:4px double #000; color:red; font-weight:bold; font-size:16px;" class="hours title">HORARIO DE ATENCI&Oacute;N<br>
            <div style="color:black; margin-top:3px; font-size:12px; font-weight:lighter">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
              Sábado de 8:00 a.m. a 1:30 p.m.<br>&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td class="right">Av. Padre Kino 4300 Zona Río, CP. 22320,<br />
              Tel. (664) 900-9010</td>
            <td class="right">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
              Tel. (686) 900-9010</td> 
              <td style="text-align:center">Av. Balboa 146 Esq.
              López Mateos Fracc. Granados<br />
              Tel. (646) 900-9010</td> 
          </tr>
          
        </table>
       
        
      </div>
    </div>
    <div class="folio">
   
      <div class="info">
        <div class="message">
          <p><div  style=" padding-left:95px; margin-bottom:10px; margin-top:-5px; font-size:16px; "><div style="font-weight:bold;" >OPTIMA AUTOMOTRIZ S.A. DE C.V.</div>
       <div style=" padding-left:55px;">RFC: OAU990325G88</div></div></p>
        </div>
        <div class="service-order" style="margin-top:5px;">
          <table class="order-date radius">
           <tr >
              <td width="33%" class="gray">LOCALIDAD</td>
               <td width="33%"  class="gray" style="text-align:center;height:15px;">FECHA</td>
               <td width="33%" class="gray">ORDEN DE SERVICIO</td>
               
               </tr><tr>
              <td class="white" style="text-align:center; height:15px; border:1px solid #000"><?php echo $_SESSION['sfworkshop'];?></td>
            
             
              <td style="border:1px solid #000"><?php list($fec,$hor)=explode(" ",$orden[0]->seo_date); echo $fec;?></td>
           
              
            
        
              <td class="white" style="text-align:center; height:15px;border:1px solid #000"><?php if($orden[0]->seo_serviceOrder==''){echo $orden[0]->seo_tower;}else{ echo $orden[0]->seo_serviceOrder;}?></td>
            </tr>
            
            
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="customer-info" style="margin-top:25px;">
      <table class="radius">
        <tr class="first-row">
          <th colspan="2" class="title">DATOS DEL CLIENTE ( CONSUMIDOR )</th>
        </tr>
        <tr>
          <td colspan="2" style="width:95px;" ><b>Nombre:</b> <?php echo substr(ucwords(strtolower($orden[0]->sec_nombre)),0,50);?></td>
          
        </tr>
        <tr  >
          <td style="font-size:12px;"  ><b>R.F.C.:</b><?php echo strtoupper($orden[0]->sec_rfc);?>
          </td>
          <td  style="font-size:12px;">
          <b>E.mail:</b><?php $eema=$orden[0]->sec_email.';xxx'; list($email,$emailb)=explode(';',$eema);
		   echo substr(ucwords(strtolower($email)),0,25);?>
          </td>
         
        </tr>
        <tr>
          <td style="font-weight:bold; background-color:black; color:white" colspan="2" >Domicilio: </td>
          
        </tr>
        <tr>
          <td style="font-size:12px;"  ><b>Calle:</b><?php echo substr(ucwords(strtolower($orden[0]->sec_calle)),0,23);?> </td>
          <td style="font-size:12px;" >
          <b>N&uacute;mero Exterior o Interior:</b> <?php echo ucwords(strtolower($orden[0]->sec_num_ext));?>
          
          </td>
        </tr>
         <tr>
          <td colspan="2" style="font-size:12px;"  ><b>Colonia:</b><?php echo ucwords(strtolower($orden[0]->sec_colonia));?> </td>
         
        </tr>
        <tr>
          <td  style="font-size:12px;"  ><b>Delegaci&oacute;n o Municipio:</b><?php echo ucwords(strtolower($orden[0]->sec_ciudad));?></td>
          
           <td style="font-size:12px;">
          <b>C&oacute;digo Postal:</b> <?php echo ucwords(strtolower($orden[0]->sec_cp));?>
          
          </td>
         
        </tr>
        
        <tr style="font-size:12px;">
          <td colspan=""  ><b>Estado:</b><?php echo ucwords(strtolower($orden[0]->sec_estado));?></td>
          <td colspan=""  ><b>Tel&eacute;fonos:</b><?php echo ucwords(strtolower($orden[0]->sec_tel));?></td>
        </tr>
       
      </table>
    </div>
    <div class="vehicle-info" style="margin-top:25px;">
      <table class="radius">
        <tr>
          <th colspan="4" class="title">CARACTERISTICAS GENERALES DEL VEHÍCULO</th>
        </tr>
        <tr>
          <td  class="col-4"><b>Marca:</b><?php echo ucwords(strtolower($orden[0]->sev_marca));?></td>
        
          <td  class="col-4" ><b>Capacidad:</b><?php echo ucwords(strtolower($orden[0]->sev_capacidad));?></td>
         
        </tr>
        <tr style="height:31px;">
          <td  ><b>Submarca: </b><?php echo ucwords(strtolower($orden[0]->sev_sub_marca));?> </td>
          <td  ><b>Modelo:</b> <?php echo ucwords(strtolower($orden[0]->sev_modelo));?></td>
        </tr>
        <tr style="height:31px;">
          <td  ><b>Tipo o versión: </b><?php echo strtoupper($orden[0]->sev_version);?> </td>
          <td  ><b>N&uacute;mero de placas:</b> <?php echo strtoupper($orden[0]->sev_placas);?></td>
        </tr>
        <tr style="height:31px;">
          <td  ><b>Color:</b> <?php echo ucwords(strtolower($orden[0]->sev_color));?></td>
          <td  ><b>Año:</b> <?php echo ucwords(strtolower($orden[0]->sev_modelo));?></td>
        </tr>
        
        <tr style="height:31px;">
          <td colspan="4"  style="font-weight:bold" class="col-5"><div style="float:left">Número Identificación Vehicular:</div> <div style="font-weight:lighter; float:left"><?php echo strtoupper ($orden[0]->sev_vin);?></div></td>
          
        </tr>
       <tr style="height:31px;">
          <td colspan="4"  style="font-weight:bold" class="col-5">
          <div style="float:left">Número de kilómetros recorridos:</div> <div style="font-weight:lighter; float:left">&nbsp; <?php echo ucwords(strtolower($orden[0]->sev_km_recepcion));?></div></td>
         
        </tr>
     </table>



    </div>
  </div>
  <div class="row" style="margin-top:20px;">
    <div class="reception-info">
      <table  class="radius">
        <tr>
          <td height="34" ><b>Asesor:</b> <?php echo $orden[0]->sus_name.' '.$orden[0]->sus_lastName?></td>
          
        </tr>
        <tr>
          <td height="34" ><div style="float:left"><b>Pir&aacute;mide:</b></div><div style="float:left; font-weight:bold; font-size:24px;">&nbsp;&nbsp; <?php echo $orden[0]->seo_tower;?></div> </td>
         
        </tr>
        <tr>
          <td height="34" style="font-size:15px;" ><b>Fecha y hora de recepción del vehículo:</b> <?php echo ucwords(strtolower($orden[0]->seo_timeReceived));?></td>
          
        </tr>
        
         <tr>
          <td height="34" style="font-size:15px;"><b>Fecha y hora de entrega del vehículo:</b> <?php echo ucwords(strtolower($orden[0]->seo_promisedTime));?></td>
          
        </tr>
        
      </table>
    </div>
    <div class="confirmation radius" style="padding-left:5px; font-size:12px;">Se entregan las partes o refacciones reemplazadas al consumidor<br>
      SI &nbsp;&nbsp;( <?php if($orden[0]->seo_se_entregan==1){echo '&radic;';}?> ) &nbsp;&nbsp;&nbsp;&nbsp; NO &nbsp;&nbsp;( <?php if($orden[0]->seo_se_entregan==0){echo '&radic;';}?> )<br>
      <b>NOTA:</b> Las partes y/o refacciones no se entregar&aacute;n al consumidor cuando:<br>
      a) Sean cambiadas por garantía<br>
      b) Se trate de residuos peligrosos deacuerdo con las disposiciones legales aplicables.<br>
      
      Servicio  en el domicilio del consumidor &nbsp;( <?php if($orden[0]->seo_ser_domi==1){echo '&radic;';}?> ) &nbsp; SI  &nbsp;( <?php if($orden[0]->seo_ser_domi==0){echo '&radic;';}?> ) &nbsp; NO<br>
      
      Poliza de seguros para cubrir al consumidor los daños o extravios de <br> bienes: SI ( <?php if($orden[0]->seo_poliza==1){echo '&radic;';}?> ) Numero:<?php if($orden[0]->seo_poliza==''){echo '______________';}else{echo $orden[0]->seo_poliza;}?>.  ( <?php if($orden[0]->seo_poliza==0){echo '&radic;';}?> )NO 
      
    </div>
  </div>
  <div class="row">
    <div class="col-10 floated">
      
      
       
       <div  class="comments" style="margin-top:18px; width:1020px; margin-left:-10px;">
        <div class="text radius" style=" height:310px; "> 
          <table width="1024px" style="text-align:center; " cellspacing="0"  border="0">
  <tr>
    <td width="30%" style=" border-bottom:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">OPERACIONES A EFECTUAR</td>
    <td width="30%" style=" border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PARTES Y/O REFACCIONES</td>
    <td width="30%" style="  border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PRECIOS UNITARIOS</td>
    <td width="10%" style=" border-bottom:1px solid #000; border-top:1px solid #000;"></td>
  </tr>
  
  <?php 
  setlocale(LC_MONETARY, 'en_US');
  $num=count($servicios);
  $numsv=count($serviciossv);
  $limite=11 - $num - $numsv;
  $monto=0;
  $tiposer='';
  foreach($servicios as $ser){
	  
	  
	 if($ser->ser_tipo=='pro'){
	  $comercial=$ser->ser_comercial / 1.16;
	 }else{
	  $comercial=$ser->ser_comercial;
	 }
	 $tiposer=$ser->ssr_venta;
	 
	  $monto+=$comercial;
	  
	  
	echo ' 
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$ser->set_name.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($comercial, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';  
	  }
	  
	  
	  foreach($serviciossv as $sersv){
	  
	  
	
	  
	  
	echo ' 
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$sersv->sev_nombre.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format(0, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';  
	  }
	  
	  
  for($x=0; $x<$limite; $x++) {?>
  <tr>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style="border-bottom:1px solid #999; text-align:left; ">$</td>
  </tr>
 <?php } ?>
 
 
 
  <tr>
    <td width="30%" style="border-right:1px solid #000;">&nbsp;</td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;">MONTO DE LA OPERACI&Oacute;N</td>
    <td width="10%" style=" text-align:left;border-top:1px solid #000;"><?php echo '<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($monto, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>'; ?></td>
  </tr>
 <tr>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="10%" style=" text-align:left;  ">&nbsp;</td>
  </tr>
</table>    
          </div>
      </div>
      
       <div  class="comments" style="margin-top:200px; width:1020px; margin-left:-10px;">
        <div class="text radius" style=" height:50px; ">
        <table>
       <tr>
    <td width="30%" style=" text-decoration:underline;">POSIBLES CONSECUENCIAS:</td>
    <td width="30%" style=""></td>
    <td width="30%" style=""></td>
    <td width="10%" style=" text-align:left"></td>
  </tr>
  <tr>
    <td width="30%" style=""><?php echo $orden[0]->seo_consecuencias;?></td>
    <td width="30%" style=""></td>
    <td width="30%" style=""></td>
    <td width="10%" style=" text-align:left"></td>
  </tr>
      </table>
      
      </div></div>
      
      
      <div class="comments" style="margin-top:-55px; width:1020px; margin-left:-10px;">
          <div class="text radius" style=" height:210px; ">
          <table width="100%" style="text-align:center; font-size:14px;"  cellspacing="0"  border="0">
  <tr>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">FORMA DE PAGO</td>
    <td width="10%" style="border-right:1px solid #000; text-align:left; border-top:1px solid #000; text-decoration:underline;"></td>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">SERVICIOS ADICIONALES:</td>
    <td width="10%" style=" border-top:1px solid #000;"></td>
  </tr>

 <tr>
    <td  style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">Monto de la operaci&oacute;n:
    
    </td>
    <td style="border-right:1px solid #000; text-align:left; "><?php echo '<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($monto, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>'; ?></td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" "></td>
  </tr>
  <tr>
    <td style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Otros cargos:
    </td>
    <td  style="border-right:1px solid #000;text-align:left;">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Total refacciones y servicios adicionales</td>
    <td  style=" text-align:left">$</td>
  </tr>
<tr>
    <td style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">
    Servicios Adicionales:
    
    </td>
    <td  style="border-right:1px solid #000; text-align:left;">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" border-top:1px solid #000;"></td>
  </tr>
  <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Parcial:
    
    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; "></td>
    <td  ></td>
  </tr>
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Impuesto al Valor Agregado:
    
    </td>
    <td  style="border-right:1px solid #000;text-align:left; "><?php 
	if($tiposer=='interna'){$iva=0;}
	else{$iva=0.16;}
	
	echo '<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($monto * $iva, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>'; ?></td>
    <td  style="border-right:1px solid #000; text-align:left; ">Fecha y monto de anticipo:</td>
    <td  style="  text-align:left">$</td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">

    
    </td>
    <td  style="border-right:1px solid #000;text-align:left; border-bottom:1px solid #000; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">El resto del monto total de la operaci&oacute;n. Se </td>
    <td  style=" border-top:1px solid #000;"></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Monto Total:
    
    </td>
    <td style="border-right:1px solid #000;text-align:left;  "><?php 
	if($tiposer=='interna'){$miva=0;}
	else{$miva=1.16;}
	echo '<table width="100%"><tr>
	<td width="1%">$</td>
	<td width="98%" align="right">
	'.number_format($monto * $miva, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>'; ?></td>
    <td  style="border-right:1px solid #000; text-align:left; ">liquidara en la fecha programada para la entrega</td>
    <td  style=" "></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
(Incluye mano de obra)
    
    </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; ">del veh&iacute;culo</td>
    <td  style=" "></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
     </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>
  
  <tr>
    <td style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Efectivo ( <?php if($orden[0]->seo_pago==1){echo '&radic;';}?> ) Cheque ( <?php if($orden[0]->seo_pago==2){echo '&radic;';}?> ) tarjeta de cr&eacute;dito ( <?php if($orden[0]->seo_pago==3){echo '&radic;';}?> ) Otro ( <?php if($orden[0]->seo_pago==4){echo '&radic;';}?> ) 
     </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  ></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">&nbsp;
 
     </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  ></td>
  </tr>
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">

     </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  ></td>
  </tr>
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">&nbsp;

     </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  ></td>
  </tr>
 
</table>    
          </div>
      </div>
      
      
      <br><br><br>
     
    </div>
    
  </div>
  <div class="row"> 
 
   
  </div>
</div>


<div style="margin-top:400px;" class="page">
  <div class="row head">
    <div class="contact">
      <img src="<?php echo base_url(); ?>documents/hlogo.png" width="350px">
      <div>
       
        <table cellspacing="0"  class="address">
          <tr>
            <th width="25%" class="title right">TIJUANA:</th>
            <th width="25%" class="title right">MEXICALI:</th>
            <th width="22%" class="title ">ENSENADA:</th>
            <td  width="28%" rowspan="2"  style="border-left:4px double #000; color:red; font-weight:bold; font-size:16px;" class="hours title">HORARIO DE ATENCI&Oacute;N<br>
            <div style="color:black; margin-top:3px; font-size:12px; font-weight:lighter">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
              Sábado de 8:00 a.m. a 1:30 p.m.<br>&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td class="right">Av. Padre Kino 4300 Zona Río, CP. 22320,<br />
              Tel. (664) 900-9010</td>
            <td class="right">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
              Tel. (686) 900-9010</td> 
              <td style="text-align:center">Av. Balboa 146 esq.
              López Mateos Fracc. Granados<br />
              Tel. (646) 900-9010</td> 
          </tr>
          
        </table>
       
        
      </div>
    </div>
    <div class="folio">
   
      <div class="info">
        <div class="message">
          <p><div  style=" padding-left:155px; margin-bottom:10px; margin-top:-5px; font-size:34px; "><div style="font-weight:bold;" >ANEXO</div>
       </div></p>
        </div>
        <div class="service-order" style="margin-top:5px;">
          <table class="order-date radius">
           <tr >
              <td width="33%" class="gray">LOCALIDAD</td>
               <td width="33%"  class="gray" style="text-align:center;height:15px;">FECHA</td>
               <td width="33%" class="gray">ORDEN DE SERVICIO</td>
               
               </tr><tr>
              <td class="white" style="text-align:center; height:15px; border:1px solid #000"><?php echo $_SESSION['sfworkshop'];?></td>
            
             
              <td style="border:1px solid #000"><?php list($fec,$hor)=explode(" ",$orden[0]->seo_date); echo $fec;?></td>
           
              
            
        
              <td class="white" style="text-align:center; height:15px;border:1px solid #000"><?php if($orden[0]->seo_serviceOrder==''){echo $orden[0]->seo_tower;}else{ echo $orden[0]->seo_serviceOrder;}?></td>
            </tr>
            
            
          </table>
        </div>
      </div>
    </div>
  </div>
 
  <div class="row" style="margin-top:30px;"> 
  <div class=" floated">
      <div style="padding-left:-10px;" class="comments">
        <div class="text radius" style=" padding-left:-5px; height:200px; margin-left:-05px;"> <span class="title">COMENTARIOS:</span> <br>
          <div style="font-size:16px; ">
          <?php echo $orden[0]->seo_comments;?>
           </div>
           </div>
      </div>
      
       <div class="itemsb"  style="margin-top:90px;">
        <table >
          <tr style="height:25px">
            <td width="15px;" ><?php if($orden[0]->seo_tapetes==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">TAPETES</td>
            <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_tapones==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">TAPONES</td>
            <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_herramienta==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">HERRAMIENTA</td>
             <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_reflejantes==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">REFLEJANTES</td>
             <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_ecualizador==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ECUALIZADOR</td>
              <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_tapon==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">TAPÓN GAS</td>
                <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_ventanas==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">VENTANAS</td>
          </tr>
          <tr style="height:25px">
            <td><?php if($orden[0]->seo_espejo==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ESPEJO</td>
               <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_radio==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">RADIO</td>
            <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_llantarefaccion==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">LLANTA REF.</td>
               <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_extinguidor==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">EXTINGUIDOR</td>
          <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_gato==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">GATO</td>
              <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_carnet==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">CARNET</td>
                 <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_vestidura==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">VESTIDURA</td>
          </tr>
          <tr style="height:25px">
               <td><?php if($orden[0]->seo_antena==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ANTENA</td>
                 <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_encendedor==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ENCENDEDOR</td>
           <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_limpiadores==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">LIMPIADORES</td>
                    <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_cables==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">CABLES P/C</td>
                   <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_estereo==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ESTEREO</td>
                      <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_carroceria==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">CARROCERÍA</td>
                     <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_parabrisas==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">PARABRISAS</td>
          </tr>
          <tr >
            <td height="18px;"></td>
            <td ></td>
            <td ></td>
            <td></td>
            <td></td> <td colspan="4" style="font-weight:bold; font-family:14px; background-color:black; color:white; text-align:center" > CANTIDAD DE COMBUSTIBLE: </td>
            <td style="background-color:black; color:white; text-align:center; font-weight:bold"><?php echo ' '.$orden[0]->seo_combustible;?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
       
      <div class="tecs" style=" margin-top:40px;">
        <table style="text-align:center; margin-top:30px; font-weight:bold; font-size:16px;" class="radius" >
          <tr>
            <td valign="middle" style="padding-top:3px;">Hora de Recepción</td>
            <td>Hora de Inicio de Trabajo</td>
            <td>Hora de Trabajo Terminado</td>
          </tr>
          <tr>
            <td>
              <?php list($fectab,$hortab)=explode(" ",$orden[0]->seo_timeReceived); echo $hortab;?></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
   
   <div class=" floated" style="margin-top:-65px; margin-left:10px;">
     <img src="<?php echo base_url(); ?>signature/<?php echo $orden[0]->sei_cha_uno;?>" width="180px;">
   <table style="margin-top:-175px;" width="130px" height="150px;">
<tr>
<td width="20px;" align="center"><?php if($orden[0]->sei_cha_uno=='ok'){echo ' X';}?></td>
<td width="95px;"></td>
<td><?php if($orden[0]->sei_cha_dos=='ok'){echo 'X';}?></td>
</tr>
<tr height="43px;">
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td width="20px;" align="center"><?php if($orden[0]->sei_cha_tres=='ok'){echo 'X';}?></td>
<td></td>
<td><?php if($orden[0]->sei_cha_cuatro=='ok'){echo 'X';}?></td>
</tr>

</table>
     
    </div>
  </div>
  
  <div class="row"> 
  <br>
    <b style=" font-size:16px; margin-left:15px; ">Imagenes del Vehículo</b> <br>
    <br>
    <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Costado Derecho</th>
        <th width="20%" >Costado Izquierdo</th>
        <th width="20%" >Parte Frontal</th>
        <th width="20%" >Parte Trasera</th>
        <th width="20%" >Tablero</th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td><?php 
		$ider=$orden[0]->sei_inf_der; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_inf_izq; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_pos_der; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_pos_izq; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_tablero; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
      </tr>
    </table>
   <br>
    <b style=" font-size:16px;">Imagenes Extras</b> <br>
    <br>
    <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Extra 1</th>
        <th width="20%" >Extra 2</th>
        <th width="20%" >Extra 3</th>
        <th width="40%" ></th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td style="width:20%"><?php 
		$ider=$orden[0]->sei_extra_uno; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td style="width:20%"><?php 
		$ider=$orden[0]->sei_extra_dos; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td style="width:20%"><?php 
		$ider=$orden[0]->sei_extra_tres; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td width="40%"></td>
      </tr>
    </table>
  </div>
</div>
</div>
</div>



</body>
</html>
<script type="text/javascript">

//window.print();

</script>