
<script src="<?php echo base_url();?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"  
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-validation/dist/additional-methods.min.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" 
type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/fuelux/js/spinner.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/moment.min.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.input-ip-address-control-1.0.min.js" 
type="text/javascript" ></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript" ></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-markdown/lib/markdown.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" 
type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" 
type="text/javascript"></script>


<script src="<?php echo base_url();?>assets/plugins/jquery-knob/js/jquery.knob.js"></script>



<script src="<?php echo base_url();?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<!-- END PAGE LEVEL PLUGINS-->
<!-- BEGIN:File Upload Plugin JS files-->
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo base_url();?>assets/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-ui-1.11.1/jquery-ui.min.js"></script>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/scripts/form-wizard.js"></script>
<script src="<?php echo base_url();?>assets/scripts/form-components.js"></script>
<script src="<?php echo base_url();?>assets/scripts/ui-knob.js"></script>
<script src="<?php echo base_url();?>assets/scripts/form-fileupload.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/scripts/jquery-ui-slider-pips.min.js"></script>
<script src="<?php echo base_url();?>assets/scripts/jquery-ui-slider-pips.js"></script>


<script>
jQuery(document).ready(function() {
	FormFileUpload.init();
   UIKnob.init();
   //UIjQueryUISliders.init();
   FormWizard.init();
   FormComponents.init();
});
</script>