<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
header('Content-Type: text/html; charset=UTF-8'); 
?>
<title>Orden de Servicio</title>
<style>
.page {
	width:1024px;
	height:1000px;
	font-family:Arial;
	
}
.head {
	width:100%;
	height:180px;
}
.contact {
	width:40%;
	height:100%;
	float:left;
}
.logo {
	width:100%;
	height:70px;
	
	background-size: 80%;
	background-repeat:no-repeat;
}
.address {
	width:790px;
	height:100%;
	border:none;
	border-collapse:collapse;
}
.address th {
	font-size:16px;
}
.address td {
	font-size:12px;
}
.address .hours {
	font-size:16px;
	text-align:center;
}
.folio {
	width:60%;
	height:100%;
	float:left;
}
.os-type {
	width:100%;
	height:25px;
	padding-top:20px;
	font-size:16px;
	font-weight:bold;
	text-align:right;
}
.info {
	width:100%;
	height:120px;
	font-size:16px;
}
.message {
	width:60%;
	height:60px;
	float:left;
}
.message p {
	width:98%;
	margin:auto;
	margin-top:10px;
	padding:5px;
	border:0px solid rgb(51,51,51);
	border-radius:15px;
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:11px;
	text-align:justify;
	line-height:180%;
}
.service-order {
	width:40%;
	height:100%;
	float:left;
}
.service-order table {
	width:80%;
	margin:auto;
	margin-top:10px; margin-left:50px;
	font-size:16px;
	border:2px solid rgb(102,102,102);
	border-spacing:0;
	text-align:center;
}
.order-date .gray {
	background-color:rgb(102,102,102);
	color:rgb(255,255,255);
	font-size:12px;
	font-weight:900;
}h
.order-date .white {
	height:27px;
	font-size:8.4px;
	font-weight:900;
	vertical-align:text-top;
	text-align:left;
}
.order-date td {
	border:1px solid rgb(102,102,102);
}
.customer-info {
	width:45%;
	float:left;
	font-size:16px;
}
.customer-info table {
	width:100%;
	margin:auto;
	line-height:150%;
}
.customer-info table th {
	border-bottom:1px solid rgb(0,0,0);
}
.customer-info table td {
	padding-left:5px;
}
.vehicle-info {
	width:55%;
	float:left;
	font-size:16px;
}
.vehicle-info table {
	width:100%;
	margin:auto;
	line-height:150%;
}
.vehicle-info table th {
	border-bottom:1px solid rgb(51,51,51);
}
.vehicle-info table td {
	padding-left:5px;
}
.reception-info {
	width:45%;
	float:left;
	font-size:16px;
}
.reception-info table {
	width:100%;
	height:120px;
	margin:auto;
	line-height:150%;
}
.reception-info table td {
	padding-left:5px;
}
.confirmation {
	width:54%;
	height:135px;
	margin:auto;
	float:left;
	font-size:16px;
	
	line-height:150%;
}
.confirmation p {
	
}
.details {
	width:70%;
}
.comments {
	width:100%;
	height:135px;
	float:left;
	font-size:16px;
	text-align:justify;
	vertical-align:top;
}
.comments .text {
	width:97%;
	height:100%;
	margin:auto;
}
.text .title {
	font-size:12px;
	font-weight:bold;
	margin-left:5px;
}
.itemsb {
	width:80%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:14px;
}
.itemsb table {
	width:100%;
	margin:auto;
	border:none;
}
.items {
	width:100%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.items table {
	width:95%;
	margin:auto;
	border:none;
}
.tecs {
	width:82%;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.tecs table {
	width:100%;
	margin:auto;
}
.tecs table td {
	height:25px;
	padding-left:5px;
	vertical-align:text-top;
	border-bottom:1px solid rgb(0,0,0);
	border-right:1px solid rgb(0,0,0);
}
.tecs table td:last-child {
	border-right:none;
}
.chasis {
	width:100%;
	height:205px;
	float:left;
	background-image:url('');
	background-size: 100%;
	background-repeat:no-repeat;
}
.services-head {
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:16px;
	margin-bottom:10px;
}
.services-head th {
	height:22px;
}
.services {
	font-size:16px;
	border:1px solid rgb(226,0,43);
	border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}
.first-cell {
	border-right:1px solid rgb(226,0,43);
}
.total {
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(0,0,0);
	border-radius:5px;
}
.total-text {
	text-align:right;
}
.row {
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child {
	margin-bottom:0px;
}
.floated {
	float:left;
}
.col-12 {
	width:100%;
}
.col-10 {
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8 {
	width:66.66%;
}
.col-7 {
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5 {
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:20.5%;
}
.col-1 {
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right {
	text-align:center;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left {
	text-align:left;
	padding-left:3px;
}
.radius {
	border: 1px solid rgb(51,51,51);
	border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>
</head>

<body>
<div class="page">
  <div class="row head">
    <div class="contact">
      <img src="<?php echo base_url(); ?>documents/hlogo.png" width="350px">
      <div>
       <div  style=" padding-left:45px; margin-bottom:10px; margin-top:-5px; font-size:16px; "><div style="font-weight:bold;" >OPTIMA AUTOMOTRIZ S.A. DE C.V.</div>
       <div style=" padding-left:40px;">RFC: OAU990325G88</div></div>
        <table cellspacing="0"  class="address" style="width:827px;">
          <tr>
            <th width="25%" class="title right">TIJUANA B.C.:</th>
            <th width="25%" class="title right">MEXICALI B.C.:</th>
            <th width="22%" class="title right">ENSENADA B.C.:</th>
            <th width="30%" class="title">HORARIO DE ATENCI&Oacute;N<th>
          </tr>
          <tr>
            <td class="right">Av. Padre Kino 4300 Zona Río,<br> CP. 22320,<br />
              Tel. (664) 900-9010</td>
            <td class="right">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
              Tel. (686) 900-9010</td> 
			<td class="right" style="text-align:center">Av. Balboa 146 Esq.<br />
              López Mateos Fracc. Granados. CP.  228402,<br />
              Tel. (646) 900-9010
			</td> 
			<td style="text-align:center">Lunes a Viernes de 8:00 A.M. a 6:00 P.M.<br />
              Sábado de 8:00 A.M. a 1:30 P.M.
			</td>
			<td></td>
			
          </tr>
          
        </table>
       
        
      </div>
    </div>
    <div class="folio">
   
      <div class="info">
        <div class="message">
          <p  style="background-color:white"></p>
        </div>
        <div class="service-order">
          <table class="order-date radius">
            <tr class="gray">
              <td>ORDEN DE SERVICIO</td>
            </tr>
            <tr class="white" style="text-align:center; height:15px;">
              <td>&nbsp;</td>
            </tr>
            <tr class="gray">
              <td>FECHA</td>
            </tr>
            <tr class="white" style="text-align:center;height:15px;" >
              <td><?php list($fec,$hor)=explode(" ",$orden[0]->seo_date); echo $fec;?></td>
            </tr>
             <tr class="gray">
              <td>LOCALIDAD</td>
            </tr>
            <tr class="white" style="text-align:center; height:15px;" >
              <td><?php echo $_SESSION['sfworkshop'];?></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin-bottom: 2px;   margin-top: 5px;">
    <div class="customer-info" style="margin-top:12px;">
      <table class="radius">
        <tr class="first-row">
          <th colspan="2" class="title">DATOS DEL CLIENTE (CONSUMIDOR)</th>
        </tr>
        <tr>
          <td colspan="2" ><b>Nombre:</b> <?php echo substr(ucwords(strtolower($orden[0]->sec_nombre)),0,50);?></td>
        </tr>
        <tr>
          <td colspan="2"   ><b>Dirección: </b><?php echo substr(ucwords(strtolower($orden[0]->sec_calle)),0,23);?><?php echo ucwords(strtolower($orden[0]->sec_num_ext));?></td>
        </tr>
        <tr>
         <td ><b>Ciudad:</b> <?php echo ucwords(strtolower($orden[0]->sec_ciudad));?></td>
        <td ><b>Estado:</b> <?php echo ucwords(strtolower($orden[0]->sec_estado));?> <b>CP:</b><?php echo ucwords(strtolower($orden[0]->sec_cp));?></td>
        </tr>
        <tr  >
          <td  class="col-5" style="font-size:15px"><b>R.F.C.:</b><?php echo strtoupper($orden[0]->sec_rfc);?>
</td>
          <td class="col-7" style="font-size:14px"><b>Email:</b> <?php $eema=$orden[0]->sec_email.';xxx'; list($email,$emailb)=explode(';',$eema);
		   echo substr(ucwords(strtolower($email)),0,25);?></td>
        </tr>
        <tr>
          <td colspan="2"  ><b>Teléfono:</b><?php echo ucwords(strtolower($orden[0]->sec_tel));?></td>

        </tr>
        <tr>
          <td  colspan="2" ><b>Persona autorizada para recoger la unidad:</b> &nbsp;</td>
         
        </tr>
        <tr>
          <td  colspan="2" ><b>Nombre:</b> <?php echo substr(ucwords(strtolower($orden[0]->sec_nombre)),0,50);?></td>
         
        </tr>
        <tr>
          <td colspan="2" ><b>Teléfono:</b><?php echo ucwords(strtolower($orden[0]->sec_tel));?></td>
        </tr>
      </table>
    </div>
    <div class="vehicle-info" style="margin-top:12px;">
      <table class="radius">
        <tr>
          <th colspan="4" class="title">DATOS DEL VEHÍCULO</th>
        </tr>
       
        <tr height="36px;">
          <td ><b>Marca:</b><?php echo ucwords(strtolower($orden[0]->sev_marca));?></td>
          <td ><b>Sub Marca:</b> <?php echo ucwords(strtolower($orden[0]->sev_sub_marca));?></td>
        </tr>
        <tr style="height:36px;">
          <td ><b>Año-Modelo:</b><?php echo ucwords(strtolower($orden[0]->sev_modelo));?></td>
          <td ><b>Placas:</b> &nbsp;</td>
        </tr>
        <tr style="height:36px;">
          <td ><b>Fecha de Venta:</b> &nbsp;</td>
  
          <td ><b>Último Servicio:</b> &nbsp;</td>

        </tr>
        <tr style="height:36px;">
          <td class="col-6"><b>Color:</b>  <?php echo ucwords(strtolower($orden[0]->sev_color));?></td>
          <td class="col-6"><b>Seguro:</b> &nbsp;</td>
        </tr>
        <tr style="height:36px;">
          <td  class="col-6"><b>No. de  VIN:</b> <?php echo strtoupper ($orden[0]->sev_vin);?></td>
          <td class="col-6"><b>Km. de Entrada:</b><?php echo ucwords(strtolower($orden[0]->sev_km_recepcion));?></td>
        </tr>
        <tr>
         <td  class="col-6"><b>No. de  Motor:</b> &nbsp;</td>
          <td class="col-6"><b>Capacidad:</b> <?php echo ucwords(strtolower($orden[0]->sev_capacidad));?></td>

        </tr>
       
      </table>
    </div>
  </div>
  <div class="row" style="margin-bottom: 2px;" >
    <div class="reception-info">
      <table  class="radius" width="100%">
        
        <tr>
          <td colspan="2"><b>Asesor:</b> <?php echo $orden[0]->sus_name.' '.$orden[0]->sus_lastName?></td>
        </tr>
        <tr>
          <td colspan="2"><b>Técnico:</b> &nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><b>Fecha y hora de recepción:</b><?php echo ucwords(strtolower($orden[0]->seo_timeReceived));?></td>
        </tr>
        <tr>
          <td colspan="2" ><b>Fecha de entrega del vehículo:</b> <?php echo date('Y-m-d');?></td>

        </tr>
       
        <tr height="27px;">
          <td  style=" padding-top:4px; text-align:center; font-weight:bold;background-color:black; color:white" class="col-5" >NUMERO DE TORRE: </td>
          <td width="27px;" style="font-size:22px; font-weight:bold;  border:1px solid white"><?php echo $orden[0]->seo_tower;?></td>
          
        </tr>
        
      </table>
    </div>
    <div class="confirmation radius" style="padding-left:5px; font-weight:bold; color:black; font-size:13px;">Se entregan las partes o refacciones reemplazadas al consumidor SI &nbsp;&nbsp;(  ) &nbsp;&nbsp;&nbsp;&nbsp; NO &nbsp;&nbsp;( )
      <br>
      <b>NOTA:</b> Las partes y/o refacciones no se entregar&aacute;n al consumidor cuando:<br>
      a) Sean cambiadas por garantía<br>
      b) Se trate de residuos peligrosos deacuerdo con las disposiciones legales aplicables.<br>
      
      Servicio  en el domicilio del consumidor &nbsp;( ) &nbsp; SI  &nbsp;(  ) &nbsp; NO<br>
      
      Poliza de seguros para cubrir al consumidor los daños o extravios de <br> bienes: SI ( ) Numero:______________.  ( )NO 
      
    </div>
  </div>
  <div class="row">
    <div class="floated">
      <div style="margin-bottom: 73px;">
        <div class="text radius" style=" height:50px; padding-left:15px;"> <span class="title">COMENTARIOS:</span> <br>  <?php echo $orden[0]->seo_comments;?>
          <div style="font-size:16px; ">
           </div></div>
      </div>
     
       <div class="itemsb"  style="margin-top:-70px;">
       <div style="font-weight:bold; width:100%; text-align:center; color:rgb(226,0,43);">INVENTARIO DEL VEHICULO</div>
        <table >
          <tr style="height:25px">
            <td width="15px;" ><?php if($orden[0]->seo_tapetes==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">TAPETES</td>
            <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_tapones==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">TAPONES</td>
            <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_herramienta==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">HERRAMIENTA</td>
             <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_reflejantes==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">REFLEJANTES</td>
             <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_ecualizador==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ECUALIZADOR</td>
              <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_tapon==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">TAPÓN GAS</td>
                <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_ventanas==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">VENTANAS</td>
          </tr>
          <tr style="height:25px">
            <td><?php if($orden[0]->seo_espejo==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ESPEJO</td>
               <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_radio==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">RADIO</td>
            <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_llantarefaccion==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">LLANTA REF.</td>
               <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_extinguidor==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">EXTINGUIDOR</td>
          <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_gato==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">GATO</td>
              <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_carnet==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">CARNET</td>
                 <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_vestidura==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">VESTIDURA</td>
          </tr>
          <tr style="height:25px">
               <td><?php if($orden[0]->seo_antena==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ANTENA</td>
                 <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_encendedor==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ENCENDEDOR</td>
           <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_limpiadores==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">LIMPIADORES</td>
                    <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_cables==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">CABLES P/C</td>
                   <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_estereo==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">ESTEREO</td>
                      <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_carroceria==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">CARROCERÍA</td>
                     <td width="15px;" style="text-align:right"><?php if($orden[0]->seo_parabrisas==2){echo'&radic;';}else{echo'X';}?></td>
            <td style=" font-weight:bold">PARABRISAS</td>
          </tr>
          <tr >
            <td height="18px;"></td>
            <td ></td>
            <td ></td>
            <td></td>
            <td></td> <td colspan="4" style="font-weight:bold; font-family:14px; background-color:black; color:white; text-align:center" > CANTIDAD DE COMBUSTIBLE: </td>
            <td style="background-color:black; color:white; text-align:center; font-weight:bold"><?php echo ' '.$orden[0]->seo_combustible;?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
      <div class="tecs" style=" padding-right:30px; margin-top:8px;">
        <table style="text-align:center; margin-top:30px; font-weight:bold; font-size:16px;" class="radius" >
          <tr>
            <td valign="middle" style="padding-top:3px;">Hora de Recepción</td>
            <td>Hora de Inicio de Trabajo</td>
            <td>Hora de Trabajo Terminado</td>
          </tr>
          <tr>
            <td><?php list($fech,$hora)=explode(" ",$orden[0]->seo_timeReceived); echo ucwords(strtolower($hora));?>
             </td>
            <td></td>
            <td><?php echo ucwords(strtolower($orden[0]->seo_promisedTime));?></td>
          </tr>
        </table>
      </div>
    <div class=" floated" style="margin-top:-73px; padding-right:10px;">
     <img src="<?php echo base_url(); ?>documents/chasis.png" height="180px;" width="133px;">
   <table style="margin-top:-175px;" width="130px" height="150px;">
<tr>
<td width="20px;" align="center"><?php if($orden[0]->sei_cha_uno=='ok'){echo ' ';}?></td>
<td width="95px;"></td>
<td><?php if($orden[0]->sei_cha_dos=='ok'){echo '';}?></td>
</tr>
<tr height="43px;">
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td width="20px;" align="center"><?php if($orden[0]->sei_cha_tres=='ok'){echo '';}?></td>
<td></td>
<td><?php if($orden[0]->sei_cha_cuatro=='ok'){echo '';}?></td>
</tr>

</table> 
    </div>
       <div  class="comments" style="margin-top:10px; height: 93px; width:1022px; padding-left:5px; margin-left:-10px;">
        <div class="text radius" style=" width:1022px; height:90px; "> 
          <table width="1024px" style="text-align:center; " cellspacing="0"  border="0">
  <tr>
    <td width="30%" style=" border-bottom:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">OPERACIONES A EFECTUAR</td>
    <td width="30%" style=" border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PARTES Y/O REFACCIONES</td>
    <td width="30%" style="  border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PRECIOS UNITARIOS</td>
    <td width="10%" style=" border-bottom:1px solid #000; border-top:1px solid #000;"></td>
  </tr>
  
  <?php 
  setlocale(LC_MONETARY, 'en_US');
  $num=count($servicios);
  $numsv=count($serviciossv);
  $limite=11 - $num - $numsv;
  $monto=0;
  $tiposer='';
  foreach($servicios as $ser){
	  
	  
	 if($ser->ser_tipo=='pro'){
	  $comercial=$ser->ser_comercial / 1.16;
	 }else{
	  $comercial=$ser->ser_comercial;
	 }
	 $tiposer=$ser->ssr_venta;
	 
	  $monto+=$comercial;
	  
	  
	echo ' 
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$ser->set_name.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($comercial, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';  
	  }
	  
	  
	  foreach($serviciossv as $sersv){
	  
	  
	
	  
	  
	echo ' 
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$sersv->sev_nombre.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format(0, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';  
	  }
	  
	  
  for($x=0; $x<$limite; $x++) {?>
  <tr>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style="border-bottom:1px solid #999; text-align:left; ">$</td>
  </tr>
 <?php } ?>
 
 
 
  <tr>
    <td width="30%" style="border-right:1px solid #000;">&nbsp;</td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;">MONTO DE LA OPERACI&Oacute;N</td>
    <td width="10%" style=" text-align:left;border-top:1px solid #000;"><?php echo '<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($monto, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>'; ?></td>
  </tr>
 <tr>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="10%" style=" text-align:left;  ">&nbsp;</td>
  </tr>
</table> 
 
   
          </div>
      </div>

      <div style="margin-bottom: 32px;">
        <div class="text radius" style=" height:50px; padding-left:15px;"> <span class="title">POSIBLES CONSECUENCIAS:</span> <br>
          <div style="font-size:16px; ">
           </div></div>
      </div>      
      <div class="comments" style="margin-top:-30px; width:1020px; margin-left:-10px;">
          <div class="text " style=" height:100px; ">
          <table width="100%">
          <tr>
          <td width="15%">FORMA DE PAGO</td>
          <td width="50%">EFECTIVO ( <?php if($orden[0]->seo_pago==1){echo '&radic;';}?>) CHEQUE (<?php if($orden[0]->seo_pago==2){echo '&radic;';}?>) TARJETA DE CREDITO (<?php if($orden[0]->seo_pago==3){echo '&radic;';}?>) OTRO (<?php if($orden[0]->seo_pago==4){echo '&radic;';}?> )</td> 
          <td width="25" style=" text-align:right">SUBTOTAL</td>         
          <td width="10%">$ <?php echo number_format($monto, 2, '.', '');?></td>         
          </tr>
          <tr>
          <td width="15%"></td>
          <td width="50%"></td> 
          <td width="25" style=" text-align:right">IVA</td>         
          <td width="10%">
          <?php if($tiposer=='interna'){$iva=0;}
	else{$iva=0.16;}?>
          $<?php echo number_format($monto * $iva, 2, '.', '');?></td>         
          </tr>
          <tr>
          <td width="15%"></td>
          <td width="50%"></td> 
          <td width="25" style=" text-align:right"> TOTAL</td>         
          <td width="10%">$</td>         
          </tr>
          <tr>
          <td width="15%">&nbsp;</td>
          <td width="50%"></td> 
          <td width="25" style=" text-align:right"></td>         
          <td width="10%"></td>         
          </tr>
          <tr>
          <td colspan="4" style="font-size:13px; font-weight:bold" width="100%">
          *EL TOTAL NO INCLUYE SERVICIOS ADICIONALES. EN CASO DE SER NECESARIOS, SE SOLICITARA AUTORIZACION AL CLIENTE VIA TELEFONICA
          </td>
                 
          </tr>
          </table>
          
          <!--<table width="100%" style="text-align:center; font-size:14px;"  cellspacing="0"  border="0">
  <tr>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">FORMA DE PAGO</td>
    <td width="10%" style="border-right:1px solid #000; text-align:left; border-top:1px solid #000; text-decoration:underline;"></td>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">SERVICIOS ADICIONALES:</td>
    <td width="10%" style=" border-top:1px solid #000;"></td>
  </tr>

 <tr>
    <td  style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">Monto de la operaci&oacute;n:
    
    </td>
    <td style="border-right:1px solid #000; text-align:left; ">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" "></td>
  </tr>
  <tr>
    <td style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Otros cargos:
    </td>
    <td  style="border-right:1px solid #000;text-align:left;">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Total refacciones y servicios adicionales</td>
    <td  style=" text-align:left">$</td>
  </tr>
<tr>
    <td style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">
    Servicios Adicionales:
    
    </td>
    <td  style="border-right:1px solid #000; text-align:left;">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" border-top:1px solid #000;"></td>
  </tr>
  <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Parcial:
    
    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Fecha y monto de anticipo:</td>
    <td  style="text-align:left" >$</td>
  </tr>
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Impuesto al Valor Agregado:
    
    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">El resto del monto total de la operaci&oacute;n. Se liquidara en</td>
    <td  style="border-top:1px solid #000;  text-align:left"></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Monto Total (Incluye mano de obra):
    
    </td>
    <td  style="border-right:1px solid #000;text-align:left; border-bottom:1px solid #000; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">  la fecha programada para la entrega del veh&iacute;culo</td>
    <td  style=" "></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Efectivo ( &nbsp; ) Cheque ( ) tarjeta de cr&eacute;dito ( ) Otro ( )
    
    </td>
    <td style="border-right:1px solid #000;text-align:left;  ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>
  
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">

    
    </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>
  
   
  
  
  
   
   
 
</table>    -->
          </div>
      </div>
      
      
     
     
    </div>
    
    
    
    
  </div>
  <div class="row" style="margin-top:-20px;"> 
  
    <b style=" font-size:16px; margin-left:5px; margin-top:-26px; ">Imagenes del Vehículo ( Condiciones de Recepción )</b> <br>

     <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Costado Derecho</th>
        <th width="20%" >Costado Izquierdo</th>
        <th width="20%" >Parte Frontal</th>
        <th width="20%" >Parte Trasera</th>
        <th width="20%" >Tablero</th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td><?php 
		$ider=$orden[0]->sei_inf_der; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_inf_izq; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_pos_der; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_pos_izq; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td><?php 
		$ider=$orden[0]->sei_tablero; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
      </tr>
    </table>

    <b style=" font-size:16px;">Imagenes Extras</b> <br>

    <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Extra 1</th>
        <th width="20%" >Extra 2</th>
        <th width="20%" >Extra 3</th>
        <th width="40%" ></th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td style="width:20%"><?php 
		$ider=$orden[0]->sei_extra_uno; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td style="width:20%"><?php 
		$ider=$orden[0]->sei_extra_dos; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td style="width:20%"><?php 
		$ider=$orden[0]->sei_extra_tres; 
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';	
			}?>
            <a href="<?php echo $url;?>/<?php echo $ider;?>" target="_blank">
          <img style="border:1px solid #CCC" src="<?php echo $url;?>/<?php echo $ider;?>" width="100px;"></a></td>
        <td width="40%"></td>
      </tr>
    </table>
  </div>
</div>



</body>
</html>
<script type="text/javascript">
<!--
//window.print();
//-->
</script>