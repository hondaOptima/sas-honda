<body <?php if(!empty($cerrar)){ echo 'onload="load()"';}?> class="page-header-fixed page-sidebar-closed">
<!--page-sidebar-fixed-->
<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" href="<?php echo base_url();?>">
		<img src="<?php echo base_url();?>assets/img/logo.png" alt="logo" class="img-responsive" />
		</a>
		
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<img src="<?php echo base_url();?>assets/img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<li class="dropdown" id="header_notification_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="fa fa-warning"></i>
				<span class="badge badge-danger">
                <?php
				$this->load->model('Permisomodel');
				
                $OS=$this->Permisomodel->misOrdenes();
				echo count($OS);
				
				?>
					 
				</span>
				</a>
				<ul class="dropdown-menu extended notification">
					<li>
						<p>
							Mis Ordenes de Servicio
						</p>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" style="height: 250px;">
							<?php foreach($OS  as $ores){
								if($ores->seo_cliente==0){
									$ira=''.base_url().'/clientes/';
									}else{
										$ira=''.base_url().'orden/llenar1/'.$ores->seo_idServiceOrder.'';
										}
								
								?>
							<li>
								<a  href="<?php echo $ira;?>">
								<span class="label label-sm label-icon 
                                <?php
                                    if($ores->enp_status ==1){echo 'label-danger';}
									if($ores->enp_status ==2){echo 'label-warning';}
									if($ores->enp_status ==3){echo 'label-success';}
									if($ores->seo_cliente==0){echo 'Es necesario que seleccione un cliente, pero si no existe seleccionar la opcion "El Cliente no Existe en la Base de Datos".';}
									?>
                                ">
									<i class="fa fa-bolt"></i>
								</span>
								 <?php echo $ores->app_customerName.' '.$ores->app_customerLastName; ?>
                                 <br>
								<span class="time">
									<?php
                                    if($ores->enp_status ==1){echo 'Orden Incompleta';}
									if($ores->enp_status ==2){echo 'Orden En Captura para Intellisis';}
									if($ores->enp_status ==3){echo 'Orden Finalizada Exitosamente';}
									?>
								</span>
								</a>
							</li>
						<?php } ?>
						
						</ul>
					</li>
					
				</ul>
			</li>
         
			<!-- END NOTIFICATION DROPDOWN -->
			<!-- BEGIN INBOX DROPDOWN -->
			
			<!-- END TODO DROPDOWN -->
			<li class="devider">
				 &nbsp;
			</li>
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<img alt="" width="29" src="<?php echo base_url();?>assets/img/avatar.png"/>
				<span class="username">
					 <?php echo $_SESSION['sfname'].'('.$_SESSION['sfworkshop'].')'; ?>
				</span>
				<i class="fa fa-angle-down"></i>
				</a>
                
				<ul class="dropdown-menu">
					<?php if($_SESSION['sftype']==1 || $_SESSION['sftype']==2) {?>
					<li>
						<a href="<?php echo base_url();?>usuario"><i class="fa fa-user"></i> Usuarios</a>
					</li>
                    
                    <li>
						<a href="<?php echo base_url();?>usuario/equipos"><i class="fa fa-group"></i> Mantenimiento de Equipos</a>
					</li>
                     <li><a href="<?php echo base_url();?>dashboard/ppto"><i class="fa fa-usd"></i>PPTO</a></li>
                    <?php  if($_SESSION['sfrol']==1){?> 
					<li>
					<a href="<?php echo base_url();?>capacidad"><i class="fa fa-filter"></i> Capacidad</a>
					</li>
                   
                    <?php } ?>
                    <?php if($_SESSION['sfrol']==1){?> 
                    <li>
					<a href="<?php echo base_url();?>servicio"><i class="fa fa-wrench"></i> Servicios</a>
					</li>
                    <?php } if($_SESSION['sfrol']==1 || $_SESSION['sftype']==2){?> 
                      <li>
					<a href="<?php echo base_url();?>setupcalendario"><i class="fa fa-calendar"></i> Calendario</a>
					</li>
                    <li>
					<a href="<?php echo base_url();?>reportes/horario_entregas"><i class="fa fa-clock-o"></i> Horario Entregas</a>
					</li>
                     <?php } ?>
                     <li>
					<a href="<?php echo base_url();?>promocion"><i class="fa fa-tag"></i> Promociones</a>
					</li>
					
					<li class="divider">
					</li>
				<li>
					<a href="<?php echo base_url();?>assets/manual.pdf"><i class="fa fa-book"></i> Manual de Usuario</a>
				</li> <?php } ?>
				<li>
					<a href="<?php echo base_url();?>acceso/logout/"><i class="fa fa-key"></i> Cerrar Sesión</a>
				</li>
			</ul>
           
		</li>
		<!-- END USER LOGIN DROPDOWN -->
	</ul>
	<!-- END TOP NAVIGATION MENU -->
</div>
</div>
<!-- END TOP NAVIGATION BAR -->
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->