<style type="text/css">
body {
    overflow:hidden;
}
</style>
<body  onLoad="mueveReloj()"  class="page-header-fixed page-sidebar-closed" style="background-color:white">
<br>
<!--page-sidebar-fixed-->
<div class="header navbar navbar-inverse navbar-fixed-top" style="margin-top:30px;">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<a class="navbar-brand" href="<?php base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png" alt="logo" class="img-responsive" /></a>
        <ul class="nav navbar-nav pull-right" style="margin-top:10px;">
			<!-- BEGIN NOTIFICATION DROPDOWN -->
            <script language="JavaScript">
            function mueveReloj() {
                var tipo = '';
               	momentoActual = new Date();
               	hora = momentoActual.getHours();
               	minuto = momentoActual.getMinutes();
               	segundo = momentoActual.getSeconds();

               	str_segundo = new String (segundo)
               	if (str_segundo.length == 1) segundo = "0" + segundo;

               	str_minuto = new String (minuto);
               	if (str_minuto.length == 1) minuto = "0" + minuto;

               	str_hora = new String (hora);
               	if(str_hora.length == 1) hora = "0" + hora;

                if(hora>12) { hora=hora-12; tipo='P.M.'; } else tipo='A.M.';
               	horaImprimible = hora + " : " + minuto + " : " + segundo + " " + tipo;
               	var hr=document.getElementById('hora').innerHTML=horaImprimible;
               	setTimeout("mueveReloj()", 1000);
            }
            </script>
            <li>
                <div id="hora" style="color:white; margin-right:110px; margin-top:-15px; font-weight:bold; font-size:40px;"></div>
            </li>
        	<li class="dropdown user" style="color:white; font-size:40px; ">
                <div style=" margin-top:-15px;">
        		    <?php echo $di.' '.$meses[date($ms)].' '.$an;?>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="page-container" style=" margin-top:20px;">
<!-- BEGIN SIDEBAR -->
