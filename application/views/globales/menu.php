<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" >
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<div class="clearfix">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>
				</li>
                <?php
                if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==6 || $_SESSION['sfrol']==7 || $_SESSION['sfrol']==8) {
				 ?>
                 <li class="<?php if(!empty($menu)){if($menu=='citas'){echo 'start active';}}?>">
					<a title="Citas" href="<?= base_url();?>citas">
					<i class="fa fa-calendar"></i>
					<span class="title">
					Citas
					</span>
					</a>
				</li>
                <?php }
				 if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==6 || $_SESSION['sfrol']==7 || $_SESSION['sfrol']==8) {
				?>
                <li class="<?php if(!empty($menu)){if($menu=='captura'){echo 'start active';}}?>">
					<a title="Captura de Captura Intelisis" href="<?= base_url();?>captura-orden">
						<i class="fa fa-keyboard-o"></i>
						<span class="title">
							Captura Intelisis
						</span>
					</a>
				</li>

                 <?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==4 || $_SESSION['sfrol']==5 || $_SESSION['sfrol']==6 || $_SESSION['sfrol']==7 || $_SESSION['sfrol']==8) {
				 ?>
                <li class="<?php if(!empty($menu)){if($menu=='pizarron'){echo 'start active';}}?>">
					<a title="Pizarrón Electrónico" href="<?php echo base_url();?>pizarron">
					<i class="fa fa-desktop "></i>
					<span class="title">
					Pizarrón Electrónico
					</span>
					</a>
				</li>
				<?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==4 || $_SESSION['sfrol']==5 || $_SESSION['sfrol']==6 || $_SESSION['sfrol']==7 || $_SESSION['sfrol']==8) {
				?>
				<li class="<?php if(!empty($menu)) { if($menu == 'pizarron') { echo 'start active'; } } ?>">
					<a title="Lavado" href="<?= base_url();?>lavado">
						<i class="fa fa-shower"></i>
						<span class="title">
							Lavado
						</span>
					</a>
				</li>
                <?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==4 || $_SESSION['sfrol']==5 || $_SESSION['sfrol']==6 || $_SESSION['sfrol']==7 || $_SESSION['sfrol']==8) {
				 ?>
                <li class="<?php if(!empty($menu)){if($menu=='carroceria'){echo 'start active';}}?>">
					<a title="Carrocería" href="<?= base_url();?>carroceria">
					<i class="fa fa-truck "></i>
					<span class="title">
					Carrocería
					</span>
					</a>
				</li>
                <?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3  || $_SESSION['sfrol']==7 || $_SESSION['sfrol']==8) {
				?>
                <li class="<?php if(!empty($menu)){if($menu=='cotizaciones'){echo 'start active';}}?>">
					<a title="Cotizaciones" href="<?php echo base_url();?>cotizaciones">
					<i class="fa fa-money "></i>
					<span class="title">
					Cotizaciones
					</span>
					</a>
				</li>
                <?php } ?>
                  <?php if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==8) {?>
				<li class="<?php if(!empty($menu)){if($menu=='control'){echo 'start active';}}?>">
					<a title="Control de Flujo" href="<?php echo base_url();?>control">
					<i class="fa  fa-clipboard"></i>
					<span class="title">
						Control de Flujo
					</span>
					</a>
				</li>

                <?php }
                 if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==8) {?>
				<li class="<?php if(!empty($menu)){if($menu=='productividad'){echo 'start active';}}?> ">
					<a title="Productividad" href="<?php echo base_url();?>productividad">
					<i class="fa fa-gears"></i>
					<span class="title">
						Productividad
					</span>
					<span class="selected">
					</span>
					</a>
				</li>

                <?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2  ) {
				?>
                 <li class="<?php if(!empty($menu)){if($menu=='estadisticas'){echo 'start active';}}?>">
					<a title="Estadisticas" href="<?php echo base_url();?>estadisticas">
					<i class="fa fa-bar-chart-o"></i>
					<span class="title">
					Concentrado
					</span>
					</a>
				</li>

                 <?php } if($_SESSION['sfrol']==7 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==8) {?>
                 <li class="<?php if(!empty($menu)){if($menu=='reportes'){echo 'start active';}}?>">
					<a href="<?= base_url()?>reportes/saveVentaPerdida/">
					<i class="fa fa-table"></i>
					<span class="title">
						Venta Perdida
					</span>
					<span class="arrow ">
					</span>
					</a>
                    </li>
				 <?php } if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfid'] == 65 || $_SESSION['sfid'] == 159) {?>


                <li class="<?php if(!empty($menu)){if($menu=='dash'){echo 'start active';}}?> ">
					<a  title="Dashboard" href="<?php echo base_url();?>dashboard">
					<i class="fa fa-dashboard"></i>
					<span class="title">
						Dashboard
					</span>
					<span class="selected">
					</span>
					</a>
				</li>
                 <li class="<?php if(!empty($menu)){if($menu=='reportes'){echo 'start active';}}?>">
					<a href="javascript:;">
					<i class="fa fa-table"></i>
					<span class="title">
						Reportes
					</span>
					<span class="arrow ">
					</span>
					</a>
					<ul class="sub-menu">

						<li>
							<a href="<?= base_url(); ?>comportamiento-citas">
							Comportamiento de las Citas</a>
						</li>
                        <li>
							<a href="<?= base_url(); ?>analisis-diario-ordenes">
							Analisis Diario de Ordenes</a>
						</li>
                        <li>
							<a href="<?= base_url(); ?>profit-watch">
							Profit Watch</a>
						</li>

                        <li>
							<a href="<?= base_url(); ?>venta-perdida">
							Venta Perdida</a>
						</li>
						<li>
							<a href="<?= base_url(); ?>auditoria-citas">
							Auditoria Citas</a>
						</li>



					</ul>

				</li>
                <?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2 || $_SESSION['sfrol']==3 || $_SESSION['sfrol']==8) {
				?>

                 <li class="<?php if(!empty($menu)){if($menu=='clientes'){echo 'start active';}}?>">
					<a href="javascript:;">
					<i class="fa fa-male "></i>
					<span class="title">
					Clientes
					</span>
					</a>
                    <ul class="sub-menu">
						<li>
							<a href="<?php echo base_url();?>clientes">
							Lista de Clientes
                            </a>
						</li>

                        <li>
							<a href="<?php echo base_url();?>clientes/campanias">
							Campañas
                            </a>
						</li>
                    </ul>
				</li>
                <?php }
				if($_SESSION['sfrol']==1 || $_SESSION['sfrol']==2  ) {
				?>

                <li class="<?php if(!empty($menu)){if($menu=='demo'){echo 'start active';}}?>">
					<a title="Orden Demo" target="_blank" href="http://www.hondaoptima.com/sas/orden/printtestDuplicadoBueno/14725">
					<i class="fa fa-play"></i>
					<span class="title">
					Orden Demo
					</span>
					</a>
				</li>


                <?php }

				?>
                <li class="<?php if(!empty($menu)){if($menu=='inventario'){echo 'start active';}}?>">
					<a href="javascript:;">
					<i class="fa fa-book"></i>
					<span class="title">
						Inventario
					</span>
					<span class="arrow ">
					</span>
					</a>
					<ul class="sub-menu">

						<li>
							<a href="<?= base_url();?>inventario">
							LLegada de Inventario</a>
						</li>
                        <li>
							<a href="<?= base_url();?>status-vehiculo">
							Status del Vehículo</a>
						</li>
                        <li>
							<a href="<?= base_url();?>calendario-entregas">
							Calendario de Entregas</a>
						</li>

                        <li>
							<a href="<?= base_url();?>autos-recibidos">
							Autos Recibidos</a>
						</li>

                         <li>
							<a href="<?= base_url();?>autos-proceso-venta">
							Autos en Proceso de Venta</a>
						</li>

                         <li>
							<a href="<?= base_url();?>status-torres">
							Status Torres</a>
						</li>

                        <li>
							<a href="<?= base_url();?>autos-traslados">
							Autos en Traslado</a>
						</li>

						<li>
							<a href="<?= base_url();?>traslados-entregas/<?= $_SESSION["sfworkshop"]?>">
							Imrpimir Inventario <?= $_SESSION["sfworkshop"] ?></a>
						</li>
					</ul>
				</li>

				<?php if($_SESSION['sfid']==97){ ?>
					<li class="<?php if(!empty($menu)){if($menu=='demo'){echo 'start active';}}?>">
						<a title="Traslados"  href="http://hsas.gpoptima.net/traslados/">
						<i class="fa fa-times"></i>
						<span class="title">
						Traslados
						</span>
						</a>
					</li>
					<li class="<?php if(!empty($menu)) { if($menu=='demo') { echo 'start active'; } } ?>">
						<a title="Traslados"  href="http://hsas.gpoptima.net/traslados/config">
						<i class="fa fa-wrench"></i>
						<span class="title">
						Traslados Config
						</span>
						</a>
					</li>
				<?php } ?>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>

</div>
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
