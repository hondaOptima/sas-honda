<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="esp" class="no-js">
<head>
<?php
header('Content-Type: text/html; charset=UTF-8');
?>

<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>Honda Optima | SAS</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" href="<?= base_url() ?>images/honda-icon.png">
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<!-- <link href="<?= base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/> -->
<link href="<?= base_url();?>assets-new/plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<link href="<?= base_url();?>assets/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
<!-- DATE TIME PICKER-->
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/gritter/css/jquery.gritter.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/select2/select2_conquer.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-conquer.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link href="<?= base_url();?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/plugins/bootstrap-toastr/toastr.min.css"/>
<!-- BEGIN THEME STYLES -->
<link href="<?= base_url();?>assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css"/>

<link href="<?= base_url();?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/jquery-ui-1.11.1/jquery-ui.css" rel="stylesheet" type="text/css"/>

<link href="<?= base_url();?>assets/css/jquery-ui-slider-pips.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url();?>assets/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>

<link href="<?= base_url();?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="<?= base_url();?>favicon.ico"/>
<script> window.base_url = "<?= base_url() ?>"</script>
</head>
