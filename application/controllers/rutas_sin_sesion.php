<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rutas_sin_sesion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reportesmodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->helper('dates');
		$this->load->helper('token');
		session_start();
        $this->token = md5(sha1('honda_'.date("Y-m-d")));
	}

    function traslados_entregas($ciudad)
    {
        // if(isset($_SESSION['sfid']) || (!empty($_GET['token']) && $_GET['token'] == $this->token))
        if(isset($_SESSION['sfid']) || (!empty($_GET['token']) && construct_token($_GET['token'])))
        {
            //9)Almacen 1;  10)Almacen 2; 11) Almacen 3; 12) Almacen 4;
            $data["fecha"] = (!empty($_GET['fecha'])) ? $_GET['fecha'] : date("Y-m-d");
            $data["fecha_completa"] = fecha_completa_larga($data["fecha"]);
            $data["entregas"] = $this->Reportesmodel->get_entregas($ciudad, $data["fecha"]);
            $data["traslados"] = $this->Reportesmodel->get_traslados($ciudad, $data["fecha"]);
            $data["almacenes"] = array(9 => "Almacén 1",10 => "Almacén 2", 11 => "Almacén 3", 12 => "Almacén 4");
            if(count($data["entregas"]) > 0 || count($data["traslados"]) > 0)
                $this->load->view("reportes/traslado_entrega", $data);
            else
                echo 'No hay entregas ni traslados para el día de hoy '.$data["fecha_completa"];
        }
        else
            redirect('acceso');
    }

    function ver_token()
    {
        echo construct_token("ae6c476d1ab09d29802fafb324df490f&53");
    }
}
