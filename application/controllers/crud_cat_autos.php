
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Crud_cat_autos extends CI_Controller {
 
function __construct()
{
        parent::__construct();
        $this->load->database();
       $this->load->library('grocery_CRUD');
}
 
public function index()
{
 
}
 
public function autos()
{
$crud = new grocery_CRUD();
 //$crud->set_theme('datatables');
$crud->set_lang_string('list_add','Agregar Auto')
					   ->set_lang_string('list_record',' ');
$crud->set_table('caracteristicas_auto');
$crud->columns('cau_codigo','cau_modelo','cau_nombre','cau_version','cau_motor','cau_transmision','cau_rines','cau_llanta','cau_llantas_delanteras_psi','cau_llantas_traseras_psi','cau_capacidad_tanque','cau_torque','cau_potencia','cau_motor_cilindros','cau_motor_litros','cau_motor_valvulas','cau_motor_cilindros_posicion');
$crud->fields('cau_codigo','cau_modelo','cau_nombre','cau_version','cau_motor','cau_transmision','cau_rines','cau_llanta','cau_llantas_delanteras_psi','cau_llantas_traseras_psi','cau_capacidad_tanque','cau_torque','cau_potencia','cau_motor_cilindros','cau_motor_litros','cau_motor_valvulas','cau_motor_cilindros_posicion');

$crud->display_as('cau_modelo','Modelo');
$crud->display_as('cau_nombre','Nombre');
$crud->display_as('cau_version','Version');
$crud->display_as('cau_motor','Motor');
$crud->display_as('cau_transmision','Transmision');
$crud->display_as('cau_rines','Rines');
$crud->display_as('cau_llanta','Tamano de llanta');

$crud->display_as('cau_llantas_delanteras_psi','Presion llanta delanteras');
$crud->display_as('cau_llantas_traseras_psi','Presion llanta traseras');

$crud->display_as('cau_capacidad_tanque','Capacidad de tanque');
$crud->display_as('cau_potencia','Potencia');
$crud->display_as('cau_torque','Torque');
$crud->display_as('cau_motor_valvulas','Numero de Valvulas');
$crud->display_as('cau_motor_litros','Motor litros');
$crud->display_as('cau_motor_cilindros','Cilindros');

$crud->display_as('cau_motor_cilindros_posicion','Cilindros Posicion');

$crud->display_as('cau_codigo','Codigo');


$crud->set_rules('cau_modelo','Modelo', 'required');
$crud->set_rules('cau_nombre','Nombre', 'required');
$crud->set_rules('cau_version','Version', 'required');
$crud->set_rules('cau_motor','Motor', 'required');
$crud->set_rules('cau_transmision','Transmision', 'required');
$crud->set_rules('cau_rines','Rines', 'required');
$crud->set_rules('cau_llanta','Tamano de llanta', 'required');

$crud->set_rules('cau_llantas_traseras_psi','Presion de llanta traseras', 'required');
$crud->set_rules('cau_llantas_delanteras_psi','Presion de llanta delanteras', 'required');

$crud->set_rules('cau_capacidad_tanque','Capacidad de tanque', 'required');
$crud->set_rules('cau_potencia','Potencia', 'required');
$crud->set_rules('cau_torque','Torque', 'required');

$crud->set_rules('cau_motor_valvulas','Valvulas', 'required');
$crud->set_rules('cau_motor_cilindros','Cilindros', 'required');
$crud->set_rules('cau_motor_litros','Motor litros', 'required');

$crud->set_rules('cau_motor_cilindros_posicion','Cilindros posicion', 'required');
$crud->set_rules('cau_codigo','Codigo', 'required');



$output = $crud->render();
 
$this->_example_output($output);                
}
 
function _example_output($output = null)
{
$this->load->view('grocery_c_a.php',$output);    
}    
}