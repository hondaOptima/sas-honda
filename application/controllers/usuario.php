<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['usuarios']=$this->Usuariomodel->listaUsuarios($this->per);	
		$this->load->view('usuario/vista',$data);
	}
	
	public function crear()
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
		$this->form_validation->set_rules('telefono', 'Telefono', 'required');
		$this->form_validation->set_rules('numEmpleado', 'N�mero de empleado');
		$this->form_validation->set_rules('numAsesor', 'N�mero de asesor');
		$this->form_validation->set_rules('dtps', 'DTPS');
		$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('contrasena', 'Contrase�a', 'required');
		$this->form_validation->set_rules('tipoUsuario', 'Tipo de usuario', 'required');
		$this->form_validation->set_rules('rol', 'Rol', 'required');
		$this->form_validation->set_rules('taller', 'Taller', 'required');
		$nombre='usuario.jpg';
 
        if ($this->form_validation->run())
        {
			$toemail=$this->input->post('correo');
			$nombre=$this->input->post('nombre');
			$apellido=$this->input->post('apellido');
			$correo=$this->input->post('correo');
			$con=$this->input->post('contrasena');
			
			$tmp_new_name = $_FILES["userfile"]["name"];
		 	$path_parts = pathinfo($tmp_new_name);
			$new_name =time() .'.'. $path_parts['extension'];
			$config['upload_path'] = './images/empleados/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name'] = $new_name;
			$config['max_size'] = '10000';
			$config['max_width'] = '1920';
			$config['max_height'] = '1280';
			$nombre=$new_name;
			$idImagen;
			
			$this->load->library('upload', $config);
			
			
			if(!$this->upload->do_upload()){echo $this->upload->display_errors();}
			else {}
			
			
			
			
			
			$usuario = array(
						'sus_name'=>ucwords(strtolower($this->input->post('nombre'))),
						'sus_lastName'=>ucwords(strtolower($this->input->post('apellido'))),
						'sus_telephone'=>$this->input->post('telefono'),
						'sus_employeeNumber'=>$this->input->post('numEmpleado'),
						'sus_adviserNumber'=>$this->input->post('numAsesor'),
						'sus_dtps'=>$this->input->post('dtps'),
						'sus_email'=>$this->input->post('correo'),
						'sus_password'=>md5($this->input->post('contrasena')),
						'sus_userType'=>$this->input->post('tipoUsuario'),
						'sus_dateAdded'=>date("Y-m-d H:i:s"),
						'sus_rol'=>$this->input->post('rol'),
						'sus_status'=>'1',
						'sus_workshop'=>$this->input->post('taller'),
						'sus_foto'=>$nombre,
						'sus_orden'=>$this->input->post('orden'),
						'sus_usuario_intelisis'=>$this->input->post('uin'),
						);
				

						
		   	$this->Usuariomodel->agregarUsuario($usuario);
			$this->Usuariomodel->emailBienvenida($toemail,$nombre,$apellido,$correo,$con);
			$this->session->set_flashdata('message', " toastr[success]('a', 'Toastr Notifications'); ");
			
			redirect('usuario');
        }
        else
        {
			$data['tiposUsuario']=$this->Usuariomodel->obtenerTipos();
			$data['roles']=$this->Usuariomodel->obtenerRoles();
			$data['talleres']=$this->Usuariomodel->obtenerTalleres();
            $this->load->view('usuario/crear',$data);
        }	
	}
	
	function editar($id){
	 	$data['usuario'] = $this->Usuariomodel->obtenerUsuario($id);
		
		if($data['usuario']){
			$data['flash_message'] = $this->session->flashdata('message');
			$data['tiposUsuario']=$this->Usuariomodel->obtenerTipos();
			$data['roles']=$this->Usuariomodel->obtenerRoles();
			$data['talleres']=$this->Usuariomodel->obtenerTalleres();
			
			$this->load->view('usuario/editar', $data);
		} else{
			redirect('error');
		}
	}
	
	function equipos(){
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}		
	 	$data['usuario'] = $this->Usuariomodel->obtenerAsesores($taller);
		
		if($data['usuario']){
			$data['flash_message'] = $this->session->flashdata('message');
			$data['equipos'] = $this->Usuariomodel->obtenerEquipos();
			
			$this->load->view('usuario/equipos/vista', $data);
		} else{
			redirect('error');
		}
	}
	
	function equiposEditar($id){
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}	
	 	$data['usuario'] = $this->Usuariomodel->obtenerUsuario($id);
		
		if($data['usuario']){
			$data['flash_message'] = $this->session->flashdata('message');
			$data['lista'] = $this->Usuariomodel->obtenerlEquipos($id);
			$data['tecnicos'] = $this->Usuariomodel->obtenerAsesores($taller);
			
			$this->load->view('usuario/equipos/editar', $data);
		} else{
			redirect('error');
		}
	}
	
	function updateEquipo(){
	 	
		
		list($idAse,$idTec)=explode('-',$_POST['id']);
		if($_POST['eve']=='add'){
			
			$data['exite'] = $this->Usuariomodel->getEquipoId($idAse,$idTec);
			if($data['exite']){}
			else{
		$this->Usuariomodel->AgregarEquipo($idAse,$idTec);		
				}
			}
		else{
		$this->Usuariomodel->eliminarEquipo($idAse,$idTec);	
			}
		
		
		
		
	}
	
	function actualizar($id){
	
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
		$this->form_validation->set_rules('telefono', 'Telefono', 'required');
		$this->form_validation->set_rules('numEmpleado', 'N�mero de empleado');
		$this->form_validation->set_rules('numAsesor', 'N�mero de asesor');
		$this->form_validation->set_rules('dtps', 'DTPS');
		$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('nuevaContrasena', 'Contrase�a');
		$this->form_validation->set_rules('contrasena', 'Contrase�a');
		$this->form_validation->set_rules('tipoUsuario', 'Tipo de usuario', 'required');
		$this->form_validation->set_rules('rol', 'Rol', 'required');
		$this->form_validation->set_rules('taller', 'Taller', 'required');
		
		
		
 
        if ($this->form_validation->run())
        {
			
			$idImagen;
			$tmp_new_name = $_FILES["userfile"]["name"];
			if($tmp_new_name!=''){
				$tmp_new_name = $_FILES["userfile"]["name"];
				$path_parts = pathinfo($tmp_new_name);
				$new_name =time() .'.'. $path_parts['extension'];
				$config['upload_path'] = './images/empleados/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name'] = $new_name;
				$config['max_size'] = '10000';
				$config['max_width'] = '1920';
				$config['max_height'] = '1280';
				$nombre=$new_name;
				
				$this->load->library('upload', $config);
				
				if(!$this->upload->do_upload()){echo $this->upload->display_errors();}
				else {
					$idImagen = $nombre;
					}
			
			} else{
				 $idImagen = $this->input->post('imagen');
			}
			
			
			
			$password =  $this->input->post('nuevaContrasena');
			if($password==''){
				$password = $this->input->post('contrasena');
			} else{
				$password = md5($this->input->post('nuevaContrasena'));
			}
			
			$usuario = array(
						'sus_name'=>$this->input->post('nombre'),
						'sus_lastName'=>$this->input->post('apellido'),
						'sus_telephone'=>$this->input->post('telefono'),
						'sus_employeeNumber'=>$this->input->post('numEmpleado'),
						'sus_adviserNumber'=>$this->input->post('numAsesor'),
						'sus_dtps'=>$this->input->post('dtps'),
						'sus_email'=>$this->input->post('correo'),
						'sus_password'=>$password,
						'sus_userType'=>$this->input->post('tipoUsuario'),
						'sus_rol'=>$this->input->post('rol'),
						'sus_status'=>'1',
						'sus_workshop'=>$this->input->post('taller'),
						'sus_foto'=>''.$idImagen.'',
						'sus_orden'=>''.$this->input->post('orden').'',
						'sus_usuario_intelisis'=>''.$this->input->post('uin').''
						);

						
		   	$this->Usuariomodel->actualizarUsuario($usuario,$id);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable">
<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
<strong>Hecho!</strong>
La informaci�n fue actualizada con exito.
</div>'); 
			redirect('usuario');
		}
        else
        {
			redirect('usuario/editar',$id);
		}	
		
	}
	
	function desactivar($id){
	 	$this->Usuariomodel->desactivarUsuario($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable">
<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
<strong>Hecho!</strong>
Ha cambiado de estatus.
</div>'); 
		redirect('usuario');
	}
	
	function activar($id){
	 	$this->Usuariomodel->activarUsuario($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable">
<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
<strong>Hecho!</strong>
Ha cambiado de estatus.
</div>'); 
		redirect('usuario');
	}
	
	function borrar($id){
	 	$this->Usuariomodel->borrarUsuario($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable">
<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
<strong>Hecho!</strong>
Se ha eliminado el usuario.
</div>'); 
		redirect('usuario');
	}
	
	

	
}