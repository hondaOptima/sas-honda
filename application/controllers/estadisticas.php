<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estadisticas extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Estadisticasmodel');
		$this->load->model('Productividadmodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index(){
		if(empty($_GET['fecha'])){
			$fecha=date('Y-m-d');
			}
		else{
			$fecha=$_GET['fecha'];
			}
			
			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}	
			
			
		function week_bounds($date, &$start, &$end) {
    $date = strtotime($date);
    $start = $date;
    while( date('w', $start)>1 ) {
        $start -= 86400;
    }
    $end = date('Y-m-d', $start + (6*86400) );
    $start = date('Y-m-d', $start);
}

week_bounds("".$fecha."", $start, $end);
//echo $start."<br>".$end;	
		$data['inicio']=$start;	
		$data['fecha']=$fecha;	
		$data['taller']=$taller;
		$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
		if(empty($_GET['asesor'])){$ase=0;}else{$ase=$_GET['asesor'];}
		$data['lista'] = $this->Productividadmodel->obtenerEquipos($taller,$start,$end);
		$data['os'] = $this->Productividadmodel->obtenerOS($taller,$start,$end,$ase);
		$data['cos'] = $this->Productividadmodel->getLista($taller,$start,$end,$ase);
		$data['ossv'] = $this->Productividadmodel->obtenerOSsv($taller,$start,$end,$ase);
		$data['tecnicos'] = $this->Productividadmodel->obtenerEquipos($taller,$start,$end);
		$data['capacidades'] = $this->Productividadmodel->listarCapacidades(0);
		
		$this->load->view('estadisticas/vista',$data);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

?>