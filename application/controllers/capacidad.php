<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Capacidad extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Capacidadmodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		$data['capacidades'] = $this->Capacidadmodel->listarCapacidades($_SESSION['sfidws']);
		$this->load->view('capacidad/vista', $data);
	}
	
	function horaswd($cd)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['horaswd'] = $this->Capacidadmodel->horaswd($cd);
		$this->load->view('capacidad/horaswd', $data);
	}
	
	function horaswe($cd)
	{  $data['flash_message'] = $this->session->flashdata('message');
		$data['horaswd'] = $this->Capacidadmodel->horaswe($cd);
		$this->load->view('capacidad/horaswe', $data);
	}
	
	function editar($id)
	{	
		$data['capacidad'] = $this->Capacidadmodel->obtenerCapacidad($id);
		
		if($data['capacidad'])
		{
			$this->load->view('capacidad/editar',$data);
		} 
		else
		{
			redirect('error');
		}
	}
	
	
	public function actualizar($id)
	{
        $this->form_validation->set_rules('weekdays', 'D�as de Lunes a Viernes', 'required');
		$this->form_validation->set_rules('weekends', 'S�bados', 'required');
		$this->form_validation->set_rules('factor', 'Factor de productividad', 'required');
		$this->form_validation->set_rules('tecnicos', 'T&eacute;cnicos', 'required');
		
		
        if ($this->form_validation->run())
        {
			$capacidad = array(
						'woc_weekdays_service_hours'=>$this->input->post('weekdays'),
						'woc_weekends_service_hours'=>$this->input->post('weekends'),
						'woc_tecnicos'=>$this->input->post('tecnicos'),
						'woc_productivity_factor'=>$this->input->post('factor'),
						);

						
		   	$this->Capacidadmodel->actualizarCapacidad($capacidad, $id);
			
			redirect('capacidad');
		} 
		else
		{
			redirect('capacidad/editar',$id);
		}
	}
	
	
	
	function eliminarHora($id,$taller)
	{   $x=1;
		$this->Capacidadmodel->eliminarHora($id);
		
$this->session->set_flashdata('message',"<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'></button>Hora Eliminada</div>");
		redirect('capacidad/horaswd/'.$taller.'');
	}
	
	function eliminarHoraE($id,$taller)
	{   $x=1;
		$this->Capacidadmodel->eliminarHora($id);
		
$this->session->set_flashdata('message',"<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'></button>Hora Eliminada</div>");
		redirect('capacidad/horaswe/'.$taller.'');
	}
	
	
	
	
	function agregarHora()
	{
        $this->form_validation->set_rules('taller', '', 'required');
		 if ($this->form_validation->run())
        {
			$info= array(
						'hor_hora'=>$this->input->post('nhora').':'.$this->input->post('nmin'),
						'hor_wor_idWorkshop'=>$this->input->post('taller'),
						'hor_dia'=>$this->input->post('dia')
						);

						
		   	$this->Capacidadmodel->agregarHora($info);
			
			$this->session->set_flashdata('message',"<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'></button>Hora Agregada con Exito !</div>");
			
			if($this->input->post('dia')==1){
			    redirect('capacidad/horaswd/'.$this->input->post('taller').'');
			}
			else{
				redirect('capacidad/horaswe/'.$this->input->post('taller').'');
				}
		
		} 
		
	}
	
	
	
	
	
	
}