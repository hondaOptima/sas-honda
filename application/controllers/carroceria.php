<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carroceria extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model('torresmodel','torre');
        $this->load->library('session');
		$this->load->library('curl');
        $this->load->model('Permisomodel');
        $this->load->model('Carroceriamodel','carroceria');
        $this->load->helper('url');
        $this->load->helper('dates');
		$this->per = $this->Permisomodel->acceso();
	}

	public function index()
    {
        $fecha = (empty($_GET["fecha"])) ? date("Y-m-d") : $_GET["fecha"];
        $this->load->model('Tallermodel',"taller");
        $data['flash_message'] = $this->session->flashdata('message');
        $data["fecha"] = $fecha;
        $data["fecha_completa"] = fecha_completa_larga($fecha);
        $data["taller"] = $this->taller->get_taller($_SESSION["sfidws"]);
		$this->load->view('carroceria/index',$data);
	}

	function ver(){ print_r($_SESSION); }

	function get_carroceria()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		if(!empty($_POST["id"]))
		{
			$fecha_vacia = "0000-00-00 00:00:00";
			extract($_POST);
			$carroceria = $this->carroceria->get_carroceria($id);
			if(empty($carroceria->fecha_enviado) || $carroceria->fecha_enviado == $fecha_vacia)  $carroceria->fecha_enviado = "";
			if(empty($carroceria->fecha_recibido) || $carroceria->fecha_recibido == $fecha_vacia) $carroceria->fecha_recibido = "";
			if(empty($carroceria->fecha_autorizada) || $carroceria->fecha_autorizada == $fecha_vacia) $carroceria->fecha_autorizada = "";
			$response = $carroceria;
		}
		else $response["error"] = "No se recibió el id";
		echo json_encode($response);
	}

	function get_carroceria_espera()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		$resultado = $this->carroceria->get_carrocerias('0');
		$fecha_vacia = "0000-00-00 00:00:00";

		$i = 0;
		$data["data"] = array();
		foreach($resultado AS $r)
		{
			$a = array();
			$a[] = ++$i;
			$a[] = (int)$r->id;
			$a[] = $r->tipo;
			$a[] = $r->tipo_descripcion;
			$a[] = $r->vin;
			$a[] = $r->modelo." ".$r->anio;
			$a[] = $r->nombre_cliente;
			$a[] = $r->poliza;
			$a[] = $r->siniestro;
			$a[] = (!empty($r->fecha_recibido) && $r->fecha_recibido != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_recibido))) : '';
			$a[] = (!empty($r->fecha_recibido) && $r->fecha_recibido != $fecha_vacia) ? $r->fecha_recibido : "";
			$a[] = (!empty($r->fecha_recibido) && $r->fecha_recibido != $fecha_vacia) ? date('h:i A', strtotime($r->fecha_recibido)) : "";

			$a[] = (!empty($r->fecha_enviado) && $r->fecha_enviado != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_enviado))) : '';
			$a[] = (!empty($r->fecha_enviado) && $r->fecha_enviado != $fecha_vacia) ? $r->fecha_enviado : "";
			$a[] = (!empty($r->fecha_enviado) && $r->fecha_enviado != $fecha_vacia) ? date('h:i A', strtotime($r->fecha_enviado)) : "";

			$a[] = (!empty($r->fecha_tentativa_entrega) && $r->fecha_tentativa_entrega != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_tentativa_entrega))) : '';
			$a[] = (!empty($r->fecha_tentativa_entrega) && $r->fecha_tentativa_entrega != $fecha_vacia) ? $r->fecha_tentativa_entrega : "";
			$a[] = (!empty($r->fecha_tentativa_entrega) && $r->fecha_tentativa_entrega != $fecha_vacia) ? date('h:i A', strtotime($r->fecha_tentativa_entrega)) : "";

			$a[] = $r->refacciones_index;
			$a[] = $r->refaccion;
			$a[] = "";
			$a[] = "";
			$a[] = "";
			$data["data"][] = $a;
		}
		echo json_encode($data);
	}

	function get_carroceria_autorizados()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		$resultado = $this->carroceria->get_carrocerias('1');
		$fecha_vacia = "0000-00-00 00:00:00";

		$i = 0;
		$data["data"] = array();
		foreach($resultado AS $r)
		{
			$a = array();

			$a[] = ++$i;
			$a[] = (int)$r->id;
			$a[] = $r->tipo;
			$a[] = $r->tipo_descripcion;
			$a[] = (!empty($r->fecha_autorizada) && $r->fecha_autorizada != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_autorizada))) : '';
			$a[] = $r->vin;
			$a[] = $r->modelo." ".$r->anio;
			$a[] = $r->nombre_cliente;
			$a[] = $r->poliza;
			$a[] = $r->siniestro;

			$a[] = (!empty($r->fecha_recibido) && $r->fecha_recibido != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_recibido))) : '';
			$a[] = (!empty($r->fecha_recibido) && $r->fecha_recibido != $fecha_vacia) ? $r->fecha_recibido : "";
			$a[] = (!empty($r->fecha_recibido) && $r->fecha_recibido != $fecha_vacia) ? date('h:i A', strtotime($r->fecha_recibido)) : "";

			$a[] = (!empty($r->fecha_enviado) && $r->fecha_enviado != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_enviado))) : '';
			$a[] = (!empty($r->fecha_enviado) && $r->fecha_enviado != $fecha_vacia) ? $r->fecha_enviado : "";
			$a[] = (!empty($r->fecha_enviado) && $r->fecha_enviado != $fecha_vacia) ? date('h:i A', strtotime($r->fecha_enviado)) : "";

			$a[] = (!empty($r->fecha_tentativa_entrega) && $r->fecha_tentativa_entrega != $fecha_vacia) ? fecha_completa_corta(date('Y-m-d', strtotime($r->fecha_tentativa_entrega))) : '';
			$a[] = (!empty($r->fecha_tentativa_entrega) && $r->fecha_tentativa_entrega != $fecha_vacia) ? $r->fecha_tentativa_entrega : "";
			$a[] = (!empty($r->fecha_tentativa_entrega) && $r->fecha_tentativa_entrega != $fecha_vacia) ? date('h:i A', strtotime($r->fecha_tentativa_entrega)) : "";

			$a[] = $r->refacciones_index;
			$a[] = $r->refaccion;

			$a[] = $r->talleres_index;
			$a[] = $r->taller;
			$a[] = $r->deducible;

			$a[] = "Cita";

		 	$data["data"][] = $a;
		}
		echo json_encode($data);
	}

	function update_carroceria()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		if(!empty($_POST["id"]))
		{
			if(intval($_SESSION["sfrol"]) == 8)//8 es el ID del rol para las personas encargadas de carroceria.
			{
				extract($_POST);
				$datos = array();

				if(!empty($tipo)) $datos["car_tipo"] = $tipo;
				if(!empty($fecha_recibido)) $datos["car_fecha_recibido"] = $fecha_recibido;
				if(!empty($fecha_enviado)) $datos["car_fecha_enviado"] = $fecha_enviado;
				if(!empty($fecha_tentativa)) $datos["car_fecha_tentativa_entrega"] = $fecha_tentativa;
				if(!empty($fecha_autorizada)) $datos["car_fecha_autorizada"] = $fecha_autorizada;
				if(isset($poliza)) $datos["car_poliza"] = $poliza;
				if(isset($siniestro)) $datos["car_siniestro"] = $siniestro;
				if(!empty($taller)) $datos["car_taller"] = $taller;
				if(!empty($autorizado)) $datos["car_autorizado"] = $autorizado;
				if(isset($activo)) $datos["car_activo"] = intval($activo);
				if(isset($refaccion)) $datos["car_refacciones"] = $refaccion;
				if(isset($deducible))
				{
					if($deducible == "") $datos["car_deducible"] = null;
					else $datos["car_deducible"] = $deducible;
				}
				$response["datos"] = $datos;
				$response["id_datos"] = $id;
				$carroceria_before = $this->carroceria->get_carroceria($id);
				unset($carroceria_before->refacciones_index);
				unset($carroceria_before->talleres_index);
				unset($carroceria_before->tipo);

				if($this->carroceria->update_carroceria($id,$datos))
				{
					$response["success"] = "Datos actualizados";
					$response["data"] = $datos;
					$str = ""; $primero = true;

					$carroceria_after = $this->carroceria->get_carroceria($id);
					unset($carroceria_after->refacciones_index);
					unset($carroceria_after->talleres_index);
					unset($carroceria_after->tipo);

					foreach($carroceria_after AS $k => $p)
					{
						if($carroceria_before->{$k} != $p)
						{
							if($primero) $primero = false; else $str.=", ";
							$str.=$k.": ".$p;
						}
					}

					$bitacora = array(
						"cb_id_carroceria"	=> $id,
						"cb_id_susers"		=> $_SESSION["sfid"],
						"cb_bitacora"		=> "Actualizó: ".$str,
						"cb_fecha"			=> date("Y-m-d H:i:s")
					);

					if($this->carroceria->agregar_bitacora($bitacora)) $response["bitacora_status"] = true;
					else $response["bitacora_status"] = false;
				}
				else $response["error"] = "No se pudieron actualizar los datos";
			} else $response["error"] = "Disculpe las molestias, no tiene autorización para realizar esta operación";
		}
		else $response["error"] = "No se recibió el id del registro";
		echo json_encode($response);
	}

	function add_carroceria_comment()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		if(!empty($_POST["id"]) && !empty($_POST["comentario"]))
		{
			if(intval($_SESSION["sfrol"]) == 8)
			{
				extract($_POST);

				$comentario = array(
					"cb_id_carroceria"	=> $id,
					"cb_id_susers"		=> $_SESSION["sfid"],
					"cb_bitacora"		=> $comentario,
					"cb_fecha"			=> date("Y-m-d H:i:s"),
					"cb_is_comment"		=> 1
				);

				if($this->carroceria->agregar_bitacora($comentario)) $response["success"] = "Comentario registrado";
				else $response["error"] = "No se pudo registrar el comentario";
			}
			else $response["error"] = "Disculpe las molestias, no tiene autorización para realizar esta operación";

		}
		else $response["error"] = "No se recibieron los parámetros";
		echo json_encode($response);
	}

	function get_comments($id = null)
	{
		header('Content-Type: application/json; charset=utf-8');

		$data["data"] = array();

		if($id != null)
		{
			$comments = $this->carroceria->get_comments($id);
			$i = 0;
			foreach($comments AS $r)
			{
				$a = array();

				$a[] = ++$i;
				$a[] = $r->id;
				$a[] = $r->id_carroceria;
				$a[] = $r->bitacora;
				$a[] = fecha_completa_larga(date('Y-m-d', strtotime($r->fecha)))." ".date('h:i A', strtotime($r->fecha));//aqui
				$a[] = $r->nombre;

				$data["data"][] = $a;
			}
		}

		echo json_encode($data);
	}

	function get_tipos_carroceria()
	{
		header('Content-Type: application/json; charset=utf-8');
		$items = $this->carroceria->get_enum_field('carroceria','car_tipo');//Tabla, campo.
		$arr = array(); $i = 0;

		foreach($items AS $it)
		{
			if($it == "Interno") $color = "btn-success";
			else if($it == "Particulares") $color = "btn-warning";
			else if($it == "Por Seguro") $color = "btn-primary";

			$arr[] = array("id" => ++$i, "descripcion" => $it, "color" => $color);
		}
		echo json_encode($arr);
	}

	function get_refacciones_carroceria()
	{
		header('Content-Type: application/json; charset=utf-8');
		$items = $this->carroceria->get_enum_field('carroceria','car_refacciones');//Tabla, campo.
		$arr = array(); $i = 0;
		$colores = array("btn-warning","btn-primary","btn-success");
		foreach($items AS $it)
			$arr[] = array("color" => $colores[$i],"id" => ++$i, "descripcion" => $it);
		echo json_encode($arr);
	}

	function get_carroceria_data_status()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		$tipos = $this->carroceria->get_enum_field('carroceria','car_tipo');//Tabla, campo.
		$arr = array(); $i = 0;

		foreach($tipos AS $it)
		{
			if($it == "Interno") $color = "btn-success";
			else if($it == "Particulares") $color = "btn-warning";
			else if($it == "Por Seguro") $color = "btn-primary";

			$arr[] = array("id" => ++$i, "descripcion" => $it, "color" => $color);
		}

		$response["tipos"] = $arr;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$refacciones = $this->carroceria->get_enum_field('carroceria','car_refacciones');//Tabla, campo.
		$arr = array(); $i = 0;
		$colores = array("btn-warning","btn-primary","btn-success");
		foreach($refacciones AS $it) $arr[] = array("color" => $colores[$i],"id" => ++$i, "descripcion" => $it);
		$response["refacciones"] = $arr;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$talleres = $this->carroceria->get_enum_field('carroceria','car_taller');//Tabla, campo.
		$arr = array(); $i = 0;
		$colores = array("btn-warning","btn-primary","btn-success");
		foreach($talleres AS $it) $arr[] = array("color" => $colores[$i],"id" => ++$i, "descripcion" => $it);
		$response["talleres"] = $arr;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$arr = array(array("descripcion" => "Si", "id" => 1, "color" => "btn-success"),array("descripcion" => "No", "id" => 0, "color" => "btn-warning"));
		$response["deducibles"] = $arr;
		echo json_encode($response);
	}


















	function vco()
	{
		error_reporting(E_ALL);
		$this->load->model("Torresmodel","vco");
		$vcos = "229697,229698,229699,229704,229705,229706,229707,229708,229709,229952,229953,229954,229955,229957,229958,229959,229960,229961,229962,229963,229964,230000,230105,230106,230107,230108,230109,230110,230111,230112,230113,230114,230116,230117,230118,230119,230120,230121,230122,230123,230124";
		$vcos = explode(",",$vcos);
		$i = 0;
		foreach($vcos AS $v)
		{
			//echo ++$i." | ".$v."<br>";
			$r = $this->vco->paso_1($v);
			echo "-- REGISTRO ".++$i."<br><br>";
			echo $r."<br> -- ---------------------------------------------------------------------------------------------------------<br>";
		}
	}

	function preparar_garantias()
	{
		//header('Content-Type: text/html; charset=utf-8;');
		$ruta = base_url()."ejemplo/garantias.csv";

		$handle = fopen($ruta, "r");
		if ($handle)
		{
			while (($line = fgets($handle)) !== false) {
				//echo utf8_encode($line); echo "<br>";
				$l = explode(",",$line);
				//print_r($l);

				$s = "INSERT INTO garantias (`COL 1`,inicio,`COL 3`,km_status,fecha_vigencia,vin,`COL 7`,`COL 10`,`COL 11`)<br>";
				//$s.= " VALUES ('".$l[4]."','".$l[6]."','".$l[7]."','".$l[5]."','".$l[8]."','".$l[0]."','".utf8_encode($l[1])."','".$l[2]."','".$l[3]."');"."<br><br>";
				$s.= " VALUES ('".$l[6]."','".$l[8]."','".$l[9]."','".$l[7]."','".$l[10]."','".$l[2]."','".
						utf8_encode($l[3])."','".$l[4]."','".$l[5]."');"."<br><br>";
				echo $s;
			}

			fclose($handle);
		}
		else
		{
			echo "No se abrió el documento.";
		}

		// $registros = $this->torre->preparar_garantias();
		// $primero = true;
		// foreach($registros AS $r) {
		// 	if($primero) $primero = false;
		// 	else{
		// 		$s = "INSERT INTO garantias (`COL 1`,inicio,`COL 3`,km_status,fecha_vigencia,vin,`COL 7`,`COL 10`,`COL 11`)<br>";
		// 		$s.= " VALUES ('".$r->{'COL 3'}."','".$r->{'COL 7'}."', '".$r->{'COL 9'}."', '".$r->{'COL 8'}."', '".$r->{'COL 10'}."', '".$r->{'COL 14'}."', '".$r->{'COL 4'}."', '".$r->{'COL 5'}."', '".$r->{'COL 6'}."');<br><br>";
		// 		echo $s;
		// 	}
		// }
	}
}
