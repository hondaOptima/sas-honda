<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pizarronu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pizarronmodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");;
		
		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
									
	}
	
	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		
		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}	
		
		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		$this->load->view('pizarronu/vistac',$data);
	}
	public function index_inv()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		
		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}	
		
		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		$this->load->view('pizza/vistac',$data);
	}
	function cambiardia(){
		//echo .' '.;
		
		// $this->Pizarronmodel->updateDia($_POST['ido'],$_POST['fecha']);
		echo $_POST['ido']." ".$_POST['fecha'];
		// $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Torre Cambiada de Fecha.</div>');
		// list($an,$ms,$dd)=explode("-",$_POST['fecha']);
		// $fecha=$dd.'-'.$ms.'-'.$an;
		// redirect('pizza?fecha='.$fecha.'');

		
		}
	
	function update()
	{
		//Actualiza estatus en el Pizarron
		$this->Pizarronmodel->update($_GET['id'],$_GET['campo'],$_GET['value']);
		
		//Trae informacion de la Orden
		$info=$this->Pizarronmodel->getOrdenVehi($_GET['id']);
		//Si tiene Emial ENvia Email
		// $info[0]->app_reminderType;
		if($info[0]->app_reminderType==2 || $info[0]->app_reminderType==4)
		{
			if($info[0]->piz_status_mecanico==5 && $info[0]->piz_status_lavado==5){
				$auto=$info[0]->sev_marca.' '.$info[0]->sev_sub_marca.' '.$info[0]->sev_version.' '.$info[0]->sev_color.' '.$info[0]->sev_modelo;	
		$this->Pizarronmodel->emailRemider($_GET['value'],$info[0]->app_customerEmail,$info[0]->piz_status_mecanico,$info[0]->piz_status_lavado,$info[0]->piz_ID_orden,$info[0]->seo_promisedTime,$auto);
				}
			else{
		
			}
		}
		//si tiene SMS Envia SMS
				if($info[0]->app_reminderType==3 || $info[0]->app_reminderType==4)
		{
			if($info[0]->piz_status_mecanico==5 && $info[0]->piz_status_lavado==5){
				function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
				$tel=clean($info[0]->app_customerTelephone);
				$tel=preg_replace("/[^0-9]/","",$tel);
				if($_SESSION['sfworkshop']=='Tijuana'){}
				else{
				$this->Pizarronmodel->SmsBienvenida($tel);
				}}
		}
		
    //si el estatus es 8 se modificara la base de datos de autos de CRM autos_en_pisoe invetario    
    // a y traves de CURL
      if($_GET['value']==8){
    $url = "".base_url()."ajax/inventario/inv_status_update.php"; 
    $post_data = array ( 
    "status" => $_GET['value'], 
    "vin" =>$_GET['vin'] );
    $this->curl->simple_post($url, $post_data);	
        }
		
		if($_GET['value']> 8){
    $url = "".base_url()."ajax/inventario/inv_status_ubicacion.php"; 
    $post_data = array ( 
    "ubicacion" => $_GET['value'], 
    "vin" =>$_GET['vin'] );
    $this->curl->simple_post($url, $post_data);	
        }
        
        
        
        
        
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Status Modificado</div>');
		redirect('pizarron?fecha='.$_GET['fecha'].'');
	}
	
	function updatePizarronInventario(){
	list($torre,$id)=explode("/",$_GET['valor']);
	$datos=array('pii_torre'=>$torre);
	$this->Pizarronmodel->updatePizarronInventario($datos,$id);		
	}
	
	function pasaraPreviado($tecnico,$vin){

	$datos=array(
	'pii_estado'=>'Asignado',
	'pii_tecnico'=>$tecnico
	);
	$this->Pizarronmodel->updatePizarronInventario($datos,$vin);
	}
	
}