<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Torre extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Permisomodel');
		$this->load->model('torresmodel','torre');
		$this->load->helper('url');
		$this->load->library('session');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	}

	function get_torres()
    {
        header('Content-Type: application/json; charset=utf-8');
        $a = $this->torre->get_torres($_SESSION["sfworkshop"]);//Agencia de la sesion.
        echo json_encode($a);
    }

	function update_torre()
	{
		header('Content-Type: application/json; charset=utf-8');
		if(!empty($_GET["torre"]) && !empty($_GET["agencia"]) && !empty($_GET["vin"]))
		{
			$r = $this->torre->update_torre();
			$json = ($r == 1) ? '{"success" : "Se ha actualizado correctamente la torre"}' : '{"error" : "No se ha podido actualizar la torre"}';
			echo $json;
		}
		else echo '{"error" : "No se recibió id de la torre, la agencia o el vin"}';
	}

	function ver()
	{
		print_r($_SESSION);
	}
}
