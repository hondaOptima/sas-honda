<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocion extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Promocionmodel');
		$this->load->model('Imagenmodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['promociones']=$this->Promocionmodel->listarPromociones($this->per);
		$this->load->view('promocion/vista',$data);
	}
	
	public function crear()
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');
		$this->form_validation->set_rules('inicio', 'Fecha de Inicio', 'required');
		$this->form_validation->set_rules('final', 'Fecha de Vencimiento', 'required');
		
        if ($this->form_validation->run())
        {
			$tmp_new_name = $_FILES["userfile"]["name"];
		 	$path_parts = pathinfo($tmp_new_name);
			$new_name =time() .'.'. $path_parts['extension'];
			$config['upload_path'] = './images/promotions/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name'] = $new_name;
			$config['max_size'] = '10000';
			$config['max_width'] = '1920';
			$config['max_height'] = '1280';
			$nombre=$new_name;
			$idImagen;
			
			$this->load->library('upload', $config);
			
			
			if(!$this->upload->do_upload()){echo $this->upload->display_errors();}
			else {
				$imagen = array('ima_name'=>$nombre);
				$idImagen = $this->Imagenmodel->agregarImagen($imagen);
				echo $idImagen;
			}
		
			
			$promocion = array(
						'pro_name'=>$this->input->post('nombre'),
						'pro_description'=>$this->input->post('descripcion'),
						'pro_startDate'=>$this->input->post('inicio'),
						'pro_endDate'=>$this->input->post('final'),
						'Images_ima_idImage'=>$idImagen,
						);
			
		   	$this->Promocionmodel->agregarPromocion($promocion);
			redirect('promocion');
		} 
		else
		{
			$this->load->view('promocion/crear');
		}
	}
	
	public function editar($id)
	{
	 	$data['promocion'] = $this->Promocionmodel->obtenerPromocion($id);
		
		if($data['promocion'])
		{
			$this->load->view('promocion/editar', $data);
		} 
		else
		{
			redirect('error');
		}
	}
	
	public function actualizar($id)
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');
		$this->form_validation->set_rules('inicio', 'Fecha de Inicio', 'required');
		$this->form_validation->set_rules('final', 'Fecha de Vencimiento', 'required');
		
        if ($this->form_validation->run())
        {
			
			$idImagen;
			$tmp_new_name = $_FILES["userfile"]["name"];
			if($tmp_new_name!=''){
				$tmp_new_name = $_FILES["userfile"]["name"];
				$path_parts = pathinfo($tmp_new_name);
				$new_name =time() .'.'. $path_parts['extension'];
				$config['upload_path'] = './images/promotions/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name'] = $new_name;
				$config['max_size'] = '10000';
				$config['max_width'] = '1920';
				$config['max_height'] = '1280';
				$nombre=$new_name;
				
				$this->load->library('upload', $config);
				
				if(!$this->upload->do_upload()){echo $this->upload->display_errors();}
				else {
					$imagen = array('ima_name'=>$nombre);
					$idImagen = $this->Imagenmodel->agregarImagen($imagen);
					unlink(realpath('images/promotions/'.$this->input->post('nombreImagen')));
					$this->Imagenmodel->borrarImagen($this->input->post('imagen'));
					}
			
			} else{
				echo $idImagen = $this->input->post('imagen');
			}
			
			$promocion = array(
						'pro_name'=>$this->input->post('nombre'),
						'pro_description'=>$this->input->post('descripcion'),
						'pro_startDate'=>$this->input->post('inicio'),
						'pro_endDate'=>$this->input->post('final'),
						'Images_ima_idImage'=>$idImagen,
						);

		   	$this->Promocionmodel->actualizarPromocion($promocion, $id);
			redirect('promocion');
		} 
		else
		{
			redirect('promocion/editar',$id);
		}
	}
	
	function desactivar($id){
	 	$this->Promocionmodel->desactivarPromocion($id);
		redirect('promocion');
	}
	
	function eliminar($id){
	 	$this->Promocionmodel->eliminarPromocion($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> ha Eliminado una Promocion.</div>');
		redirect('promocion');
	}
	
	function activar($id){
	 	$this->Promocionmodel->activarPromocion($id);
		redirect('promocion');
	}
	
	public function subirImagen($archivo)
	{
		echo 'este es el archivo: '.$archivo;
		$path_parts = pathinfo($archivo);
		$new_name =time() .'.'. $path_parts['extension'];
		$config['upload_path'] = './images/promotions/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name'] =$new_name;
		$config['max_size'] = '10000';
		$config['max_width'] = '1920';
		$config['max_height'] = '1280';
		$nombre=$new_name;
		
		$this->load->library('upload', $config);
		if($this->upload->do_upload())
		{
			$imagen = array('ima_nombre'=>$nombre);
			$idImagen = $this->Imagenmodel->agregarImagen($imagen);
			return $idImagen;
		}
		return '0';
	}
	
	
}