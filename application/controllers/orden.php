<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orden extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ordendeserviciomodel');


		$this->load->model('Imagenmodel');
		$this->load->model('Citamodel');
		$this->load->model('Permisomodel');
		$this->load->model('Productividadmodel');
		$this->load->library('curl');
		$this->load->library('uri');
		$this->load->helper('url');
		$this->load->helper('dates');
		$this->load->library('pdf');
		//$this->load->library('email');
		if($this->uri->segment(1) != "get_customers_oda" && $this->uri->segment(1) != "get_campanias" && $this->uri->segment(1) != "camp_exist"){
			$this->load->library('session');
			//$this->load->library('form_validation');
			session_start();
			if(!isset($_SESSION['sfid'])) {redirect('acceso');}
			$this->sfid = $_SESSION['sfid'];
			$this->sfemail = $_SESSION['sfemail'];
			$this->sfname = $_SESSION['sfname'];
			$this->sfrol = $_SESSION['sfrol'];
			$this->sftype = $_SESSION['sftype'];
			$this->sfworkshop = $_SESSION['sfworkshop'];
		}
	}

	function saveespera()//71925.
	{
		//Revisar si no existe la orden de servicio.
		$res = $this->Ordendeserviciomodel->verInsertOrden($_GET['idapp']);

		if(!empty($res[0]->seo_date))
			redirect('clientes');
		else
		{
			//Si no existe insertamos una nueva con el id de la cita.
			$IDO = $this->Ordendeserviciomodel->insertOrden($_GET['idapp'],$_SESSION['sfid']);

			$info = array(
				'enp_ID_asesor' => $_SESSION['sfid'],
				'enp_ID_cita' => $_GET['idapp'],
				'enp_ID_proceso' => $IDO,
				'enp_status' => 1
			);

			$info2= array('app_status' => 1);

			//Guardar en lista de espera.
			$this->Ordendeserviciomodel->listadeespera($info);
			//cambiar el estatus de la cita a cita en revision
			$this->Ordendeserviciomodel->updateCita($info2,$_GET['idapp']);

			redirect('clientes');

		}
	}


		function saveesperaB(){

		$IDO=$this->Ordendeserviciomodel->insertOrdenB($_GET['idapp'],$_SESSION['sfid']);

		redirect('clientes');

		}

		function getExtGarantia()
		{
			$R=$this->Ordendeserviciomodel->getExtGarantiaSta($_GET['vin']);
 			if($R)
			{
				//list($mes,$dia,$anio)=explode("/",$R[0]->fecha_vigencia);
				$m='';
				$mes=1;
				//modificar el formato de fecha dia mes y año
				if($mes==1){$m='Ene';}
				if($mes==2){$m='Feb';}
				if($mes==3){$m='Mar';}
				if($mes==4){$m='Abr';}
				if($mes==5){$m='May';}
				if($mes==6){$m='Jun';}
				if($mes==7){$m='Jul';}
				if($mes==8){$m='Ago';}
				if($mes==9){$m='Sep';}
				if($mes==10){$m='Oct';}
				if($mes==11){$m='Nov';}
				if($mes==12){$m='Dic';}
				echo 'Ext. Garantia (Fecha Vencimiento: '.$R[0]->fecha_vigencia.'  Km Vencimiento:'.$R[0]->km_status.')';
		  }
			else
			{
				echo 'GARANTIA EXTENDIDA NO';# code...
			}
		}


		function GetCustomersOd(){
			$customers = $this->Ordendeserviciomodel->GetCusOda();
			$status = false;
			$message = "error";
			if($customers->num_rows() > 0){
				$status = true;
				$message = "success";
			}
			$response = array(
				"Message" => $message,
				"Status" => $status,
				"Object" => $customers->result()
			);

			echo json_encode($response);
		}

    function pizarron(){
  		$data['lista']=  $this->Ordendeserviciomodel->unafuncion();
		$this->load->view('orden/unalista',$data);
    }

		function enviarCaptura($id,$time){


		//actualizar lista de espera
		$info= array(
				'enp_status' => 2
			);
			$this->Ordendeserviciomodel->listadeesperaUpdate($info,$id);

		//Insertar en tabla pizarron
$info=array('piz_ID' => 'NULL','piz_ID_orden' => ''.$id.'','piz_tiempo_tabulado' => ''.$time.'','piz_entrega_lavado' => '',
'piz_status_mecanico' => '1','piz_status_lavado' => '1','piz_status_surtido'=>'6','piz_fecha' => ''.date('Y-m-d').'');

		 $this->Ordendeserviciomodel->insertPizarron($info, $id);
		redirect('pizarron');

		}


		function checkFotos(){

		$data['res']=$this->Ordendeserviciomodel->checkFotos($_GET['id']);
		if($data['res']){
			echo 1;
			}
			else{
				echo  0;
				}


		}

		function GetCampaniasVin($vin){
			$camps = $this->Ordendeserviciomodel->GetCampsVin($vin);
			$response = array(
					'Status' => true,
					'Message' => 'success',
					'Object' => $camps
				);
			echo json_encode($response);

		}

	public function index()
	{
		$data['ordenes']=$this->Ordendeserviciomodel->listarOrdenes();
		$this->load->view('orden/vista');
	}

	public function saveAjaxOrden(){
		$id=$_GET['id'];
		$campo=$_GET['campo'];
		$valor=$_GET['valor'];

		$this->Ordendeserviciomodel->updateOrdenLlenado($id,$campo,$valor);
		echo $campo;
		}


		function updateOrdenAsesor(){
		$id=$_GET['id'];
		$valor=$_GET['valor'];

		$info= array('seo_adviser' => $valor);
		$infodos= array('enp_ID_asesor' => $valor);

		$this->Ordendeserviciomodel->updateControl($info,$id);
		$this->Ordendeserviciomodel->updateControlProceso($infodos,$id);
		echo $valor;
		}

	function updateGarantiaExist(){
		$clienteConGarantia = $this->Ordendeserviciomodel->getclienteConGarantia($_GET['vin']);
		if($clienteConGarantia){
			$garantia_exist = array(
						'vin'=>$_GET['vin'],
						'status'=>$_GET['status'],
						'tipo'=>$_GET['tipo'],
						'km'=>$_GET['km'],
						'tiempo'=>$_GET['tiempo']
					);
			$exec = $this->Ordendeserviciomodel->updateGarantiaExist($garantia_exist,$_GET['vin']);
			if($exec){
				$json = '{"status":"1"}';
				echo $json;
			}else{
				$json= '{"status":"0"}';
				echo $json;
			}
		}else{
			$garantia_exist = array(
						'id'=>'NULL',
						'vin'=>$_GET['vin'],
						'status'=>$_GET['status'],
						'tipo'=>$_GET['tipo'],
						'km'=>$_GET['km'],
						'tiempo'=>$_GET['tiempo']
					);
			$this->Ordendeserviciomodel->insert_garantia($garantia_exist);
			echo "hola";
		}

	}


		function crear()
		{
			$listEspera = $this->Ordendeserviciomodel->getlistadeEspera($_SESSION['sfid']);

			if(!empty($listEspera) && count($listEspera) > 0)//Validar existencia de espera.
			{
				//Si el Cliente Existe Tomar la Informacion de la Ultima Orde de Servicio
				//Si el cliente no existes
				$infoCliente = $this->Ordendeserviciomodel->getInfoCliente($_GET['vin']);

				$clienteConGarantia = $this->Ordendeserviciomodel->getclienteConGarantia($infoCliente[0]->cus_serie);
				$orden = array('seo_cliente' =>$infoCliente[0]->cus_idCustomer);
				$images = array('sei_carpeta' =>$infoCliente[0]->cus_serie);

				$cliente = array(
					'sec_nombre' =>$infoCliente[0]->cus_name,
					'sec_email' => $infoCliente[0]->cus_email,
					'sec_tel' => $infoCliente[0]->cus_telephone,
					'sec_cel' =>$infoCliente[0]->cus_celular,
					'sec_calle' => $infoCliente[0]->cus_calle,
					'sec_num_ext' => $infoCliente[0]->cus_num,
					'sec_colonia' => $infoCliente[0]->cus_colonia,
					'sec_cp' => $infoCliente[0]->cus_cp,
					'sec_ciudad' => $infoCliente[0]->cus_ciudad,
					'sec_estado' => $infoCliente[0]->cus_estado,
					'sec_rfc' => $infoCliente[0]->cus_rfc,
					'sec_venta' => $infoCliente[0]->cus_fecha_venta,
					'sec_ultimo_servicio' => $infoCliente[0]->cus_ultimo_servicio,
				);

				$vehiculo = array(
					'sev_marca' => $infoCliente[0]->cus_marca,
					'sev_sub_marca' => $infoCliente[0]->cus_submarca,
					'sev_version' => $infoCliente[0]->cus_version,
					'sev_color' => $infoCliente[0]->cus_color,
					'sev_modelo' => $infoCliente[0]->cus_modelo,
					'sev_vin' => $infoCliente[0]->cus_serie,
					'sev_capacidad' => $infoCliente[0]->cus_capacidad,
					'sev_km_recepcion' => $infoCliente[0]->cus_km_recepcion,
					'sev_placas' => $infoCliente[0]->cus_placas,
					'sev_ultima_fecha' => $infoCliente[0]->cus_ultimo_servicio,
					'sev_orden' => $infoCliente[0]->cus_orden,
					'sev_fecha_venta' => $infoCliente[0]->cus_fecha_venta
				);

				if($clienteConGarantia)
				{

				}
				else
				{
					$garantia_exist = array(
						'id'=>'NULL',
						'vin'=>$infoCliente[0]->cus_serie,
						'status'=>'0',
						'tipo'=>'0',
						'km'=>'',
						'tiempo'=>''
					);

					$this->Ordendeserviciomodel->insert_garantia($garantia_exist);
				}

				$this->Ordendeserviciomodel->updateCliente($cliente,$listEspera[0]->enp_ID_proceso);
				$this->Ordendeserviciomodel->updateVehiculo($vehiculo,$listEspera[0]->enp_ID_proceso);
				$this->Ordendeserviciomodel->updateOrden($orden,$listEspera[0]->enp_ID_proceso);
				$this->Ordendeserviciomodel->updateImages($images,$listEspera[0]->enp_ID_proceso);

				$structure = './calendario/ajax/uploads/'.$infoCliente[0]->cus_serie;
				if(!file_exists($structure))
				{
					if (!mkdir($structure, 0777, true)) die('Error al creal el Folder...');
					else chmod($structure, 0777);
				}
				// if($this->sfid == 45 || $this->sfid == 138 || $this->sfid == 69 || $this->sfid == 132){
				header('Location:../../orden/llenar1/'.$listEspera[0]->enp_ID_proceso.'');
				// }else{
				//      header('Location:../../orden/llenar/'.$listEspera[0]->enp_ID_proceso.'');
				// }
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Alerta</strong> Es necesario crear una Orden de Servicio desde EL modulo de citas seleccionando una cita en especifico</div>');
				redirect('clientes');
			}
		}











		function cancelarOrden(){

	  //Orden en lista de espera
	  $listEspera=$this->Ordendeserviciomodel->getlistadeEspera($_SESSION['sfid']);
	  if($listEspera){
	  $CITA=$this->Ordendeserviciomodel->getCita($listEspera[0]->enp_ID_proceso);
	  $this->Ordendeserviciomodel->updateCitaOrden($CITA[0]->seo_IDapp);
	  $this->Ordendeserviciomodel->OsEliminarOrden($listEspera[0]->enp_ID_proceso);
	  redirect('clientes');
		  }
	  else{
	$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Alerta</strong> Es necesario crear una Orden de Servicio desde EL modulo de citas, seleccionando una cita en especifico</div>');
		redirect('clientes');
		}

		}

		function clienteNuevo(){

	  //Orden en lista de espera
	  $listEspera=$this->Ordendeserviciomodel->getlistadeEspera($_SESSION['sfid']);
	  if($listEspera){
	  //informacion de la cita
	  $cita=$this->Ordendeserviciomodel->getInfoCita($listEspera[0]->enp_ID_cita);

	  //actualizar, dar nombre y crear la carpeta de las imagenes del vehiculo
	  $carpeta=rand(10000,100000);
	  $images=array('sei_carpeta' =>$carpeta);
	  $this->Ordendeserviciomodel->updateImages($images,$listEspera[0]->enp_ID_proceso);
	  $structure = './calendario/ajax/uploads/'.$carpeta;
		if (!file_exists($structure)) {
            if (!mkdir($structure, 0777, true)) { die('Error al creal el Folder...');}else{chmod($structure, 0777);                                                }
		                              }
	  //inserta nuevo cliente vacio
	  $clienteNuevo=array(
	  'cus_name'=>$cita[0]->app_customerName.' '.$cita[0]->app_customerLastName,
	  'cus_email'=>$cita[0]->app_customerEmail,
	  'cus_telephone'=>$cita[0]->app_customerTelephone,
	  );
	  $IDC=$this->Ordendeserviciomodel->insertCliente($clienteNuevo);

	  //actualizar id del cliente en orden de servicio
	  $orden=array('seo_cliente' =>$IDC);
	  $this->Ordendeserviciomodel->updateOrden($orden,$listEspera[0]->enp_ID_proceso);

	// if($this->sfid == 45 || $this->sfid == 138 || $this->sfid == 69 || $this->sfid == 132){
     redirect('orden/llenar1/'.$listEspera[0]->enp_ID_proceso.'');
	// }else{
	//      redirect('orden/llenar/'.$listEspera[0]->enp_ID_proceso.'');
	// }


	  }
else{
	$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Alerta</strong> Es necesario crear una Orden de Servicio desde EL modulo de citas seleccionando una cita en especifico</div>');
		redirect('clientes');
		}

		}


		function llenar2($id)
			{
			$data['orden']=$this->Ordendeserviciomodel->getOrden($id);
             $data['check'] = $data['orden'][0]->seo_csvcheck;
            if($data['orden'][0]->seo_csvcheck == "0"){
                $vinSend = $data['orden'][0]->sev_vin;
                if($vinSend[0] == '.'){
                    $vinSend = substr($vinSend,1);
                }
                $url = 'http://hsas.gpoptima.net/ajax/ordendeservicio/checkCsv.php?vin='.$vinSend.'&city='.$this->sfworkshop.'&orden='.$data['orden'][0]->seo_idServiceOrder.'';
                $this->curl->simple_get($url);
            }

			if($data['orden'][0]->seo_cliente==0)
			    {
				  redirect('clientes');
				}
			else
			   {
			$data['tecnicos']=$this->Ordendeserviciomodel->getTecnicos($data['orden'][0]->sus_idUser);
			$data['promociones']=$this->Ordendeserviciomodel->listarPromociones();
			$data['vehiculos']=$this->Citamodel->obtenerModelos();
			$data['campanias']=$this->Ordendeserviciomodel->obtenerCampanias($data['orden'][0]->sev_vin);
            $data['historial'] = $this->Ordendeserviciomodel->GetHistorial($this->sfid);
			$data['vin']=$data['orden'][0]->sev_vin;

            $data['order'] = $data['orden'][0]->seo_idServiceOrder;
			if(sizeof($data['campanias'])>0)
			{
				$data['exists'] = 1;
			}
			else
			{
				$data['exists'] = 0;
			}
	        echo $this->load->view('orden/crearwf2',$data, true);
			echo ob_get_clean();
			flush();
			ob_start();
			   }
			}

			function CampaniaExist($vin){
				$camp = $this->Ordendeserviciomodel->obtenerCampaniasTodas($vin);
				$exist = 'false';
				if(sizeof($camp)>0)
					$exist = 'true';
				else
					$exist = 'false';
				$response = '{
								"Status" : true,
								"Message" : "success",
								"Object" : {
												"Status" : '.$exist.'
											}
							}';
				echo $response;
			}


			function llenar1($id)
			{
				$data['orden']=$this->Ordendeserviciomodel->getOrden($id);
				if($data['orden'][0]->seo_csvcheck == "0")
				{
					$vinSend = $data['orden'][0]->sev_vin;
					if($vinSend[0] == '.')
						$vinSend = substr($vinSend,1);
					$url = 'http://hsas.gpoptima.net/ajax/ordendeservicio/checkCsv.php?vin='.$vinSend.'&city='.$this->sfworkshop.'&orden='.$data['orden'][0]->seo_idServiceOrder.'';
					$this->curl->simple_get($url);
				}

				if($data['orden'][0]->seo_cliente==0)
					redirect('clientes');
				else
				{
					$data['tecnicos']=$this->Ordendeserviciomodel->getTecnicos($data['orden'][0]->sus_idUser);
					$data['promociones']=$this->Ordendeserviciomodel->listarPromociones();
					$data['garantias_base']=$this->Ordendeserviciomodel->garantiasBaseExt($data['orden'][0]->sev_vin);
					$data['garantias_ext']=$this->Ordendeserviciomodel->getExtGarantiaSta($data['orden'][0]->sev_vin);
					$data['vehiculos']=$this->Citamodel->obtenerModelos();
					$data['campanias']=$this->Ordendeserviciomodel->obtenerCampanias($data['orden'][0]->sev_vin);
					$data['historial'] = $this->Ordendeserviciomodel->GetHistorial($this->sfid);
					$data['vin']=$data['orden'][0]->sev_vin;
					$data['order'] = $data['orden'][0]->seo_idServiceOrder;
					if(sizeof($data['campanias'])>0)
						$data['exists'] = 1;
					else
						$data['exists'] = 0;
					echo $this->load->view('orden/crearwf',$data, true);
					echo ob_get_clean();
					flush();
					ob_start();
				}
			}

			function ver_victor($id)
			{
				$data['orden']=$this->Ordendeserviciomodel->getOrden($id);
				if($data['orden'][0]->seo_csvcheck == "0")
				{
					$vinSend = $data['orden'][0]->sev_vin;
					if($vinSend[0] == '.')
						$vinSend = substr($vinSend,1);
					$url = 'http://hsas.gpoptima.net/ajax/ordendeservicio/checkCsv.php?vin='.$vinSend.'&city='.$this->sfworkshop.'&orden='.$data['orden'][0]->seo_idServiceOrder.'';
					$this->curl->simple_get($url);
				}

				if($data['orden'][0]->seo_cliente==0)
					redirect('clientes');
				else
				{
					$data['tecnicos']=$this->Ordendeserviciomodel->getTecnicos($data['orden'][0]->sus_idUser);
					$data['promociones']=$this->Ordendeserviciomodel->listarPromociones();
					$data['vehiculos']=$this->Citamodel->obtenerModelos();
					$data['campanias']=$this->Ordendeserviciomodel->obtenerCampanias($data['orden'][0]->sev_vin);
					$data['historial'] = $this->Ordendeserviciomodel->GetHistorial($this->sfid);
					$data['vin']=$data['orden'][0]->sev_vin;
					$data['order'] = $data['orden'][0]->seo_idServiceOrder;
					if(sizeof($data['campanias'])>0)
						$data['exists'] = 1;
					else
						$data['exists'] = 0;

					print_r($data);
					// echo $this->load->view('orden/crearwf',$data, true);
					// echo ob_get_clean();
					// flush();
					// ob_start();
				}
			}

			function llenar_vic($id)
			{
				$data['orden']=$this->Ordendeserviciomodel->getOrden($id);
				if($data['orden'][0]->seo_csvcheck == "0")
				{
					$vinSend = $data['orden'][0]->sev_vin;
					if($vinSend[0] == '.')
						$vinSend = substr($vinSend,1);
					$url = 'http://hsas.gpoptima.net/ajax/ordendeservicio/checkCsv.php?vin='.$vinSend.'&city='.$this->sfworkshop.'&orden='.$data['orden'][0]->seo_idServiceOrder.'';
					$this->curl->simple_get($url);
				}

				if($data['orden'][0]->seo_cliente==0)
					redirect('clientes');
				else
				{
					$data['tecnicos']=$this->Ordendeserviciomodel->getTecnicos($data['orden'][0]->sus_idUser);
					$data['promociones']=$this->Ordendeserviciomodel->listarPromociones();
					$data['vehiculos']=$this->Citamodel->obtenerModelos();
					$data['campanias']=$this->Ordendeserviciomodel->obtenerCampanias($data['orden'][0]->sev_vin);
					$data['historial'] = $this->Ordendeserviciomodel->GetHistorial($this->sfid);
					$data['vin']=$data['orden'][0]->sev_vin;
					$data['order'] = $data['orden'][0]->seo_idServiceOrder;
					if(sizeof($data['campanias'])>0)
						$data['exists'] = 1;
					else
						$data['exists'] = 0;
					//echo $this->load->view('orden/crearwf',$data, true);
					// echo ob_get_clean();
					// flush();
					// ob_start();
				}
				print_r($data["tecnicos"]);
				print_r($data['orden'][0]->sus_idUser);
			}

			function llenarDev($id)
			{
			$data['orden']=$this->Ordendeserviciomodel->getOrden($id);


            if(is_null($data['orden'][0]->seo_csvcheck)){
                $vinSend = $data['orden'][0]->sev_vin;
                if($vinSend[0] == '.'){
                    $vinSend = substr($vinSend,1);
                }
                $url = 'http://hsas.gpoptima.net/ajax/ordendeservicio/checkCsv.php?vin='.$vinSend.'&city='.$this->sfworkshop.'&orden='.$data['orden'][0]->seo_idServiceOrder.'';
                $this->curl->simple_get($url);
            }

			if($data['orden'][0]->seo_cliente==0)
			    {
				  redirect('clientes');
				}
			else
			   {
			$data['tecnicos']=$this->Ordendeserviciomodel->getTecnicos($data['orden'][0]->sus_idUser);
			$data['promociones']=$this->Ordendeserviciomodel->listarPromociones();
			$data['vehiculos']=$this->Citamodel->obtenerModelos();
			$data['campanias']=$this->Ordendeserviciomodel->obtenerCampanias($data['orden'][0]->sev_vin);

			$data['vin']=$data['orden'][0]->sev_vin;

            $data['order'] = $data['orden'][0]->seo_idServiceOrder;
			if(sizeof($data['campanias'])>0)
			{
				$data['exists'] = 1;
			}
			else
			{
				$data['exists'] = 0;
			}
	        $this->load->view('orden/CrearDev',$data);
			   }
			}


			function llenar($id)
			{
				redirect('orden/llenar1/'.$id);
			// $data['orden']=$this->Ordendeserviciomodel->getOrden($id);
			// if($data['orden'][0]->seo_cliente==0)
			//     {
			// 	  redirect('clientes');
			// 	}
			// else
			//    {
			// $data['tecnicos']=$this->Ordendeserviciomodel->getTecnicos($data['orden'][0]->sus_idUser);
			// $data['promociones']=$this->Ordendeserviciomodel->listarPromociones();
			// $data['vehiculos']=$this->Citamodel->obtenerModelos();
			// $data['campanias']=$this->Ordendeserviciomodel->obtenerCampanias($data['orden'][0]->sev_vin);
			// $data['vin']=$data['orden'][0]->sev_vin;
   //          $data['order'] = $data['orden'][0]->seo_idServiceOrder;
			// if(sizeof($data['campanias'])>0)
			// {
			// 	$data['exists'] = 1;
			// }
			// else
			// {
			// 	$data['exists'] = 0;
			// }
	  //       /*
		 //    if($_SESSION['sfworkshop']=='Tijuana')
			// $this->load->view('orden/crearwf',$data);
			// else
			// */
			// $this->load->view('orden/crear_c',$data);

			//    }
			}

	/*public function crear()
	{


			$tapetes = $this->input->post('tapetes');
			if($tapetes != '1')
				$tapetes = 0;
			$espejo = $this->input->post('espejo');
			if($espejo != '1')
				$espejo = 0;
			$antena = $this->input->post('antena');
			if($antena != '1')
				$antena = 0;
			$tapones = $this->input->post('tapones');
			if($tapones != '1')
				$tapones = 0;
			$radio = $this->input->post('radio');
			if($radio != '1')
				$radio = 0;
			$encendedor = $this->input->post('encendedor');
			if($encendedor != '1')
				$encendedor = 0;
			$herramienta = $this->input->post('herramienta');
			if($herramienta != '1')
				$herramienta = 0;
			$llanta = $this->input->post('llanta');
			if($llanta != '1')
				$llanta = 0;
			$limpiadores = $this->input->post('limpiadores');
			if($limpiadores != '1')
				$limpiadores = 0;
			$reflejantes = $this->input->post('reflejantes');
			if($reflejantes != '1')
				$reflejantes = 0;
			$extinguidor = $this->input->post('extinguidor');
			if($extinguidor != '1')
				$extinguidor = 0;
			$cables = $this->input->post('cables');
			if($cables != '1')
				$cables = 0;
			$ecualizador = $this->input->post('ecualizador');
			if($ecualizador != '1')
				$ecualizador = 0;
			$gato = $this->input->post('gato');
			if($gato != '1')
				$gato = 0;
			$estereo = $this->input->post('estereo');
			if($estereo != '1')
				$estereo = 0;
			$tapongas = $this->input->post('tapongas');
			if($tapongas != '1')
				$tapongas = 0;
			$carnet = $this->input->post('carnet');
			if($carnet != '1')
				$carnet = 0;
			$carroceria = $this->input->post('carroceria');
			if($carroceria != '1')
				$carroceria = 0;
			$ventanas = $this->input->post('ventanas');
			if($ventanas != '1')
				$ventanas = 0;
			$vestidura = $this->input->post('vestidura');
			if($vestidura != '1')
				$vestidura = 0;
			$parabrisas = $this->input->post('parabrisas');
			if($parabrisas != '1')
				$parabrisas = 0;


			$orden = array(
				'seo_date' => '',
				'seo_adviser' => '',
				'seo_timeReceived' => $this->input->post('horaRecibido'),
				'seo_promisedTime' => $this->input->post('horaPrometida'),
				'seo_tower' => $this->input->post('torre'),
				'seo_technicianTeam' => $this->input->post('tecnicos'),
				'seo_inKilometers' => $this->input->post('vehiculoKmIn'),
				'seo_combustible' => $this->input->post('combustible'),
				'seo_tapetes' => $tapetes,
				'seo_tapones' => $tapones,
				'seo_herramienta' => $herramienta,
				'seo_reflejantes' => $reflejantes,
				'seo_ecualizador' => $ecualizador,
				'seo_tapon' => $tapongas,
				'seo_ventanas' => $ventanas,
				'seo_espejo' => $espejo,
				'seo_radio' => $radio,
				'seo_llantarefaccion' => $llanta,
				'seo_extinguidor' => $extinguidor,
				'seo_gato' => $gato,
				'seo_carnet' => $carnet,
				'seo_vestidura' => $vestidura,
				'seo_antena' => $antena,
				'seo_encendedor' => $encendedor,
				'seo_limpiadores' => $limpiadores,
				'seo_cables' => $cables,
				'seo_estereo' => $estereo,
				'seo_carroceria' => $carroceria,
				'seo_parabrisas' => $parabrisas,
				'seo_amount' => $this->input->post('montoServicio'),
				'seo_comments' => $this->input->post('comentarios'),
				'seo_clientInformation' => $clienteInfo,
				'seo_vehicleInformation' => $vehiculoInfo,

			);

			$this->Ordendeserviciomodel->agregarOrden($orden);
			redirect('pizarron');
		}
		else
		{

			$this->load->view('orden/crear');
		}

	}*/

	function printTest($IDO){

		$info=$data['orden']=$this->Ordendeserviciomodel->getOrden($IDO);
		$data['servicios']=$this->Ordendeserviciomodel->getOrdenServicios($IDO);
		$data['serviciossv']=$this->Ordendeserviciomodel->getOrdenServiciossv($IDO);
		$this->load->view('orden/printtest',$data);
	}

	function printtestDuplicadoBueno($IDO){

		$info=$data['orden']=$this->Ordendeserviciomodel->getOrden($IDO); //OK
		$data['servicios']=$this->Ordendeserviciomodel->getOrdenServicios($IDO);
		$data['serviciossv']=$this->Ordendeserviciomodel->getOrdenServiciossv($IDO);
		$data['tecnico']=$this->Ordendeserviciomodel->getTecnico($info[0]->seo_technicianTeam);
		$this->load->view('orden/printtestDuplicadoBuenoxxx',$data);
	}

	function testPrint($IDO)
	{

	}

	function printtestDuplicadoBueno_vin($IDO)
	{

		$info=$data['orden']=$this->Ordendeserviciomodel->getOrden($IDO);
		$data['servicios']=$this->Ordendeserviciomodel->getOrdenServicios($IDO);
		$data['serviciossv']=$this->Ordendeserviciomodel->getOrdenServiciossv($IDO);
		$data['tecnico']=$this->Ordendeserviciomodel->getTecnico($info[0]->seo_technicianTeam);
		$this->load->view('orden/printtestDuplicadoBuenovin',$data);
	}

	function printFirma($IDO){

		$info=$data['orden']=$this->Ordendeserviciomodel->getOrden($IDO);
		$data['servicios']=$this->Ordendeserviciomodel->getOrdenServicios($IDO);
		$data['serviciossv']=$this->Ordendeserviciomodel->getOrdenServiciossv($IDO);
		$data['tecnico']=$this->Ordendeserviciomodel->getTecnico($info[0]->seo_technicianTeam);
		$this->load->view('orden/print_firma',$data);
	}

	function imprimirOrden()
	{
		$html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
.page {
	font-family:Verdana, Geneva, sans-serif;
}
	.head {
		width:100%;

		margin-bottom:0px;

	}
		.contact {
			width:55%;

			float:left;
		}
			.logo {
				width:100%;
				height:70px;
				background-image:url('http://webmarketingnetworks.com/sas/documents/hlogo.png');
				background-size: 80%;
				background-repeat:no-repeat;
			}
			.address {
				width:100%;
				height:100%;
				border:none;
				border-collapse:collapse;
			}
				.address th {
					font-size:9px;
				}
				.address td {
					font-size:8px;
				}
				.address .hours{
					font-size:10px;
					text-align:center;
				}
		.folio {
			width:45%;

			float:left;
		}
			.os-type {
				width:100%;
				height:25px;
				padding-top:20px;
				font-size:12px;
				font-weight:bold;
				text-align:right;
			}
			.info {
				width:100%;
				height:120px;
				font-size:9px;
			}
				.message {
					width:60%;
					height:60px;
					float:left;
				}
					.message p{
						width:98%;
						margin:auto;
						margin-top:20px;
						padding:5px;
						border:0px solid rgb(51,51,51);
						border-radius:15px;
						background-color:rgb(226,0,43);
						color:rgb(255,255,255);
						font-size:8px;
						text-align:justify;
						line-height:180%;
					}
				.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin:auto;
						margin-top:20px;
						font-size:8px;
						border:2px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}
						.order-date .gray{
							background-color:rgb(102,102,102);
							color:rgb(255,255,255);
							font-size:8.4px;
							font-weight:900;
						}
						.order-date .white{
							height:27px;
							font-size:8.4px;
							font-weight:900;
							vertical-align:text-top;
							text-align:left;
						}
						.order-date td{
							border:1px solid rgb(102,102,102);
						}

.customer-info {
	width:40%;
	float:left;
	font-size:9px;
}

	.customer-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.customer-info table th{
		border-bottom:1px solid rgb(0,0,0);
	}
	.customer-info table td{
		padding-left:5px;
	}

.vehicle-info {
	width:60%;
	float:left;
	font-size:9px;
}

	.vehicle-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.vehicle-info table th {
		border-bottom:1px solid rgb(51,51,51);
	}
	.vehicle-info table td{
		padding-left:5px;
	}

.reception-info {
	width:60%;
	float:left;
	font-size:9px;
}
	.reception-info table{

		height:120px;
		margin:auto;

	}
	.reception-info table td{
		padding-left:5px;
	}

.confirmation {

	float:left;


}

.confirmation p{
	padding:0 10px 0 10px;
}

.details {
	width:70%;
}

.comments{
	width:100%;

	float:left;
	font-size:9px;
	text-align:justify;
	vertical-align:top;
}
	.comments .text{
		width:99%;
		height:40px;
		margin:auto;
	}
	.text .title{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
	}

	.title-address-right{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:right;
	color:rgb(226,0,43);
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
	}

	.title-address-left{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:left;
	color:rgb(226,0,43);
	padding-left:3px;
	}

.items{
	width:100%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:9px;
}
	.items table{
		width:95%;
		margin:auto;
		border:none;
	}

.tecs{
	width:100%;
	margin-top:10px;
	float:left;
	font-size:8px;
}
	.tecs table{
		width:99%;
		margin:auto;
	}
	.tecs table td{
		height:25px;
		padding-left:5px;
		vertical-align:text-top;
		border-bottom:1px solid rgb(0,0,0);
		border-right:1px solid rgb(0,0,0);
	}

	.tecs table td:last-child{
		border-right:none;
	}

.chasis {
	width:100%;
	height:205px;
	float:left;
	background-image:url('http://webmarketingnetworks.com/sas/documents/chasis.png');
	background-size: 100%;
	background-repeat:no-repeat;
}

.services-head{
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:10px;
	margin-bottom:10px;
}
	.services-head th{
		height:22px;
	}

.services{
	font-size:9px;
	border:1px solid rgb(226,0,43);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}

.first-cell{
	border-right:1px solid rgb(226,0,43);
}

.total{
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(0,0,0);
	border-radius:5px;
}
.total-text{
	text-align:right;
}

.row{
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child{
	margin-bottom:0px;
}
.floated{
	float:left;
}
.col-12 {
	width:100%;
}
.col-10{
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8{
	width:66.66%;
}
.col-7{
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5{
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:16.5%;
}
.col-1{
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right{
	text-align:right;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left{
	text-align:left;
	padding-left:3px;
}
.radius {
    border: 1px solid rgb(51,51,51);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>

<div class="page">
	<div class="head">
    	<div class="contact">
        	<div class="logo">

			</div>

            	<table class="address">
                    	<tr>
                        	<th class="title-address-right">TIJUANA:</th>
                        	<th class="title-address-left">MEXICALI:</th>
                        </tr>
                    	<tr>
                        	<td class="right">Av. Padre Kino 4300 Zona Río, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td class="left">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr>
                        	<th class="title-address-right">ENSENADA:</th>
                        	<td class="hours">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr>
                        	<td class="right">Ensenada Av. Balboa 146 esq.<br />
							López Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td class="hours">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            Sábado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>

        </div>

        <div class="folio">
        	<div class="os-type">
        		GARANTÍA PAGO CLIENTE INTERNO
        </div>
        <div class="info">
        	<div class="message">
                <p>SÓLO A LA PRESENTACIÓN DE ESTA CONTRASEÑA SE HARÁ ENTREGA DEL VEHÍCULO, SUPLICAMOS LA CONSERVE
                PUES NO NOS HACEMOS RESPONSABLES DEL MAL USO QUE SE HAGA DE ELLA.</p>
			</div>
            	<div class="service-order">
                	<table class="order-date radius">
                        	<tr class="gray">
                            	<td>ORDEN DE SERVICIO</td>
                            </tr>
                            <tr class="white">
                            	<td>AAAA</td>
                            </tr>
                            <tr class="gray">
                            	<td>FECHA</td>
                            </tr>
                            <tr class="white">
                            	<td>AAAA</td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>



	<div class="row">
    	<div class="customer-info">
   	  <table class="radius" style="font-size:11px;">
                    <tr class="first-row">
                        <th colspan="2" class="title">DATOS DEL CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-3">Nombre: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Dirección: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Ciudad: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">R.F.C.: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Teléfono: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Atención a: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Teléfono: </td>
                        <td class="col-9">AAAA</td>
                    </tr>
            </table>
        </div>
        <div class="vehicle-info">
        	<table class="radius" style="font-size:11px;">
                    <tr>
                        <th colspan="4" class="title">DATOS DEL VEHÍCULO</th>
                    </tr>
                    <tr>
                        <td class="col-3">Modelo: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Placas: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Año: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Último Servicio: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Fecha de Venta: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Seguro: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">Color: </td>
                        <td class="col-3">AAAA</td>
                        <td class="col-3">Km. de Entrada: </td>
                        <td class="col-3">AAAA</td>
                    </tr>
                    <tr>
                        <td class="col-3">No. de  Serie: </td>
                        <td class="col-3">AAAA</td>
						 <td class="col-3"> </td>
                        <td class="col-3"></td>
                    </tr>
                    <tr>
                        <td class="col-3">No. de Motor: </td>
                        <td class="col-3">AAAA</td>
						<td class="col-3"> </td>
                        <td class="col-3"></td>
                    </tr>
                    <tr>
                        <td class="col-3">Transmisión: </td>
                        <td class="col-3">AAAA</td>
						<td class="col-3"> </td>
                        <td class="col-3"></td>
                    </tr>
            </table>
        </div>
    </div>
	<div class="row">
   	  <div class="reception-info">
  			<table class="radius" style="font-size:11px;">
                	<tr>
                    	<td class="col-4">Fecha y hora de recepción: </td>
                    	<td class="col-8">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Asesor: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Técnico: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-4">Fecha y hora prometida: </td>
                    	<td class="col-8">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-4">Monto orden de servicio: </td>
                    	<td class="col-8">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Torre: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
                	<tr>
                    	<td class="col-3">Firma del cliente: </td>
                    	<td class="col-9">AAAA</td>
                    </tr>
            </table>
        </div>
      <div class="confirmation " style="border: 2px solid rgb(51,51,51); font-size:7px;">
        	<p class="confirm">GARANTIZO Y ASEGURO QUE SOY DUEÑO O ESTOY AUTORIZADO PARA ORDENAR ESTA REPARACIÓN CON LA
            PRESENTE AUTORIZO EL TRABAJO DESCRITO JUNTO CON LAS PIEZAS DE REPUESTO Y/O MATARIALES NECESARIOS PARA EFECTUARLOS.

			</p><br /><br><br>

			<p>FIRMA DEL CLIENTE: __________________________________</p>
        </div>
    </div>
	<div class="row">
    	<div class="col-10 floated">
            <div class="comments">
            	<div class="text " style="border: 1px solid rgb(51,51,51);">
                	<span class="title">COMENTARIOS:</span>
                </div>
            </div>
            <div class="items">
                <table style=" font-size:9px; width:100%">
                        <tr>
                            <td>TAPETES</td>
                            <td>TAPONES</td>
                            <td>HERRAMIENTA</td>
                            <td>REFLEJANTES</td>
                            <td>ECUALIZADOR</td>
                            <td>TAPÓN GAS</td>
                            <td>VENTANAS</td>
                        </tr>
                        <tr>
                            <td>ESPEJO</td>
                            <td>RADIO</td>
                            <td>LLANTA REF.</td>
                            <td>EXTINGUIDOR</td>
                            <td>GATO</td>
                            <td>CARNET</td>
                            <td>VESTIDURA</td>
                        </tr>
                        <tr>
                            <td>ANTENA</td>
                            <td>ENCENDEDOR</td>
                            <td>LIMPIADORES</td>
                            <td>CABLES P/C</td>
                            <td>ESTEREO</td>
                            <td>CARROCERÍA</td>
                            <td>PARABRISAS</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>CANTIDAD COMB.</td>
                        </tr>
                </table>
            </div>
            <div class="tecs">
                <table class="radius" style=" font-size:9px; width:100%">
                        <tr>
                            <td>MEC.</td>
                            <td>MEC.</td>
                            <td>MEC.</td>
                            <td>MEC.</td>
                        </tr>
                        <tr>
                            <td>HR.</td>
                            <td>HR.</td>
                            <td>HR.</td>
                            <td>HR.</td>
                        </tr>
                </table>
            </div>
        </div>
        <div class="col-2 floated">
        	<div class="chasis">
            </div>
        </div>
    </div>
	<div class="row">
    	<table class="radius services-head" style="width:100%;">
        	<tr>
            	<th class="first-cell col-2">CLAVE</th>
            	<th class="col-5">DESCRIPCIÓN</th>
            	<th class="col-2">P.U.</th>
            	<th class="col-2">CANTIDAD</th>
            	<th class="col-1">P. TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
            	<tr>
                	<td class="first-cell col-2">AAAA</td>
                	<td class="col-5">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-1">AAAA</td>
                </tr>
            	<tr>
                	<td class="first-cell col-1">AAAA</td>
                	<td class="col-5">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-2">AAAA</td>
                	<td class="col-1">AAAA</td>
                </tr>
            	<tr>
                	<td class="first-cell">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
            	<tr>
                	<td class="first-cell">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
            	<tr>
                	<td class="first-cell">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
				<tr>
                  	<td class="first-cell">AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                	<td>AAAA</td>
                </tr>
            	<tr>
                  	<td class="first-cell"></td>
                	<td colspan="3" class="total-text">Precio total de refacciones y mano de obra</td>
                	<td><p class="total">AAAA</p></td>
                </tr>
        </table>
    </div>
</div>
EOF;
		$this->load->library('mpdf');
	$this->mpdf->writeHTML($html);
		$this->mpdf->Output();


	}


	function captura()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		if(empty($_GET['fecha'])){
			//$fecha=date('Y-m-d');
			$fecha2=date('Y-m-d');
			}
		else{
			//list($di,$ms,$an)=explode("-",$_GET['fecha']);
			//$fecha=$an.'-'.$ms.'-'.$di;
			$fecha2=$_GET['fecha'];
			}

			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}

		$data['fecha']=$fecha2;
		$data['taller']=$taller;
		$data['ordenesPen']=$this->Ordendeserviciomodel->listaparaCapturaEspera($taller);
		$data['ordenes']=$this->Ordendeserviciomodel->listaparaCaptura($taller,$fecha2,2);
		$data['ordenes_cap']=$this->Ordendeserviciomodel->listaparaCaptura($taller,$fecha2,3);

		$data['utilizacion'] = $this->Productividadmodel->utilizacion($fecha2,$taller);
		$this->load->view('orden/captura/vista',$data);
	}

	function updateOrdenCaptura()
	{
		echo $_GET['valor'];
	$dato= array('seo_serviceOrder' =>$_GET['valor']);
	$data= array('enp_status' =>'3');
	$this->Ordendeserviciomodel->updateOrdenCaptura($dato,$_GET['id']);
	$this->Ordendeserviciomodel->updateEspera($data,$_GET['id']);
    }

	function eliminarOrden($id){
		if($id=='undefined'){
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error</strong> Necesita Seleccionar una Orden de Servicio</div>');
		redirect('orden/captura');
			}
			else{
		$this->Ordendeserviciomodel->OsEliminarOrden($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong>Ha Eliminado Una Orden de Serivicio.</div>');
		redirect('orden/captura');
			}
		}



	function updateControl()
	{

	 list($id,$sta)=explode('_',$_GET['data']);

	 $datos= array('seo_control_sta' => $sta,);
	 $this->Ordendeserviciomodel->updateControl($datos,$id);

	if($sta==1){ echo '/';}
	if($sta==2){ echo  'X';}
	if($sta==3){ echo  '(X)';}

    }

	function updateControlHora(){
	 $datos= array('seo_control_hora' => $_GET['value'],);
	 $this->Ordendeserviciomodel->updateControl($datos,$_GET['id']);
    }

	function updateEntregado(){
	 $datos= array('seo_control_entregado' => $_GET['valor'],);
	 $this->Ordendeserviciomodel->updateControl($datos,$_GET['id']);
    }


	function agregar_inventario_pizarron(){
	$total_asesores=$this->Ordendeserviciomodel->contar_pizarron($_SESSION['sfworkshop']);
	$min=$this->Ordendeserviciomodel->contar_pizarron_minimo($_SESSION['sfworkshop']);
	$min=$min[0]->numero;
	$tecnico=116;
	foreach($total_asesores as $num){
		$num->lsp_numero;
	if($min >= $num->lsp_numero){
	 $min=$num->lsp_numero;
	$tecnico=$num->lsp_tecnico;
		}
		}
	echo $tecnico.'-----------';

	if($_SESSION['sfidws']==1){$asesor=131;	}
	if($_SESSION['sfidws']==2){$asesor=35;	}
	if($_SESSION['sfidws']==3){$asesor=124;	}

	$datos= array(
	'pii_fecha'=>date('Y-m-d H:i:s'),
	'pii_vin' => $_POST['vin'],
	'pii_asesor'=>$asesor,
	'pii_tecnico'=>$tecnico,
	'pii_servicio'=>2,
	'pii_torre'=>$_POST['torre'],
	'pii_estado'=>'Sin Previa',
	'pii_ubicacion'=>0
	);
	//agregamos auto de inventario al pizarron
	$this->Ordendeserviciomodel->insert_inventario_pizarron($datos);
	//update pizarron tecnico
	$this->Ordendeserviciomodel->updateTecnicoSM($tecnico,'suma');
    echo $tecnico;
	}



    function crearOrdenPrevias($idCita,$idCliente,$tecnico,$tor,$vin,$resta){


    $IDO=$this->Ordendeserviciomodel->insertOrden($idCita,$_SESSION['sfid']);
    $datos=array('seo_technicianTeam'=>$tecnico, 'seo_cliente' =>$idCliente,'seo_tower'=>$tor,'seo_vehicle'=>$vin);


     // Editar orden de Servicio.
    $this->Ordendeserviciomodel->updateOrden($datos,$IDO);


    //Insertar en tabla pizarron
$info=array('piz_ID' => 'NULL','piz_ID_orden' => ''.$IDO.'','piz_tiempo_tabulado' => '2','piz_entrega_lavado' => '','piz_status_mecanico' => '1','piz_status_lavado' => '1','piz_status_surtido'=>'6','piz_fecha' => ''.date('Y-m-d').'');

   ///informacion de l Cliente
   $infoCliente=array('sec_nombre'=>'Cliente Previa' );


    $this->Ordendeserviciomodel->insertPizarron($info);
	$this->Ordendeserviciomodel->updateTecnicoSM($resta,'resta');
	$this->Ordendeserviciomodel->updateCliente($infoCliente,$idCliente);
   echo $tecnico;
    }

	// function servicios_por_vin()
	// {
	// 	header('Content-Type: application/json; charset=utf-8');
	// 	$response = array();
	// 	if(!empty($_POST["vin"]))
	// 	{
	// 		extract($_POST);
	// 		$resultado = $this->Ordendeserviciomodel->servicios_por_vin($vin);
	// 		$response["vin"] = $vin;
	//
	// 		foreach($resultado AS $r)
	// 		{
	// 			$r->fecha_completa = fecha_completa_larga($r->fecha)." ".$r->time;
	// 			unset($r->fecha_time);
	// 			$r->id_orden = (int)$r->id_orden;
	// 		}
	// 		$response["data"] = $resultado;
	// 		$response["count"] = count($resultado);
	// 		$response["status"] = "success";
	// 		$response["message"] = "Registros encontrados";
	// 	}
	// 	else { $response["status"] = "error"; $response["message"] = "No se recibió vin"; }
	// 	echo json_encode($response);
	// }
}
