<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produccion extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Produccionmodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		//$data['listaProduccion']=$this->Produccionmodel->listaProduccion($this->per);	
		$this->load->view('produccion/vista');
	}
	
	function editar()
	{
		//$data['listaProduccion']=$this->Produccionmodel->listaProduccion($this->per);	
		$this->load->view('produccion/editar');
	}
	
	
}