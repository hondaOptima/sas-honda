<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModeloVehiculo extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modelovehiculomodel');
		$this->load->model('Serviciomodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		$data['modelos']=$this->Modelovehiculomodel->listarModelos($this->per);
		$this->load->view('modelovehiculo/vista',$data);
	}
	
	public function crear()
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		
        if ($this->form_validation->run())
        {
			$modelo = array(
						'vem_name'=>$this->input->post('nombre'),
						'vem_isActive'=>1,
						);
						
			$serpro=$this->Modelovehiculomodel->modelospro();
			$sernopro=$this->Modelovehiculomodel->modelosnopro();
			$ID=$this->Modelovehiculomodel->agregarModelo($modelo);			
						
			foreach($serpro as $ser){
				  $pro=array('ser_price' => '0',
				  'ser_refacciones' => '0',
				  'ser_mat_varios' => '0',
				  'ser_comercial' => '0',
				  'ser_vehicleModel' => $ID,
				  'ser_serviceType' => $ser->set_idServiceType,
				  'ser_isActive' => '1',
				  'ser_approximate_duration' => '0',
				  'ser_tipo' => 'pro');
				  $this->Serviciomodel->agregarServicio($pro);			  
				}
				
				foreach($sernopro as $ser){
				 $nopro=array('ser_price' => '0',
				  'ser_refacciones' => '0',
				  'ser_mat_varios' => '0',
				  'ser_comercial' => '0',
				  'ser_vehicleModel' => $ID,
				  'ser_serviceType' => $ser->set_idServiceType,
				  'ser_isActive' => '1',
				  'ser_approximate_duration' => '0',
				  'ser_tipo' => 'nopro');
				 $this->Serviciomodel->agregarServicio($nopro);
				}			

		   	
			redirect('modelovehiculo');
		} 
		else
		{
			$this->load->view('modelovehiculo/crear');
		}
	}
	
	public function editar($id)
	{
	 	$data['modelo'] = $this->Modelovehiculomodel->obtenerModelo($id);
		
		if($data['modelo'])
		{
			$this->load->view('modelovehiculo/editar', $data);
		} 
		else
		{
			redirect('error');
		}
	}
	
	public function actualizar($id)
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');		
		
        if ($this->form_validation->run())
        {
			$modelo = array(
						'vem_name'=>$this->input->post('nombre'),
						);

						
		   	$this->Modelovehiculomodel->actualizarModelo($modelo, $id);
			
			redirect('modelovehiculo');
		} 
		else
		{
			redirect('modelovehiculo/editar',$id);
		}
	}
	
	function desactivar($id){
	 	$this->Modelovehiculomodel->desactivarModelo($id);
		redirect('modelovehiculo');
	}
	
	function activar($id){
	 	$this->Modelovehiculomodel->activarModelo($id);
		redirect('modelovehiculo');
	}
	
	
	
}