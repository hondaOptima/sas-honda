<?php 
	
	class trasladosmodel extends CI_Model 
	{
		function __construct()
		{
			parent::__construct();
			$this->db2 = $this->load->database('dboptima', true);
			$this->db3 = $this->load->database('crmtraslados', true);
		}

		//------------------------------
		//
		//	Info. para vista
		//
		//------------------------------

		function obtener_traslados($mes, $anio)
		{
			$this->db2->select('fechasTraslados.id, nombreTrasladista, fecha, hora_inicio, hora_fin');

			$this->db2->from('fechasTraslados');
			
			$this->db2->where("MONTH( STR_TO_DATE( fechasTraslados.fecha,  '%d-%c-%Y' ) ) = ", $mes);
			$this->db2->where("YEAR( STR_TO_DATE( fechasTraslados.fecha,  '%d-%c-%Y' ) ) = ", $anio);

			$this->db2->order_by('fecha', 'asc');

			$result = $this->db2->get();
			
			return $result->result_array();
		}
        
        function obtener_traslado($id) {

            $this->db2->select('id, nombreTrasladista, fecha, hora_inicio, hora_fin');
            $this->db2->from('fechasTraslados');
			$this->db2->where("id", $id);

            $result = $this->db2->get();

            return $result->row_array();
        }

		function obtener_paquetes($traslado)
		{
			$this->db2->select('id, traslado, numserie1, numserie2, horasTraslado, origen, destino');

			$this->db2->from('trasladoPaquete');

			$this->db2->where('trasladoPaquete.traslado = ', $traslado);

			$this->db2->order_by('id', 'asc');

			$result = $this->db2->get();

			return $result->result_array();
		}

		function obtener_paquete($id)
		{
			$this->db2->select('id, traslado, numserie1, numserie2, horasTraslado, origen, destino');

			$this->db2->from('trasladoPaquete');

			$this->db2->where('trasladoPaquete.id = ', $id);

			$result = $this->db2->get();

			return $result->row_array();
		}

		function obtener_ubicacion($id)
		{
			$this->db2->select('agencia.id as id, agencia.nombre as agencia, agencia_marca.nombre as marca, ciudades.nombre as ciudad, agencia.mail as mail');

			$this->db2->from('agencia');
			$this->db2->join('agencia_marca', 'agencia.marca = agencia_marca.id');
			$this->db2->join('ciudades', 'agencia.ciudad = ciudades.id');

			$this->db2->where('agencia.id = ', $id);

			$result = $this->db2->get();

			return $result->row_array();
		}

		function obtener_catalogoUbicaciones()
		{
			$this->db2->select('agencia.id as id, agencia.nombre as agencia, agencia_marca.nombre as marca, ciudades.nombre as ciudad');

			$this->db2->from('agencia');
			$this->db2->join('agencia_marca', 'agencia.marca = agencia_marca.id');
			$this->db2->join('ciudades', 'agencia.ciudad = ciudades.id');

			$result = $this->db2->get();

			return $result->result_array();
		}

		function obtener_catalogoNumSerie($status = '')
		{
			$this->db3->select('tra_ID as id, tra_vin as vin, tra_fecha as fecha_peticion, tra_origen as origen, tra_destino as destino, tra_status as status, tra_modelo as modelo, tra_anio as anio, tra_color as color, tra_fecha_salida as fecha_solicitada');
			$this->db3->from('traslado');

			if($status != ''){
				$this->db3->where('tra_status = 0 or tra_status = 1', NULL, FALSE);
			}

			$this->db3->order_by('tra_fecha', 'desc');
			$this->db3->group_by('tra_vin');
			$result = $this->db3->get();
			return $result->result_array();
		}

		function obtener_detallesNumSerie($ns)
		{
			$this->db3->select('tra_ID as id, tra_vin as vin, tra_fecha as fecha_peticion, tra_origen as origen, tra_destino as destino, tra_status as status, tra_modelo as modelo, tra_anio as anio, tra_color as color, tra_fecha_salida as fecha_solicitada');
			$this->db3->from('traslado');
			$this->db3->where('tra_vin', $ns);
			$this->db3->order_by('tra_fecha', 'desc');
			$this->db3->limit(1);

			$result = $this->db3->get();

			return $result->row_array();
		}

		function cambiar_trasladoStatus($id, $status)
		{
			return $this->db3->update('traslado', array("tra_status" => $status), array('tra_id' => $id));
		}
		
		function obtener_trasladoDia($fecha)
		{
			$this->db2->select('fechasTraslados.id, nombreTrasladista, fecha, hora_inicio, hora_fin');
			
			$this->db2->from('fechasTraslados');
			
			$this->db2->where("fecha", $fecha);
			
			$this->db2->order_by('fecha', 'asc');
			
			$result = $this->db2->get();
			
			return $result->row_array();
		}

		function borrar_traslado($traslado)
		{
			$this->db2->trans_start();

			$this->db2->delete('fechasTraslados', array("id" => $traslado));
			$this->db2->delete('trasladoPaquete', array("traslado" => $traslado));

			$this->db2->trans_complete();

			return $this->db2->trans_status();
		}

		//------------------------------
		//
		//	Creacion de Paquetes
		//
		//------------------------------

		function inicializar_traslado($fecha)
		{
			//para que se pueda recoger el codigo de error (y que no lo arroje una excepcion), se necesita tener desactivado el debug de mysql (por parte de CI)

			$this->db2->set(array('nombreTrasladista' => "", 'fecha' => $fecha, 'hora_inicio' => "05:00:00", 'hora_fin' => "22:00:00"));
			
			$result = $this->db2->insert('fechasTraslados');
			
			$returnvar = '';

			if($result)
			{
				$returnvar['result'] = TRUE;
				$returnvar['id'] = $this->db2->insert_id();
			}
			else
			{	
				$returnvar['result'] = FALSE;
				//Para CI 3.x
				//$returnvar['error'] = $this->db2->error()['code'];
				//Para CI 2.x
				$returnvar['error'] = $this->db2->_error_number();
			}

			return $returnvar;
		}

		//funcion para poder obtener el ID sin tener que insertar fecha (el puro ID)
		function obtener_id_traslado($fecha)
		{
			$this->db2->select('id');

			return $this->db2->get_where('fechasTraslados', array("fecha" => $fecha));
		}

		function asignar_trasladista($trasladista, $id, $horainicio, $horafin)
		{
			$set = array('hora_inicio' => $horainicio, 'hora_fin' => $horafin);
			if($trasladista != '')
				$set['nombreTrasladista'] = $trasladista;
			
			return $this->db2->update('fechasTraslados', $set, array('id' => $id));
		}

		//entrada: ID de traslado
		function obtener_configuracion($id)
		{
			$result = $this->db2->query("select paquetes.id, diaPaquete.paq_orden, horasTraslado, origen, destino from paquetes join diaPaquete on paquetes.id = diaPaquete.paquete join dias on diaPaquete.dia = dias.id where dias.numdia = ( select DAYOFWEEK( STR_TO_DATE( fecha, '%d-%c-%Y' ) ) - 1 from fechasTraslados where id = ? ) order by diaPaquete.paq_orden asc", array($id));

			return $result->result_array();
		}

		function crear_trasladoPaquete($traslado, $origen, $destino, $horas)
		{
			$this->db2->set(array("traslado" => $traslado, "origen" => $origen, "destino" => $destino, "horasTraslado" => $horas, "numserie1" => "", "numserie2" => ""));

			$result = $this->db2->insert("trasladoPaquete");

			if($result)
			{
				$result = $this->db2->insert_id();
			}
			//devuelve false o el ID (0 o un numero)
			return $result;
		}

		function asignar_trasladoPaquete($id, $numerosSerie)
		{

			return $this->db2->update('trasladoPaquete', $numerosSerie, array("id" => $id));
		}

		//------------------------------
		//
		//	Configuracion de Paquetes
		//
		//------------------------------

		/*//Explicacion en el stored procedure
		//Parametros:
		// 'id' : ID del paquete en la tabla intermedia
		// 'orden' : El ordenamiento destino que se le asignara al ID
		function config_cambiarOrden($id, $orden)
		{
			//Stored Procedure: Intenta cambiar el orden, si funciona trae 2 (el numero de registros modificados), y sino trae 0 (falso)
			//trae el resultado del SP, luego obtiene el registro y finalmente devuelve el (unico) resultado: respuesta
			return $this->db2->query("call changeOrder(?, ?)", array($id, $orden))->row()->respuesta;
		}*/

		//Trae los paquetes del dia, tal como dice
		function config_obtenerPaquetes($numdia)
		{
			//diaPaquete.id es el ID de la tabla intermedia, usado para administracion del orden
			//paquete es lo mismo que paquetes.id, que es para modificar el paquete singular
			$this->db2->select('paquete, horasTraslado, origen, destino, paq_orden');

			$this->db2->from('paquetes');
			$this->db2->join('diaPaquete', 'paquetes.id = paquete');
			$this->db2->join('dias', 'dia = dias.id');

			$this->db2->where('dias.numdia', $numdia);

			$this->db2->order_by('paq_orden', 'asc');

			$result = $this->db2->get();

			return $result->result_array();
		}

		//Trae todos los paquetes en existencia
		function config_catalogoPaquetes()
		{
			return $this->db2->get('paquetes')->result_array();
		}

		//Esta funcion guarda la relacion paquete-dia para marcar la configuracion al crear traslados
		//Primero vacia los registros de dicho dia y luego los vuelve a llenar con los datos nuevos
		function config_guardarDia($sets)
		{
			//primero se vacia dicho dia
			$this->db2->where("dia", $sets[0]['dia']);
			$this->db2->delete('diaPaquete');

			//luego se insertan los datos
			return $this->db2->insert_batch('diaPaquete', $sets);
		}

		//esta funcion crea un paquete nuevo (en la configuracion)
		function config_crearPaquete($set)
		{
			$returnvar;

			if($this->db2->insert('paquetes', $set))
				$returnvar = $this->db2->insert_id();
			else
				$returnvar = false;

			return $returnvar;
		}

		//esta funcion actualiza un paquete (en la configuracion)
		function config_guardarPaquete($set, $id)
		{
			return $this->db2->update('paquetes', $set, array("id" => $id));
		}

		//NOTA: Tiene la funcionalidad de que
		// ==>> elmimina todos los registros en la tabla intermedia <<== 
		// . . . que se enlacen al paquete eliminado
		//Y luego elimina dicho paquete
		function config_eliminarPaquete($id)
		{
			$successOrder = $this->db2->delete('diaPaquete', array("paquete" => $id));
			$successPackg = $this->db2->delete('paquetes', array("id" => $id));
			
			return $successPackg && $successOrder;
		}

	}
	
?>