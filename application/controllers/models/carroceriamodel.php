<?php

class Carroceriamodel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->crm = $this->load->database('crmtraslados', TRUE);
	}

    function agregar_siniestro($datos)
    {
        $this->db->insert('carroceria', $datos);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

	function agregar_bitacora($datos)
    {
        $this->db->insert('carroceria_bitacora', $datos);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

	function get_carroceria($id)
	{
		$sql = "SELECT c.car_id AS 'id', c.car_tipo+0 AS 'tipo', c.car_tipo AS 'tipo_descripcion',
                CONCAT_WS(' ',a.app_customerName,a.app_customerLastName) AS 'nombre_cliente',m.vem_name AS 'modelo',a.app_vehicleYear AS 'anio',
                c.car_poliza AS 'poliza', c.car_siniestro AS 'siniestro',c.car_fecha_recibido AS 'fecha_recibido',c.car_fecha_enviado AS 'fecha_enviado',
                c.car_refacciones AS 'refaccion', c.car_refacciones+0 AS 'refacciones_index',c.car_autorizado AS 'autorizado',
				c.car_fecha_autorizada AS 'fecha_autorizada', c.car_taller+0 AS 'talleres_index',c.car_taller AS 'taller',c.car_deducible AS 'deducible',
				sv.sev_vin AS 'vin', c.car_fecha_tentativa_entrega AS 'fecha_tentativa_entrega'
                FROM carroceria c
                INNER JOIN appointments a ON a.app_idAppointment = c.car_id_appointment
                LEFT JOIN vehicleModels m ON m.vem_idVehicleModel = a.app_vehicleModel
				LEFT JOIN serviceOrders s ON s.seo_IDapp = a.app_idAppointment
				LEFT JOIN serviceOrder_vehicleInformation sv ON sv.sev_idVehicleInformation = s.seo_idServiceOrder
                WHERE car_id = ".$id;

		return $this->db->query($sql)->row();
	}

	function get_comments($id)
	{
		$sql = "SELECT b.cb_id AS 'id', b.cb_id_carroceria AS 'id_carroceria', b.cb_bitacora AS 'bitacora', b.cb_fecha AS 'fecha',
				CONCAT_WS(' ',s.sus_name,s.sus_lastName) AS 'nombre'
				FROM carroceria_bitacora b
				INNER JOIN carroceria c ON c.car_id = b.cb_id_carroceria
				INNER JOIN susers s ON s.sus_idUser = cb_id_susers
				WHERE b.cb_is_comment = 1 AND b.cb_id_carroceria = ".$id;
		$sql.=" ORDER BY b.cb_fecha ASC";

		return $this->db->query($sql)->result();
	}

    function get_carrocerias($autorizados = null, $activos = 1)
    {
        $sql = "SELECT c.car_id AS 'id', c.car_tipo+0 AS 'tipo', c.car_tipo AS 'tipo_descripcion',
                CONCAT_WS(' ',a.app_customerName,a.app_customerLastName) AS 'nombre_cliente',m.vem_name AS 'modelo',a.app_vehicleYear AS 'anio',
                c.car_poliza AS 'poliza', c.car_siniestro AS 'siniestro',c.car_fecha_recibido AS 'fecha_recibido',c.car_fecha_enviado AS 'fecha_enviado',
                c.car_refacciones AS 'refaccion', c.car_refacciones+0 AS 'refacciones_index',c.car_autorizado AS 'autorizado',
				c.car_fecha_autorizada AS 'fecha_autorizada', c.car_taller+0 AS 'talleres_index',c.car_taller AS 'taller',c.car_deducible AS 'deducible',
				sv.sev_vin AS 'vin', c.car_fecha_tentativa_entrega AS 'fecha_tentativa_entrega'
                FROM carroceria c
                INNER JOIN appointments a ON a.app_idAppointment = c.car_id_appointment
                LEFT JOIN vehicleModels m ON m.vem_idVehicleModel = a.app_vehicleModel
				LEFT JOIN serviceOrders s ON s.seo_IDapp = a.app_idAppointment
				LEFT JOIN serviceOrder_vehicleInformation sv ON sv.sev_idVehicleInformation = s.seo_idServiceOrder
                WHERE app_IDtaller = ".$_SESSION["sfidws"]. " AND car_activo = ".$activos;

		if($autorizados != null) $sql.=" AND car_autorizado = ".$autorizados;

        return $this->db->query($sql)->result();
    }

	function update_carroceria($id,$datos)
    {
        // $this->db->where('car_id', $id);
        // $this->db->update('carroceria',$datos);
        // return ($this->db->affected_rows() != 1) ? false : true;

		$this->db->trans_start();
		$this->db->where('car_id', $id);
        $this->db->update('carroceria',$datos);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) return false; else return true;
    }

	function get_enum_field($tabla, $campo)
	{
		$type = $this->db->query( "SHOW COLUMNS FROM ".$tabla." WHERE Field = '".$campo."'" )->row( 0 )->Type;
	    preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
	    $enum = explode("','", $matches[1]);
	    return $enum;
	}
}

?>
