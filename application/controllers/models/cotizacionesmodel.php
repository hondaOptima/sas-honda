<?php

class Cotizacionesmodel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	// Crud de cotizaciones

	function obtener($id)
	{
		$query = $this->db->query(
			"SELECT *
			FROM estimates,  serviceOrders,susers
			WHERE est_idEstimate = ".$id."
			and est_service_order=seo_idServiceOrder
			and seo_adviser=sus_idUser"
			);

		return $query->result();
	}

	function updateFile($name, $estimate){
		$query = "update estimates set est_file = '".$name."' where est_idEstimate = ".$estimate;
		$exec = $this->db->query($query);
		return $exec;
	}

	function obtenerEmail($id)
	{
		$query = $this->db->query(
			"SELECT *
			FROM estimates,  serviceOrders,susers, serviceOrder_clientInformation
			WHERE est_idEstimate = ".$id."
			and est_service_order=seo_idServiceOrder
			and seo_adviser=sus_idUser
			and sec_idClientInformation = seo_idServiceOrder"
			);

		return $query->result();
	}

	function crear($cotizacion)
	{
		$this->db->insert('estimates', $cotizacion);
		return $this->db->insert_id();
	}

	function actualizar($cotizacion, $id)
	{
		$this->db->update('estimates', $cotizacion, array('est_idEstimate' => $id));
	}

	function borrar($id)
	{
		$this->db->query("DELETE FROM estimates WHERE est_idEstimate = ".$id);
	}

	// Crud de servicios

	function obtener_servicios_cotizacion($cotizacion)
	{
		$query = $this->db->query(
			"SELECT e.*
			FROM estimate_services as e
			WHERE ess_estimate = ".$cotizacion
			);

		return $query->result();
	}

	function crear_servicio($servicio)
	{
		$this->db->insert('estimate_services', $servicio);

	}

	function actualizar_servicio($servicio, $id)
	{
		$this->db->update('estimate_services', $servicio, array('ess_idservice' => $id));
	}


	function desconfirmar($id)
	{
		$this->db->query(
			"UPDATE estimates
			SET est_confirmed = 0
			WHERE est_idEstimate = ".$id
			);
	}

	function confirmar($id)
	{
		$this->db->query(
			"UPDATE estimates
			SET est_confirmed = 1
			WHERE est_idEstimate = ".$id
			);
	}


	// Consultas

	function obtener_ordenes_por_fecha($date,$taller)
	{
		$query = $this->db->query(
			"SELECT so.*, sc.*, sv.*, u.sus_name, u.sus_lastName, u.sus_adviserNumber,
            exists
            (
                select cam_ID from campanias, serviceOrder_vehicleInformation
                where cam_vin = sev_vin
                and sev_idVehicleInformation = seo_idServiceOrder
            ) as camp,
            exists
            (
                select cam_ID from campanias, serviceOrder_vehicleInformation
                where cam_vin = sev_vin
                and sev_idVehicleInformation = seo_idServiceOrder
                and (cam_status = 1 || cam_status = 2)
            ) as campGreen,
            exists(
                SELECT est_idEstimate
			    FROM estimates
			    WHERE est_service_order = seo_idServiceOrder
            ) as cotiza,
			exists
			(
				select * from garantia_exist ge
				where ge.vin = sev_vin
				and sev_idVehicleInformation = seo_idServiceOrder
				and ge.status = 1
			) as garexist,
			exists
			(
				select * from garantias ge
				where ge.vin = sev_vin
				and sev_idVehicleInformation = seo_idServiceOrder
				and ge.km_status != 'VENCIDO'
			) as extension
			FROM serviceOrders as so, pizarron as p, serviceOrder_clientInformation as sc, serviceOrder_vehicleInformation sv, susers as u
			WHERE piz_ID_orden = seo_idServiceOrder
			AND sec_idClientInformation = seo_idServiceOrder
			AND sev_idVehicleInformation = seo_idServiceOrder
			AND seo_adviser = sus_idUser
			AND sus_workshop='".$taller."'
			AND piz_fecha = '".$date."'
			ORDER BY seo_idServiceOrder DESC"
			);

		return $query->result();
	}

	function obtener_ordenes_por_asesor($asesor, $date,$taller)
	{
		$query = $this->db->query(
			"SELECT so.*, sc.*, sv.*, u.sus_name, u.sus_lastName, u.sus_adviserNumber
			FROM serviceOrders as so, pizarron as p, serviceOrder_clientInformation as sc, serviceOrder_vehicleInformation sv, susers as u
			WHERE piz_ID_orden = seo_idServiceOrder
			AND sec_idClientInformation = seo_idServiceOrder
			AND sev_idVehicleInformation = seo_idServiceOrder
			AND seo_adviser = sus_idUser
			AND piz_fecha = '".$date."'
			AND seo_adviser = '".$asesor."'
			AND sus_workshop='".$taller."'
			ORDER BY seo_idServiceOrder DESC"
			);

		return $query->result();
	}

	function obtener_cotizaciones_por_orden($orden)
	{
		$query = $this->db->query(
			"SELECT e.*, DATE_FORMAT(e.est_date,'%d %b %Y') as date
			FROM estimates as e
			WHERE est_service_order = ".$orden."
			ORDER BY est_idEstimate DESC"
			);

		return $query->result();
	}

	function obtener_cotizaciones_por_cliente($cliente)
	{
		$query = $this->db->query(
			"SELECT e.*
			FROM estimates as e
			WHERE est_customer = ".$cliente."
			ORDER BY est_idEstimate DESC"
			);

		return $query->result();
	}

	function obtener_info_cotizacion($orden)
	{
		$query = $this->db->query(
			"SELECT so.*, sc.*, sv.*, u.sus_name, u.sus_lastName, u.sus_adviserNumber
			FROM serviceOrders as so, pizarron as p, serviceOrder_clientInformation as sc, serviceOrder_vehicleInformation sv, susers as u
			WHERE piz_ID_orden = seo_idServiceOrder
			AND sec_idClientInformation = seo_idServiceOrder
			AND sev_idVehicleInformation = seo_idServiceOrder
			AND seo_adviser = sus_idUser
			AND piz_ID_orden = ".$orden
			);

		return $query->result();
	}

	function obtener_servicios()
	{
		$query= $this->db->query(
				'SELECT set_idServiceType, set_name
				FROM serviceTypes WHERE set_isActive = 1');
		$opciones = $query->result();
		$servicios = array();
			$servicios[0] = '';
		foreach($opciones as $opcion){
			$servicios[$opcion->set_idServiceType] = $opcion->set_name;
		}
		return $servicios;
	}

	function obtener_total_cotizacion($id)
	{
		$query= $this->db->query(
				'SELECT SUM(ess_total_price) as total
				FROM estimate_services
				WHERE ess_estimate = '.$id);

		return $query->result();
	}

}

?>
