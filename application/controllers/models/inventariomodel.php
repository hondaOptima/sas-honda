<?php

class Inventariomodel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->crm = $this->load->database('crmtraslados', TRUE);
	}

    function get_autos_piso()
    {
        if(empty($_SESSION['sfid'])) $cd = $_GET['cd'];
        else
        {
            if(!empty($_GET['cd'])) $cd = $_GET['cd'];
            else $cd = $_SESSION['sfworkshop'];
        }

		$and = $and_cd = "";

        if($_GET['status']=='8') {
            $and=" AND api_status IN (8,9) ";
        }
        elseif($_GET['status']=='4') {
            $and="AND api_status IN (4,9) ";
        }
        elseif($_GET['status']=='1000') {
            $and = "";
        }
        else {
            $and = ' AND api_status='.$_GET['status'].' ';
        }

		//if(!empty($_GET["id"])) $and.= " AND api_ID = '".$_GET["id"]."'";
		//if(!empty($_GET["vin"])) $and.= " AND api_vin = '".$_GET["vin"]."'";

        if($cd=='0') $and_cd=''; else $and_cd='AND  api_cd="'.$cd.'"';
        $sql = "SELECT api_ID AS 'id', api_fecha AS 'fecha', api_vin AS 'vin', api_usuario AS 'usuario',
				api_status AS 'status', api_danios AS 'danios', api_danios_desc AS 'danios_desc',
				api_cd AS 'ciudad', api_anio AS 'anio', api_modelo AS 'modelo', api_color AS 'color',
				api_salida_carroceria AS salida_carroceria, api_torre AS 'torre', api_ubicacion AS 'ubicacion',
				api_venta AS 'venta', api_motor AS 'motor', clave_radio
				from autos_en_piso where api_ID > 0 and api_venta = 0 ".$and."  ".$and_cd;
		return $this->crm->query($sql)->result();
    }

	function get_auto_piso()
	{
		$sql = "SELECT api_ID AS 'id',api_fecha AS 'fecha',api_vin AS 'vin', api_usuario AS 'usuario', api_status AS 'status',
				api_danios AS 'danios', api_danios_desc AS 'danios_desc', api_cd AS 'cd', api_anio AS 'anio',
				api_modelo AS 'modelo', api_color AS 'color', api_salida_carroceria AS 'salida_carroceria', api_torre AS 'torre',
				api_ubicacion AS 'ubicacion', api_venta AS 'venta', api_motor AS 'motor'
				FROM autos_en_piso WHERE 1 = 1 ";

		if(!empty($_GET["vin"])) $sql.=" AND api_vin = '".$_GET["vin"]."'";
		if(!empty($_GET["id"])) $sql.=" AND api_ID = '".$_GET["id"]."'";

		return $this->crm->query($sql)->row();
	}

	function get_traslado()
	{
		$sql = "SELECT tra_ID AS 'id', tra_fecha AS 'fecha', tra_vin AS 'vin', tra_origen AS 'origen',
		tra_destino AS 'destino', tra_fecha_salida AS 'fecha_salida', tra_hora_salida AS 'hora_salida',
		tra_fecha_llegada AS 'fecha_llegada',tra_status AS 'status',tra_trasladista AS 'trasladista',
		tra_marca AS 'marca',tra_modelo AS 'modelo',tra_anio AS 'anio',tra_color AS 'color',tra_motor AS motor,
		tra_desc AS 'desc',tra_imagen AS 'imagen',tra_age AS 'age',tra_fandi AS 'fandi',tra_servicio AS 'servicio',
		tra_ventas AS 'ventas',tra_logistica AS 'logistica',tra_poliza AS 'poliza'
		FROM traslado WHERE tra_vin = '".$_GET["vin"]."'";
		return $this->crm->query($sql)->row();
	}
}

?>
