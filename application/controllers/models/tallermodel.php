<?php

class Tallermodel extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

    //Regresa el listado de los talleres.
    //Tabla: workshops.
    function get_talleres() {
        $sql = "SELECT wor_idWorkshop AS 'id',wor_city AS 'city',wor_productivityRate AS 'rate' FROM workshops ORDER BY wor_city";
        return $this->db->query($sql)->result();
    }

    //Regresa el registro de un taller.
    //Tabla: workshops.
    //Parámetros: id.
    function get_taller($id) {
        $sql = "SELECT wor_idWorkshop AS 'id',wor_city AS 'city',wor_productivityRate AS 'rate' FROM workshops WHERE wor_idWorkshop = ".$id;
        return $this->db->query($sql)->row();
    }
}

?>
