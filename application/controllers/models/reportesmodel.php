<?php

class Reportesmodel extends CI_Model {

	public function __construct() {
		parent::__construct();
	}


	function getianalisis($taller) {
		$query = $this->db->query("SELECT * FROM ianalisis where ia_archivocsv='".$taller."' ");
		return $query->result();
	}

	function statustorres($taller) {
		$query = $this->db->query("SELECT * FROM torre where tor_agencia='".$taller."'");
		return $query->result();
	}

	function getResumenCd($cd,$mes) {
		if($cd=='Mexicali') $cd='mex';
		if($cd=='Tijuana') $cd='tij';
		if($cd=='Ensenada') $cd='ens';
		echo $que="SELECT * FROM resumen_semaforo where rss_cd='".$cd."'";
		$query = $this->db->query($que);
		return $query->result();
	}

	function getResumen($taller,$fecha,$tipo) {
		$query = $this->db->query("SELECT * FROM resumen_semaforo where   rss_cd='".$taller."' and rss_tipo='".$tipo."' and MONTH(rss_mes)=MONTH(NOW()) ");
		return $query->result();
	}

	function getSinCero($taller,$file) {
		$q="SELECT * FROM  inv_sin_exi_cero where isc_cd='".$taller."' and isc_file='".$file."'";
		$query = $this->db->query($q);
		return $query->result();
	}


	function getVentaPerdidaMes($taller,$mes,$anio) {
		$query = $this->db->query("
		SELECT * FROM hits
		where hit_taller='".$taller."' and hit_date ='".$mes.$anio."' ");
		return $query->result();
	}

	function getVentaPerdida($taller) {
		$time = strtotime("2015-05-19");
		$final = date("Y-m-d", strtotime("-12 month", $time));
		list($an,$ms,$di)=explode('-',$final);


		$query = $this->db->query("
		SELECT * FROM hits
		where hit_taller='".$taller."' and hit_date >='".$ms.$an."' ");
		return $query->result();
	}

	function updatefecha($id,$fecha){

	}

	function updateProfit($dato,$id) {
    	$this->db->update('profit', $dato, array('pro_ID_iec' => $id));
	}

	function update_traslado($id,$datos)
	{
		$crm = $this->load->database('crmtraslados', TRUE);

		$crm->trans_start();
		$crm->where('tra_ID', $id);
		$crm->update('traslado ', $datos);
		$crm->trans_complete();

		if ($crm->trans_status() === FALSE) return false;
		else return true;

	}


	function getNoPartes($taller,$pibote) {
		$lista='';
		for($i=0; $i<12; $i++) {
			$time = strtotime("".date('Y')."-".$pibote."");
			$final = date("Y-m", strtotime("-".$i." month", $time));
			list($aa,$mm)=explode('-',$final);
			$lista.="COUNT(CASE WHEN hit_date ='".$mm.$aa."' THEN hit_noparte END) AS ".$this->Reportesmodel->mes($mm).",";
		}
		$lista;
		echo  $qu="
		SELECT
		hit_noparte,
		".$lista."
		SUM(CASE WHEN hit_date =".$pibote.date('Y')." THEN hit_piezas END) AS piezas,
		max(hit_costo) AS costo
		FROM hits
		GROUP BY hit_noparte;";

		$query = $this->db->query($qu);
		return $query->result();

		/*$query = $this->db->query("
		SELECT
		hit_noparte,
		COUNT(CASE WHEN hit_date = 042015 THEN hit_noparte END) AS abril,
		COUNT(CASE WHEN hit_date = 032015 THEN hit_noparte END) AS marzo,
		COUNT(CASE WHEN hit_date = 022015 THEN hit_noparte END) AS febrero,
		COUNT(CASE WHEN hit_date = 012015 THEN hit_noparte END) AS enero,
		COUNT(CASE WHEN hit_date = 122014 THEN hit_noparte END) AS diciembre,
		COUNT(CASE WHEN hit_date = 112014 THEN hit_noparte END) AS noviembre,
		COUNT(CASE WHEN hit_date = 102014 THEN hit_noparte END) AS octubre,
		COUNT(CASE WHEN hit_date = 092014 THEN hit_noparte END) AS septiembre,
		COUNT(CASE WHEN hit_date = 082014 THEN hit_noparte END) AS agosto,
		COUNT(CASE WHEN hit_date = 072014 THEN hit_noparte END) AS julio,
		COUNT(CASE WHEN hit_date = 062014 THEN hit_noparte END) AS junio,
		COUNT(CASE WHEN hit_date = 052014 THEN hit_noparte END) AS mayo,
		SUM(CASE WHEN hit_date = 042015 THEN hit_piezas END) AS piezas
		FROM hits
		GROUP BY hit_noparte;


		");
		return $query->result();*/
	}

	function lista($pibote){
		$total=$pibote + 12;
	}

	function mes($mes) {
		if($mes==1) $n_mes='enero';
		if($mes==2) $n_mes='febrero';
		if($mes==3) $n_mes='marzo';
		if($mes==4) $n_mes='abril';
		if($mes==5) $n_mes='mayo';
		if($mes==6) $n_mes='junio';
		if($mes==7) $n_mes='julio';
		if($mes==8) $n_mes='agosto';
		if($mes==9) $n_mes='septiembre';
		if($mes==10) $n_mes='octubre';
		if($mes==11) $n_mes='noviembre';
		if($mes==12) $n_mes='diciembre';
		return $n_mes;
	}

	function getNoPartesMes($taller,$pibote) {
		$lista='';
		for($i=0; $i<12; $i++)
		{
			$time = strtotime("".(date('Y')-1)."-".$pibote."");
			$final = date("Y-m", strtotime("-".$i." month", $time));
			list($aa,$mm)=explode('-',$final);
			$lista.="COUNT(CASE WHEN hit_date ='".$mm.$aa."' THEN hit_noparte END) AS ".$this->Reportesmodel->mes($mm).",";
		}
		$lista;
		echo $qu="
		SELECT
		hit_noparte,
		".$lista."
		SUM(CASE WHEN hit_date ='".$pibote."2016' THEN hit_piezas END) AS piezas,
		func_get_costo(hit_noparte) AS costo
		FROM hits
		where hit_taller=".$taller."
		GROUP BY hit_noparte
		order by hit_date asc
		";

		$query = $this->db->query($qu);
		return $query->result();

		/*
		SELECT
		hit_noparte,
		COUNT(CASE WHEN hit_mes = 4 THEN hit_noparte END) AS abril,
		COUNT(CASE WHEN hit_mes = 3 THEN hit_noparte END) AS marzo,
		COUNT(CASE WHEN hit_mes = 2 THEN hit_noparte END) AS febrero,
		COUNT(CASE WHEN hit_mes = 1 THEN hit_noparte END) AS enero,
		COUNT(CASE WHEN hit_mes = 12 THEN hit_noparte END) AS diciembre,
		COUNT(CASE WHEN hit_mes = 11 THEN hit_noparte END) AS noviembre,
		COUNT(CASE WHEN hit_mes = 10 THEN hit_noparte END) AS octubre,
		COUNT(CASE WHEN hit_mes = 9 THEN hit_noparte END) AS septiembre,
		COUNT(CASE WHEN hit_mes = 8 THEN hit_noparte END) AS agosto,
		COUNT(CASE WHEN hit_mes = 7 THEN hit_noparte END) AS julio,
		COUNT(CASE WHEN hit_mes = 6 THEN hit_noparte END) AS junio,
		COUNT(CASE WHEN hit_mes = 5 THEN hit_noparte END) AS mayo,
		SUM(CASE WHEN hit_mes = 4 THEN hit_piezas END) AS piezas,
		max(hit_costo) AS costo
		FROM hits
		GROUP BY hit_noparte;
		*/
	}

function getSemaforo($cd,$file,$tipo,$fecha,$fecha_b) {
	if($tipo=='morado') $and='and cos_file="'.$file.'" and cos_ult_s="0000-00-00" ';
	if($tipo=='rojo') $and='and cos_file="'.$file.'" and cos_ult_s!="0000-00-00" and cos_ult_s<="'.$fecha.'"   ';
	if($tipo=='ama') $and='and cos_file="'.$file.'" and cos_ult_s!="0000-00-00" and cos_ult_s>="'.$fecha.'" and cos_ult_s<="'.$fecha_b.'"   ';
	if($tipo=='ver') $and='and cos_file="'.$file.'" and cos_ult_s!="0000-00-00" and cos_ult_s>="'.$fecha.'"   ';
	$consulta="SELECT * FROM lista_semaforo where cos_ciudad='".$cd."' ".$and."  ";
	$query = $this->db->query($consulta);
	return $query->result();
}

	function costoparte($parte) {
		$consulta="SELECT hit_costo FROM hits where hit_noparte='".$parte."' ORDER BY  `hits`.`hit_IDhits` DESC LIMIT 1 ";
		$query = $this->db->query($consulta);
		return $query->result();
	}


	function getCitas($taller,$mesi,$mesf) {
		$qu="
		SELECT app_idAppointment,app_day,app_status,app_referencia,app_folio,app_customerArrival
		FROM v_listadecitas,calendario
		where
		app_idAppointment=cal_app_idAppointment
		and app_IDtaller=".$taller." and app_day between '".$mesi."' and '".$mesf."'
		and app_status!=2
		";
		$query = $this->db->query($qu);
		return $query->result();
	}

	function getCitasSin($taller,$mesi,$mesf) {
		$qu="
		SELECT app_idAppointment,app_day,app_status,app_referencia
		FROM v_listadecitas
		where
		app_IDtaller=".$taller." and app_day between '".$mesi."' and '".$mesf."'
		and app_status!=2
		and app_referencia=2

		";
		$query = $this->db->query($qu);
		return $query->result();
	}


	function getProfitPrint($id) {
		$qu="
		select * from profit where pro_ID_iec=".$id."
		";
		$query = $this->db->query($qu);
		return $query->result();
	}

	function insertProfit($usuario) {
		$this->db->insert('profit', $usuario);
	}

	function insertResumen($tipo,$cd,$parte,$pieza,$valor,$des) {
		$info=array(
			'rss_ID_desc'=>$tipo,
			'rss_mes'=>date('Y-m-d'),
			'rss_cd'=>$cd,
			'rss_partes'=>$parte,
			'rss_piezas'=>$pieza,
			'rss_valor'=>$valor,
			'rss_tipo'=>$des
		);
		$this->db->insert('resumen_semaforo', $info);
	}

	function insertCsvIntelisis($usuario) {
		$this->db->insert('csv_intelisis', $usuario);
	}

	function insertHits($data) {
		$this->db->insert('hits', $data);
	}

	function insertInvExiCero($data) {
		$this->db->insert('inv_exi_cero', $data);
	}

	function InvExiCero() {
		$query= $this->db->query(
		'
		SELECT *
		FROM inv_exi_cero where YEAR(iec_fecha)=YEAR(NOW())
		');
		return $query->result();
	}

	function updateStatus($ID) {
		$query= $this->db->query(
		'
		update inv_exi_cero set iec_procesado="2" where iec_ID='.$ID.'
		');
		return true;
	}

	function InvExiCeroID($id) {
		$query= $this->db->query(
		'
		SELECT *
		FROM inv_exi_cero
		where iec_ID='.$id.'
		');
		return $query->result();
	}

	function getCsvIntelisis($per) {
		$query= $this->db->query(
		'
		SELECT *
		FROM csv_intelisis
		WHERE 	cin_status="on"	 and cin_ciudad='.$per.'
		');
		return $query->result();
	}

	function inventario() {
		$query= $this->db->query(
		'
		SELECT *
		FROM costo_de_venta
		');
		return $query->result();
	}

	function deletefile($per) {
		$this->db->delete('csv_intelisis', array('cin_file' => $per));
	}

	function deleteHit($per) {
		$this->db->delete('hits', array('hit_IDhits' => $per));
	}

	function eliminarFile($per) {
		$this->db->delete('inv_exi_cero', array('iec_file' => $per));
		$this->db->delete('costo_de_venta', array('cos_file' => $per));
	}

	function getAsesores() {
		$instruccion = $this->db->query('SELECT sus_idUser,sus_name FROM susers WHERE sus_rol = 3  ORDER BY sus_name');
		return $instruccion->result();
	}

	function getSucursales() {
		$instruccion = $this->db->query('SELECT `wor_idWorkshop`,`wor_city` FROM workshops');
		return $instruccion->result();
	}

	function getHistorialCitas($idSucursal,$fechai,$fechaf) {
		$instruccion = $this->db->query("SELECT wor_city,sus_name,sus_lastName,aud_fecha,app_idAppointment,app_status,app_customerName,app_customerLastName,app_customerEmail,app_customerTelephone
						FROM auditoria
						INNER JOIN appointments ON aud_ID_tabla = app_idAppointment
						INNER JOIN susers ON aud_usuario = sus_idUser
						INNER JOIN workshops ON sus_workshop = wor_idWorkshop
						WHERE sus_workshop =".$idSucursal."
						AND aud_fecha >= '".$fechai."'
						AND aud_fecha <= '".$fechaf."'");
		return $instruccion->result();
	}

	function torres($torre) {
	//$instruccion = $this->db->query("INSERT INTO `torre` (`tor_ID`, `tor_agencia`, `tor_status`, `tor_torre`, `tor_vin`, `tor_fecha`) VALUES (NULL, 'Ensenada', 'on', '".$torre."', '', '');");

	}

	function get_entregas($ciudad) {
		//$ciudad = $_GET['ciudad'];
		$crm = $this->load->database('crmtraslados', TRUE);
		$sql = "SELECT c.cen_ID AS 'id',c.cen_fecha_creada AS 'fecha_creada', c.cen_hora AS 'hora', c.cen_vin AS 'vin', c.cen_cliente AS 'cliente',c.cen_auto AS 'auto', c.cen_sala AS 'sala',CONCAT_WS(' ',a.hus_nombre,a.hus_apellido) AS 'nombre'
				FROM calendario_entregas c
				LEFT JOIN asesores_empleados a ON c.cen_asesor = a.hus_IDhuser
				WHERE c.cen_fecha_entrega = '".date("Y-m-d")."' AND cen_agencia = '".$ciudad."'
				ORDER BY cen_hora;";
		return $crm->query($sql)->result();
	}

	function get_traslados($ciudad) {
		$crm = $this->load->database('crmtraslados', TRUE);
		$sql = "SELECT t.tra_fecha_salida AS hora, CONCAT_WS(' ',t.tra_marca,t.tra_modelo) AS 'auto',
				t.tra_trasladista AS 'nombre',t.tra_vin AS 'vin', a.api_ubicacion AS 'almacen'
				FROM traslado t
				LEFT JOIN autos_en_piso a ON a.api_vin = t.tra_vin
				WHERE t.tra_fecha_salida = '".date("Y-m-d")."' AND t.tra_origen = '".$ciudad."'";

		return $crm->query($sql)->result();
	}

	// function get_traslados_salida($ciudad = null)
	// {
	// 	$crm = $this->load->database('crmtraslados', TRUE);
	// 	$sql = "SELECT * FROM traslado WHERE tra_ID > 0 AND (tra_fecha_salida >= DATE_ADD(CURDATE(), INTERVAL '-15' DAY) or tra_status<2) ";
	// 	if($ciudad != null)
	// 		$sql.=" AND tra_origen = '".$ciudad."'";
	// 	return $crm->query($sql)->result();
	// }
	//
	// function get_traslados_entrada($ciudad = null)
	// {
	// 	$crm = $this->load->database('crmtraslados', TRUE);
	// 	$sql = "SELECT * FROM traslado WHERE tra_ID > 0 AND (tra_fecha_salida >= DATE_ADD(CURDATE(), INTERVAL '-15' DAY) or tra_status<2) ";
	// 	if($ciudad != null)
	// 		$sql.=" AND tra_destino = '".$ciudad."'";
	// 	return $crm->query($sql)->result();
	// }

	function get_traslados_tipo($ciudad = null,$tipo = null)
	{
		$crm = $this->load->database('crmtraslados', TRUE);
		$sql = "SELECT * FROM traslado WHERE tra_ID > 0 AND (tra_fecha_salida >= DATE_ADD(CURDATE(), INTERVAL '-15' DAY) or tra_status<2) ";
		if($tipo != null && $ciudad != null)
		{
			if($tipo == "salida")
				$sql.=" AND tra_origen = '".$ciudad."'";
			else if($tipo == "entrada")
				$sql.=" AND tra_destino = '".$ciudad."'";
		}

		return $crm->query($sql)->result();
	}

	function get_traslados_origen_y_destino($ciudad)
	{
		$crm = $this->load->database('crmtraslados', TRUE);
		$sql = "SELECT * FROM traslado WHERE tra_ID > 0 AND tra_fecha_salida >= DATE_ADD(CURDATE(), INTERVAL '-5' DAY)
				AND tra_origen != '".$ciudad."' and tra_destino != '".$ciudad."'";

		return $crm->query($sql)->result();
	}
}
?>
