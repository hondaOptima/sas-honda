<?php

class Citamodel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->load->library('email');
		$this->crm = $this->load->database('crmtraslados', TRUE);
	}

	function oper($taller)
	{
		$query = $this->db->query("SELECT * FROM workshop_capabilities WHERE woc_workshop=".$taller);
		return $query->result();
	}

	function insertAuditoria($idc)
	{
		$info = array(
			'aud_usuario' => $_SESSION['sfid'],
			'aud_descripcion' => 'Creacion de nueva Cita',
			'aud_fecha' => date("Y-m-d H:i:s"),
			'aud_status' => 0,
			'aud_tabla' => 'appointments',
			'aud_ID_tabla' => $idc,
		);
		$this->db->insert('auditoria', $info);
	}

	function checkCalendario($date,$taller)
	{
		$query = $this->db->query("SELECT cpo_ID FROM calendario_porcentaje WHERE cpo_fecha='".$date."' and cpo_wor_idWorkshop=".$taller);
		return $query->result();
	}

	function obtenerPermiso($user)
	{
		$query = $this->db->query("SELECT * FROM enproceso WHERE enp_ID_asesor=".$user." and enp_status=1");
		return $query->result();
	}

	function revisarCitas($date,$taller)
	{
		$query = $this->db->query("SELECT app_idAppointment FROM v_listadecitas WHERE app_status != 2 AND app_day='".$date."' AND app_IDtaller=".$taller);
		return $query->result();
	}

function porcentaje($date,$oper)
{
	$d = date('N',strtotime($date));
	if($d == 6) $di=$oper[0]->woc_weekends_service_hours;
	else $di=$oper[0]->woc_weekdays_service_hours;
	return ($di * $oper[0]->woc_tecnicos * $oper[0]->woc_productivity_factor);
}

	function listarCitas($date)
	{
		$query = $this->db->query("
				SELECT * FROM appointments, susers, reminderTypes, vehicleModels, serviceTypes WHERE
				sus_idUser = app_desiredAdviser AND rem_idReminderType = app_reminderType AND
				vem_idVehicleModel = app_vehicleModel AND set_idServiceType = app_service
				AND app_onlineConfirmed = 1 AND DATE_FORMAT(app_day,'%Y-%m-%d') = '".$date."'");
		return $query->result();
	}

	function listaPorcentajeCitas($id)
	{
		$date = date('m');
		$nfecha = ''.date('Y').'-'.$date.'-01';
		$query = $this->db->query("SELECT * FROM calendario_porcentaje WHERE cpo_ver=1 AND cpo_wor_idWorkshop=".$id." AND cpo_fecha>='".$nfecha."'");
		return $query->result();
	}

	function citasPorHora($date,$taller)
	{
		$query = $this->db->query("SELECT  cal_ID, app_idAppointment, cal_status, cal_hora, sus_name, sus_lastName, app_customerArrival, app_message, app_customerName, app_customerLastName, app_vehicleYear, app_status, app_confirmed, app_folio, vem_name, set_name, rem_name,app_tipo_telefono,set_codigo,app_customerTelephone, app_timeAttended, app_timeToOrder
									FROM calendario
									LEFT JOIN v_listadecitas
									ON  `cal_app_idAppointment` =  `app_idAppointment`
									WHERE  `cal_fecha` =  '".$date."'
									AND cal_wor_idWorkshop=".$taller."
									ORDER BY  `cal_hora` ASC
		");
		return $query->result();
	}

		function citasPorHoraSinCita($date,$taller) {
			$query = $this->db->query("SELECT
					app_idAppointment,
					app_status,
					app_hour,
					app_folio,
					app_customerName,
					app_customerLastName,
					app_customerTelephone,
					app_vehicleYear,
					sus_idUser,
					sus_name,
					sus_lastName,
					set_codigo,
					vem_name,
					set_name
					FROM v_listadecitas
					WHERE  `app_day` =  '".$date."'
					AND app_IDtaller=".$taller."
					AND app_referencia='2'
					AND app_status !=2
					ORDER BY  `app_hour` ASC
			");
			return $query->result();
		}

	function citasPorHoraTot($date,$taller)
	{
		$query = $this->db->query("SELECT  SUM(ser_approximate_duration) as tot
									FROM calendario
									LEFT JOIN v_listadecitas
									ON  `cal_app_idAppointment` =  `app_idAppointment`
									WHERE  `cal_fecha` =  '".$date."'
									AND cal_wor_idWorkshop=".$taller."
		");

		return $query->result();
	}

	function citasPorHoraTotSin($date,$taller)
	{
		$query = $this->db->query("SELECT  SUM(ser_approximate_duration) as tot FROM  v_listadecitas
									WHERE  `app_day` =  '".$date."' AND app_IDtaller=".$taller."
									AND app_referencia=2 AND app_status!=2");
		return $query->result();
	}

	function citasPorHoraAse($date,$taller,$ase)
	{
		if($ase == 0) $add = '';
		else $add='and sus_idUser="'.$ase.'"';
		$query = $this->db->query("SELECT  * FROM calendario
									LEFT JOIN v_listadecitas ON  `cal_app_idAppointment` =  `app_idAppointment`
									WHERE  `cal_fecha` =  '".$date."'
									AND cal_wor_idWorkshop=".$taller."
									".$add."ORDER BY  `cal_hora` ASC");
		return $query->result();
	}

	function listarCitasPorConfirmar($date){

		$query = $this->db->query("
				SELECT *
				FROM appointments, susers, reminderTypes, vehicleModels, serviceTypes
				WHERE
				sus_idUser = app_desiredAdviser
				AND
				rem_idReminderType = app_reminderType
				AND
				vem_idVehicleModel = app_vehicleModel
				AND
				set_idServiceType = app_service
				AND
				app_onlineConfirmed = 0
				AND
				DATE_FORMAT(app_day,'%Y-%m-%d') = '".$date."'");
		return $query->result();
	}

	function listarPorConfirmarAjax(){
		$query = $this->db->query("
			  SELECT DATE_FORMAT(app.app_day,'%Y-%m-%d') as date, app.app_customerName as client_first_name, app.app_customerLastName as client_last_name
			  FROM appointments as app
			  WHERE app_onlineConfirmed = 0
		  ");
		return $query->result();
	}

	function revisarFechas($date,$taller){
		$query= $this->db->query(
				'
				SELECT *
FROM  `calendario_porcentaje`
WHERE  `cpo_fecha` >  "'.$date.'"
AND cpo_wor_idWorkshop ='.$taller.'
ORDER BY  `calendario_porcentaje`.`cpo_ID` ASC
				');
		 return $query->result();
		}


	function horaswd($cd,$tipo){

		$query= $this->db->query(
				'
				SELECT *
				FROM horas,workshops
				WHERE
				hor_wor_idWorkshop='.$cd.'
				and wor_idWorkshop = hor_wor_idWorkshop
				and hor_dia='.$tipo.'
				ORDER BY  `horas`.`hor_hora` ASC
				');
		 return $query->result();
	}

	function agregarCita($cita)
	{
		$this->db->insert('appointments', $cita);
		return $this->db->insert_id();
	}

	function insertNuevaHora($info)
	{
        $this->db->insert('calendario', $info);
	}

	function insertHorasTaller($fecha,$taller)
	{
		$info=array(
			'cpo_fecha'=>$fecha,
			'cpo_wor_idWorkshop'=>$taller,
			'cpo_status'=>1,
			'cpo_ver'=>1
		);

		$this->db->insert('calendario_porcentaje', $info);
	}



	function actualizarCita($cita, $id){
         $this->db->update('appointments', $cita, array('app_idAppointment' => $id));
	}

	function updateCalendario($date, $taller){
        $query= $this->db->query(
				'UPDATE appointments
				SET app_referencia=4
				WHERE app_day="'.$date.'" and app_IDtaller="'.$taller.'"

				');
	}

	function updateCalendarioCitas($data, $id){
         $this->db->update('calendario', $data, array('cal_ID' => $id));
	}

	function eliminarHorasCalendario($date,$taller){
        $query= $this->db->query(
				'DELETE FROM calendario WHERE cal_fecha="'.$date.'" and cal_wor_idWorkshop="'.$taller.'" and cal_status=1

				');
	}

	function obtenerCita($id){
		$query= $this->db->query(
				'SELECT *
				FROM appointments,services,serviceTypes,calendario
				WHERE
				app_idAppointment = '.$id.'
				AND app_service=ser_serviceType
				and app_vehicleModel=ser_vehicleModel
				AND ser_serviceType=set_idServiceType
				AND cal_app_idAppointment=app_idAppointment
				');
		 return $query->result();
	}

	function updateCalendarioFestivo($date,$taller){

		$query= $this->db->query(
				'update calendario
				set cal_status=2
				WHERE
				cal_fecha="'.$date.'"
				and cal_wor_idWorkshop='.$taller.'
				');
	}

	function ocupado($id)
	{
		$query= $this->db->query('SELECT * FROM calendario WHERE cal_ID='.$id);
		 return $query->result();
	}

	function updateCalendarioPorcentaje($date,$taller)
	{
		$query= $this->db->query(
				'update calendario_porcentaje
				set cpo_status=2
				WHERE
				cpo_fecha="'.$date.'"
				and cpo_wor_idWorkshop='.$taller.'
		');
	}

	function obtenerSinCita($id)
	{
		$query= $this->db->query(
								'SELECT *
								FROM appointments,services,serviceTypes
								WHERE
								app_idAppointment ='.$id.'
								AND app_service=ser_serviceType
				                AND `app_vehicleModel`=ser_vehicleModel
								AND ser_serviceType=set_idServiceType
		');
		 return $query->result();
	}

	function confirmarCita($id)
	{
		$query = $this->db->query('UPDATE appointments SET app_confirmed = 1 WHERE app_idAppointment = '.$id);
	}

	function confirmarCitaOnline($id)
	{
		$query = $this->db->query('UPDATE appointments SET app_onlineConfirmed = 1 WHERE app_idAppointment = '.$id);
	}

	function liberarHora($id)
	{
		$query = $this->db->query('UPDATE calendario SET cal_app_idAppointment = 0, cal_status=1 WHERE cal_ID = '.$id);
	}

	function reagendarNuevaHora($idhora,$idApp)
	{
		$query = $this->db->query('UPDATE calendario SET cal_app_idAppointment = '.$idApp.' WHERE cal_ID = '.$idhora.'');
	}

	function cancelarCita($id)
	{
		$query = $this->db->query('UPDATE appointments SET app_status = 2 WHERE app_idAppointment = '.$id);
	}

	function  obtenerAsesoresFecha($fecha,$hora)
	{
		$instruccion = 'SELECT sus_idUser, sus_name, sus_lastName FROM calendario
						inner join appointments on app_idAppointment = cal_app_idAppointment
						inner join susers on sus_idUser = app_desiredAdviser
						WHERE cal_fecha = "'.$fecha.'" and cal_hora = "'.$hora.'"
						AND sus_workshop='.$_SESSION['sfidws'];
		return $this->db->query($instruccion)->result();
	}

	function obtenerAs()
	{
		$instruccion ='SELECT sus_idUser, sus_name, sus_lastName FROM susers WHERE sus_rol = 3
						AND sus_workshop='.$_SESSION['sfidws'].' AND sus_isDeleted=0 AND sus_isActive=1';
		return $this->db->query($instruccion)->result();
	}

	function obtenerAsesores()
	{
		$query = $this->db->query('SELECT sus_idUser, sus_name, sus_lastName FROM susers WHERE
									sus_rol = 3 AND sus_workshop='.$_SESSION['sfidws'].' AND sus_isDeleted = 0 AND sus_isActive = 1
		');

		$opciones = $query->result();
		$asesores = array();
		foreach($opciones as $opcion)
			$asesores[$opcion->sus_idUser] = $opcion->sus_name.' '.$opcion->sus_lastName;
		return $asesores;
	}

	function obtenerAsesoresCarroceria()
	{
		$query = $this->db->query("SELECT s.sus_idUser,s.sus_name,s.sus_lastName FROM susers s
									WHERE s.sus_rol = 8
									AND s.sus_isActive = 1
									AND s.sus_isDeleted = 0
									AND s.sus_workshop = '".$_SESSION['sfidws']."'
									ORDER BY s.sus_name,s.sus_lastName");

		$opciones = $query->result();
		$asesores = array();
		foreach($opciones as $opcion)
			$asesores[$opcion->sus_idUser] = $opcion->sus_name.' '.$opcion->sus_lastName;
		return $asesores;
	}

	function obtenerServicios()
	{
		$query= $this->db->query('SELECT set_idServiceType, set_name FROM serviceTypes where set_isActive = 1');
		$opciones = $query->result();
		$servicios = array();
		foreach($opciones as $opcion)
			$servicios[$opcion->set_idServiceType] = $opcion->set_name;
		return $servicios;
	}

	function listadeServicios()
	{
		$query = $this->db->query('SELECT set_idServiceType, set_name FROM serviceTypes where set_isActive = 1');
		return $query->result();
	}

	function obtenerVehiculos()
	{
		$query = $this->db->query('SELECT veh_idVehicle, veh_model, veh_year FROM vehicles');
		$opciones = $query->result();
		$vehiculos = array();
		foreach($opciones as $opcion)
			$vehiculos[$opcion->veh_idVehicle] = $opcion->veh_model.'-'.$opcion->veh_year;
		return $vehiculos;
	}

	function obtenerRecordatorios()
	{
		$query= $this->db->query('SELECT rem_idReminderType, rem_name FROM reminderTypes');
		$opciones = $query->result();
		$recordatorios = array();
		foreach($opciones as $opcion)
			$recordatorios[$opcion->rem_idReminderType] = $opcion->rem_name;
		return $recordatorios;
	}

	function obtenerModelos()
	{
		$query= $this->db->query('SELECT vem_idVehicleModel, vem_name FROM vehicleModels where vem_isActive=1 ORDER BY  `vehicleModels`.`vem_name` ASC ');
		$opciones = $query->result();
		$modelos = array();
		$modelos['']='Seleccione una Opción';
		foreach($opciones as $opcion)
			$modelos[$opcion->vem_idVehicleModel] = $opcion->vem_name;
		return $modelos;
	}

	function sendEmailCitaOnline($toemail,$fecha,$hora,$servicio,$modelo,$anio,$nombre,$apellido)
	{
		$this->email->set_mailtype("html");
		$this->email->from('info@webmarketingnetworks.com', 'Honda Optima - Servicios');
		$this->email->to($toemail);
		$this->email->subject('Registro de Cita | Honda Optima');
		$this->email->message('
								<table>
								  <tr>
									<td></td>
								  </tr>
								</table>
								<br>
								<table>
								  <tr>
									<td>Datos del Registro de la cita para: '.$nombre.' '.$apellido.' </td>
									<td></td>
								  </tr>
								</table>
								<br>
								<table>
								  <tr>
									<td></td>
								  </tr>
								</table>

								<br>
								<br>
								<table>

								  <tr>
									<td><b>Fecha:</b></td>
									<td> '.$fecha.'</td>
								  </tr>
								  <tr>
									<td><b>Hora:</b></td>
									<td> '.$hora.'</td>
								  </tr>
								   <tr>
									<td><b>Servicio:</b></td>
									<td> '.$servicio.'</td>
								  </tr>
								   <tr>
									<td><b>Vehículo:</b></td>
									<td> '.$modelo.' '.$anio.'</td>
								  </tr>
								</table>
								<br>
								<br>
								<hr/>
								<table>
								  <tr>
									<td></td>
								  </tr>
								</table>
								');
		$this->email->send();
	}

	function deleteHora($id)
	{
		$this->db->delete('calendario', array('cal_ID' => $id));
	}

	function listaUsuarios($per)
	{
		$query= $this->db->query('SELECT * FROM susers,userTypes,permisos, roles, workshops WHERE
									ust_idUserType = sus_userType AND rol_idRol = sus_rol AND
									wor_idWorkshop = sus_workshop AND sus_isDeleted = 0
									AND sus_idUser!=67 and sus_workshop='.$per.'
									ORDER BY  `susers`.`sus_orden` ASC
		');
		return $query->result();
	}

	function insertAppointmentSOAPP($data)
	{
		$query = $this->db->insert('appointments2', $data);
	}

	function get_data_asesor_by_vin($vin)
	{
		// $sql = "SELECT u.hus_correo AS 'correo',CONCAT_WS(' ',u.hus_nombre,u.hus_apellido) AS 'nombre',u.hus_ciudad AS 'ciudad',
		// 		CONCAT_WS(' ',v.datc_primernombre, v.datc_segundonombre,v.datc_apellidopaterno,datc_apellidomaterno) AS 'nombre_cliente',
		// 		CONCAT_WS(' ',v.data_modelo,v.data_ano,v.data_color) AS 'auto',
		// 		v.dat_fecha_entrega AS 'fecha_auto_entrega'
		// 		FROM ventas_facturado v
		// 		INNER JOIN huser u ON u.hus_IDhuser = v.hus_IDhuser
		// 		WHERE v.data_vin = '".$vin."';";

		$sql = "SELECT u.hus_correo AS 'correo',CONCAT_WS(' ',u.hus_nombre,u.hus_apellido) AS 'nombre',u.hus_ciudad AS 'ciudad',
				CONCAT_WS(' ',v.datc_primernombre, v.datc_segundonombre,v.datc_apellidopaterno,v.datc_apellidomaterno) AS 'nombre_cliente',
				CONCAT_WS(' ',v.data_modelo,v.data_ano,v.data_color) AS 'auto',
				v.dat_fecha_entrega AS 'fecha_auto_entrega', u.hus_IDhuser AS 'id_asesor',c.datc_IDdatos_clientes AS 'id_cliente'
				FROM ventas_facturado v
				INNER JOIN huser u ON u.hus_IDhuser = v.hus_IDhuser
				INNER JOIN datos_cliente c ON v.datc_primernombre = c.datc_primernombre AND v.datc_segundonombre = c.datc_segundonombre
				AND v.datc_apellidopaterno = c.datc_apellidopaterno AND v.datc_apellidomaterno = c.datc_apellidomaterno
				AND v.datc_moral = c.datc_moral AND v.datc_persona_fisica_moral = c.datc_persona_fisica_moral
				WHERE v.data_vin = '".$vin."'";

		$row = $this->crm->query($sql)->row();
		if($row != null) return $row;
		else return null;
	}
}

?>
