<?php

class Multipuntosmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
    
    //FUNCION PARA OBTENER TODOS LOS DATOS DEL MULTIPUNTOS 
    //REFERENTE A UNA ORDEN EN ESPECIFICO
    function getMulti($orden){
        
        //obtiene el query: selecciona cada campo individualmente y enlaza las tablas relacionadas al multipuntos
		// los valores tienen un orden, todos los campos adicionales que se agregue seran agregados
		// antes de la pala from y del ultimo campo en el select	
        $query = $this->db->query("SELECT
        
		`mul_luztablero`, 
        `mul_luces`, 
        `mul_parabrisas`, 
        `mul_frenomano`, 
        `mul_claxon`, 
        `mul_clutch`,
		
		`mul_electrolito`, 
        `mul_terminales`, 
        `mul_sujecion`,
		
		`mul_soportes`, 
        `mul_liquidos`, 
        `mul_radiadores`, 
        `mul_ajustefreno`, 
        `mul_flechas`, 
        `mul_escape`, 
        `mul_fugas`, 
        
        `mul_gomas`, 
        `mul_basesamortiguadores`, 
        `mul_terminalesdirecion`, 
        `mul_amortiguadores`,
        
        `llan_di`, 
        `llan_diprof`, 
        `llan_dipres`, 
        `llan_dd`, 
        `llan_ddprof`, 
        `llan_ddpres`, 
        `llan_ti`, 
        `llan_tiprof`, 
        `llan_tipres`, 
        `llan_td`, 
        `llan_tdprof`, 
        `llan_tdpres`, 
        `llan_ref`, 
        `llan_refprof`, 
        `llan_refpres`,


        `fre_di`, 
        `fre_dicero`, 
        `fre_diactual`, 
        `fre_dd`, 
        `fre_ddcero`, 
        `fre_ddactual`, 
        `fre_ti`, 
        `fre_ticero`, 
        `fre_tiactual`, 
        `fre_td`, 
        `fre_tdcero`, 
        `fre_tdactual`,
		 mul_tipo,
		 mul_proximo,
		 mul_km

        FROM `multipuntos`, `llantas`, `frenos`
        WHERE `fre_orden` = `mul_orden` 
        AND `llan_orden` = `mul_orden` 
        AND `mul_orden` = ".$orden);
        
        $this->db->close();
        
        //regresa arreglo de objetos resultante
        return $query->result();        
    }    
    //FIN DE LA FUNCION 
    
    //Funcion para actualizar una funcion 
    public function updMulti($order, $table, $field, $value, $ordField)
    {
        //$this->db->set($field, $value)->get_compiled_update($table);
        //$this->db->from($table);
        //$this->db->where($ordField, $order);
        //$query = $this->db->set(array($field => $value))->get_compiled_update($table);

		//se construye el query
        //campo a asignar con su valor respectivo
        $this->db->set(array($field => $value));
        //tabla de origen
        $this->db->from($table);
        //donde se asigna el cambio
        $this->db->where($ordField, $order);
        //regresa booleano al asignar
        return $this->db->update();
    }

    function getVinInfo($idOrden){
        $query = 'select * from serviceOrder_vehicleInformation, serviceOrder_clientInformation, serviceOrders
        where sev_idVehicleInformation = '.$idOrden.' 
        and sec_idClientInformation = sev_idVehicleInformation
        and seo_idServiceOrder = sec_idClientInformation';
        $exec = $this->db->query($query);
        return $exec->result();
    }
	
	function editMulti()
	{
		echo $q='
		update 
			multipuntos 
		set 
			'.$_GET['campo'].'="'.$_GET['valor'].'"
		where 
			mul_orden='.$_GET['id'].'
		';
		$this->db->query($q);
	}	
    
    public function getOrdenData($order)
    {
        $query = $this->db->query('select * from serviceOrder_vehicleInformation, 
        customers, 
        serviceOrders, 
        serviceTypes,
        serviceOrders_service,
        susers
                where sev_vin = cus_serie
                and seo_idServiceOrder = sev_idVehicleInformation
                and ssr_ID = seo_idServiceOrder
                and ssr_service = set_idServiceType
                and sus_idUser = seo_adviser
                and sev_idVehicleInformation = '.$order);
                
        return $query->result();
    }
	
	 public function getNameTec($id)
    {
        $query = $this->db->query('select sus_name,sus_lastName from susers where sus_idUser= '.$id);
        $res=$query->result();
		return $res[0]->sus_name.' '.$res[0]->sus_lastName;
    }
    //FIN DE LA FUNCION
} 