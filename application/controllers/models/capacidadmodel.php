<?php

class Capacidadmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	
	function listarCapacidades($per){
		if($_SESSION['sfrol']==1){$and='';
				}else{$and='and wor_idWorkshop='.$per.'';}
		
		$query= $this->db->query(
				'
				SELECT w.*, c.*
				FROM workshops AS w, workshop_capabilities AS c
				WHERE 
				wor_idWorkshop = woc_workshop
				'.$and.'
				');
		 return $query->result();
	}
	
	
	function horaswd($cd){
		$query= $this->db->query(
				'
				SELECT *
				FROM horas,workshops
				WHERE 
				hor_wor_idWorkshop='.$cd.'
				and wor_idWorkshop = hor_wor_idWorkshop
				and hor_dia=1
				ORDER BY  `horas`.`hor_hora` ASC 
				');
		 return $query->result();
	}
	
	function horaswe($cd){
		$query= $this->db->query(
				'
				SELECT *
				FROM horas,workshops
				WHERE 
				hor_wor_idWorkshop='.$cd.'
				and wor_idWorkshop = hor_wor_idWorkshop
				and hor_dia=2
				ORDER BY  `horas`.`hor_hora` ASC 
				');
		 return $query->result();
	}
	
	function obtenerCapacidad($id){
		$query= $this->db->query(
				'
				SELECT *
				FROM workshop_capabilities,workshops
				WHERE 
				woc_workshop=wor_idWorkshop and 
				woc_idcapability = '.$id.'
				');
		 return $query->result();
	}
	
	function actualizarCapacidad($capacidad, $id){
         $this->db->update('workshop_capabilities', $capacidad, array('woc_idcapability' => $id)); 
	}
	
	
	function eliminarHora($id){
		$this->db->delete('horas', array('hor_ID' => $id)); 
		
		}
		
		function agregarHora($info){
			
			$this->db->insert('horas', $info); 
			}
	
	
	
}
	

?>