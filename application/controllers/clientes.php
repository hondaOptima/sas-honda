<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Clientesmodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
		$this->load->library('ciqrcode');
	
	}
	
	public function index()
	{
		
		$data['flash_message'] = $this->session->flashdata('message');
		$this->load->view('clientes/vista',$data);
	}
	
	function historia($numero)
	{	$vin = str_replace('%','',$numero);
	
		$datos['vin'] = $vin;		
		$datos['servicios'] = $this->Clientesmodel->obtenerServicios($vin);
		
		
		$serviceOrder = $datos['servicios'];
		if(ISSET($serviceOrder[0]->seo_idServiceOrder)){ 
		$datos['presupuestos'] = $this->Clientesmodel->obtenerPresupuesto($serviceOrder[0]->seo_idServiceOrder)->result();	
		}		
		$datos['imgs'] = $this->Clientesmodel->obtenerImagenes($vin)->row();						
		$cont = 0;
		if(count($datos['imgs']) > 0)
		{
			foreach($datos['imgs'] as $dato){
				
				$nombre_fichero = base_url().'calendario/ajax/uploads/'.$vin.'/'.$dato.'';			
				
				if(@getimagesize($nombre_fichero))
				{
					$imagenes[$cont] = $nombre_fichero;
					
									
				}
				else 
				{
					$imagenes[$cont] = 'http://www.dubaisquares.com/image/no_img.jpg';
					
				}
				$cont = $cont +1;
			}		
			$datos['imagenes'] = $imagenes;
		}

		$primero = True;
		$contador = 0;
		if(count($datos['servicios']) > 0)
		{
			foreach($datos['servicios'] as $servicio){

				if($primero){ $idservicio = $servicio->set_name;  $primero = False;
				$servicios[$contador] = $servicio;
				}
				
				if($servicio->set_name != $idservicio)
				{ 
					$contador++;
					$servicios[$contador] = $servicio;
					
				}
				else 
				{
					$idservicio = $servicio->set_name;
				}
				
				
			}
			
		}
		else {
			$servicios = null;
			
		}
		
		$datos['serviciosf'] = $this->Clientesmodel->obtenerServiciosFaltantes($servicios)->result();
		
		$this->load->view('clientes/historia/vista',$datos);
	}
	function printQR($vin){
		$params['data'] = 'http://hondaoptima.com/sas/orden/crear?vin='.$vin.'';
$params['level'] = 'H';
$params['size'] = 2.5;
$params['savename'] = FCPATH.'tes.png';
$this->ciqrcode->generate($params);

echo '<table style="margin-left:-5px;"><tr><td><img style="margin-left:-5px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:1px;">'.mb_strtoupper($vin).'</div>
</td></tr>
<tr height="10px;"><td></td><td></td></tr>
<tr><td><img style="margin-left:-5px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:1px;">'.mb_strtoupper($vin).'</div>
</td></tr>
<table>
';

//echo '<table style="margin-left:-5px;"><tr><td><img style="margin-left:-5px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:-3px;">'.mb_strtoupper($vin).'</div></td><td><img style="margin-left:2px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:7px;">'.mb_strtoupper($vin).'</div></td></tr><tr height="10px;"><td></td><td></td></tr><tr><td><img style="margin-left:-5px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:-3px;">'.mb_strtoupper($vin).'</div></td><td><img style="margin-left:2px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:7px;">'.mb_strtoupper($vin).'</div></td></tr><table>';

		}
		
		
		
		function printQRD($vinu,$vind){
$params['data'] = 'http://hondaoptima.com/sas/orden/crear?vin='.$vinu.'';
$params['level'] = 'H';
$params['size'] = 2.5;
$params['savename'] = FCPATH.'tesxxx.png';
$this->ciqrcode->generate($params);

$paramsb['data'] = 'http://hondaoptima.com/sas/orden/crear?vin='.$vind.'';
$paramsb['level'] = 'H';
$paramsb['size'] = 2.5;
$paramsb['savename'] = FCPATH.'tesb.png';
$this->ciqrcode->generate($paramsb);

/*echo '<table style="margin-left:-5px;"><tr><td><img style="margin-left:-5px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:1px;">'.mb_strtoupper($vin).'</div>
</td></tr>
<tr height="10px;"><td></td><td></td></tr>
<tr><td><img style="margin-left:-5px;" src="'.base_url().'tes.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:1px;">'.mb_strtoupper($vin).'</div>
</td></tr>
<table>
';*/

echo '<table style="margin-left:-5px;"><tr><td><img style="margin-left:-5px;" src="'.base_url().'tesxxx.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:-3px;">'.mb_strtoupper($vinu).'</div></td><td><img style="margin-left:2px;" src="'.base_url().'tesxxx.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:7px;">'.mb_strtoupper($vinu).'</div></td></tr><tr height="10px;"><td></td><td></td></tr><tr><td><img style="margin-left:-5px;" src="'.base_url().'tesb.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:-3px;">'.mb_strtoupper($vind).'</div></td><td><img style="margin-left:2px;" src="'.base_url().'tesb.png" /><br><div style="font-size:9.5px;font-family:arial; margin-left:7px;">'.mb_strtoupper($vind).'</div></td></tr><table>';

		}
		
		
		
		function eliminarsas($vin)
	{
		$this->Clientesmodel->eliminarsas($vin);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Cliente Eliminado</div>');
		redirect('clientes');
	}
	
	
	function insertsas()
	{
		$tab=$this->Clientesmodel->tabla24();
		
		foreach($tab as $row){
			 $arreglo=array(
			 'cus_name' => $row->COL_5.' '.$row->COL_6.' '.$row->COL_7,
			 'cus_email' => $row->COL_17,
			 'cus_telephone' => $row->COL_15,
			 'cus_celular' => $row->COL_16,
			 'cus_calle' => $row->COL_9,
			 'cus_num' => $row->COL_10,
			 'cus_colonia' => $row->COL_11,
			 'cus_cp' => $row->COL_14,
			 'cus_ciudad' => $row->COL_12,
			 'cus_estado' => $row->COL_13,
			 'cus_rfc' => $row->COL_8,
			 'cus_marca' => $row->COL_4,
			 'cus_submarca' => '',
			 'cus_version' => '',
			 'cus_color' => '',
			 'cus_modelo' => $row->COL_3,
			 'cus_serie' => $row->COL_2,
			 'cus_capacidad' => '',
			 'cus_km_recepcion' => '',
			 'cus_placas' => '',
			 'cus_ultimo_servicio' => '',
			 'cus_orden' => '',
			 'cus_fecha_venta' => '',
			 'cus_cd' => 'EN',
			 );
			$this->Clientesmodel->inserttabla24($arreglo);
			}
		
	}
    
    
    function agregarNuevoCliente($vin)
	{
		
		
			 $arreglo=array(
			 'cus_name' =>'Cliente Nuevo',
			 'cus_email' =>'',
			 'cus_telephone' =>'',
			 'cus_celular' =>'',
			 'cus_calle' =>'',
			 'cus_num' => '',
			 'cus_colonia' =>'',
			 'cus_cp' => '',
			 'cus_ciudad' =>$_SESSION['sfworkshop'],
			 'cus_estado' =>'',
			 'cus_rfc' =>'',
			 'cus_marca' => 'Honda',
			 'cus_submarca' => '',
			 'cus_version' => '',
			 'cus_color' => '',
			 'cus_modelo' => '',
			 'cus_serie' => $vin,
			 'cus_capacidad' => '',
			 'cus_km_recepcion' => '',
			 'cus_placas' => '',
			 'cus_ultimo_servicio' => '',
			 'cus_orden' => '',
			 'cus_fecha_venta' => '',
			 'cus_cd' => '',
			 );
			echo $id=$this->Clientesmodel->inserttabla24($arreglo);
			
		
	}
		
		
		
		
	
}

?>