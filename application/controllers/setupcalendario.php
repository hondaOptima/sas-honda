<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setupcalendario extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Setupcalendariomodel');
		$this->load->model('Citamodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library("Pdf");
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	}
	
	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/
				
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}		
				
				
				
		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
		
			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
			
			$data['citas']=$this->Citamodel->listarCitas($date);
			$this->load->view('setupcalendario/vista',$data);
	}
	
	
		public function calendario()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/
				
		if(empty($_GET['taller'])){
			$taller=2;
			}else{
				$taller=$_GET['taller'];}		
				
				
				
		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
		
			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
			
			$data['citas']=$this->Citamodel->listarCitas($date);
			$this->load->view('cita/calendario',$data);
	}
	
		public function gracias()
	{
	
			$this->load->view('cita/gracias');
	}
	
	
	
		public function calpasodos()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/
				
		if(empty($_GET['taller'])){
			$taller=2;
			}else{
				$taller=$_GET['taller'];}		
				
				
				
		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
		
			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
			
			$data['citas']=$this->Citamodel->listarCitas($date);
			
		
			$data['taller']=$_GET['taller'];
			$data['servicio']=$_GET['servicio'];
			$data['modelo']=$_GET['modeloVehiculo'];
			$data['anio']=$_GET['anoVehiculo'];			
			
			$this->load->view('cita/calpasodos',$data);
	
	
	}
	
	public function calpasotres()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/
				
		if(empty($_GET['taller'])){
			$taller=2;
			}else{
				$taller=$_GET['taller'];}		
				
				
				
		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
		
			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
			
			$data['citas']=$this->Citamodel->listarCitas($date);
			
			
			$data['taller']=$_GET['taller'];
			$data['servicio']=$_GET['servicio'];
			$data['modelo']=$_GET['modelo'];
			$data['anio']=$_GET['anio'];
			$data['hora']=$_GET['hora'];
			$data['idh']=$_GET['idh'];
			
			
			
			$this->load->view('cita/calpasotres',$data);
	}
	
	public function porconfirmar()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
			$data['online'] = true;
			$data['date']=$date;
			$data['citas']=$this->Citamodel->listarCitasPorConfirmar($date);
			$this->load->view('cita/lista',$data);
	}
	
	public function crear()
	{
		

		$this->form_validation->set_rules('servicio', 'Servicio', 'required');
		$this->form_validation->set_rules('asesor', 'Asesor Deseado', 'required');
		$this->form_validation->set_rules('tipoRecordatorio', 'Tipo de Recordatorio', 'required');
		$this->form_validation->set_rules('clienteNombre', 'Nombre', 'required');
		$this->form_validation->set_rules('clienteApellido', 'Apellido', 'required');
		$this->form_validation->set_rules('clienteTelefono', 'Teléfono', 'required');
		$this->form_validation->set_rules('clienteCorreo', 'Correo Electrónico', 'required');
		$this->form_validation->set_rules('modeloVehiculo', 'Modelo de Vehículo', 'required');
		$this->form_validation->set_rules('anoVehiculo', 'Año de Vehículo', 'required');
		
		if ($this->form_validation->run())
		{
			//$hora = date('Y-m-d H:i:s', strtotime($this->input->post('hora')));
			list($fecha,$idCalendario)=explode("/",$this->input->post('fecha'));
			list($dia,$hora)=explode(" ",$fecha);
			
			$valet = $this->input->post('necesitaValet');
			if($valet != '1')
			$valet = 0;
				
			$cita = array(
				'app_day'=>$dia,
				'app_hour'=>$fecha,
				'app_service'=>$this->input->post('servicio'),
				'app_desiredAdviser'=>$this->input->post('asesor'),
				'app_reminderType'=>$this->input->post('tipoRecordatorio'),
				'app_customerName'=>$this->input->post('clienteNombre'),
				'app_customerLastName'=>$this->input->post('clienteApellido'),
				'app_customerTelephone'=>$this->input->post('clienteTelefono'),
				'app_customerEmail'=>$this->input->post('clienteCorreo'),
				'app_vehicleModel'=>$this->input->post('modeloVehiculo'),
				'app_vehicleYear'=>$this->input->post('anoVehiculo'),
				'app_message'=>$this->input->post('mensaje'),
				'app_needValet'=>$valet,
				'app_onlineConfirmed'=>1,
			);
			$ci=$this->Citamodel->agregarCita($cita);

			$calendario = array('cal_app_idAppointment'=>$ci);
			$this->Citamodel->updateCalendario($calendario,$idCalendario);
			
			
			redirect('cita?date='.$dia.'');
		} 
		
	}
	
	
	public function crearonline()
	{
		

		$this->form_validation->set_rules('servicio', 'Servicio', 'required');
		$this->form_validation->set_rules('asesor', 'Asesor Deseado', 'required');
		$this->form_validation->set_rules('tipoRecordatorio', 'Tipo de Recordatorio', 'required');
		$this->form_validation->set_rules('clienteNombre', 'Nombre', 'required');
		$this->form_validation->set_rules('clienteApellido', 'Apellido', 'required');
		$this->form_validation->set_rules('clienteTelefono', 'Teléfono', 'required');
		$this->form_validation->set_rules('clienteCorreo', 'Correo Electrónico', 'required');
		$this->form_validation->set_rules('modelo', 'Modelo de Vehículo', 'required');
		$this->form_validation->set_rules('anio', 'Año de Vehículo', 'required');
		
		if ($this->form_validation->run())
		{
			//$hora = date('Y-m-d H:i:s', strtotime($this->input->post('hora')));
			//list($fecha,$idCalendario)=explode("/",$this->input->post('fecha'));
			//list($dia,$hora)=explode(" ",$fecha);
			$idCalendario=$this->input->post('idh');
			$valet = $this->input->post('necesitaValet');
			if($valet != '1')
			$valet = 0;
				
			$cita = array(
				'app_day'=>$this->input->post('fecha'),
				'app_hour'=>"".$this->input->post('fecha')." ".$this->input->post('hora').":00",
				'app_service'=>$this->input->post('servicio'),
				'app_desiredAdviser'=>$this->input->post('asesor'),
				'app_reminderType'=>$this->input->post('tipoRecordatorio'),
				'app_customerName'=>$this->input->post('clienteNombre'),
				'app_customerLastName'=>$this->input->post('clienteApellido'),
				'app_customerTelephone'=>$this->input->post('clienteTelefono'),
				'app_customerEmail'=>$this->input->post('clienteCorreo'),
				'app_vehicleModel'=>$this->input->post('modelo'),
				'app_vehicleYear'=>$this->input->post('anio'),
				'app_message'=>$this->input->post('mensaje'),
				'app_needValet'=>$valet,
				'app_onlineConfirmed'=>1,
			);
			//print_r($cita);
			$ci=$this->Citamodel->agregarCita($cita);

			$calendario = array('cal_app_idAppointment'=>$ci);
			$this->Citamodel->updateCalendario($calendario,$idCalendario);
			
			//send email de la cita
			$this->Citamodel->sendEmailCitaOnline($this->input->post('clienteCorreo'),$this->input->post('fecha'),$this->input->post('hora'),$this->input->post('servicio'),$this->input->post('modelo'),$this->input->post('anio'),$this->input->post('clienteNombre'),$this->input->post('clienteApellido'));
			
			redirect('cita/gracias?date='.$dia.'');
		} 
		
	}
	
	
	public function editar($id)
	{
		$data['cita'] = $this->Citamodel->obtenerCita($id);
		if($data['cita'])
		{
			$data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
			$this->load->view('cita/editar',$data);
		
		}
		else
		{
			redirect('error');
		}
	}
	
	public function actualizar($id)
	{
		$this->form_validation->set_rules('dia', 'Día', 'required');
		$this->form_validation->set_rules('hora', 'Hora', 'required');
		$this->form_validation->set_rules('servicio', 'Servicio', 'required');
		$this->form_validation->set_rules('asesor', 'Asesor Deseado', 'required');
		$this->form_validation->set_rules('tipoRecordatorio', 'Tipo de Recordatorio', 'required');
		$this->form_validation->set_rules('clienteNombre', 'Nombre', 'required');
		$this->form_validation->set_rules('clienteApellido', 'Apellido', 'required');
		$this->form_validation->set_rules('clienteTelefono', 'Teléfono', 'required');
		$this->form_validation->set_rules('clienteCorreo', 'Correo Electrónico', 'required');
		$this->form_validation->set_rules('modeloVehiculo', 'Modelo de Vehículo', 'required');
		$this->form_validation->set_rules('anoVehiculo', 'Año de Vehículo', 'required');
		
		if($this->form_validation->run())
		{
			
			$hora = date('Y-m-d H:i:s', strtotime($this->input->post('hora')));
			$valet = $this->input->post('necesitaValet');
			if($valet != '1')
				$valet = 0;
				
			$cita = array(
				'app_day'=>$this->input->post('dia'),
				'app_hour'=>$hora,
				'app_service'=>$this->input->post('servicio'),
				'app_desiredAdviser'=>$this->input->post('asesor'),
				'app_reminderType'=>$this->input->post('tipoRecordatorio'),
				'app_customerName'=>$this->input->post('clienteNombre'),
				'app_customerLastName'=>$this->input->post('clienteApellido'),
				'app_customerTelephone'=>$this->input->post('clienteTelefono'),
				'app_customerEmail'=>$this->input->post('clienteCorreo'),
				'app_vehicleModel'=>$this->input->post('modeloVehiculo'),
				'app_vehicleYear'=>$this->input->post('anoVehiculo'),
				'app_message'=>$this->input->post('mensaje'),
				'app_needValet'=>$valet,
			);
			
			$this->Citamodel->actualizarCita($cita,$id);
			redirect('cita');
		}
		else
		{
			redirect('cita/editar', $id);
		}
	}
	
	public function confirmar($id)
	{
		$this->Citamodel->confirmarCita($id);
		redirect('cita');
	}
	
	public function confirmaronline($id)
	{
		$this->Citamodel->confirmarCitaOnline($id);
		redirect('cita');
	}
	
	public function cancelar($id)
	{
		$this->Citamodel->cancelarCita($id);
		redirect('cita');
	}
	
	public function reagendar($id)
	{
		$data['cita'] = $this->Citamodel->obtenerCita($id);
		if($data['cita'])
		{	
			$this->form_validation->set_rules('dia', 'Día', 'required');
			$this->form_validation->set_rules('servicio', 'Servicio', 'required');
			$this->form_validation->set_rules('asesor', 'Asesor Deseado', 'required');
			$this->form_validation->set_rules('tipoRecordatorio', 'Tipo de Recordatorio', 'required');
			$this->form_validation->set_rules('clienteNombre', 'Nombre', 'required');
			$this->form_validation->set_rules('clienteApellido', 'Apellido', 'required');
			$this->form_validation->set_rules('clienteTelefono', 'Teléfono', 'required');
			$this->form_validation->set_rules('clienteCorreo', 'Correo Electrónico', 'required');
			$this->form_validation->set_rules('modeloVehiculo', 'Modelo de Vehículo', 'required');
			$this->form_validation->set_rules('anoVehiculo', 'Año de Vehículo', 'required');
			
			if ($this->form_validation->run())
			{
				$valet = $this->input->post('necesitaValet');
				if($valet != '1')
					$valet = 0;
					
				$cita = array(
					'app_day'=>$this->input->post('dia'),
					'app_service'=>$this->input->post('servicio'),
					'app_desiredAdviser'=>$this->input->post('asesor'),
					'app_reminderType'=>$this->input->post('tipoRecordatorio'),
					'app_customerName'=>$this->input->post('clienteNombre'),
					'app_customerLastName'=>$this->input->post('clienteApellido'),
					'app_customerTelephone'=>$this->input->post('clienteTelefono'),
					'app_customerEmail'=>$this->input->post('clienteCorreo'),
					'app_vehicleModel'=>$this->input->post('modeloVehiculo'),
					'app_vehicleYear'=>$this->input->post('anoVehiculo'),
					'app_message'=>$this->input->post('mensaje'),
					'app_needValet'=>$valet,
					'app_onlineConfirmed'=>1,
				);
				
				$this->Citamodel->agregarCita($cita);
				redirect('cita');
			} 
			else
			{
				$data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
				$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
				$data['asesores']=$this->Citamodel->obtenerAsesores();
				$data['servicios']=$this->Citamodel->obtenerServicios();
				$data['modelos']=$this->Citamodel->obtenerModelos();
				$this->load->view('cita/reagendar',$data);
			}
		}
		else
		{
			redirect('error');
		}
	}
	
	public function citasPorConfirmar() {
		$citasPorConfirmar = $this->Citamodel->listarPorConfirmarAjax();
		echo json_encode($citasPorConfirmar);
	}
	
	function pdfOrdendeCompra(){
		  $pdf = new TCPDF('L', PDF_UNIT, 'US Legal', true, 'UTF-8', false);    
		  $pdf->AddPage(); 
	
		  $html = 'ok ';
	
		  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
		  $pdf->Output('Hoja_de_entrega.pdf', 'I');
		}
		
	function porcentajeCita(){
		$IDT=$_GET['taller'];//ID del Taller
		$HR=1;// horas que tarda el servicio
        $DATE=$_GET['fecha'];
        $lista= $this->Citamodel->listaPorcentajeCitas($IDT);
         $output = array(); // it will wrap all of your value
    foreach($lista as $row){
          unset($temp); // Release the contained value of the variable from the last loop
             if($row->cpo_status=='2'){
			  $titulo='';
			  $color="#cc0000";
			  $url="";
		  }
		   if($row->cpo_status=='1'){
			  $titulo='';
			  $color="#1ec138";
			  $url="".base_url()."setupcalendario?date=".$row->cpo_fecha."&taller=".$IDT."";
			  }
			  if($row->cpo_status=='3'){
			  $titulo='';
			  $color="#fcb322";
			  $url="".base_url()."cita?date=".$row->cpo_fecha."&taller=".$IDT."";
			  }
		   if($row->cpo_fecha==$DATE){
			     $color="#333";
			   }
			  $temp = array(
		    'id' =>$row->cpo_ID,
			'title' =>$titulo,
			'start' =>$row->cpo_fecha,
			'url' => $url,
			'className' => '',
			'color'=> $color 
		  
		  );


          array_push($output,$temp);
    }
    header('Access-Control-Allow-Origin: *');
    header("Content-Type: application/json");
    echo json_encode($output);

		
		}
		
	
	function diaOcupado($date,$taller){//Configurar dia como ocupado
	
	//revisar que en esa fecha no hay citas
	$cita=$this->Citamodel->revisarCitas($date,$taller);
	if(!empty($cita[0]->app_idAppointment)){
		
	$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>No puede marcar este dia como Festivo</strong> Primero necesita reagendar las citas exitentes en esta fecha</div>');
			
			redirect('setupcalendario?date='.$date.'&taller='.$taller.'');	
		}
	else{

	//modificamos la tabla calendario a cal_status a ocupado en en la fecha y el taller mencionado
	$this->Citamodel->updateCalendarioFestivo($date,$taller);
	
	//modificamos la tabla calendario porcetaje cpo_status a ocupado y listo
	$this->Citamodel->updateCalendarioPorcentaje($date,$taller);
	
	//informamos que el dia fue marcado como festivo u ocupado
	$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> EL dia fue marcado como Festivo</div>');
			
			redirect('setupcalendario?date='.$date.'&taller='.$taller.'');	
	}
		}
		
		
		
		function agregarHora($date,$taller){//agregar hora
	//ir a la  tabla calendario y agregar un registro con la fecha hora , taller y cal_estatus en 1 de disponible	y listo
		$this->form_validation->set_rules('nhora', 'Hora', 'required');
			
			if ($this->form_validation->run())
			{
			$info=array(
			'cal_fecha'=>$date,
			'cal_hora'=>$this->input->post('nhora').':'.$this->input->post('nmin'),
			'cal_status'=>1,
			'cal_wor_idWorkshop'=>$taller,
			'cal_app_idAppointment'=>0						
			);
			
			$this->Citamodel->insertNuevaHora($info);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito. </strong> Ha agregado una nueva hora al Calendario.</div>');
			
			redirect('setupcalendario?date='.$date.'&taller='.$taller.'');	
			}
		}
		
		
		function eliminarHora(){//eliminar hora
	$date=$_GET['date'];
	$taller=$_GET['taller'];
	$IDH=$_GET['info'];		
	//y en la tabla calendario cambiamos cal_status a eliminada con referencia al id de la hora
	$this->Citamodel->deleteHora($IDH);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito. </strong> Ha Eliminado una Hora en el Calendario.</div>');
			
			redirect('setupcalendario?date='.$date.'&taller='.$taller.'');
		
		}
		
		
		function agregarHorasCalendario(){//agregar hora en una fecha no seleccionada o que aun no tiene citas
	$date=$_GET['date'];
	$taller=$_GET['taller'];
	$fechano=$_GET['fechano'];
	list($di,$ms,$an)=explode("-",$fechano);
	$fechano=$an.'-'.$ms.'-'.$di;
	$formato=$_GET['formato'];
	
	// revisamos que el dia no exista en la tabla calendario_porcentaje
	$res=$this->Citamodel->checkCalendario($fechano,$taller);
	
	if(!empty($res[0]->cpo_ID)){
		
		echo 'si';
		
		}else{
			
	//insertamos en la tabla calendario_porcentaje
	$this->Citamodel->insertHorasTaller($fechano,$taller);		
			
	//obtenemos la lista de la tabla horas con referencia al taller y horas de semana o horas de sabado
	$horas=$data['horaswd'] = $this->Citamodel->horaswd($taller,$formato);
	
	//insertamos en la fecha recibida en la tabla calendario para el taller recibido	
	foreach($horas as $h){
		
	$info=array(
			'cal_fecha'=>$fechano,
			'cal_hora'=>$h->hor_hora,
			'cal_status'=>1,
			'cal_wor_idWorkshop'=>$taller,
			'cal_app_idAppointment'=>0						
			);
			
			$this->Citamodel->insertNuevaHora($info);
		}	
			
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito. </strong> El Formato de horas fue agregado con exito.</div>');	
		echo 'hecho';	
			
			
			}
	
		
		}
		
		
		
		
		function infinito($date,$taller){//modificar infinito
	//definimos lunes a viernes o sabado
	
    $dia=date("w", strtotime($date));	
		
	//recibimos el marco de tiempo.
	$lista=$this->Citamodel->citasPorHora($date,$taller);
	
	//revisamos que este selecionada la opcion de lunes a viernes y sabado
	$rango=$this->Citamodel->revisarFechas($date,$taller);
	
	
	
	
	if($dia==6){
	foreach($rango as $r){
	
    $di=date("w", strtotime($r->cpo_fecha));
	if($di==6){
		
			//hacer update a todas las citas de esta fecha para pasar a estus en lista de espera
	//$this->Citamodel->updateCalendario($r->cpo_fecha,$taller);
	//borrar todas las horas del calendario
	$this->Citamodel->eliminarHorasCalendario($r->cpo_fecha,$taller);
	//agregar el formato de horas a estos dias
	foreach($lista as $li){
		
		$info=array(
			'cal_fecha'=>$r->cpo_fecha,
			'cal_hora'=>$li->cal_hora,
			'cal_status'=>1,
			'cal_wor_idWorkshop'=>$taller,
			'cal_app_idAppointment'=>0						
			);
			
			$this->Citamodel->insertNuevaHora($info);
		
		}
		
		}
	}}
	
	if($dia<6){
	foreach($rango as $r){
	
    $di=date("w", strtotime($r->cpo_fecha));
	
	if($di<6){
	//hacer update a todas las citas de esta fecha para pasar a estus en lista de espera
	//$this->Citamodel->updateCalendario($r->cpo_fecha,$taller);
	//borrar todas las horas del calendario
	$this->Citamodel->eliminarHorasCalendario($r->cpo_fecha,$taller);
	//agregar el formato de horas a estos dias
	foreach($lista as $li){
		
		$info=array(
			'cal_fecha'=>$r->cpo_fecha,
			'cal_hora'=>$li->cal_hora,
			'cal_status'=>1,
			'cal_wor_idWorkshop'=>$taller,
			'cal_app_idAppointment'=>0						
			);
			
			$this->Citamodel->insertNuevaHora($info);
		
		}
	
		}
	
		}
	}
			
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito. </strong> Ha modificado las horas del calendario.</div>');
			
			redirect('setupcalendario?date='.$date.'&taller='.$taller.'');
		}
		
		

}