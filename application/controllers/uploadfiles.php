<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uploadfiles extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Uploadfilesmodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library("curl");;				
	}

	public function GetCustomers($idcustomer = null, $find = null){
		if($find == ""){
			$customers = $this->Uploadfilesmodel->GetCustomersFirst(10, $idcustomer);
		}else{
			$customers = $this->Uploadfilesmodel->GetCustomers(urldecode($find), 20, $idcustomer);
		}
		
		$response = array(
			"Status" => true,
			"Message" => "success",
			"Object" => $customers
		);

		echo json_encode($response);
	}

	function GetFiles($customer){
		$files = $this->Uploadfilesmodel->GetFiles($customer);
		$response = array(
			"Status" => true,
			"Message" => "success",
			"Object" => $files
		);

		echo json_encode($response);
	}


	function SavePdf(){
		$this->load->library('mpdf');
		$html = '<img src="'.$_GET['path_from'].'" />';
		$this->mpdf->writeHTML($html);
		$this->mpdf->Output($_GET['path_to'], 'F');
	}


}