<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizaciones extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cotizacionesmodel');
		$this->load->model('Reportesmodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
		$this->load->model('Productividadmodel');
        $this->load->model('Ordendeserviciomodel');
		$this->load->helper('url');
		$this->load->helper('dates');
		$this->load->library('pdf');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	}

	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['taller'])) $taller=$_SESSION['sfidws'];
		else $taller=$_GET['taller'];

		if(empty($_GET['fecha'])) $data['fecha'] = date('Y-m-d');
		else $data['fecha'] = $_GET['fecha'];

		if(empty($_GET['asesor']))
		{
			$ase = 0;
			$data['ordenes'] = $this->Cotizacionesmodel->obtener_ordenes_por_fecha($data['fecha'],$taller);
		}
		else
		{
			$ase = $_GET['asesor'];
			$data['ordenes'] = $this->Cotizacionesmodel->obtener_ordenes_por_asesor($ase, $data['fecha'],$taller);
		}

		if($data['ordenes']) $data['cotizaciones'] = $this->Cotizacionesmodel->obtener_cotizaciones_por_orden($data['ordenes'][0]->seo_idServiceOrder);
		else $data['cotizaciones'] = array();

		$data['asesor'] = $ase;
		$data['usuario'] = $this->Usuariomodel->listaUsuarios(0);
		$data['utilizacion'] = $this->Productividadmodel->utilizacion($data['fecha'],$taller);

		$data['taller']=$taller;
		$this->load->view('cotizaciones/vista',$data);
	}

	public function victor_index()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['taller'])) $taller = $_SESSION['sfidws'];
		else $taller=$_GET['taller'];

		if(empty($_GET['fecha'])) $data['fecha'] = date('Y-m-d');
		else $data['fecha'] = $_GET['fecha'];

		if(empty($_GET['asesor']))
		{
			$ase = 0;
			$data['ordenes'] = $this->Cotizacionesmodel->obtener_ordenes_por_fecha($data['fecha'],$taller);
		}
		else
		{
			$ase = $_GET['asesor'];
			$data['ordenes'] = $this->Cotizacionesmodel->obtener_ordenes_por_asesor($ase, $data['fecha'],$taller);
		}

		if($data['ordenes']) $data['cotizaciones'] = $this->Cotizacionesmodel->obtener_cotizaciones_por_orden($data['ordenes'][0]->seo_idServiceOrder);
		else $data['cotizaciones'] = array();

		$data['asesor'] = $ase;
		$data['usuario'] = $this->Usuariomodel->listaUsuarios(0);
		$data['utilizacion'] = $this->Productividadmodel->utilizacion($data['fecha'], $taller);

		$data['taller'] = $taller;
		// $this->load->view('cotizaciones/vista',$data);
	}

	function formulario_cliente()
	{
		$data["fecha"] = fecha_completa_larga(date("Y-m-d"));
		$this->load->view("cotizaciones/formulario_cliente",$data);
	}

	public function crear($orden)
	{
		$data['flash_message'] = $this->session->flashdata('message');

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('fecha', 'Fecha', 'required');
		$this->form_validation->set_rules('modelo', 'Modelo', 'required');
		$this->form_validation->set_rules('ano', 'A&ntilde;o', 'required');
		$this->form_validation->set_rules('color', 'Color');

        if ($this->form_validation->run())
        {
			$cotizacion = array(
				'est_service_order' => $orden,
				'est_date' 			=> $this->input->post('fecha'),
				'est_customer' 		=> $this->input->post('nombre'),
				'est_vehicle_brand' => $this->input->post('modelo'),
				'est_vehicle_model' => $this->input->post('ano'),
				'est_vehicle_color' => $this->input->post('color'),
			);

		   	$id = $this->Cotizacionesmodel->crear($cotizacion);

			$cantidad = $this->input->post('cantidad');
			$servicio = $this->input->post('servicio');
			$descripcion = $this->input->post('descripcion');
			$refacciones = $this->input->post('refacciones');
			$total = $this->input->post('total');
			$mano_de_obra = $this->input->post('mano_obra');
			if($this->input->post('autorizo')) $autorizo = $this->input->post('autorizo');
			else $autorizo = array();

			if($this->input->post('existencia')) $existencias = $this->input->post('existencia');
			else $existencias = array();

			for($i = 0; $i < 15; $i++)
			{
				if($cantidad[$i] && $servicio[$i])
				{
					if(in_array($i, $autorizo))
						$autorizado = 1;
					else
						$autorizado = 0;
					if(in_array($i, $existencias))
						$existencia = 1;
					else
						$existencia = 0;

					$estimate_service = array(
						'ess_estimate' => $id,
						'ess_service' => $servicio[$i],
						'ess_quantity' => $cantidad[$i],
						'ess_description' => $descripcion[$i],
						'ess_unit_price' => $refacciones[$i],
						'ess_total_price' => $total[$i],
						'ess_handwork' => $mano_de_obra[$i],
						'ess_is_authorized' => $autorizado,
						'ess_is_existencia' => $existencia,
						);

					$this->Cotizacionesmodel->crear_servicio($estimate_service);
				}
			}
			/*
			$pdf = $this->crear_pdf($id);

			$cotizacion = array('est_file' => $pdf);
			$this->Cotizacionesmodel->actualizar($cotizacion, $id);
			*/


			$this->session->set_flashdata('message', "Cotizaci&oacute;n creada exitosamente");

			redirect('cotizaciones');
		}
		else
		{
			$data['info'] = $this->Cotizacionesmodel->obtener_info_cotizacion($orden);
			$data['servicios'] = $this->Cotizacionesmodel->obtener_servicios();
            $this->load->view('cotizaciones/crear', $data);
		}
	}

	public function editar($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['cotizacion'] = $this->Cotizacionesmodel->obtener($id);
		if($data['cotizacion'])
		{
			$data['servicios'] = $this->Cotizacionesmodel->obtener_servicios();
			$data['servicios_cotizacion'] = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
			$data['cantidad_servicios'] = count($data['servicios_cotizacion']);
            $this->load->view('cotizaciones/editar', $data);
		}
		else redirect('cotizaciones');
	}

	public function actualizar()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('fecha', 'Fecha', 'required');

        if ($this->form_validation->run())
        {

			$cotizacion_id = $this->input->post('id');
			$id_servicio = $this->input->post('id_servicio');
			$cantidad = $this->input->post('cantidad');
			$servicio = $this->input->post('servicio');
			$descripcion = $this->input->post('descripcion');
			$refacciones = $this->input->post('refacciones');
			$total = $this->input->post('total');
			$mano_de_obra = $this->input->post('mano_obra');
			$exi = $this->input->post('existencia');

			// REGISTRAR PARTES DEL SERVICIO
			$i = 0;
			for($i == 0; $i < 14; $i++)
			{
				$NoParte = split(' ',$descripcion[$i]);
				$piezas = $cantidad[$i];
				$precio = $total[$i];
				$n = count($exi);

				if($i < $n)
				{
					$existencia = $exi[$i];


					if($existencia != '0-1')
					{

						if($_SESSION['sfworkshop'] == 'Tijuana') $taller = 3;
						if($_SESSION['sfworkshop'] == 'Mexicali') $taller = 2;
						if($_SESSION['sfworkshop'] == 'Ensenada') $taller = 1;
						$data= array(
								'hit_IDhits'=>'NULL',
								'hit_mes'=>date('m'),
								'hit_date'=>date('m').date('Y'),
								'hit_taller'=>$taller,
								'hit_noparte'=>$NoParte[0],
								'hit_piezas'=>$piezas,
								'hit_costo'=>$precio,
								'hit_almacenable'=>0,
								'hit_hora'=>date('Y-m-d H:i:s'),
								);
						$this->Reportesmodel->insertHits($data);
					}
				}
			} // <-----

			if($this->input->post('autorizo')) { $autorizo = $this->input->post('autorizo'); }
			else { $autorizo = array(); }

			if($this->input->post('existencia')) { $existencias = $this->input->post('existencia'); }
			else { $existencias = array(); }



			$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($cotizacion_id);
			$cantidad_servicios = count($servicios_cotizacion);

			for($i = 0; $i < $cantidad_servicios; $i++)
			{

				if(in_array($i, $autorizo))
					$autorizado = 1;
				else
					$autorizado = 0;
				if(in_array($i, $existencias))
					$existencia = 1;
				else
					$existencia = 0;

				$estimate_service = array(
					'ess_service' => $servicio[$i],
					'ess_quantity' => $cantidad[$i],
					'ess_description' => $descripcion[$i],
					'ess_unit_price' => $refacciones[$i],
					'ess_total_price' => $total[$i],
					'ess_handwork' => $mano_de_obra[$i],
					'ess_is_authorized' => $autorizado,
					'ess_is_existencia' => $existencia,
					);

				$this->Cotizacionesmodel->actualizar_servicio($estimate_service, $id_servicio[$i]);
			}
			for($i = $cantidad_servicios; $i < 15; $i++)
			{
				if($cantidad[$i] && $servicio[$i])
				{

					if(in_array($i, $autorizo))
						$autorizado = 1;
					else
						$autorizado = 0;
					if(in_array($i, $existencias))
						$existencia = 1;
					else
						$existencia = 0;

					$estimate_service = array(
						'ess_estimate' => $cotizacion_id,
						'ess_service' => $servicio[$i],
						'ess_quantity' => $cantidad[$i],
						'ess_description' => $descripcion[$i],
						'ess_unit_price' => $refacciones[$i],
						'ess_total_price' => $total[$i],
						'ess_handwork' => $mano_de_obra[$i],
						'ess_is_authorized' => $autorizado,
						'ess_is_existencia' => $existencia,
						);

					$this->Cotizacionesmodel->crear_servicio($estimate_service);
				}
			}
			/*
			$cotizacion_actualizada = $this->Cotizacionesmodel->obtener($cotizacion_id);

			if (file_exists($cotizacion_actualizada[0]->est_file) && is_dir($cotizacion_actualizada[0]->est_file))
			{
				unlink($cotizacion_actualizada[0]->est_file);
			}

			$pdf = $this->crear_pdf($cotizacion_id);

			$cotizacion = array('est_file' => $pdf);
			$this->Cotizacionesmodel->actualizar($cotizacion, $cotizacion_id);
			*/

			$this->session->set_flashdata('message', " ");

			redirect('cotizaciones');
		}
		else
		{
			$cotizacion_id = $this->input->post('id');
			$data['flash_message'] = $this->session->flashdata('message');
			$data['cotizacion'] = $this->Cotizacionesmodel->obtener($cotizacion_id);
			if($data['cotizacion'])
			{
				$data['servicios'] = $this->Cotizacionesmodel->obtener_servicios();
				$this->load->view('cotizaciones/editar', $data);
			}
			else
			{
				redirect('cotizaciones');
			}
		}
	}

	public function borrar($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['cotizacion'] = $this->Cotizacionesmodel->obtener($id);
		if($data['cotizacion'])
		{
			$this->Cotizacionesmodel->borrar($id);
		}
		redirect('cotizaciones');
	}

	public function desconfirmar($id)
	{
		$this->Cotizacionesmodel->desconfirmar($id);
		redirect_back();
	}

	public function confirmar($id)
	{
		$this->Cotizacionesmodel->confirmar($id);
		redirect_back();
	}


function sendDocument($id)
	{

		$cotizacion = $this->Cotizacionesmodel->obtenerEmail($id);
		$servicios = $this->Cotizacionesmodel->obtener_servicios();
		$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
		$cantidad_servicios = count($servicios_cotizacion);
		$total = $this->Cotizacionesmodel->obtener_total_cotizacion($id);
		$iva = $total[0]->total * 0.16;
		$great_total = $total[0]->total + $iva;

		setlocale(LC_MONETARY, 'en_US');
		$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
.page {
	font-family:Verdana, Geneva, sans-serif;
}
	.head {
		width:100%;

		margin-bottom:0px;

	}
		.contact {
			width:55%;

			float:left;
		}
			.logo {
				width:100%;
				height:70px;
				background-image:url("http://webmarketingnetworks.com/sas/documents/hlogo.png");
				background-size: 80%;
				background-repeat:no-repeat;
			}
			.address {
				width:100%;
				height:100%;
				border:none;
				border-collapse:collapse;
			}
				.address th {
					font-size:9px;
				}
				.address td {
					font-size:8px;
				}
				.address .hours{
					font-size:10px;
					text-align:center;
				}
		.folio {
			width:45%;

			float:left;
		}
			.os-type {
				width:100%;
				height:25px;
				padding-top:20px;
				font-size:18px;
				font-weight:bold;
				text-align:center;
			}
			.info {
				width:60%;
				height:120px;
				font-size:12px;
				margin-left:150px;
			}
				.message {
					width:60%;
					height:60px;
					float:left;
				}
					.message p{
						width:98%;
						margin:auto;
						margin-top:20px;
						padding:5px;
						border:0px solid rgb(51,51,51);
						border-radius:15px;
						background-color:rgb(226,0,43);
						color:rgb(255,255,255);
						font-size:8px;
						text-align:justify;
						line-height:180%;
					}
				.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:10px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}
						.order-date .gray{
							background-color:rgb(208,208,208) ;
							color:rgb(255,255,255);
							font-size:10px;
							font-weight:900;
						}
						.order-date .white{
							height:27px;
							font-size:10px;
							font-weight:900;
							vertical-align:text-top;
							text-align:left;
						}
						.order-date td{
							border:1px solid rgb(255,255,255);
						}

.grey{
	background-color:rgb(208,208,208) ;
}


.comments{
	width:100%;

	float:left;
	font-size:9px;
	text-align:justify;
	vertical-align:top;
}
	.comments .text{
		width:99%;
		height:40px;
		margin:auto;
	}
	.text .title{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
	}

	.title-address-right{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:right;
	color:rgb(226,0,43);
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
	}

	.title-address-left{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:left;
	color:rgb(226,0,43);
	padding-left:3px;
	}

.customer-info {
	width:100%;
	float:left;
	font-size:9px;
}

	.customer-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.customer-info table th{
		border-bottom:1px solid rgb(0,0,0);
	}
	.customer-info table td{
		padding-left:5px;
	}

.vehicle-info {
	width:60%;
	float:left;
	font-size:9px;
}

	.vehicle-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.vehicle-info table th {
		border-bottom:1px solid rgb(51,51,51);
	}
	.vehicle-info table td{
		padding-left:5px;
	}

.details {
	width:70%;
}

.services-head{
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:10px;
	margin-bottom:10px;
}
	.services-head th{
		height:22px;
	}

.services{
	font-size:10px;
	border:1px solid rgb(226,0,43);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}

.first-cell{
	border-right:1px solid rgb(226,0,43);
}

.total{
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(72,72,72);
	border-radius:5px;
}
.total-text{
	text-align:right;
	padding-right:15px;
}

.row{
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child{
	margin-bottom:0px;
}
.floated{
	float:left;
}
.col-12 {
	width:100%;
}
.col-10{
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8{
	width:66.66%;
}
.col-7{
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5{
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:16.5%;
}
.col-1{
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right{
	text-align:right;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left{
	text-align:left;
	padding-left:3px;
}
.radius {
    border: 1px solid rgb(51,51,51);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
.headerMail{
	width:100%;
	height:80px;
	border:1px solid gray;
}

.headerMail h2{
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}
</style>


<div class="page">
	<div class="head">
    	<div class="contact">
        	<div class="logo">

			</div>

            	<table class="address">
                    	<tr>
                        	<th class="title-address-right">TIJUANA:</th>
                        	<th class="title-address-left">MEXICALI:</th>
                        </tr>
                    	<tr>
                        	<td class="right">Av. Padre Kino 4300 Zona R&iacute;o, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td class="left">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr>
                        	<th class="title-address-right">ENSENADA:</th>
                        	<td class="hours">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr>
                        	<td class="right">Ensenada Av. Balboa 146 esq.<br />
							L&oacute;pez Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td class="hours">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            S&aacute;bado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>

        </div>

        <div class="folio">

        	<div class="info">
            	<div class="service-order">
                	<table class="order-date radius">
                            <tr class="gray">
                            	<td>FECHA</td>
                            </tr>
                            <tr class="white">
                            	<td><b>
								'.date("Y-m-d", strtotime($cotizacion[0]->est_date)).'
								</b></td>
                            </tr>
                    </table>
					<br>
					<div style="margin-left:10px;">
					<center>
					<b>Asesor de Servicio</b><br>
					'.$cotizacion[0]->sus_name.' '.$cotizacion[0]->sus_lastName.'
                </center>
				</center>
				</div>
            </div>
        </div>
    </div>
	<br>
	<div class="row">
	<div class="os-type">
        		COTIZACI&Oacute;N
        	</div>
			<br>
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="2" class="title grey">CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-2">Nombre: </td>
                        <td class="col-10" style="text-align:left;"><b>'.$cotizacion[0]->est_customer.'</b></td>
                    </tr>
            </table><br>
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="6" class="title grey">VEH&Iacute;CULO</th>
                    </tr>
                    <tr>
                        <td class="col-1">Modelo: </td>
                        <td class="col-4" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_brand.'</b></td>
                        <td class="col-1">A&ntilde;o: </td>
                        <td class="col-2" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_model.'</b></td>
                        <td class="col-1">Color: </td>
                        <td class="col-3" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_color.'</b></td>
                    </tr>
            </table>
        </div>
    </div>
	<br><br><br>
	<div class="row">
    	<table class="radius services-head" style="width:100%;">
        	<tr>
            	<th class="first-cell col-1"></th>
            	<th class="col-1">CANTIDAD</th>
            	<th class="col-3">SERVICIO</th>
            	<th class="col-2">TOTAL<br>REFACCIONES</th>
            	<th class="col-2">TOTAL<br>MANO DE OBRA</th>
            	<th class="col-2">TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
		';
		$suma=0;
		$tot=0;
		foreach($servicios_cotizacion as $key => $row)
		{
			if($row->ess_is_existencia==0){$existencia='Pedido Especial';}
			else{
				$existencia='En Existencia';
				}
			$suma=$row->ess_unit_price + $row->ess_handwork;
			$suma;
			if($row->ess_service==0 || $row->ess_service=='' ){}
			else{
			$html = $html.'<tr>
					<td class="first-cell col-1">'.(1+$key).'</td>
					<th class="col-1">'.$row->ess_quantity.'</th>
					<th class="col-3" style="text-align:left;">'.$servicios[$row->ess_service].'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_unit_price).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_handwork).'</th>
					<th class="col-2">'.money_format('%(#10n', $suma).'</th>
                </tr>
				<tr>
					<td class="first-cell col-1"></td>
					<th class="col-1"></th>
					<th class="col-3" style="font-size:9px;text-align:left; padding-left:10px;"><i style="margin-left:10px; ">'.$row->ess_description.' ( '.$existencia.' )</i></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
				</tr>';
				$tot+=$suma;
			}
		}

		$html = $html.'
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">SUB TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot).'</p></td>
                </tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">IVA</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot * 0.16).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td  class="total-text" style="">GRAN TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot * 1.16).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
        </table>
    </div>
	<br><br><br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th class="title grey">INFORMACI&Oacute;N</th>
                    </tr>
                    <tr>
                        <td class="col-6">*** Esta cotizaci&oacute;n ser&aacute; v&aacute;lida durante 30 d&iacute;as a partir de esta fecha.	</td>
                    </tr>
            </table><br>
			<h3>Optima Automotriz, S.A. de C.V.</h3>
			<h3>Departamento de Servicio</h3>
        </div>
    </div>
</div>
<table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://hcrm.gpoptima.net/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>
';


$bodyx             ='

							<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;}
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; }
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}

	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">

               <table width="770" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://hcrm.gpoptima.net/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<table width="100%"><tr>
<td width="80" >
  <h3>Cotizacion #'.$id.'</h3>

</td><td width="20%">
<b>Fecha: </b>'.date('d-m-Y').'</td></tr></table>
                     	<br>
						<div style="font-size:14px;" ><b>
</div>
<div tyle="margin:10px;">
 <h2>Estimado cliente a través de este correo electrónico le hacemos llegar un pdf con la información de la cotización de su automóvil.</h2>
<table><tr>

</tr></table>
<br><br>


      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://hcrm.gpoptima.net/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>

      </td>
      </tr>
      </table>
   </body>
</html>';


		$this->load->library('mpdf');
		$this->mpdf->writeHTML($html);
		$new_name = 'documents/estimates/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output($new_name, 'F');//$new_name, 'F');
        $this->Cotizacionesmodel->updateFile($new_name,$id);
        $this->email->from('avisodeprivacidad@hondaoptima.com', 'Honda Optima');
		$this->email->set_mailtype("html");
        $this->email->to('jaime-alex@outlook.com');
        $this->email->cc('');
        $this->email->subject('Cotizacion Honda Optima');
        $this->email->message($bodyx);
        $this->email->attach($new_name);


        $this->email->send();
		redirect('cotizaciones/index');

	}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
	function prueba_documento($id)
	{

		$cotizacion = $this->Cotizacionesmodel->obtener($id);
		$servicios = $this->Cotizacionesmodel->obtener_servicios();
		$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
		$cantidad_servicios = count($servicios_cotizacion);
		$total = $this->Cotizacionesmodel->obtener_total_cotizacion($id);
		$iva = $total[0]->total * 0.16;
		$great_total = $total[0]->total + $iva;

		setlocale(LC_MONETARY, 'en_US');
		$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
.page {
	font-family:Verdana, Geneva, sans-serif;
}
	.head {
		width:100%;

		margin-bottom:0px;

	}
		.contact {
			width:55%;

			float:left;
		}
			.logo {
				width:100%;
				height:70px;
				background-image:url("http://webmarketingnetworks.com/sas/documents/hlogo.png");
				background-size: 80%;
				background-repeat:no-repeat;
			}
			.address {
				width:100%;
				height:100%;
				border:none;
				border-collapse:collapse;
			}
				.address th {
					font-size:9px;
				}
				.address td {
					font-size:8px;
				}
				.address .hours{
					font-size:10px;
					text-align:center;
				}
		.folio {
			width:45%;

			float:left;
		}
			.os-type {
				width:100%;
				height:25px;
				padding-top:20px;
				font-size:18px;
				font-weight:bold;
				text-align:center;
			}
			.info {
				width:60%;
				height:120px;
				font-size:12px;
				margin-left:150px;
			}
				.message {
					width:60%;
					height:60px;
					float:left;
				}
					.message p{
						width:98%;
						margin:auto;
						margin-top:20px;
						padding:5px;
						border:0px solid rgb(51,51,51);
						border-radius:15px;
						background-color:rgb(226,0,43);
						color:rgb(255,255,255);
						font-size:8px;
						text-align:justify;
						line-height:180%;
					}
				.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:10px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}
						.order-date .gray{
							background-color:rgb(208,208,208) ;
							color:rgb(255,255,255);
							font-size:10px;
							font-weight:900;
						}
						.order-date .white{
							height:27px;
							font-size:10px;
							font-weight:900;
							vertical-align:text-top;
							text-align:left;
						}
						.order-date td{
							border:1px solid rgb(255,255,255);
						}

.grey{
	background-color:rgb(208,208,208) ;
}


.comments{
	width:100%;

	float:left;
	font-size:9px;
	text-align:justify;
	vertical-align:top;
}
	.comments .text{
		width:99%;
		height:40px;
		margin:auto;
	}
	.text .title{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
	}

	.title-address-right{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:right;
	color:rgb(226,0,43);
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
	}

	.title-address-left{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:left;
	color:rgb(226,0,43);
	padding-left:3px;
	}

.customer-info {
	width:100%;
	float:left;
	font-size:9px;
}

	.customer-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.customer-info table th{
		border-bottom:1px solid rgb(0,0,0);
	}
	.customer-info table td{
		padding-left:5px;
	}

.vehicle-info {
	width:60%;
	float:left;
	font-size:9px;
}

	.vehicle-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.vehicle-info table th {
		border-bottom:1px solid rgb(51,51,51);
	}
	.vehicle-info table td{
		padding-left:5px;
	}

.details {
	width:70%;
}

.services-head{
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:10px;
	margin-bottom:10px;
}
	.services-head th{
		height:22px;
	}

.services{
	font-size:10px;
	border:1px solid rgb(226,0,43);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}

.first-cell{
	border-right:1px solid rgb(226,0,43);
}

.total{
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(72,72,72);
	border-radius:5px;
}
.total-text{
	text-align:right;
	padding-right:15px;
}

.row{
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child{
	margin-bottom:0px;
}
.floated{
	float:left;
}
.col-12 {
	width:100%;
}
.col-10{
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8{
	width:66.66%;
}
.col-7{
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5{
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:16.5%;
}
.col-1{
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right{
	text-align:right;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left{
	text-align:left;
	padding-left:3px;
}
.radius {
    border: 1px solid rgb(51,51,51);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>

<div class="page">
	<div class="head">
    	<div class="contact">
        	<div class="logo">

			</div>

            	<table class="address">
                    	<tr>
                        	<th class="title-address-right">TIJUANA:</th>
                        	<th class="title-address-left">MEXICALI:</th>
                        </tr>
                    	<tr>
                        	<td class="right">Av. Padre Kino 4300 Zona R&iacute;o, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td class="left">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr>
                        	<th class="title-address-right">ENSENADA:</th>
                        	<td class="hours">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr>
                        	<td class="right">Ensenada Av. Balboa 146 esq.<br />
							L&oacute;pez Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td class="hours">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            S&aacute;bado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>

        </div>

        <div class="folio">

        	<div class="info">
            	<div class="service-order">
                	<table class="order-date radius">
                            <tr class="gray">
                            	<td>FECHA</td>
                            </tr>
                            <tr class="white">
                            	<td><b>
								'.date("Y-m-d", strtotime($cotizacion[0]->est_date)).'
								</b></td>
                            </tr>
                    </table>
					<br>
					<div style="margin-left:10px;">
					<center>
					<b>Asesor de Servicio</b><br>
					'.$cotizacion[0]->sus_name.' '.$cotizacion[0]->sus_lastName.'
                </center>
				</center>
				</div>
            </div>
        </div>
    </div>
	<br>
	<div class="row">
	<div class="os-type">
        		COTIZACI&Oacute;N
        	</div>
			<br>
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="2" class="title grey">CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-2">Nombre: </td>
                        <td class="col-10" style="text-align:left;"><b>'.$cotizacion[0]->est_customer.'</b></td>
                    </tr>
            </table><br>
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="6" class="title grey">VEH&Iacute;CULO</th>
                    </tr>
                    <tr>
                        <td class="col-1">Modelo: </td>
                        <td class="col-4" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_brand.'</b></td>
                        <td class="col-1">A&ntilde;o: </td>
                        <td class="col-2" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_model.'</b></td>
                        <td class="col-1">Color: </td>
                        <td class="col-3" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_color.'</b></td>
                    </tr>
            </table>
        </div>
    </div>
	<br><br><br>
	<div class="row">
    	<table class="radius services-head" style="width:100%;">
        	<tr>
            	<th class="first-cell col-1"></th>
            	<th class="col-1">CANTIDAD</th>
            	<th class="col-3">SERVICIO</th>
            	<th class="col-2">TOTAL<br>REFACCIONES</th>
            	<th class="col-2">TOTAL<br>MANO DE OBRA</th>
            	<th class="col-2">TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
		';
		$suma=0;
		$tot=0;
		foreach($servicios_cotizacion as $key => $row)
		{
			if($row->ess_is_existencia==0){$existencia='Pedido Especial';}
			else{
				$existencia='En Existencia';
				}
			$suma=$row->ess_unit_price + $row->ess_handwork;
			$suma;
			if($row->ess_service==0 || $row->ess_service=='' ){}
			else{
			$html = $html.'<tr>
					<td class="first-cell col-1">'.(1+$key).'</td>
					<th class="col-1">'.$row->ess_quantity.'</th>
					<th class="col-3" style="text-align:left;">'.$servicios[$row->ess_service].'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_unit_price).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_handwork).'</th>
					<th class="col-2">'.money_format('%(#10n', $suma).'</th>
                </tr>
				<tr>
					<td class="first-cell col-1"></td>
					<th class="col-1"></th>
					<th class="col-3" style="font-size:9px;text-align:left; padding-left:10px;"><i style="margin-left:10px; ">'.$row->ess_description.' ( '.$existencia.' )</i></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
				</tr>';
				$tot+=$suma;
			}
		}

		$html = $html.'
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">SUB TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot).'</p></td>
                </tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">IVA</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot * 0.16).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td  class="total-text" style="">GRAN TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot * 1.16).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
        </table>
    </div>
	<br><br><br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th class="title grey">INFORMACI&Oacute;N</th>
                    </tr>
                    <tr>
                        <td class="col-6">*** Esta cotizaci&oacute;n ser&aacute; v&aacute;lida durante 30 d&iacute;as a partir de esta fecha.	</td>
                    </tr>
            </table><br>
			<h3>Optima Automotriz, S.A. de C.V.</h3>
			<h3>Departamento de Servicio</h3>
        </div>
    </div>
</div>
';

		$this->load->library('mpdf');
		$this->mpdf->writeHTML($html);
		$new_name = 'documents/estimates/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output();//$new_name, 'F');

	}

	function crear_pdf($id)
	{
		$cotizacion = $this->Cotizacionesmodel->obtener($id);
		$servicios = $this->Cotizacionesmodel->obtener_servicios();
		$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
		$cantidad_servicios = count($servicios_cotizacion);
		$total = $this->Cotizacionesmodel->obtener_total_cotizacion($id);
		$iva = $total[0]->total * 0.16;
		$great_total = $total[0]->total + $iva;

		setlocale(LC_MONETARY, 'en_US');
		$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
.page {
	font-family:Verdana, Geneva, sans-serif;
}
	.head {
		width:100%;
		margin-bottom:0px;

	}
		.contact {
			width:55%;

			float:left;
		}
			.logo {
				width:100%;
				height:70px;
				background-image:url("http://webmarketingnetworks.com/sas/documents/hlogo.png");
				background-size: 80%;
				background-repeat:no-repeat;
			}
			.address {
				width:100%;
				height:100%;
				border:none;
				border-collapse:collapse;
			}
				.address th {
					font-size:9px;
				}
				.address td {
					font-size:8px;
				}
				.address .hours{
					font-size:10px;
					text-align:center;
				}
		.folio {
			width:45%;

			float:left;
		}
			.os-type {
				width:100%;
				height:25px;
				padding-top:20px;
				font-size:18px;
				font-weight:bold;
				text-align:center;
			}
			.info {
				width:60%;
				height:120px;
				font-size:12px;
				margin-left:150px;
			}
				.message {
					width:60%;
					height:60px;
					float:left;
				}
					.message p{
						width:98%;
						margin:auto;
						margin-top:20px;
						padding:5px;
						border:0px solid rgb(51,51,51);
						border-radius:15px;
						background-color:rgb(226,0,43);
						color:rgb(255,255,255);
						font-size:8px;
						text-align:justify;
						line-height:180%;
					}
				.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:10px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}
						.order-date .gray{
							background-color:rgb(208,208,208) ;
							color:rgb(255,255,255);
							font-size:10px;
							font-weight:900;
						}
						.order-date .white{
							height:27px;
							font-size:10px;
							font-weight:900;
							vertical-align:text-top;
							text-align:left;
						}
						.order-date td{
							border:1px solid rgb(255,255,255);
						}

.grey{
	background-color:rgb(208,208,208) ;
}


.comments{
	width:100%;

	float:left;
	font-size:9px;
	text-align:justify;
	vertical-align:top;
}
	.comments .text{
		width:99%;
		height:40px;
		margin:auto;
	}
	.text .title{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
	}

	.title-address-right{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:right;
	color:rgb(226,0,43);
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
	}

	.title-address-left{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:left;
	color:rgb(226,0,43);
	padding-left:3px;
	}

.customer-info {
	width:100%;
	float:left;
	font-size:9px;
}

	.customer-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.customer-info table th{
		border-bottom:1px solid rgb(0,0,0);
	}
	.customer-info table td{
		padding-left:5px;
	}

.vehicle-info {
	width:60%;
	float:left;
	font-size:9px;
}

	.vehicle-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.vehicle-info table th {
		border-bottom:1px solid rgb(51,51,51);
	}
	.vehicle-info table td{
		padding-left:5px;
	}

.details {
	width:70%;
}

.services-head{
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:10px;
	margin-bottom:10px;
}
	.services-head th{
		height:22px;
	}

.services{
	font-size:10px;
	border:1px solid rgb(226,0,43);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}

.first-cell{
	border-right:1px solid rgb(226,0,43);
}

.total{
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(72,72,72);
	border-radius:5px;
}
.total-text{
	text-align:right;
	padding-right:15px;
}

.row{
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child{
	margin-bottom:0px;
}
.floated{
	float:left;
}
.col-12 {
	width:100%;
}
.col-10{
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8{
	width:66.66%;
}
.col-7{
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5{
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:16.5%;
}
.col-1{
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right{
	text-align:right;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left{
	text-align:left;
	padding-left:3px;
}
.radius {
    border: 1px solid rgb(51,51,51);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>

<div class="page">
	<div class="head">
    	<div class="contact">
        	<div class="logo">

			</div>

            	<table class="address">
                    	<tr>
                        	<th class="title-address-right">TIJUANA:</th>
                        	<th class="title-address-left">MEXICALI:</th>
                        </tr>
                    	<tr>
                        	<td class="right">Av. Padre Kino 4300 Zona R&iacute;o, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td class="left">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr>
                        	<th class="title-address-right">ENSENADA:</th>
                        	<td class="hours">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr>
                        	<td class="right">Ensenada Av. Balboa 146 esq.<br />
							L&oacute;pez Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td class="hours">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            S&aacute;bado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>

        </div>

        <div class="folio">
        	<div class="os-type">
        		COTIZACI&Oacute;N
        	</div>
        	<div class="info">
            	<div class="service-order">
                	<table class="order-date radius">
                            <tr class="gray">
                            	<td>FECHA</td>
                            </tr>
                            <tr class="white">
                            	<td><b>
								'.date("Y-m-d", strtotime($cotizacion[0]->est_date)).'
								</b></td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
	<br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="2" class="title grey">CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-2">Nombre: </td>
                        <td class="col-10" style="text-align:left;"><b>'.$cotizacion[0]->est_customer.'</b></td>
                    </tr>
            </table><br>
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="6" class="title grey">VEH&Iacute;CULO</th>
                    </tr>
                    <tr>
                        <td class="col-1">Modelo: </td>
                        <td class="col-4" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_brand.'</b></td>
                        <td class="col-1">A&ntilde;o: </td>
                        <td class="col-2" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_model.'</b></td>
                        <td class="col-1">Color: </td>
                        <td class="col-3" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_color.'</b></td>
                    </tr>
            </table>
        </div>
    </div>
	<br><br><br>
	<div class="row">
    	<table class="radius services-head" style="width:100%;">
        	<tr>
            	<th class="first-cell col-1"></th>
            	<th class="col-1">CANTIDAD</th>
            	<th class="col-3">SERVICIO</th>
            	<th class="col-2">TOTAL<br>REFACCIONES</th>
            	<th class="col-2">TOTAL<br>MANO DE OBRA</th>
            	<th class="col-2">TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
		';

		foreach($servicios_cotizacion as $key => $row)
		{
			$html = $html.'<tr>
					<td class="first-cell col-1">'.(1+$key).'</td>
					<th class="col-1">'.$row->ess_quantity.'</th>
					<th class="col-3" style="text-align:left;">'.$servicios[$row->ess_service].'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_unit_price).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_handwork).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_total_price).'</th>
                </tr>
				<tr>
					<td class="first-cell col-1"></td>
					<th class="col-1"></th>
					<th class="col-3" style="font-size:9px;"><i style="padding-left:10px; ">'.$row->ess_description.'</i></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
				</tr>';
		}

		$html = $html.'
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">SUB TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $total[0]->total).'</p></td>
                </tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">IVA</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $iva).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td  class="total-text" style="">GRAN TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $great_total).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
        </table>
    </div>
	<br><br><br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th class="title grey">INFORMACI&Oacute;N</th>
                    </tr>
                    <tr>
                        <td class="col-6">*** Esta cotizaci&oacute;n ser&aacute; v&aacute;lida durante 30 d&iacute;as a partir de esta fecha.	</td>
                    </tr>
            </table><br>
			<h3>Optima Automotriz, S.A. de C.V.</h3>
			<h3>Departamento de Servicio</h3>
        </div>
    </div>
</div>
';

		$this->load->library('mpdf');
		$this->mpdf->writeHTML($html);
		$new_name = 'documents/estimates/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output($new_name, 'F');
		return $new_name;
	}

	// Ajax functions

	function obtener_cotizaciones_ajax($id)
	{
		$cotizaciones = $this->Cotizacionesmodel->obtener_cotizaciones_por_orden($id);

		echo json_encode($cotizaciones);
	}

}

?>
