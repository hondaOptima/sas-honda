<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cita extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//$this->output->cache(60);
		$this->load->model('Citamodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->helper('dates');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library("Pdf");
		$this->load->library('curl');
		$this->per = $this->Permisomodel->acceso();
	}

	public function index()
    {
		redirect('cita/Prueba?date='.$_GET['date'].'&taller='.$_GET['taller'].'');
	}

	public function tv_sas()
	{
		date_default_timezone_set('America/Tijuana');
		//if(empty($_GET['date'])) $date = date('Y-m-d'); else $date = $_GET['date'];
		$date = date('Y-m-d');
		if(empty($_GET['taller'])) $taller = $_SESSION['sfidws']; else $taller = $_GET['taller'];
		list($an, $ms, $di)=explode('-', $date);
		if($ms==0) $ms=0; else $ms=$ms - 1;

		$data['meses']=$this->Permisomodel->mesesNombre();
		$data['an']=$an;
		$data['ms']=$ms;
		$data['di']=$di;
		$data['taller']=$taller;
		$data['date']=$date;
		$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
		$this->load->view('cita/tv_sas',$data);
	}

	public function pruebaold()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}

		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['sinCita']=$this->Citamodel->citasPorHoraSinCita($date,$taller);
			//$data['total']=$this->Citamodel->citasPorHoraTot($date,$taller);
			//$data['totSin']=$this->Citamodel->citasPorHoraTotSin($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
		    $data['online'] = false;
			$data['permiso']=$this->Citamodel->obtenerPermiso($_SESSION['sfid']);
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
			//$data['citas']=$this->Citamodel->listarCitas($date);
			$this->load->view('cita/vistaCopia',$data);
	}

    public function prueba($fecha = null, $taller = null)
	{
		$data['flash_message'] = $this->session->flashdata('message');

		// if(empty($_GET['date'])) $date=date('Y-m-d'); else $date=$_GET['date'];
		// if(empty($_GET['taller'])) $taller=$_SESSION['sfidws'];
		// else $taller=$_GET['taller'];

		if(empty($fecha)) $date = date('Y-m-d'); else $date = $fecha;
		if(empty($taller)) $taller = $_SESSION['sfidws'];

		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0) $ms=0; else $ms=$ms - 1;
		$data['an']=$an;
		$data['ms']=$ms;
		$data['di']=$di;
		$data['taller']=$taller;
		$data['meses']=$this->Permisomodel->mesesNombre();
		$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
		$data['sig']=$this->Permisomodel->mesSig($ms,$an);
		$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
		$data['sinCita']=$this->Citamodel->citasPorHoraSinCita($date,$taller);
		$data['total']=$this->Citamodel->citasPorHoraTot($date,$taller);
		$data['totSin']=$this->Citamodel->citasPorHoraTotSin($date,$taller);
	    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
		$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
		$data['asesores']=$this->Citamodel->obtenerAsesores();

		$data['asesores_carroceria']=$this->Citamodel->obtenerAsesoresCarroceria();
		$data["asesores"] = $data['asesores'] + $data['asesores_carroceria'];

		$data['servicios']=$this->Citamodel->obtenerServicios();
		$data['modelos']=$this->Citamodel->obtenerModelos();
	    $data['online'] = false;
		$data['permiso']=$this->Citamodel->obtenerPermiso($_SESSION['sfid']);
		$oper=$this->Citamodel->oper($taller);
		$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
		//$data['citas']=$this->Citamodel->listarCitas($date);
		$this->load->view('cita/vistaC',$data);
	}

	/*public function prueba_1000($fecha = null, $taller = null)
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($fecha)) $date = date('Y-m-d'); else $date = $fecha;
		if(empty($taller)) $taller = $_SESSION['sfidws'];

		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0) $ms=0; else $ms=$ms - 1;
		$data['an']=$an;
		$data['ms']=$ms;
		$data['di']=$di;
		$data['taller']=$taller;
		$data['meses']=$this->Permisomodel->mesesNombre();
		$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
		$data['sig']=$this->Permisomodel->mesSig($ms,$an);
		$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
		$data['sinCita']=$this->Citamodel->citasPorHoraSinCita($date,$taller);
		$data['total']=$this->Citamodel->citasPorHoraTot($date,$taller);
		$data['totSin']=$this->Citamodel->citasPorHoraTotSin($date,$taller);
	    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
		$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
		$data['asesores']=$this->Citamodel->obtenerAsesores();

		$data['asesores_carroceria']=$this->Citamodel->obtenerAsesoresCarroceria();
		$data["asesores"] = $data['asesores'] + $data['asesores_carroceria'];

		$data['servicios']=$this->Citamodel->obtenerServicios();
		$data['modelos']=$this->Citamodel->obtenerModelos();
	    $data['online'] = false;
		$data['permiso']=$this->Citamodel->obtenerPermiso($_SESSION['sfid']);
		$oper=$this->Citamodel->oper($taller);
		$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
		//$data['citas']=$this->Citamodel->listarCitas($date);
		$this->load->view('cita/vistaC',$data);
	}*/


	function obtenerAsesores(){
		$asesores = $this->Citamodel->obtenerAs();
		echo json_encode($asesores);
	}
	function obtenerDatos(){

	 $fecha = $_POST['fecha'];
	 $hora = $_POST['hora'];
	 $datos = $this->Citamodel->obtenerAsesoresFecha($fecha, $hora);
	 echo json_encode($datos);
	}


	function imprimir(){

		if(empty($_GET['ase'])){
			$ase=0;
			}else{
				$ase=$_GET['ase'];}

		$data['user']=$ase;
		$data['usuarios'] = $this->Citamodel->listaUsuarios($_SESSION['sfidws']);
		$data['lista']=$this->Citamodel->citasPorHoraAse($_GET['date'],$_GET['taller'],$ase);
			$data['sinCita']=$this->Citamodel->citasPorHoraSinCita($_GET['date'],$_GET['taller']);

			$this->load->view('cita/imprimir',$data);

		}
		public function calendario()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/

		if(empty($_GET['taller'])){
			$taller=2;
			}else{
				$taller=$_GET['taller'];}



		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();

			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);

			$data['citas']=$this->Citamodel->listarCitas($date);
			$this->load->view('cita/calendario',$data);
	}

		public function gracias()
	{

			$this->load->view('cita/gracias');
	}



		public function calpasodos()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/

		if(empty($_GET['taller'])){
			$taller=2;
			}else{
				$taller=$_GET['taller'];}



		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();

			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);

			$data['citas']=$this->Citamodel->listarCitas($date);


			$data['taller']=$_GET['taller'];
			$data['servicio']=$_GET['servicio'];
			$data['modelo']=$_GET['modeloVehiculo'];
			$data['anio']=$_GET['anoVehiculo'];

			$this->load->view('cita/calpasodos',$data);


	}

	public function calpasotres()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		/*if(empty($_GET['date'])){
			$date=date('Y-m-d');
			}else{
				$date=$_GET['date'];}*/

		if(empty($_GET['taller'])){
			$taller=2;
			}else{
				$taller=$_GET['taller'];}



		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();

			$data['online'] = false;
			//$data['date']=$date;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);

			$data['citas']=$this->Citamodel->listarCitas($date);


			$data['taller']=$_GET['taller'];
			$data['servicio']=$_GET['servicio'];
			$data['modelo']=$_GET['modelo'];
			$data['anio']=$_GET['anio'];
			$data['hora']=$_GET['hora'];
			$data['idh']=$_GET['idh'];



			$this->load->view('cita/calpasotres',$data);
	}

	public function porconfirmar()
	{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
			$data['online'] = true;
			$data['date']=$date;
			$data['citas']=$this->Citamodel->listarCitasPorConfirmar($date);
			$this->load->view('cita/lista',$data);
	}

	public function crear()//aqui
	{
		$this->form_validation->set_rules('servicio', 'Servicio', 'required');
		$this->form_validation->set_rules('asesor', 'Asesor Deseado', 'required');
		$this->form_validation->set_rules('clienteNombre', 'Nombre', 'required');
		$this->form_validation->set_rules('clienteApellido', 'Apellido', 'required');
		$this->form_validation->set_rules('clienteTelefono', 'Teléfono', 'required');
		$this->form_validation->set_rules('clienteCorreo', 'Correo Electrónico', 'required');
		$this->form_validation->set_rules('modeloVehiculo', 'Modelo de Vehículo', 'required');
		$this->form_validation->set_rules('anoVehiculo', 'Año de Vehículo', 'required');

		if ($this->form_validation->run())
		{
			if($this->input->post('referencia') == 1)
		   	{
				list($fecha,$idCalendario) = explode("/",$this->input->post('fecha'));
				list($dia,$hora) = explode(" ",$fecha);
				$ocupado = $this->Citamodel->ocupado($idCalendario);
				$folio = 'T-'.rand(10000,100000).'';
			}

			if($this->input->post('referencia') == 2)
			{
				$folio = 'P-'.rand(10000,100000);
				$dia = $this->input->post('fsin');
				$fecha = $this->input->post('fsin').' '.$this->input->post('fechaSin');
			}

			$valet = $this->input->post('necesitaValet');
			if($valet != '1') $valet = 0;

			$cita = array(
			    'app_folio'				=>$folio,
				'app_day'				=>$dia,
				'app_hour'				=>$fecha,
				'app_service'			=>$this->input->post('servicio'),
				'app_desiredAdviser'	=>$this->input->post('asesor'),
				'app_reminderType'		=>4,
				'app_customerName'		=>ucwords(strtolower($this->input->post('clienteNombre'))),
				'app_customerLastName'	=>ucwords(strtolower($this->input->post('clienteApellido'))),
				'app_customerTelephone'	=>$this->input->post('clienteTelefono'),
				'app_customerEmail'		=>$this->input->post('clienteCorreo'),
				'app_vehicleModel'		=>$this->input->post('modeloVehiculo'),
				'app_vehicleYear'		=>$this->input->post('anoVehiculo'),
				'app_message'			=>$this->input->post('mensaje'),
				'app_needValet'			=>$valet,
				'app_onlineConfirmed'	=>1,
				'app_status'			=>0,
				'app_referencia'		=>$this->input->post('referencia'),
				'app_IDtaller'			=>$_GET['taller'],
				'app_tipo_telefono'		=>$this->input->post('tipotel')
			);
			//Insertar Cita en la Base de Datos.
			$ci = $this->Citamodel->agregarCita($cita);
			//Insertar auditoria para ver quien hace las citas.
			$this->Citamodel->insertAuditoria($ci);

            if($this->input->post('referencia') == 1)
			{
				$calendario = array('cal_status'=>2,'cal_app_idAppointment'=>$ci);
				$this->Citamodel->updateCalendarioCitas($calendario,$idCalendario);
			}
			//Enviar carta de confirmacion.
			list($fe,$hora)=explode(' ',$fecha);
			$url = base_url()."ajax/calendarioClientes/cartaCliente.php";
			$post_data = array(
			    "modelo" => $this->input->post('modeloVehiculo'),
			    "asesor" => $this->input->post('asesor'),
			    "recordatorio" => $this->input->post('tipoRecordatorio'),
				"taller" => $_GET['taller'],
				"hora" => $hora.'-'.'00',
				"folio" => $folio.'X'.'00',
				"nombre" => ucwords(strtolower($this->input->post('clienteNombre'))).ucwords(strtolower($this->input->post('clienteApellido'))),
				"services" => $this->input->post('servicio'),
				"fechaselec" => $fe,
				"email" => $this->input->post('clienteCorreo'),
			);

			$this->curl->simple_post($url, $post_data);

			//Aqui vamos a usar un CURL para agregar los datos de carroceria con $this->input->post('servicio') con el
			//id 59 de la tabla "serviceTypes" en el campo "set_idServiceType".
			if(intval($this->input->post('servicio')) == 59)
				$this->agregar_carroceria($ci);

			//Fin de la carta de confirmacion.
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Ha generado una nueva cita</div>');
			// redirect('cita/Prueba?date='.$dia.'&taller='.$_GET['taller'].'');
			redirect('citas/'.$dia.'/'.$_GET['taller']);
		}
		else
		{
			$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Hora Ocupada</strong> Por favor seleccione una hora distinta</div>');
			// redirect('cita?date='.$dia.'&taller='.$_GET['taller'].'');
			redirect('citas/'.$dia.'/'.$_GET['taller'].'');
		}
	}

	function agregar_carroceria($id)//aqui
	{
		$this->load->model('Carroceriamodel','carroceria');
		$datos = array(
			//"tipo" => $_POST["tipo"],
			"car_id_appointment" => $id
			//"auto" => $_POST["auto"],
			//"poliza" => $_POST["poliza"],
			//"siniestro" => $_POST["siniestro"],
			//"fecha_recibido" => $_POST["fecha_recibido"],
			//"fecha_enviado" => $_POST["fecha_enviado"],
			//"fecha_enviado" => $_POST["fecha_enviado"],
			//"fecha_enviado" => $_POST["fecha_enviado"],
		);

		$r = $this->carroceria->agregar_siniestro($datos);
	}

	public function editar($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['cita'] = $this->Citamodel->obtenerCita($id);
		if($data['cita'])
		{
			$data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
			$this->load->view('cita/editar',$data);

		}
		else
		{
			redirect('error');
		}
	}

	public function editarSin($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['cita'] = $this->Citamodel->obtenerSinCita($id);
		if($data['cita'])
		{
			$data['recordatorios']=$this->Citamodel->obtenerRecordatorios();
			$data['vehiculos']=$this->Citamodel->obtenerVehiculos();
			$data['asesores']=$this->Citamodel->obtenerAsesores();
			$data['servicios']=$this->Citamodel->obtenerServicios();
			$data['modelos']=$this->Citamodel->obtenerModelos();
			$this->load->view('cita/editar',$data);

		}
		else
		{
			redirect('error');
		}
	}

	public function actualizar($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('servicio', 'Servicio', 'required');
		$this->form_validation->set_rules('asesor', 'Asesor Deseado', 'required');
		$this->form_validation->set_rules('tipoRecordatorio', 'Tipo de Recordatorio', 'required');
		$this->form_validation->set_rules('clienteNombre', 'Nombre', 'required');
		$this->form_validation->set_rules('clienteApellido', 'Apellido', 'required');
		$this->form_validation->set_rules('clienteTelefono', 'Teléfono', 'required');
		$this->form_validation->set_rules('clienteCorreo', 'Correo Electrónico', 'required');
		$this->form_validation->set_rules('modeloVehiculo', 'Modelo de Vehículo', 'required');
		$this->form_validation->set_rules('anoVehiculo', 'Año de Vehículo', 'required');

		if($this->form_validation->run())
		{


			$valet = $this->input->post('necesitaValet');
			if($valet != '1')
				$valet = 0;

			$cita = array(
				'app_service'=>$this->input->post('servicio'),
				'app_desiredAdviser'=>$this->input->post('asesor'),
				'app_reminderType'=>$this->input->post('tipoRecordatorio'),
				'app_customerName'=>$this->input->post('clienteNombre'),
				'app_customerLastName'=>$this->input->post('clienteApellido'),
				'app_customerTelephone'=>$this->input->post('clienteTelefono'),
				'app_customerEmail'=>$this->input->post('clienteCorreo'),
				'app_vehicleModel'=>$this->input->post('modeloVehiculo'),
				'app_vehicleYear'=>$this->input->post('anoVehiculo'),
				'app_message'=>$this->input->post('mensaje'),
				'app_needValet'=>$valet,
			);

		 $this->Citamodel->actualizarCita($cita,$id);
		redirect('cita/?taller='.$_GET['taller'].'');
		}
		else
		{
			redirect('cita/editar/'.$id.'/?taller='.$_GET['taller'].'');
		}
	}

	public function confirmar($id)
	{
		$this->Citamodel->confirmarCita($id);
		redirect('cita?date='.$_GET['date'].'&taller='.$_GET['taller'].'');
	}

	public function confirmaronline($id)
	{
		$this->Citamodel->confirmarCitaOnline($id);
		redirect('cita');
	}

	public function cancelar($idapp,$idcal)
	{
		$this->Citamodel->liberarHora($idcal);
		$this->Citamodel->cancelarCita($idapp);
		//redirect('cita?date='.$_GET['date'].'&taller='.$_GET['taller'].'');
		redirect('citas/'.$_GET['date'].'/'.$_GET['taller']);
	}

	public function cancelarSin($id)
	{

		$this->Citamodel->cancelarCita($id);
		redirect('cita?taller='.$_GET['taller']);
		redirect('citas');
	}

	function reagendar($id)
	{
		$data['cita'] = $this->Citamodel->obtenerCita($id);
		if($data['cita'])
		{
		if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
		$data['date']=$date;
		list($an,$ms,$di)=explode('-',$date);
		if($ms==0){$ms=0;}else{$ms=$ms - 1;}
			$data['an']=$an;
			$data['ms']=$ms;
			$data['di']=$di;
			$data['idc']=$id;
			$data['taller']=$taller;
			$data['meses']=$this->Permisomodel->mesesNombre();
			$data['ant']=$this->Permisomodel->mesAnt($ms,$an);
			$data['sig']=$this->Permisomodel->mesSig($ms,$an);
			$data['lista']=$this->Citamodel->citasPorHora($date,$taller);
			$data['tot']=$this->Citamodel->citasPorHoraTot($date,$taller);
		    $data['recita'] = $this->Citamodel->obtenerCita($id);
			$data['online'] = false;
			$oper=$this->Citamodel->oper($taller);
			$data['porcentaje']=$this->Citamodel->porcentaje($date,$oper);
			$data['citas']=$this->Citamodel->listarCitas($date);
			$this->load->view('cita/reagendar',$data);
		}
		else
		{
			redirect('error');
		}
	}

	function registrareagendada(){


	list($fecha,$hora,$idhora,$lastcalID,$idApp)=explode('/',$_GET['idncita']);
	//actulizamos los datos de la cita
	$cita = array(
				'app_day'=>$fecha,
				'app_hour'=>$fecha.' '.$hora.':00'
			);

		 $this->Citamodel->actualizarCita($cita,$_GET['idc']);

	//liberamos hora anterior de la tabla calendario
	$this->Citamodel->liberarHora($lastcalID);
	//actualizamos los datos de la nueva cita en la tabla calendario
	$this->Citamodel->reagendarNuevaHora($idhora,$idApp);
	//enviar citas
	redirect('cita?taller='.$_GET['taller'].'');

		}


	public function citasPorConfirmar() {
		$citasPorConfirmar = $this->Citamodel->listarPorConfirmarAjax();
		echo json_encode($citasPorConfirmar);
	}

	function pdfOrdendeCompra(){
		  $pdf = new TCPDF('L', PDF_UNIT, 'US Legal', true, 'UTF-8', false);
		  $pdf->AddPage();

		  $html = 'ok ';

		  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
		  $pdf->Output('Hoja_de_entrega.pdf', 'I');
		}

		function porcentajeCita()
		{
			$IDT = $_GET['taller'];//ID del Taller
			$HR = 1;// horas que tarda el servicio
			$DATE = $_GET['fecha'];
			$lista = $this->Citamodel->listaPorcentajeCitas($IDT);
			$output = array(); // it will wrap all of your value

			foreach($lista as $row)
			{
				unset($temp);//Release the contained value of the variable from the last loop
				if($row->cpo_status == '2')
				{
					$titulo = '';
					$color = "#cc0000";
					$url = "";
				}

				if($row->cpo_status=='1')
				{
					$titulo = '';
					$color = "#1ec138";
					//   $url = base_url()."cita/Pruebas?date=".$row->cpo_fecha."&taller=".$IDT;
					$url = base_url()."citas/".$row->cpo_fecha."/".$IDT;
				}

				if($row->cpo_status=='3')
				{
					$titulo = '';
					$color = "#fcb322";
					//   $url = base_url()."cita?date=".$row->cpo_fecha."&taller=".$IDT;
					$url = base_url()."citas/".$row->cpo_fecha."/".$IDT;
				}

				if($row->cpo_fecha==$DATE)
					$color="#333";

				$temp = array(
					'id' 		=> $row->cpo_ID,
					'title' 	=> $titulo,
					'start' 	=> $row->cpo_fecha,
					'url' 		=> $url,
					'className' => '',
					'color' 	=> $color
				);

				array_push($output,$temp);
			}

			header('Access-Control-Allow-Origin: *');
			header("Content-Type: application/json");

			echo json_encode($output);
		}


		function porcentajeCitaCliente(){
		$IDT=$_SESSION['sfidws'];//ID del Taller
		$HR=1;// horas que tarda el servicio
        $DATE=$_GET['fecha'];
        $lista= $this->Citamodel->listaPorcentajeCitas($IDT);
         $output = array(); // it will wrap all of your value
    foreach($lista as $row){
          unset($temp); // Release the contained value of the variable from the last loop
           if($row->cpo_status=='2'){
			  $titulo='Ocupado';
			  $color="#cc0000";
			  $url="";
		  }
		   if($row->cpo_status=='1'){
			  $titulo='Disponible';
			  $color="#008000";
			  $url="".base_url()."cita/calpasodos?date=".$row->cpo_fecha."&taller=".$IDT."&servicio=".$_GET['servicio']."&modeloVehiculo=".$_GET['modeloVehiculo']."&anoVehiculo=".$_GET['anoVehiculo']."";
			  }
			    if($row->cpo_status=='3'){
			  $titulo='fes';
			  $color="#fcb322";
			  $url="".base_url()."cita/calpasodos?date=".$row->cpo_fecha."&taller=".$IDT."&servicio=".$_GET['servicio']."&modeloVehiculo=".$_GET['modeloVehiculo']."&anoVehiculo=".$_GET['anoVehiculo']."";
			  }

		   if($row->cpo_fecha==$DATE){
			   $color="#fcb322";
			   }
			  $temp = array(
		    'id' =>$row->cpo_ID,
			'title' =>$titulo,
			'start' =>$row->cpo_fecha,
			'url' => $url,
			'className' => '',
			'color'=> $color

		  );


          array_push($output,$temp);
    }
    header('Access-Control-Allow-Origin: *');
    header("Content-Type: application/json");
    echo json_encode($output);


		}


		function porcentajeCitaReagendar(){
		$IDT=$_GET['taller'];//ID del Taller
		$HR=1;// horas que tarda el servicio
        $DATE=$_GET['fecha'];
        $lista= $this->Citamodel->listaPorcentajeCitas($IDT);
         $output = array(); // it will wrap all of your value
    foreach($lista as $row){
          unset($temp); // Release the contained value of the variable from the last loop
           if($row->cpo_status=='2'){
			  $titulo='';
			  $color="#cc0000";
			  $url="";
		  }
		   if($row->cpo_status=='1'){
			  $titulo='';
			  $color="#1ec138";
			  $url="".base_url()."cita/reagendar/".$_GET['idc']."/?date=".$row->cpo_fecha."&taller=".$IDT."";
			  }




		   if($row->cpo_fecha==$DATE){
			   $color="#333";
			   }
			  $temp = array(
		    'id' =>$row->cpo_ID,
			'title' =>$titulo,
			'start' =>$row->cpo_fecha,
			'url' => $url,
			'className' => '',
			'color'=> $color

		  );


          array_push($output,$temp);
    }
    header('Access-Control-Allow-Origin: *');
    header("Content-Type: application/json");
    echo json_encode($output);


		}

function eje_curl(){


	}
function enviarInventarioOrden($vin){
	$cita = array(
			    'app_folio'=>'p-11111',
				'app_day'=>date('Y-m-d'),
				'app_hour'=>date('Y-m-d H:i:s'),
				'app_service'=>'50',
				'app_desiredAdviser'=>67,
				'app_reminderType'=>'1',
				'app_customerName'=>'Previa: ',
				'app_customerLastName'=>$vin,
				'app_customerTelephone'=>'',
				'app_customerEmail'=>'',
				'app_vehicleModel'=>'0',
				'app_vehicleYear'=>'2015',
				'app_message'=>'',
				'app_needValet'=>'0',
				'app_onlineConfirmed'=>1,
				'app_status'=>1,
				'app_referencia'=>2,
				'app_IDtaller'=>$_SESSION['sfidws'],
				'app_tipo_telefono'=>''
			);

    /*Array ( [app_folio] => P-37223 [app_day] => 2015-11-18 [app_hour] => 2015-11-18 05:06:00 [app_service] => 50 [app_desiredAdviser] => 67 [app_reminderType] => 1 [app_customerName] => Previa [app_customerLastName] => _ [app_customerTelephone] => 6641685181 [app_customerEmail] => liusber_00@hotmail.com [app_vehicleModel] => 0 [app_vehicleYear] => 2015 [app_message] => Previa [app_needValet] => 0 [app_onlineConfirmed] => 1 [app_status] => 0 [app_referencia] => 2 [app_IDtaller] => 2 [app_tipo_telefono] => celular )*/
			//insertar Cita en la Base de Datos


    echo $ci=$this->Citamodel->agregarCita($cita);
	}


	function insertar()
	{
		$data = array(
			$folio => $_POST['app_folio'],
			$day => $_POST['app_day'],
			$hora => $_POST['app_hour'],
			$confir => $_POST['app_confirmed'],
			$remin => $_POST['app_remind'],
			$wasTim => $_POST['app_wasTimely'],
			$mess => $_POST['app_message'],
			$time => $_POST['app_timeAttended'],
			$cust => $_POST['app_customerArrival'],
			$desired => $_POST['app_desiredAdviser'],
			$atten => $_POST['app_attendedBy'],
			$remin => $_POST['app_reminderType'],
			$service => $_POST['app_service'],
			$customNam => $_POST['app_customerName'],
			$customLaNam => $_POST['app_customerLastName'],
			$customTele => $_POST['app_customerTelephone'],
			$customEmail => $_POST['app_customerEmail'],
			$customer => $_POST['app_customer'],
			$vehicle => $_POST['app_vehicleModel'],
			$otroModel => $_POST['app_otroModelo'],
			$vehicleYear => $_POST['app_vehicleYear'],
			$notes => $_POST['app_notes'],
			$needValet => $_POST['app_needValet'],
			$online => $_POST['app_onlineConfirmed'],
			$status => $_POST['app_status'],
			$references => $_POST['app_referencia'],
			$idTaller => $_POST['app_IDtaller'],
			$tipoTelefono => $_POST['app_tipo_telefono'],
			$timeToOrder => $_POST['app_timeToOrder'],
			$vin => $_POST['vin'],
			$sointe => $_POST['app_sointelisis']
		);

		$this->Citamodel->insertAppointmentSOAPP($data);
	}

}
