<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends CI_Controller {

	public function __construct()
	{   session_start();
		parent::__construct();
		$this->load->model('Accesomodel');
		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		
	}
	
	public function index()
	{
		$this->load->view('error/page-404');

	}
}