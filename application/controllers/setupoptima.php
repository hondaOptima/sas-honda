<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setupoptima extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Setupoptimamodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");;				
	}

	public function DeleteDir(){
		$this->Setupoptimamodel->DeleteDirecciones();
	}

	public function Test(){
		$this->Setupoptimamodel->NewCustomers();
	}
	
	public function Transfercustomers(){		
		$this->Setupoptimamodel->Transfercustomers();
	}

	public function CustomersToOptima(){
		$this->Setupoptimamodel->CustomersToOptima();
	}

	public function VehiclesToOptima(){
		$this->Setupoptimamodel->VehiclesToOptima();
	}

	public function CrmToOptima(){
		$this->Setupoptimamodel->CrmToOptima();
	}

	public function NewCustomers(){
		$this->Setupoptimamodel->NewCustomers();
	}
	
	public function index_inv()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}	
		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		$this->load->view('pizarron/vista_copia',$data);
	}
	function cambiardia(){
		$this->Pizarronmodel->updateDia($_POST['ido'],$_POST['fecha']);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Torre Cambiada de Fecha.</div>');
		list($an,$ms,$dd)=explode("-",$_POST['fecha']);
		$fecha=$dd.'-'.$ms.'-'.$an;
		redirect('pizarron?fecha='.$fecha.'');	
	}
	


	
}