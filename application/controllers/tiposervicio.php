<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TipoServicio extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tiposerviciomodel');
		
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		$data['tipos']=$this->Tiposerviciomodel->listarTipos($this->per);
		$this->load->view('tiposervicio/vista', $data);
	}
	
	public function crear()
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		
		
        if ($this->form_validation->run())
        {
			$tipo = array(
						'set_name'=>$this->input->post('nombre'),
						'set_isActive'=>'1',
						'set_codigo'=>$this->input->post('codigo'),
						'set_tipo'=>$this->input->post('tipo'),
						'set_orden'=>$this->input->post('orden')
						);
$this->load->model('Serviciomodel');
		   	$ID=$this->Tiposerviciomodel->agregarTipo($tipo);
			$tipo=$this->input->post('tipo');			
			
			$tipo=$this->input->post('tipo');
			$modelos=$this->Serviciomodel->obtenerModelos();
			if($tipo=='pro'){
			 foreach($modelos as $mod){
				 
				  $nopro=array('ser_price' => '0',
				  'ser_refacciones' => '0',
				  'ser_mat_varios' => '0',
				  'ser_comercial' => '',
				  'ser_vehicleModel' => $mod->vem_idVehicleModel,
				  'ser_serviceType' => $ID,
				  'ser_isActive' => '1',
				  'ser_approximate_duration' => '0',
				  'ser_tipo' => 'pro');
				 $this->Serviciomodel->agregarServicio($nopro);	
				
				}}
				
				if($tipo=='nopro'){
			 foreach($modelos as $mod){
				 
				  $nopro=array('ser_price' => '0',
				   'ser_refacciones' => '0',
				  'ser_mat_varios' => '0',
				  'ser_comercial' => '',
				  'ser_vehicleModel' => $mod->vem_idVehicleModel,
				  'ser_serviceType' => $ID,
				  'ser_isActive' => '1',
				  'ser_approximate_duration' => '0',
				  'ser_tipo' => 'nopro');
				 $this->Serviciomodel->agregarServicio($nopro);	
				
				}}
			
			redirect('tiposervicio');
		} 
		else
		{
			$this->load->view('tiposervicio/crear');
		}
	}
	
	public function editar($id)
	{
	 	$data['tipo'] = $this->Tiposerviciomodel->obtenertipo($id);
		
		if($data['tipo'])
		{
			$this->load->view('tiposervicio/editar', $data);
		} 
		else
		{
			redirect('error');
		}
	}
	
	public function actualizar($id)
	{
		
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');		
		
        if ($this->form_validation->run())
        {
			$tipo = array(
						'set_name'=>$this->input->post('nombre'),
						'set_codigo'=>$this->input->post('codigo'),
						'set_orden'=>$this->input->post('orden'),
						);

						
		   	$this->Tiposerviciomodel->actualizarTipo($tipo, $id);
			
			redirect('tiposervicio');
		} 
		else
		{
			redirect('tiposervicio/editar',$id);
		}
	}
	
	function desactivar($id){
	 	$this->Tiposerviciomodel->desactivarTipo($id);
		redirect('tiposervicio');
	}
	
	function activar($id){
	 	$this->Tiposerviciomodel->activarTipo($id);
		redirect('tiposervicio');
	}
	
	
}