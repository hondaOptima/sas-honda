<?php

class Multipuntos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ordendeserviciomodel');
		$this->load->model('Imagenmodel');
		$this->load->model('Multipuntosmodel');
		$this->load->model('Permisomodel');
		$this->load->model('Productividadmodel');
		$this->load->library('curl');
		$this->load->helper('url');
		$this->load->library('pdf');
        $this->load->library('mpdf');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');

        session_start();
        $this->cliente = 0;
		if(!isset($_SESSION['sfid'])) {$this->cliente = 1;}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];

	}


	function index2($orden){

        $data['cliente'] = $this->cliente;
        //obtiene los datos
        $ordenInt = 0;
        try{
            $ordenInt = (int)$orden;
            $data['orden']=$this->Multipuntosmodel->getMulti($ordenInt);
        }catch(Exception $e){
            $data['orden']=$this->Multipuntosmodel->getMulti($ordenInt);
        }


        //contador para organizar los datos a mandar a la pagina
        if(COUNT($data['orden']) > 0){
            $i = 1;

            //valores a saltarse (son numericos y no son parte de los semaforos)
            $valoresSaltar = array('tual', 'cero', 'prof', 'pres');

            //se recorren los resultados
            foreach($data['orden'][0] as $nomOrden => $valOrden)
            {
                //si el nombre del valor se encuentra dentro del arreglo de valores a saltarse (o si es ID, pero al cambiar el modelo se elimina / hace obsoleto)
                if(in_array(substr($nomOrden.'', -4), $valoresSaltar))
                {
                    //se pasa el valor directo
                    $data[$nomOrden] = $valOrden;
                }
                else
                {
                    //sino, se obtiene el status y se pone en formato de Mn y se consigue el siguiente numero
                    $data['M'.$i] = $this->getStatus($valOrden);
                    $i++;
                }
            }

            //se almacena el ID de la orden en data (para mandarlo a la vista)
            $data['idOrden'] = $orden;

            $this->load->view('multipuntos/index',$data);
            //$html = file_get_contents('http://stackoverflow.com/questions/ask');
            //echo $html;
        }else{
            $this->load->view('multipuntos/nodata');
        }


	}

    function index($orden){

        $data['cliente'] = $this->cliente;
        //obtiene los datos
        $ordenInt = 0;
        try{
            $ordenInt = (int)$orden;
            $data['orden']=$this->Multipuntosmodel->getMulti($ordenInt);
        }catch(Exception $e){
            $data['orden']=$this->Multipuntosmodel->getMulti($ordenInt);
        }


        //contador para organizar los datos a mandar a la pagina
        if(COUNT($data['orden']) > 0){
            $i = 1;

            //valores a saltarse (son numericos y no son parte de los semaforos)
            $valoresSaltar = array('tual', 'cero', 'prof', 'pres');

            //se recorren los resultados
            foreach($data['orden'][0] as $nomOrden => $valOrden)
            {
                //si el nombre del valor se encuentra dentro del arreglo de valores a saltarse (o si es ID, pero al cambiar el modelo se elimina / hace obsoleto)
                if(in_array(substr($nomOrden.'', -4), $valoresSaltar))
                {
                    //se pasa el valor directo
                    $data[$nomOrden] = $valOrden;
                }
                else
                {
                    //sino, se obtiene el status y se pone en formato de Mn y se consigue el siguiente numero
                    $data['M'.$i] = $this->getStatus($valOrden);
                    $i++;
                }
            }

            //se almacena el ID de la orden en data (para mandarlo a la vista)
            $data['idOrden'] = $orden;
            $data['multi_exist'] = 1;
            $this->load->view('multipuntos/index_app',$data);
            //$html = file_get_contents('http://stackoverflow.com/questions/ask');
            //echo $html;
        }else{
            $data['multi_exist'] = 0;
            $data['idOrden'] = $orden;
            $this->load->view('multipuntos/index_app', $data);
        }


	}


    function getStatus($status){
        $statusArray;
        switch ($status) {
            case 0:
                $statusArray['verde'] = "active";
                $statusArray['amarillo'] = "";
                $statusArray['rojo'] = "";

                break;
            case 1:
                $statusArray['verde'] = "";
                $statusArray['amarillo'] = "active";
                $statusArray['rojo'] = "";

                break;
            case 2:
                $statusArray['verde'] = "";
                $statusArray['amarillo'] = "";
                $statusArray['rojo'] = "active";

                break;
            default:
                $statusArray['verde'] = "";
                $statusArray['amarillo'] = "";
                $statusArray['rojo'] = "";
                break;
        }
        $object = (object)$statusArray;
        return $object;
    }


	function editmultipuntos()
	{
		$this->Multipuntosmodel->editMulti();
		echo 1;
	}


    function setMultipuntos()
    {

        //el campo se asigna directo, se valida junto con la tabla
        $campo = $this->input->get('campo');

        //la orden y el valor se asignan directamente, solo se checa si son numericos (al final)
        $orden = $this->input->get('orden');
        $valor = $this->input->get('valor');

        //la tabla se obtiene en base al campo
        $tabla = '';
        switch(substr($campo.'', 0, 3))
        {
            case "mul":
                $tabla = 'multipuntos';
                break;
            case "lla":
                $tabla = 'llantas';
                break;
            case "fre":
                $tabla = 'frenos';
                break;

            default:
                $tabla = '';
        }

        //para este punto, ya se valido lo que se necesita validar.
        //se obtiene el campo de la orden segun el nombre de la tabla
        $campoOrd = '';
        if(strtolower($tabla.'') == "llantas")
            { $campoOrd = substr($tabla, 0, 4); }
        else
            { $campoOrd = substr($tabla, 0, 3); }

        //se completa el nombre del campo
        $campoOrd .= "_orden";

        $resultado = false;
        //si los datos son correctos, ejecutar en base de datos (si no es correcto, se queda como fallido).
        if(!($tabla == '') && is_numeric($orden) && is_numeric($valor))
            $resultado = $this->Multipuntosmodel->updMulti($orden, $tabla, $campo, $valor, $campoOrd);

        //desplegar resultado segun el resultado
        if($resultado)
            echo '{"status" : 0, "message" : "Order updated successfuly", "valor" : "'.$valor.' '.$tabla.' '.$campoOrd.'" }';
        else
            echo '{"status" : 1, "errorMessage" : "Failed to update order", "valor" : "'.$valor.' '.$tabla.' '.$campoOrd.'"}';
    }


    function GetPDF(){


         $html = '
          <!DOCTYPE html>
         <html lang="en">
         <style>


#multi-bar{
    margin-bottom:50px;
    position: fixed;
    z-index: 1;
    width: 100%;
    background-color:rgba(211, 52, 61, 1) !important;
    color: white;
    box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
    transition: all 0.2s ease-in-out;
}

.menudisplay{
    display: block;
}

.columna1{
    margin-top:200px;
    margin-left: 15px;
}
.columna{

    margin-right: 15px;
    /*background-color: #F5F5F5;*/
    float: left;
    margin-top: 120px;
    border-radius: 5px;
    border: 1.5px solid #E0E0E0;
}

.columna:hover{
    box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
}

.firstcol{
    margin-bottom: 5px;
    margin-top: 50px;
    width: 100%;
}

.firstcol1{
    margin-bottom: 5px;
    margin-top: 190px;
    width: 100%;
}

.fctitles{
    background-color: gray;
    border-radius: 3px;
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    transition: all 0.2s ease-in-out;
    color:white;
    width: 100%;
    float: left;
}

.semaforos{
    margin: 5px;
    width:100%;
}

.h4-llantas{
    margin: 0;
    margin-bottom: 2px;
}

.div-semaforo{
    margin-bottom: 30px;
    margin-top:20px;
    float:left;
    width: 100%;
    border-radius: 3px;
    color:#000000;
    cursor: pointer;
    border: 1px solid gray;
}

.div-semaforo:hover{
    /*background-color: rgba(233, 233, 232, 0.4);*/
    background-color: rgba(255, 19, 37, 0.1);
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}

.fa-dot-circle-o{
    margin-left: 3px;
}

.semaforo{
    width:22%;
    float:left;
    margin: 2px;
    border-radius: 3px;
    padding: 4px;
    /*background-color: rgba(163, 162, 160, 0.9);*/
    border: 1.5px solid #E0E0E0;
}

.circle1{color: rgba(0, 134, 0, 0.9); }.circle2{color: rgba(255, 154, 32, 1);}.circle3{color: red;}

.lbl-semaforo{
    margin-top: 10px;
}


.fa-3x{
    margin: 2.5px;
    opacity: 0.2;
}


.div-contador{
    width:50px;
    float: left;
    margin: 5px;
    border-radius: 5px;
    margin-top: 8px;
}

.cont{
    width: 25px;
    height: 25px;
    border-radius: 20px;
    margin: 3px;
    color: white;
    padding-left: 5px;
}

.div-verde{
    width: 25px;
    height: 25px;
    border-radius: 20px;
    margin: 3px;
    float:left;
    color: white;
    padding-left: 5px;
}

.first-lbl-cont{
    margin-top: 15px;
}

.first-h4-llantas{
    margin-top: 2px !important;
}

.first-semaforo-llanta{
    margin-top: 8px
}

.frm-llantas{
    margin: 0;
}



.i-llanta{
    margin-top: 11px !important;
    color: black !important;
    opacity: 1 !important;
}

.form-control{
    padding: 4px !important;
    width: 33.2% !important;
    margin-left: 1px !important;

}

.div-lbl-cont{
    margin-bottom: 2px;
}

.div-container-lbl-cont{
    float: left;
}

.div-title-diag{
    float: center;
}

.cont-1{
    background-color: green;
}

.cont-2{
    background-color: rgba(255, 154, 32, 1);
}

.cont-3{
    background-color: red;
}

.active{
    opacity: 1;
}

#menu-bars{
    display: block;
}

input[type=checkbox] {
    zoom:5;
    margin-left: 30%;
    margin-top: 5%;
}

.divs-semaforos{
    border-radius: 15px;
}



         </style>
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                <title>Multipuntos Honda Optima</title>

                <!-- Bootstrap -->
                <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                <link href="<?php echo base_url();?>assets/css/multi/multi.css" rel="stylesheet">

                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
                <!-- Aqui va todo  -->
                <div id="multi-bar" class=" ">
                    <div class="div-contador">
                        <div class="cont-1 cont">
                            <label id="circle1">0</label>
                        </div>
                        <div class="cont-2 cont">
                            <label id="circle2">0</label>
                        </div>
                        <div class="cont-3 cont">
                            <label id="circle3">0</label>
                        </div>
                    </div>
                    <div id="menu-bars" >
                        <i class="fa fa-bars fa-2x"></i>
                    </div>
                    <div class="div-container-lbl-cont">
                        <div class="first-lbl-cont div-lbl-cont">
                        <label class="lbl-cont">OK</label>
                        </div>
                        <div class="div-lbl-cont">
                            <label class="lbl-cont">Necesita servicio</label>
                        </div>
                        <div class="div-lbl-cont">
                            <label class="lbl-cont">Alineacion inmediata</label>
                        </div>
                    </div>
                    <div class="div-title-diag text-center">
                        <h2>
                            Hoja de diagnostico de su automovil
                        </h2>
                    </div>
                </div>
                <div class="columna columna1">
                    <div id="div-interior" class="firstcol1 col-md-12">
                        <div  class="fctitles col-md-12">
                            <h4 class="text-center">Interior / Exterior</h4>
                        </div>
                        <div class="semaforos">
                             <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_luztablero" ><i class="fa circle1 fa-3x opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle2 opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle3 opa-i">&#xf058;</i></i>
                                </div>

                                    <h5 class="lbl-semaforo">Luz de tablero</h4>

                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate " id="mul_luces"><i class="fa circle1 fa-3x opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle2 opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle3 opa-i">&#xf058;</i></div>
                                <h5 class="lbl-semaforo">Luces, luces traseras, direccionales, stops, luces de emergencia, cuartos.</h4>
                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_parabrisas"><i class="fa circle1 fa-3x opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle2 opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle3 opa-i">&#xf058;</i></i></div>
                                <h5 class="lbl-semaforo">Operacion de limpiaparabrisas, plumas, liquido lipiaparabrisas.</h4>
                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate"  id="mul_frenomano"><i class="fa circle1 fa-3x opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle2 opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle3 opa-i">&#xf058;</i></i></div>
                                <h5 class="lbl-semaforo">Freno de mano.</h4>
                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate"  id="mul_claxon"><i class="fa circle1 fa-3x opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle2 opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle3 opa-i">&#xf058;</i></i></div>
                                <h5 class="lbl-semaforo">Operacion del claxon.</h4>
                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_clutch"><i class="fa circle1 fa-3x opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle2 opa-i">&#xf058;</i><i class="fa fa-check-circle fa-3x circle3 opa-i">&#xf058;</i></i></div>
                                <h5 class="lbl-semaforo">Operacion del clutch o embrague.</h4>
                            </div>
                        </div>

                        <div class="fctitles " style="margin-top:100px">
                                <h4 class="text-center">Debajo de la unidad</h4>
                            </div>

                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_gomas"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Gomas de suspension.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_basesamortiguadores"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Bases de amortiguadores.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_terminalesdireccion"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Terminales de direccion.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_amortiuadores"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Amortiguadores.</h4>
                        </div>

                        </div>

                    </div>


                </div>

                <div class="columna columna2">
                    <div id="div-debajocofre" class="firstcol col-md-12">
                        <div class="fctitles col-md-12">
                            <h4 class="text-center">Debajo del cofre</h4>
                        </div>

                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_soportes"><i class="fa fa-check-circle fa-3x circle1 opa-i"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Soportes de motor.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_liquidos"><i class="fa fa-check-circle fa-3x circle1 opa-i"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Liquidos: aceite, anticongelante,<br> direccion, transmision, frenos.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_radiadores"><i class="fa fa-check-circle fa-3x circle1 opa-i"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Radiadores, mangueras, bandas.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_ajustefreno"><i class="fa fa-check-circle fa-3x circle1 opa-i"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Ajuste de freno de mano, lineas.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_flechas"><i class="fa fa-check-circle fa-3x circle1 opa-i"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Flechas y juntas homocineticas.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_escape"><i class="fa fa-check-circle fa-3x circle1 opa-i"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Sistema de escape.</h4>
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_fugas"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h5 class="lbl-semaforo">Fugas de aceite.</h4>
                        </div>

                    </div>
                    <div id="div-debajo-unidad" class=" col-md-12">

                        <div class="fctitles col-md-12">
                                <h4 class="text-center">Funcionamiento de la Bateria</h4>
                            </div>

                            <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_electrolito"><i class="fa fa-calendar-check-o fa-3x circle1 opa-i"></i><i class="fa fa-calendar-check-o fa-3x circle2 opa-i"></i><i class="fa fa-calendar-check-o fa-3x circle3 opa-i"></i></div>
                                <h5 class="lbl-semaforo">Nivel de electrolito.</h4>
                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_terminales"><i class="fa fa-calendar-check-o fa-3x circle1 opa-i"></i><i class="fa fa-calendar-check-o fa-3x circle2 opa-i"></i><i class="fa fa-calendar-check-o fa-3x circle3 opa-i"></i></div>
                                <h5 class="lbl-semaforo">Condicion de terminales.</h4>
                            </div>
                            <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="mul_sujecion"><i class="fa fa-calendar-check-o fa-3x circle1 opa-i"></i><i class="fa fa-calendar-check-o fa-3x circle2 opa-i"></i><i class="fa fa-calendar-check-o fa-3x circle3 opa-i"></i></div>
                                <h5 class="lbl-semaforo">Estado fisico y de sujecion.</h4>
                            </div>

                    </div>
                </div>

                <div class="columna columna3">
                    <div id="div-debaj" class="firstcol col-md-12">
                        <div class="fctitles col-md-12">
                            <h4 class="text-center">Condicion De Las Llantas</h4>
                        </div>

                        <div class="div-semaforo first-semaforo-llanta">
                                <div class="semaforo unvalidate" id="llan_di"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center first-h4-llantas h4-llantas">Delantera izquierda</h4>
                                <input id="llan_diprof" type="number" class="lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" />
                                <input id="llan_dipres" type="number" class="lbl-semaforo frm-llantas" placeholder="Presion PSI" />
                        </div>

                        <div class="div-semaforo first-semaforo-llanta">
                                <div class="semaforo unvalidate" id="llan_dd"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Delantera derecha</h4>
                                <input id="llan_ddprof" type="number" class="lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" />
                                <input id="llan_ddpres" type="number" class="lbl-semaforo frm-llantas" placeholder="Presion PSI" />
                        </div>

                        <div class="div-semaforo first-semaforo-llanta">
                                <div class="semaforo unvalidate" id="llan_ti"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Trasera izquierda</h4>
                                <input id="llan_tiprof" type="number" class="lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" />
                                <input id="llan_tipres" type="number" class="lbl-semaforo frm-llantas" placeholder="Presion PSI" />
                        </div>

                        <div class="div-semaforo first-semaforo-llanta">
                                <div class="semaforo unvalidate" id="llan_td"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Trasera derecha</h4>
                                <input id="llan_tdprof" type="number" class="lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" />
                                <input id="llan_tdpres" type="number" class="lbl-semaforo frm-llantas" placeholder="Presion PSI" />
                        </div>

                    <div class="div-semaforo first-semaforo-llanta">
                                <div class="semaforo unvalidate" id="llan_ref"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Llanta De Refaccion</h4>
                                <input id="llan_refprof" type="number" class="lbl-semaforo frm-llantas" placeholder="Profundidad (mm)" />
                                <input id="llan_refpres" type="number" class="lbl-semaforo frm-llantas" placeholder="Presion PSI" />
                        </div>

                    </div>

                    <div id="div-debaj" class="firstcol col-md-12">
                        <div class="fctitles col-md-12">
                            <h4 class="text-center">Medicion de frenos</h4>
                        </div>

                        <div class="div-semaforo first-semaforo-llanta">
                                <div class="semaforo unvalidate" id="fre_di"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas first-h4-llantas">Delantera izquierda</h4>
                                <input id="fre_dicero" type="number" class="lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" />
                                <input id="fre_diactual" type="number" class="lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" />
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="fre_dd"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Delantera derecha</h4>
                                <input id="fre_ddcero" type="number" class="lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" />
                                <input id="fre_ddactual" type="number" class="lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" />
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="fre_ti"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Trasera izquierda</h4>
                                <input id="fre_ticero" type="number" class="lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" />
                                <input id="fre_tiactual" type="number" class="lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" />
                        </div>
                        <div class="div-semaforo">
                                <div class="semaforo unvalidate" id="fre_td"><i class="fa fa-check-circle fa-3x circle1"></i><i class="fa fa-check-circle fa-3x circle2"></i><i class="fa fa-check-circle fa-3x circle3"></i></div>
                                <h4 class="text-center h4-llantas">Trasera derecha</h4>
                                <input id="fre_tdcero" type="number" class="lbl-semaforo frm-llantas frm-frenos" placeholder="Balata cero Km (mm)" />
                                <input id="fre_tdactual" type="number" class="lbl-semaforo frm-llantas" placeholder="Su balata actual (mm)" />
                        </div>


                    </div>
                </div>

                <!-- Aqui se termina todo  -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url();?>js/multipuntos/multi.js"></script>
            </body>
            </html>';


            $this->load->library('mpdf');
		    $this->mpdf->writeHTML($html);
            $new_name = 'temppdf.pdf';
            $this->mpdf->Output($new_name, 'I');

     }
}
