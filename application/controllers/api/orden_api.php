<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Orden_api extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ordendeserviciomodel');
		$this->load->helper('dates');
	}

	function servicios_por_vin_post()
	{
		header('Content-Type: application/json; charset=utf-8');
		header('Access-Control-Allow-Origin: *');
		$response = array();
		if(!empty($_POST["vin"]))
		{
			extract($_POST);
			$resultado = $this->Ordendeserviciomodel->servicios_por_vin($vin);
			$response["vin"] = $vin;

			foreach($resultado AS $r)
			{
				$r->fecha_completa = fecha_completa_larga($r->fecha)." ".$r->time;
				unset($r->fecha_time);
				$r->id_orden = (int)$r->id_orden;
			}
			$response["data"] = $resultado;
			$response["count"] = count($resultado);
			$response["status"] = "success";
			$response["message"] = "Registros encontrados";

			$response["nombre"] = $resultado[count($resultado)-1]->cliente;
			$response["auto"] = $resultado[count($resultado)-1]->auto;
		}
		else { $response["status"] = "error"; $response["message"] = "No se recibió vin"; }
		echo json_encode($response);
	}
}
