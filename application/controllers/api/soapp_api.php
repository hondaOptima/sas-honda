<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Order_api extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Citamodel','cita');
		$this->load->model('Ordendeserviciomodel','orden');
		$this->load->helper('dates');
	}

    function send_mail_servicio_asesor_post()
	{
		header('Content-Type: application/json; charset=utf-8');
		$response = array();
		if(!empty($_POST["vin"]) && !empty($_POST["id"]))
		{
			extract($_POST);
			$asesor = $this->cita->get_data_asesor_by_vin($vin);
			if($asesor != null && !empty($asesor->correo))//Preparar correo.//aqui
			{
				$orden_enviada = $this->orden->is_service_order_sent($id);
				if(!$orden_enviada)
				{
					$fecha_vacia = "0000-00-00";
					$asesor->nombre_cliente = ucwords(strtolower($asesor->nombre_cliente));
					$asesor->nombre = ucwords(strtolower($asesor->nombre));
					$asesor->auto = ucwords(strtolower($asesor->auto));
					$asesor->hoy = fecha_completa_larga(date("Y-m-d"));
					$asesor->date_time = date("Y-m-d H:i:s");

					$asesor->fecha_auto_entrega = ($asesor->fecha_auto_entrega != $fecha_vacia) ? fecha_completa_larga($asesor->fecha_auto_entrega) : "(Sin registrar)";//aqui

					$this->email->from('avisodeprivacidad@hondaoptima.com', 'Honda Optima');
					$this->email->to($asesor->correo);

					//$this->email->cc("lestrada@hondaoptima.com");

			        $this->email->subject('Honda Optima | Cliente en servicio');
					$this->email->set_mailtype("html");

					$html_msg = $this->load->view('plantillas/correos/notificar_asesor', $asesor, true);
			        $this->email->message($html_msg);

			        if ($this->email->send())
					{
						$datos = array("seo_is_email_advise_send" => 1);
						$this->orden->updateOrden($datos,$id);
						$response["success"] = "Se envió el correo a ".$asesor->correo." al asesor ".$asesor->nombre;
						$response["data"] = $asesor;
					}
					else $response["error"] = "No se logró enviar el correo";
				}
				else $response["success"] = "El correo ya ha sido enviado anteriormente";

			}
			else $response["No se encontró el correo"];
		}
		else $response["error"] = "No se recibieron los parámetros";
		echo json_encode($response);
	}
}
