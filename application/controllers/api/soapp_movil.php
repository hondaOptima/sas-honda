<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Soapp_movil extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('api/Acceso_movil_model');

	}


    function login_post()
	{  
		$user= $this->post('user');
		$pass= $this->post('pass');
		
		if(($user!=null) && ($pass!=null)) 
		{
		
          $res = $this->Acceso_movil_model->acceso_soapp($user,$pass);
          if ($res) {
          	$this->response($res);
          }else
          {
            $res= array('Status' => 0,
                       'Message'=> 'Error en servidor');
	       $this->response($res);

          }
		  
		}
		else
		{

	       $res= array('Status' => 0,
                       'Message'=> 'Faltan parámetros');
	       $this->response($res);
			
		}
		
	}

    function login_get()
	{  
		$user= $this->get('user');
		$pass= $this->get('pass');
	

		
		if(($user!=null) && ($pass!=null)) 
		{
		
          $res = $this->Acceso_movil_model->acceso_soapp($user,$pass);
		  $this->response($res);
		}
		else
		{

	       $res= array('Status' => 0,
                       'Message'=> 'Faltan parámetros');
	       $this->response($res);
			
		}
	}
}