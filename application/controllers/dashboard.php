<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        //$this->output->cache(60);
		$this->load->model('Dashboardmodel');
		$this->load->model('Reportesmodel');
		$this->load->model('Permisomodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Productividadmodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');

		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
		 ini_set('memory_limit', '1024M');
	}

	public function index()
	{
		if(empty($_GET['year']))
		{
			$year=date('Y');
		}
		else
		{
			$year=$_GET['year'];
		}

		if(empty($_GET['mes']))
		{
			$mes=date('m');
		}
		else
		{
			$mes=$_GET['mes'];
		}

		if(empty($_GET['taller']))
		{
			$taller=$_SESSION['sfidws'];
		}else
		{
			$taller=$_GET['taller'];
		}

		if(empty($_GET['date']))
		{
			if(date('H')<=7) $date=date('Y-m-d'); else $date=date('Y-m-d');
		}
		else
		{
			$date=$_GET['date'];
		}

		if(empty($_GET['dia']))
		{
			$dia=date('d');
		}
		else
		{
			$dia=$_GET['dia'];
		}

		$mesi=$year.'-'.date('m').'-'.'01';
		$mesf=$year.'-'.date('m').'-31';
		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['mes']=$mes;
		$data['anio']=$year;
		$data['dia']=$dia;
		$data['dias']=cal_days_in_month(CAL_GREGORIAN, $mes, date('Y'));
		$data['meses']=$this->Dashboardmodel->meses();
		$data['ppto']=$this->Dashboardmodel->pptomes($mes,$dia);
		$data['pptoTotal']=$this->Dashboardmodel->pptomesTotal($mes);
		$data['pptoa']=$this->Dashboardmodel->pptomesA($mes);
		$data['das']=$this->Dashboardmodel->desgloce_dashboard($date);
		//print_r($data['das']);
		$data['desgloce']=$this->Dashboardmodel->desgloce();
		//$data['desglocem']=$this->Dashboardmodel->desglocemes($mes,$dia);
		$data['desglocea']=$this->Dashboardmodel->desgloceAcumulado($mes);

		//CITAS
		$data['citasm']=$this->Reportesmodel->getCitas('2',$mesi,$mesf);
		$data['citast']=$this->Reportesmodel->getCitas('3',$mesi,$mesf);
		$data['citase']=$this->Reportesmodel->getCitas('1',$mesi,$mesf);
		//Obtenemos la cantidad de días que tiene septiembre del 2008
	    $data['lista']=$this->Dashboardmodel->getMonthDays($mes, $year);
		$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
		//meta
		$data['hos']=3.5;

		 //Utilizacion del taller.
		$data['capacidad'] = $this->Productividadmodel->capacidad($taller);
		 //Utilizacion en horas
		$data['ectaller']=$this->Productividadmodel->capacidadtaller($date,1,$data['capacidad']);
	    $data['mctaller']=$this->Productividadmodel->capacidadtaller($date,2,$data['capacidad']);
	    $data['tctaller']=$this->Productividadmodel->capacidadtaller($date,3,$data['capacidad']);

		// data['os'] = $this->Productividadmodel->obtenerOS($taller,$start,$end,$ase);
	    // data['cos'] = $this->Productividadmodel->getLista($taller,$start,$end,$ase);
	    // data['ossv'] = $this->Productividadmodel->obtenerOSsv($taller,$start,$end,$ase);

		// real de utilizacion por dia
		// $data['ruens']=$this->Productividadmodel->utilizacion($date,1);
		// $data['rutij']=$this->Productividadmodel->utilizacion($date,3);
	    $omex=0;
		$somex=0;
		$dcerom=0;
		$otij=0;
		$sotij=0;
		$dcerot=0;
		$oens=0;
		$soens=0;
		$dceroe=0;
		$dtraTij=0;
		$dtraMex=0;
		$dtraEns=0;

		for($i=1; $i<=date('d'); $i++)
		{
			if($i<10) $d='0'.$i; else $d=$i;
			$oens=$this->Productividadmodel->utilizacion(date('Y-m').'-'.$d,1);
			$omex=$this->Productividadmodel->utilizacion(date('Y-m').'-'.$d,2);
			$otij=$this->Productividadmodel->utilizacion(date('Y-m').'-'.$d,3);

			$soens+=$oens;
			$somex+=$omex;
			$sotij+=$otij;
		}

		$dtraMex= date('d') - $dcerom;
		$dtraTij= date('d') - $dcerot;
		$dtraEns= date('d') - $dceroe;

		$data['uMex']=$somex / $dtraMex;
		$data['uTij']=$sotij / $dtraTij;
		$data['uEns']=$soens / $dtraEns;

		$data['tjPend'] = $this->Productividadmodel->pendientesDia('Tijuana',3,'nodate','nodate');
		$data['mxPend'] = $this->Productividadmodel->pendientesDia('Mexicali',2,'nodate','nodate');
		$data['enPend'] = $this->Productividadmodel->pendientesDia('Ensenada',1,'nodate','nodate');

		$data['campanias']=$this->Productividadmodel->campanias();
		$data['campaniasDia'] = $this->Productividadmodel->campaniasDia();
		$data['campaniasABT'] = $this->Productividadmodel->campaniasABT();
		$data['pendientesABT'] = 436-$data['campaniasABT'][0]->realizadas;

		$data['campaniasABM'] = $this->Productividadmodel->campaniasABM();
		$data['pendientesABM'] = 493-$data['campaniasABM'][0]->realizadas;

		$data['campaniasABE'] = $this->Productividadmodel->campaniasABE();
		$data['pendientesABE'] = 144-$data['campaniasABE'][0]->realizadas;
		//app_idAppointment,app_day
	    $this->load->view('dashboard/vista',$data);
	}

	public function vic()
	{
		if(empty($_GET['year'])) $year=date('Y');
		else $year = $_GET['year'];

		if(empty($_GET['mes'])) $mes = date('m');
		else $mes = $_GET['mes'];

		if(empty($_GET['taller'])) $taller = $_SESSION['sfidws'];
		else $taller=$_GET['taller'];

		if(empty($_GET['date']))
			if(date('H') <= 7) $date = date('Y-m-d'); else $date = date('Y-m-d');
		else $date = $_GET['date'];

		if(empty($_GET['dia'])) $dia = date('d');
		else $dia = $_GET['dia'];

		$mesi = $year.'-'.date('m').'-'.'01';
		$mesf = $year.'-'.date('m').'-31';
		$data['fecha'] = $date;
		$data['taller'] = $taller;
		$data['mes'] = $mes;
		$data['anio'] = $year;
		$data['dia'] = $dia;
		$data['dias'] = cal_days_in_month(CAL_GREGORIAN, $mes, date('Y'));
		$data['meses'] = $this->Dashboardmodel->meses();
		$data['ppto'] = $this->Dashboardmodel->pptomes($mes,$dia);
		$data['pptoTotal'] = $this->Dashboardmodel->pptomesTotal($mes);
		$data['pptoa'] = $this->Dashboardmodel->pptomesA($mes);
		$data['das'] = $this->Dashboardmodel->desgloce_dashboard($date);

		$data['desgloce'] = $this->Dashboardmodel->desgloce();

		$data['desglocea'] = $this->Dashboardmodel->desgloceAcumulado($mes);

		//CITAS
		$data['citasm'] = $this->Reportesmodel->getCitas('2',$mesi,$mesf);
		$data['citast'] = $this->Reportesmodel->getCitas('3',$mesi,$mesf);
		$data['citase'] = $this->Reportesmodel->getCitas('1',$mesi,$mesf);
		//Obtenemos la cantidad de días que tiene septiembre del 2008
	    $data['lista'] = $this->Dashboardmodel->getMonthDays($mes, $year);
		$data['usuario'] = $this->Usuariomodel->listaUsuarios(0);
		//meta
		$data['hos'] = 3.5;

		 //Utilizacion del taller.
		$data['capacidad'] = $this->Productividadmodel->capacidad($taller);
		 //Utilizacion en horas
		$data['ectaller'] = $this->Productividadmodel->capacidadtaller($date,1,$data['capacidad']);
	    $data['mctaller'] = $this->Productividadmodel->capacidadtaller($date,2,$data['capacidad']);
	    $data['tctaller'] = $this->Productividadmodel->capacidadtaller($date,3,$data['capacidad']);

	    $omex = 0;
		$somex = 0;
		$dcerom = 0;
		$otij = 0;
		$sotij = 0;
		$dcerot = 0;
		$oens = 0;
		$soens = 0;
		$dceroe = 0;
		$dtraTij = 0;
		$dtraMex = 0;
		$dtraEns = 0;

		for($i = 1; $i <= date('d'); $i++)
		{
			if($i<10) $d='0'.$i; else $d=$i;
			$oens = $this->Productividadmodel->utilizacion(date('Y-m').'-'.$d,1);
			$omex = $this->Productividadmodel->utilizacion(date('Y-m').'-'.$d,2);
			$otij = $this->Productividadmodel->utilizacion(date('Y-m').'-'.$d,3);

			$soens += $oens;
			$somex += $omex;
			$sotij += $otij;
		}

		$dtraMex = date('d') - $dcerom;
		$dtraTij = date('d') - $dcerot;
		$dtraEns = date('d') - $dceroe;

		$data['uMex'] = $somex / $dtraMex;
		$data['uTij'] = $sotij / $dtraTij;
		$data['uEns'] = $soens / $dtraEns;

		$data['tjPend'] = $this->Productividadmodel->pendientesDia('Tijuana',3,'nodate','nodate');
		$data['mxPend'] = $this->Productividadmodel->pendientesDia('Mexicali',2,'nodate','nodate');
		$data['enPend'] = $this->Productividadmodel->pendientesDia('Ensenada',1,'nodate','nodate');

		$data['campanias'] = $this->Productividadmodel->campanias();
		$data['campaniasDia'] = $this->Productividadmodel->campaniasDia();
		$data['campaniasABT'] = $this->Productividadmodel->campaniasABT();
		$data['pendientesABT'] = 436-$data['campaniasABT'][0]->realizadas;

		$data['campaniasABM'] = $this->Productividadmodel->campaniasABM();
		$data['pendientesABM'] = 493-$data['campaniasABM'][0]->realizadas;

		$data['campaniasABE'] = $this->Productividadmodel->campaniasABE();
		$data['pendientesABE'] = 144-$data['campaniasABE'][0]->realizadas;

		print_r($data["campaniasABE"]);
	}

	function getCPWDate(){
		$cpendientes = $this->Productividadmodel->pendientesDia($_GET['city'],$_GET['cityNum'],$_GET['date1'],$_GET['date2']);
		echo json_encode($cpendientes);
	}

	function takataDetail(){
		$city = $_GET['city'];
		$data['realizadas'] = $this->Productividadmodel->takataRealizadas($city);
		$data['pendientes'] = $this->Productividadmodel->takataPendientes($city);
		$data['ciudadNum'] = $city;

		$data['realizadasNum'] = count($data['realizadas']);
		$data['pendientesNum'] = count($data['pendientes']);
		$data['realizadasPor'] = ($data['realizadasNum'] * 100) / ($data['realizadasNum'] + $data['pendientesNum']);
		$data['pendientesPor'] = 100 - $data['realizadasPor'];
		$data['totalNum'] = $data['realizadasNum'] + $data['pendientesNum'];

		$this->load->view('dashboard/takata/detail.php', $data);
	}


	function detalle()
	{
		if(empty($_GET['year'])){
				$year=date('Y');}
			else{
				$year=$_GET['year'];
				}

		if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}
			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
			if(empty($_GET['date'])){$date=date('Y-m-d');}else{$date=$_GET['date'];}

			if(empty($_GET['dia'])){
			$dia=date('d');
			}else{

				$dia=$_GET['dia'];
				}

	$mesi=$year.'-'.$mes.'-'.'01';
	$mesf=$year.'-'.$mes.'-31';
	$data['fecha']=$date;
	$data['taller']=$taller;
	$data['mes']=$mes;
	$data['anio']=$year;
	$data['dia']=$dia;
	$data['dias']=cal_days_in_month(CAL_GREGORIAN, $mes, date('Y'));
	$data['meses']=$this->Dashboardmodel->meses();

	$data['desglocem']=$this->Dashboardmodel->desglocemes($mes,date('d'));
    $data['lista']=$this->Dashboardmodel->getMonthDays($mes, $year);
	$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
	$data['pmes']=641977.37;
	$data['diastra']=23;
	$data['hos']=3.5;


	$this->load->view('dashboard/detalle',$data);
	}










	function citasPorConfirmar(){
		echo 100;
		}


	function ppto()
	{
	if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}

	if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}
		else{
			$taller=$_GET['taller'];
			}

	if(empty($_GET['anio'])){
			$anio=date('Y');
			}
		else{
			$anio=$_GET['anio'];
			}
	$data['anio']=$anio;
	$data['taller']=$taller;
	$data['mes']=$mes;
	$data['desgloce']=$this->Dashboardmodel->desglocemes($mes,date('d'));
	$data['meses']=$this->Dashboardmodel->meses();
	$data['usuario']=$this->Usuariomodel->listaUsuarios(0);
	$data['nomina']=$this->Dashboardmodel->nomina($taller,$anio,$mes);
	$data['ppto']=$this->Dashboardmodel->ppto($taller,$anio);
	$data['dias_tra']=$this->Dashboardmodel->dias_tra($taller,$anio,$mes);

	$this->load->view('dashboard/ppto/vista',$data);

	}

	function editppto()
	{
		$this->form_validation->set_rules('idpp', 'IDPPTO', '');

        if ($this->form_validation->run())
        {
			$ppto = array(
						'ppt_01'=>$this->input->post('ppt_a'),
						'ppt_02'=>$this->input->post('ppt_b'),
						'ppt_03'=>$this->input->post('ppt_c'),
						'ppt_04'=>$this->input->post('ppt_d'),
						'ppt_05'=>$this->input->post('ppt_e'),
						'ppt_06'=>$this->input->post('ppt_f'),
						'ppt_07'=>$this->input->post('ppt_g'),
						'ppt_08'=>$this->input->post('ppt_h'),
						'ppt_09'=>$this->input->post('ppt_i'),
						'ppt_10'=>$this->input->post('ppt_j'),
						'ppt_11'=>$this->input->post('ppt_k'),
						'ppt_12'=>$this->input->post('ppt_l'),
						);
					$this->Dashboardmodel->updateppto($ppto,$this->input->post('idpp'));
					redirect('dashboard/ppto/');
		}
		else{
	$data['ppto']=$this->Dashboardmodel->getPpto($_GET['cd']);
	$this->load->view('dashboard/ppto/editppto',$data);
		}
	}

	function editarMonto()
	{
	$this->Dashboardmodel->editarMonto($_GET['id'],$_GET['campo']);
	}

	function dias_tra()
	{
		$this->form_validation->set_rules('dias', 'Dias', '');

        if ($this->form_validation->run())
        {
			$info = array(
						'dit_dias'=>$this->input->post('dias'),
						'dit_sabados'=>$this->input->post('sabados'),
						);
					$this->Dashboardmodel->updateDiasTra($info,$this->input->post('iddt'));

			redirect('dashboard/ppto');
		}
		else{
	$data['info']=$this->Dashboardmodel->getDiasTra($_GET['iddt']);
	$this->load->view('dashboard/ppto/edit_dias_tra',$data);

	}}

	function editarComentario() {
		$json['status'] = 0;

		$campania = $this->input->get("id");
		$comentario = $this->input->get("comentario");

		if($campania != '' and $comentario != '')
		{
			if($this->Dashboardmodel->setComentario($campania, $comentario))
			{
				$json['message'] = "Comentario actualizado satisfactoriamente";
			}
			else
			{
				$json['status'] = 2;
				$json['message'] = "Ocurrio un error al actualizar el comentario";
			}
		}
		else
		{
			$json['status'] = 1;
			$json['message'] = "Los datos enviados son invaldos";
		}

		$this->output->set_output(json_encode($json));
	}

/*	public function datad()
	{

	$clientes=$this->Dashboardmodel->clientesTijuana();


	foreach($clientes as $cli){
		$info=array(
		'cus_name'=>$cli->nombre,
		'cus_email'=>$cli->email,
		'cus_telephone'=>$cli->tel,
		'cus_celular'=>$cli->celular,
		'cus_calle'=>$cli->calle,
		'cus_num'=>$cli->num,
		'cus_colonia'=>$cli->colonia,
		'cus_cp'=>$cli->cp,
		'cus_ciudad'=>$cli->ciudad,
		'cus_estado'=>$cli->estado,
		'cus_rfc'=>$cli->rfc,
		'cus_marca'=>$cli->marca,
		'cus_submarca'=>$cli->submarca,
		'cus_version'=>$cli->version,
		'cus_color'=>$cli->color,
		'cus_modelo'=>$cli->modelo,
		'cus_serie'=>$cli->serie,
		'cus_capacidad'=>$cli->capacidad,
		'cus_km_recepcion'=>$cli->kms,
		'cus_placas'=>$cli->placas,
		'cus_ultimo_servicio'=>$cli->ultimafecha,
		'cus_orden'=>$cli->orden,
		'cus_fecha_venta'=>$cli->fvta,
		'cus_cd'=>'TJ',
		);
		$this->Dashboardmodel->insertClientes($info);
		}
	}*/


}
