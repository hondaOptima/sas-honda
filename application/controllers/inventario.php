<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventario extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Permisomodel');
		$this->load->model('Inventariomodel','inventario');
		$this->load->helper('url');
		$this->load->library('session');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	}

	function get_autos_piso()
    {
        header('Content-Type: application/json; charset=utf-8');

		$autos = $this->inventario->get_autos_piso();
		$data = array();
		foreach($autos AS $r)
		{
			$a = array();
			$a[] = "D";
			$a[] = $r->fecha;
			$a[] = $r->vin;
			$a[] = $r->modelo." ".$r->color;
			$a[] = $r->status;
			$a[] = $r->torre;
			$a[] = $r->ubicacion;
			$a[] = "";
			$data["data"][] = $a;
		}
        echo json_encode($data);
    }

	function get_traslado()
	{
		header('Content-Type: application/json; charset=utf-8');
		if(!empty($_GET["vin"]))
		{
			$traslado = $this->inventario->get_traslado();
			echo json_encode($traslado);
		}
		else echo '{"error" : "No se recibió el vin del auto."}';
	}

	function get_auto_piso()
	{
		header('Content-Type: application/json; charset=utf-8');
		if(!empty($_GET["vin"]) || !empty($_GET["id"]))
		{
			$auto_piso = $this->inventario->get_auto_piso();
			echo json_encode($auto_piso);
		}
		else echo '{"error" : "No se recibió el vin o el id del auto en piso."}';
	}
}
