<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reportesmodel');
		$this->load->model('Dashboardmodel');
		$this->load->model('Permisomodel');
		$this->load->model('Productividadmodel');
		$this->load->model('Multipuntosmodel');
		$this->load->helper('url');
		$this->load->helper('dates');
		$this->load->library('email');
		$this->load->library('upload');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('curl');
		$this->load->library('pdf');
		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
	}

	public function index()
	{
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}

		if(empty($_GET['from'])){
			$from=date('m').'/01'.'/'.date('Y');
			}else{
				$from=$_GET['from'];


				}

				if(empty($_GET['to'])){
			$to=date('m').'/31'.'/'.date('Y');
			}else{
				$to=$_GET['to'];


				}

		$data['info']=0;
		$data['taller']=$taller;
		$data['from']=$from;
		$data['to']=$to;
		$data['ppto']=$this->Dashboardmodel->ppto();
	$this->load->view('reportes/vista',$data);
	}


	public function ordenes()
	{
		 $this->form_validation->set_rules('ciudad', 'Ciudad', '');
	if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
				if(empty($_GET['from'])){
			$from=date('m').'/01'.'/'.date('Y');
			}else{
				$from=$_GET['from'];


				}

				if(empty($_GET['to'])){
			$to=date('m').'/31'.'/'.date('Y');
			}else{
				$to=$_GET['to'];


				}


			  if ($this->form_validation->run())
        {



			$files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {

       $tmp_new_name = $files["userfile"]["name"][$i];
		 	$path_parts = pathinfo($tmp_new_name);
			$new_name =time() .rand().'.'. $path_parts['extension'];

        $_FILES['userfile']['name']= $new_name;
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];
        $this->upload->initialize($this->set_upload_options());
       $this->upload->do_upload();
       // insert database.
	   $nombre[]=$new_name;
    }



			$data= array(
						'cin_ID'=>'NULL',
						'cin_ciudad'=>$this->input->post('ciudad'),
						'cin_file'=>$nombre[0],
						'cin_date'=>date('Y-m-d'),
						'cin_status'=>'on',
						'cin_nombre'=>$this->input->post('nombredel'),
						);
			$this->Reportesmodel->insertCsvIntelisis($data);

			redirect('reportes/ordenes/');
		}
		else{

		$data['archivos']=$this->Reportesmodel->getCsvIntelisis($taller);
		$data['taller']=$taller;
		$data['from']=$from;
		$data['to']=$to;
	$this->load->view('reportes/ordenes',$data);
		}
	}



	function prueba()
	{
		$data['info']=$this->Reportesmodel->getianalisis($_GET['file']);
		// print_r($data["info"]);//aqui
		$this->load->view('reportes/prueba',$data);
	}

	function save_analisis(){
	$data['info']=$this->Reportesmodel->getianalisis($_GET['file']);
	$this->load->view('reportes/prueba',$data);
		}


	function saveVentaPerdida(){

	if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}
			if(empty($_GET['mes'])){
			$mes=date('m');
			}
		else{
			$mes=$_GET['mes'];
			}

			if(empty($_GET['anio'])){
			$anio=date('Y');
			}
		else{
			$anio=$_GET['anio'];
			}

				$data['taller']=$taller;
				$data['mes']=$mes;
				$data['anio']=$anio;

		$this->form_validation->set_rules('ciudad', 'Ciudad', '');
		 if ($this->form_validation->run())
        {
			$data= array(
						'hit_IDhits'=>'NULL',
						'hit_mes'=>date('m'),
						'hit_date'=>date('m').date('Y'),
						'hit_taller'=>$taller,
						'hit_noparte'=>$this->input->post('parte'),
						'hit_piezas'=>$this->input->post('pieza'),
						'hit_costo'=>$this->input->post('unitario'),
						'hit_almacenable'=>0,
						'hit_hora'=>date('Y-m-d H:i:s'),
						);
			$this->Reportesmodel->insertHits($data);

			redirect('reportes/saveVentaPerdida/');


		}


			$data['meses']=$this->Dashboardmodel->meses();
			$data['info']=$this->Reportesmodel->getVentaPerdidaMes($taller,$mes,$anio);
			$this->load->view('reportes/saveVentaPerdida',$data);
		}

	function ventaPerdida(){
	if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}

				$data['taller']=$taller;
	$data['info']=$this->Reportesmodel->getVentaPerdida($taller);
	$this->load->view('reportes/ventaPerdida',$data);
		}

		function semaforo()
		{
$fecha=date('Y').'-'.$_GET['mes'].'';
$f_rojo= date('Y-m',strtotime('-13 months', strtotime($fecha)));
$f_ama= date('Y-m',strtotime('-12 months', strtotime($fecha)));
$f_ver= date('Y-m',strtotime('-6 months', strtotime($fecha)));

			$cd=$_GET['cd'];
			$file=$_GET['file'];
			$mes=$_GET['mes'];
			$anio_a=(date('Y')-1);
			$anio_n=date('Y');

			if($cd=='Mexicali'){$cd='mex';}
if($cd=='Tijuana'){$cd='tij';}
if($cd=='Ensenada'){$cd='ens';}



	$data['inv']=$this->Reportesmodel->getSemaforo($cd,$file,'morado','0','0');
	$data['rojo']=$this->Reportesmodel->getSemaforo($cd,$file,'rojo',''.$f_rojo.'-31','0');
	$data['ama']=$this->Reportesmodel->getSemaforo($cd,$file,'ama',''.$f_ama.'-01',''.$f_ver.'-01');
	$data['ver']=$this->Reportesmodel->getSemaforo($cd,$file,'ver',''.$f_ver.'-01','0');
	$data['mes']=$mes;
	$data['cd']=$cd;

	$this->load->view('reportes/semaforo',$data);
		}

		function detalleSemaforo(){
			$cd=$_GET['cd'];
			$file=$_GET['file'];
			$tp=$_GET['tipo'];

			if($cd=='Mexicali'){$cd='mex';}
if($cd=='Tijuana'){$cd='tij';}
if($cd=='Ensenada'){$cd='ens';}

	$fecha=date('Y').'-'.$_GET['mes'].'';
$f_rojo= date('Y-m',strtotime('-13 months', strtotime($fecha)));
$f_ama= date('Y-m',strtotime('-12 months', strtotime($fecha)));
$f_ver= date('Y-m',strtotime('-6 months', strtotime($fecha)));


	$data['inv']=$this->Reportesmodel->getSemaforo($cd,$file,'morado','0','0');
	$data['rojo']=$this->Reportesmodel->getSemaforo($cd,$file,'rojo',''.$f_rojo.'-31','0');
	$data['ama']=$this->Reportesmodel->getSemaforo($cd,$file,'ama',''.$f_ama.'-01',''.$f_ver.'-01');
	$data['ver']=$this->Reportesmodel->getSemaforo($cd,$file,'ver',''.$f_ver.'-01','0');
	$this->load->view('reportes/detalleSemaforo',$data);
		}

	function eliminarHit($id){
	$this->Reportesmodel->deleteHit($id);

	 redirect('reportes/saveVentaPerdida');
		}

		function deletefile(){
	$this->Reportesmodel->deleteFile($_GET['file']);
	 unlink('./intelisis/'.$_GET['file'].'');
	 redirect('reportes/ordenes');
		}

		function eliminarFile(){
	$this->Reportesmodel->eliminarFile($_GET['file']);
	 unlink('./profit/'.$_GET['file'].'');
	 redirect('reportes/profit');
		}

		function citas(){
			if(empty($_GET['year'])){
				$year=date('Y');}
			else{
				$year=$_GET['year'];
				}
				if(empty($_GET['mes'])){
			$mes=date('m');
			}else{
				$mes=$_GET['mes'];


				}
			if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}

	$mesi=$year.'-'.$mes.'-'.'01';
	$mesf=$year.'-'.$mes.'-31';

	$data['year']=$year;
	$data['mes']=$mes;
	$data['ndias']=cal_days_in_month(CAL_GREGORIAN, $mes, $year);
	$data['taller']=$taller;
	$data['citas']=$this->Reportesmodel->getCitas($taller,$mesi,$mesf);
	$data['citasy']=$this->Reportesmodel->getCitas($taller,'2015-01-01','2015-12-31');
	$data['citassin']=$this->Reportesmodel->getCitasSin($taller,$mesi,$mesf);
	$data['citassiny']=$this->Reportesmodel->getCitasSin($taller,'2015-01-01','2015-12-31');
	$data['info']=1;
	$this->load->view('reportes/citas',$data);
		}




			function profit()
	    {
		$data['flash_message'] = $this->session->flashdata('message');

	if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}

				$this->form_validation->set_rules('ciudad', 'Ciudad', '');
			  if ($this->form_validation->run())
        {


	$nombre=array();
	$files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {

       $tmp_new_name = $files["userfile"]["name"][$i];
	   $path_parts = pathinfo($tmp_new_name);
	   $new_name =time() .rand().'.'. $path_parts['extension'];

        $_FILES['userfile']['name']= $new_name;
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];
        $this->upload->initialize($this->set_upload_options());
       $this->upload->do_upload();
       // insert database.
	   $nombre[]=$new_name;
    }

	$data= array(
						'iec_ID'=>'NULL',
						'iec_desc'=>$this->input->post('desc'),
						'iec_fecha'=>date('Y-m-d'),
						'iec_file_semaforo'=>$nombre[0],
						'iec_file_inv_cec'=>$nombre[1],
						'iec_cd'=>$this->input->post('cd'),
						'iec_mes'=>$this->input->post('mes'),
						'iec_procesado'=>0
						);
 $this->Reportesmodel->insertInvExiCero($data);
$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Los archivos fueron subidos con Exito!.</div>');
 redirect('reportes/profit/');


		}
		else{


		$data['taller']=$taller;

		$data['meses']=$this->Dashboardmodel->meses();
		$data['file']=$this->Reportesmodel->InvExiCero();
	$this->load->view('reportes/profit_form',$data);
		}
	}



	function deleteArchivosProfit($taller,$mes,$id){

			$this->Reportesmodel->deleteProfitArchivos($taller,$mes,$id);
			redirect('reportes/profit/');
			}

	function profit_print(){
			$respuesta=$this->Reportesmodel->getProfitPrint($_GET['id']);

			if(empty($respuesta[0]->pro_ID)){
			//traemos info del iec tabla
			$iec=$this->Reportesmodel->InvExiCeroID($_GET['id']);
			//insert nuevo profit
			$info=array(
			'pro_cd'=>$iec[0]->iec_cd,
			'pro_mes'=>date('Y-m-d'),
			'pro_ID_iec'=>$_GET['id'],
			'pro_inv_va'=>0,
			'pro_v_aceite'=>0,
			'pro_v_partes'=>0,
			'pro_u_partes'=>0,
			'pro_v_acc'=>0,
			'pro_u_acc'=>0,
			'pro_ub'=>0,
			'pro_gastos_a'=>0,
			'pro_uo'=>0,
			'pro_gastos_v'=>0,
			'pro_ped_parte'=>0,
			'pro_ped_pieza'=>0,
			'pro_ped_valor'=>0,
			'pro_com_parte'=>0,
			'pro_com_pieza'=>0,
			'pro_com_valor'=>0
			);

			$this->Reportesmodel->insertProfit($info);

				}


			$this->form_validation->set_rules('pro_inv_va', 'Inv. Valor Actual', '');
		 if ($this->form_validation->run())
        {
			$data= array(
			'pro_inv_va'=>$this->input->post('pro_inv_va'),
			'pro_v_aceite'=>$this->input->post('pro_v_aceite'),
			'pro_v_partes'=>$this->input->post('pro_v_partes'),
			'pro_u_partes'=>$this->input->post('pro_u_partes'),
			'pro_v_acc'=>$this->input->post('pro_v_acc'),
			'pro_u_acc'=>$this->input->post('pro_u_acc'),
			'pro_ub'=>$this->input->post('pro_ub'),
			'pro_gastos_a'=>$this->input->post('pro_gastos_a'),
			'pro_uo'=>$this->input->post('pro_uo'),
			'pro_gastos_v'=>$this->input->post('pro_gastos_v'),
			'pro_ped_parte'=>$this->input->post('pro_ped_parte'),
			'pro_ped_pieza'=>$this->input->post('pro_ped_pieza'),
			'pro_ped_valor'=>$this->input->post('pro_ped_valor'),
			'pro_com_parte'=>$this->input->post('pro_com_parte'),
			'pro_com_pieza'=>$this->input->post('pro_com_pieza'),
			'pro_com_valor'=>$this->input->post('pro_com_valor'),
						);
			$this->Reportesmodel->updateProfit($data,$_GET['id']);

			redirect('reportes/profit_print/?id='.$_GET['id'].'');


		}



				$iec=$this->Reportesmodel->InvExiCeroID($_GET['id']);
			$data['id']=$_GET['id'];
			$data['profit']=$this->Reportesmodel->getProfitPrint($_GET['id']);
			$data['resumen']=$this->Reportesmodel->getResumenCd($iec[0]->iec_cd,$iec[0]->iec_fecha);

			$this->load->view('reportes/profit_print',$data);
			}

	function profit_view(){

		$respuesta=$this->Reportesmodel->getProfitPrint($_GET['id']);

			if(empty($respuesta[0]->pro_ID)){
			//traemos info del iec tabla
			$iec=$this->Reportesmodel->InvExiCeroID($_GET['id']);
			//insert nuevo profit
			$info=array(
			'pro_cd'=>$iec[0]->iec_cd,
			'pro_mes'=>date('Y-m-d'),
			'pro_ID_iec'=>$_GET['id'],
			'pro_inv_va'=>0,
			'pro_v_aceite'=>0,
			'pro_v_partes'=>0,
			'pro_u_partes'=>0,
			'pro_v_acc'=>0,
			'pro_u_acc'=>0,
			'pro_ub'=>0,
			'pro_gastos_a'=>0,
			'pro_uo'=>0,
			'pro_gastos_v'=>0,
			'pro_ped_parte'=>0,
			'pro_ped_pieza'=>0,
			'pro_ped_valor'=>0,
			'pro_com_parte'=>0,
			'pro_com_pieza'=>0,
			'pro_com_valor'=>0
			);

			$this->Reportesmodel->insertProfit($info);

				}
			$data['profit']=$this->Reportesmodel->getProfitPrint($_GET['id']);
			$this->load->view('reportes/profit_view',$data);
			}

		function sin_exi_cero(){
if($_GET['cd']=='Mexicali'){$cd='mex';}
if($_GET['cd']=='Tijuana'){$cd='tij';}
if($_GET['cd']=='Ensenada'){$cd='ens';}

if($_GET['cd']=='mex'){$cd='mex';}
if($_GET['cd']=='tij'){$cd='tij';}
if($_GET['cd']=='ens'){$cd='ens';}



			$data['info']=$this->Reportesmodel->getSinCero($cd,$_GET['file']);
			$data['cd']=$cd;
			$this->load->view('reportes/sinCero',$data);
			}

			function no_parte(){
			$data['info']=$this->Reportesmodel->getNoPartes(2);
			$this->load->view('reportes/no_parte',$data);
			}

			function no_parte_mes(){

			if($_GET['cd']=='Mexicali'){$cd='mex'; $icd=2;}
			if($_GET['cd']=='Tijuana'){$cd='tij'; $icd=3;}
			if($_GET['cd']=='Ensenada'){$cd='ens';$icd=1;}
			$data['cd']=$cd;
			$data['mes']=date('Y-m-d');

			//$mesp=(date('n')-1);
			$mesp='02';
			$data['info']=$this->Reportesmodel->getNoPartesMes($icd,$mesp);
			if(!empty($_GET['id'])) $this->Reportesmodel->updateStatus($_GET['id']);
			$this->load->view('reportes/no_parte_mes',$data);
			}

			private function set_upload_options()
{
//  upload an image options
    $config = array();
    $config['upload_path'] = './profit/';
    $config['allowed_types'] = 'csv';
    $config['max_size']      = '50000';
    $config['overwrite']     = FALSE;


    return $config;
}

	function auditoriacitas(){

		if(empty($_GET['taller'])){	$taller=$_SESSION['sfidws'];}
		else{$taller=$_GET['taller'];}

		if(empty($_GET['mes'])){$mes=date('m');	}
		else{$mes=$_GET['mes'];	}

		$data['taller']=$taller;
		$data['mes']=$mes;

		$this->form_validation->set_rules('asesor', 'asesor', '');
		 if ($this->form_validation->run())
        {
			$data['fechai'] = $_POST['fechai'];
			$data['fechaf'] = $_POST['fechaf'];
			$data['idSucursal'] = $_POST['sucursal'];
			$data['citas']=$this->Reportesmodel->getHistorialCitas($data['idSucursal'],$data['fechai'],$data['fechaf']);
		}
			$data['sucursales']=$this->Reportesmodel->getSucursales();
			$this->load->view('reportes/auditoriacitas',$data);
		}


		function inventario()
		{
				$this->load->view('reportes/inventario/lista');
		}

		function lista_inventario(){
				$this->load->view('reportes/inventario/lista_inventario');
				}




		function calendario_inventario()
		{

				$this->load->view('reportes/inventario/calendario_inventario');
		}
    function autos_recibidos()
				{
					if(empty($_GET['taller'])) $t=''; else
					{
					if($_GET['taller']==1){$t="Ensenada";}
					if($_GET['taller']==2){$t="Mexicali";}
					if($_GET['taller']==3){$t="Tijuana";}

					}
					echo $data['taller']=$t;
					$this->load->view('reportes/inventario/autos_recibidos',$data);
				}

    function autos_proceso_venta(){

				$this->load->view('reportes/inventario/autos_proceso_venta');
				}

    function horario_entregas()
	{
		$this->load->view('reportes/inventario/horario_entregas');
	}
    function torre(){
		for($i=1; $i<101; $i++)
		{
		$data['sucursales']=$this->Reportesmodel->torres($i);
		}
		}

	function statustorres()
	   {
		$data['agencia']=$this->sfworkshop;
		$data['array']=$this->Reportesmodel->statustorres($this->sfworkshop);
		$this->load->view('reportes/inventario/torres_view',$data);
	   }

	function traslados()
	{
		$this->load->view('reportes/inventario/traslados');
	}

	function crear_hoja_traslado($idt)
	  {
	  $data['idt']=$idt;
	  $data['sucursales']=$this->Reportesmodel->getSucursales();
	  $this->load->view('reportes/inventario/crear_hoja_traslado',$data);
	  }

	  function recibir_hoja_traslado($idt)
	  {
	  $data['idt']=$idt;
	  $data['sucursales']=$this->Reportesmodel->getSucursales();
	  $this->load->view('reportes/inventario/recibir_hoja_traslado',$data);
	  }

	  function pdf_hoja_traslado($idt)
	  {
	  $data['idt']=$idt;
	  $data['sucursales']=$this->Reportesmodel->getSucursales();
	  $this->load->view('reportes/inventario/crear_hoja_traslado',$data);
	  }

	  function imprimir($vin)
	  {
	  $this->load->view('reportes/imprimir/vista');
	  }


	function getStatus($status){
        $statusArray;
        switch ($status) {
            case 0:
                $statusArray['color'] = 0;

                break;
            case 1:
                $statusArray['color'] = 1;

                break;
            case 2:
                $statusArray['color'] = 2;

                break;
            default:
                $statusArray['color'] = 3;
                break;
        }
        $object = (object)$statusArray;
        return $object;
    }

		function pdf_multipuntos()
		{
			$data['orden']=$this->Multipuntosmodel->getMulti($_GET['orden']);
			$datosOrden = $this->Multipuntosmodel->getOrdenData($_GET['orden']);
			$nombreTecnico=$this->Multipuntosmodel->getNameTec($datosOrden[0]->seo_technicianTeam);
			//contador para organizar los datos a mandar a la pagina
			$i = 1;

			//valores a saltarse (son numericos y no son parte de los semaforos)
			$valoresSaltar = array('tual', 'cero', 'prof', 'pres');

			//se recorren los resultados
			foreach($data['orden'][0] as $nomOrden => $valOrden)
			{
				//si el nombre del valor se encuentra dentro del arreglo de valores a saltarse (o si es ID, pero al cambiar el modelo se elimina / hace obsoleto)
				if(in_array(substr($nomOrden.'', -4), $valoresSaltar))
				{
					//se pasa el valor directo
					$data[$nomOrden] = $valOrden;
				}
				else
				{
					//sino, se obtiene el status y se pone en formato de Mn y se consigue el siguiente numero
					$data['M'.$i] = $this->getStatus($valOrden);
					$i++;
				}
			}


		$pdf = new TCPDF('', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Optima Automotriz');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins('0', '0','0');
		$pdf->SetFooterMargin(0);
		$pdf->SetAutoPageBreak(TRUE, 1);
		$pdf->AddPage();

		if($data['orden'][0]->mul_tipo=='s')
			$iser='<img width="15px;" src="images/multipuntos/ok.png">';
		else
			$iser='<img width="15px;" src="images/multipuntos/no.png">';

		if($data['orden'][0]->mul_tipo=='g')
			$igar='<img width="15px;" src="images/multipuntos/ok.png">';
		else
			$igar='<img width="15px;" src="images/multipuntos/no.png">';

		if($data['orden'][0]->mul_tipo=='c')
			$icar='<img width="15px;" src="images/multipuntos/ok.png">';
		else
			$icar='<img width="15px;" src="images/multipuntos/no.png">';

 		$pdf->SetMargins(10, PDF_MARGIN_TOP, 2);
        $html='
		<html>
		<style>
			.centrado
			{
				text-align: center;

			}
			.centrarVertical
			{
				vertical-align:middle;
			}
			.titulo
			{
				font-family: Verdana;
				font-size: 14pt;


			}
			table tr td
			{

			}
			table tr
			{

			}
			table
			{
				width: 100%;
			}
			.borde
			{
				border: 1px solid gray;
			}
			.sesenta
			{
				width: 60%;
			}
			.cuarenta
			{
				width: 40%
			}
		</style>
		<body>


			<table  class="" width="100%">
				<tr style="background-color: #CC0000;  color: white;">
				<th style="border:2px solid #cc0000;" align="center" width="55%">
					<table style="width:100%"><tr style="line-height:.5em;"><td ></td></tr></table>
						&nbsp;&nbsp;&nbsp;&nbsp;HOJA DE DIAGNOSTICO DE SU AUTOMOVIL
						<br>
						SIMBOLOGIA<br>
						<table  style=" background-color:white;color:black; font-size:7px;">
							<tr>
								<td  width="15%"  >
									<img width="10px;" src="/images/multipuntos/icon_v.png">OK
								</td>

								<td   width="37%">
									<img width="10px;" src="/images/multipuntos/icon_a.png">Necesita Servicio
								</td>

								<td  width="38%">
									<img width="10px;" src="/images/multipuntos/icon_r.png">Atencion Inmediata
								</td>
							</tr>
						</table>
				</th>
					<th style=" font-size:18px; font-weight:bold;   border:2px solid #cc0000; color:white"align="center" width="45%" >
					<table style="line-height:1em;"><tr><td></td></tr></table>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HONDA OPTIMA
					</th>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="50%"  >


						<table style="border:2px solid gray;">
						<tr bgcolor="gray">
						<td width="2%"></td>
					    <td bgcolor="gray" width="98%" colspan="2" class="titulo" style="text-align:center; bgcolor="gray"  border-bottom:1 px solid gray; background="black" color="white" ">
						Interior / Exterior
						</td>
						</tr>
				';

	if($data['M1']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M1']->color  == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M1']->color  == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M1']->color  == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html .='
				<tr style="font-size:11px;">
					<td width="2%"></td>
					<td  width="18%;">
						<table style="width:100%"><tr style="line-height:.1em;"><td></td><td></td></tr></table>
						<img width="50px;" src="'.$semaforo.'" >
					</td>
					<td style="line-height: 2px;   solid #ccc; vertical-align:middle;"width="80%;">
					Luz del tablero
					</td>
				</tr>
				<tr style="line-height:.3em;"><td style="  "></td><td style="  "></td></tr>
				';

				$tipo = 1;
	if($data['M2']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M2']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M2']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M2']->color == 3)
		$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html .='
				<tr style="line-height:.2em;"><td></td><td></td></tr>
				<tr style="font-size:11px;">
				<td width="2%"></td>
					<td  style="   " width="18%;">
									<img width="50px;" src="'.$semaforo.'" >
					</td>
					<td style="  font-size:11px;" width="80%;"  >Luces, luces traseras, direccionales, stops.
					luces de emergencia, cuartos.</td>
				</tr>
				<tr style="line-height:.3em;"><td></td><td></td></tr>
				';

				$tipo = 0;
	if($data['M3']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M3']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M3']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M3']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height:.5em;"><td style=""></td><td style=""></td></tr>
				<tr style=" font-size:11px;" >
				<td width="2%"></td>
					<td  width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td  width="250px;"  >
						Operación de limpiaparabrisas, plumas, liquido limpiaparabrisas
					</td>
				</tr>
				<tr style="line-height:.4em;"><td style="  "></td><td style="  "></td></tr>
				';


				$tipo = 0;
	if($data['M4']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M4']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M4']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M4']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height:.1em;"><td ></td><td></td></tr>
				<tr  >
				<td width="2%"></td>
					<td width="50px;" style=""><img width="50px;" src="'.$semaforo.'" ></td>
					<td style=" font-size:11px;" width="250px;"  >
						<table style="line-height:.5em"><tr><td></td></tr></table>
						Freno de mano
					</td>
				</tr>

				<tr style="line-height:.3em;"><td style=" "></td><td style=""></td></tr>
				';


				$tipo = 0;
	if($data['M5']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M5']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M5']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M5']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height:.1em;"><td ></td><td></td></tr>
				<tr  >
				<td width="2%"></td>
					<td width="50px;" style=""><img width="50px;" src="'.$semaforo.'" ></td>
					<td style=" font-size:11px;" width="250px;"  >
						<table style="line-height:.5em"><tr><td></td></tr></table>
						Operación del Claxón
					</td>
				</tr>

				<tr style="line-height:.3em;"><td style="  "></td><td style=""></td></tr>
				';
				$tipo = 0;
	if($data['M6']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M6']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M6']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M6']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height:.1em;"><td ></td><td></td></tr>
				<tr  >
				<td width="2%"></td>
					<td width="50px;" style=""><img width="50px;" src="'.$semaforo.'" ></td>
					<td style=" font-size:11px;" width="250px;"  >
						<table style="line-height:.5em"><tr><td></td></tr></table>
						Operación del clutch o embrague
					</td>
				</tr>

				<tr style="line-height:1.8em;"><td style=" "></td><td style=""></td></tr>
				';
				$tipo = 0;
	if($data['M7']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M7']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M7']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M7']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html.='<tr bgcolor="gray" style=" color:white">
				<td width="286px;" colspan="2" class="titulo" style="text-align:center;">
				Funcionamiento de Bateria
				</td></tr>';
				$html .='
				<tr style="font-size:11px;">
				<td width="2%"></td>
					<td  width="18%;">
						<table style="width:100%"><tr style="line-height:.1em;"><td></td><td></td></tr></table>
						<img width="50px;" src="'.$semaforo.'" >
					</td>
					<td style="line-height: 2.5em;   solid #ccc; vertical-align:middle;"width="80%;">
						Nivel Electrolitos
					</td>
				</tr>
				<tr style="line-height:.3em;"><td style="  "></td><td style="  "></td></tr>
				';

				$tipo = 0;
	if($data['M8']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M8']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M8']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M8']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";
			$html .='
				<tr style="line-height:.1em;"><td ></td><td></td></tr>
				<tr  >
				<td width="2%"></td>
					<td width="50px;" style=""><img width="50px;" src="'.$semaforo.'" ></td>
					<td style=" font-size:11px;" width="250px;"  >
						<table style="line-height:.1em"><tr><td></td></tr></table>
							Condicion de terminales
					</td>
				</tr>

				<tr style="line-height:.3em;"><td style="  "></td><td style=""></td></tr>
				';

				$tipo = 0;
	if($data['M9']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M9']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M9']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M9']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height:.1em;"><td ></td><td></td></tr>
				<tr  >
				<td width="2%"></td>
					<td width="50px;" style=""><img width="50px;" src="'.$semaforo.'" ></td>
					<td style=" font-size:11px;" width="250px;"  >
						<table style="line-height:.1em"><tr><td></td></tr></table>
							Estado Físico y de Sujeción
					</td>
				</tr>

				<tr style="line-height:1.8em;"><td style="  "></td><td style=""></td></tr>
				';

$html.='<tr bgcolor="gray" style=" color:white">
				<td width="286px;" colspan="2" class="titulo" style="text-align:center; ">
				Debajo del Cofre
				</td></tr>';
				$tipo = 0;
	if($data['M10']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M10']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M10']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M10']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";

			$html .='
				<tr style="line-height:.5em;"><td ></td><td></td></tr>
				<tr style="font-size:11px;">
				<td width="2%"></td>
					<td  width="18%;">
						<table style="width:100%"><tr style="line-height:.1em;"><td></td><td></td></tr></table>
						<img width="50px;" src="'.$semaforo.'" >
					</td>
					<td style="line-height: 2em;   solid #ccc; vertical-align:middle;"width="80%;">
						Soportes del Motor
					</td>
				</tr>
				<tr style="line-height:.3em;"><td style="  "></td><td style="  "></td></tr>
				';


				if($data['M11']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M11']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M11']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M11']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";


$html .='
				<tr style="line-height:.1em;"><td ></td><td></td></tr>
				<tr  >
				<td width="2%"></td>
					<td width="50px;" style="line-height:.5em"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style=" font-size:11px;" width="250px;"  >

							Líquidos: aceites, anticongelante, dirección, transmisión, frenos
					</td>
				</tr>

				<tr style="line-height:.3em;"><td style="  "></td><td style=""></td></tr>
				';


				if($data['M12']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
			if($data['M12']->color == 1)
				$semaforo = "images/multipuntos/amarilloEncendido.png";
			if($data['M12']->color == 2)
				$semaforo = "images/multipuntos/rojoEncendido.png";
		if($data['M12']->color == 3)
		$semaforo = "images/multipuntos/ningunoEncendido.jpg";


$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Radiadores, mangueras, bandas</td>
				</tr>';



			if($data['M13']->color == 0)
		$semaforo = "images/multipuntos/verdeEncendido.png";
	if($data['M13']->color == 1)
		$semaforo = "images/multipuntos/amarilloEncendido.png";
	if($data['M13']->color == 2)
		$semaforo = "images/multipuntos/rojoEncendido.png";
if($data['M13']->color == 3)
$semaforo = "images/multipuntos/ningunoEncendido.jpg";

$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Ajustes de frenos de mano, lineas</td>
				</tr>';

				if($data['M14']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M14']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M14']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M14']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td  style="border 1px solid #ccc" width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Flechas y juntas homocineticas</td>
				</tr>';

				if($data['M15']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M15']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M15']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M15']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Sistema de escape</td>
				</tr>';
				if($data['M16']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M16']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M16']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M16']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td  style="border 1px solid #ccc" width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Fugas de aceite</td>
				</tr><br>';

$html.='<tr bgcolor="gray" style=" color:white">
				<td width="286px;" colspan="2" class="titulo" style="text-align:center;">
				Debajo de la unidad
				</td></tr>';

				if($data['M17']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M17']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M17']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M17']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html .='
					<tr style="line-height:.2em;"><td ></td><td></td></tr>
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Gomas de Suspencion</td>
				</tr>';


				if($data['M18']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M18']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M18']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M18']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Bases de amortiguadores</td>
				</tr>';

				if($data['M19']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M19']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M19']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M19']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Terminales de dirección</td>
				</tr>';


				if($data['M20']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M20']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M20']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M20']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
				$html .='
				<tr style="line-height: 2px;" >
				<td width="2%"></td>
					<td width="50px;"><img width="50px;" src="'.$semaforo.'" ></td>
					<td style="border 1px solid #ccc; font-size:11px;" width="250px;"  >Amortiguadores</td>
				</tr>';






						$html.='
						</table>

						</td>
						<td width="50%">

						<table >
						<tr>
							<td width="50%" style=" border-right:2px solid gray; border-left:1px solid gray; border-bottom:1px solid gray" >
								<table>
									<tr bgcolor="gray">
								<td bgcolor="gray" colspan="2" class="titulo" style="text-align:center; bgcolor="gray"  border-bottom:1 px solid gray; background="black" color="white" ">Condición de Llantas </td>
						</tr>
								</table>';

							if($data['M21']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M21']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M21']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M21']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html.='
				<br><br>
				<table><tr>
				<td width="2%"></td>
				<td colspan="2" style="font-size:10px; font-weight:bold">Delantera Izquierda</td></tr>
				<tr>
					<td></td>
					<td colspan="2"><img width="40px;" src="'.$semaforo.'" ></td>
				</tr>
				<tr>
					<td></td>
					<td width="100px;" style="font-size:10px;">Profundidad (mm)</td>
					<td width="30px;" style=" text-align:center;border-bottom:1px solid black">'.$data['llan_diprof'].'</td>
				</tr>
				<tr style="line-height:.3em"><td></td></tr>
				<tr>
					<td></td>
					<td style="font-size:10px;">Presion PSI</td>
					<td style=" text-align:center; border-bottom:1px solid black"> '.$data['llan_dipres'].'</td>
				</tr>
				</table>';

				if($data['M22']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M22']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M22']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M22']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html.='
				<br><br>
				<table>
				<tr>
					<td width="2%"></td>
					<td colspan="2" style="font-size:10px; font-weight:bold">Delantera Derecha</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2"><img width="40px;" src="'.$semaforo.'" ></td>
				</tr>
				<tr>
					<td></td>
					<td width="100px;" style="font-size:10px;">Profundidad (mm)</td>
					<td width="30px;" style=" text-align:center;border-bottom:1px solid black">'.$data['llan_ddprof'].'</td>
				</tr>
				<tr style="line-height:.3em"><td></td></tr>
				<tr>
					<td></td>
					<td style="font-size:10px;">Presion PSI</td>
					<td style=" text-align:center;border-bottom:1px solid black"> '.$data['llan_ddpres'].'</td>
				</tr>
				</table>';


				if($data['M23']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M23']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M23']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M23']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html.='
				<br><br>
				<table><tr>
				<td width="2%"></td>
				<td colspan="2" style="font-size:10px; font-weight:bold">Trasera Izquierda</td></tr>
				<tr>
				<td></td>
				<td colspan="2"><img width="40px;" src="'.$semaforo.'" ></td></tr>
				<tr>
				<td></td>
				<td width="100px;" style="font-size:10px;">Profundidad (mm)</td>
				<td width="30px;" style=" text-align:center;border-bottom:1px solid black">'.$data['llan_tiprof'].'</td></tr>
				<tr>
				<td></td>
				<td style="font-size:10px;">Presion PSI</td>
				<td style=" text-align:center;border-bottom:1px solid black">'.$data['llan_tipres'].'</td></tr>
				</table>';

				if($data['M24']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M24']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M24']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M24']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html.='
				<br><br>
				<table><tr>
				<td width="2%"></td>
				<td colspan="2" style="font-size:10px; font-weight:bold">Trasera Derecha</td></tr>
				<tr>
				<td></td>
				<td colspan="2"><img width="40px;" src="'.$semaforo.'" ></td></tr>
				<tr>
				<td></td>
				<td width="100px;" style="font-size:10px;">Profundidad (mm)</td>
				<td width="30px;" style=" text-align:center;border-bottom:1px solid black">'.$data['llan_tdprof'].'</td></tr>
				<tr>
				<td></td>
				<td style="font-size:10px;">Presion PSI</td>
				<td style=" text-align:center;border-bottom:1px solid black">'.$data['llan_tdpres'].'</td></tr>
				</table>';


				if($data['M25']->color == 0)
				$semaforo = "images/multipuntos/verdeEncendido.png";
				if($data['M25']->color == 1)
					$semaforo = "images/multipuntos/amarilloEncendido.png";
				if($data['M25']->color == 2)
					$semaforo = "images/multipuntos/rojoEncendido.png";
				if($data['M25']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";

				$html.='
				<br><br>
				<table><tr>
				<td width="2%"></td>
				<td colspan="2" style="font-size:10px; font-weight:bold">Llanta de Refacción</td></tr>
				<tr>
				<td></td>
				<td colspan="2"><img width="40px;" src="'.$semaforo.'" ></td></tr>
				<tr>
				<td></td>
				<td width="100px;" style="font-size:10px;">Profundidad (mm)</td>
				<td width="30px;" style=" text-align:center;border-bottom:1px solid black">'.$data['llan_refprof'].'</td></tr>
				<tr>
				<td></td>
				<td style="font-size:10px;">Presion PSI</td>
				<td style=" text-align:center;border-bottom:1px solid black">'.$data['llan_refpres'].'</td>
				</tr>
				</table><br><br>';



							$html.='</td>
							<td width="100%"><table >
						<tr>
							<td width="50%" style=" border-right:2px solid gray; border-left:1px solid gray; border-bottom:1px solid gray" >
								<table>
									<tr bgcolor="gray">
								<td bgcolor="gray" colspan="2" class="titulo" style="text-align:center; bgcolor="gray"  border-bottom:2 px solid gray; background="black" color="white" ">
								Medicion de Frenos
								</td>
						</tr>
								</table>';
					if($data['M27']->color == 0)
				$semaforo = "images/multipuntos/lg_green_on.png";
				if($data['M27']->color == 1)
					$semaforo = "images/multipuntos/lg_yellow_on.png";
				if($data['M27']->color == 2)
					$semaforo = "images/multipuntos/lg_red_on.jpg";
				if($data['M27']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
								$html.='
				<br><br>
				<table><tr>
				<td width="2%"></td>
				<td width="100%" colspan="2"  style="font-size:10px; font-weight:bold">Delantero Izquierdo</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
					<table style="font-size:8px;">
					<tr>

						<td style="text-align:center">Balata Cero km(mm)</td>
						<td width="28%"></td>
						<td style="text-align:center">Su balata actual(mm)</td>
					</tr>
					<tr style="line-height:.6em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>

						<td style="font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_dicero'].'</td>
						<td ></td>
						<td style="font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_diactual'].'</td>
					</tr>
					<tr style="line-height:.6em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</table>

					<table style="font-size:8px;">
					<tr>
					<td width="2%"></td>
						<td width="30%" style="text-align:center">De 8 a <br>11 mm</td>
						<td width="30%" style="text-align:center">De 5 a <br>7 mm</td>
						<td  width="30%" style="text-align:center">De 2 a <br>4 mm</td>
					</tr>
					<tr>
						<td width="8%"></td>
						<td width="80%" colspan="3"><img src="'.$semaforo.'"></td>
					</tr>
					</table>

				</td></tr>

				</table>';


					if($data['M27']->color == 0)
				$semaforo = "images/multipuntos/lg_green_on.png";
				if($data['M27']->color == 1)
					$semaforo = "images/multipuntos/lg_yellow_on.png";
				if($data['M27']->color == 2)
					$semaforo = "images/multipuntos/lg_red_on.jpg";
				if($data['M27']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
								$html.='
					<br><br>
				<table><tr>
				<td width="2%"></td>
				<td width="100%" colspan="2"  style="font-size:10px; font-weight:bold">Delantero Derecho</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
					<table style="font-size:8px;">
					<tr>

						<td style="text-align:center">Balata Cero km(mm)</td>
						<td width="28%"></td>
						<td style="text-align:center">Su balata actual(mm)</td>
					</tr>
					<tr style="line-height:.6em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>

						<td style="font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_ddcero'].'</td>
						<td ></td>
						<td style="font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_ddactual'].'</td>
					</tr>
					<tr style="line-height:.6em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</table>

					<table style="font-size:8px;">
					<tr>
					<td width="2%"></td>
						<td width="30%" style="text-align:center">De 8 a <br>11 mm</td>
						<td width="30%" style="text-align:center">De 5 a <br>7 mm</td>
						<td  width="30%" style="text-align:center">De 2 a <br>4 mm</td>
					</tr>
					<tr>
						<td width="8%"></td>
						<td width="80%" colspan="3"><img src="'.$semaforo.'"></td>
					</tr>
					</table>

				</td></tr>

				</table>';


				if($data['M28']->color == 0)
				$semaforo = "images/multipuntos/lg_green_on.png";
				if($data['M28']->color == 1)
					$semaforo = "images/multipuntos/lg_yellow_on.png";
				if($data['M28']->color == 2)
					$semaforo = "images/multipuntos/lg_red_on.jpg";
				if($data['M28']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
								$html.='
					<br><br>
				<table><tr>
				<td width="2%"></td>
				<td width="100%" colspan="2"  style="font-size:10px; font-weight:bold">Trasero Izquierdo</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
					<table style="font-size:8px;">
					<tr>

						<td style="text-align:center">Balata Cero km(mm)</td>
							<td width="28%"></td>
						<td style="text-align:center">Su balata actual(mm)</td>
					</tr>
					<tr style="line-height:.4em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>

						<td style="font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_ticero'].'</td>
						<td ></td>
						<td style=" font-weight:bold;text-align:center;border-bottom:1px solid black">'.$data['fre_tiactual'].'</td>
					</tr>
					<tr style="line-height:.4em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</table>

					<table style="font-size:8px;">
					<tr>
					<td width="2%"></td>
						<td width="30%" style="text-align:center">De 8 a <br>11 mm</td>
						<td width="30%" style="text-align:center">De 5 a <br>7 mm</td>
						<td  width="30%" style="text-align:center">De 2 a <br>4 mm</td>
					</tr>
					<tr>
						<td width="8%"></td>
						<td width="80%" colspan="3"><img src="'.$semaforo.'"></td>
					</tr>
					</table>

				</td></tr>

				</table>';


				if($data['M29']->color == 0)
				$semaforo = "images/multipuntos/lg_green_on.png";
				if($data['M29']->color == 1)
					$semaforo = "images/multipuntos/lg_yellow_on.png";
				if($data['M29']->color == 2)
					$semaforo = "images/multipuntos/lg_red_on.jpg";
				if($data['M29']->color == 3)
				$semaforo = "images/multipuntos/ningunoEncendido.jpg";
								$html.='
					<br><br>
				<table><tr>
				<td width="2%"></td>
				<td width="100%" colspan="2"  style="font-size:10px; font-weight:bold">Delantero Izquierdo</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
					<table style="font-size:8px;">
					<tr>

						<td style="text-align:center">Balata Cero km(mm)</td>
						<td width="28%"></td>
						<td style="text-align:center">Su balata actual(mm)</td>
					</tr>
					<tr style="line-height:.4em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>

						<td style=" font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_tdcero'].'</td>
						<td ></td>
						<td style="font-weight:bold; text-align:center;border-bottom:1px solid black">'.$data['fre_tdactual'].'</td>
					</tr>
					<tr style="line-height:.4em">
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</table>

					<table style="font-size:8px;">
					<tr>
					<td width="2%"></td>
						<td width="30%" style="text-align:center">De 8 a <br>11 mm</td>
						<td width="30%" style="text-align:center">De 5 a <br>7 mm</td>
						<td  width="30%" style="text-align:center">De 2 a <br>4 mm</td>
					</tr>
					<tr>
						<td width="8%"></td>
						<td width="80%" colspan="3"><img src="'.$semaforo.'"></td>
					</tr>
					</table>

				</td></tr>

				</table>';


								$html.='</td>
								</tr>
								</table>

								</td>
						</tr>
						</table>



								';
						list($fe,$hr)=explode(" ",$datosOrden[0]->seo_date);
						$html.='
						<table width="100%"  style=" font-size:11px;border:2xp solid #999999; " >
					<tr bgcolor="gray">
								<td colspan="4"
								style=" font-size:14px; color:white; background-color:#CC0000; text-align:center;">
								Cultura de Servicio Honda
								</td>
						</tr>
				<tr  style="line-height:1em">
					<td width="2%"></td>
					<td></td>
					<td width="30%"></td>
					<td	width="30%"></td>
				</tr>
				<tr>
					<td></td>
					<td width="66%" colspan="2"><b>Fecha:</b>&nbsp;&nbsp; '.$fe.'</td>
					<td width="35%"><b>Orden No.:</b>'.$_GET['orden'].'</td>
				</tr>
				<tr><td colspan="4" style="line-height:.2em"></td></tr>
				<tr>
					<td></td>
					<td colspan="3"><b>VIN:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$datosOrden[0]->sev_vin.'</td>
				</tr>
				<tr><td colspan="4" style="line-height:.2em"></td></tr>

				<tr>
					<td></td>
					<td width="66%" colspan="2"><b>Modelo:</b>'.$datosOrden[0]->sev_sub_marca.'</td>
					<td width="35%"><b>Torre.:</b>'.$datosOrden[0]->seo_tower.'</td>
				</tr>




				<tr><td colspan="4" style="line-height:.3em"></td></tr>
				<tr>
					<td></td>

					<td colspan="3">
					<table><tr>
					<td>'.$iser.' Servicio</td><td> '.$igar.' Garantia </td><td>'.$icar.' Carrocería
					</td>
					</tr></table>
					</td>
				</tr>
				<tr><td colspan="4" style="line-height:.2em"></td></tr>
				<tr>
					<td></td>
					<td colspan="3"><b>Servicio:</b> '.$datosOrden[0]->set_name.'</td>

				</tr>
				<tr><td colspan="4" style="line-height:.2em"></td></tr>
				<tr>
					<td></td>
					<td colspan="3"  width="98%"><b>Asesor de servicio:</b> '.$datosOrden[0]->sus_name.'</td>
				</tr>
				<tr><td colspan="4" style="line-height:.2em"></td></tr>
				<tr>
					<td></td>
					<td colspan="3"><b>Técnico: </b>'.$nombreTecnico.'</td>
				</tr>
				<tr><td colspan="4" style="line-height:.2em"></td></tr>

				<tr><td colspan="4" style="line-height:.2em"></td></tr>
				<tr>
					<td></td>
					<td colspan="3"><b>Nombre del cliente:</b><br>'.$datosOrden[0]->cus_name.'</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="3"><b>Proximo Servicio:</b>'.$data['orden'][0]->mul_proximo.'</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="3"><b>Observaciones:</b><br>'.$data['orden'][0]->mul_km.'</td>
				</tr>

				<tr><td colspan="4" style="line-height:.0em"></td></tr>
				<tr>
					<td colspan="4" style="line-height:.0em"></td>
				</tr>

				<tr><td><br></td></tr>
			</table>

						</td>
					</tr>
				</table>
		</body>
		</html>
		';


// Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0,$reseth = true, $align = '', $autopadding = true);

		//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        //Cerramos y damos salida al fichero PDF
    	//$pdf->Output('hoja_entrega.pdf', 'I');

	    $nombre_archivo = "MULTIPUNTOS.pdf";
        $pdf->Output($nombre_archivo, 'F');


		$this->email->from('casa@hondaoptima.com', 'Honda Optima');
        $this->email->to(''.$datosOrden[0]->cus_email.'');
        $this->email->cc('lestrada@hondaoptima.com');
        $this->email->subject('HOJA DE DIAGNOSTICO DE SU AUTOMOVIL');
        $this->email->message('
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;}
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; }
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}

	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">

               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img  src="http://hcrm.gpoptima.net/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<table width="100%"><tr>


<div >
 <h1>HOJA DE DIAGNOSTICO DE SU AUTOMOVIL.</h1>

<br><br>

<table>
<tr>
<td >
Estimado(a) '.$datosOrden[0]->cus_name.'. El servicio de su vehículo ha concluido.
Adjunto a este correo, encontrara la Hoja de Diagnostico de su vehículo.

</td></tr>
</table>
</div>
<div>
<br><br>
Gracias por su prefencia !
            </td>
         </tr>
      </table>




      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://hcrm.gpoptima.net/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>

      </td>
      </tr>
      </table>
   </body>
</html>');
$this->email->set_mailtype('html');
$this->email->attach($nombre_archivo);


        if ($this->email->send())
		{
		    echo 'success';
            //echo $this->email->send();
            // echo $this->email->print_debugger();
		}else{
			echo 'error----'.$eema.'----'.$orden[0]->sus_firma;
             echo $this->email->send();
            // echo $this->email->print_debugger();
		}


		}

				function pdf_seminuevos()
		{
		$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(' ');
        $pdf->SetTitle('Multipuntos');
        $pdf->SetSubject('');
        $pdf->SetKeywords('SIN');

         //definimos hora de B.C
         date_default_timezone_set('Mexico/BajaNorte');

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
// set default header data
    //$pdf->SetHeaderData('logo.png', '40', 'Hoja Diagnostico de su Automóvil', '', array(0,64,5), array(0,0,0));
    //$pdf->setFooterData(array(0,64,0), array(0,0,0));

    // set header and footer fonts

    $pdf->setHeaderFont(Array('', '', '10'));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);
    $pdf->SetFont('times', '', 12, '', true);


     //formato de hora
   /* ini_set('date.timezone','Mexico/BajaNorte');
    .date("g_i_A").*/



//fijar efecto de sombra en el texto
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0, 'depth_h' => 0, 'color' => array(196, 196, 196), 'opacity' => 0, 'blend_mode' => 'Normal'));

// Establecemos el contenido para imprimir
   //TOP, LOGO Y HEADER

        //preparamos y maquetamos el contenido a crear
$pdf->AddPage();

        $html='
		<style>

		</style>
		<table>
			<tr>
				<td colspan="2">Lista / Orden de Servicio de Inspeccion
</td>
				<td>Cliente:</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>No. Serie</td>
				<td>Color</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Modelo</td>
				<td>Año</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Kilometraje</td>
				<td>Placas</td>
				<td></td>
			</tr>
		</table>
		<table>
			<tr>
				<td colspan="3"></td>
			</tr>
			<tr>
				<td>Fecha</td>
				<td colspan="2"></td>

			</tr>
			<tr>
				<td>Torre</td>
				<td>Inspección de Aparencia</td>
				<td>Seminuevo</td>
			</tr>
		</table>
		';
// Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

// ---------------------------------------------------------
// Cerrar el documento PDF y preparamos la salida
// Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = "seminuevos.pdf";
        $pdf->Output($nombre_archivo, 'I');
		}

	function get_lista_traslados()
	{
		header("Content-type: application/json; charset=utf-8");
		$lista = array();
		if(!empty($_GET["tipo"]))
		{
			$fecha_vacia = "0000-00-00"; $fecha_hora_vacia = "0000-00-00 00:00:00";
			extract($_GET);
			$ciudad = (empty($_GET["ciudad"]) ? $_SESSION["sfworkshop"] : $_GET["ciudad"]);
			$lista = $this->Reportesmodel->get_traslados_tipo($ciudad,$tipo);

			foreach($lista AS $r)
			{
				$r->fecha_salida_formateada = fecha_completa_corta(date('Y-m-d', strtotime($r->tra_fecha_salida)))." ".$r->tra_hora_salida;

				if($r->tra_fecha_salida_real != $fecha_hora_vacia)
				{
					$r->mostrar_real = true;
					$r->fecha_salida_real_formateada = fecha_completa_corta(date('Y-m-d', strtotime($r->tra_fecha_salida_real)))." ".date('h:i A', strtotime($r->tra_fecha_salida_real));
				}
				else
					$r->mostrar_real = false;
			}
		}

		echo json_encode($lista);
	}

	function get_traslados_origen_y_destino()
	{
		header("Content-type: application/json; charset=utf-8");
		$lista = array();

		$fecha_vacia = "0000-00-00"; $fecha_hora_vacia = "0000-00-00 00:00:00";
		extract($_GET);
		$ciudad = (empty($_GET["ciudad"]) ? $_SESSION["sfworkshop"] : $_GET["ciudad"]);
		$lista = $this->Reportesmodel->get_traslados_origen_y_destino($ciudad);

		foreach($lista AS $r)
		{
			$r->fecha_salida_formateada = fecha_completa_corta(date('Y-m-d', strtotime($r->tra_fecha_salida)))." ".$r->tra_hora_salida;

			if($r->tra_fecha_salida_real != $fecha_hora_vacia)
			{
				$r->mostrar_real = true;
				$r->fecha_salida_real_formateada = fecha_completa_corta(date('Y-m-d', strtotime($r->tra_fecha_salida_real)))." ".date('h:i A', strtotime($r->tra_fecha_salida_real));
			}
			else
				$r->mostrar_real = false;
		}

		echo json_encode($lista);
	}

	function update_fecha_salida_real()
	{
		header("Content-type: application/json; charset=utf-8");
		$response = array();

		if(!empty($_POST["id"]) && !empty($_POST["fecha_salida_real"]))
		{
			if($_SESSION["sfid"] == 97)//ID de Genesis.
			{
				extract($_POST);

				$datos = array("tra_fecha_salida_real" => $fecha_salida_real);

				if($this->Reportesmodel->update_traslado($id,$datos))
				{
					$response["success"] = "Se registró correctamente";
					$response["fecha_nueva"] = fecha_completa_corta(date('Y-m-d', strtotime($fecha_salida_real)))." ".date('h:i A', strtotime($fecha_salida_real));
				}
				else $response["error"] = "No se logró registrar la fecha";
			}
			else $response["error"] = "Disculpe, no tiene permisos para realizar esta operación";
		}
		else $response["No se recibió el id o la fecha"];

		echo json_encode($response);
	}
}
