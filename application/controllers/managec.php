<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Managec extends CI_Controller { 

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pizarronmodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
        $this->load->model('Managecmodel');
        
		$this->load->helper('url');
        
        $this->load->library('email');
		$this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");;
		
		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
									
		
									
	}
	
	public function managehome()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		
		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}	
        
        $data['asesores']=$this->Managecmodel->getAsesores($taller);
        $data['datos']=$this->Managecmodel->getEfficiency($taller);
        $data['wshop'] = $taller;
		$this->load->view('managec/managehome', $data);
	}
    
    
}

