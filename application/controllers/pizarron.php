<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pizarron extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pizarronmodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->helper('dates');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('textmagic');
		$this->load->library('form_validation');
		$this->load->library("curl");;

		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
	}

	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}

		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		$this->load->view('pizarronu/vistac',$data);
	}

	public function victor()
	{
		header("Content-type: application/json; charset=utf-8");


		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}

		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		echo json_encode($data);
	}

	public function indexB()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}

		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		$this->load->view('pizarron/vista',$data);
	}

	public function index_inv()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['fecha'])){$date=date('d-m-Y');}else{$date=$_GET['fecha'];}
		if(empty($_GET['taller'])){$taller=$_SESSION['sfidws'];}else{$taller=$_GET['taller'];}

		$data['fecha']=$date;
		$data['taller']=$taller;
		$data['usuarios']=$this->Usuariomodel->listaUsuarios(0);
		$data['arraystatus']=$this->Pizarronmodel->status();
		$data['servicios']=$this->Pizarronmodel->getOrdenServiciosTodas($taller);
		$data['serviciossv']=$this->Pizarronmodel->getOrdenServiciosTodassv($taller);
		$data['pizarron']=$this->Pizarronmodel->getLista($taller,$date);
		$data['pizarron_inventario']=$this->Pizarronmodel->getLista_inventario($taller,$date);
		$this->load->view('pizarron/vista_copia',$data);
	}

	function cambiardia()
	{
		$this->Pizarronmodel->updateDia($_POST['ido'],$_POST['fecha']);

		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Torre Cambiada de Fecha.</div>');
		list($an,$ms,$dd)=explode("-",$_POST['fecha']);
		$fecha=$dd.'-'.$ms.'-'.$an;
		redirect('pizarron?fecha='.$fecha.'');
	}

	function update()
	{
		//Actualiza estatus en el Pizarron
		$this->Pizarronmodel->update($_GET['id'],$_GET['campo'],$_GET['value']);
		//Trae informacion de la Orden
		$info=$this->Pizarronmodel->getOrdenVehi($_GET['id']);
			//En rampa=2;				//En prueba=4			//Terminado=5
		if($_GET['value'] == 2 || $_GET['value'] == 4 || $_GET['value'] == 5)
		{
			if($_GET['campo'] == 'piz_status_lavado' && $_GET['value'] == 5)
				$this->curl->simple_get('http://happ.gpoptima.net/PushNotifications/firstNotification.php?vin='.$info[0]->sev_vin.'&status=lavado');
			else
				$this->curl->simple_get('http://happ.gpoptima.net/PushNotifications/firstNotification.php?vin='.$info[0]->sev_vin.'&status='.$_GET['value']);
		}
		//Si tiene Emial ENvia Email

		if($info[0]->app_reminderType==2 || $info[0]->app_reminderType == 4)
		{
			if($info[0]->piz_status_mecanico == 5 && $info[0]->piz_status_lavado == 5)
			{
				$auto = $info[0]->sev_marca.' '.$info[0]->sev_sub_marca.' '.$info[0]->sev_version.' '.$info[0]->sev_color.' '.$info[0]->sev_modelo;
				$this->Pizarronmodel->emailRemider($_GET['value'],$info[0]->app_customerEmail,$info[0]->piz_status_mecanico,$info[0]->piz_status_lavado,$info[0]->piz_ID_orden,$info[0]->seo_promisedTime,$info[0]->pizarron, $auto);
			}
		}

		//si tiene SMS Envia SMS
		if($info[0]->app_reminderType == 3 || $info[0]->app_reminderType == 4)
		{
			if($info[0]->piz_status_mecanico==5 && $info[0]->piz_status_lavado==5)
			{
				function clean($string)
				{
					$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
					return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
				}
				$tel = clean($info[0]->app_customerTelephone);
				$tel = preg_replace("/[^0-9]/","",$tel);
				if($_SESSION['sfworkshop'] != 'Tijuana')
					$this->Pizarronmodel->SmsBienvenida($tel);
			}
		}

		//si el estatus es 8 se modificara la base de datos de autos de CRM autos_en_pisoe invetario
		// a y traves de CURL
		if($_GET['value']==8)
		{
			$this->Pizarronmodel->updateClaveRadio($_GET['orden'],$_GET['clave']);

			$url = base_url()."ajax/inventario/inv_status_update.php";
			$post_data = array(
				"status" => $_GET['value'],
				"vin" =>$_GET['vin']
			);

			$this->curl->simple_post($url, $post_data);
		}

		if($_GET['value']> 8)
		{
			$url = base_url()."ajax/inventario/inv_status_ubicacion.php";

			$post_data = array(
				"ubicacion" => $_GET['value'],
				"vin" =>$_GET['vin']
			);

			$this->curl->simple_post($url, $post_data);
		}

		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Exito</strong> Status Modificado</div>');
		redirect('pizarron?fecha='.$_GET['fecha']);
	}

	function updatePizarronInventario()
	{
		list($torre,$id) = explode("/",$_GET['valor']);
		$datos = array('pii_torre' => $torre);
		$this->Pizarronmodel->updatePizarronInventario($datos,$id);
	}

	function pasaraPreviado($tecnico,$vin)
	{
		$datos = array(
			'pii_estado'=>'Asignado',
			'pii_tecnico'=>$tecnico
		);

		$this->Pizarronmodel->updatePizarronInventario($datos,$vin);
	}

	function lavado()
	{
		$data["fecha"] = fecha_completa_larga(date("Y-m-d"));
		$this->load->view("pizarron/lavado", $data);
	}

	function get_autos_lavado()
	{
		header("Content-type: application/json; charset=utf-8");
		// $data["data"] = $this->Pizarronmodel->get_autos_lavado($_SESSION["sfidws"]);
		$fecha = date("Y-m-d");
		$resultado = $this->Pizarronmodel->get_autos_lavado($_SESSION["sfidws"], $fecha);
		$data["data"] = array();
		$i = 0;
		foreach($resultado AS $r)
		{
			$a = array();

			$a[] = $r->orden_id;
			$a[] = $r->asesor_id;
			$a[] = $r->tiempo_entrega;
			$a[] = $r->tecnico_id;

			$a[] = ++$i;
			$a[] = $r->tiempo_entrega_am_pm;
			$a[] = $r->auto;
			$a[] = $r->torre;
			$a[] = $r->vin;
			$a[] = $r->asesor_nombre;
			$a[] = $r->tecnico_nombre;
			$a[] = $r->status_lavado;

			// $a[] = "";



		 	$data["data"][] = $a;
		}


		echo json_encode($data,JSON_NUMERIC_CHECK);
	}
}
