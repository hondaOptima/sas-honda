<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicio extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Serviciomodel');
		$this->load->model('Permisomodel');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	
	}
	
	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['tservicios']=$this->Serviciomodel->listarTServicios($this->per);
		$data['modelos']=$this->Serviciomodel->listarModelos($this->per);
		$data['colPro']=$this->Serviciomodel->serviciosPro($this->per);
		$data['colNopro']=$this->Serviciomodel->serviciosNoPro($this->per);
		$data['servicios']=$this->Serviciomodel->listarServicios($this->per);
		$data['serviciosx']=$this->Serviciomodel->listarServiciosx($this->per);
		// $this->load->view('servicio/vista',$data);
        redirect('servicio/index2');
	}

	public function index2()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['tservicios']=$this->Serviciomodel->listarTServicios($this->per);
		$data['modelos']=$this->Serviciomodel->listarModelos($this->per);
		$result = array();
		foreach ( $data['modelos'] as $elem) {
			$objeto = array();
			$objetoB = array();
			$objeto[0] = $elem->vem_name;
			$objeto[1] = $this->Serviciomodel->getProsNoPros($elem->ser_vehicleModel,'pro');
			$objeto[2] = $this->Serviciomodel->getProsNoPros($elem->ser_vehicleModel,'nopro');
			array_push($objetoB,$objeto);
			array_push($result, $objetoB);
		}
		$data['result'] = $result;
		$data['colPro']=$this->Serviciomodel->serviciosPro($this->per);
		$data['colNopro']=$this->Serviciomodel->serviciosNoPro($this->per);
		$data['servicios']=$this->Serviciomodel->listarServicios($this->per);
		$data['serviciosx']=$this->Serviciomodel->listarServiciosx($this->per);
		$this->load->view('servicio/vistaNueva',$data);
	}
	
	public function crear()
	{
		
     
		$this->form_validation->set_rules('tipoServicio', 'Tipo de Servicio', 'required');
		
        if ($this->form_validation->run())
        {
			
			$modelo=$this->input->post('modelo');
			$num=$this->input->post('num');
			$hora=$this->input->post('hora');
			$active=$this->input->post('active');
			
			
			for($i=0; $i< $num; $i++){
			
			$servicio = array(
						'ser_price'=>'',
						'ser_vehicleYearFrom'=>'',
						'ser_vehicleYearTo'=>'',
						'ser_description'=>'',
						'ser_vehicleModel'=>$modelo[$i],
						'ser_serviceType'=>$this->input->post('servicio'),
						'ser_isActive'=>1,
						'ser_approximate_duration'=>$hora[$i],	
						'ser_tipo'=>$this->input->post('tipoServicio'),												
						);
         //print_r($servicio);
		 $this->Serviciomodel->agregarServicio($servicio);
			}
		//redirect('servicio');
		} 
		else
		{
			$data['servicio']=$this->Serviciomodel->obtenerTipos();
			$data['modelos']=$this->Serviciomodel->obtenerModelos();
			$this->load->view('servicio/crear',$data);
		}
	}
	
	public function editar($id)
	{
	 	$data['servicio'] = $this->Serviciomodel->obtenerServicio($id);
		
		if($data['servicio'])
		{
			//$data['tiposServicio']=$this->Serviciomodel->obtenerTipos();
			//$data['modelos']=$this->Serviciomodel->obtenerModelos();
			
			$this->load->view('servicio/editar', $data);
		} 
		else
		{
			redirect('error');
		}
	}
	
	public function actualizar(){
       redirect('servicio');
	}
	
	function update(){
       $id=$_POST['id'];
	   $val=$_POST['valor'];
	   $tipo=$_POST['tipo'];
	   
	   if($tipo=='price'){
		  $servicio = array(
						'ser_price'=>$val
						); 
		   }
	   elseif($tipo=='time'){
	   $servicio = array(
						'ser_approximate_duration'=>$val
						);
	   }
	   elseif($tipo=='refac'){
	   $servicio = array(
						'ser_refacciones'=>$val
						);
	   }
	   elseif($tipo=='materiales'){
	   $servicio = array(
						'ser_mat_varios'=>$val
						);
	   }
	   elseif($tipo=='comercial'){
	   $servicio = array(
						'ser_comercial'=>$val
						);
	   }
       elseif($tipo=='actual'){
	   $servicio = array(
						'ser_actual'=>$val
						);
	   }
	   
	   $this->Serviciomodel->actualizarServicio($servicio,$id);
	}
	
	function updateSta(){
       $id=$_POST['id'];
	   $val=$_POST['valor'];
	   $servicio = array(
						'ser_isActive'=>$val
						);
       $this->Serviciomodel->actualizarServicio($servicio,$id);
	}
	
	function desactivar($id){
	 	$this->Serviciomodel->desactivarServicio($id);
		redirect('servicio');
	}
	
	function activar($id){
	 	$this->Serviciomodel->activarServicio($id);
		redirect('servicio');
	}
	
	function lista()
	{
	 	$data['lista'] = $this->Modelovehiculomodel->obtenerModelo($id);
		
		if($data['lista'])
		{
			$this->load->view('servicio/listaServicios', $data);
		} 
		else
		{
			redirect('error');
		}
	}
	
	
}