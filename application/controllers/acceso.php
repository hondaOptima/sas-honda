<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acceso extends CI_Controller {

	public function __construct()
	{   session_start();
		parent::__construct();
		$this->load->model('Accesomodel');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('email');
	}

	public function index()
	{
		if(isset($_SESSION['sfid'])) redirect('dashboard');
		$data['flash_message'] = $this->session->flashdata('message');
		$this->form_validation->set_rules('user', 'Usuario', 'trim|required|valid_email');
		$this->form_validation->set_rules('pass', 'Contraseña', 'trim|required|min_length[4]');
		$user=$this->input->post('user');
		$pass=$this->input->post('pass');

		if ($this->form_validation->run())
		{
			$res = $this->Accesomodel->validar($user,$pass);
			if($res)
			{
				$_SESSION['sfemail'] = $res[0]->sus_email;
				$_SESSION['sfid'] = $res[0]->sus_idUser;
				$_SESSION['sfname'] = $res[0]->sus_name.' '.$res[0]->sus_lastName;
				$_SESSION['sfrol'] = $res[0]->sus_rol;
				$_SESSION['sftype'] = $res[0]->sus_userType;
				$_SESSION['sfworkshop']= $res[0]->wor_city;
				$_SESSION['sfidws']= $res[0]->wor_idWorkshop;

				if($_SESSION['sfemail'] == 'consulta@hondaoptima.com')
				{

				}

				if($_SESSION['sfrol']==2) redirect('estadisticas');
				if($_SESSION['sfrol']==1) redirect('dashboard');
				if($_SESSION['sfrol']==3) redirect('cita');
				if($_SESSION['sfrol']==4 || $_SESSION['sfrol']==5) redirect('pizarronu');
				if($_SESSION['sfrol']==6 || $_SESSION['sfrol']==7) redirect('orden/captura');
			}
			else
			{
				$this->session->set_flashdata('message', "Usuario o Contraseña incorrecto !");
			    redirect('acceso');
			}
		}
		$this->load->view('acceso/login',$data);
	}

	public function recuperar()
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$this->form_validation->set_rules('user', 'Usuario', 'trim|required|valid_email');
		$user=$this->input->post('user');
		if ($this->form_validation->run()){
			$res = $this->Accesomodel->validarUsuario($user);
			if($res){
				//crear contrasenia aleatoria
				$con= $this->Accesomodel->nuevaContrasenia();
				//update contrasenia
				$this->Accesomodel->updateContrasenia($con,$res[0]->sus_idUser);
				//Enviar contrasenia por email
				$this->Accesomodel->enviarContraseniaEmail($res[0]->sus_email,$con);
				//Enviar Mensaje
				$this->session->set_flashdata('message', " Una nueva contraseña fue enviada a su Correo Electrónico!");
				redirect('acceso');
			} else{
				$this->session->set_flashdata('message', " Usuario no Existente!");
			    redirect('acceso/recuperar');
			}
		}
		$this->load->view('acceso/recuperar',$data);
	}

	public function logout()
	{
		$_SESSION['sfemail'];
		$_SESSION['sfid'];
		$_SESSION['sfname'];
		$_SESSION['sfrol'];
		$_SESSION['sftype'];
		$_SESSION['sfworkshop'];
		session_destroy();
		redirect('acceso');
	}
}
