<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizacionesc extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cotizacionesmodel');
		$this->load->model('Reportesmodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Permisomodel');
		$this->load->model('Productividadmodel');
        $this->load->model('Ordendeserviciomodel');
		$this->load->helper('url');
		$this->load->library('pdf');
		$this->load->library('tcpdf');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		//$this->load->helper('url');
		$this->per=$this->Permisomodel->acceso();
		$this->per=$this->Permisomodel->permisosVer('Usuario');
	}

	public function index()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		if(empty($_GET['taller'])) $taller=$_SESSION['sfidws'];
		else $taller=$_GET['taller'];

		if(empty($_GET['fecha'])) $data['fecha'] = date('Y-m-d');
		else $data['fecha'] = $_GET['fecha'];

		if(empty($_GET['asesor']))
		{
			$ase = 0;
			$data['ordenes'] = $this->Cotizacionesmodel->obtener_ordenes_por_fecha($data['fecha'],$taller);
		}
		else
		{
			$ase = $_GET['asesor'];
			$data['ordenes'] = $this->Cotizacionesmodel->obtener_ordenes_por_asesor($ase, $data['fecha'],$taller);
		}

		if($data['ordenes'])
			$data['cotizaciones'] = $this->Cotizacionesmodel->obtener_cotizaciones_por_orden($data['ordenes'][0]->seo_idServiceOrder);
		else
			$data['cotizaciones'] = array();
			
		$data['asesor'] = $ase;
		$data['usuario'] = $this->Usuariomodel->listaUsuarios(0);
		$data['utilizacion'] = $this->Productividadmodel->utilizacion($data['fecha'],$taller);

		$data['taller']=$taller;
		$this->load->view('cotizaciones/vista',$data);
	}

	public function crear($orden)
	{
		$data['flash_message'] = $this->session->flashdata('message');

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('fecha', 'Fecha', 'required');
		$this->form_validation->set_rules('modelo', 'Modelo', 'required');
		$this->form_validation->set_rules('ano', 'A&ntilde;o', 'required');
		$this->form_validation->set_rules('color', 'Color');

        if ($this->form_validation->run())
        {
			$cotizacion = array(
						'est_service_order' => $orden,
						'est_date' => $this->input->post('fecha'),
						'est_customer' => $this->input->post('nombre'),
						'est_vehicle_brand' => $this->input->post('modelo'),
						'est_vehicle_model' => $this->input->post('ano'),
						'est_vehicle_color' => $this->input->post('color'),
						);

		   	$id = $this->Cotizacionesmodel->crear($cotizacion);

			$cantidad = $this->input->post('cantidad');
			$servicio = $this->input->post('servicio');
			$descripcion = $this->input->post('descripcion');
			$refacciones = $this->input->post('refacciones');
			$total = $this->input->post('total');
			$mano_de_obra = $this->input->post('mano_obra');
			if($this->input->post('autorizo')) { $autorizo = $this->input->post('autorizo'); }
			else { $autorizo = array(); }

			if($this->input->post('existencia')) { $existencias = $this->input->post('existencia'); }
			else { $existencias = array(); }

			for($i = 0; $i < 15; $i++)
			{
				if($cantidad[$i] && $servicio[$i])
				{
					if(in_array($i, $autorizo))
						$autorizado = 1;
					else
						$autorizado = 0;
					if(in_array($i, $existencias))
						$existencia = 1;
					else
						$existencia = 0;

					$estimate_service = array(
						'ess_estimate' => $id,
						'ess_service' => $servicio[$i],
						'ess_quantity' => $cantidad[$i],
						'ess_description' => $descripcion[$i],
						'ess_unit_price' => $refacciones[$i],
						'ess_total_price' => $total[$i],
						'ess_handwork' => $mano_de_obra[$i],
						'ess_is_authorized' => $autorizado,
						'ess_is_existencia' => $existencia,
						);

					$this->Cotizacionesmodel->crear_servicio($estimate_service);
				}
			}
			/*
			$pdf = $this->crear_pdf($id);

			$cotizacion = array('est_file' => $pdf);
			$this->Cotizacionesmodel->actualizar($cotizacion, $id);
			*/


			$this->session->set_flashdata('message', "Cotizaci&oacute;n creada exitosamente");

			redirect('cotizaciones');
		}
		else
		{
			$data['info'] = $this->Cotizacionesmodel->obtener_info_cotizacion($orden);
			$data['servicios'] = $this->Cotizacionesmodel->obtener_servicios();
            $this->load->view('cotizaciones/crear', $data);
		}
	}

	public function editar($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['cotizacion'] = $this->Cotizacionesmodel->obtener($id);
		if($data['cotizacion'])
		{
			$data['servicios'] = $this->Cotizacionesmodel->obtener_servicios();
			$data['servicios_cotizacion'] = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
			$data['cantidad_servicios'] = count($data['servicios_cotizacion']);
            $this->load->view('cotizaciones/editar', $data);
		}
		else
		{
			redirect('cotizaciones');
		}
	}

	public function actualizar()
	{
		$data['flash_message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('fecha', 'Fecha', 'required');

        if ($this->form_validation->run())
        {

			$cotizacion_id = $this->input->post('id');
			$id_servicio = $this->input->post('id_servicio');
			$cantidad = $this->input->post('cantidad');
			$servicio = $this->input->post('servicio');
			$descripcion = $this->input->post('descripcion');
			$refacciones = $this->input->post('refacciones');
			$total = $this->input->post('total');
			$mano_de_obra = $this->input->post('mano_obra');
			$exi = $this->input->post('existencia');

			// REGISTRAR PARTES DEL SERVICIO
			$i = 0;
			for($i == 0; $i < 14; $i++)
			{
				$NoParte = split(' ',$descripcion[$i]);
				$piezas = $cantidad[$i];
				$precio = $total[$i];
				$n = count($exi);

				if($i < $n)
				{
					$existencia = $exi[$i];


					if($existencia != '0-1')
					{

						if($_SESSION['sfworkshop'] == 'Tijuana') $taller = 3;
						if($_SESSION['sfworkshop'] == 'Mexicali') $taller = 2;
						if($_SESSION['sfworkshop'] == 'Ensenada') $taller = 1;
						$data= array(
								'hit_IDhits'=>'NULL',
								'hit_mes'=>date('m'),
								'hit_date'=>date('m').date('Y'),
								'hit_taller'=>$taller,
								'hit_noparte'=>$NoParte[0],
								'hit_piezas'=>$piezas,
								'hit_costo'=>$precio,
								'hit_almacenable'=>0,
								'hit_hora'=>date('Y-m-d H:i:s'),
								);
						$this->Reportesmodel->insertHits($data);
					}
				}
			} // <-----

			if($this->input->post('autorizo')) { $autorizo = $this->input->post('autorizo'); }
			else { $autorizo = array(); }

			if($this->input->post('existencia')) { $existencias = $this->input->post('existencia'); }
			else { $existencias = array(); }



			$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($cotizacion_id);
			$cantidad_servicios = count($servicios_cotizacion);

			for($i = 0; $i < $cantidad_servicios; $i++)
			{

				if(in_array($i, $autorizo))
					$autorizado = 1;
				else
					$autorizado = 0;
				if(in_array($i, $existencias))
					$existencia = 1;
				else
					$existencia = 0;

				$estimate_service = array(
					'ess_service' => $servicio[$i],
					'ess_quantity' => $cantidad[$i],
					'ess_description' => $descripcion[$i],
					'ess_unit_price' => $refacciones[$i],
					'ess_total_price' => $total[$i],
					'ess_handwork' => $mano_de_obra[$i],
					'ess_is_authorized' => $autorizado,
					'ess_is_existencia' => $existencia,
					);

				$this->Cotizacionesmodel->actualizar_servicio($estimate_service, $id_servicio[$i]);
			}
			for($i = $cantidad_servicios; $i < 15; $i++)
			{
				if($cantidad[$i] && $servicio[$i])
				{

					if(in_array($i, $autorizo))
						$autorizado = 1;
					else
						$autorizado = 0;
					if(in_array($i, $existencias))
						$existencia = 1;
					else
						$existencia = 0;

					$estimate_service = array(
						'ess_estimate' => $cotizacion_id,
						'ess_service' => $servicio[$i],
						'ess_quantity' => $cantidad[$i],
						'ess_description' => $descripcion[$i],
						'ess_unit_price' => $refacciones[$i],
						'ess_total_price' => $total[$i],
						'ess_handwork' => $mano_de_obra[$i],
						'ess_is_authorized' => $autorizado,
						'ess_is_existencia' => $existencia,
						);

					$this->Cotizacionesmodel->crear_servicio($estimate_service);
				}
			}
			/*
			$cotizacion_actualizada = $this->Cotizacionesmodel->obtener($cotizacion_id);

			if (file_exists($cotizacion_actualizada[0]->est_file) && is_dir($cotizacion_actualizada[0]->est_file))
			{
				unlink($cotizacion_actualizada[0]->est_file);
			}

			$pdf = $this->crear_pdf($cotizacion_id);

			$cotizacion = array('est_file' => $pdf);
			$this->Cotizacionesmodel->actualizar($cotizacion, $cotizacion_id);
			*/

			$this->session->set_flashdata('message', " ");

			redirect('cotizaciones');
		}
		else
		{
			$cotizacion_id = $this->input->post('id');
			$data['flash_message'] = $this->session->flashdata('message');
			$data['cotizacion'] = $this->Cotizacionesmodel->obtener($cotizacion_id);
			if($data['cotizacion'])
			{
				$data['servicios'] = $this->Cotizacionesmodel->obtener_servicios();
				$this->load->view('cotizaciones/editar', $data);
			}
			else
			{
				redirect('cotizaciones');
			}
		}
	}

	public function borrar($id)
	{
		$data['flash_message'] = $this->session->flashdata('message');
		$data['cotizacion'] = $this->Cotizacionesmodel->obtener($id);
		if($data['cotizacion'])
		{
			$this->Cotizacionesmodel->borrar($id);
		}
		redirect('cotizaciones');
	}

	public function desconfirmar($id)
	{
		$this->Cotizacionesmodel->desconfirmar($id);
		redirect_back();
	}

	public function confirmar($id)
	{
		$this->Cotizacionesmodel->confirmar($id);
		redirect_back();
	}

	function enviar_documento($id, $publicity)
	{//aqui
        $info = $orden = $this->Ordendeserviciomodel->getOrden($id);
		$servicios=$this->Ordendeserviciomodel->getOrdenServicios($id);
		$serviciossv=$this->Ordendeserviciomodel->getOrdenServiciossv($id);
		$tecnico=$this->Ordendeserviciomodel->getTecnico($info[0]->seo_technicianTeam);

        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>Orden de Servicio</title>
<style>

.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:15px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}

.contact {
	width:70%;

	float:left;
}
.logo {
	width:100%;
	height:70px;

	background-size: 80%;
	background-repeat:no-repeat;
}
.head {
	width:100%;
	height:180px;
}
.title {
	color:rgb(226,0,43);
}
.order-date .gray {
	background-color:rgb(102,102,102);
	color:rgb(255,255,255);
	font-size:12px;
	font-weight:900;
}h
.order-date .white {
	height:27px;
	font-size:8.4px;
	font-weight:900;
	vertical-align:text-top;
	text-align:left;
}
.order-date td {
	border:1px solid rgb(102,102,102);
}
.message {
	width:60%;
	height:70px;
	float:left;
}
.message p {
	width:98%;
	margin:0;
	margin-top:10px;
	padding:5px;
	border:0px solid rgb(51,51,51);
	border-radius:15px;
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:11px;
	text-align:justify;
	line-height:170%;
}
.customer-info {
    margin-right:2px;
	width:50%;
	float:left;
    border-radius:5px;
	font-size:16px;
}
.customer-info table {
	width:100%;
	margin:auto;
	line-height:150%;
}
.customer-info table th {
	border-bottom:1px solid rgb(0,0,0);
}
.customer-info table td {
	padding-left:5px;
}
.vehicle-info {
	width:50%;
	float:left;
	font-size:16px;
    border-radius:5px;
}
.vehicle-info table {
	width:100%;
	margin-left:5px;
	line-height:150%;
    border:1;
}
.info {
	width:100%;
	height:120px;
	font-size:16px;
}
.vehicle-info table th {
	border-bottom:1px solid rgb(51,51,51);
}
.vehicle-info table td {
	padding-left:5px;
}
.reception-info {
	width:45%;
	float:left;
	font-size:16px;
}
.reception-info table {
	width:100%;
	height:120px;
	margin:auto;
	line-height:150%;
}
.reception-info table td {
	padding-left:5px;
}
.confirmation {
	height:135px;
	width:49%;
	float:left;
	font-size:16px;
}
.confirmation p {

}
.right {
	text-align:center;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.first-cell {
	border-right:1px solid rgb(226,0,43);
}
.row {
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child {
	margin-bottom:0px;
}
.services {
	font-size:16px;
	border:1px solid rgb(226,0,43);
	border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}
.services-head {
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:16px;
	margin-bottom:10px;
}
.services-head th {
	height:19px;
}
.tecs {
	width:82%;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.tecs table {
	width:100%;
	margin:auto;
}
.tecs table td {
	height:25px;
	padding-left:5px;
	vertical-align:text-top;
	border-bottom:1px solid rgb(0,0,0);
	border-right:1px solid rgb(0,0,0);
}
.tecs table td:last-child {
	border-right:none;
}
.floated {
	float:left;
}
.radius {
	border: 1px solid rgb(51,51,51);
	border-spacing:0;
	overflow:hidden;
}
.itemsb {
	width:80%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:14px;
}
.itemsb table {
	width:100%;
	margin:auto;
	border:none;
}
.items {
	width:100%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.items table {
	width:95%;
	margin:auto;
	border:none;
}
.comments {
	width:100%;
	height:90px;
	float:left;
	font-size:16px;
	text-align:justify;
	vertical-align:top;
}
.comments .text {
	width:97%;
	height:100%;
	margin:auto;
}
.text .title {
	font-size:12px;
	font-weight:bold;
	margin-left:5px;
}
.folio {
	width:30%;
	float:left;
}
</style>
</head>

<body id="body">
<div class="page">
  <div class="row head">
    <div class="contact">
      <img src="'.base_url().'documents/hlogo.png" width="350px">
      <div>
       <div  style=" padding-left:45px; margin-bottom:10px; margin-top:-5px; font-size:16px; "><div style="font-weight:bold;" >OPTIMA AUTOMOTRIZ S.A. DE C.V.</div>
       <div style=" padding-left:40px;">RFC: OAU990325G88</div></div>
        <table cellspacing="0"  class="address" style="width:950px;">
          <tr>
            <th width="22%" class="title right">TIJUANA B.C.:</th>
            <th width="28%" class="title right">MEXICALI B.C.:</th>
            <th width="28%" class="title right">ENSENADA B.C.:</th>
            <th width="22%" class="title">HORARIO DE ATENCI&Oacute;N<th>
          </tr>
          <tr>
            <td class="right">Av. Padre Kino 4300 Zona Río,<br> CP. 22320,<br />
              Tel. (664) 900-9010</td>
            <td class="right">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
              Tel. (686) 900-9010</td>
			<td class="right" style="text-align:center">Av. Balboa 146 Esq.<br />
              López Mateos Fracc. Granados. CP.  228402,<br />
              Tel. (646) 900-9010
			</td>
			<td style="text-align:center">Lunes a Viernes de 8:00 A.M. a 6:00 P.M.<br />
              Sábado de 8:00 A.M. a 1:30 P.M.
			</td>
			<td></td>

          </tr>

        </table>


      </div>
    </div>
    <div class="folio">

      <div class="info">
        <div class="message">
          <p  style="background-color:white"></p>
        </div>
        <div style="width:45%" class="service-order">
          <table class="order-date radius">
            <tr class="gray">
              <td>ORDEN DE SERVICIO</td>
            </tr>
            <tr class="white" style="text-align:center; height:15px;">
              <td>';if($orden[0]->seo_serviceOrder==''){
                  $html.=$orden[0]->seo_tower;
              }else{
                  $html.=$orden[0]->seo_serviceOrder;
              }
        $html.='</td>
            </tr>
            <tr class="gray">
              <td>FECHA</td>
            </tr>
            <tr class="white" style="text-align:center;height:15px;" >
              <td>';list($fec,$hor)=explode(" ",$orden[0]->seo_date); $html.= $fec.'</td>
            </tr>
             <tr class="gray">
              <td>LOCALIDAD</td>
            </tr>
            <tr class="white" style="text-align:center; height:15px;" >
              <td>'; $html.=$_SESSION['sfworkshop'].'</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin-bottom: 2px;   margin-top: 5px;">
    <div class="customer-info" style="margin-top:12px;padding-top:10px !important;">
      <table class="radius" style="border-radius:5px;">
        <tr class="first-row">
          <th colspan="3" class="title">DATOS DEL CLIENTE (CONSUMIDOR)</th>
        </tr>
        <tr>
          <td colspan="3" ><b>Nombre:</b>'; $html.=substr(ucwords(strtolower($orden[0]->sec_nombre)),0,50).'</td>
        </tr>
        <tr>
          <td colspan="3"   ><b>Dirección: </b>';$html.=substr(ucwords(strtolower($orden[0]->sec_calle)),0,23).''.ucwords(strtolower($orden[0]->sec_num_ext)).'</td>
        </tr>
        <tr>
         <td style=" font-size:14px;"><b>Ciudad:</b>'.ucwords(strtolower($orden[0]->sec_ciudad)).'</td>
        <td style=" font-size:14px;" ><b>Estado:</b>'.ucwords(strtolower($orden[0]->sec_estado)).'</td>
        <td style=" font-size:14px;"><b>CP:</b>'.ucwords(strtolower($orden[0]->sec_cp)).'</td>
        </tr>
        <tr  >
          <td  class="col-5" style="font-size:15px"><b>R.F.C.:</b>'.strtoupper($orden[0]->sec_rfc).'
</td>

        </tr>
        <tr>
            <td class="col-7" colspan="2" style="font-size:14px"><b>Email:</b>'.$eema=$orden[0]->sec_email.';'; list($email,$emailb)=explode(';',$eema);
		   substr(ucwords(strtolower($email)),0,25); $html.='</td>
        </tr>
        <tr>
          <td colspan="3"  ><b>Teléfono:</b>'.ucwords(strtolower($orden[0]->sec_tel)).'</td>

        </tr>
        <tr>
          <td  colspan="3" ><b>Persona autorizada para recoger la unidad:</b> &nbsp;</td>

        </tr>
        <tr>
          <td  colspan="3" ><b>Nombre:</b>'.substr(ucwords(strtolower($orden[0]->seo_persona_recoge)),0,50).'</td>

        </tr>
        <tr>
          <td colspan="3" ><b>Teléfono:</b>'.ucwords(strtolower($orden[0]->seo_telefono_recoge)).'</td>
        </tr>
      </table>
    </div>
    <div class="vehicle-info" style="margin-top:12px;">
      <table style="height:300px !important;" class="radius">
        <tr class="first-row">
          <th colspan="4" class="title">DATOS DEL VEHÍCULO</th>
        </tr>

        <tr height="36px;">
          <td ><b>Marca:</b>'.ucwords(strtolower($orden[0]->sev_marca)).'</td>
          <td ><b>Sub Marca:</b>'.ucwords(strtolower($orden[0]->sev_sub_marca)).'</td>
        </tr>
        <tr style="height:36px;">
          <td ><b>Año-Modelo:</b>'.ucwords(strtolower($orden[0]->sev_modelo)).'</td>
          <td ><b>Placas:</b>'.strtoupper($orden[0]->sev_placas).'</td>
        </tr>
        <tr style="height:36px;">
          <td ><b>Fecha de Venta:</b>'.$orden[0]->sec_venta.'</td>

          <td ><b>Último Servicio:</b>'.$orden[0]->sec_ultimo_servicio.'</td>

        </tr>
        <tr style="height:36px;">
          <td class="col-6"><b>Color:</b>'.ucwords(strtolower($orden[0]->sev_color)).'</td>
          <td class="col-6"><b>Seguro:</b> &nbsp;</td>
        </tr>
        <tr style="height:36px;">
          <td  class="col-6"><b>No. de  VIN:</b>'.strtoupper ($orden[0]->sev_vin).'</td>
          <td class="col-6"><b>Km. de Entrada:</b>'.ucwords(strtolower($orden[0]->sev_km_recepcion)).'</td>
        </tr>
        <tr style="margin-bottom:10px;">
         <td  class="col-6"><b>No. de  Motor:</b> &nbsp;</td>
          <td class="col-6"><b>Capacidad:</b>'.ucwords(strtolower($orden[0]->sev_capacidad)).'

          </td>

        </tr>



      </table>
    </div>
  </div>
  <div class="row" style="margin-bottom: 2px;" >
    <div class="reception-info">
      <table  class="radius" width="100%">

        <tr>
          <td colspan="2"><b>Asesor:</b>'.$orden[0]->sus_name.' '.$orden[0]->sus_lastName.'</td>
        </tr>
        <tr>
           <td colspan="2"><b>Técnico:</b>'.$tecnico[0]->sus_name.' '.$tecnico[0]->sus_lastName.'</td>
        </tr>
        <tr>
          <td colspan="2"><b>Fecha y hora de recepción:</b>'.ucwords(strtolower($orden[0]->seo_timeReceived)).'</td>
        </tr>
        <tr>
          <td colspan="2" ><b>Fecha de entrega del vehículo:</b>'.date('Y-m-d').'</td>

        </tr>
        <tr height="27px;">
          <td  style=" padding-top:4px; text-align:center; font-weight:bold;background-color:black; color:white" class="col-5" >NUMERO DE TORRE: </td>
          <td width="27px;" style="font-size:22px; font-weight:bold;  border:1px solid white">'.$orden[0]->seo_tower.'</td>
        </tr>
        <tr>
          <td colspan="2" ><b>            -</b></td>
        </tr>
      </table>
    </div>
    <div class="confirmation radius" style="margin-left:5px !important; padding-left:5px; font-weight:bold; color:black; font-size:13px;">Se entregan las partes o refacciones reemplazadas al consumidor SI &nbsp;&nbsp;('; if($orden[0]->seo_se_entregan==1){ $html.='&radic;';} $html.=' ) &nbsp;&nbsp;&nbsp;&nbsp; NO &nbsp;&nbsp;( '; if($orden[0]->seo_se_entregan==0){ $html.='&radic;';}$html.=')
      <br>
      <b>NOTA:</b> Las partes y/o refacciones no se entregar&aacute;n al consumidor cuando:<br>
      a) Sean cambiadas por garantía<br>
      b) Se trate de residuos peligrosos deacuerdo con las disposiciones legales aplicables.<br>

      Servicio  en el domicilio del consumidor &nbsp;(';if($orden[0]->seo_ser_domi==1){ $html.='&radic;';} $html.=') &nbsp; SI  &nbsp;( ';if($orden[0]->seo_ser_domi==0){ $html.='&radic;';}$html.=') &nbsp; NO<br>

      Poliza de seguros para cubrir al consumidor los daños o extravios de <br> bienes: SI (';
        if($orden[0]->seo_poliza==1){
            $html.= '&radic;';
        }$html.=' ) Numero:';
        if($orden[0]->seo_poliza==''){
            $html.= '______________';
        }else{
            $html.=$orden[0]->seo_poliza;
        }
        $html.= ' ( ';
        if($orden[0]->seo_poliza==0){
            $html.='&radic;';
        }
        $html.=' )NO

    </div>
  </div>
  <div class="row">
    <div class="floated">
      <div style="margin-bottom: 73px;">
        <div class="text radius" style=" height:50px; padding-left:15px;"> <span class="title">COMENTARIOS:</span> <br>'.$orden[0]->seo_comments.'
          <div style="font-size:16px; ">
           </div></div>
      </div>

       <div class="itemsb"  style="margin-top:-70px;">
       <div style="font-weight:bold; width:100%; text-align:center; color:rgb(226,0,43);">INVENTARIO DEL VEHICULO</div>
        <table >
          <tr style="height:10px">
            <td width="15px;" >';if($orden[0]->seo_tapetes==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">TAPETES</td>
            <td width="15px;" style="text-align:right">';if($orden[0]->seo_tapones==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">TAPONES</td>
            <td width="15px;" style="text-align:right">';if($orden[0]->seo_herramienta==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">HERRAMIENTA</td>
             <td width="15px;" style="text-align:right">'; if($orden[0]->seo_reflejantes==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">REFLEJANTES</td>
             <td width="15px;" style="text-align:right">'; if($orden[0]->seo_ecualizador==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ECUALIZADOR</td>
              <td width="15px;" style="text-align:right">'; if($orden[0]->seo_tapon==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">TAPÓN GAS</td>
                <td width="15px;" style="text-align:right">'; if($orden[0]->seo_ventanas==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">VENTANAS</td>
          </tr>
          <tr style="height:10px">
            <td>'; if($orden[0]->seo_espejo==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ESPEJO</td>
               <td width="15px;" style="text-align:right">'; if($orden[0]->seo_radio==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">RADIO</td>
            <td width="15px;" style="text-align:right">'; if($orden[0]->seo_llantarefaccion==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">LLANTA REF.</td>
               <td width="15px;" style="text-align:right">'; if($orden[0]->seo_extinguidor==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">EXTINGUIDOR</td>
          <td width="15px;" style="text-align:right">';if($orden[0]->seo_gato==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">GATO</td>
              <td width="15px;" style="text-align:right">';if($orden[0]->seo_carnet==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">CARNET</td>
                 <td width="15px;" style="text-align:right">'; if($orden[0]->seo_vestidura==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">VESTIDURA</td>
          </tr>
          <tr style="height:10px">
               <td>'; if($orden[0]->seo_antena==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ANTENA</td>
                 <td width="15px;" style="text-align:right">'; if($orden[0]->seo_encendedor==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ENCENDEDOR</td>
           <td width="15px;" style="text-align:right">'; if($orden[0]->seo_limpiadores==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">LIMPIADORES</td>
                    <td width="15px;" style="text-align:right">'; if($orden[0]->seo_cables==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">CABLES P/C</td>
                   <td width="15px;" style="text-align:right">'; if($orden[0]->seo_estereo==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ESTEREO</td>
                      <td width="15px;" style="text-align:right">'; if($orden[0]->seo_carroceria==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">CARROCERÍA</td>
                     <td width="15px;" style="text-align:right">'; if($orden[0]->seo_parabrisas==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">PARABRISAS</td>
          </tr>
          <tr >
            <td height="18px;"></td>
            <td ></td>
            <td ></td>
            <td></td>
            <td></td> <td colspan="4" style="font-weight:bold; font-family:14px; background-color:black; color:white; text-align:center" > CANTIDAD DE COMBUSTIBLE: </td>
            <td style="background-color:black; color:white; text-align:center; font-weight:bold">'.$orden[0]->seo_combustible.'</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
      <div class="tecs" style=" padding-right:30px; margin-top:8px;">
        <table style="text-align:center; margin-top:30px;margin-bottom:90px; font-weight:bold; font-size:16px;" class="radius" >
          <tr>
            <td valign="middle" style="padding-top:3px;">Hora de Recepción</td>
            <td>Hora de Inicio de Trabajo</td>
            <td>Hora de Trabajo Terminado</td>
          </tr>
          <tr>
            <td>'; list($fech,$hora)=explode(" ",$orden[0]->seo_timeReceived); $html.=ucwords(strtolower($hora));$html.='
             </td>
            <td></td>
            <td>'.ucwords(strtolower($orden[0]->seo_promisedTime)).'</td>
          </tr>
        </table>
      </div>
    <div class=" floated" style="margin-top:-73px; padding-right:10px;">

     <img src="'.base_url().'signature/'; if(empty($orden[0]->sei_cha_uno))$html.= 'chasis.png'; else $html.= $orden[0]->sei_cha_uno; $html.='" height="280px;" width="143px;">
   <table style="margin-top:-175px;" width="130px" height="150px;">
<tr>
<td width="20px;" align="center">'; if($orden[0]->sei_cha_uno=='ok'){$html.= ' ';}$html.='</td>
<td width="95px;"></td>
<td>'; if($orden[0]->sei_cha_dos=='ok'){$html.= '';}$html.='</td>
</tr>
<tr height="43px;">
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td width="20px;" align="center">'; if($orden[0]->sei_cha_tres=='ok'){$html.= '';}$html.='</td>
<td></td>
<td>'; if($orden[0]->sei_cha_cuatro=='ok'){$html.= '';}$html.='</td>
</tr>

</table>
    </div>
       <div  class="comments" style="margin-top:-50px; height: 85px; width:1022px; padding-left:5px; margin-left:-10px;">
        <div class="text radius" style=" width:1022px; height:90px; margin-top:-20px; ">
          <table width="1024px" style="text-align:center; " cellspacing="0"  border="0">
  <tr>
    <td width="30%" style=" border-bottom:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">OPERACIONES A EFECTUAR</td>
    <td width="30%" style=" border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PARTES Y/O REFACCIONES</td>
    <td width="30%" style="  border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PRECIOS UNITARIOS</td>
    <td width="10%" style=" border-bottom:1px solid #000; border-top:1px solid #000;"></td>
  </tr>

  ';
  setlocale(LC_MONETARY, 'en_US');
  $num=count($servicios);
  $numsv=count($serviciossv);
  $limite=11 - $num - $numsv;
  $monto=0;
  $tiposer='';
  foreach($servicios as $ser){


	 if($ser->ser_tipo=='pro'){
	  $comercial=$ser->ser_comercial / 1.16;
	 }else{
	  $comercial=$ser->ser_comercial;
	 }
	 $tiposer=$ser->ssr_venta;

	  $monto+=$comercial;


	$html.='
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$ser->set_name.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($comercial, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';
	  }


	  foreach($serviciossv as $sersv){





	$html.= '
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$sersv->sev_nombre.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format(0, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';
	  }


  for($x=0; $x<$limite; $x++) { $html.='
  <tr>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style="border-bottom:1px solid #999; text-align:left; ">$</td>
  </tr>';
 }



  $html.='<tr>
    <td width="30%" style="border-right:1px solid #000;">&nbsp;</td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;">MONTO DE LA OPERACI&Oacute;N</td>
    <td width="10%" style=" text-align:left;border-top:1px solid #000;">'; $html.= '<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($monto, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>';
	$html.='</td>
  </tr>
	</table>
    	</div>
      	</div>

      <div class="comments" style="margin-top:0;  width:1020px; margin-left:-10px;">
          <div class="text " style=" height:90px; ">
          <table width="100%">
          <tr>
          <td width="15%">FORMA DE PAGO</td>
          <td width="50%">EFECTIVO ( ';
		  if($orden[0]->seo_pago==1){$html.= '&radic;';}$html.=') CHEQUE (';
		  if($orden[0]->seo_pago==2){$html.= '&radic;';}$html.=') TARJETA DE CREDITO (';
		  if($orden[0]->seo_pago==3){$html.= '&radic;';}$html.=') OTRO (';
		  if($orden[0]->seo_pago==4){$html.= '&radic;';}$html.=' )</td>
          <td width="25" style=" text-align:right">SUBTOTAL</td>
          <td width="10%">$ '.number_format($monto, 2, '.', '').'</td>
          </tr>
          <tr>
          <td width="15%"></td>
          <td width="50%"></td>
          <td width="25" style=" text-align:right">IVA</td>
          <td width="10%">
          '; if($tiposer=='interna'){$iva=0;}
	else{$iva=0.16;}$html.='
          $'.number_format($monto * $iva, 2, '.', '').'</td>
          </tr>
          <tr>
          <td width="15%"></td>
          <td width="50%"></td>
          <td width="25" style=" text-align:right"> TOTAL</td>
          <td width="10%">
          '; if($tiposer=='interna'){$miva=0;}
	else{$miva=1.16;}$html.='
          $'.number_format($monto * $miva, 2, '.', '').'</td>
          </tr>
          <tr>
          <td width="15%">&nbsp;</td>
          <td width="50%"></td>
          <td width="25" style=" text-align:right"></td>
          <td width="10%"></td>
          </tr>
          <tr>
          <td colspan="4" style="font-size:13px; font-weight:bold" width="100%; ">
          *EL TOTAL NO INCLUYE SERVICIOS ADICIONALES. EN CASO DE SER NECESARIOS, SE SOLICITARA AUTORIZACION AL CLIENTE VIA TELEFONICA
          </td>

          </tr>
          </table>

          <!--<table width="100%" style="text-align:center; font-size:14px;"  cellspacing="0"  border="0">
  <tr>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">FORMA DE PAGO</td>
    <td width="10%" style="border-right:1px solid #000; text-align:left; border-top:1px solid #000; text-decoration:underline;"></td>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">SERVICIOS ADICIONALES:</td>
    <td width="10%" style=" border-top:1px solid #000;"></td>
  </tr>

 <tr>
    <td  style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">Monto de la operaci&oacute;n:

    </td>
    <td style="border-right:1px solid #000; text-align:left; ">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" "></td>
  </tr>
  <tr>
    <td style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Otros cargos:
    </td>
    <td  style="border-right:1px solid #000;text-align:left;">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Total refacciones y servicios adicionales</td>
    <td  style=" text-align:left">$</td>
  </tr>
<tr>
    <td style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">
    Servicios Adicionales:

    </td>
    <td  style="border-right:1px solid #000; text-align:left;">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" border-top:1px solid #000;"></td>
  </tr>
  <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Parcial:

    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Fecha y monto de anticipo:</td>
    <td  style="text-align:left" >$</td>
  </tr>
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Impuesto al Valor Agregado:

    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">El resto del monto total de la operaci&oacute;n. Se liquidara en</td>
    <td  style="border-top:1px solid #000;  text-align:left"></td>
  </tr>

   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Monto Total (Incluye mano de obra):

    </td>
    <td  style="border-right:1px solid #000;text-align:left; border-bottom:1px solid #000; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">  la fecha programada para la entrega del veh&iacute;culo</td>
    <td  style=" "></td>
  </tr>

   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Efectivo ( &nbsp; ) Cheque ( ) tarjeta de cr&eacute;dito ( ) Otro ( )

    </td>
    <td style="border-right:1px solid #000;text-align:left;  ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>

   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">


    </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>








</table>    -->
          </div>
      </div>


     <div style="margin-bottom: 32px;">
        <div class="text radius" style=" height:25px;  padding-left:15px;"> <span class="title">POSIBLES CONSECUENCIAS:</span> <br>
          <div style="font-size:16px; ">
           </div></div>
      </div>

    </div>




  </div>


  <div class="row" style="margin-top:-30px;">

    <b style=" font-size:16px; margin-left:5px; margin-top:-26px; ">Imagenes del Vehículo ( Condiciones de Recepción )</b> <br>

     <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Costado Derecho</th>
        <th width="20%" >Costado Izquierdo</th>
        <th width="20%" >Parte Frontal</th>
        <th width="20%" >Parte Trasera</th>
        <th width="20%" >Tablero</th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td>';
		$ider=$orden[0]->sei_inf_der;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_inf_izq;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_pos_der;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			};$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_pos_izq;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_tablero;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
      </tr>
    </table>

    <b style=" font-size:16px;">Imagenes Extras</b> <br>

    <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Extra 1</th>
        <th width="20%" >Extra 2</th>
        <th width="20%" >Extra 3</th>
        <th width="40%" ></th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td style="width:20%">';
		$ider=$orden[0]->sei_extra_uno;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='
            <a href="'.$url.'/'.$ider.'" target="_blank">
          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td style="width:20%">';
		$ider=$orden[0]->sei_extra_dos;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='
            <a href="'.$url.'/'.$ider.'" target="">
          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td style="width:20%">';
		$ider=$orden[0]->sei_extra_tres;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='
            <a href="'.$url.'/'.$ider.'" target="_blank">
          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td width="40%"></td>
      </tr>
    </table>
  </div>
</div>



</body>
</html>
';


        $this->load->library('mpdf');
		$this->mpdf->writeHTML($html);

        $this->mpdf->SetImportUse();
        $pagecount = $this->mpdf->SetSourceFile('documents/cotizaciones/docs/adicionales.pdf');
            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $this->mpdf->ImportPage($i);
                $this->mpdf->UseTemplate($import_page);

                if ($i < $pagecount)
                {$this->mpdf->AddPage();

                }
                if($i == 1){
                    $html3='';
                   for($ii = 0;$ii<33; $ii++){
                        $html3.='<br>';
                    }
                    $html3 .= '<img width="50px" height="25px" style="margin-left:20%" src="firmas/'.$orden[0]->sec_firma.'"/>';
                    for($ii = 0;$ii<9; $ii++){
                        $html3.='<br>';
                    }
                    $html3.='
                    <div width="45%" style="float:left">
                    <img width="110px" height="60px" style="margin-left:12%" src="firmas/asesores/'.$orden[0]->sus_firma.'"/>
                    </div>
                    <div width="45%" style="float:left">
                    <img width="110px" height="60px" style="float:right; margin-left:70%" src="firmas/'.$orden[0]->sec_firma.'"/>
                    </div>';
                    $this->mpdf->writeHTML($html3);
                }

            }

        $html2 = '';
        for($ii = 0;$ii<42; $ii++)
		{
            $html2.='<br>';
        }

        $html2.='<img width="110px" height="65px" style="margin-left:40%" src="firmas/'.$orden[0]->sec_firma.'"/>';
        $this->mpdf->writeHTML($html2);

		$new_name = 'documents/cotizaciones/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output($new_name, 'F');

        $this->email->from('avisodeprivacidad@hondaoptima.com', 'Honda Optima');
        //$this->email->to($eema);
        $this->email->to("victor.em.017@gmail.com");
        //$this->email->cc('');
        $this->email->subject('Aviso De Privacidad | Ejemplo');
        $this->email->message('');
        $this->email->attach($new_name);


        if($publicity=='false'){
            $this->Ordendeserviciomodel->InsertMailWithoutPublicity($eema);
        }

        /*if ($this->email->send()) echo 'success';
		else echo 'error----'.$eema.'----'.$orden[0]->sus_firma;*/
		echo $new_name;
	}

	function enviar_documento_reload($IDO)
	{
		//52175
		$id = $IDO;
		$info = $orden = $data['orden'] = $this->Ordendeserviciomodel->getOrden($id);
		$data['servicios'] = $servicios=$this->Ordendeserviciomodel->getOrdenServicios($id);
		$data['serviciossv'] = $serviciossv = $this->Ordendeserviciomodel->getOrdenServiciossv($id);
		$data['tecnico'] = $tecnico = $this->Ordendeserviciomodel->getTecnico($info[0]->seo_technicianTeam);
		$eema = $orden[0]->sec_email;

		$html = $msg = $this->load->view('orden/enviar_orden_pdf', $data, true);
		$this->load->library('mpdf');
		$this->mpdf = new mPDF('utf-8', 'LETTER');
		$this->mpdf->SetDisplayMode('fullpage');
		$this->mpdf->list_indent_first_level = 0;
		$this->mpdf->writeHTML($html);
        $this->mpdf->SetImportUse();
        $pagecount = $this->mpdf->SetSourceFile('documents/cotizaciones/docs/adicionales.pdf');


		for ($i=1; $i<=$pagecount; $i++)
		{
			$import_page = $this->mpdf->ImportPage($i);
			$this->mpdf->UseTemplate($import_page);

			if($i < $pagecount)
				$this->mpdf->AddPage();

			if($i == 1)
			{
				$html3='';
			   	for($ii = 0;$ii<33; $ii++)
					$html3.='<br>';

				$html3 .= '<img width="50px" height="25px" style="margin-left:20%" src="firmas/'.$orden[0]->sec_firma.'"/>';
				for($ii = 0;$ii<9; $ii++)
					$html3.='<br>';

				$html3.='
				<div width="45%" style="float:left">
				<img width="110px" height="60px" style="margin-left:12%" src="firmas/asesores/'.$orden[0]->sus_firma.'"/>
				</div>
				<div width="45%" style="float:left">
				<img width="110px" height="60px" style="float:right; margin-left:70%" src="firmas/'.$orden[0]->sec_firma.'"/>
				</div>';
				$this->mpdf->writeHTML($html3);
			}
		}

		$html2 = '';
		for($ii = 0;$ii<42; $ii++)
			$html2.='<br>';

		$html2.='<img width="110px" height="65px" style="margin-left:40%" src="firmas/'.$orden[0]->sec_firma.'"/>';
		$this->mpdf->writeHTML($html2);

		$new_name = 'documents/cotizaciones/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output($new_name, 'F');
        $this->email->from('avisodeprivacidad@hondaoptima.com', 'Honda Optima');
		$this->email->to($eema);//aqui
        $this->email->subject('Aviso De Privacidad');
        $this->email->message('');
        $this->email->attach($new_name);

        if ($this->email->send()) { echo 'success<br>'; echo '<b><a target="_blank" href="'.base_url().$new_name.'" >Ver PDF</a>';}
		else echo 'error----'.$eema.'----'.$orden[0]->sus_firma;
	}

    function enviar_documentoTest($id, $publicity)
	{
        $info = $orden=$this->Ordendeserviciomodel->getOrden($id);
		$servicios=$this->Ordendeserviciomodel->getOrdenServicios($id);
		$serviciossv=$this->Ordendeserviciomodel->getOrdenServiciossv($id);
		$tecnico=$this->Ordendeserviciomodel->getTecnico($info[0]->seo_technicianTeam);

        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>Orden de Servicio</title>
<style>

.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:15px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}

.contact {
	width:70%;

	float:left;
}
.logo {
	width:100%;
	height:70px;

	background-size: 80%;
	background-repeat:no-repeat;
}
.head {
	width:100%;
	height:180px;
}
.title {
	color:rgb(226,0,43);
}
.order-date .gray {
	background-color:rgb(102,102,102);
	color:rgb(255,255,255);
	font-size:12px;
	font-weight:900;
}h
.order-date .white {
	height:27px;
	font-size:8.4px;
	font-weight:900;
	vertical-align:text-top;
	text-align:left;
}
.order-date td {
	border:1px solid rgb(102,102,102);
}
.message {
	width:60%;
	height:70px;
	float:left;
}
.message p {
	width:98%;
	margin:0;
	margin-top:10px;
	padding:5px;
	border:0px solid rgb(51,51,51);
	border-radius:15px;
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:11px;
	text-align:justify;
	line-height:170%;
}
.customer-info {
    margin-right:2px;
	width:50%;
	float:left;
    border-radius:5px;
	font-size:16px;
}
.customer-info table {
	width:100%;
	margin:auto;
	line-height:150%;
}
.customer-info table th {
	border-bottom:1px solid rgb(0,0,0);
}
.customer-info table td {
	padding-left:5px;
}
.vehicle-info {
	width:50%;
	float:left;
	font-size:16px;
    border-radius:5px;
}
.vehicle-info table {
	width:100%;
	margin-left:5px;
	line-height:150%;
    border:1;
}
.info {
	width:100%;
	height:120px;
	font-size:16px;
}
.vehicle-info table th {
	border-bottom:1px solid rgb(51,51,51);
}
.vehicle-info table td {
	padding-left:5px;
}
.reception-info {
	width:45%;
	float:left;
	font-size:16px;
}
.reception-info table {
	width:100%;
	height:120px;
	margin:auto;
	line-height:150%;
}
.reception-info table td {
	padding-left:5px;
}
.confirmation {
	height:135px;
	width:49%;
	float:left;
	font-size:16px;
}
.confirmation p {

}
.right {
	text-align:center;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.first-cell {
	border-right:1px solid rgb(226,0,43);
}
.row {
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child {
	margin-bottom:0px;
}
.services {
	font-size:16px;
	border:1px solid rgb(226,0,43);
	border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}
.services-head {
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:16px;
	margin-bottom:10px;
}
.services-head th {
	height:19px;
}
.tecs {
	width:82%;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.tecs table {
	width:100%;
	margin:auto;
}
.tecs table td {
	height:25px;
	padding-left:5px;
	vertical-align:text-top;
	border-bottom:1px solid rgb(0,0,0);
	border-right:1px solid rgb(0,0,0);
}
.tecs table td:last-child {
	border-right:none;
}
.floated {
	float:left;
}
.radius {
	border: 1px solid rgb(51,51,51);
	border-spacing:0;
	overflow:hidden;
}
.itemsb {
	width:80%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:14px;
}
.itemsb table {
	width:100%;
	margin:auto;
	border:none;
}
.items {
	width:100%;
	height:65px;
	margin-top:10px;
	float:left;
	font-size:16px;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.items table {
	width:95%;
	margin:auto;
	border:none;
}
.comments {
	width:100%;
	height:90px;
	float:left;
	font-size:16px;
	text-align:justify;
	vertical-align:top;
}
.comments .text {
	width:97%;
	height:100%;
	margin:auto;
}
.text .title {
	font-size:12px;
	font-weight:bold;
	margin-left:5px;
}
.folio {
	width:30%;
	float:left;
}
</style>
</head>

<body id="body">
<div class="page">
  <div class="row head">
    <div class="contact">
      <img src="'.base_url().'documents/hlogo.png" width="350px">
      <div>
       <div  style=" padding-left:45px; margin-bottom:10px; margin-top:-5px; font-size:16px; "><div style="font-weight:bold;" >OPTIMA AUTOMOTRIZ S.A. DE C.V.</div>
       <div style=" padding-left:40px;">RFC: OAU990325G88</div></div>
        <table cellspacing="0"  class="address" style="width:950px;">
          <tr>
            <th width="22%" class="title right">TIJUANA B.C.:</th>
            <th width="28%" class="title right">MEXICALI B.C.:</th>
            <th width="28%" class="title right">ENSENADA B.C.:</th>
            <th width="22%" class="title">HORARIO DE ATENCI&Oacute;N<th>
          </tr>
          <tr>
            <td class="right">Av. Padre Kino 4300 Zona Río,<br> CP. 22320,<br />
              Tel. (664) 900-9010</td>
            <td class="right">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
              Tel. (686) 900-9010</td>
			<td class="right" style="text-align:center">Av. Balboa 146 Esq.<br />
              López Mateos Fracc. Granados. CP.  228402,<br />
              Tel. (646) 900-9010
			</td>
			<td style="text-align:center">Lunes a Viernes de 8:00 A.M. a 6:00 P.M.<br />
              Sábado de 8:00 A.M. a 1:30 P.M.
			</td>
			<td></td>

          </tr>

        </table>


      </div>
    </div>
    <div class="folio">

      <div class="info">
        <div class="message">
          <p  style="background-color:white"></p>
        </div>
        <div style="width:45%" class="service-order">
          <table class="order-date radius">
            <tr class="gray">
              <td>ORDEN DE SERVICIO</td>
            </tr>
            <tr class="white" style="text-align:center; height:15px;">
              <td>';if($orden[0]->seo_serviceOrder==''){
                  $html.=$orden[0]->seo_tower;
              }else{
                  $html.=$orden[0]->seo_serviceOrder;
              }
        $html.='</td>
            </tr>
            <tr class="gray">
              <td>FECHA</td>
            </tr>
            <tr class="white" style="text-align:center;height:15px;" >
              <td>';list($fec,$hor)=explode(" ",$orden[0]->seo_date); $html.= $fec.'</td>
            </tr>
             <tr class="gray">
              <td>LOCALIDAD</td>
            </tr>
            <tr class="white" style="text-align:center; height:15px;" >
              <td>'; $html.=$_SESSION['sfworkshop'].'</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin-bottom: 2px;   margin-top: 5px;">
    <div class="customer-info" style="margin-top:12px;padding-top:10px !important;">
      <table class="radius" style="border-radius:5px;">
        <tr class="first-row">
          <th colspan="3" class="title">DATOS DEL CLIENTE (CONSUMIDOR)</th>
        </tr>
        <tr>
          <td colspan="3" ><b>Nombre:</b>'; $html.=substr(ucwords(strtolower($orden[0]->sec_nombre)),0,50).'</td>
        </tr>
        <tr>
          <td colspan="3"   ><b>Dirección: </b>';$html.=substr(ucwords(strtolower($orden[0]->sec_calle)),0,23).''.ucwords(strtolower($orden[0]->sec_num_ext)).'</td>
        </tr>
        <tr>
         <td style=" font-size:14px;"><b>Ciudad:</b>'.ucwords(strtolower($orden[0]->sec_ciudad)).'</td>
        <td style=" font-size:14px;" ><b>Estado:</b>'.ucwords(strtolower($orden[0]->sec_estado)).'</td>
        <td style=" font-size:14px;"><b>CP:</b>'.ucwords(strtolower($orden[0]->sec_cp)).'</td>
        </tr>
        <tr  >
          <td  class="col-5" style="font-size:15px"><b>R.F.C.:</b>'.strtoupper($orden[0]->sec_rfc).'
</td>

        </tr>
        <tr>
            <td class="col-7" colspan="2" style="font-size:14px"><b>Email:</b>'.$eema=$orden[0]->sec_email.';'; list($email,$emailb)=explode(';',$eema);
		   substr(ucwords(strtolower($email)),0,25); $html.='</td>
        </tr>
        <tr>
          <td colspan="3"  ><b>Teléfono:</b>'.ucwords(strtolower($orden[0]->sec_tel)).'</td>

        </tr>
        <tr>
          <td  colspan="3" ><b>Persona autorizada para recoger la unidad:</b> &nbsp;</td>

        </tr>
        <tr>
          <td  colspan="3" ><b>Nombre:</b>'.substr(ucwords(strtolower($orden[0]->seo_persona_recoge)),0,50).'</td>

        </tr>
        <tr>
          <td colspan="3" ><b>Teléfono:</b>'.ucwords(strtolower($orden[0]->seo_telefono_recoge)).'</td>
        </tr>
      </table>
    </div>
    <div class="vehicle-info" style="margin-top:12px;">
      <table style="height:300px !important;" class="radius">
        <tr class="first-row">
          <th colspan="4" class="title">DATOS DEL VEHÍCULO</th>
        </tr>

        <tr height="36px;">
          <td ><b>Marca:</b>'.ucwords(strtolower($orden[0]->sev_marca)).'</td>
          <td ><b>Sub Marca:</b>'.ucwords(strtolower($orden[0]->sev_sub_marca)).'</td>
        </tr>
        <tr style="height:36px;">
          <td ><b>Año-Modelo:</b>'.ucwords(strtolower($orden[0]->sev_modelo)).'</td>
          <td ><b>Placas:</b>'.strtoupper($orden[0]->sev_placas).'</td>
        </tr>
        <tr style="height:36px;">
          <td ><b>Fecha de Venta:</b>'.$orden[0]->sec_venta.'</td>

          <td ><b>Último Servicio:</b>'.$orden[0]->sec_ultimo_servicio.'</td>

        </tr>
        <tr style="height:36px;">
          <td class="col-6"><b>Color:</b>'.ucwords(strtolower($orden[0]->sev_color)).'</td>
          <td class="col-6"><b>Seguro:</b> &nbsp;</td>
        </tr>
        <tr style="height:36px;">
          <td  class="col-6"><b>No. de  VIN:</b>'.strtoupper ($orden[0]->sev_vin).'</td>
          <td class="col-6"><b>Km. de Entrada:</b>'.ucwords(strtolower($orden[0]->sev_km_recepcion)).'</td>
        </tr>
        <tr style="margin-bottom:10px;">
         <td  class="col-6"><b>No. de  Motor:</b> &nbsp;</td>
          <td class="col-6"><b>Capacidad:</b>'.ucwords(strtolower($orden[0]->sev_capacidad)).'

          </td>

        </tr>



      </table>
    </div>
  </div>
  <div class="row" style="margin-bottom: 2px;" >
    <div class="reception-info">
      <table  class="radius" width="100%">

        <tr>
          <td colspan="2"><b>Asesor:</b>'.$orden[0]->sus_name.' '.$orden[0]->sus_lastName.'</td>
        </tr>
        <tr>
           <td colspan="2"><b>Técnico:</b>'.$tecnico[0]->sus_name.' '.$tecnico[0]->sus_lastName.'</td>
        </tr>
        <tr>
          <td colspan="2"><b>Fecha y hora de recepción:</b>'.ucwords(strtolower($orden[0]->seo_timeReceived)).'</td>
        </tr>
        <tr>
          <td colspan="2" ><b>Fecha de entrega del vehículo:</b>'.date('Y-m-d').'</td>

        </tr>
        <tr height="27px;">
          <td  style=" padding-top:4px; text-align:center; font-weight:bold;background-color:black; color:white" class="col-5" >NUMERO DE TORRE: </td>
          <td width="27px;" style="font-size:22px; font-weight:bold;  border:1px solid white">'.$orden[0]->seo_tower.'</td>
        </tr>
        <tr>
          <td colspan="2" ><b>            -</b></td>
        </tr>
      </table>
    </div>
    <div class="confirmation radius" style="margin-left:5px !important; padding-left:5px; font-weight:bold; color:black; font-size:13px;">Se entregan las partes o refacciones reemplazadas al consumidor SI &nbsp;&nbsp;('; if($orden[0]->seo_se_entregan==1){ $html.='&radic;';} $html.=' ) &nbsp;&nbsp;&nbsp;&nbsp; NO &nbsp;&nbsp;( '; if($orden[0]->seo_se_entregan==0){ $html.='&radic;';}$html.=')
      <br>
      <b>NOTA:</b> Las partes y/o refacciones no se entregar&aacute;n al consumidor cuando:<br>
      a) Sean cambiadas por garantía<br>
      b) Se trate de residuos peligrosos deacuerdo con las disposiciones legales aplicables.<br>

      Servicio  en el domicilio del consumidor &nbsp;(';if($orden[0]->seo_ser_domi==1){ $html.='&radic;';} $html.=') &nbsp; SI  &nbsp;( ';if($orden[0]->seo_ser_domi==0){ $html.='&radic;';}$html.=') &nbsp; NO<br>

      Poliza de seguros para cubrir al consumidor los daños o extravios de <br> bienes: SI (';
        if($orden[0]->seo_poliza==1){
            $html.= '&radic;';
        }$html.=' ) Numero:';
        if($orden[0]->seo_poliza==''){
            $html.= '______________';
        }else{
            $html.=$orden[0]->seo_poliza;
        }
        $html.= ' ( ';
        if($orden[0]->seo_poliza==0){
            $html.='&radic;';
        }
        $html.=' )NO

    </div>
  </div>
  <div class="row">
    <div class="floated">
      <div style="margin-bottom: 73px;">
        <div class="text radius" style=" height:50px; padding-left:15px;"> <span class="title">COMENTARIOS:</span> <br>'.$orden[0]->seo_comments.'
          <div style="font-size:16px; ">
           </div></div>
      </div>

       <div class="itemsb"  style="margin-top:-70px;">
       <div style="font-weight:bold; width:100%; text-align:center; color:rgb(226,0,43);">INVENTARIO DEL VEHICULO</div>
        <table >
          <tr style="height:10px">
            <td width="15px;" >';if($orden[0]->seo_tapetes==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">TAPETES</td>
            <td width="15px;" style="text-align:right">';if($orden[0]->seo_tapones==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">TAPONES</td>
            <td width="15px;" style="text-align:right">';if($orden[0]->seo_herramienta==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">HERRAMIENTA</td>
             <td width="15px;" style="text-align:right">'; if($orden[0]->seo_reflejantes==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">REFLEJANTES</td>
             <td width="15px;" style="text-align:right">'; if($orden[0]->seo_ecualizador==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ECUALIZADOR</td>
              <td width="15px;" style="text-align:right">'; if($orden[0]->seo_tapon==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">TAPÓN GAS</td>
                <td width="15px;" style="text-align:right">'; if($orden[0]->seo_ventanas==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">VENTANAS</td>
          </tr>
          <tr style="height:10px">
            <td>'; if($orden[0]->seo_espejo==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ESPEJO</td>
               <td width="15px;" style="text-align:right">'; if($orden[0]->seo_radio==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">RADIO</td>
            <td width="15px;" style="text-align:right">'; if($orden[0]->seo_llantarefaccion==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">LLANTA REF.</td>
               <td width="15px;" style="text-align:right">'; if($orden[0]->seo_extinguidor==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">EXTINGUIDOR</td>
          <td width="15px;" style="text-align:right">';if($orden[0]->seo_gato==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">GATO</td>
              <td width="15px;" style="text-align:right">';if($orden[0]->seo_carnet==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">CARNET</td>
                 <td width="15px;" style="text-align:right">'; if($orden[0]->seo_vestidura==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">VESTIDURA</td>
          </tr>
          <tr style="height:10px">
               <td>'; if($orden[0]->seo_antena==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ANTENA</td>
                 <td width="15px;" style="text-align:right">'; if($orden[0]->seo_encendedor==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ENCENDEDOR</td>
           <td width="15px;" style="text-align:right">'; if($orden[0]->seo_limpiadores==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">LIMPIADORES</td>
                    <td width="15px;" style="text-align:right">'; if($orden[0]->seo_cables==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">CABLES P/C</td>
                   <td width="15px;" style="text-align:right">'; if($orden[0]->seo_estereo==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">ESTEREO</td>
                      <td width="15px;" style="text-align:right">'; if($orden[0]->seo_carroceria==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">CARROCERÍA</td>
                     <td width="15px;" style="text-align:right">'; if($orden[0]->seo_parabrisas==2){$html.='&radic;';}else{$html.='X';}$html.='</td>
            <td style=" font-weight:bold">PARABRISAS</td>
          </tr>
          <tr >
            <td height="18px;"></td>
            <td ></td>
            <td ></td>
            <td></td>
            <td></td> <td colspan="4" style="font-weight:bold; font-family:14px; background-color:black; color:white; text-align:center" > CANTIDAD DE COMBUSTIBLE: </td>
            <td style="background-color:black; color:white; text-align:center; font-weight:bold">'.$orden[0]->seo_combustible.'</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
      <div class="tecs" style=" padding-right:30px; margin-top:8px;">
        <table style="text-align:center; margin-top:30px;margin-bottom:90px; font-weight:bold; font-size:16px;" class="radius" >
          <tr>
            <td valign="middle" style="padding-top:3px;">Hora de Recepción</td>
            <td>Hora de Inicio de Trabajo</td>
            <td>Hora de Trabajo Terminado</td>
          </tr>
          <tr>
            <td>'; list($fech,$hora)=explode(" ",$orden[0]->seo_timeReceived); $html.=ucwords(strtolower($hora));$html.='
             </td>
            <td></td>
            <td>'.ucwords(strtolower($orden[0]->seo_promisedTime)).'</td>
          </tr>
        </table>
      </div>
    <div class=" floated" style="margin-top:-73px; padding-right:10px;">

     <img src="'.base_url().'signature/'; if(empty($orden[0]->sei_cha_uno))$html.= 'chasis.png'; else $html.= $orden[0]->sei_cha_uno; $html.='" height="280px;" width="143px;">
   <table style="margin-top:-175px;" width="130px" height="150px;">
<tr>
<td width="20px;" align="center">'; if($orden[0]->sei_cha_uno=='ok'){$html.= ' ';}$html.='</td>
<td width="95px;"></td>
<td>'; if($orden[0]->sei_cha_dos=='ok'){$html.= '';}$html.='</td>
</tr>
<tr height="43px;">
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td width="20px;" align="center">'; if($orden[0]->sei_cha_tres=='ok'){$html.= '';}$html.='</td>
<td></td>
<td>'; if($orden[0]->sei_cha_cuatro=='ok'){$html.= '';}$html.='</td>
</tr>

</table>
    </div>
       <div  class="comments" style="margin-top:-50px; height: 85px; width:1022px; padding-left:5px; margin-left:-10px;">
        <div class="text radius" style=" width:1022px; height:90px; margin-top:-20px; ">
          <table width="1024px" style="text-align:center; " cellspacing="0"  border="0">
  <tr>
    <td width="30%" style=" border-bottom:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">OPERACIONES A EFECTUAR</td>
    <td width="30%" style=" border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PARTES Y/O REFACCIONES</td>
    <td width="30%" style="  border-bottom:1px solid #000;border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">PRECIOS UNITARIOS</td>
    <td width="10%" style=" border-bottom:1px solid #000; border-top:1px solid #000;"></td>
  </tr>

  ';
  setlocale(LC_MONETARY, 'en_US');
  $num=count($servicios);
  $numsv=count($serviciossv);
  $limite=11 - $num - $numsv;
  $monto=0;
  $tiposer='';
  foreach($servicios as $ser){


	 if($ser->ser_tipo=='pro'){
	  $comercial=$ser->ser_comercial / 1.16;
	 }else{
	  $comercial=$ser->ser_comercial;
	 }
	 $tiposer=$ser->ssr_venta;

	  $monto+=$comercial;


	$html.='
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$ser->set_name.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($comercial, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';
	  }


	  foreach($serviciossv as $sersv){





	$html.= '
	<tr>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;">'.$sersv->sev_nombre.'</td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style=" border-bottom:1px solid #999; text-align:left; ">
	<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format(0, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>
	</td>
  </tr>';
	  }


  for($x=0; $x<$limite; $x++) { $html.='
  <tr>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style=" border-bottom:1px solid #999;border-right:1px solid #000;"></td>
    <td width="30%" style="border-bottom:1px solid #999; border-right:1px solid #000;"></td>
    <td width="10%" style="border-bottom:1px solid #999; text-align:left; ">$</td>
  </tr>';
 }



  $html.='<tr>
    <td width="30%" style="border-right:1px solid #000;">&nbsp;</td>
    <td width="30%" style="border-right:1px solid #000;"></td>
    <td width="30%" style="border-right:1px solid #000;">MONTO DE LA OPERACI&Oacute;N</td>
    <td width="10%" style=" text-align:left;border-top:1px solid #000;">'; $html.= '<table width="100%"><tr>
	<td width="20%">$</td>
	<td width="79%" align="right">
	'.number_format($monto, 2, '.', '').'
	</td>
	<td width="1%"></td>
	</tr></table>'; $html.='</td>
  </tr>

</table>


          </div>
      </div>


      <div class="comments" style="margin-top:0;  width:1020px; margin-left:-10px;">
          <div class="text " style=" height:90px; ">
          <table width="100%">
          <tr>
          <td width="15%">FORMA DE PAGO</td>
          <td width="50%">EFECTIVO ( '; if($orden[0]->seo_pago==1){$html.= '&radic;';}$html.=') CHEQUE ('; if($orden[0]->seo_pago==2){$html.= '&radic;';}$html.=') TARJETA DE CREDITO ('; if($orden[0]->seo_pago==3){$html.= '&radic;';}$html.=') OTRO ('; if($orden[0]->seo_pago==4){$html.= '&radic;';}$html.=' )</td>
          <td width="25" style=" text-align:right">SUBTOTAL</td>
          <td width="10%">$ '.number_format($monto, 2, '.', '').'</td>
          </tr>
          <tr>
          <td width="15%"></td>
          <td width="50%"></td>
          <td width="25" style=" text-align:right">IVA</td>
          <td width="10%">
          '; if($tiposer=='interna'){$iva=0;}
	else{$iva=0.16;}$html.='
          $'.number_format($monto * $iva, 2, '.', '').'</td>
          </tr>
          <tr>
          <td width="15%"></td>
          <td width="50%"></td>
          <td width="25" style=" text-align:right"> TOTAL</td>
          <td width="10%">
          '; if($tiposer=='interna'){$miva=0;}
	else{$miva=1.16;}$html.='
          $'.number_format($monto * $miva, 2, '.', '').'</td>
          </tr>
          <tr>
          <td width="15%">&nbsp;</td>
          <td width="50%"></td>
          <td width="25" style=" text-align:right"></td>
          <td width="10%"></td>
          </tr>
          <tr>
          <td colspan="4" style="font-size:13px; font-weight:bold" width="100%; ">
          *EL TOTAL NO INCLUYE SERVICIOS ADICIONALES. EN CASO DE SER NECESARIOS, SE SOLICITARA AUTORIZACION AL CLIENTE VIA TELEFONICA
          </td>

          </tr>
          </table>

          <!--<table width="100%" style="text-align:center; font-size:14px;"  cellspacing="0"  border="0">
  <tr>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">FORMA DE PAGO</td>
    <td width="10%" style="border-right:1px solid #000; text-align:left; border-top:1px solid #000; text-decoration:underline;"></td>
    <td width="37.5%" style="border-right:1px solid #000; border-top:1px solid #000; text-decoration:underline;">SERVICIOS ADICIONALES:</td>
    <td width="10%" style=" border-top:1px solid #000;"></td>
  </tr>

 <tr>
    <td  style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">Monto de la operaci&oacute;n:

    </td>
    <td style="border-right:1px solid #000; text-align:left; ">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" "></td>
  </tr>
  <tr>
    <td style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Otros cargos:
    </td>
    <td  style="border-right:1px solid #000;text-align:left;">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Total refacciones y servicios adicionales</td>
    <td  style=" text-align:left">$</td>
  </tr>
<tr>
    <td style="border-right:1px solid #000; text-align:left;  padding-left:10px; ">
    Servicios Adicionales:

    </td>
    <td  style="border-right:1px solid #000; text-align:left;">$</td>
    <td style="border-right:1px solid #000; "></td>
    <td  style=" border-top:1px solid #000;"></td>
  </tr>
  <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Parcial:

    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">Fecha y monto de anticipo:</td>
    <td  style="text-align:left" >$</td>
  </tr>
   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
    Impuesto al Valor Agregado:

    </td>
    <td  style="border-right:1px solid #000;text-align:left; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">El resto del monto total de la operaci&oacute;n. Se liquidara en</td>
    <td  style="border-top:1px solid #000;  text-align:left"></td>
  </tr>

   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Monto Total (Incluye mano de obra):

    </td>
    <td  style="border-right:1px solid #000;text-align:left; border-bottom:1px solid #000; ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; ">  la fecha programada para la entrega del veh&iacute;culo</td>
    <td  style=" "></td>
  </tr>

   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">
Efectivo ( &nbsp; ) Cheque ( ) tarjeta de cr&eacute;dito ( ) Otro ( )

    </td>
    <td style="border-right:1px solid #000;text-align:left;  ">$</td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>

   <tr>
    <td  style="border-right:1px solid #000; text-align:left; padding-left:10px; ">


    </td>
    <td  style="border-right:1px solid #000;text-align:left;  "></td>
    <td  style="border-right:1px solid #000; text-align:left; "></td>
    <td  style=" "></td>
  </tr>








</table>    -->
          </div>
      </div>


     <div style="margin-bottom: 32px;">
        <div class="text radius" style=" height:25px;  padding-left:15px;"> <span class="title">POSIBLES CONSECUENCIAS:</span> <br>
          <div style="font-size:16px; ">
           </div></div>
      </div>

    </div>




  </div>


  <div class="row" style="margin-top:-30px;">

    <b style=" font-size:16px; margin-left:5px; margin-top:-26px; ">Imagenes del Vehículo ( Condiciones de Recepción )</b> <br>

     <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Costado Derecho</th>
        <th width="20%" >Costado Izquierdo</th>
        <th width="20%" >Parte Frontal</th>
        <th width="20%" >Parte Trasera</th>
        <th width="20%" >Tablero</th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td>';
		$ider=$orden[0]->sei_inf_der;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_inf_izq;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_pos_der;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			};$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_pos_izq;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td>';
		$ider=$orden[0]->sei_tablero;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='

          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
      </tr>
    </table>

    <b style=" font-size:16px;">Imagenes Extras</b> <br>

    <table class="radius services-head" style="width:100%; text-align:center">
      <tr>
        <th width="20%" class="first-cell col-2">Extra 1</th>
        <th width="20%" >Extra 2</th>
        <th width="20%" >Extra 3</th>
        <th width="40%" ></th>
      </tr>
    </table>
    <table class="services" align="center" width="100%" style="text-align:center">
      <tr>
        <td style="width:20%">';
		$ider=$orden[0]->sei_extra_uno;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='
            <a href="'.$url.'/'.$ider.'" target="_blank">
          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td style="width:20%">';
		$ider=$orden[0]->sei_extra_dos;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='
            <a href="'.$url.'/'.$ider.'" target="">
          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td style="width:20%">';
		$ider=$orden[0]->sei_extra_tres;
		if($ider=='noimagen.png'){
			 $url=''.base_url().'assets/img';	}
		else{
		   $url=''.base_url().'calendario/ajax/uploads/'.$orden[0]->sei_carpeta.'';
			}$html.='
            <a href="'.$url.'/'.$ider.'" target="_blank">
          <img style="border:1px solid #CCC" src="'.$url.'/'.$ider.'" width="100px;"></a></td>
        <td width="40%"></td>
      </tr>
    </table>
  </div>
</div>



</body>
</html>
';


         $this->load->library('mpdf');
		$this->mpdf->writeHTML($html);

        $this->mpdf->SetImportUse();
        $pagecount = $this->mpdf->SetSourceFile('documents/cotizaciones/docs/adicionales.pdf');
            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $this->mpdf->ImportPage($i);
                $this->mpdf->UseTemplate($import_page);

                if ($i < $pagecount)
                {$this->mpdf->AddPage();

                }
                if($i == 1){
                    $html3='';
                   for($ii = 0;$ii<33; $ii++){
                        $html3.='<br>';
                    }
                    $html3 .= '<img width="50px" height="25px" style="margin-left:20%" src="firmas/'.$orden[0]->sec_firma.'"/>';
                    for($ii = 0;$ii<9; $ii++){
                        $html3.='<br>';
                    }
                    $html3.='
                    <div width="45%" style="float:left">
                    <img width="110px" height="60px" style="margin-left:12%" src="firmas/asesores/'.$orden[0]->sus_firma.'"/>
                    </div>
                    <div width="45%" style="float:left">
                    <img width="110px" height="60px" style="float:right; margin-left:70%" src="firmas/'.$orden[0]->sec_firma.'"/>
                    </div>';
                    $this->mpdf->writeHTML($html3);
                }

            }

        $html2 = '';
        for($ii = 0;$ii<42; $ii++){
            $html2.='<br>';
        }
        $html2.='<img width="110px" height="65px" style="margin-left:40%" src="firmas/'.$orden[0]->sec_firma.'"/>';
        $this->mpdf->writeHTML($html2);

		$new_name = 'documents/cotizaciones/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output($new_name, 'I');

        if($publicity=='false'){
            $this->Ordendeserviciomodel->InsertMailWithoutPublicity($eema);
        }

        $this->email->from('avisodeprivacidad@hondaoptima.com', 'Honda Optima');
        $this->email->to($eema);
        $this->email->cc('');
        $this->email->subject('Aviso De Privacidad');
        $this->email->message('');
        $this->email->attach($new_name);


            echo $eema;
            echo $this->email->send();
            echo $this->email->print_debugger();


	}



	function prueba_documento($id)
	{

		$cotizacion = $this->Cotizacionesmodel->obtener($id);
		$servicios = $this->Cotizacionesmodel->obtener_servicios();
		$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
		$cantidad_servicios = count($servicios_cotizacion);
		$total = $this->Cotizacionesmodel->obtener_total_cotizacion($id);
		$iva = $total[0]->total * 0.16;
		$great_total = $total[0]->total + $iva;

		setlocale(LC_MONETARY, 'en_US');
		$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
.page {
	font-family:Verdana, Geneva, sans-serif;
}
	.head {
		width:100%;

		margin-bottom:0px;

	}
		.contact {
			width:55%;

			float:left;
		}
			.logo {
				width:100%;
				height:70px;
				background-image:url("http://webmarketingnetworks.com/sas/documents/hlogo.png");
				background-size: 80%;
				background-repeat:no-repeat;
			}
			.address {
				width:100%;
				height:100%;
				border:none;
				border-collapse:collapse;
			}
				.address th {
					font-size:9px;
				}
				.address td {
					font-size:8px;
				}
				.address .hours{
					font-size:10px;
					text-align:center;
				}
		.folio {
			width:45%;

			float:left;
		}
			.os-type {
				width:100%;
				height:25px;
				padding-top:20px;
				font-size:18px;
				font-weight:bold;
				text-align:center;
			}
			.info {
				width:60%;
				height:120px;
				font-size:12px;
				margin-left:150px;
			}
				.message {
					width:60%;
					height:60px;
					float:left;
				}
					.message p{
						width:98%;
						margin:auto;
						margin-top:20px;
						padding:5px;
						border:0px solid rgb(51,51,51);
						border-radius:15px;
						background-color:rgb(226,0,43);
						color:rgb(255,255,255);
						font-size:8px;
						text-align:justify;
						line-height:180%;
					}
				.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:10px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}
						.order-date .gray{
							background-color:rgb(208,208,208) ;
							color:rgb(255,255,255);
							font-size:10px;
							font-weight:900;
						}
						.order-date .white{
							height:27px;
							font-size:10px;
							font-weight:900;
							vertical-align:text-top;
							text-align:left;
						}
						.order-date td{
							border:1px solid rgb(255,255,255);
						}

.grey{
	background-color:rgb(208,208,208) ;
}


.comments{
	width:100%;

	float:left;
	font-size:9px;
	text-align:justify;
	vertical-align:top;
}
	.comments .text{
		width:99%;
		height:40px;
		margin:auto;
	}
	.text .title{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
	}

	.title-address-right{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:right;
	color:rgb(226,0,43);
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
	}

	.title-address-left{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:left;
	color:rgb(226,0,43);
	padding-left:3px;
	}

.customer-info {
	width:100%;
	float:left;
	font-size:9px;
}

	.customer-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.customer-info table th{
		border-bottom:1px solid rgb(0,0,0);
	}
	.customer-info table td{
		padding-left:5px;
	}

.vehicle-info {
	width:100%;
	float:left;
	font-size:9px;
}

	.vehicle-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.vehicle-info table th {
		border-bottom:1px solid rgb(51,51,51);
	}
	.vehicle-info table td{
		padding-left:5px;
	}

.details {
	width:70%;
}

.services-head{
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:10px;
	margin-bottom:10px;
}
	.services-head th{
		height:22px;
	}

.services{
	font-size:10px;
	border:1px solid rgb(226,0,43);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}

.first-cell{
	border-right:1px solid rgb(226,0,43);
}

.total{
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(72,72,72);
	border-radius:5px;
}
.total-text{
	text-align:right;
	padding-right:15px;
}

.row{
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child{
	margin-bottom:0px;
}
.floated{
	float:left;
}
.col-12 {
	width:100%;
}
.col-10{
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8{
	width:66.66%;
}
.col-7{
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5{
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:16.5%;
}
.col-1{
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right{
	text-align:right;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left{
	text-align:left;
	padding-left:3px;
}
.radius {
    border: 1px solid rgb(51,51,51);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>

<div class="page">
	<div class="head">
    	<div class="contact">
        	<div class="logo">

			</div>

            	<table style="width:200px !important;" class="address">
                    	<tr>
                        	<th class="title-address-right">TIJUANA:</th>
                        	<th class="title-address-left">MEXICALI:</th>
                        </tr>
                    	<tr>
                        	<td class="right">Av. Padre Kino 4300 Zona R&iacute;o, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td class="left">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr>
                        	<th class="title-address-right">ENSENADA:</th>
                        	<td class="hours">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr>
                        	<td class="right">Ensenada Av. Balboa 146 esq.<br />
							L&oacute;pez Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td class="hours">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            S&aacute;bado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>

        </div>

        <div class="folio">

        	<div class="info">
            	<div class="service-order">
                	<table class="order-date radius">
                            <tr class="gray">
                            	<td>FECHA</td>
                            </tr>
                            <tr class="white">
                            	<td><b>
								'.date("Y-m-d", strtotime($cotizacion[0]->est_date)).'
								</b></td>
                            </tr>
                    </table>
					<br>
					<div style="margin-left:10px;">
					<center>
					<b>Asesor de Servicio</b><br>
					'.$cotizacion[0]->sus_name.' '.$cotizacion[0]->sus_lastName.'
                </center>
				</center>
				</div>
            </div>
        </div>
    </div>
	<br>
	<div class="row">
	<div class="os-type">
        		COTIZACI&Oacute;N
        	</div>
			<br>
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="2" class="title grey">CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-2">Nombre: </td>
                        <td class="col-10" style="text-align:left;"><b>'.$cotizacion[0]->est_customer.'</b></td>
                    </tr>
            </table><br>
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="6" class="title grey">VEH&Iacute;CULO</th>
                    </tr>
                    <tr>
                        <td class="col-1">Modelo: </td>
                        <td class="col-4" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_brand.'</b></td>
                        <td class="col-1">A&ntilde;o: </td>
                        <td class="col-2" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_model.'</b></td>
                        <td class="col-1">Color: </td>
                        <td class="col-3" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_color.'</b></td>
                    </tr>
            </table>
        </div>
    </div>
	<br><br><br>
	<div class="row">
    	<table class="radius services-head" style="width:100%;">
        	<tr>
            	<th class="first-cell col-1"></th>
            	<th class="col-1">CANTIDAD</th>
            	<th class="col-3">SERVICIO</th>
            	<th class="col-2">TOTAL<br>REFACCIONES</th>
            	<th class="col-2">TOTAL<br>MANO DE OBRA</th>
            	<th class="col-2">TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
		';
		$suma=0;
		$tot=0;
		foreach($servicios_cotizacion as $key => $row)
		{
			if($row->ess_is_existencia==0){$existencia='Pedido Especial';}
			else{
				$existencia='En Existencia';
				}
			$suma=$row->ess_unit_price + $row->ess_handwork;
			$suma;
			if($row->ess_service==0 || $row->ess_service=='' ){}
			else{
			$html = $html.'<tr>
					<td class="first-cell col-1">'.(1+$key).'</td>
					<th class="col-1">'.$row->ess_quantity.'</th>
					<th class="col-3" style="text-align:left;">'.$servicios[$row->ess_service].'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_unit_price).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_handwork).'</th>
					<th class="col-2">'.money_format('%(#10n', $suma).'</th>
                </tr>
				<tr>
					<td class="first-cell col-1"></td>
					<th class="col-1"></th>
					<th class="col-3" style="font-size:9px;text-align:left; padding-left:10px;"><i style="margin-left:10px; ">'.$row->ess_description.' ( '.$existencia.' )</i></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
				</tr>';
				$tot+=$suma;
			}
		}

		$html = $html.'
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">SUB TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot).'</p></td>
                </tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">IVA</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot * 0.16).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td  class="total-text" style="">GRAN TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $tot * 1.16).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
        </table>
    </div>
	<br><br><br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th class="title grey">INFORMACI&Oacute;N</th>
                    </tr>
                    <tr>
                        <td class="col-6">*** Esta cotizaci&oacute;n ser&aacute; v&aacute;lida durante 30 d&iacute;as a partir de esta fecha.	</td>
                    </tr>
            </table><br>
			<h3>Optima Automotriz, S.A. de C.V.</h3>
			<h3>Departamento de Servicio</h3>
        </div>
    </div>
</div>
';

		$this->load->library('mpdf');
		$this->mpdf->writeHTML($html);
		$new_name = 'documents/estimates/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output();//$new_name, 'F');

	}

	function crear_pdf($id)
	{
		$cotizacion = $this->Cotizacionesmodel->obtener($id);
		$servicios = $this->Cotizacionesmodel->obtener_servicios();
		$servicios_cotizacion = $this->Cotizacionesmodel->obtener_servicios_cotizacion($id);
		$cantidad_servicios = count($servicios_cotizacion);
		$total = $this->Cotizacionesmodel->obtener_total_cotizacion($id);
		$iva = $total[0]->total * 0.16;
		$great_total = $total[0]->total + $iva;

		setlocale(LC_MONETARY, 'en_US');
		$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
.page {
	font-family:Verdana, Geneva, sans-serif;
}
	.head {
		width:100%;
		margin-bottom:0px;

	}
		.contact {
			width:55%;

			float:left;
		}
			.logo {
				width:100%;
				height:70px;
				background-image:url("http://webmarketingnetworks.com/sas/documents/hlogo.png");
				background-size: 80%;
				background-repeat:no-repeat;
			}
			.address {
				width:100%;
				height:100%;
				border:none;
				border-collapse:collapse;
			}
				.address th {
					font-size:9px;
				}
				.address td {
					font-size:8px;
				}
				.address .hours{
					font-size:10px;
					text-align:center;
				}
		.folio {
			width:45%;

			float:left;
		}
			.os-type {
				width:100%;
				height:25px;
				padding-top:20px;
				font-size:18px;
				font-weight:bold;
				text-align:center;
			}
			.info {
				width:60%;
				height:120px;
				font-size:12px;
				margin-left:150px;
			}
				.message {
					width:60%;
					height:60px;
					float:left;
				}
					.message p{
						width:98%;
						margin:auto;
						margin-top:20px;
						padding:5px;
						border:0px solid rgb(51,51,51);
						border-radius:15px;
						background-color:rgb(226,0,43);
						color:rgb(255,255,255);
						font-size:8px;
						text-align:justify;
						line-height:180%;
					}
				.service-order {

					float:left;
				}
					.service-order table {
						width:80%;
						margin-top:20px;
						font-size:10px;
						border:1px solid rgb(102,102,102);
						border-spacing:0;
						text-align:center;
					}
						.order-date .gray{
							background-color:rgb(208,208,208) ;
							color:rgb(255,255,255);
							font-size:10px;
							font-weight:900;
						}
						.order-date .white{
							height:27px;
							font-size:10px;
							font-weight:900;
							vertical-align:text-top;
							text-align:left;
						}
						.order-date td{
							border:1px solid rgb(255,255,255);
						}

.grey{
	background-color:rgb(208,208,208) ;
}


.comments{
	width:100%;

	float:left;
	font-size:9px;
	text-align:justify;
	vertical-align:top;
}
	.comments .text{
		width:99%;
		height:40px;
		margin:auto;
	}
	.text .title{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
	}

	.title-address-right{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:right;
	color:rgb(226,0,43);
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
	}

	.title-address-left{
		font-size:10px;
		font-weight:bold;
		margin-left:5px;
		text-align:left;
	color:rgb(226,0,43);
	padding-left:3px;
	}

.customer-info {
	width:100%;
	float:left;
	font-size:9px;
}

	.customer-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.customer-info table th{
		border-bottom:1px solid rgb(0,0,0);
	}
	.customer-info table td{
		padding-left:5px;
	}

.vehicle-info {
	width:60%;
	float:left;
	font-size:9px;
}

	.vehicle-info table {
		width:98%;
		margin:auto;
		line-height:150%;
	}

	.vehicle-info table th {
		border-bottom:1px solid rgb(51,51,51);
	}
	.vehicle-info table td{
		padding-left:5px;
	}

.details {
	width:70%;
}

.services-head{
	background-color:rgb(226,0,43);
	color:rgb(255,255,255);
	font-size:10px;
	margin-bottom:10px;
}
	.services-head th{
		height:22px;
	}

.services{
	font-size:10px;
	border:1px solid rgb(226,0,43);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
	text-align:center;
}

.first-cell{
	border-right:1px solid rgb(226,0,43);
}

.total{
	height:15px;
	margin:5px;
	vertical-align:center-top;
	border:1px solid rgb(72,72,72);
	border-radius:5px;
}
.total-text{
	text-align:right;
	padding-right:15px;
}

.row{
	width:100%;
	margin-bottom:10px;
	float:left;
}
.row:first-child{
	margin-bottom:0px;
}
.floated{
	float:left;
}
.col-12 {
	width:100%;
}
.col-10{
	width:83.33%;
}
.col-9 {
	width:75%;
}
.col-8{
	width:66.66%;
}
.col-7{
	width:58.33%;
}
.col-6 {
	width:50%;
}
.col-5{
	width:41.66%;
}
.col-4 {
	width:33.33%;
}
.col-3 {
	width:25%;
}
.col-2 {
	width:16.5%;
}
.col-1{
	width:8.33%;
}
.first-row {
	border:1px solid rgb(0,0,0);
}
.title {
	color:rgb(226,0,43);
}
.right{
	text-align:right;
	padding-right:3px;
	border-right:1px solid rgb(0,0,0);
}
.left{
	text-align:left;
	padding-left:3px;
}
.radius {
    border: 1px solid rgb(51,51,51);
    border-radius: 10px;
	border-spacing:0;
	overflow:hidden;
}
</style>

<div class="page">
	<div class="head">
    	<div class="contact">
        	<div class="logo">

			</div>

            	<table class="address">
                    	<tr>
                        	<th class="title-address-right">TIJUANA:</th>
                        	<th class="title-address-left">MEXICALI:</th>
                        </tr>
                    	<tr>
                        	<td class="right">Av. Padre Kino 4300 Zona R&iacute;o, CP. 22320,<br />
                            Tel. (664) 900-9010</td>
                        	<td class="left">Calz. Justo Sierra 1233 Fracc. Los Pinos, CP. 21230,<br />
                            Tel. (686) 900-9010</td>
                        </tr>
                        <tr>
                        	<th class="title-address-right">ENSENADA:</th>
                        	<td class="hours">HORARIO DE SERVICIO</td>
                        </tr>
                        <tr>
                        	<td class="right">Ensenada Av. Balboa 146 esq.<br />
							L&oacute;pez Mateos Fracc. Granados<br />
                            Tel. (646) 900-9010</td>
                        	<td class="hours">Lunes a Viernes de 8:00 a.m. a 6:00 p.m.<br />
                            S&aacute;bado de 8:00 a.m. a 1:20 p.m.</td>
                        </tr>
                </table>

        </div>

        <div class="folio">
        	<div class="os-type">
        		COTIZACI&Oacute;N
        	</div>
        	<div class="info">
            	<div class="service-order">
                	<table class="order-date radius">
                            <tr class="gray">
                            	<td>FECHA</td>
                            </tr>
                            <tr class="white">
                            	<td><b>
								'.date("Y-m-d", strtotime($cotizacion[0]->est_date)).'
								</b></td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
	<br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="2" class="title grey">CLIENTE</th>
                    </tr>
                    <tr>
                        <td class="col-2">Nombre: </td>
                        <td class="col-10" style="text-align:left;"><b>'.$cotizacion[0]->est_customer.'</b></td>
                    </tr>
            </table><br>
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th colspan="6" class="title grey">VEH&Iacute;CULO</th>
                    </tr>
                    <tr>
                        <td class="col-1">Modelo: </td>
                        <td class="col-4" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_brand.'</b></td>
                        <td class="col-1">A&ntilde;o: </td>
                        <td class="col-2" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_model.'</b></td>
                        <td class="col-1">Color: </td>
                        <td class="col-3" style="text-align:left;"><b>'.$cotizacion[0]->est_vehicle_color.'</b></td>
                    </tr>
            </table>
        </div>
    </div>
	<br><br><br>
	<div class="row">
    	<table class="radius services-head" style="width:100%;">
        	<tr>
            	<th class="first-cell col-1"></th>
            	<th class="col-1">CANTIDAD</th>
            	<th class="col-3">SERVICIO</th>
            	<th class="col-2">TOTAL<br>REFACCIONES</th>
            	<th class="col-2">TOTAL<br>MANO DE OBRA</th>
            	<th class="col-2">TOTAL</th>
			</tr>
        </table>
    	<table class="services" style="width:100%;">
		';

		foreach($servicios_cotizacion as $key => $row)
		{
			$html = $html.'<tr>
					<td class="first-cell col-1">'.(1+$key).'</td>
					<th class="col-1">'.$row->ess_quantity.'</th>
					<th class="col-3" style="text-align:left;">'.$servicios[$row->ess_service].'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_unit_price).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_handwork).'</th>
					<th class="col-2">'.money_format('%(#10n', $row->ess_total_price).'</th>
                </tr>
				<tr>
					<td class="first-cell col-1"></td>
					<th class="col-1"></th>
					<th class="col-3" style="font-size:9px;"><i style="padding-left:10px; ">'.$row->ess_description.'</i></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
					<th class="col-2"></th>
				</tr>';
		}

		$html = $html.'
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">SUB TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $total[0]->total).'</p></td>
                </tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td class="total-text" style="">IVA</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $iva).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
            	<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="3" ></td>
                	<td  class="total-text" style="">GRAN TOTAL</td>
                	<td class="total grey"><p>'.money_format('%(#10n', $great_total).'</p></td>
                </tr>
				<tr>
                  	<td class="first-cell col-1"></td>
					<td colspan="5"><br></td>
				</tr>
        </table>
    </div>
	<br><br><br>
	<div class="row">
    	<div class="customer-info">
   	  		<table class="radius" style="font-size:10px;">
                    <tr class="first-row">
                        <th class="title grey">INFORMACI&Oacute;N</th>
                    </tr>
                    <tr>
                        <td class="col-6">*** Esta cotizaci&oacute;n ser&aacute; v&aacute;lida durante 30 d&iacute;as a partir de esta fecha.	</td>
                    </tr>
            </table><br>
			<h3>Optima Automotriz, S.A. de C.V.</h3>
			<h3>Departamento de Servicio</h3>
        </div>
    </div>
</div>
';

		$this->load->library('mpdf');
		$this->mpdf->writeHTML($html);
		$new_name = 'documents/estimates/'.substr(str_shuffle(MD5(microtime())), 0, 10).'.pdf';
		$this->mpdf->Output($new_name, 'F');
		return $new_name;
	}

	// Ajax functions

	function obtener_cotizaciones_ajax($id)
	{
		$cotizaciones = $this->Cotizacionesmodel->obtener_cotizaciones_por_orden($id);

		echo json_encode($cotizaciones);
	}

}

?>
