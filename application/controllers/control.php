<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Controlmodel');
		$this->load->model('Permisomodel');
		$this->load->model('Usuariomodel');
		$this->load->model('Productividadmodel');
		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('pdf');
		$this->load->library('session');
		$this->load->library('form_validation');
		
		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
	}
	
	public function index()
	{
		if(empty($_GET['taller'])){
			$taller=$_SESSION['sfidws'];
			}else{
				$taller=$_GET['taller'];}	
				
		$data['hrsTrab']=0;
		if(empty($_GET['fecha'])){$date=date('Y-m-d');}else{$date=$_GET['fecha'];}
		$data['fecha']=$date;
		$data['flash_message'] = $this->session->flashdata('message');
		if(empty($_GET['asesor'])){$ase=0;}else{$ase=$_GET['asesor'];}
		if($ase==0){
	     $data['res']=$this->Controlmodel->checkManodeObraTaller($date,$taller);}
		else{
		$data['res']=$this->Controlmodel->checkManodeObraAsesor($date,$ase);
			}
		$data['asesor'] = $ase;
		$data['taller'] = $taller;
		$data['usuario'] = $this->Usuariomodel->listaUsuarios(0);
		$data['os'] = $this->Productividadmodel->obtenerOSHojaAzul($taller,$date,$date,$ase);
		$data['lista']=$this->Controlmodel->citasPorHora($date,$taller,$ase);
		$data['mecanicos']=$this->Controlmodel->mecanicos($date,$taller,$ase);
		$data['capacidad']=$this->Controlmodel->capacidad($taller);	
		$data['noautos']=$this->Controlmodel->noAutos($date,$ase);	
		$data['servicios']=$this->Controlmodel->servicios($date,$ase);
		$data['numCarriOver']=$this->Controlmodel->numeroCarriOver($date,$ase);
		$data['tot']=$this->Controlmodel->checkOSP($date,$ase);		
		$this->load->view('control/vista',$data);
	}
	
	function pdfAzul(){
      $pdf = new TCPDF('L', PDF_UNIT, 'US Legal', true, 'UTF-8', false);    
      $pdf->AddPage(); 

      $html='<table  width="100%;" style="min-width:1024px; font-size:8px;" >
								<thead style="color:#3ca1ca;">
								<tr>
<th style="width:10px;"  ></th>                                
<th colspan="5" style="
border-bottom:1px solid #41abdd;
border-right:1px solid #41abdd;
border-top:2px solid #41abdd;
border-left:2px solid #41abdd;
text-align:center">
										INFORMACI&Oacute;N DE LA ORDEN DE REPARACI&Oacute;N
									</th>
<th colspan="4" style="border-bottom:1px solid #41abdd;border-right:1px solid #41abdd;border-top:2px solid #41abdd; text-align:center">AVALUO INICIAL
									</th>
<th colspan="2" style="border-bottom:1px solid #41abdd;border-right:1px solid #41abdd;border-top:2px solid #41abdd; text-align:center">RECORDATORIO
									</th>
<th style="border-bottom:1px solid #41abdd;border-right:1px solid #41abdd;border-top:2px solid #41abdd; text-align:center">
FALLA PRINCIPAL</th>
 <th rowspan="2" style="text-align:center;border-top:2px solid #41abdd; border-left:1px solid #41abdd;background-color:#b9f3ff; border-bottom:2px solid #41abdd; font-size:6px; ">TOTAL<br>HORAS<br>FACTURADAS
									</th>                                    
                                    
<th rowspan="2" style="border-bottom:2px solid #41abdd;border-left:1px solid #41abdd; border-top:2px solid #41abdd; border-right:2px solid #41abdd; text-align:center; font-size:7px;">
M<br>E<br>C<br>A<br>N<br>I<br>C<br>O
</th>
<th rowspan="2" style="border-bottom:2px solid #41abdd; border-top:2px solid #41abdd; border-right:2px solid #41abdd; text-align:center;font-size:6px;"> TRABAJO <br>TERMINADO<br>CONTACTO<br>C/CLIENTE
                                    
									</th>
								</tr>
  <tr style="text-align:center; font-size:11px;">
                              
                                <th style="width:10px; border-bottom:2px solid #41abdd;    ">
                               
                                </th>
									<th class="highlight"  style=" text-align:center;  border-bottom:2px solid #41abdd;  border-left:2px solid #41abdd;  background-color:#b9f3ff; font-size:7px;">
										No. O.R.
									</th>
									<th style="text-align:center; border-bottom:2px solid #41abdd;  border-left:1px solid #41abdd; font-size:7px; ">
										 No. TORRE
									</th>
									<th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff; border-bottom:2px solid #41abdd; font-size:7px;">
									CLIENTE
									</th>
<th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd; font-size:7px;">TEL. DEL CLIENTE</th>

<th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff; border-bottom:2px solid #41abdd; font-size:7px;">
										 MARCA/MOD
									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd; font-size:7px;">
                                  TIEMPO <br>DE<br>ENTRADA
									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff; border-bottom:2px solid #41abdd; font-size:7px; ">
										 TIEMPO <br>PROMETIDO
									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd; font-size:7px;">
										 HORAS<BR>EN O.R.
									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff;border-bottom:2px solid #41abdd; font-size:7px; ">
                                    PQT-MENU
									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd; font-size:7px;">
										STATUS
									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; background-color:#b9f3ff;border-bottom:2px solid #41abdd; font-size:7px;">ESCRITO<br>POR									</th>
                                    <th style="text-align:center; border-left:1px solid #41abdd; border-bottom:2px solid #41abdd; font-size:7px;">

										NOTAS DEL TRABAJO
									</th>
									
                                   
								</tr>                                
                                
								</thead></table>';								
								
								
								

      $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
      $pdf->Output('Hoja_de_entrega.pdf', 'I');    
 
		}
		
		
		
		function updateManodeObra(){
		if($_GET['asesor']==0){
	        $res=$this->Controlmodel->checkManodeObraTaller($_GET['fecha'],$_GET['taller']);}
		else{
			$res=$this->Controlmodel->checkManodeObraAsesor($_GET['fecha'],$_GET['asesor']);
			}
		
		if($res){
			$res=$this->Controlmodel->updateManodeObra($res[0]->ID,$_GET['valor']);
			}
			else{
				$this->Controlmodel->insertManodeObra($_GET['fecha'],$_GET['valor'],$_GET['asesor'],$_GET['taller']);
				}	
			
		
    }
	
	
}