<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminuevos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ordendeserviciomodel');
		$this->load->model('Imagenmodel');
		$this->load->model('Multipuntosmodel');
		$this->load->model('Permisomodel');
		$this->load->model('Productividadmodel');
		$this->load->model('Seminuevosmodel');
		$this->load->library('curl');
		$this->load->helper('url');
		$this->load->library('pdf');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('form_validation');
		
		session_start();
		if(!isset($_SESSION['sfid'])) {redirect('acceso');}
		$this->sfid = $_SESSION['sfid'];
		$this->sfemail = $_SESSION['sfemail'];
		$this->sfname = $_SESSION['sfname'];
		$this->sfrol = $_SESSION['sfrol'];
		$this->sftype = $_SESSION['sftype'];
		$this->sfworkshop = $_SESSION['sfworkshop'];
	}
	
	
	
	 function index(){
            $data['infoVin'] = $this->Multipuntosmodel->getVinInfo($_GET['id']);
			$data['orden'] = $_GET['id'];
            $this->load->view('seminuevos/index', $data);
            //$html = file_get_contents('http://stackoverflow.com/questions/ask');
            //echo $html;
		}
        
     function multipuntos(){
            //$data['orden']=$this->Multipuntosmodel->getMulti($orden);
            $this->load->view('seminuevos/multipuntos');
            //$html = file_get_contents('http://stackoverflow.com/questions/ask');
            //echo $html;
		}

	public function guardar()
	{
		$myArray = array(
			'sem_noSerie' => $_POST['vin'],
			'sem_kilometraje' => $_POST['kilometraje'],
			'sem_anio' => $_POST['anio'],
			'sem_modelo' => $_POST['modelo'],
			'sem_color' => $_POST['color'],
			'sem_placas' => $_POST['placas'],
			'sem_c1_cumpleEstandar' => $_POST['check1'],
			'sem_c2_requiereServicio' => $_POST['check2'],
			'sem_costo_debajoCofre' => $_POST['debajoCofre'],
			'sem_c3_cumpleEstandar' => $_POST['check3'],
			'sem_c4_requiereServicio' => $_POST['check4'],	
			'sem_costo_defensaDelantera' => $_POST['defensaDelantera'],	
			'sem_c5_cumpleEstandar' => $_POST['check5'],
			'sem_c6_requiereServicio' => $_POST['check6'],
			'sem_costo_parrilla' => $_POST['parrilla'],	
			'sem_c7_cumpleEstandar' => $_POST['check7'],
			'sem_c8_requiereServicio' => $_POST['check8'],
			'sem_costo_cofre' => $_POST['cofre'],	
			'sem_c9_cumpleEstandar' => $_POST['check9'],
			'sem_c10_requiereServicio' => $_POST['check10'],
			'sem_costo_salDelanteraIzquierda' => $_POST['salpicaderaDelanteraIzquierda'],	
			'sem_c11_cumpleEstandar' => $_POST['check11'],
			'sem_c12_requiereServicio' => $_POST['check12'],
			'sem_costo_puerDelanteraIzquierda' => $_POST['puertaDelanteraIzquierda'],	
			'sem_c13_cumpleEstandar' => $_POST['check13'],
			'sem_c14_requiereServicio' => $_POST['check14'],
			'sem_costo_puerTraseraIzquierda' => $_POST['puertaTraseraIzquierda'],	
			'sem_c15_cumpleEstandar' => $_POST['check15'],
			'sem_c16_requiereServicio' => $_POST['check16'],
			'sem_costo_salTraseraIzquierda' => $_POST['SalpicaderaTraseraIzquierda'],	
			'sem_c17_cumpleEstandar' => $_POST['check17'],
			'sem_c18_requiereServicio' => $_POST['check18'],
			'sem_costo_cajuela' => $_POST['cajuela'],
			'sem_c19_cumpleEstandar' => $_POST['check19'],
			'sem_c20_requiereServicio' => $_POST['check20'],
			'sem_costo_defensaTrasera' => $_POST['defensaTrasera'],	
			'sem_c21_cumpleEstandar' => $_POST['check21'],
			'sem_c22_requiereServicio' => $_POST['check22'],
			'sem_costo_salTraseraDerecha' => $_POST['salpicaderaTraseraDerecha'],
			'sem_c23_cumpleEstandar' => $_POST['check23'],
			'sem_c24_requiereServicio' => $_POST['check24'],
			'sem_costo_puerTraseraDerecha' => $_POST['puertaTraseraDerecha'],
			'sem_c25_cumpleEstandar' => $_POST['check25'],
			'sem_c26_requiereServicio' => $_POST['check26'],
			'sem_costo_puerDelanteraDerecha' => $_POST['puertaDelanteraDerecha'],
			'sem_c27_cumpleEstandar' => $_POST['check27'],
			'sem_c28_requiereServicio' => $_POST['check28'],
			'sem_costo_salDelanteraDerecha' => $_POST['salpicaderaDelanteraDerecha'],
			'sem_c29_cumpleEstandar' => $_POST['check29'],
			'sem_c30_requiereServicio' => $_POST['check30'],
			'sem_costo_techo' => $_POST['techo'],
			'sem_c31_cumpleEstandar' => $_POST['check31'],
			'sem_c32_requiereServicio' => $_POST['check32'],
			'sem_costo_vidrios' => $_POST['vidrios'],
			'sem_fechaHoy' => $_POST['fec'],
			'sem_torre' => $_POST['torre'],
			'sem_costo_reparacionApariencia' => $_POST['costo'],
			'sem_notas' => $_POST['nota'],
			'sem_imagen' => $_POST['img']
			);

			$this->Seminuevosmodel->insertar($myArray);
	}
		// 	*/
		// 			$file = fopen("archivo.txt", "w");
		// 			foreach ($myModel as $key) {
		// 				# code...
		// 			fwrite($file, $key . PHP_EOL);
		// 			}
		// 			fclose($file);
		// }

		function listado()
		{
			$this->load->view('form/registros');
		}

		function graficas()
		{
			$this->load->view('form/graficas');
		}

		function GetLista(){
			$result = $this->Seminuevosmodel->lista_registros();
			$json ='[';
			$first = 1;
			foreach($result as $item){
				if($first == 1){$first = 2;}else{$json.=',';}
				$json.='{"value":"'. $item->que_ch1.'"}';
			}
			$json.=']';
			echo $json;
		}
}

