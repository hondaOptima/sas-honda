	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class traslados extends CI_Controller
	{
		function __construct()
		{ 
			parent::__construct();
			$this->load->model('trasladosmodel');
			$this->load->library('email');
			$this->load->library('session');

			$this->load->model('Permisomodel');
			// $this->per=$this->Permisomodel->acceso();
			// $this->per=$this->Permisomodel->permisosVer('Usuario');
			session_start();

			$this->allowed_users = array(96, 97,31);

			if(!isset($_SESSION['sfid'])) {
				$this->sfid = '0';
				$this->sfemail = '0';
				$this->sfname = '0';
				$this->sfrol = '0';
				$this->sftype = '0';
				$this->sfworkshop = '0';
			}else{
				$this->sfid = $_SESSION['sfid'];
				$this->sfemail = $_SESSION['sfemail'];
				$this->sfname = $_SESSION['sfname'];
				$this->sfrol = $_SESSION['sfrol'];
				$this->sftype = $_SESSION['sftype'];
				$this->sfworkshop = $_SESSION['sfworkshop'];
			}
		}

		function index()
		{
			//pone permisos de editado en falso
			$data['usuario'] = false;
			//si el usuario tiene permisos, lo pone en verdadero
			if(in_array($this->sfid, $this->allowed_users))
				$data['usuario'] = true;
			else{
				$data['usuario'] = false;
			}
			//y carga la vista segun los permisos
			$this->load->view('traslados/vista', $data);
		}

		function config(){
			//se checa si el usuario tiene permisos, sino redirige a la tabla
			if(in_array($this->sfid, $this->allowed_users))
				$this->load->view('traslados/config');
			else
				redirect(site_url("traslados"));
		}

		function testaddev() {
			$this->load->view("addevent");
		}

		//--------------------------------------------------------
		//
		//	APIs
		//
		//--------------------------------------------------------

		//-------------------
		//				
		//	Calendario
		//					
		//-------------------

		//funcion que trae los traslados de un mes
		//parametros GET:
		// 'mes' : Numero de mes del año (1-12)
		// 'anio' : Numero del año en cuestión (minimo 1970)
		function getTransferTable()
		{
			$json['status'] = 0;
			$mes = $this->input->get('mes');
			$anio = $this->input->get('anio');
			if(is_numeric($mes) && $mes <= 12 && $mes >= 1 && is_numeric($anio) && strlen($anio) == 4)
			{
				//se obtiene los traslados
				$dbtraslados = $this->trasladosmodel->obtener_traslados($mes, $anio);
				//administra el indice del arreglo de semanas (y conteo para JSON)
				$numSemana = -1;
				//para uso en for
				$diasEnMes = date('t', strtotime("1-$mes-$anio"));
				//para saber el indice actual en arreglo (mirar for)
				$j = 0;
				//para obtener los dias de la semana, en caso 
				$diasSemana = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
				//se inicaliza la variable saliente
				$json['mes'] = array();
				//se inicializan variables para redondeo de mes
				$semanaAntes = false;
				$semanaDespues = false;
				//Dia no del mes:
				// - Cuando se rellena la semana anterior, es la fecha usada en el algoritmo
				// - Cuando se rellena la semana posterior, es la fecha limite del algoritmo
				$diaNoDelMes = '';
				//se recorren los dias en mes. El indice en el arreglo de base de datos es manejado por $j
				for($i = 1; $i <= $diasEnMes; $i++)
				{

					$fecha = date('d-m-Y', strtotime("$i-$mes-$anio"));
					$numdia = date('w', strtotime($fecha));

					//si es domingo, no se registra nada
					if($numdia != 0)
					{
						//chequeo: Que sea un nuevo comienzo de semana laboral (lunes) o sea el inicio del codigo
						if(1 == $numdia || 0 == count($json['mes']))
						{
							$numSemana++;

							$fechaActualTime = strtotime($fecha);
							//si es domingo (a prueba de errores), se queda la fecha actual, sino se obtiene el ultimo domingo
							$fechaDomingo = '';
							if($numdia == 0)
								$fechaDomingo = date('d-m-Y', $fechaActualTime);
							else
								$fechaDomingo = date('d-m-Y', strtotime('last sunday', $fechaActualTime));
							//igual que el domingo, pero con proximo sabado
							$fechaSabado = '';
							if($numdia == 6)
								$fechaSabado = date('d-m-Y', $fechaActualTime);
							else
								$fechaSabado = date('d-m-Y', strtotime('next saturday', $fechaActualTime));
							//se agrega la semana al mes
							array_push($json['mes'], array("numeroSemana" => $numSemana + 1, "rangoSemana" => $fechaDomingo.' - '.$fechaSabado, "dias" => array()));

							//y se checa: La semana comienza antes del primer dia del mes? O termina despues del ultimo dia del mes?
							//check 1: 
							// - Si es la primera semana
							// - Si el numero del domingo es diferente que el ultimo dia del mes del domingo (si el mes pasado termina en domingo, no importa)
							// - Si el numero de dia del domingo obtenido es mayor que el primer dia (1)
							if($numSemana == 0 && date('t', strtotime('-1 day' , strtotime("1-$mes-$anio"))) != date('j', strtotime($fechaDomingo)) && date('j', strtotime($fechaDomingo)) > $i) //------------------------------------------------------------------------
							{
								//Si cumple con todos los parametros, existen dias antes del primer dia del mes, y debe llenarse la semana
								$semanaAntes = true;
								$diaNoDelMes = $fechaDomingo;
							}

							//check 2:
							// - Si es la ultima semana del mes (incluye domingos)
							// - Si el mes del sabado siguiente es diferente al actual
							// - Si el ultimo dia del mes no es domingo
							if($i + 6 >= $diasEnMes && date('n', strtotime($fechaSabado)) != $mes && date('w', strtotime("$diasEnMes-$mes-$anio")) != 0)
							{
								$semanaDespues = true;
								$diaNoDelMes = $fechaSabado;
							}
						}

						//se ejecuta solo si se encontraron dias antes del mes, pero en el rango de la semana
						while($semanaAntes) 
						{
							$numdiaAntes = date('w', strtotime($diaNoDelMes));
							if($numdiaAntes != 0)
							{
								//mismas reglas que el codigo principal
								$traslado = $this->trasladosmodel->obtener_trasladoDia($diaNoDelMes);
								if(count($traslado) > 0)
								{
									//se genera un nuevo dia
									$dia = array
									(
										"numdia" => $numdiaAntes,
										"dia" => $diasSemana[$numdiaAntes],
										"activo" => 1,
										"trasladista" => $traslado['nombreTrasladista'],
										"horaEntrada" => $traslado['hora_inicio'],
										"horaSalida" => $traslado['hora_fin'],
										"fecha" => $traslado['fecha'],
										"desactivado" => 'disabled="disabled"',
										"paquetes" => array()
									);

									$paquetes = $this->trasladosmodel->obtener_paquetes($traslado['id']);
									//se obtienen los paquetes del dia
									foreach($paquetes as $paq)
									{
										//se obtienen el origen y destino del paquete
										$origen = $this->trasladosmodel->obtener_ubicacion($paq['origen']);
										$destino = $this->trasladosmodel->obtener_ubicacion($paq['destino']);
										
										//y se obtienen los detalles de los automoviles a transportar
										$numserie1 = $this->trasladosmodel->obtener_detallesNumSerie($paq['numserie1']);
										$numserie2 = $this->trasladosmodel->obtener_detallesNumSerie($paq['numserie2']);

										array_push($dia['paquetes'], array
											(
												"id" => $paq['id'],
												"numeroSerie1" => $numserie1,
												"numeroSerie2" => $numserie2,
												"horas" => $paq['horasTraslado'],
												"origen" => $origen,
												"destino" => $destino
											)
										);
									}
								}
								else
								{
									$dia = array
									(
										"numdia" => $numdiaAntes,
										"dia" => $diasSemana[$numdiaAntes],
										"activo" => 0,
										"desactivado" => 'disabled="disabled"',
										"fecha" => $diaNoDelMes
									);
								}
								//y se asigna la informacion del dia a la semana actual
								array_push($json['mes'][$numSemana]['dias'], $dia);
							}

							//ya procesado el dia, se obtiene la siguiente fecha y se analiza
							$timeSiguiente = strtotime('+1 day', strtotime($diaNoDelMes));
							//si el comienzo del mes solicitado es el mismo dia que el obtenido, se termina el ciclo
							if(strtotime("1-$mes-$anio") == $timeSiguiente)
								$semanaAntes = false;
							//sino, se asigna la siguiente fecha
							else
								$diaNoDelMes = date('d-m-Y', $timeSiguiente);
						}
						
						//codigo principal: Se llenan los dias del mes
						//se crea un nuevo dia
						$dia = array();
						//si la fecha del siguiente traslado es la actual, se llena el dia con los mismos
						if(count($dbtraslados) > 0 && date('j', strtotime($dbtraslados[$j]['fecha'])) == $i) 
						{
							//se genera un nuevo dia
							$dia = array
							(
								"numdia" => $numdia,
								"dia" => $diasSemana[$numdia],
								"activo" => 1,
								"trasladista" => $dbtraslados[$j]['nombreTrasladista'],
								"horaEntrada" => $dbtraslados[$j]['hora_inicio'],
								"horaSalida" => $dbtraslados[$j]['hora_fin'],
								"fecha" => $dbtraslados[$j]['fecha'],
								"desactivado" => '',
								"paquetes" => array()

							);

							$paquetes = $this->trasladosmodel->obtener_paquetes($dbtraslados[$j]['id']);
							//se obtienen los paquetes del dia
							foreach($paquetes as $paq)
							{
								//se obtienen el origen y destino del paquete
								$origen = $this->trasladosmodel->obtener_ubicacion($paq['origen']);
								$destino = $this->trasladosmodel->obtener_ubicacion($paq['destino']);
								
								//y se obtienen los detalles de los automoviles a transportar
								$numserie1 = $this->trasladosmodel->obtener_detallesNumSerie($paq['numserie1']);
								$numserie2 = $this->trasladosmodel->obtener_detallesNumSerie($paq['numserie2']);

								array_push($dia['paquetes'], array
									(
										"id" => $paq['id'],
										"numeroSerie1" => $numserie1,
										"numeroSerie2" => $numserie2,
										"horas" => $paq['horasTraslado'],
										"origen" => $origen,
										"destino" => $destino
									)
								);
							}

							//ya que se obtuvo la información del traslado, se pasa al siguiente. Si dicho traslado no existe, se vuelve al primero (inalcanzable)
							if(count($dbtraslados) <= ++$j)
								$j = 0;
						}
						//en caso contrario, se llena vacio
						else
						{
							$dia = array
							(
								"numdia" => $numdia,
								"dia" => $diasSemana[$numdia],
								"activo" => 0,
								"desactivado" => '',
								"fecha" => $fecha
							);
						}
						//y se asigna la informacion del dia a la semana actual
						array_push($json['mes'][$numSemana]['dias'], $dia);

						//si se va a comenzar el ciclo para relleno de semana posterior, se modifica la fecha (para que no ponga la misma 2 veces)
						if($semanaDespues && $i == $diasEnMes)
						{
							$fecha = date('d-m-Y', strtotime('+1 day', strtotime($fecha)));
							//y se inicia el ciclo
							while($semanaDespues) 
							{
								//el algoritmo es casi el mismo que el de la semana antes del mes, pero usa la fecha usada en el codigo principal
								$numdiaDespues = date('w', strtotime($fecha));
								if($numdiaDespues != 0)
								{
									//mismas reglas que el codigo principal
									$traslado = $this->trasladosmodel->obtener_trasladoDia($fecha);

									if(count($traslado) > 0)
									{
										//se genera un nuevo dia
										$dia = array
										(
											"numdia" => $numdiaDespues,
											"dia" => $diasSemana[$numdiaDespues],
											"activo" => 1,
											"trasladista" => $traslado['nombreTrasladista'],
											"horaEntrada" => $traslado['hora_inicio'],
											"horaSalida" => $traslado['hora_fin'],
											"fecha" => $traslado['fecha'],
											"desactivado" => 'disabled="disabled"',
											"paquetes" => array()
										);

										$paquetes = $this->trasladosmodel->obtener_paquetes($traslado['id']);
										//se obtienen los paquetes del dia
										foreach($paquetes as $paq)
										{
											//se obtienen el origen y destino del paquete
											$origen = $this->trasladosmodel->obtener_ubicacion($paq['origen']);
											$destino = $this->trasladosmodel->obtener_ubicacion($paq['destino']);
											
											//y se obtienen los detalles de los automoviles a transportar
											$numserie1 = $this->trasladosmodel->obtener_detallesNumSerie($paq['numserie1']);
											$numserie2 = $this->trasladosmodel->obtener_detallesNumSerie($paq['numserie2']);

											array_push($dia['paquetes'], array
												(
													"id" => $paq['id'],
													"numeroSerie1" => $numserie1,
													"numeroSerie2" => $numserie2,
													"horas" => $paq['horasTraslado'],
													"origen" => $origen,
													"destino" => $destino
												)
											);
										}
									}
									else
									{
										$dia = array
										(
											"numdia" => $numdiaDespues,
											"dia" => $diasSemana[$numdiaDespues],
											"activo" => 0,
											"desactivado" => 'disabled="disabled"',
											"fecha" => $fecha
										);
									}
									//y se asigna la informacion del dia a la semana actual
									array_push($json['mes'][$numSemana]['dias'], $dia);
								}

								//ya procesado el dia, se obtiene la siguiente fecha y se analiza
								$timeSiguiente = strtotime('+1 day', strtotime($fecha));
								//si la fecha siguiente sobrepasa el ultimo sabado a acceder, se termina el ciclo
								if(strtotime($diaNoDelMes) < $timeSiguiente)
									$semanaDespues = false;
								//sino, se asigna la siguiente fecha
								else
									$fecha = date('d-m-Y', $timeSiguiente);
							}
						}
					}
				}
			}
			else
			{
				$json['status'] = 1;
				$json['message'] = 'Datos ingresados invalidos';
			}
			//echo json_encode($json);
			$this->output->set_output(json_encode($json));
		}

		function insertTransfer_setDate()
		{
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{
				$fecha = $this->input->post('fecha');
				//se checa si se ingreso el parametro (fecha)
				if( $fecha != '' )
				{
					//luego, se comprueba que la fecha sea valida
					
					//se convierte a tiempo
					$time = strtotime($fecha);
					
					//y se checa: 
					//1 - Que no haya fallado en crear la fecha
					//2 - Que la fecha convertida sea la misma que la original (que PHP no la corrija automaticamente)
					if($time && date('d-m-Y', $time) == $fecha)
					{
						//se crea el traslado y se verifica si pudo insertarse
						$resultado = $this->trasladosmodel->inicializar_traslado($fecha);
						if($resultado['result'] === TRUE)
						{
							$id = $resultado['id'];
							//si se crea el traslado, se obtiene la configuracion del dia del traslado
							$configuracion = $this->trasladosmodel->obtener_configuracion($id);
							
							if(count($configuracion) > 0)
							{
								$json['message'] = "Traslado creado satisfactoriamente";
								//y se obtiene la informacion de los paquetes a crear
								$json['paquetes'] = array();
								foreach($configuracion as $confpaquetes)
								{
									$origen = $this->trasladosmodel->obtener_ubicacion($confpaquetes['origen']);
									$destino = $this->trasladosmodel->obtener_ubicacion($confpaquetes['destino']);

									$paquete = $this->trasladosmodel->crear_trasladoPaquete($id, $confpaquetes['origen'], $confpaquetes['destino'], $confpaquetes['horasTraslado']);

									array_push($json['paquetes'], array
									(
										"id" => $paquete,
										"numpaquete" => $confpaquetes['paq_orden'],
										"horas" => $confpaquetes['horasTraslado'],
										"origen" => $origen,
										"destino" => $destino,
									));
								}
							}
							else
							{
								$json['status'] = 1;
								$json['message'] = 'No se encontro la configuracion correspondiente';
							}
						}
						else
						{
							//si no se pudo insertar, se verifica el codigo de error.
							//El error importante es saber si ya existe la fecha (codigo duplicado)
							if($resultado['error'] == '1062')
							{
								$json['status'] = 2;
								$json['message'] = 'La fecha ingresada ya cuenta con traslado. Intente con otra';
							}
							else
							{
								$json['status'] = 1;
								$json['message'] = 'Ocurrio un error al insertar trasaldo';
							}
						}
					}
					else
					{
						$json['status'] = 1;
						$json['message'] = 'La fecha ingresada no es valida';
					}
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = 'Los datos ingresados son invalidos';
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}

			$this->output->set_output(json_encode($json));
		}

		function insertTransfer_removeDate()
		{
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{
				$fecha = $this->input->post('fecha');

				$time = "";

				if($fecha != '')
				{
					$time = strtotime($fecha);
				}

				//se checa si se ingreso el parametro (fecha) y si es correcto
				if($fecha != '' && $time && date('d-m-Y', $time) == $fecha)
				{
					$traslado = $this->trasladosmodel->obtener_trasladoDia($fecha);
					//se checa si existe el traslado del dia obtenido
					if(count($traslado) > 0)
					{
						//Si hay traslado, checa si tiene vins asignados en los paquetes
						$sinvins = true;
						$paquetes = $this->trasladosmodel->obtener_paquetes($traslado['id']);
						foreach($paquetes as $paq)
						{
							if(!($paq['numserie1'] == "" && $paq['numserie2'] == ""))
							{
								$sinvins = false;
							}
						}
						//en caso de que existan vins asignados, no deja eliminar el dia
						if($sinvins)
						{
							if($this->trasladosmodel->borrar_traslado($traslado['id']))
							{
								$json['message'] = 'Dia eliminado satisfactoriamente';
							}
							else
							{
								$json['status'] = 1;
								$json['message'] = 'Ocurrio un error al eliminar el dia';
							}
						}
						else
						{
							$json['status'] = 1;
							$json['message'] = 'Por favor desasigne los automoviles antes de eliminar un dia';
						}
					}
					else
					{
						$json['status'] = 1;
						$json['message'] = 'La fecha ingresada no cuenta con traslado';
					}
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = 'Los datos ingresados son invalidos'.$fecha;
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}
			$this->output->set_output(json_encode($json));
		}

		function insertTransfer_setTransferPerson()
		{
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{

				$fecha = $this->input->post('fecha');
				$trasladista = $this->input->post('trasldaista');
				$horainicio = $this->input->post('horainicio');
				$horafin = $this->input->post('horafin');

				//se revisan los parametros 
				if($horainicio != '' && $horafin != '' && $fecha != '')
				{
					//se comprueba si las horas son realmente horas (formato 24h) y si el ID es numerico
					$checkhoraI = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $horainicio);
					$checkhoraF = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $horafin);
					$checkFecha = strtotime($fecha) && date('d-m-Y', strtotime($fecha)) == $fecha;

					if($checkhoraI && $checkhoraF && $checkFecha) 
					{
						//primero, se obtiene el ID que le corresponde a la fecha
						$queryID = $this->trasladosmodel->obtener_id_traslado($fecha);
						//si hubo resultado, obtiene el ID
						if(count($queryID->result()) > 0) 
						{
							$id = $queryID->row()->id;
							//se crea el traslado y se verifica si pudo insertarse
							if($this->trasladosmodel->asignar_trasladista($trasladista, $id, $horainicio, $horafin))
							{
								$json['message'] = "Los datos del traslado se han guardado satisfactoriamente";
							}
							else
							{
								$json['status'] = 1;
								$json['message'] = 'Ocurrio un error al insertar trasaldo';	
							}
						}
						else
						{
							$json['status'] = 2;
							$json['message'] = 'La fecha elegida no cuenta con trasaldo';
						}
						
					}
					else
					{
						$json['status'] = 1;
						$json['message'] = 'Los datos ingresados no son del formato correspondiente';
					}
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = 'Los datos ingresados son invalidos';
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}

			$this->output->set_output(json_encode($json));
		}

		function insertTransfer_setPackage()
		{
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{
				//verificar parametros (tipo y nombre)
				$id = $this->input->get('id');
				$ns = $this->input->get('numserie');
				$numero = $this->input->get('numero');

				//requerimiento: Existen dos numeros de serie como maximo, pero el minimo es uno
				if($id != '' && ($numero == 1 || $numero == 2))
				{
					$asignacion = array("numserie".$numero => $ns);
					$datavin = $this->trasladosmodel->obtener_detallesNumSerie($ns);

					$paquete = $this->trasladosmodel->obtener_paquete($id);
					$idns2 = '';

					if(count($paquete) > 0 && $paquete['numserie'.$numero] != '')
					{
						$datavin2 = $this->trasladosmodel->obtener_detallesNumSerie($paquete['numserie'.$numero]);
						if(count($datavin2) > 0)
							if($datavin2['status'] != 0)
								$idns2 = $datavin2['id'];
					}
					//chequeo:
					// - Si el paquete no es vacio, que sea diferente a los existentes
					if($ns == "" || ($ns != $paquete['numserie1'] && $ns != $paquete['numserie2']))
					{
						//chequeos:
						// - Que se hallan encontrado datos del carro (se encuentre en solicitud de traslado) (cuando no se agregue un carro en blanco)
						// - Que se haya podido asignar el carro al dia (Esta es la unica accion validada si se agrega un carro en blanco)
						// - Que se haya podido cambiar el estatus del carro a "en traslado" (cuando no se agregue un carro en blanco)
						if(($ns == '' || count($datavin) > 0) 
							&& $this->trasladosmodel->asignar_trasladoPaquete($id, $asignacion)
							&& ($ns == '' || $this->trasladosmodel->cambiar_trasladoStatus($datavin['id'], 1))
							)
						{
							$json['message'] = "Datos asignados correctamente";

							if($idns2 != '')
								if($this->trasladosmodel->cambiar_trasladoStatus($idns2, 0))
									$json['message'] .= '.';

							if($ns != '') {
								$traslado = $this->trasladosmodel->obtener_traslado($paquete['traslado']);
								$destino = $paquete['destino'];
                                $this->_sendConfirmationMail($datavin, $traslado['fecha'],$destino);
                            }
						}
						else
						{
							$json['status'] = 1;
							$json['message'] = "Error al guardar los datos";
						}
					}
					else
					{
						$json['status'] = 1;
						$json['message'] = "El automovil seleccionado ya se encuentra en proceso de traslado";
					}
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = "Los datos ingresados son invalidos";
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}

			$this->output->set_output(json_encode($json));
		}

		private function _sendConfirmationMail($paquete, $fecha, $destino)
		{
			$correos = array();
			$ubicacion = $this->trasladosmodel->obtener_ubicacion($destino);
			$mail = $ubicacion['mail'];
			

			$this->email->clear();

			$this->email->from($this->sfemail, 'Honda Optima');

			
	        $this->email->to($mail);

	        $this->email->subject('Un automovil solicitado fue aprobado para traslado');
			$this->email->message("Email de prueba<br>
				VIN: ".$paquete['vin']."<br>
				Origen: ".$paquete['origen']."<br>
				Destino: ".$paquete['destino']."<br>
				Modelo: ".$paquete['modelo']." ".$paquete['anio']." ".$paquete['color']."<br>
				Fecha: ".$fecha);
			$this->email->set_mailtype('html');

			$this->email->send();
		}

		function getAllSerialNums()
		{
			//llama al API de catalogo de VINs
			$json["status"] = 0;
			$status = $this->input->get('status');

			$api = $this->trasladosmodel->obtener_catalogoNumSerie($status);

			if(count($api) > 0)
			{
				$json['vins'] = $api;	
			}
			else
			{
				$json['status'] = 1;
				$json['message'] = "No se encontraron resultados";
			}

			$this->output->set_output(json_encode($json));
		}

		function getDetailSerialNum()
		{
			$json["status"] = 0;
			$ns = $this->input->get('vin');

			if($ns)
			{
				$api = $this->trasladosmodel->obtener_detallesNumSerie($ns);

				if(count($api) > 0)
				{
					$json['vin'] = $api;	
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = "No se encontraron resultados";
				}
			}
			else
			{
				$json['status'] = 2;
				$json['message'] = 'Los datos obtenidos son invalidos';
			}

			$this->output->set_output(json_encode($json));
		}

		//-------------------
		//
		//	Configuracion
		//
		//-------------------

		//funcion para obtener la configuracion
		// parametros POST:
		// - datos : Identificador para determinar la informacion que se traera
		function configuration_getConfiguration()
		{
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{
				$tipo = $this->input->post('datos');
				if( $tipo === 'dias' || $tipo === 'paquetes' )
				{
					//se prepara para recibir los paquetes y se buscan
					$json['paquetes'] = array();
					$catalogopaquetes = $this->trasladosmodel->config_catalogoPaquetes();

					foreach($catalogopaquetes as $pack)
					{
						//se obtienen el origen y destino del paquete
						$origen = $this->trasladosmodel->obtener_ubicacion($pack['origen']);
						$destino = $this->trasladosmodel->obtener_ubicacion($pack['destino']);

						//y se agrega al arreglo de paquetes
						array_push($json['paquetes'], array
						(
							"id" => $pack['id'],
							"horas" => $pack['horasTraslado'],
							"origen" => $origen,
							"destino" => $destino
						));
					}

					//si se elige la opcion de traer dias, trae ambos paquetes y dias
					if($tipo === 'dias')
					{
						$json['dias'] = array();
						//mismo arreglo que en el metodo "getTransferTable" pero lower case
						$diasSemana = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");

						//como no existe domingo, se comienza en 1
						for($i = 1; $i < count($diasSemana); $i++)
						{

							$dia = array();
							//se asigna el numero de dia
							$dia['numdia'] = $i;
							//se obtienen los paquetes del dia

							$paquetes = $this->trasladosmodel->config_obtenerPaquetes($i);

							//se prepara para recibir los paquetes
							$dia['paquetes'] = array();
							foreach($paquetes as $paq)
							{
								//se obtienen el origen y destino del paquete
								$origen = $this->trasladosmodel->obtener_ubicacion($paq['origen']);
								$destino = $this->trasladosmodel->obtener_ubicacion($paq['destino']);

								//y se agrega al arreglo de paquetes
								array_push($dia['paquetes'], array
								(
									"id" => $paq['paquete'],
									"orden" => $paq['paq_orden'],
									"horas" => $paq['horasTraslado'],
									"origen" => $origen,
									"destino" => $destino
								));
							}

							//y se ingresa al arreglo de salida con su respectivo nombre
							array_push($json['dias'], $dia);
						}
					}
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = 'Los datos obtenidos son invalidos';
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}

			$this->output->set_output(json_encode($json));
		}

		//funcion que guarda la configuracion de un dia en especifico
		//parametos POST:
		// json - Objeto JSON con la informacion completa:
		//		{ dia: int, paquetes: [int, int..., int] } 
		function configuration_saveConfiguration()
		{
			//variable de salida
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{
				//se obtienen los datos de entrada (en formato JSON)
				$datosJSON = $this->input->post('json');
				//se parsean
				$datos = json_decode($datosJSON, true);
				//y se comprueba de que existan (y en JSON)
				if($datosJSON != '' && $datos != NULL && !is_numeric($datos))
				{
					//se obtiene el numero de dia
					$dia = $datos['dia'];
					$paquetes = array();
					$count = 0;

					foreach($datos['paquetes'] as $paquete)
					{
						array_push($paquetes, array
							(
								"dia" => $dia, 
								"paquete" => $paquete, 
								"paq_orden" => ++$count
							)
						);
					}

					if($this->trasladosmodel->config_guardarDia($paquetes))
	                    $json['message'] = "Configuracion guardada";
	                else
	                {
	                    //para desplegar el dia que ha fallado
	                    $diasSemana = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
	                    
	                    $json['status'] = 1;
	                    $json['message'] = "Error al guardar el dia ".$diasSemana[$dia];
	                }
				}
				else
				{
					$json['status'] = 1;
					$json['message'] = 'Los datos obtenidos son invalidos';
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}

			$this->output->set_output(json_encode($json));

		}

		//funcion para manipular un paquete del lado de la configuracion
		// parametros GET:
		// -- Principal:
		// modalidad - String con el comando que se desea hacer

		// -- parametros de modalidad "insert"
		// origen - ID del lugar de origen del paquete
		// destino - ID del lugar destino del paquete
		// horas - horas de traslado del paquete

		// -- parametros de modalidad "update"
		// __(Se repiten los de "insert")__
		// id - ID del paquete a modificar

		// -- parametros de modalidad "delete"
		// id - ID del paquete a eliminar
		function configuration_changePacket()
		{
			$json['status'] = 0;

			//se checa si el usuario tiene permisos
			if(in_array($this->sfid, $this->allowed_users))
			{
				//se obtiene el modo en que procede la funcion
				$modo = $this->input->post('modalidad');
				//y se prepara para "parsear" el modo
				$insert = false;
				$update = false;
				$delete = false;

				switch(strtolower($modo))
				{
					case "insert":
						$insert = true;
						break;
					case "update":
						$update = true;
						break;
					case "delete":
						$delete = true;
						break;
					default: 
						break;
				}

				if($insert || $update)
				{
					//se prepara para aniadir la informacion
					$set = array();
					$set['origen'] = $this->input->post('origen');
					$set['destino'] = $this->input->post('destino');
					$set['horasTraslado'] = $this->input->post('horas');

					//se checa la integridad de los datos
					if(!in_array('', $set) || ($update && $this->input->post('id') != ''))
					{
						//y dependiendo de la modalidad, se aniade
						if($insert)
						{
							$result = $this->trasladosmodel->config_crearPaquete($set);
							if($result)
							{
								$json['message'] = 'Paquete creado satisfactoriamente';
								$json['id'] = $result;
							}
							else
							{
								$json['status'] = 1;
								$json['message'] = 'Hubo un error al crear el paquete';
							}
						}
						else //if($update)
						{
							$id = $this->input->post('id');

							if($this->trasladosmodel->config_guardarPaquete($set, $id))
							{
								$json['message'] = 'Paquete guardado satisfactoriamente';
							}
							else
							{
								$json['status'] = 1;
								$json['message'] = 'Hubo un error al guardar el paquete';
							}
						}
					}
					else
					{
						$json['status'] = 1;
						$json['message'] = 'Los datos obtenidos son invalidos';
					}
				}
				else
				{
					//doble chequeo: Si es la funcion de delete y si el ID esta asignado
					if($delete && $this->input->post('id') != '')
					{
						$id = $this->input->post('id');

						if($this->trasladosmodel->config_eliminarPaquete($id))
						{
							$json['message'] = 'Paquete eliminado satisfactoriamente';
						}
						else
						{
							$json['status'] = 1;
							$json['message'] = 'Hubo un error al eliminar el paquete';
						}
					}
					else //if(!$update && !$insert && !$delete)
					{
						$json['status'] = 1;
						$json['message'] = 'Los datos obtenidos son invalidos';
					}
				}
			}
			else
			{
				$json['status'] = 8;
				$json['message'] = 'El usuario logueado no tiene los permisos requeridos para ejecutar la accion';
			}

			$this->output->set_output(json_encode($json));
		}

		function configuration_getPlacesCatalog()
		{
			$json['status'] = 0;

			$agencias = $this->trasladosmodel->obtener_catalogoUbicaciones();

			if(count($agencias) > 0)
			{
				$json['agencias'] = array();
				
				foreach($agencias as $ag) {
					array_push($json['agencias'], 
						array(
							"id" => $ag['id'],
							"nombre" => $ag['agencia']
						)
					);
				}
			}
			else
			{
				$json['status'] = 1;
				$json['message'] = "Ocurrio un error al obtener las agencias";
			}
			$this->output->set_output(json_encode($json));
		}

	}

?>