<?php

class Tiposerviciomodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	
	function listarTipos($per){
		$query= $this->db->query(
				'
				SELECT *
				FROM serviceTypes
				where set_isActive=1
				ORDER BY  `serviceTypes`.`set_orden` ASC
				');

		return $query->result();
	}
	
	function agregarTipo($tipo){
         $this->db->insert('serviceTypes', $tipo); 
		  return $this->db->insert_id();
	}
	
	function actualizarTipo($tipo, $id){
         $this->db->update('serviceTypes', $tipo, array('set_idServiceType' => $id)); 
	}
	
	function obtenerTipo($id){
		$query= $this->db->query(
				'SELECT *
				FROM serviceTypes
				WHERE
				set_idServiceType = '.$id.'');
		 return $query->result();
	}
	
	function desactivarTipo($id){

		$query = $this->db->query('UPDATE serviceTypes
			SET set_isActive = 0
			WHERE 
			set_idServiceType = '.$id.'');
	}
	
	function activarTipo($id){

		$query = $this->db->query('UPDATE serviceTypes
			SET set_isActive = 1
			WHERE 
			set_idServiceType = '.$id.'');
	}
	
}

?>