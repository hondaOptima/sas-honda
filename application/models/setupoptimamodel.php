<?php

class Setupoptimamodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('crmtraslados',true);
		$this->db3 = $this->load->database('dboptima', true);
	}

	function CrmToOptima(){
		$ex_crm = $this->db2->query('select * from datos_auto, datos_cliente where data_ = datc_IDdatos_clientes and status_optima is NULL');
		foreach($ex_crm->result() as $row){
			$this->db3->initialize();
			$ex_check = $this->db3->query('select * from vehiculos where vin = "'.$row->data_vin.'"');
			$this->db3->close();
			if($ex_check->num_rows() == 0){
				$calle = preg_replace('/[0-9]/','', $row->datc_calle);
				$matches = intval(preg_replace('/[^0-9]+/', '', $row->datc_calle), 10);
				$ciudad = '';
				switch ($row->datc_ciudad) {
				    case "TIJUANA":
				        $ciudad = 3;
				        break;
				    case "MEXICALI":
				        $ciudad = 2;
				        break;
				    case "ENSENADA":
				        $ciudad = 1;
				        break;
				    default:
				        $ciudad = 3;
				}

				$direccion = array(
						'colonia' => $row->datc_colonia,
						'calle' => $calle,
						'numero' => $matches,
						'codigo_postal' => $row->datc_cp,
						'ciudad' => $ciudad
					);
				$this->db3->initialize();
				$this->db3->insert('direccion', $direccion);
				$dir_id = $this->db3->insert_id();
				$this->db3->close();

				$cliente = array(
					   'nombre' => $row->datc_primernombre ,
					   'sexo' => '' ,
					   'paterno' => $row->datc_apellidopaterno ,
					   'materno' => $row->datc_apellidomaterno ,
					   'edad' => '' ,
					   'telefono' => $row->datc_tcelular ,
					   'email' => $row->datc_email ,
					   'direccion' => $dir_id ,
					   'rfc' => $row->datc_rfc,
					   'vin' => $row->data_vin
					);
				$this->db3->initialize();
				$this->db3->insert('clientes', $cliente);
				$this->db3->close();
				$qu_cl = "select * from clientes where vin = '".$row->data_vin."'";
				$idCliente = '';
				$this->db3->initialize();
				$ex_cl = $this->db3->query($qu_cl);
				foreach($ex_cl->result() as $cl){
					$idCliente = $cl->id;
				}
				$this->db3->close();

				$version = $this->findVersion($row->data_modelo);
				$submarca = $this->modelExist($row->data_modelo);
				$vehiculo = array(
						'vin' => $row->data_vin,
						'color' => $row->data_color,
						'tipo' => 1,
						'anio' => $row->data_ano,
						'motor' => $row->data_motor,
						'transmision' => '',
						'marca' => $row->data_marca,
						'modelo' => $row->data_ano,
						'version' => $version,
						'submarca' => $submarca,
						'capacidad' => $row->data_capacidad,
						'placas' => '',
						'km_actual' => '',
						'cliente' => $idCliente
					);
				$this->db3->initialize();
				$this->db3->insert('vehiculos', $vehiculo);
				echo json_encode($vehiculo).'<br>';
				$this->db3->close();

				echo 'Agregado '.$row->data_vin.' <br>';
			}
			$this->db2->query('update datos_auto set status_optima = "TRANSFERIDO" where data_ = '.$row->data_);
		}
	}

	function DeleteDirecciones1(){
		$ex_dir = $this->db3->query('select * from direccion order by id desc');
		foreach($ex_dir->result() as $dir){
			$ex_find = $this->db3->query('select * from clientes where direccion = '.$dir->id);
			if($ex_find->num_rows() == 0){
				//$ex_delete = $this->db3->query('delete from direccion where id = '.$dir->id);
				echo 'Se elimino la direccion '.$dir->id.'<br>';
			}
		}
	}

	function DeleteDirecciones(){
		$ex_dir = $this->db3->query('select * from clientes order by id desc');
		foreach($ex_dir->result() as $dir){
			$ex_find = $this->db3->query('select * from direccion where id = '.$dir->direccion);
			if($ex_find->num_rows() == 0){
				$qu_vin = $this->db3->query('select vin from vehiculos where cliente = '.$dir->id.' limit 1');
				if($qu_vin->num_rows() > 0){
					foreach($qu_vin->result() as $vin){
						
						$ex_crm = $this->db2->query('select * from datos_auto, datos_cliente where data_ = datc_IDdatos_clientes and data_vin = "'.$vin->vin.'" order by data_ desc limit 1');
						if($ex_crm->num_rows() > 0){
							foreach($ex_crm->result() as $row){
								$calle = preg_replace('/[0-9]/','', $row->datc_calle);
								$matches = intval(preg_replace('/[^0-9]+/', '', $row->datc_calle), 10);
								$ciudad = '';
								switch ($row->datc_ciudad) {
								    case "TIJUANA":
								        $ciudad = 3;
								        break;
								    case "MEXICALI":
								        $ciudad = 2;
								        break;
								    case "ENSENADA":
								        $ciudad = 1;
								        break;
								    default:
								        $ciudad = 3;
								}

								$direccion = array(
										'colonia' => $row->datc_colonia,
										'calle' => $calle,
										'numero' => $matches,
										'codigo_postal' => $row->datc_cp,
										'ciudad' => $ciudad
									);
								$this->db3->initialize();
								$this->db3->insert('direccion', $direccion);
								$dir_id = $this->db3->insert_id();
								$this->db3->close();

								$this->db3->initialize();
								$this->db3->query('update clientes set direccion = '.$dir_id.' where id = '.$dir->id);
								$this->db3->close();
								echo 'Se actualizo el cliente '.$dir->id.' con la direccion '.$dir_id.'<br>';
								echo json_encode($direccion).'<br>';
								echo 'select * from datos_auto, datos_cliente where data_ = datc_IDdatos_clientes and data_vin = "'.$vin->vin.'"<br><br>';
							}
						}
					}
				}
				
			}
		}
	}
	
	function CustomersToOptima(){
		$ex_customers = $this->db->query('select * from customers where status_optima is NULL');
		foreach($ex_customers->result() as $row){
			$this->db3->initialize();
			$ex_check = $this->db3->query('select * from vehiculos where vin = "'.$row->cus_serie.'"');
			if($ex_check->num_rows() == 0){
				$ciudad = '';
				switch ($row->cus_cd) {
				    case "TIJ":
				        $ciudad = 3;
				        break;
				    case "MX":
				        $ciudad = 2;
				        break;
				    case "EN":
				        $ciudad = 1;
				        break;
				    default:
				        $ciudad = 3;
				}

				$direccion = array(
						'colonia' => $row->cus_colonia,
						'calle' => $row->cus_calle,
						'numero' => $row->cus_num,
						'codigo_postal' => $row->cus_cp,
						'ciudad' => $ciudad
					);
				
				$this->db3->insert('direccion', $direccion);
				$dir_id = $this->db3->insert_id();


				$cliente = array(
					   'nombre' => $row->cus_name ,
					   'sexo' => '' ,
					   'paterno' => '' ,
					   'materno' => '' ,
					   'edad' => '' ,
					   'telefono' => $row->cus_telephone ,
					   'email' => $row->cus_email ,
					   'direccion' => $dir_id ,
					   'rfc' => $row->cus_rfc,
					   'vin' => $row->cus_serie
					);
				$this->db3->insert('clientes', $cliente);

				$ex_idCliente = $this->db3->query('select id from clientes where vin = "'.$row->cus_serie.'"');
				$idCliente = '';
				foreach($ex_idCliente->result() as $pre_id){
					$idCliente = $pre_id->id;
				}

				$vehiculo = array(
						'vin' => $row->cus_serie,
						'color' => $row->cus_color,
						'tipo' => 1,
						'anio' => $row->cus_fecha_venta,
						'motor' => $row->cus_motor,
						'transmision' => $row->cus_transmision,
						'marca' => $row->cus_marca,
						'modelo' => $row->cus_modelo,
						'version' => $row->cus_version,
						'submarca' => $row->cus_submarca,
						'capacidad' => $row->cus_capacidad,
						'placas' => $row->cus_placas,
						'km_actual' => '',
						'cliente' => $idCliente
					);
				$this->db3->insert('vehiculos', $vehiculo);

				$this->db3->close();
				echo 'Agregado '.$row->cus_serie.' <br>';
			}

			$this->db->query('update customers set status_optima = "TRANSFERIDO" where cus_idCustomer = '.$row->cus_idCustomer);

		}
	}

	function VehiclesToOptima(){
		$ex_vehicles = $this->db->query('select * from customers_v where cus_idCustomer >= 5529');
		foreach($ex_vehicles->result() as $row){
			$ex_get_client = $this->db3->query('select * from clientes where nombre = "'.$row->cus_name.'" limit 1');
			$idCliente = '';
			foreach($ex_get_client->result() as $ob){
				$idCliente = $ob->id;
			}
			$vehiculo = array(
					'vin' => $row->cus_serie,
					'color' => $row->cus_color,
					'tipo' => 1,
					'anio' => $row->cus_fecha_venta,
					'motor' => $row->cus_motor,
					'transmision' => $row->cus_transmision,
					'marca' => $row->cus_marca,
					'modelo' => $row->cus_modelo,
					'version' => $row->cus_version,
					'submarca' => $row->cus_submarca,
					'capacidad' => $row->cus_capacidad,
					'placas' => $row->cus_placas,
					'km_actual' => '',
					'cliente' => $idCliente
				);
			$this->db3->insert('vehiculos', $vehiculo);
		}
	}

    
    function Transfercustomers(){
		$qu_customers = 'select data_vin from ventas_facturado where data_vin != "" and length(data_vin) = 17 group by data_vin';
		$ex_customers= $this->db2->query($qu_customers);
		echo $ex_customers->num_rows()."<br>";
		foreach($ex_customers->result() as $row){
			$qu_find = 'select * from vehiculos where vin = "'.$row->data_vin.'" limit 1';
			$ex_find = $this->db3->query($qu_find);
			if($ex_find->num_rows() > 0){
				foreach($ex_find->result() as $find){
					$upd_string = '';
					$upd = false;
					if(ValidateField($find->color)){
						$upd_string .= ' color = "'.$row->data_color.'"';
					}

					if(ValidateField($find->anio)){
						$upd_string .= ' anio = "'.$row->data_ano.'"';
					}

					if(ValidateField($find->motor)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->transmision)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->marca)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->modelo)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->version)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->submarca)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->capacidad)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->placas)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

					if(ValidateField($find->cliente)){
						$upd_string .= ' color = "'.$row->color.'"';
					}

				}
			}else{
				
			}
		}
	}

	function TransferManual($vin){
		$qu_ap = "select * from autos_en_piso where api_vin = '".$vin."'";
		$ex_ap = $this->db2->query($qu_ap);
		$num = 0;
		foreach($ex_ap->result() as $find){
			$qu_f = "select * from vehiculos where vin = '".$find->api_vin."'";
			$ex_f = $this->db3->query($qu_f);
			if($ex_f->num_rows > 0){

			}else{
				$this->db3->initialize();
				$qu_new = 'select * from datos_auto, datos_cliente where data_ = datc_IDdatos_clientes and data_vin = "'.$find->api_vin.'" limit 1';
				$ex_new = $this->db2->query($qu_new);
				$this->db3->close();
				if($ex_new->num_rows > 0){
					foreach($ex_new->result() as $row){
						$this->db3->initialize();
						$qu_clienteexist = 'select * from clientes where nombre like "%'.$row->datc_nombrecompleto.'%" and email like "%'.$row->datc_email.'%" and rfc = "'.$row->datc_rfc.'" limit 1';
						$ex_clienteexist = $this->db3->query($qu_clienteexist);
						$this->db3->close();
						if($ex_clienteexist->num_rows > 0 && $row->datc_rfc != '' && $row->datc_rfc != 'XAXXX010101000'){
							$idCliente = '';
							foreach($ex_clienteexist->result() as $ex){
								$idCliente = $ex->id;
							}
							$version = $this->findVersion($find->api_modelo);
							$submarca = $this->modelExist($find->api_modelo);

							$vehiculo = array(
									'vin' => $find->api_vin,
									'color' => $find->api_color,
									'tipo' => 1,
									'anio' => $find->api_anio,
									'motor' => $row->data_motor,
									'transmision' => '',
									'marca' => $row->data_marca,
									'modelo' => $find->api_anio,
									'version' => $version,
									'submarca' => $submarca,
									'capacidad' => $row->data_capacidad,
									'placas' => '',
									'km_actual' => '',
									'cliente' => $idCliente
								);
							$this->db3->initialize();
							$this->db3->insert('vehiculos', $vehiculo);
							echo json_encode($vehiculo).'<br>';
							$this->db3->close();
							echo 'si existia el cliente: '.$qu_clienteexist.'<br>';
						}else{
							$calle = preg_replace('/[0-9]/','', $row->datc_calle);
							$matches = intval(preg_replace('/[^0-9]+/', '', $row->datc_calle), 10);
							$ciudad = '';
							switch ($row->datc_ciudad) {
							    case "TIJUANA":
							        $ciudad = 3;
							        break;
							    case "MEXICALI":
							        $ciudad = 2;
							        break;
							    case "ENSENADA":
							        $ciudad = 1;
							        break;
							    default:
							        $ciudad = 3;
							}

							$direccion = array(
									'colonia' => $row->datc_colonia,
									'calle' => $calle,
									'numero' => $matches,
									'codigo_postal' => $row->datc_cp,
									'ciudad' => $ciudad
								);
							$this->db3->initialize();
							$this->db3->insert('direccion', $direccion);
							$dir_id = $this->db3->insert_id();
							$this->db3->close();

							$cliente = array(
								   'nombre' => $row->datc_primernombre ,
								   'sexo' => '' ,
								   'paterno' => $row->datc_apellidopaterno ,
								   'materno' => $row->datc_apellidomaterno ,
								   'edad' => '' ,
								   'telefono' => $row->datc_tcasa ,
								   'email' => $row->datc_email ,
								   'direccion' => $dir_id ,
								   'rfc' => $row->datc_rfc,
								   'vin' => $find->api_vin
								);
							$this->db3->initialize();
							$this->db3->insert('clientes', $cliente);
							
							$qu_cl = "select * from clientes where vin = '".$find->api_vin."'";
							$idCliente = '';
							$ex_cl = $this->db3->query($qu_cl);
							foreach($ex_cl->result() as $cl){
								$idCliente = $cl->id;
							}

							$version = $this->findVersion($find->api_modelo);
							$submarca = $this->modelExist($find->api_modelo);

							$vehiculo = array(
									'vin' => $find->api_vin,
									'color' => $find->api_color,
									'tipo' => 1,
									'anio' => $find->api_anio,
									'motor' => $row->data_motor,
									'transmision' => '',
									'marca' => $row->data_marca,
									'modelo' => $find->api_anio,
									'version' => $version,
									'submarca' => $submarca,
									'capacidad' => $row->data_capacidad,
									'placas' => '',
									'km_actual' => '',
									'cliente' => $idCliente
								);
							$this->db3->insert('vehiculos', $vehiculo);
							echo json_encode($vehiculo).'<br>';
							$this->db3->close();
							echo 'no existia el cliente <br>';
						}
						

						
					}
				}
				$num++;
			}
		}
		echo $num;
	}

	function NewCustomers(){
		$qu_ap = "select * from vehiculos where cliente = 0";
		$ex_ap = $this->db3->query($qu_ap);
		$num = 0;
		foreach($ex_ap->result() as $find){
			
			$this->db2->initialize();
			$qu_new = 'select * from datos_auto, datos_cliente where data_ = datc_IDdatos_clientes and data_vin = "'.$find->vin.'" limit 1';
			$ex_new = $this->db2->query($qu_new);
			$this->db2->close();
			if($ex_new->num_rows > 0){
				foreach($ex_new->result() as $row){
					$this->db3->initialize();
					$qu_clienteexist = 'select * from clientes where rfc = "'.$row->datc_rfc.'" limit 1';
					$ex_clienteexist = $this->db3->query($qu_clienteexist);
					$this->db3->close();
					if($ex_clienteexist->num_rows > 0 && $row->datc_rfc != '' && $row->datc_rfc != 'XAXXX010101000'){
						$idCliente = '';
						foreach($ex_clienteexist->result() as $ex){
							$idCliente = $ex->id;
						}
						$this->db3->initialize();
						$this->db3->query('update vehiculos set cliente = '.$idCliente.' where vin = "'.$find->vin.'"');
						$this->db3->close();
						echo 'si existia el cliente: '.$qu_clienteexist.'<br>';
					}else{
						$calle = preg_replace('/[0-9]/','', $row->datc_calle);
						$matches = intval(preg_replace('/[^0-9]+/', '', $row->datc_calle), 10);
						$ciudad = '';
						switch ($row->datc_ciudad) {
						    case "TIJUANA":
						        $ciudad = 3;
						        break;
						    case "MEXICALI":
						        $ciudad = 2;
						        break;
						    case "ENSENADA":
						        $ciudad = 1;
						        break;
						    default:
						        $ciudad = 3;
						}

						$direccion = array(
								'colonia' => $row->datc_colonia,
								'calle' => $calle,
								'numero' => $matches,
								'codigo_postal' => $row->datc_cp,
								'ciudad' => $ciudad
							);
						$this->db3->initialize();
						$this->db3->insert('direccion', $direccion);
						$dir_id = $this->db3->insert_id();
						$this->db3->close();

						$cliente = array(
							   'nombre' => $row->datc_primernombre ,
							   'sexo' => '' ,
							   'paterno' => $row->datc_apellidopaterno ,
							   'materno' => $row->datc_apellidomaterno ,
							   'edad' => '' ,
							   'telefono' => $row->datc_tcelular ,
							   'email' => $row->datc_email ,
							   'direccion' => $dir_id ,
							   'rfc' => $row->datc_rfc,
							   'vin' => $row->data_vin
							);
						$this->db3->initialize();
						$this->db3->insert('clientes', $cliente);
						
						$qu_cl = "select * from clientes where vin = '".$row->data_vin."'";
						echo json_encode($cliente).'<br>';
						$idCliente = '';
						$ex_cl = $this->db3->query($qu_cl);
						foreach($ex_cl->result() as $cl){
							$idCliente = $cl->id;
						}
						$this->db3->close();

						$this->db3->initialize();
						$this->db3->query('update vehiculos set cliente = '.$idCliente.' where vin = "'.$find->vin.'"');
						$this->db3->close();

						echo 'no existia el cliente update vehiculos set cliente = '.$idCliente.' where vin = "'.$find->vin.'"<br>';
					}
					

					
				}
			}else{
				echo 'no existia en crm <br>';
			}

			
		}
		echo $num;
	}


	function Test(){
		$qu_ap = "select * from vehiculos";
		$ex_ap = $this->db3->query($qu_ap);
		$num = 1;
		foreach($ex_ap->result() as $find){
			$qu_f = "select * from autos_en_piso where api_vin = '".$find->vin."'";
			$ex_f = $this->db2->query($qu_f);
			if($ex_f->num_rows > 0 ){
				foreach($ex_f->result() as $row){
					$upd_string = 'update vehiculos set';
					$upd = false;
					$first = true;
					if($this->ValidateField($find->color)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$upd_string .= ' color = "'.$row->api_color.'"';
						$upd = true;
					}

					if($this->ValidateField($find->anio)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$upd_string .= ' anio = "'.$row->api_anio.'"';
						$upd = true;
					}

					if($this->ValidateField($find->motor)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$upd_string .= ' motor = "'.$row->api_motor.'"';
						$upd = true;
					}

					if($this->ValidateField($find->transmision)){
						//$upd_string .= ' transmision = "'.$row->color.'"';
						//$upd = true;
					}

					if($this->ValidateField($find->marca)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$upd_string .= ' marca = "Honda"';
						$upd = true;
					}

					if($this->ValidateField($find->modelo)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$upd_string .= ' modelo = "'.$row->api_anio.'"';
						$upd = true;
					}

					if($this->ValidateField($find->version)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$version = $this->findVersion($row->api_modelo);
						$upd_string .= ' version = "'.$version.'"';
						$upd = true;
					}

					if($this->ValidateField($find->submarca)){
						if(!$first){
							$upd_string .= ',';
						}else{
							$first = false;
						}
						$submarca = $this->modelExist($row->api_modelo);
						$upd_string .= ' submarca = "'.$submarca.'"';
						$upd = true;
					}

					if($this->ValidateField($find->capacidad)){
						//$upd_string .= ' capacidad = "'.$row->color.'"';
						//$upd = true;
					}

					if($this->ValidateField($find->placas)){
						//$upd_string .= ' placas = "'.$row->color.'"';
						//$upd = true;
					}

					if($this->ValidateField($find->cliente)){
						//$upd_string .= ' cliente = "'.$row->color.'"';
						//$upd = true;
					}

					if($upd){
						$upd_string .= ' where vin = "'.$find->vin.'"';
						echo $upd_string.'<br>';
						$this->db3->query($upd_string);
						$num++;
					}
				}
			}else{
				
			}
		}

		echo $num;
	}

	function modelExist($m) {
				$m = strtoupper($m);
				$word_found = "";
				$exist = 0;
				$words = array("ACCORD", "CIVIC", "CITY", "HRV", "FIT", "CRV",
				"ODYSSEY", "RIDGELINE", "PILOT");
				$count_words = count($words);
				for($i = 0; $i < $count_words; $i++){
					if (strpos($m, $words[$i]) !== false) {
						$word_found = $words[$i];
						$exist = 1;
						$i = $count_words+1;
					}
				}
			
			return $word_found;
		}
		
		function findVersion($text_in){

			$text_in = trim($text_in, " ");
			$word_found = "";
			$words = array("EX", "LX", "UNIQ", "EXL NAVI", "i-Style", "EXL ", 
			"COOL", "TOURING", "HIT", "TURBO", "EPIC", "FUN");
			$count_words = count($words);
			for($i = 0; $i < $count_words; $i++){
			if (strpos($text_in, $words[$i]) !== false) {
			$word_found = $words[$i];
			$i = $count_words+1;
			}
			}     
			return trim($word_found, " ");
		}


	function ValidateField($field){
		if(!is_null($field) && !empty($field) && strlen($field) > 1){
			return true;
		}else{
			return false;
		}
	}

}

?>