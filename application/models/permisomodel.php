<?php

class Permisomodel extends CI_Model{

	public function __construct()
	{
		parent::__construct();
	}

	function acceso()
	{
		session_start();
		if(!isset($_SESSION['sfid'])) return redirect('acceso');
	}

	function permisosVer($mod)
	{
		$result= $this->db->query('SELECT *
				FROM userTypes,permisos
				WHERE
				userTypes_ust_idUserType=ust_idUserType
				AND ust_idUserType='.$_SESSION['sftype'].'
				AND modulo="'.$mod.'"
		');
		$val = 0;
		if($result->num_rows() > 0)
		{
	        foreach($result->result_array() as $row)
				$val = $row['ver'];
		}

		if($val == '1') $res = 'sus_workshop="'.$_SESSION['sfworkshop'].'"';
		else $res = 'sus_workshop IN ("Tijuana","Mexicali","Ensenada")';
		return $res;
	}


	function mesesNombre()
	{
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		return $meses;
	}

	function mesAnt($m,$a)
	{
		if($m == 0) $m = 0; else $m = $m + 1;
		if($m == 0)
		{
			$m = 12;
			$a = $a - 1;
		}
		else
		{
			$m = $m - 1;
			$a = $a;
		}
		return $a.'-'.$m.'-'.'01';
	}

	function mesSig($m,$a)
	{
		if($m == 0) $m = 1; else $m = $m + 1;
		if($m == 12)
		{
			$m = 1;
			$a = $a +1;
		}
		else
		{
			$m = $m + 1;
			$a = $a;
		}
		return $a.'-'.$m.'-'.'01';
	}

	function misOrdenes()
	{
		if($_SESSION['sftype'] == 1 || $_SESSION['sftype'] == 2) $cd = 'and sus_workshop="'.$_SESSION['sfidws'].'"';
		else $cd = 'and sus_idUser="'.$_SESSION['sfid'].'"';

		$query= $this->db->query('SELECT seo_idServiceOrder,app_customerName,app_customerLastName,enp_status,seo_cliente
									FROM enproceso,serviceOrders,appointments,susers
									WHERE
									enp_ID_proceso=seo_idServiceOrder
									and enp_ID_cita=app_idAppointment
									and seo_IDapp=app_idAppointment
									and seo_date="'.date('Y-m-d').' 00:00:00"
									and seo_adviser=sus_idUser
									'.$cd.'
		');
		return $query->result();
	}
}

?>
