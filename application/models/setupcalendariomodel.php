<?php

class Setupcalendariomodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->library('email');
	}
	
	function oper($taller){
//return date('N',strtotime($date));

$query = $this->db->query("
				SELECT *
				FROM workshop_capabilities
				WHERE 
				woc_workshop=".$taller."
				
				");

		return $query->result();
		
		}
	
	function porcentaje($date,$oper){
	$d=date('N',strtotime($date));
	if($d==6){$di=$oper[0]->woc_weekends_service_hours;}	
		else{$di=$oper[0]->woc_weekdays_service_hours;}	
		
		return ($di * $oper[0]->woc_tecnicos * $oper[0]->woc_productivity_factor);
		
		
		}
	function listarCitas($date){
		
		$query = $this->db->query("
				SELECT *
				FROM appointments, susers, reminderTypes, vehicleModels, serviceTypes
				WHERE 
				sus_idUser = app_desiredAdviser
				AND
				rem_idReminderType = app_reminderType
				AND
				vem_idVehicleModel = app_vehicleModel
				AND
				set_idServiceType = app_service
				AND
				app_onlineConfirmed = 1
				AND 
				DATE_FORMAT(app_day,'%Y-%m-%d') = '".$date."'");

		return $query->result();
	}
	
	function listaPorcentajeCitas($id){
		
		$query = $this->db->query("
				SELECT *
				FROM calendario_porcentaje
				WHERE 
				cpo_ver=1
				AND
				cpo_wor_idWorkshop=".$id."");

		return $query->result();
	}
	function citasPorHora($date,$taller){
		$query = $this->db->query("
		
SELECT  *
FROM calendario
LEFT JOIN v_listadecitas
ON  `cal_app_idAppointment` =  `app_idAppointment` 
WHERE  `cal_fecha` =  '".$date."'
AND cal_wor_idWorkshop=".$taller."
				
				");

		return $query->result();
		}
		
		function citasPorHoraTot($date,$taller){
		$query = $this->db->query("
		
SELECT  SUM(ser_approximate_duration) as tot
FROM calendario
LEFT JOIN v_listadecitas
ON  `cal_app_idAppointment` =  `app_idAppointment` 
WHERE  `cal_fecha` =  '".$date."'
AND cal_wor_idWorkshop=".$taller."
				
				");

		return $query->result();
		}
	
	function listarCitasPorConfirmar($date){
		
		$query = $this->db->query("
				SELECT *
				FROM appointments, susers, reminderTypes, vehicleModels, serviceTypes
				WHERE 
				sus_idUser = app_desiredAdviser
				AND
				rem_idReminderType = app_reminderType
				AND
				vem_idVehicleModel = app_vehicleModel
				AND
				set_idServiceType = app_service
				AND
				app_onlineConfirmed = 0
				AND 
				DATE_FORMAT(app_day,'%Y-%m-%d') = '".$date."'");
		return $query->result();
	}
	
	function listarPorConfirmarAjax(){
		$query = $this->db->query("
			  SELECT DATE_FORMAT(app.app_day,'%Y-%m-%d') as date, app.app_customerName as client_first_name, app.app_customerLastName as client_last_name
			  FROM appointments as app
			  WHERE app_onlineConfirmed = 0
		  ");
		return $query->result();
	}
	
	function agregarCita($cita){
         $this->db->insert('appointments', $cita); 
		 $id = $this->db->insert_id();
		return $id;
	}
	
	function actualizarCita($cita, $id){
         $this->db->update('appointments', $cita, array('app_idAppointment' => $id)); 
	}
	function updateCalendario($cita, $id){
         $this->db->update('calendario', $cita, array('cal_ID' => $id)); 
	}
	
	function obtenerCita($id){
		$query= $this->db->query(
				'SELECT app_idAppointment, app_day, app_message, app_desiredAdviser, app_service,
				app_reminderType, app_customerName, app_customerLastName, app_customerTelephone,
				app_customerEmail, app_vehicleModel, app_vehicleYear, app_needValet, app_hour
				FROM appointments,susers,reminderTypes,vehicleModels,serviceTypes
				WHERE 
				rem_idReminderType = app_reminderType
				AND
				sus_idUser = app_desiredAdviser
				AND
				set_idServiceType = app_service
				AND
				vem_idVehicleModel = app_vehicleModel
				AND
				app_idAppointment = '.$id.'');
		 return $query->result();
	}
	
	function confirmarCita($id){
		$query = $this->db->query('UPDATE appointments
			SET app_confirmed = 1
			WHERE 
			app_idAppointment = '.$id.'');
	}
	
	function confirmarCitaOnline($id){
		$query = $this->db->query('UPDATE appointments
			SET app_onlineConfirmed = 1
			WHERE 
			app_idAppointment = '.$id.'');
	}
	
	
	function cancelarCita($id){
		$query = $this->db->query('UPDATE appointments
			SET app_confirmed = 2
			WHERE 
			app_idAppointment = '.$id.'');
	}
	
	/* Funciones para opciones en Selects */
	
	function obtenerAsesores(){
		$query= $this->db->query(
				'SELECT sus_idUser, sus_name, sus_lastName
				FROM susers
				WHERE
				sus_rol = 3
				AND sus_workshop='.$_SESSION['sfidws'].'
				AND sus_isDeleted=0
				');
		$opciones = $query->result();
		$asesores = array();
		foreach($opciones as $opcion){
			$asesores[$opcion->sus_idUser] = $opcion->sus_name.' '.$opcion->sus_lastName;
		}
		return $asesores;
	}

	function obtenerServicios(){
		$query= $this->db->query(
				'SELECT s.ser_idService, st.set_name
				FROM services AS s, serviceTypes AS st
				WHERE ser_serviceType = set_idServiceType
				GROUP BY st.set_name');
		$opciones = $query->result();
		$servicios = array();
		foreach($opciones as $opcion){
			$servicios[$opcion->ser_idService] = $opcion->set_name;
		}
		return $servicios;
	}

	function obtenerVehiculos(){
		$query= $this->db->query(
				'SELECT veh_idVehicle, veh_model, veh_year
				FROM vehicles');
		$opciones = $query->result();
		$vehiculos = array();
		foreach($opciones as $opcion){
			$vehiculos[$opcion->veh_idVehicle] = $opcion->veh_model.'-'.$opcion->veh_year;
		}
		return $vehiculos;
	}
	
	function obtenerRecordatorios(){
		$query= $this->db->query(
				'SELECT rem_idReminderType, rem_name
				FROM reminderTypes');
		$opciones = $query->result();
		$recordatorios = array();
		foreach($opciones as $opcion){
			$recordatorios[$opcion->rem_idReminderType] = $opcion->rem_name;
		}
		return $recordatorios;
	}
	
	
	function obtenerModelos(){
		$query= $this->db->query(
				'SELECT vem_idVehicleModel, vem_name
				FROM vehicleModels');
		$opciones = $query->result();
		$modelos = array();
		foreach($opciones as $opcion){
			$modelos[$opcion->vem_idVehicleModel] = $opcion->vem_name;
		}
		return $modelos;
	}
	
	
	
	
		function sendEmailCitaOnline($toemail,$fecha,$hora,$servicio,$modelo,$anio,$nombre,$apellido){
			$this->email->set_mailtype("html");
			$this->email->from('info@webmarketingnetworks.com', 'Honda Optima - Servicios');
			$this->email->to($toemail);
			$this->email->subject('Registro de Cita | Honda Optima');
			$this->email->message('
									<table>
									  <tr>
										<td></td>
									  </tr>
									</table>
									<br>
									<table>
									  <tr>
										<td>Datos del Registro de la cita para: '.$nombre.' '.$apellido.' </td>
										<td></td>
									  </tr>
									</table>
									<br>
									<table>
									  <tr>
										<td></td>
									  </tr>
									</table>
								
									<br>
									<br>
									<table>
									  
									  <tr>
										<td><b>Fecha:</b></td>
										<td> '.$fecha.'</td>
									  </tr>
									  <tr>
										<td><b>Hora:</b></td>
										<td> '.$hora.'</td>
									  </tr>
									   <tr>
										<td><b>Servicio:</b></td>
										<td> '.$servicio.'</td>
									  </tr>
									   <tr>
										<td><b>Vehículo:</b></td>
										<td> '.$modelo.' '.$anio.'</td>
									  </tr>
									</table>
									<br>
									<br>
									<hr/>
									<table>
									  <tr>
										<td></td>
									  </tr>
									</table>
									');
												
			$this->email->send();
	}
	
	
	
	
	
	
	
	
	
	
	
}

?>