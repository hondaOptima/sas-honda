<?php

class Usuariomodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		
		$this->load->library('email');
	}
	
	
	function listaUsuariosActivos($per){
  $query= $this->db->query(
    '
    SELECT *
    FROM susers,userTypes,permisos, roles, workshops
    WHERE 
    ust_idUserType = sus_userType
    AND
    rol_idRol = sus_rol
    AND
    wor_idWorkshop = sus_workshop
    AND
    sus_isDeleted = 0
    AND
    sus_isActive = 1
    AND
    sus_idUser!=67
    
    ORDER BY  `susers`.`sus_orden` ASC 
    ');
   return $query->result();
 }
	
	
	function listaUsuarios($per){
		$query= $this->db->query(
				'
				SELECT *
				FROM susers,userTypes,permisos, roles, workshops
				WHERE 
				ust_idUserType = sus_userType
				AND
				rol_idRol = sus_rol
				AND
				wor_idWorkshop = sus_workshop
				AND
				sus_isDeleted = 0
				AND
				sus_idUser!=67
				AND
				sus_isActive = 1
				ORDER BY  `susers`.`sus_orden` ASC				
				');
		 return $query->result();
	}
	
	function agregarUsuario($usuario){
         $this->db->insert('susers', $usuario); 
	}
	
	
	
	function actualizarUsuario($usuario, $id){
         $this->db->update('susers', $usuario, array('sus_idUser' => $id)); 
	}
	
	function borrarUsuario($id){		
		$query = $this->db->query(
				'UPDATE susers
				SET sus_isDeleted = 1
				WHERE 
				sus_idUser='.$id.'');
	}
	
	function getEquipoId($idAse,$idTec){		
		$query = $this->db->query(
				'
				select * from equipos
				where equ_asesor='.$idAse.'
				and equ_tecnico='.$idTec.'
				');
				 return $query->result();
	}
	
	function obtenerlEquipos($id){		
		$query = $this->db->query(
				'
				select * from equipos
				where equ_asesor='.$id.'
				');
				 return $query->result();
	}
	
	function obtenerEquipos(){
		$query = $this->db->query(
				'
				select * from equipos,susers,workshops
				where 
				equ_tecnico=sus_idUser
				and sus_workshop=wor_idWorkshop
				and wor_city="'.$_SESSION['sfworkshop'].'"
				
				
				');
				 return $query->result();
		
		}
	
	
	function AgregarEquipo($idAse,$idTec){		
		$query = $this->db->query(
'INSERT INTO `equipos` (`equ_ID`, `equ_asesor`, `equ_tecnico`, `equ_fecha`)
 VALUES (NULL, '.$idAse.', '.$idTec.', "'.date('Y-m-d').'");');
	}
	
	function eliminarEquipo($idAse,$idTec){		
		$query = $this->db->query(
				'
				delete from equipos
				where equ_asesor='.$idAse.'
				and equ_tecnico='.$idTec.'
				');
	}
	
	
	function desactivarUsuario($id){

		$query = $this->db->query('UPDATE susers
			SET sus_isActive = 0
			WHERE 
			sus_idUser = '.$id.'');
	}
	
	function activarUsuario($id){

		$query = $this->db->query('UPDATE susers
			SET sus_isActive = 1
			WHERE 
			sus_idUser = '.$id.'');
	}
	
	function obtenerUsuario($id){
		
		
		$query= $this->db->query(
				'
				SELECT *, 
				CAST(sus_status AS unsigned integer) As status
				FROM susers,userTypes,permisos, roles, workshops
				WHERE 
				ust_idUserType = sus_userType
				AND
				rol_idRol = sus_rol
				AND
				wor_idWorkshop = sus_workshop
				AND
				sus_idUser = '.$id.'
				
				');
		 return $query->result();
	}
	
	function obtenerAsesores($taller){
		$query= $this->db->query(
				'
				SELECT *, 
				CAST(sus_status AS unsigned integer) As status
				FROM susers,userTypes,permisos, roles, workshops
				WHERE 
				ust_idUserType = sus_userType
				AND
				rol_idRol = sus_rol
				AND
				wor_idWorkshop = sus_workshop
				AND
				sus_workshop = '.$taller.'
				AND sus_isDeleted=0
				AND sus_idUser!=67
				');
		 return $query->result();
	}
	
	function emailBienvenida($toemail,$nombre,$apellido,$correo,$con){
			$this->email->set_mailtype("html");
			$this->email->from('casa@hondaoptima.com.com', 'Honda Optima - Servicios');
			$this->email->to($toemail);
			$this->email->subject('Bienvenido a SAS Honda Optima');
			$this->email->message('
									<table>
									  <tr>
										<td></td>
									  </tr>
									</table>
									<br>
									<table>
									  <tr>
										<td>Bienvenido:</td>
										<td>'.$nombre.' '.$apellido.'</td>
									  </tr>
									</table>
									<br>
									<table>
									  <tr>
										<td>Le damos la mas cordial bienvenido al equipo de venta Servicios Honda Optima.</td>
									  </tr>
									</table>
									<table>
									  <tr>
										<td>Usted podr&aacute; ingresar a este sistema en l&iacute;nea, siguiendo la direcci&oacute;n y los datos que <br>
										  se muestran a continuaci&oacute;n:</td>
									  </tr>
									</table>
									<br>
									<br>
									<table>
									  <tr>
										<td><b>Direcci&oacute;n:</b></td>
										<td></td>
									  </tr>
									  <tr>
										<td><a href="hondaoptima.com/servicios/">www.hondaoptima.com/sas/</a></td>
									  </tr>
									  <tr>
										<td><b>Correo electr&oacute;nico:</b></td>
										<td> '.$correo.'</td>
									  </tr>
									  <tr>
										<td><b>Contrase&ntilde;a:</b></td>
										<td> '.$con.'</td>
									  </tr>
									</table>
									<br>
									<br>
									<hr/>
									<table>
									  <tr>
										<td></td>
									  </tr>
									</table>
									');
												
			$this->email->send();
	}
	
	/*Funciones para opciones en Selects */
		
	function obtenerTipos(){
		
		if($_SESSION['sfrol']==1){$and='';}
		else{$and='and ust_idUserType!=1';}
		
		$query= $this->db->query(
				'SELECT ust_idUserType, ust_name
				FROM userTypes');
		$opciones = $query->result();
		$tiposUsuario = array();
		foreach($opciones as $opcion){
			$tiposUsuario[$opcion->ust_idUserType] = $opcion->ust_name;
		}
		return $tiposUsuario;
	}
	
	function obtenerRoles(){
		if($_SESSION['sfrol']==1){$and='';}
		else{$and='where rol_idRol!=1';}
		$query= $this->db->query(
				'SELECT rol_idRol, rol_name
				FROM roles '.$and.'');
		$opciones = $query->result();
		$roles = array();
		foreach($opciones as $opcion){
			$roles[$opcion->rol_idRol] = $opcion->rol_name;
		}
		return $roles;
	}
	
	function obtenerTalleres(){
		$query= $this->db->query(
				'SELECT wor_idWorkshop, wor_city
				FROM workshops');
		$opciones = $query->result();
		$talleres = array();
		foreach($opciones as $opcion){
			$talleres[$opcion->wor_idWorkshop] = $opcion->wor_city;
		}
		return $talleres;

	}
	
}

?>