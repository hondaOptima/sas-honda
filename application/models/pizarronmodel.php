<?php

class Pizarronmodel extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('crmtraslados',true);
	}

	function status()
	{
		$aa=array('1'=>'En Espera','2'=>'En Rampa','3'=>'Pend. X Auto','4'=>'En Prueba','5'=>'Terminado','6'=>'No Surtido','7'=>'Surtido','8'=>'Previado','9'=>'Almacen 1','10'=>'Almacen 2','11'=>'Almacen 3');
		$bb=array('1'=>'#C00','2'=>'#fcb322','3'=>'#FF0','4'=>'#3cc051','5'=>'green','6'=>'#C00','7'=>'green','8'=>'green','9'=>'green','10'=>'green','11'=>'green');
		return array($aa,$bb);
	}

    function updatePizarronInventario($datos,$id)
	{
		$this->db->update('pizarron_inventario', $datos, array('pii_vin' => $id));
	}

    function updateClaveRadio($id, $value){
		$qu = 'select sev_vin from serviceOrder_vehicleInformation where sev_idVehicleInformation = '.$id;
		$query= $this->db->query($qu);
		$res = $query->result();
		$vin = $res[0]->sev_vin;
		if($vin != null && $vin != ''){
			$data = array(
				'clave_radio' => $value,
			);
			$this->db2->query('update autos_en_piso set clave_radio = '.$value.' where api_vin = "'.$vin.'"');
		}
	}


	function getLista($cd,$date){
		list($d,$m,$a)=explode('-',$date);

		$date=''.$a.'-'.$m.'-'.$d.'';
		$qu='SELECT
                exists
(
	select * from campanias, serviceOrder_vehicleInformation
    where cam_vin = sev_vin
    and sev_idVehicleInformation = seo_idServiceOrder
) as camp,
exists
(
	select * from garantia_exist ge, serviceOrder_vehicleInformation
    where ge.vin = sev_vin
    and sev_idVehicleInformation = seo_idServiceOrder
	and ge.status = 1
) as garexist,
exists
			(
				select * from garantias ge
				where ge.vin = sev_vin
				and sev_idVehicleInformation = seo_idServiceOrder
			) as extension,
piz_ID,
piz_status_lavado,
piz_status_mecanico,
piz_entrega_lavado,
piz_status_surtido,
seo_technicianTeam,
seo_tower,
seo_idServiceOrder,
seo_promisedTime,
seo_technicianTeam,
sev_vin as seo_vehicle
				FROM  pizarron,serviceOrders,susers,workshops,appointments,services,serviceTypes
				,serviceOrder_vehicleInformation
				WHERE

				piz_ID_orden=seo_idServiceOrder
				and seo_idServiceOrder = sev_idVehicleInformation
				and seo_technicianTeam=sus_idUser
				and sus_workshop=wor_idWorkshop
				and wor_idWorkshop="'.$cd.'"
				and seo_IDapp=app_idAppointment
				and	app_service=ser_serviceType
				and app_vehicleModel=ser_vehicleModel
				and ser_serviceType=set_idServiceType
				and piz_fecha="'.$date.'"
				order by seo_idServiceOrder
				';
		 $query= $this->db->query($qu);
		return $query->result();
	}

	function getLista_inventario($cd,$date){
		list($d,$m,$a)=explode('-',$date);

		$date=''.$a.'-'.$m.'-'.$d.'';
		$query= $this->db->query(
				'
				SELECT
`pii_ID`,
`pii_fecha`,
`pii_vin`,
`pii_asesor`,
`pii_tecnico`,
`pii_servicio`,
`pii_torre`,
`pii_estado`,
`pii_ubicacion`
FROM
`pizarron_inventario`,
 susers
 WHERE
`pii_asesor`=sus_idUser
and sus_workshop='.$cd.'
and pii_estado IN ("Sin Previa","citado")
Limit 10

				');
        $this->db->close();
		 return $query->result();
	}

	function updateDia($ido,$date){

		$query= $this->db->query(
				'
				update pizarron set piz_fecha="'.$date.'" where piz_ID_orden='.$ido.'

				');

	}


		function getOrdenServiciosTodas($ciudad)
		{
			$hoy = date('Y-m-d');
			$cincoDiasAtras = date('Y-m-d', strtotime($hoy . " - 5 day"));

			$qu = '
				SELECT *
				FROM  serviceOrders,serviceOrders_service,services,serviceTypes,susers,appointments
				WHERE  seo_date BETWEEN "'.$cincoDiasAtras.'"  AND "'.$hoy.'"
				and ssr_service=ser_serviceType
				and ssr_modelo=ser_vehicleModel
				and ssr_IDNULL=seo_idServiceOrder
				and ser_serviceType=set_idServiceType
				and seo_adviser=sus_idUser
				and sus_workshop='.$ciudad.'
				and seo_IDapp=app_idAppointment
			';

			$query= $this->db->query($qu);
			$this->db->close();
			return $query->result();
		}

		function getOrdenServiciosTodassv($ciudad){

		$query= $this->db->query(
				'
SELECT *
FROM  servicios_varios,pizarron
WHERE STR_TO_DATE(  `piz_fecha` ,  "%Y-%m-%d" )
BETWEEN DATE_SUB( CURDATE( ) , INTERVAL 5
DAY )
AND CURDATE( )
and
piz_ID_orden=sev_id_orden


				');
            $this->db->close();
		 return $query->result();
		}

	function getOrdenVehi($id){
		$query= $this->db->query('
SELECT *
FROM  pizarron,appointments, serviceOrders, serviceOrder_vehicleInformation
WHERE
piz_ID='.$id.'
and piz_ID_orden=seo_idServiceOrder
and seo_IDapp=app_idAppointment
and seo_idServiceOrder=sev_idVehicleInformation
 ');
		 return $query->result();

		}

	function update($id,$campo,$valor){
		  date_default_timezone_set('America/Tijuana');
		$query= $this->db->query(
				'
				update pizarron set '.$campo.'="'.$valor.'" where piz_ID='.$id.'
				');

	}

	function emailRemider($id,$email,$statusme,$statusla,$ido,$hora,$auto){
		if($statusla==5 && $statusme==5){
			$text='El Servicio Solicitado Para su Auto ha Concluido. El Vehículo Esta Lavado y en Espera de Usted!.
			* Antes de pasar a caja, pase con su Asesor de Servicio.';
			}
		else{
		if($statusme==1){$text='El estatus de su auto es: EN ESPERA';}
		if($statusme==2){$text='Su Auto a pasado a Rampa';}
		if($statusme==3){$text='Su Auto esta en En Espera de su Autorización';}
		if($statusme==4){$text='Su Auto se encuentra en prueba de carretera';}

		}
           if(empty($email))$email='sas@hondaoptima.com';

			$this->email->set_mailtype("html");
			$this->email->from('sas@hondaoptima.com.com', 'Honda Optima - Servicios');
			$this->email->to($email);
			$this->email->subject('[Honda Optima] Notificación de Servicio');
			$this->email->message('
						<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <title>Honda Optima</title>
   </head>
   <body bgcolor="#F1F1F1">
   <style type="text/css">
	body{margin:0;padding:0;}
	.bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;}
	table{font-family:Helvetica, Arial, sans-serif;font-size:13px;color:#787878;}
	div{line-height:18px;color:#202020;}
	img{display:block;}
	td,tr{padding:0;}
	ul{margin-top:24px; margin-left:-40px; margin-bottom:24px;list-style: none;}
	li{background-image: url(iconlist.jpg);
background-repeat: no-repeat;
background-position: 0px 5px;
padding-left: 20px; line-height:24px; }
	a{color:#EE1C25;text-decoration:none;padding:2px 0px;}
	.headertbl{border-bottom:1px solid #E1E1E1;}
	.contenttbl{background-color:#FFFFFF;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;}
	.footertbl{border-top:1px solid #E1E1E1;}
	.sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;}
	.separador{background-color:#ffffff; border-bottom: 1px dotted #B6B6B6;line-height:1px;}
	.h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#EE1C25;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;}
	.h2 div{font-family:Helvetica,Arial,sans-serif;font-size:22px;color:#4E4E4E;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;}
	.h div{font-family:Helvetica,Arial,sans-serif;font-size:20px;color:#e92732;letter-spacing:-1px;margin-bottom:2px;margin-top:2px;line-height:24px;}
	.hintro h2{font-family:Helvetica,Arial,sans-serif;font-size:28px;color:#DD263C;letter-spacing:-1px;margin-bottom:6px;margin-top:20px;line-height:24px;}
	.hintro p{font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#4E4E4E;letter-spacing:-1px;margin-bottom:4px;margin-top:6px;line-height:24px;}
	.precio { font-size:14px; color:#0f0f0f;}
	.precio strong { color:#333; font-weight:800;}
	.invert div, .invert .h{color:#F4F4F4;}
	.invert div a{color:#FFFFFF;}
	.line{border-top:1px dotted #D1D1D1;}
	.logo{border-right:1px dotted #D1D1D1;}
	.small div{font-size:10px; line-height:16px;}
	.btn{margin-top:10px;display:block;}
	.btn img,.social img{display:inline;}

	div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;}
    .templateButton{ margin-top: 10px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; background-color:#dd263c;}
    .templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a { color:#f1f1f1; font-family:Arial, Helvetica; font-size:16px; font-weight:100; line-height:125%; padding:14px 6px; text-align:center; text-decoration:none; }
</style>
      <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#333333">
         <tr>
            <td align="center">

               <table width="750" cellpadding="0" cellspacing="0" class="headertbl contenttbl" bgcolor="#f1f1f1">
                  <tr>
                     <td valign="top" align="center">
                         <img src="http://www.hondaoptima.com/ventas/img/encabezadohonda.jpg">
                     </td>
                  </tr>
               </table>      <table width="750" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="contenttbl">
                          <tr>
            <td height="10">&nbsp;</td>
         </tr>
                  <tr>
                     <td valign="top" align="center">
                        <table width="570" cellpadding="0" cellspacing="0">
                           <tr>
                     <td width="" valign="top" align="left">
                     	<table width="100%"><tr>
<td width="80" >
  <h3>Vehiculo:'.$auto.'</h3>

</td><td width="20%">
<b>Fecha: </b>'.date('d-m-Y').'</td></tr></table>

                     	<br>

<div >
<h1>'.$text.' </h1>
<h1>Hora Prometida:'.$hora.'</h1>
<br><br>


</div>
<div>
<br><br>
            </td>
         </tr>
      </table>




      <table width="750" cellpadding="0" cellspacing="0" class="footertbl" bgcolor="#ffffff">
         <tr>
            <td valign="top" align="center">
              <a href="https://www.facebook.com/hondaoptima">
              <img src="http://www.hondaoptima.com/ventas/img/piehonda.jpg">
              </a>
            </td>
         </tr>
         <tr>
            <td height="24"  style="text-align: right"></td>
         </tr>
      </table>
      </td>
      </tr>
      </table>
   </body>
</html>



								');

			$this->email->send();

	}


	function SmsBienvenida($CAU){
     $txtsms="El Servicio Solicitado Para su Auto ha Concluido. El Vehiculo Esta Lavado y en Espera de Usted!";
	 $CAT='52'.$CAU;
	 $phone = array($CAT);
     $response = $this->textmagic->send($txtsms, $phone);
	}



	function asesi_inv($array,$id){
$ok='no';
foreach($array as $ar){
if($ar->pii_tecnico==$id)
$ok='ok';
}

return $ok;
	}

	function contar_piz_inv($array,$id){
$res=0;
foreach($array as $ar){
	if($ar->pii_tecnico==$id)
	$res++;
	}
	return $res;
	}

	function get_autos_lavado($workshop_id, $fecha = null)
	{
		$sql = "SELECT s.seo_idServiceOrder AS 'orden_id',u.sus_idUser AS 'asesor_id',
				STR_TO_DATE(s.seo_promisedTime, '%l:%i %p') AS 'tiempo_entrega',
				s.seo_promisedTime AS 'tiempo_entrega_am_pm',
				CONCAT_WS(' ',sv.sev_sub_marca,sv.sev_version,sv.sev_color,sv.sev_modelo) AS 'auto',
				s.seo_tower AS 'torre',sv.sev_vin AS 'vin',CONCAT_WS(' ',u.sus_name,u.sus_lastName) AS 'asesor_nombre',
				CONCAT_WS(' ',t.sus_name,t.sus_lastName) AS 'tecnico_nombre',t.sus_idUser AS 'tecnico_id',p.piz_status_lavado AS 'status_lavado'
				FROM serviceOrders s
				INNER JOIN susers u ON s.seo_adviser = u.sus_idUser
				INNER JOIN serviceOrder_vehicleInformation AS sv ON s.seo_idServiceOrder = sv.sev_idVehicleInformation
				INNER JOIN pizarron p ON s.seo_idServiceOrder = p.piz_ID_orden
				LEFT JOIN susers t ON s.seo_technicianTeam = t.sus_idUser
				WHERE DATE_FORMAT(s.seo_date,'%Y-%m-%d') = '$fecha'
				AND u.sus_workshop = $workshop_id
				ORDER BY tiempo_entrega";

		return $this->db->query($sql)->result();
	}

}

?>
