<?php

class Serviciomodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	function serviciosPro($per){
		$query= $this->db->query(
				'
				SELECT set_name,set_codigo,set_idServiceType
				FROM services,vehicleModels,serviceTypes
				WHERE 
				vem_idVehicleModel = ser_vehicleModel
				AND
				set_idServiceType = ser_serviceType
				and ser_tipo ="pro"
                                group by(`ser_serviceType`)
				ORDER BY  ser_serviceType ASC 
				');

		return $query->result();
	}
	
	function serviciosNoPro($per){
		$query= $this->db->query(
				'
				SELECT set_name,set_codigo,set_idServiceType
				FROM services,vehicleModels,serviceTypes
				WHERE 
				vem_idVehicleModel = ser_vehicleModel
				AND
				set_idServiceType = ser_serviceType
				and ser_isActive = 1
				and vem_isActive = 1
				and set_isActive = 1
				and ser_tipo ="nopro"
                                group by(`ser_serviceType`)
				ORDER BY  ser_serviceType ASC 
				');

		return $query->result();
	}
	
	function listarServicios($per){
		$query= $this->db->query(
				'
				SELECT *
				FROM services,vehicleModels,serviceTypes
				WHERE 
				vem_idVehicleModel = ser_vehicleModel
				and ser_isActive = 1
				and vem_isActive = 1
				and set_isActive = 1
				AND
				set_idServiceType = ser_serviceType
				and ser_tipo ="pro"
				ORDER BY  vem_idVehicleModel ASC 
				');

		return $query->result();
	}
	function listarServiciosx($per){
		$query= $this->db->query(
				'
				SELECT *
				FROM services,vehicleModels,serviceTypes
				WHERE 
				vem_idVehicleModel = ser_vehicleModel
				AND
				set_idServiceType = ser_serviceType
				and ser_isActive = 1
				and vem_isActive = 1
				and set_isActive = 1
				and ser_tipo ="nopro"
				ORDER BY  vem_idVehicleModel ASC 
				');

		return $query->result();
	}
	
	function listarTServicios($per){
		$query= $this->db->query(
				'
			SELECT  `ser_serviceType`,set_name,set_codigo
				FROM services,vehicleModels,serviceTypes
				WHERE 
				vem_idVehicleModel = ser_vehicleModel
				AND
				set_idServiceType = ser_serviceType
				
				group by (`ser_serviceType`)
				ORDER BY  `services`.`ser_serviceType` ASC 
				');

		return $query->result();
	}
	
	function listarModelos($per){
		$query= $this->db->query(
				'
				SELECT ser_vehicleModel, vem_name
FROM services, vehicleModels, serviceTypes
WHERE vem_idVehicleModel = ser_vehicleModel
and vem_isActive = 1
AND set_idServiceType = ser_serviceType
GROUP BY (
`ser_vehicleModel`
)
ORDER BY  vem_name ASC 
				');

		return $query->result();
	}

	function getProsNoPros($model,$tipo){
		$query= $this->db->query(
				'
				SELECT services.* from services, vehicleModels, serviceTypes
				where ser_vehicleModel = '.$model.'
				and vem_idVehicleModel = ser_vehicleModel
				and set_idServiceType = ser_serviceType
				and set_isActive = 1
				and vem_isActive = 1
								and ser_tipo = "'.$tipo.'"
				and ser_isActive = 1
				');

		return $query->result();
	}
	
	function agregarServicio($servicio){
         $this->db->insert('services', $servicio); 
	}
	
	function actualizarServicio($servicio, $id){
         $this->db->update('services', $servicio, array('ser_idService' => $id)); 
	}
	
	function obtenerServicio($id){
		$query= $this->db->query(
				'SELECT *
				FROM services,vehicleModels,serviceTypes
				WHERE 
				vem_idVehicleModel = ser_vehicleModel
				AND
				set_idServiceType = ser_serviceType
				AND
				`ser_serviceType` = '.$id.'
				ORDER BY  `services`.`ser_serviceType` ASC 
				');
		 return $query->result();
	}
	
	function desactivarServicio($id){

		$query = $this->db->query('UPDATE services
			SET ser_isActive = 0
			WHERE 
			ser_idService = '.$id.'');
	}
	
	function activarServicio($id){

		$query = $this->db->query('UPDATE services
			SET ser_isActive = 1
			WHERE 
			ser_idService = '.$id.'');
	}
	
	/* Funciones para opciones en Selects */
	
	function obtenerTipos(){
		$query= $this->db->query(
				'SELECT set_idServiceType, set_name
				FROM serviceTypes
				WHERE
				set_isActive = 1
				and set_idServiceType NOT IN(5)');
		$opciones = $query->result();
		$tiposServicio = array();
		foreach($opciones as $opcion){
			$tiposServicio[$opcion->set_idServiceType] = $opcion->set_name;
		}
		return $tiposServicio;
	}
	
	function obtenerModelos(){
		$query= $this->db->query(
				'SELECT vem_idVehicleModel, vem_name
				FROM vehicleModels
				WHERE
				vem_isActive = 1');
		$opciones = $query->result();
		$modelos = array();
		foreach($opciones as $opcion){
			$modelos[$opcion->vem_idVehicleModel] = $opcion->vem_name;
		}
		return $opciones;
	}
	
	
}

?>