<?php

class Promocionmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	
	function listarPromociones($per){
		$query= $this->db->query(
				'
				SELECT *
				FROM promotions, images
				WHERE
				ima_idImage = Images_ima_idImage
				');

		return $query->result();
	}
	
	function agregarPromocion($promocion){
         $this->db->insert('promotions', $promocion); 
	}
	
	function actualizarPromocion($promocion, $id){
         $this->db->update('promotions', $promocion, array('pro_idPromotion' => $id)); 
	}
	
	function obtenerPromocion($id){
		$query= $this->db->query(
				'SELECT *
				FROM promotions,images
				WHERE 
				ima_idImage = Images_ima_idImage
				AND
				pro_idPromotion = '.$id.'');
		 return $query->result();
	}
	
	function desactivarPromocion($id){

		$query = $this->db->query('UPDATE promotions
			SET pro_isActive = 0
			WHERE 
			pro_idPromotion = '.$id.'');
	}
	
	function activarPromocion($id){

		$query = $this->db->query('UPDATE promotions
			SET pro_isActive = 1
			WHERE 
			pro_idPromotion = '.$id.'');

	}
	
	function eliminarPromocion($id){

		$query = $this->db->query('delete from promotions WHERE pro_idPromotion = '.$id.'');

	}
	
	
	
}

?>