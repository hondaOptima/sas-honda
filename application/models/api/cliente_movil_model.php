<?php

class Cliente_movil_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	
           $this->crmdb = $this->load->database('crmtraslados', TRUE);
		
	}

	function auto($vin)
	{
        $a=$this->auto_info($vin);
        $s=$this->auto_seguro($vin);
        $a=$a[0];
        $auto_array=array(
        	'auto' => array(
                   "vin"=>$vin,
					"version"=>$a->cus_version,
					"color"=>strtoupper($a->cus_color),
					"modelo"=>$this->model($a->cus_modelo, $a->cus_submarca),
					"anio"=>substr($this->fechaCompra($a->cus_fecha_venta),6),
					"placas"=>$a->cus_placas,
				    "fecha_compra"=>$this->fechaCompra($a->cus_fecha_venta),
				    "fecha_vencimiento_seguro"=>"pendiente",//$s->fecha_vencimiento,
				    "fecha_vencimiento_placas"=>"pendiente",
				    "vencimiento_garantia"=>$this->vigencia($this->fechaCompra($a->cus_fecha_venta)),
				    "capacidad_tanque"=>"pendiente",
				    "motor"=>"pendiente",
					"Transmision"=>"pendiente",
					"rin"=>"pendiente",
				    "presion_llantas"=>"pendiente"
        		)
        	 );

       // $this->auto_detalle($this->model($a->cus_modelo, $a->cus_submarca),
        //	substr($this->fechaCompra($a->cus_fecha_venta),6),$a->cus_version);
        return $auto_array;
	}

	
	function auto_info($vin){
		
		$instruccion ="SELECT  
			   		`cus_version` , 
			    	`cus_color` ,  
					`cus_submarca` ,  
					`cus_placas` , 
					`cus_fecha_venta` ,
					`cus_modelo`,  
					`cus_name` ,
                    `cus_telephone` ,
                    `cus_email`,
					`cus_motor`,
					`cus_transmision`
				     FROM  `customers` 
				     WHERE  `cus_serie` =  '".$vin."' limit 1";
		
		return $this->db->query($instruccion)->result();
		
	} 



	function auto_detalle($mod,$anio,$version)
    {
		$anio = substr($anio,6,4);
	  	$vemodel='a';
		$rin='b';
		$llantas='c';
		$tanque='d';
	    if(empty($mod)) $mod='';
		$lista='';
		$instruccion='
		SELECT
		car_vehicleModels,
		car_rin,
		car_neumaticos,
		car_capacidad_tanque
		FROM
		dbsas.caracteristicas_modelos
		WHERE
		car_vehicleModels like "%'.$mod.'%"
		and
		car_anio like "%'.$anio.'%"
		and
		car_version like "%'.$version.'%"
		limit 1';
	    $res= $this->db->query($instruccion)->result();

		var_dump($res);
	}


	function auto_seguro($vin){
			$lista='';
			$pol='';
			$ase='';
			$fecha='';
			$tel='';

			$instruccion='
			SELECT
				poliza,
			    aseguradora,
			    max(fecha_vencimiento) as fecha_vencimiento,
				vence_credito
			from
				hondaoptima_crm.seguro_contado_tijuana
			where
				vin="'.$vin.'"

			union all

			SELECT
				poliza,
			    aseguradora,
			    max(fecha_vencimiento) as fecha_vencimiento,
				vence_credito
			from
			    hondaoptima_crm.seguro_credito_tijuana
			where
				vin="'.$vin.'"

			union all

			SELECT
				poliza,
			    aseguradora,
			    max(fecha_vencimiento) as fecha_vencimiento,
				vence_credito
			from
			    hondaoptima_crm.seguro_ensenada
			where
				vin="'.$vin.'"
			limit 1

			union all

			SELECT
				poliza,
			    aseguradora,
			    max(fecha_vencimiento) as fecha_vencimiento,
				vence_credito
			from
			    hondaoptima_crm.seguro_gnp_tijuana
			where
				vin="'.$vin.'"

			union all

			SELECT
				poliza,
			    aseguradora,
			    max(fecha_vencimiento) as fecha_vencimiento,
				vence_credito
			from
			    hondaoptima_crm.seguro_mexicali
			where
				vin="'.$vin.'"
			';
		
		$res=$this->crmdb->query($instruccion)->result();;

		foreach ($res as $seguro)
		{
		 
           if (!empty($seguro->poliza) && $seguro->poliza != null) 
           {  
                $tel=" ";
            	if($seguro->aseguradora=='GNP')
           		{
           			$tel='01 800 400 9000';
           		}

				$seguro->{"telefono"} = $tel;

                return $seguro;

           }
		}
		
		return null;

	}

	function vigencia($f)
	{
		if($f=='0000-00-00' || $f=='')
		{
			return 'N/A';
		}
		else
		{
			list($d,$m,$a)=explode('/',$f);
			$fecha = ''.$a.'-'.$m.'-'.$d.'';
			$nuevafecha = strtotime ( '+3 year' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
			return  $nuevafecha;
		}
	}

	function fechaCompra($fecha)
	{
	    if(empty($fecha))
	    {
             return 'N/A';
	    } 
		else
		{
			
            if (strpos($fecha, "/") !== false)
            {
                return $fecha;
            }
            else if (strpos($fecha, "-") !== false)
            {
            	list($aa,$m,$d)=explode('-',$a[4]);
			    return $d.'/'.$m.'/'.$aa;
            }
            else
            {
				return $fecha;
            }

			
		}
	}

	function model($modelo, $submarca)
	{
           $exist =$this->modelExist($modelo);
				$modelReal = '';
				if($exist == 1)
				{
					$modelReal = $this->models($modelo);
				}
				else
				{
					$exist = $this->modelExist($submarca);
					if($exist == 1){
						$modelReal = $this->models($submarca);
					}
				}

				return $modelReal;
	}

	function submarca($modelo, $submarca)
	{
               $exist =$this->modelExist($modelo);
				$preVersion = '';
				if($exist == 1){
					$preVersion = $modelo;
				}else{
					$exist = $this->modelExist($submarca);
					if($exist == 1){
						$preVersion = $submarca;
					}
				}


				return $preVersion;
	}

	function models($m) {
				$m = strtoupper($m);
				$word_found = "";
				$words = array("ACCORD", "CIVIC", "CITY", "HRV", "FIT", "CRV",
				"ODYSSEY", "RIDGELINE", "PILOT");
				$count_words = count($words);
				for($i = 0; $i < $count_words; $i++){
				if (strpos($m, $words[$i]) !== false) {
				$word_found = $words[$i];
				$i = $count_words+1;
				}
			}
			
			return $word_found;
		}
		
	function modelExist($m) {
				$m = strtoupper($m);
				$word_found = "";
				$exist = 0;
				$words = array("ACCORD", "CIVIC", "CITY", "HRV", "FIT", "CRV",
				"ODYSSEY", "RIDGELINE", "PILOT");
				$count_words = count($words);
				for($i = 0; $i < $count_words; $i++){
					if (strpos($m, $words[$i]) !== false) {
						$word_found = $words[$i];
						$exist = 1;
						$i = $count_words+1;
					}
				}
			
			return $exist;
		}
		
	function findVersion($text_in){

			$text_in = trim($text_in, " ");
			$word_found = "";
			$words = array("EX", "LX", "UNIQ", "EXL NAVI", "i-Style", "EXL ", 
			"COOL", "TOURING", "HIT", "TURBO", "EPIC", "FUN");
			$count_words = count($words);
			for($i = 0; $i < $count_words; $i++){
			if (strpos($text_in, $words[$i]) !== false) {
			$word_found = $words[$i];
			$i = $count_words+1;
			}
			}     
			return trim($word_found, " ");
		}

}

?>