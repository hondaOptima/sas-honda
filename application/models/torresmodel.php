<?php

class Torresmodel extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->crm = $this->load->database('crmtraslados', TRUE);
	}

	//Regresa el listado de torres.
	function get_torres($agencia = null)
    {
		$sql = "SELECT api_vin AS 'vin', api_torre AS 'torre' FROM autos_en_piso WHERE api_cd= '".$agencia."' AND api_venta = 0 ORDER BY api_torre ASC";
		$autos_en_piso = $this->crm->query($sql)->result();

		$sql2 = "SELECT tor_ID AS 'id', tor_torre AS 'torre' FROM inv_torre WHERE tor_agencia='".$agencia."' ORDER BY tor_torre";
		$torres = $this->crm->query($sql2)->result();

		$array = $json = array();

		if(count($autos_en_piso) > 0)
		{
			foreach ($autos_en_piso as $a)
				$array[] = $a->torre;

			foreach($torres AS $t)
			{
				if (in_array($t->torre, $array))
				{

				}
				else
					$json[] = array("id" => $t->id, "torre" => $t->torre);
			}
		}
		return $json;
    }

	function update_torre()
	{
		$this->crm->trans_start();
		extract($_GET);
		$data = array('api_torre' => $torre);
		$this->crm->where('api_vin', $vin);
		$this->crm->where('api_cd', $agencia);
		$this->crm->update('autos_en_piso', $data);
		$this->crm->trans_complete();
		if ($this->crm->trans_status() === FALSE) return 0;
		else return 1;
	}


	// --------------------------------------------------------------------------------------------------------------------------------
	function paso_1($vco = null)
	{
		error_reporting(E_ALL);
		$str = "";
		if($vco != null)
		{
			$sql_1 = "SELECT * FROM detalle_asp d WHERE d.das_vco = '".$vco."'";
			$r_1 = $this->crm->query($sql_1)->result();

			if(count($r_1) == 1)//Si solo hay un registro.
			{
				$r_1 = $r_1[0];
				$str = "UPDATE detalle_asp SET das_status = 'eliminado' WHERE das_vco = '".$vco."'".";<br>";//Paso 1.

				$sql_2 = "SELECT * FROM asigancion_auto WHERE aau_IdFk = ".$r_1->das_IDdetalle_asp;

				$r_2 = $this->crm->query($sql_2)->result();//Paso 2.
				if(count($r_2) == 1)
				{
					$r_2 = $r_2[0];

					//$sql_3 = "SELECT * FROM apartado WHERE apartado_id_contacto = ".$r_2->contacto_con_IDcontacto;
					//$sql_3 = "SELECT * FROM apartado WHERE apartado_id_contacto = ".$r_2->contacto_con_IDcontacto." ORDER BY apartado_fecha_recibo DESC LIMIT 1;";
					$sql_3 = "SELECT a.*,CONCAT_WS(' ',c.con_nombre,c.con_apellido) AS 'nombre' FROM apartado a LEFT JOIN contacto c ON a.apartado_id_contacto = c.con_IDcontacto WHERE apartado_id_contacto = ".$r_2->contacto_con_IDcontacto." ORDER BY apartado_fecha_recibo DESC LIMIT 1;";

					$r_3 = $r_2 = $this->crm->query($sql_3)->result();//Paso 3

					if(count($r_3) == 1)
					{
						$r_3 = $r_3[0];
						$str.="UPDATE apartado SET apartado_status = 3 WHERE apartado_id = ".$r_3->apartado_id.";<br>";//Paso 4.
						$str.="-- Se modifico al contacto ".$r_3->nombre." con la fecha ".$r_3->apartado_fecha_recibo." con el auto ".$r_3->apartado1_id_version." ".$r_3->apartado1_id_color;
					}
					else $str.="-- Paso 3. Encontramos errores en la busca del apartado. Este registro tiene ".count($r_2)." registros.<br>";
					//$str.= " -- Este registro tiene".count($r_2)." registro.<br>";
				}
				else $str.="-- Paso 2. Tiene ".count($r_2)." registros.<br>";























			}
			/*else if(count($r_1) > 1) $str.= "-- El VCO ".$vco." tiene más de un registro.";
			else if(count($r_1) == 0) $str.="-- No hay registros para el VCO ".$vco;
			else $str.="-- Error con el VCO ".$vco;*/
			return $str;
		}
		else return null;
	}


	function preparar_garantias()
	{
		$sql = "SELECT * FROM `TABLE 114`";
		return $this->db->query($sql)->result();
	}
}

?>
