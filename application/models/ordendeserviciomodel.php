<?php

class Ordendeserviciomodel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

    function unafuncion(){
    $query= $this->db->query(
				'
        SELECT

*

FROM `serviceOrders`,enproceso ,susers

WHERE

`seo_idServiceOrder`=enp_ID_proceso

and DATE(`seo_date`) between "2015-05-01" and "2015-05-31"

and seo_adviser=sus_idUser

and sus_workshop IN (3,2,1) ');

return $query->result();

    }

    function GetCampsVin($vin){
    	$query = $this->db->query('SELECT cam_ID as Id, cam_campania as Description FROM campanias
    where cam_vin in

        (SELECT sev_vin
        FROM
        enproceso,
        serviceOrders,
        serviceOrder_vehicleInformation,
        appointments
        WHERE
        sev_idVehicleInformation=seo_idServiceOrder
        and sev_vin = "'.$vin.'"
        and
        seo_idServiceOrder=enp_ID_proceso
        and
        enp_ID_cita=app_idAppointment)');
        return $query->result();
    }

	function getExtGarantiaSta($vin)
	{
		$query = $this->db->query('SELECT `km_status`,`fecha_vigencia` FROM `garantias` WHERE `vin`="'.$vin.'" ');
		return $query->result();
	}
	function garantiasBaseExt($vin)
	{
		$query = $this->db->query('SELECT `fecha_vencimiento` FROM `garantias_base` WHERE `vin`="'.$vin.'" ');
		return $query->result();
	}

	function GetCusOda(){
		$query = 'select cus_idCustomer as Id, cus_name as Name, cus_name as LastName, cus_email as Email from customers order by cus_idCustomer desc limit 30';
		$exec = $this->db->query($query);
		return $exec;
	}

	function obtenerCampanias($vin)
	{
		$query= $this->db->query(
				'
				SELECT *
				FROM campanias
				where cam_vin="'.$vin.'"
				and (cam_status = 1 || cam_status = 2)

				');

		return $query->result();
	}

	function obtenerCampaniasTodas($vin)
	{
		$query= $this->db->query(
				'
				SELECT *
				FROM campanias
				where cam_vin="'.$vin.'"
				');

		return $query->result();
	}

	function GetHistorial($id){
		$query= $this->db->query(
				'
				SELECT * from susers
				where sus_idUser = '.$id.'
				');

		return $query->result();
	}

	function listarPromociones(){
		$query= $this->db->query(
				'
				SELECT *
				FROM promotions, images
				WHERE
				ima_idImage = Images_ima_idImage
				and pro_isActive=1
				');

		return $query->result();
	}
	function obtenerUsuario($id){
		$query= $this->db->query(
				'
				SELECT *,
				CAST(sus_status AS unsigned integer) As status
				FROM susers,userTypes,permisos, roles, workshops
				WHERE
				ust_idUserType = sus_userType
				AND
				rol_idRol = sus_rol
				AND
				wor_idWorkshop = sus_workshop
				AND
				sus_idUser = '.$id.'
				');
		 return $query->result();
	}

	function getOrdenServicios($id){

		$query= $this->db->query(
				'

SELECT *
FROM serviceOrders_service,services,serviceTypes
WHERE ssr_service=ser_serviceType
and ssr_modelo=ser_vehicleModel
and ssr_IDNULL='.$id.'
and ser_serviceType=set_idServiceType

				');
		 return $query->result();
		}


		function getOrdenServiciossv($id){

		$query= $this->db->query(
				'
SELECT *
FROM servicios_varios
WHERE sev_id_orden='.$id.'');
		 return $query->result();
		}

	function checkFotos($id){
		$query= $this->db->query(
				"
				SELECT sei_inf_der FROM `serviceOrders_images` WHERE
`sei_inf_der`!='noimagen.png'
and `sei_inf_izq`!='noimagen.png'
and `sei_pos_der`!='noimagen.png'
and `sei_pos_izq`!='noimagen.png'
and `sei_tablero`!='noimagen.png'
and `sei_ID`=".$id."
				");
		 return $query->result();
		}

		function insertOrden($idapp,$idsuser)
		{
			$app = $this->Ordendeserviciomodel->app($idapp);

			date_default_timezone_set('America/Chihuahua');
			$orden = array(
				'seo_idServiceOrder' => NULL,
				'seo_date' => date('Y-m-d'),
				'seo_promisedTime' => NULL,
				'seo_amount' => NULL,
				'seo_tower' =>'0',
				'seo_tapetes' => '2',
				'seo_tapones' => '2',
				'seo_herramienta' => '2',
				'seo_reflejantes' => '2',
				'seo_ecualizador' => '2',
				'seo_tapon' => '2',
				'seo_ventanas' => '2',
				'seo_espejo' => '2',
				'seo_radio' => '2',
				'seo_llantarefaccion' => '2',
				'seo_extinguidor' => '2',
				'seo_gato' => '2',
				'seo_carnet' => '2',
				'seo_vestidura' => '2',
				'seo_antena' => '2',
				'seo_encendedor' => '2',
				'seo_limpiadores' => '2',
				'seo_cables' => '2',
				'seo_estereo' => '2',
				'seo_carroceria' => '2',
				'seo_parabrisas' => '2',
				'seo_combustible' => NULL,
				'seo_comments' => NULL,
				'seo_serviceOrder' => NULL,
				'seo_vehicle' => NULL,
				'seo_adviser' => $idsuser,
				'seo_status' => '1',
				'seo_timeReceived' => date('Y-m-d h:i:s'),
				'seo_technicianTeam' => '0',
				'seo_inKilometers' => '0',
				'seo_outKilometer' => '0',
				'seo_IDapp' => $idapp,
				'seo_cliente' => 0,
				'seo_garantia' => 0,
				'seo_se_entregan' => '1',
				'seo_ser_domi' => '0',
				'seo_poliza' => '0',
				'seo_num_poliza' => '',
				'seo_consecuencias' => '',
				'seo_pago' => '1',
				'seo_vehiculo' => '0',
				'seo_control_sta'=>'0',
				'seo_control_hora'=>'',
				'seo_control_entregado'=>'0',
				'seo_persona_recoge'=>'',
				'seo_telefono_recoge'=>'',
				'seo_recoge_check'=>''
			);

			$this->db->insert('serviceOrders', $orden);
			$IDO = $this->db->insert_id();
			$cliente = array(
				'sec_idClientInformation' => $IDO,
				'sec_nombre' => '',
				'sec_email' => '',
				'sec_tel' => '',
				'sec_cel' => '',
				'sec_calle' => '',
				'sec_num_ext' => '',
				'sec_colonia' =>'',
				'sec_cp' => '',
				'sec_ciudad' => '',
				'sec_estado' => '',
				'sec_rfc' => '',
				'sec_venta' => '',
				'sec_ultimo_servicio' => '',
				'sec_firma' => ''
			);

			$vehiculo = array(
				'sev_idVehicleInformation' => $IDO,
				'sev_marca' => 'Honda',
				'sev_sub_marca' => '0',
				'sev_version' => '',
				'sev_color' => '0',
				'sev_modelo' => '',
				'sev_vin' => '',
				'sev_capacidad' => '',
				'sev_km_recepcion' => '',
				'sev_placas' => '',
				'sev_ultima_fecha' => '',
				'sev_orden' => '',
				'sev_fecha_venta' => '',
				'sev_motor' => '',
				'sev_trans' => ''
			);

			$images = array(
				'sei_ID' =>  $IDO,
				'sei_inf_der' => 'noimagen.png',
				'sei_inf_izq' => 'noimagen.png',
				'sei_pos_der' => 'noimagen.png',
				'sei_pos_izq' => 'noimagen.png',
				'sei_tablero' => 'noimagen.png',
				'sei_carpeta' => '',
				'sei_extra_uno' => 'noimagen.png',
				'sei_extra_dos' => 'noimagen.png',
				'sei_extra_tres' => 'noimagen.png',
				'sei_cha_uno' => '',
				'sei_cha_dos' => '',
				'sei_cha_tres' => '',
				'sei_cha_cuatro' => ''
			);

			if($app[0]->app_service==30)
				$ttip='garantia';
			if($app[0]->app_service==50)
				$ttip='interna';
			else
				$ttip='venta';

			$ser = array(
				'ssr_ID'=>'NULL',
				'ssr_service'=>$app[0]->app_service,
				'ssr_tipo'=>'master',
				'ssr_IDNULL'=>$IDO,
				'ssr_venta'=>$ttip,
				'ssr_modelo'=>$app[0]->app_vehicleModel
			);

			$multi = array(
				'mul_id'=>'NULL',
				'mul_luztablero'=>'3',
				'mul_luces'=>'3',
				'mul_parabrisas'=>'3',
				'mul_frenomano'=>'3',
				'mul_claxon'=>'3',
				'mul_clutch'=>'3',
				'mul_electrolito'=>'3',
				'mul_terminales'=>'3',
				'mul_sujecion'=>'3',
				'mul_soportes'=>'3',
				'mul_liquidos'=>'3',
				'mul_radiadores'=>'3',
				'mul_ajustefreno'=>'3',
				'mul_flechas'=>'3',
				'mul_escape'=>'3',
				'mul_fugas'=>'3',
				'mul_gomas'=>'3',
				'mul_basesamortiguadores'=>'3',
				'mul_terminalesdirecion'=>'3',
				'mul_amortiguadores'=>'3',
				'mul_proximo'=>'',
				'mul_tipo'=>'',
				'mul_km'=>'',
				'mul_orden'=>$IDO
			);

			$llantas = array(
				'llan_id'=>'NULL',
				'llan_di'=>'3',
				'llan_diprof'=>'0',
				'llan_dipres'=>'0',
				'llan_dd'=>'3',
				'llan_ddprof'=>'0',
				'llan_ddpres'=>'0',
				'llan_ti'=>'3',
				'llan_tiprof'=>'0',
				'llan_tipres'=>'0',
				'llan_td'=>'3',
				'llan_tdprof'=>'0',
				'llan_tdpres'=>'0',
				'llan_ref'=>'3',
				'llan_refprof'=>'0',
				'llan_refpres'=>'0',
				'llan_orden'=>$IDO
			);

			$frenos = array(
				'fre_id'=>'NULL',
				'fre_di'=>'3',
				'fre_dicero'=>'0',
				'fre_diactual'=>'0',
				'fre_dd'=>'3',
				'fre_ddcero'=>'0',
				'fre_ddactual'=>'0',
				'fre_ti'=>'3',
				'fre_ticero'=>'0',
				'fre_tiactual'=>'0',
				'fre_td'=>'3',
				'fre_tdcero'=>'0',
				'fre_tdactual'=>'0',
				'fre_orden'=>$IDO
			);


			$this->db->insert('multipuntos',$multi);
			$this->db->insert('llantas',$llantas);
			$this->db->insert('frenos',$frenos);

			$this->db->insert('serviceOrder_clientInformation',$cliente);
			$this->db->insert('serviceOrder_vehicleInformation',$vehiculo);
			$this->db->insert('serviceOrders_images',$images);
			$this->db->insert('serviceOrders_service',$ser);

			return $IDO;
		}


			function insertOrdenB($idapp,$idsuser){



		$multi = array(
			'mul_id'=>'NULL',
			'mul_luztablero'=>'0',
			'mul_luces'=>'0',
			'mul_parabrisas'=>'0',
			'mul_frenomano'=>'0',
			'mul_claxon'=>'0',
			'mul_clutch'=>'0',
			'mul_electrolito'=>'0',
			'mul_terminales'=>'0',
			'mul_sujecion'=>'0',
			'mul_soportes'=>'0',
			'mul_liquidos'=>'0',
			'mul_radiadores'=>'0',
			'mul_ajustefreno'=>'0',
			'mul_flechas'=>'0',
			'mul_escape'=>'0',
			'mul_fugas'=>'0',
			'mul_gomas'=>'0',
			'mul_basesamortiguadores'=>'0',
			'mul_terminalesdirecion'=>'0',
			'mul_amortiguadores'=>'0',
			'mul_orden'=>200
		);

		$llantas = array(
			'llan_id'=>'NULL',
			'llan_di'=>'0',
			'llan_diprof'=>'0',
			'llan_dipres'=>'0',
			'llan_dd'=>'0',
			'llan_ddprof'=>'0',
			'llan_ddpres'=>'0',
			'llan_ti'=>'0',
			'llan_tiprof'=>'0',
			'llan_tipres'=>'0',
			'llan_td'=>'0',
			'llan_tdprof'=>'0',
			'llan_tdpres'=>'0',
			'llan_ref'=>'0',
			'llan_refprof'=>'0',
			'llan_refpres'=>'0',
			'llan_orden'=>200
		);

		$frenos = array(
			'fre_id'=>'NULL',
			'fre_di'=>'0',
			'fre_dicero'=>'0',
			'fre_diactual'=>'0',
			'fre_dd'=>'0',
			'fre_ddcero'=>'0',
			'fre_ddactual'=>'0',
			'fre_ti'=>'0',
			'fre_ticero'=>'0',
			'fre_tiactual'=>'0',
			'fre_td'=>'0',
			'fre_tdcero'=>'0',
			'fre_tdactual'=>'0',
			'fre_orden'=>300
		);


		$this->db->insert('multipuntos',$multi);
		$this->db->insert('llantas',$llantas);
		$this->db->insert('frenos',$frenos);
		}


        function InsertMailWithoutPublicity($mail){

    	 $querycheck= $this->db->query(
				'
				select * from correosSinPublicidad where correo = "'.$mail.'"');

		$querycheck->result();

		if($querycheck->num_rows() == 0){
			$query= $this->db->query(
				'
				INSERT INTO `correosSinPublicidad`( `correo`, `fecha`) VALUES ("'.$mail.'",now())
				');

			$query->result();
		}
    }

		function insertPizarron($info, $id)
		{
			$this->db->insert('pizarron',$info);

			$query= $this->db->query(
			'
			update appointments set app_timeToOrder=DATE_SUB(NOW(), INTERVAL 2 HOUR) where app_idAppointment=
			(select seo_IDapp from serviceOrders where seo_idServiceOrder = '.$id.')
			');
			$query->result();

		}

		function insertCliente($info){
			$this->db->insert('customers',$info);
			$IDO = $this->db->insert_id();
			return $IDO;
			}



		function listadeesperaUpdate($dato,$id){

         $this->db->update('enproceso', $dato, array('enp_ID_proceso' => $id));

			}


	function app($id){
		$query= $this->db->query(
				'
				SELECT *
				FROM appointments
				WHERE app_idAppointment='.$id.'
				');

		return $query->result();
		}

		function verInsertOrden($id)
		{
			$query= $this->db->query('SELECT seo_date FROM serviceOrders WHERE seo_IDapp='.$id);
			return $query->result();
		}

		function updateCliente($dato, $id)
		{
			$this->db->update('serviceOrder_clientInformation', $dato, array('sec_idClientInformation' => $id));
		}

		function updateVehiculo($dato, $id)
		{
			$this->db->update('serviceOrder_vehicleInformation', $dato, array('sev_idVehicleInformation' => $id));
		}

		function updateOrden($dato, $id)
		{
			$this->db->update('serviceOrders', $dato, array('seo_idServiceOrder' => $id));
		}

		function updateOrdenCaptura($dato, $id) {
			$this->db->update('serviceOrders', $dato, array('seo_idServiceOrder' => $id));
		}

		function updateEspera($dato, $id)
		{
			$this->db->update('enproceso', $dato, array('enp_ID_proceso' => $id));
		}

		function updateImages($dato, $id)
		{
			$this->db->update('serviceOrders_images', $dato, array('sei_ID' => $id));
		}

		function getCliente($id)
		{
			$query= $this->db->query('SELECT seo_cliente FROM  serviceOrders WHERE seo_idServiceOrder='.$id);
			return $query->result();
		}

		function updateControl($dato, $id)
		{
			$this->db->update('serviceOrders', $dato, array('seo_idServiceOrder' => $id));
		}

		function updateControlProceso($dato, $id)
		{
			$this->db->update('enproceso', $dato, array('enp_ID_proceso' => $id));
		}

function updateOrdenLlenado($id,$campo,$valor){
$tabla='';
$ref='';

if($campo=='mask_phone'){
	$campo='sec_clientTelephone';
	$tabla='serviceOrder_clientInformation';
	$ref='sec_idClientInformation';
	           }else{

	if($campo=='mask_number_km'){

	$campo='seo_inKilometers';
	$tabla='serviceOrders';
	$ref='seo_idServiceOrder';

		}

	else{

list($tab,$res)=explode("_",$campo);

if($tab=='sec'){
	$tabla='serviceOrder_clientInformation';
	$ref='sec_idClientInformation';
	$IDC=$this->Ordendeserviciomodel->getCliente($id);

	if($campo=='sec_nombre'){$camCliente='cus_name';}
	if($campo=='sec_rfc'){$camCliente='cus_rfc';}
	if($campo=='sec_email'){$camCliente='cus_email';}
	if($campo=='sec_calle'){$camCliente='cus_calle';}
	if($campo=='sec_num_ext'){$camCliente='cus_num';}
	if($campo=='sec_colonia'){$camCliente='cus_colonia';}
	if($campo=='sec_cp'){$camCliente='cus_cp';}
	if($campo=='sec_ciudad'){$camCliente='cus_ciudad';}
	if($campo=='sec_estado'){$camCliente='cus_estado';}
	if($campo=='sec_tel'){$camCliente='cus_telephone';}



	$dup=array(''.$camCliente.''=>$valor);
	$this->db->update('customers', $dup, array('cus_idCustomer' => $IDC[0]->seo_cliente));



	           }


if($tab=='sev'){
	$tabla='serviceOrder_vehicleInformation';
	$ref='sev_idVehicleInformation';
	$IDC=$this->Ordendeserviciomodel->getCliente($id);
	if($campo=='sev_marca'){$camCliente='cus_marca';}
	if($campo=='sev_sub_marca'){$camCliente='cus_submarca';}
	if($campo=='sev_modelo'){$camCliente='cus_modelo';}
	if($campo=='sev_version'){$camCliente='cus_version';}
	if($campo=='sev_capacidad'){$camCliente='cus_capacidad';}
	if($campo=='sev_placas'){$camCliente='cus_placas';}
	if($campo=='sev_color'){$camCliente='cus_color';}
	if($campo=='sev_vin'){$camCliente='cus_serie';}
	if($campo=='sev_km_recepcion'){$camCliente='cus_km_recepcion';}
	if($campo=='sev_motor'){$camCliente='cus_motor';}
	if($campo=='sev_trans'){$camCliente='cus_transmision';}
	$dup=array(''.$camCliente.''=>$valor);
	$this->db->update('customers', $dup, array('cus_idCustomer' => $IDC[0]->seo_cliente));
	           }

if($tab=='seo'){
	$tabla='serviceOrders';
	$ref='seo_idServiceOrder';
	           }

if($tab=='sei'){
	$tabla='serviceOrders_images';
	$ref='sei_ID';
	           }

if($tab=='app'){
	$tabla='appointments';
	$ref='app_idAppointment';
	           }


	}}



	echo $con='
				update  '.$tabla.'

				set  '.$campo.'="'.$valor.'"

				 where  '.$ref.'='.$id.'
				';
$query= $this->db->query($con);

		return $query->result();

	}

		function getInfoCliente($vin)
		{
			$query= $this->db->query('SELECT * FROM customers WHERE cus_serie="'.$vin.'"');
			return $query->result();
		}

		function getlistadeEspera($id)
		{
			$query = $this->db->query('SELECT * FROM enproceso WHERE enp_ID_asesor='.$id.' and enp_status=1');
			return $query->result();
		}

		function getclienteConGarantia($vin)
		{
			$query= $this->db->query('SELECT * FROM garantia_exist WHERE vin="'.$vin.'"');
			return $query->result();
		}

		function getInfoCita($id)
		{
			$query= $this->db->query('SELECT * FROM  appointments WHERE app_idAppointment='.$id);
			return $query->result();
		}

		function getTecnicos($id)
		{
			$query= $this->db->query('select * from equipos,susers,workshops where equ_tecnico=sus_idUser and sus_workshop=wor_idWorkshop and equ_asesor="'.$id.'" and sus_isDeleted=0 and sus_isActive=1');
			return $query->result();
		}

		function getTecnico($id)
		{
			$query= $this->db->query('select * from susers where sus_idUser='.$id);
			return $query->result();
		}

		function getOrden($id)
		{
			$query= $this->db->query('
			SELECT *
			FROM
			enproceso,
			serviceOrders,
			serviceOrder_clientInformation,
			serviceOrder_vehicleInformation,
			appointments,
			susers,
			services,
			serviceOrders_images

			WHERE
			sec_idClientInformation=seo_idServiceOrder
			and
			sev_idVehicleInformation=seo_idServiceOrder
			and
			seo_idServiceOrder='.$id.'
			and
			seo_idServiceOrder=enp_ID_proceso
			and
			enp_ID_cita=app_idAppointment
			and
			enp_ID_asesor=sus_idUser
			and
			app_vehicleModel=ser_vehicleModel
			and
			app_service=ser_serviceType
			and
			seo_idServiceOrder=sei_ID
			');

			return $query->result();
		}

		function listarOrdenes()
		{
			$query = $this->db->query('SELECT * FROM serviceOrders');
			return $query->result();
		}

		function getCita($id)
		{
			$query= $this->db->query('SELECT seo_idServiceOrder,seo_IDapp
										FROM serviceOrders
										WHERE seo_idServiceOrder='.$id);

			return $query->result();
		}

		function updateCitaOrden($id)
		{
			$query= $this->db->query('update appointments set app_status="0"  where  app_idAppointment='.$id);
		}

	function listaparaCaptura($cd,$fecha,$sta){
		if($fecha==0){
			$and='';
			}
		else{
			$and='and piz_fecha="'.$fecha.'"';
			}
		$query= $this->db->query(
				'
				SELECT *
				FROM enproceso,pizarron,serviceOrder_vehicleInformation,susers,serviceOrders
				where
				enp_ID_proceso=piz_ID_orden
				'.$and.'
				and enp_status='.$sta.'
				and sev_idVehicleInformation=piz_ID_orden
				and sev_idVehicleInformation=enp_ID_proceso
				and enp_ID_asesor=sus_idUser
				and sus_workshop='.$cd.'
				and piz_ID_orden=seo_idServiceOrder
				and enp_ID_proceso=seo_idServiceOrder
				and sev_idVehicleInformation=seo_idServiceOrder
				');

		return $query->result();
	}


	function listaparaCapturaEspera($cd){

		$query= $this->db->query(
				'
				SELECT *
				FROM enproceso,serviceOrder_vehicleInformation,susers,serviceOrders
				where
				 enp_status=1
				and sev_idVehicleInformation=enp_ID_proceso
				and enp_ID_asesor=sus_idUser
				and sus_workshop='.$cd.'
				and enp_ID_proceso=seo_idServiceOrder
				and sev_idVehicleInformation=seo_idServiceOrder
				');

		return $query->result();
	}

	function contar_pizarron($cd){

		$query= $this->db->query(
				'
				SELECT *
				FROM lista_pizarron
				where
				lsp_agencia="'.$cd.'"
				');

		return $query->result();
	}

	function contar_pizarron_minimo($cd){

		$query= $this->db->query(
				'
				SELECT min(lsp_numero) as numero
				FROM lista_pizarron
				where
				lsp_agencia="'.$cd.'"
				');

		return $query->result();
	}

	function updateTecnicoSM($tec,$tipo){

		if($tipo=='suma'){ $sg='+';}
		if($tipo=='resta'){ $sg='-';}

		$query= $this->db->query(
				'
				UPDATE lista_pizarron
				SET lsp_numero= lsp_numero '.$sg.' 1
				where
				lsp_tecnico="'.$tec.'"
				');
		}



	function listadeespera($cita)
	{
         $this->db->insert('enproceso', $cita);
	}

	function insertControl($info){
         $this->db->insert('control_flujo', $info);

	}
	function insert_inventario_pizarron($datos){
		         $this->db->insert('pizarron_inventario', $datos);

	}
	function insert_garantia($datos){
		         $this->db->insert('garantia_exist', $datos);

	}
	function updateCita($cita, $id){
         $this->db->update('appointments', $cita, array('app_idAppointment' => $id));
	}

	function updateGarantiaExist($garantia, $vin){
         $exec = $this->db->update('garantia_exist', $garantia, array('vin' => $vin));
		 return $exec;
	}

	function OsEliminarOrden($id)
	{
		$this->db->delete('serviceOrders', array('seo_idServiceOrder' => $id));
		$this->db->delete('serviceOrder_clientInformation', array('sec_idClientInformation' => $id));
		$this->db->delete('serviceOrder_vehicleInformation', array('sev_idVehicleInformation' => $id));
		$this->db->delete('serviceOrders_service', array('ssr_IDNULL' => $id));
		$this->db->delete('pizarron', array('piz_ID_orden' => $id));
		$this->db->delete('enproceso', array('enp_ID_proceso' => $id));
		$this->db->delete('serviceOrders_images', array('sei_ID' => $id));
	}

	function updateOSAsesor($id,$info) {
		$this->db->update('serviceOrders', $info, array('seo_adviser' => $id));
	}

	function servicios_por_vin($vin)
	{
		$sql = "SELECT v.sev_marca AS 'marca', v.sev_sub_marca AS 'submarca', v.sev_modelo AS 'modelo',
				CONCAT_WS(' ',v.sev_marca,v.sev_sub_marca,v.sev_modelo) AS 'auto',c.sec_nombre AS 'cliente',
				DATE_FORMAT(o.seo_timeReceived,'%Y-%m-%d') AS 'fecha',DATE_FORMAT(o.seo_timeReceived,'%h:%i:%s %p') AS 'time',
				o.seo_timeReceived AS 'fecha_time',o.seo_idServiceOrder AS 'id_orden'
				FROM serviceOrder_vehicleInformation v
				LEFT JOIN serviceOrder_clientInformation c ON c.sec_idClientInformation = v.sev_idVehicleInformation
				LEFT JOIN serviceOrders o ON v.sev_idVehicleInformation = o.seo_idServiceOrder
				WHERE v.sev_vin = '".$vin."'";
		return $this->db->query($sql)->result();
	}

	function is_service_order_sent($id)//Id de la orden.
	{
		// $sql = "SELECT seo_is_email_advise_send AS 'is_sent'
		// 		FROM serviceOrders o
		// 		WHERE o.seo_idServiceOrder = ".$id;

		$sql = "SELECT o.seo_is_email_advise_send AS 'is_sent',DATE_FORMAT(o.seo_timeReceived,'%Y-%m-%d') AS 'fecha'
				FROM serviceOrders o
				WHERE o.seo_idServiceOrder = $id
				AND DATE_FORMAT(o.seo_timeReceived,'%Y-%m-%d') = '".date("Y-m-d")."'";

		$row = $this->db->query($sql)->row();
		if($row->is_sent == 1) return true; else return false;
	}

}

?>
