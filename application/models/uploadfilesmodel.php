<?php

class Uploadfilesmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('crmtraslados',true);
		$this->db3 = $this->load->database('dboptima', true);
	}

	function GetCustomers($find = null, $rows = 5, $idhuser){
		
		$ex_customers = $this->db2->query('select con_IDcontacto as Id, concat(con_nombre," ",con_apellido) as Name, con_folder as FolderName from contacto where concat(con_nombre," ",con_apellido) like "%'.$find.'%" and huser_hus_IDhuser = '.$idhuser.' limit '.$rows);

		return $ex_customers->result();
	}

	function GetCustomersFirst($rows = 5, $idhuser){
		
		$ex_customers = $this->db2->query('select con_IDcontacto as Id, concat(con_nombre," ",con_apellido) as Name, con_folder as FolderName from contacto where huser_hus_IDhuser = '.$idhuser.' order by con_IDcontacto desc limit '.$rows);

		return $ex_customers->result();
	}

	function GetFiles($customer){
		$ex_files = $this->db2->query('select exp_IDexpediente as Id, exp_name as Name, exp_name_file as FileName, exp_folder_arbol as ExpName, contacto_con_IDcontacto as CustomerId from expediente where contacto_con_IDcontacto = '.$customer.'');
		return $ex_files->result();
	}
}

?>