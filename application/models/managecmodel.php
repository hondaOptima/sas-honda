<?php

class Managecmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
    
    function getAsesores($wshop)
    {
        $query = 'SELECT * FROM `susers`   
                 WHERE `sus_rol` = 3
                 AND `sus_workshop` = '.$wshop;
        
        $execute = $this->db->query($query);
        return $execute->result();
    }

    function getGraficaCampanias($ciudad){
        $query = 'select 
        distinct(substr(cam_campania,1,6)) as campania, 
        count(cam_campania) as posibles,
        count(CASE WHEN cam_status = 0  then 1 END) as realizadas,
        count(CASE WHEN cam_status = 1 then 1 END) as pendientes 
        from campanias
        where cam_ciudad = "'.$ciudad.'"
        group by campania';
        $execute = $this->db->query($query);
        $varExecute = $execute->result();
        return $varExecute;
    }
    
    function getEfficiency($wshop)
    {
        $fecha_actual = date('d/m/y');
        $dia_actual_mes = (date('j')-1);
        
        
        $arreglo = array();
        for($i = 0; $i <= date('t'); $i++)
        {
            
            $query = 'select 
            sus_idUser ,
            COUNT(CASE WHEN sus_idUser = seo_adviser THEN 1 END) posibles, 
            COUNT(CASE WHEN (cam_status = 2 || cam_status = 1) and sus_idUser = seo_adviser THEN 1 END) pendientes
            from susers,
            (select cam_status, seo_adviser from campanias, serviceOrder_vehicleInformation, serviceOrders
            where cam_vin = sev_vin
            and sev_idVehicleInformation = seo_idServiceOrder
            and seo_date = ADDDATE(date_sub(curdate(), INTERVAL DAY(NOW())-1 DAY),'.$i.')) as camps
            where sus_rol = 3 
            and sus_workshop = '.$wshop.'
            group by sus_idUser


            ';
            $execute = $this->db->query($query);
            $varExecute = $execute->result();
            //$arrayencode = json_decode(json_encode($varExecute), true);
            $arreglo[$i] = $varExecute; 
        }
        
        return $arreglo;
    }
    
}
     
?>