<?php

class Modelovehiculomodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	
	function listarModelos($per){
		$query= $this->db->query(
				'
				SELECT *
				FROM vehicleModels
				');

		return $query->result();
	}
	
	function modelospro(){
		$query= $this->db->query(
				'
				SELECT *
				FROM serviceTypes
				WHERE
				set_tipo="pro"
				and set_isActive=1
				');

		return $query->result();
		}
		
		function modelosnopro(){
		$query= $this->db->query(
				'
				SELECT *
				FROM serviceTypes
				WHERE
				set_tipo="nopro"
				and set_isActive=1
				');

		return $query->result();
		}
	
	function agregarModelo($modelo){
         $this->db->insert('vehicleModels', $modelo);
		 $id = $this->db->insert_id();
		return $id; 
	}
	
	function actualizarModelo($modelo, $id){
         $this->db->update('vehicleModels', $modelo, array('vem_idVehicleModel' => $id)); 
	}
	
	function obtenerModelo($id){
		$query= $this->db->query(
				'SELECT *
				FROM vehicleModels
				WHERE
				vem_idVehicleModel = '.$id.'');
		 return $query->result();
	}
	
	function desactivarModelo($id){

		$query = $this->db->query('UPDATE vehicleModels
			SET vem_isActive = 0
			WHERE 
			vem_idVehicleModel = '.$id.'');
	}
	
	function activarModelo($id){

		$query = $this->db->query('UPDATE vehicleModels
			SET vem_isActive = 1
			WHERE 
			vem_idVehicleModel = '.$id.'');
	}
}

?>