<?php

class Productividadmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}


	function takataPendientes($city){
		$query = null;
		if($city == 2){
			$query = $this->db->query('
			select cus.cus_serie,`COL 2` as camp,cus.cus_name,cus.cus_telephone,cus.cus_email,cus.cus_modelo, cus.cus_version,cus.cus_color,cus.cus_submarca 
			
			from 
				`takata_mexicali`, (select * from customers group by cus_serie) as cus
			where concat(substring(`COL 1`,0,6),`COL 1`) not IN 
			(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)
			and cus.cus_serie = `COL 2`');
		}else{
			if($city == 3){
				$query = $this->db->query('
				select cus.cus_serie,`COL 2` as camp,cus.cus_name,cus.cus_telephone,cus.cus_email,cus.cus_submarca ,cus.cus_modelo, cus.cus_version,cus.cus_color from (select * from `TABLE 93` 
WHERE `TABLE 93`.`COL 3` IN (SELECT `COL 3` FROM `TABLE 99` )) AS camprep, (select * from customers group by cus_serie) as cus
				where concat(substring(`COL 2`,0,6),`COL 3`) not IN 
				(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)
				and cus.cus_serie = `COL 3`');
			}else{
				if($city == 1){
					$query = $this->db->query('
					select cus.cus_serie,`COL 2` as camp,cus.cus_name,cus.cus_telephone,cus.cus_email from `TABLE 95`, (select * from customers group by cus_serie) as cus
					where concat(substring(`COL 2`,0,6),`COL 3`) not IN 
					(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)
					and cus.cus_serie = `COL 3`');
				}
			}
		}
		
		return $query->result();
	}

	function takataRealizadas($city){
		$query = null;
		if($city == 2){
			$query = $this->db->query('
			select cus.cus_serie,`COL 1` as camp,cus.cus_name,cus.cus_telephone,cus.cus_email from `takata_mexicali`, (select * from customers group by cus_serie) as cus
			where concat(substring(`COL 1`,0,6),`COL 2`) IN 
			(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)
			and cus.cus_serie = `COL 2`');
		}else{
			if($city == 3){
				$query = $this->db->query('
				select cus.cus_serie,`COL 2` as camp,cus.cus_name,cus.cus_telephone,cus.cus_email from (select * from `TABLE 93` 
WHERE `TABLE 93`.`COL 3` IN (SELECT `COL 3` FROM `TABLE 99` )) AS camprep, (select * from customers group by cus_serie) as cus
				where concat(substring(`COL 2`,0,6),`COL 3`) IN 
				(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)
				and cus.cus_serie = `COL 3`');
			}else{
				if($city == 1){
					$query = $this->db->query('
					select cus.cus_serie,`COL 2` as camp,cus.cus_name,cus.cus_telephone,cus.cus_email from `TABLE 95`, (select * from customers group by cus_serie) as cus
					where concat(substring(`COL 2`,0,6),`COL 3`) IN 
					(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)
					and cus.cus_serie = `COL 3`');
				}
			}
		}
		
		return $query->result();
	} 
	
	
	function getEquipoId($idAse,$idTec,$fecha){		
		$query = $this->db->query(
				'
				select * from equipos_productividad
				where eqp_asesor='.$idAse.'
				and eqp_tecnico='.$idTec.'
				and eqp_fecha="'.$fecha.'"
				');
				 return $query->result();
	}

	function campaniasABT(){
		$query = $this->db->query("select (select count(*) from `TABLE 93`
		where concat(substring(`COL 2`,0,6),`COL 3`) IN 
		(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)) as realizadas");
		return $query->result();
	}

	function campaniasABM(){
		$query = $this->db->query("select (select count(*) from `takata_mexicali`
		where concat(substring(`COL 1`,0,6),`COL 2`) IN 
		(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)) as realizadas");
		return $query->result();
	}

	function campaniasABE(){
		$query = $this->db->query("select (select count(*) from `TABLE 95`
		where concat(substring(`COL 2`,0,6),`COL 3`) IN 
		(SELECT concat(substring(cam_campania,0,6), CAM_VIN) FROM campanias where cam_status = 0)) as realizadas"); 
		return $query->result();
	}
	
	function pendientesDia($city, $cityNum, $date1, $date2){
		if($date1 == 'nodate'){
			$query = "select cam_ID as cam_id, piz_fecha as fecha, cam_campania as campania, cam_vin as vin, seo_tower as torre, concat(st.sus_name,' ',st.sus_lastName) as tecnico, concat(sa.sus_name,' ',sa.sus_lastName) as asesor, comentario as comentario from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = '".$city."' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
			and st.sus_workshop = ".$cityNum." and seo_adviser = sa.sus_idUser
			and sa.sus_workshop = ".$cityNum." and piz_ID_orden = seo_idServiceOrder order by cam_ID";
			$execute = $this->db->query($query);
			return $execute->result();
		}else{
			$query = "select cam_ID as cam_id, piz_fecha as fecha, cam_campania as campania, cam_vin as vin, seo_tower as torre, concat(st.sus_name,' ',st.sus_lastName) as tecnico, concat(sa.sus_name,' ',sa.sus_lastName) as asesor, comentario as comentario from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = '".$city."' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and (piz_fecha between '".date('Y-m-d',strtotime($date1))."' AND '".date('Y-m-d',strtotime($date2))."') and seo_technicianTeam = st.sus_idUser
			and st.sus_workshop = ".$cityNum." and seo_adviser = sa.sus_idUser
			and sa.sus_workshop = ".$cityNum." and piz_ID_orden = seo_idServiceOrder order by cam_ID";
			$execute = $this->db->query($query);
			return $execute->result();
		}
		
	}
	
	function campaniasDia(){
		$query = $this->db->query(
				"
				select * from 
				(select count( cam_vin) as realizadas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 0 and cam_ciudad = 'Tijuana' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 3 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 3 and piz_ID_orden = seo_idServiceOrder) as REALIZADAS,

				(select count( cam_vin) as pendientes from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = 'Tijuana' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 3 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 3 and piz_ID_orden = seo_idServiceOrder) as PENDIENTES,

				(select count( cam_vin) as canceladas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 2 and cam_ciudad = 'Tijuana' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 3 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 3 and piz_ID_orden = seo_idServiceOrder) as CANCELADAS

				UNION  all

				select * from 
				(select count( cam_vin) as realizadas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 0 and cam_ciudad = 'Ensenada' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 1 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 1 and piz_ID_orden = seo_idServiceOrder) as REALIZADAS,

				(select count( cam_vin) as pendientes from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = 'Ensenada' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 1 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 1 and piz_ID_orden = seo_idServiceOrder) as PENDIENTES,

				(select count( cam_vin) as canceladas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 2 and cam_ciudad = 'Ensenada' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 1 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 1 and piz_ID_orden = seo_idServiceOrder) as CANCELADAS

				union all

				select * from 
				(select count( cam_vin) as realizadas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 0 and cam_ciudad = 'Mexicali' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 2 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 2 and piz_ID_orden = seo_idServiceOrder) as REALIZADAS,
				(select count( cam_vin) as pendientes from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = 'Mexicali' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 2 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 2 and piz_ID_orden = seo_idServiceOrder) as PENDIENTES,
				(select count( cam_vin) as canceladas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 2 and cam_ciudad = 'Mexicali' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(piz_fecha) = YEAR(NOW()) and MONTH(piz_fecha) = MONTH(NOW()) and DAY(piz_fecha) = DAY(NOW()) AND seo_technicianTeam = st.sus_idUser
				and st.sus_workshop = 2 and seo_adviser = sa.sus_idUser
				and sa.sus_workshop = 2 and piz_ID_orden = seo_idServiceOrder) as CANCELADAS
				");
				 return $query->result();
	}
	
	
	function campanias()
	{
		$query = $this->db->query(
				"
				select * from 
(select count( cam_vin) as realizadas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 0 and cam_ciudad = 'Tijuana' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 3 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 3 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as REALIZADAS,

(select count( cam_vin) as pendientes from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = 'Tijuana' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 3 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 3 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as PENDIENTES,

(select count( cam_vin) as canceladas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 2 and cam_ciudad = 'Tijuana' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 3 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 3 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as CANCELADAS

UNION  all

select * from 
(select count( cam_vin) as realizadas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 0 and cam_ciudad = 'Ensenada' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 1 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 1 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as REALIZADAS,

(select count( cam_vin) as pendientes from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = 'Ensenada' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 1 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 1 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as PENDIENTES,

(select count( cam_vin) as canceladas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 2 and cam_ciudad = 'Ensenada' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 1 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 1 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as CANCELADAS

union all

select * from 
(select count( cam_vin) as realizadas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 0 and cam_ciudad = 'Mexicali' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 2 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 2 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as REALIZADAS,
(select count( cam_vin) as pendientes from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 1 and cam_ciudad = 'Mexicali' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 2 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 2 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as PENDIENTES,
(select count( cam_vin) as canceladas from pizarron, campanias, susers sa, susers st, serviceOrder_vehicleInformation, serviceOrders where cam_status = 2 and cam_ciudad = 'Mexicali' and cam_vin = sev_vin and sev_idVehicleInformation = seo_idServiceOrder and YEAR(seo_date) = YEAR(NOW()) and MONTH(seo_date) = MONTH(NOW()) AND seo_technicianTeam = st.sus_idUser
and st.sus_workshop = 2 and seo_adviser = sa.sus_idUser
and sa.sus_workshop = 2 and piz_ID_orden = seo_idServiceOrder
and piz_fecha = seo_date) as CANCELADAS
				");
				 return $query->result();
		
	}
	
	function capacidad(){		
		$query = $this->db->query(
				'
				SELECT w.*, c.*
				FROM workshops AS w, workshop_capabilities AS c
				WHERE 
				wor_idWorkshop = woc_workshop
				');
				 return $query->result();
	}
	
		function AgregarEquipo($idAse,$idTec,$fecha){		
		$query = $this->db->query(
'INSERT INTO `equipos_productividad` (`eqp_ID`, `eqp_asesor`, `eqp_tecnico`, `eqp_fecha`)
 VALUES (NULL, '.$idAse.', '.$idTec.', "'.$fecha.'");');
	}
	
	function eliminarEquipo($idAse,$idTec,$fecha){		
		$query = $this->db->query(
				'
				delete from equipos_productividad
				where eqp_asesor='.$idAse.'
				and eqp_tecnico='.$idTec.'
								and eqp_fecha="'.$fecha.'"
				');
	}
	function listarCapacidades($per){
		$query= $this->db->query(
				'
				SELECT w.*, c.*
				FROM workshops AS w, workshop_capabilities AS c
				WHERE 
				wor_idWorkshop = woc_workshop
				');
		 return $query->result();
	}
	
	function obtenerEquipos($cd,$inicio,$fin){
		$query = $this->db->query(
				'
				select * from equipos_productividad,susers,workshops
				where 
				eqp_tecnico=sus_idUser
				and sus_workshop=wor_idWorkshop
				and wor_idWorkshop="'.$cd.'"
				and eqp_fecha between "'.$inicio.'" and "'.$fin.'"
				
				
				');
				 return $query->result();
		
		}
		
		function getLista($cd,$inicio,$fin,$ase){
			$and='';
			if($ase!=0){
				$and='and seo_adviser='.$ase.'';
				}
		$query= $this->db->query(
				'
				SELECT *
				FROM pizarron,serviceOrders,susers,workshops
				WHERE 
				piz_ID_orden=seo_idServiceOrder
				and seo_technicianTeam=sus_idUser
				and sus_workshop=wor_idWorkshop
				and wor_idWorkshop="'.$cd.'"
				'.$and.'
				and piz_fecha between "'.$inicio.'" and "'.$fin.'"
                             
				');
		 return $query->result();
	}
	
		
	
		
		function obtenerOS($cd,$inicio,$fin,$ase){
			
			$and='';
			if($ase!=0){
				$and='and seo_adviser='.$ase.'';
				}
				
				
		$qu='select * 
				from serviceOrders_service,services,serviceTypes,serviceOrders,susers,pizarron
				where 
				ssr_service=ser_serviceType
				and ssr_modelo=ser_vehicleModel
				and ser_serviceType=set_idServiceType
				and ssr_IDNULL=seo_idServiceOrder
				and seo_adviser=sus_idUser
				and sus_workshop='.$cd.'
				and piz_ID_orden=seo_idServiceOrder
				and piz_ID_orden=ssr_IDNULL
				'.$and.'
				and piz_fecha between "'.$inicio.'" and "'.$fin.'"
				';		
		$query = $this->db->query($qu);
				 return $query->result();
		
		}
		
		function obtenerOSsv($cd,$inicio,$fin,$ase){
			
			$and='';
			if($ase!=0){
				$and='and seo_adviser='.$ase.'';
				}
				
		$query = $this->db->query(
				'
				select * 
				from servicios_varios,serviceOrders,susers,pizarron
				where 
				sev_id_orden=seo_idServiceOrder
				and seo_adviser=sus_idUser
				and sus_workshop='.$cd.'
				and piz_ID_orden=seo_idServiceOrder
				and piz_ID_orden=sev_id_orden
				
				'.$and.'
				and piz_fecha between "'.$inicio.'" and "'.$fin.'"
				');
				 return $query->result();
		
		}
		
		
		function obtenerOSHojaAzul($cd,$inicio,$fin,$ase){
			
			$and='';
			if($ase!=0){
				$and='and seo_adviser='.$ase.'';
				}
				
		$query = $this->db->query(
				'
				select * 
				from serviceOrders_service,services,serviceTypes,serviceOrders,susers,pizarron,serviceOrder_clientInformation, serviceOrder_vehicleInformation,appointments
				where 
				ssr_service=ser_serviceType
				and ssr_modelo=ser_vehicleModel
				and ser_serviceType=set_idServiceType
				and ssr_IDNULL=seo_idServiceOrder
				and seo_adviser=sus_idUser
				and sus_workshop='.$cd.'
				and piz_ID_orden=seo_idServiceOrder
				and piz_ID_orden=ssr_IDNULL
				'.$and.'
				and piz_ID_orden=sec_idClientInformation
				and piz_ID_orden=sev_idVehicleInformation
				and seo_idServiceOrder=sec_idClientInformation
				and seo_idServiceOrder=sev_idVehicleInformation
				and piz_fecha between "'.$inicio.'" and "'.$fin.'"
				and seo_IDapp=app_idAppointment
				');
				 return $query->result();
		
		}
		
		
		
		function tecnicos($arr,$da){
		$sum=0;
		foreach($arr as $ar){
		if($ar->eqp_fecha==$da )$sum++;
		}
		return $sum;
		}
		
		function horasV($arr,$da){
		$sum=0;
		foreach($arr as $ar){
		if($ar->piz_fecha==$da  )$sum+=$ar->ser_approximate_duration;	
		}
		return $sum;
		}
		
	function horasVsv($arr,$da){
		$sum=0;
		foreach($arr as $ar){
			
			if($ar->piz_fecha==$da && ($ar->sev_tipo_venta=='garantia' ||  $ar->sev_tipo_venta=='garantiai' ||  $ar->sev_tipo_venta=='interna' || $ar->sev_tipo_venta=='venta' || $ar->sev_tipo_venta=='adicional'))$sum+=$ar->sev_tiempo;	
			
			
			}
		return $sum;
		}	
		
		function utilizacion($fecha,$taller){
		$x=date("w", strtotime($fecha));
		
		$os = $this->Productividadmodel->obtenerOS($taller,$fecha,$fecha,0);
		$osv= $this->Productividadmodel->obtenerOSsv($taller,$fecha,$fecha,0);
		$tecnicos = $this->Productividadmodel->obtenerEquipos($taller,$fecha,$fecha);
		$capacidades = $this->Productividadmodel->listarCapacidades(0);
		
		$numb=$this->Productividadmodel->horasV($os,$fecha);
		$numbo=$this->Productividadmodel->horasVsv($osv,$fecha);
		$numb= $numb + $numbo;
		$tec=$this->Productividadmodel->tecnicos($tecnicos,$fecha); 
		
            if($x==6)
			    $semana=$capacidades[0]->woc_weekends_service_hours;
			else
			    $semana=$capacidades[0]->woc_weekdays_service_hours;
				
		$oper=$tec * $semana * $capacidades[0]->woc_productivity_factor;		 
		
		if($oper==0) return 0;
		else         $poru=($numb / $oper)*100;
		
		 return $poru;
		}
		
		
		
		
		
		
		
		
		
		
		function capacidadtaller($fecha,$taller,$capacidad){
			
		if($taller==1){$ta=0;}
		if($taller==2){$ta=1;}
		if($taller==3){$ta=2;}	
			
        $ho=$capacidad[$ta]->woc_weekdays_service_hours;
        $tec=$capacidad[$ta]->woc_tecnicos;
        $fac=$capacidad[$ta]->woc_productivity_factor;
        $how=$capacidad[$ta]->woc_weekends_service_hours;
			
			
		  $total=0;
	      $x=date("w", strtotime($fecha));
		  
		  
		  
		  if($x==6){
			  $total=($how * $tec) * $fac;
			  
			  }else{
			
			  $total=($ho * $tec) * $fac;
				  }
		  
			return $total;
			}
		
		
		
		
		
		
		
		function dias_semana($x,$inicio)
		{
			if($x==0)
			{          
			            $day=$inicio;
						list($an,$ms,$di)=explode("-",$inicio);
						$dayg=$inicio;
						$days=$an.'/'.$ms.'/'.$di;	
			}
			else
			{
							
			list($an,$ms,$di)=explode("-",$inicio);			
            $date1 = str_replace('-', '/', $ms.'-'.$di.'-'.$an);
$day = date('m-d-Y',strtotime($date1 . "+".$x." days"));
list($ms,$di,$an)=explode("-",$day);
$dayg=$an.'-'.$ms.'-'.$di;
$days=$an.'/'.$ms.'/'.$di;
             }
			return $day=array('dayg'=>$dayg,'days'=>$days);
		}
		
	
	
}
	

?>