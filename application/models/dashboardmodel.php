<?php

class Dashboardmodel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	function nomina($taller,$anio,$mes){
		$query= $this->db->query(
				'
				SELECT *
				FROM nomina
				where nom_anio='.$anio.'
				and nom_taller='.$taller.'
				ORDER BY  `nomina`.`nom_ID` ASC 	
				');

		return $query->result();
		}
		
	function desgloce_dashboard($fecha)
	{
		$query= $this->db->query(
				'
				SELECT *  
                FROM  `dashboard` 
                WHERE  `das_fecha` = "'.$fecha.'" 	order by das_taller
				');

		return $query->result();
		
	}	
		function dias_tra($taller,$anio,$mes){
		$query= $this->db->query(
				'
				SELECT *
				FROM dias_trabajados
				where dit_anio='.$anio.'
				and dit_cd='.$taller.'
				and dit_mes='.$mes.'
				
				');

		return $query->result();
		}
		
		function getDiasTra($id){
		$query= $this->db->query(
				'
				SELECT *
				FROM dias_trabajados
				where dit_ID='.$id.'
				
				');

		return $query->result();
		}
		
		
		function ppto($taller,$anio){
		$query= $this->db->query(
				'
				SELECT *
				FROM ppto,workshops,presupuestos
				where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc
				and wor_idWorkshop='.$taller.'
				and ppt_anio='.$anio.'
				 ORDER BY  `presupuestos`.`pre_ID` ASC 
				');

		return $query->result();
		}
		
		function desgloce(){
		$query= $this->db->query(
				'
				SELECT *
				FROM desgloce_csv 
				ORDER BY  `desgloce_csv`.`des_ID` ASC 
				');

		return $query->result();
		}
		
		function desglocemes($mes,$dia){
		$query= $this->db->query(
				'
				SELECT *
				FROM desgloce_csv 
				where des_aa between "'.date('Y').'-'.$mes.'-01" and "'.date('Y').'-'.$mes.'-'.$dia.'"
				ORDER BY  `desgloce_csv`.`des_ID` ASC 
				');

		return $query->result();
		}
		function desgloceAcumulado($mes){
			
			$qu='
				SELECT sum(`das_mostrador`) as mostrador, sum(`das_manoobra`) as mano, sum(`das_refacciones`) as refacciones FROM `das_desgloce` WHERE `das_cd`="tij" and das_mes <='.$mes.'
				
				union all
				
				SELECT sum(`das_mostrador`) as mostrador, sum(`das_manoobra`) as mano, sum(`das_refacciones`) as refacciones FROM `das_desgloce` WHERE `das_cd`="mex" and das_mes <='.$mes.'
				union all
				
				SELECT sum(`das_mostrador`) as mostrador, sum(`das_manoobra`) as mano, sum(`das_refacciones`) as refacciones FROM `das_desgloce` WHERE `das_cd`="ens" and das_mes <='.$mes.'
				';
			
			$query= $this->db->query($qu);

		return $query->result();
			}
		
		function pptomes($mes){
		$query= $this->db->query('Select ppt_'.$mes.' as res from v_ppto_venta');
		return $query->result();
		}
		
		function pptomesTotal($mes){
		$query= $this->db->query('
SELECT ppt_desc, (
SUM( ppt_'.date('m').')
) AS total, ppt_ciudad, ppt_anio
FROM ppto, workshops, presupuestos
WHERE ppt_ciudad = wor_idWorkshop
AND pre_ID = ppt_desc
AND ppt_ciudad =1
AND ppt_anio =  "'.date('Y').'"
AND ppt_desc
IN ( 2, 3, 4, 5, 6, 7 ) 
GROUP BY (ppt_ciudad)
UNION ALL
SELECT ppt_desc, (
SUM( ppt_'.date('m').')
) AS total, ppt_ciudad, ppt_anio
FROM ppto, workshops, presupuestos
WHERE ppt_ciudad = wor_idWorkshop
AND pre_ID = ppt_desc
AND ppt_ciudad =1
AND ppt_anio =  "'.(date('Y')-1).'"
AND ppt_desc
IN ( 2, 3, 4, 5, 6, 7 ) 
GROUP BY (ppt_ciudad)
UNION ALL
SELECT ppt_desc, (
SUM( ppt_'.date('m').')
) AS total, ppt_ciudad, ppt_anio
FROM ppto, workshops, presupuestos
WHERE ppt_ciudad = wor_idWorkshop
AND pre_ID = ppt_desc
AND ppt_ciudad =2
AND ppt_anio =  "'.date('Y').'"
AND ppt_desc
IN ( 2, 3, 4, 5, 6, 7 ) 
GROUP BY (ppt_ciudad)
UNION ALL
SELECT ppt_desc, (
SUM( ppt_'.date('m').')
) AS total, ppt_ciudad, ppt_anio
FROM ppto, workshops, presupuestos
WHERE ppt_ciudad = wor_idWorkshop
AND pre_ID = ppt_desc
AND ppt_ciudad =2
AND ppt_anio =  "'.(date('Y')-1).'"
AND ppt_desc
IN ( 2, 3, 4, 5, 6, 7 ) 
GROUP BY (ppt_ciudad)
UNION ALL
SELECT ppt_desc, (
SUM( ppt_'.date('m').')
) AS total, ppt_ciudad, ppt_anio
FROM ppto, workshops, presupuestos
WHERE ppt_ciudad = wor_idWorkshop
AND pre_ID = ppt_desc
AND ppt_ciudad =3
AND ppt_anio =  "'.date('Y').'"
AND ppt_desc
IN ( 2, 3, 4, 5, 6, 7 ) 
GROUP BY (ppt_ciudad)
UNION ALL
SELECT ppt_desc, (
SUM( ppt_'.date('m').')
) AS total, ppt_ciudad, ppt_anio
FROM ppto, workshops, presupuestos
WHERE ppt_ciudad = wor_idWorkshop
AND pre_ID = ppt_desc
AND ppt_ciudad =3
AND ppt_anio =  "'.(date('Y')-1).'"
AND ppt_desc
IN ( 2, 3, 4, 5, 6, 7 ) 
GROUP BY (ppt_ciudad)

');
		return $query->result();
		}
		
		
		
		function pptomesA($mesx){

$mes='';
for($i=1; $i<=$mesx; $i++){

	if($i<=9){$i='0'.$i;}	
$mes.='SUM(ppt_'.$i.') +';	
	}
$mes=substr($mes, 0, -1);				


			
		$query= $this->db->query('
SELECT ppt_desc, ('.$mes.') AS total,ppt_ciudad,ppt_anio				
FROM ppto,workshops,presupuestos
where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc 
and ppt_ciudad=1 and ppt_anio="'.date('Y').'"
and ppt_desc IN(2,3,4,5,6,7)				
group by(ppt_ciudad)

UNION ALL

SELECT ppt_desc, ('.$mes.') AS total,ppt_ciudad,ppt_anio				
FROM ppto,workshops,presupuestos
where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc 
and ppt_ciudad=1 and ppt_anio="'.(date('Y')-1).'"
and ppt_desc IN(2,3,4,5,6,7)				
group by(ppt_ciudad)

UNION ALL

SELECT ppt_desc, ('.$mes.') AS total,ppt_ciudad,ppt_anio				
FROM ppto,workshops,presupuestos
where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc 
and ppt_ciudad=2 and ppt_anio="'.date('Y').'"
and ppt_desc IN(2,3,4,5,6,7)				
group by(ppt_ciudad)

UNION ALL

SELECT ppt_desc, ('.$mes.') AS total,ppt_ciudad,ppt_anio				
FROM ppto,workshops,presupuestos
where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc 
and ppt_ciudad=2 and ppt_anio="'.(date('Y')-1).'"
and ppt_desc IN(2,3,4,5,6,7)				
group by(ppt_ciudad)

UNION ALL

SELECT ppt_desc, ('.$mes.') AS total,ppt_ciudad,ppt_anio				
FROM ppto,workshops,presupuestos
where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc 
and ppt_ciudad=3 and ppt_anio="'.date('Y').'"
and ppt_desc IN(2,3,4,5,6,7)				
group by(ppt_ciudad)

UNION ALL

SELECT ppt_desc, ('.$mes.') AS total,ppt_ciudad,ppt_anio				
FROM ppto,workshops,presupuestos
where ppt_ciudad=wor_idWorkshop	and pre_ID=ppt_desc 
and ppt_ciudad=3 and ppt_anio="'.(date('Y')-1).'"
and ppt_desc IN(2,3,4,5,6,7)				
group by(ppt_ciudad)

				');
		return $query->result();
		}
		
		function getPpto($cd){
		$query= $this->db->query(
				'
				SELECT *
				FROM ppto,workshops,presupuestos
				where ppt_ciudad=wor_idWorkshop
				and ppt_ID='.$cd.' and pre_ID=ppt_desc
				');

		return $query->result();
		}
		
		function updateppto($dato,$id){
			$this->db->update('ppto', $dato, array('ppt_ID' => $id));
			}
			
			function updateDiasTra($dato,$id){
			$this->db->update('dias_trabajados', $dato, array('dit_ID' => $id));
			}
		
		
		
		
		
	function meses(){
		
	$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	return $meses;	
		}
		
		
		
			function app(){
		
		$query= $this->db->query(
				'
				SELECT *
				FROM serviceOrders
				where 	seo_date="'.date('Y-m-d').' 00:00:00"	
				');

		return $query->result();
	}
	
	function appedit($id){
		
		$query= $this->db->query(
				'
				update appointments set app_status=1   where app_idAppointment='.$id.' 
				');

	}
	
	
	function editarMonto($id,$val){
	$query= $this->db->query('update nomina set nom_monto="'.$val.'"   where nom_ID='.$id.' ');
    }
	
	
	
	function getMonthDays($Month, $Year)
{
   //Si la extensión que mencioné está instalada, usamos esa.
   if( is_callable("cal_days_in_month"))
   {
      return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
   }
   else
   {
      //Lo hacemos a mi manera.
      return date("d",mktime(0,0,0,$Month+1,0,$Year));
   }
}

function nameDay($date){
$day = date('l', strtotime($date));
if($day=='Sunday'){return 'Dom';}
if($day=='Monday'){return 'Lun';}
if($day=='Tuesday'){return 'Mar';}
if($day=='Wednesday'){return 'Mie';}
if($day=='Thursday'){return 'Jue';}
if($day=='Friday'){return 'Vie';}
if($day=='Saturday'){return 'Sab';}
}

/*function agente($ase){
$age=0;	
if($ase==32){$age='273';}
if($ase==68){$age='271';}
if($ase==69){$age='333';}
if($ase==81){$age='346';}
if($ase==35){$age='347';}	
return $age;	
	}*/

function limpia_espacios($cadena){
	$cadena = str_replace(' ', '', $cadena);
	return $cadena;
}	
	
function formaton($num){
	/*$etopu=0;
	$etopur=0;
	$etopum=0;
	$etoput=0;
	$mmex=0;
	$t=0;
	$mmex=substr($num, 1);
	$t=strlen($mmex);
	if($t<=6){}
	else{
	list($n1,$n2)=explode(',',$mmex);
	$mmex=$n1.$n2;
	}
	 */
	return $num;
	}		
		
		function verdia($desgloce,$age,$dia,$mes,$anio){
$res=0;
$suma=0;
foreach($desgloce as $des){
if( $des->des_a==''.$dia.'/'.$mes.'/'.$anio.'' && $des->des_d==$age){
if($des->des_l==''){$res=0;}
else{$res=$des->des_l;}
$suma+=$res;
}}

return $suma;
}

function verdiaacu($desgloce,$age,$dia,$mes,$anio){
$res=0;
$suma=0;
foreach($desgloce as $des){
if( $des->des_a>='01/'.date('m').'/'.$anio.'' && $des->des_a<=''.$dia.'/'.$mes.'/'.$anio.'' && $des->des_d==$age){
if($des->des_l==''){$res=0;}
else{$res=$des->des_l;}
$suma+=$res;
}}

return $suma;
}
		
		
		
		
		function horasase($desgloce,$age,$dia,$mes,$anio){
$dec='';
$bul='no';
$smtot=0;
$nor=0;
foreach($desgloce as $des){

	

if($des->des_tipo=='SERVICIO'){
if(  $des->des_a==''.$dia.'/'.$mes.'/'.$anio.'' && $des->des_d==$age){
	 $nor++;
	  $smtot+=$des->des_k.'<br>';
	}
}


}
if($smtot==0){return 0;}
else{
return $smtot / $nor;}
}


function thorasase($desgloce,$age){
$dec='';
$bul='si';
$smtot=0;
$nor=0;
foreach($desgloce as $des){
if($des->des_e=='MOSTRADOR/REFACCIONES' || $des->des_e=='SERVICIO' || $des->des_e=='OTROS'){ $bul='si';}{}
if($des->des_e=='INTERNAS'){$bul='no';}

if($bul=='si'){
if($des->des_d==$age){
	 $nor++;
	 $smtot+=$des->des_k.'<br>';
	}
}

}

if($smtot==0){return 0;}
else{
return array('horas'=>$smtot,'ordenes'=>$nor);}
}



//horas de servicio al dia
function hos($usuario,$lista,$desgloce,$taller,$mes,$anio)
{
$totho=0;
$totos=0;
$res=0;
foreach($usuario as $us)
   {
		 if($us->rol_idRol=='3' && $us->sus_isActive=='1' && $us->sus_workshop==$taller) 
        {
		$age=$us->sus_adviserNumber;	 
		$verdia=$this->Dashboardmodel->thorasase($desgloce,$age);
		$totho+=$verdia['horas'];
		$totos+=$verdia['ordenes'];
        }
	}	

if($totho!=0){ $res=$totho/$totos;}else{ $res=0; }
return number_format($res, 2, '.', '');	
}


	
	





function formatodeVentas($tij,$tijr,$tijm){
    $topu=0;
	$topur=0;
	$topum=0;
	$toput=0;
	$mmex=0;
	$mmex=substr($tij, 1);list($n1,$n2)=explode(',',$mmex);
	$mmex=$n1.$n2; 
	$topu=$mmex;
	
	$mmex=substr($tijr, 1);list($n1,$n2)=explode(',',$mmex);
	$mmex=$n1.$n2; 
	$topur=$mmex;
	
	$mmex=substr($tijm, 1);list($n1,$n2)=explode(',',$mmex);
	$mmex=$n1.$n2; 
	$topum=$mmex;
	
	return $topu + $topur + $topum + $toput;	
	
	}

	
	
	function citas($array,$fecha,$dd){
$n=0;
$r=0;	
$f=0;
$s=0;
$c=0;
$i=0;
$in='';
$fo='';
$p=0;
foreach($array as $ar){
	list($in,$fo)=explode('-',$ar->app_folio);
	list($an,$ms,$di)=explode('-',$ar->app_day);
	
	if($ar->app_status=='1'){$r++;}
	if( $ar->app_status=='0'){$f++;}
	
	}
	
	return array('r'=>$r,'f'=>$f);	
	}
	
	
	
	
	
	
	
	
	function realpormes($desgloce,$cd){
if($cd=='TIJUANA'){$taller='Taller Matriz';}
if($cd=='Mexicali'){$taller='Taller Mexicali';}
if($cd=='Ensenada'){$taller='Taller Ensenada';}
	
$ntipo='';
$mostrador=0;
//mano de obra
$ser_l=0;
$gar_l=0;
$cia_l=0;
$int_l=0;
$otr_l=0;
//reffacciones
$ser_n=0;
$gar_n=0;
$cia_n=0;
$int_n=0;
$otr_n=0;
$tot_q=0;
//mano de obra items
$sser=0;
$sgar=0;
$sseg=0;
$sint=0;
$sotr=0;
$stot=0;

	$x=1;
foreach($desgloce as $row) {
if($row->des_a=='Fecha'  || $row->des_a==''){}else{

$sub=substr($row->des_b, 0, 4);


//limpiar espacion de la cadena
$ntipo=$this->Dashboardmodel->limpia_espacios($row->des_tipo);
$ntipo=substr($ntipo, 0, 3);
//sumar mostrador refacciones		
if($ntipo=='MOS' && $row->des_cd==$cd){
//echo $row->des_n.'-----'.$x++.'---'.$cd.'--'.$row->des_day.'-<br>';	
 $mostrador+=$row->des_n;	

}	

//sumar servicio (L) , refaciones (N)		
if(($row->des_cd==$cd || $row->des_cd==$taller) && $ntipo=='SER'){
$ser_l+=$row->des_l;	
$ser_n+=$row->des_n;
$tot_q+=$row->des_q;
$sser++;	
}	

//sumar Garantia (L) , refaciones (N)		
if( ($row->des_cd==$cd || $row->des_cd==$taller) && $ntipo=='GAR'){
$gar_l+=$row->des_l;	
$gar_n+=$row->des_n;
$tot_q+=$row->des_q;	
$sgar++;	
}	


//sumar CIA SEGURO (L) , refaciones (N)		
if(($row->des_cd==$cd || $row->des_cd==$taller) && $ntipo=='CIA'){
$cia_l+=$row->des_l;	
$cia_n+=$row->des_n;
$tot_q+=$row->des_q;	
$sseg++;	
}	

//sumar INTERNAS (L) , refaciones (N)		
if( ($row->des_cd==$cd || $row->des_cd==$taller) && $ntipo=='INT'){
$int_l+=$row->des_l;	
//echo $row->des_b.'<br>';
$int_n+=$row->des_n;
$tot_q+=$row->des_q;
$sint++;	
}	

//sumar OTROS (L) , refaciones (N)		
if(($row->des_cd==$cd || $row->des_cd==$taller) && $ntipo=='OTR'){
$otr_l+=$row->des_l;	
$otr_n+=$row->des_n;
$tot_q+=$row->des_q;	
$sotr++;	
}	

	
    }//else
	

	}//foreach end
	
//echo 'mostrador:'.$mostrador.'<br>';
		//echo 'mano de obra:'.$ser_l.'<br>'.$gar_l.'<br>'.$cia_l.'<br>'.$int_l.'<br>'.$otr_l.'<br>';
		//	echo 'refacciones:'.$ser_n.'<br>'.$gar_n.'<br>'.$cia_n.'<br>'.$int_n.'<br>'.$otr_n.'<br>';
		/*	echo 'Servicio:'.($sser).'<br>';
				echo 'Garantias:'.($sgar).'<br>';
					echo 'Seguros:'.($sseg).'<br>';
						echo 'Internas:'.($sint).'<br>';
							echo 'Otros:'.($sotr).'<br>';*/	
		


return array(
'most'=>$mostrador,
'mano'=>($ser_l + $gar_l + $cia_l + $int_l + $otr_l),
'refac'=>($ser_n + $gar_n + $cia_n + $int_n + $otr_n),
'serv'=>($sser),
'gara'=>($sgar),
'segu'=>($sseg),
'inte'=>($sint),
'otro'=>($sotr),
'tot'=>$tot_q);
		}
	
	
		function realacumulado($desgloce){
//Marzo	
$val=0;	
$tij=0;
$mex=0;
$ens=0;

$valr=0;	
$tijr=0;
$mexr=0;
$ensr=0;

$valm=0;	
$tijm=0;
$mexm=0;
$ensm=0;

$valc=0;	
$tijc=0;
$mexc=0;
$ensc=0;

$tijcc=0;
$mexcc=0;
$enscc=0;

$valtot=0;
$ttot=0;
$mtot=0;
$etot=0;

//Enero
$eval=0;	
$etij=0;
$emex=0;
$eens=0;

$evalr=0;	
$etijr=0;
$emexr=0;
$eensr=0;

$evalm=0;	
$etijm=0;
$emexm=0;
$eensm=0;

$evalc=0;	
$etijc=0;
$emexc=0;
$eensc=0;

$etijcc=0;
$emexcc=0;
$eenscc=0;

$evaltot=0;
$ettot=0;
$emtot=0;
$eetot=0;

//Febrero	
$fval=0;	
$ftij=0;
$fmex=0;
$fens=0;

$fvalr=0;	
$ftijr=0;
$fmexr=0;
$fensr=0;

$fvalm=0;	
$ftijm=0;
$fmexm=0;
$fensm=0;

$fvalc=0;	
$ftijc=0;
$fmexc=0;
$fensc=0;

$ftijcc=0;
$fmexcc=0;
$fenscc=0;

$fvaltot=0;
$fttot=0;
$fmtot=0;
$fetot=0;		
		

for($s=1; $s<=date('m'); $s++){
$f=0;	
	if($s<10){$s='0'.$s;}
foreach($desgloce as $des){
	
if(strpos($des->des_j, 'Total Sucursal:')!==false ){
if($des->des_mes==$s){
	if($des->des_j== 'Total Sucursal:'){
		//venta de mano de obra
		$val=$this->Dashboardmodel->formaton($des->des_l);
		//venta de refacciones
		$valr=$this->Dashboardmodel->formaton($des->des_n);
		//venta refacciones costo
		$valc=$this->Dashboardmodel->formaton($des->des_o);
		//venta de mostrador
		$valm=$this->Dashboardmodel->formaton($des->des_n);
		//tot
		$valtot=$this->Dashboardmodel->formaton($des->des_q);
		}

$f++;	

if($f==2){$tij+=$val; $tijr+=$valr; $tijc+=$valc; $ttot+=$valtot;}
if($f==4){$mex+=$val; $mexr+=$valr; $mexc+=$valc; $mtot+=$valtot;}
if($f==6){$ens+=$val; $ensr+=$valr; $ensc+=$valc; $etot+=$valtot;}

if($f==1){$tijm+=$valm; $tijcc+=$valc;}
if($f==3){$mexm+=$valm; $mexcc+=$valc;}
if($f==5){$ensm+=$valm; $enscc+=$valc;}	
//echo '<tr><td>'.$des->des_c.'-'.$des->des_j.'-'.$valm.'</td></tr>';	
	
    }
	}	
	}
}

return array(
'tij'=>$tij,'tijr'=>$tijr,'tijc'=>$tijc,'ttot'=>$ttot,'tijm'=>$tijm,'tijcc'=>$tijcc,
'mex'=>$mex,'mexr'=>$mexr,'mexc'=>$mexc,'mtot'=>$mtot,'mexm'=>$mexm,'mexcc'=>$mexcc,
'ens'=>$ens,'ensr'=>$ensr,'ensc'=>$ensc,'etot'=>$etot,'ensm'=>$ensm,'enscc'=>$enscc);

		}
	
	
	
	function getTipodeOrden($taller,$encuentra,$omite,$omiteb,$desgloce){
$bul='no';
$buli='no';
$lev=0;

//internas Mexicali
foreach($desgloce as $des){
	
if($des->des_c==$taller){$bul='si';}
if($bul=='si' && $des->des_e==$encuentra){$buli='si';}
if($bul=='si' && $buli=='si' && ($des->des_e==$omite || $des->des_e==$omiteb)){$buli='no'; $bul='no';}	
if($bul=='si' && $buli=='si'){
'<tr><td>'.$lev++.''.$des->des_a.''.$des->des_b.' '.$des->des_c.' '.$des->des_e.' '.$des->des_j.' </td></tr>';
}}	
		
	return $lev;	}
	
	function setComentario($camp, $com) {
		$this->db->set(array("comentario" => $com));
		$this->db->from("campanias");
		$this->db->where("cam_ID", $camp);

		return $this->db->update();
	}
	
	
	
}





?>