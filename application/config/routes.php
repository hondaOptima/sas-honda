<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "dashboard";
$route['404_override'] = '';

//Carrocería.
$route['carroceria'] = 'carroceria/index';

$route['traslados-entregas/(:any)'] = 'rutas_sin_sesion/traslados_entregas/$1';

$route['get_carroceria_espera'] = 'carroceria/get_carroceria_espera';
$route['get_carroceria_autorizados'] = 'carroceria/get_carroceria_autorizados';
$route['update_carroceria'] = 'carroceria/update_carroceria';
$route['get_tipos_carroceria'] = 'carroceria/get_tipos_carroceria';
$route['get_refacciones_carroceria'] = 'carroceria/get_refacciones_carroceria';
$route['get_carroceria_data_status'] = 'carroceria/get_carroceria_data_status';
$route['get_carroceria'] = 'carroceria/get_carroceria';
$route['add_carroceria_comment'] = 'carroceria/add_carroceria_comment';
$route['get_comments/(:num)'] = 'carroceria/get_comments/$1';
$route['get_comments'] = 'carroceria/get_comments';

$route['probar_vic'] = 'carroceria/probar_vic';

//Citas
$route['citas'] = 'cita/prueba';
$route['citas/(:any)/(:num)'] = 'cita/prueba/$1/$2';
$route['send_mail_servicio_asesor'] = 'api/cita_api/send_mail_servicio_asesor';

//Cotizaciones.
$route['confirmar-cotizacion'] = 'cotizaciones/formulario_cliente';

//Inventario.
$route['get_autos_piso']    = 'inventario/get_autos_piso';
$route['get_auto_piso']     = 'inventario/get_auto_piso';
$route['get_traslado']      = 'inventario/get_traslado';

//Torres
$route['get_torres']    = 'torre/get_torres';
$route['update_torre']  = 'torre/update_torre';

//Setup dboptima
$route['fill_custom']   = 'setupoptima/Transfercustomers';
$route['cus_optima']    = 'setupoptima/CustomersToOptima';
$route['veh_optima']    = 'setupoptima/VehiclesToOptima';
$route['crm_optima']    = 'setupoptima/CrmToOptima';
$route['new_crm']       = 'setupoptima/NewCustomers';
$route['del_dir']       = 'setupoptima/DeleteDir';

//App Upload Files
$route['auf_customers/(:any)']  = 'uploadfiles/GetCustomers/$1';
$route['auf_customers']         = 'uploadfiles/GetCustomers';
$route['auf_files/(:any)']      = 'uploadfiles/GetFiles/$1';

// Campanias
$route['camp_exist/(:any)']     = 'orden/CampaniaExist/$1';
$route['get_campanias/(:any)']  = 'orden/GetCampaniasVin/$1';
$route['get_customers_oda']     = 'orden/GetCustomersOd';

//Orden.
$route['get_servicios_vin'] = 'api/orden_api/servicios_por_vin';
$route['captura-orden'] = 'orden/captura';


//Reportes
$route['comportamiento-citas']      = 'reportes/citas';
$route['analisis-diario-ordenes']   = 'reportes/ordenes';
$route['profit-watch']              = 'reportes/profit';
$route['venta-perdida']             = 'reportes/saveVentaPerdida';
$route['auditoria-citas']           = 'reportes/auditoriacitas';
$route['inventario']                = 'reportes/inventario';
$route['status-vehiculo']           = 'reportes/lista_inventario';
$route['calendario-entregas']       = 'reportes/calendario_inventario';
$route['autos-recibidos']           = 'reportes/autos_recibidos';
$route['autos-proceso-venta']       = 'reportes/autos_proceso_venta';
$route['status-torres']             = 'reportes/statustorres';
$route['autos-traslados']           = 'reportes/traslados';

$route['update_fecha_salida_real']          = 'reportes/update_fecha_salida_real';
$route['get_lista_traslados_salida']        = 'reportes/get_lista_traslados_salida';
$route['get_lista_traslados_entrada']       = 'reportes/get_lista_traslados_entrada';
$route['get_lista_traslados']               = 'reportes/get_lista_traslados';
$route['get_traslados_origen_y_destino']    = 'reportes/get_traslados_origen_y_destino';
$route['recibir-hoja-traslado/(:num)']      = 'reportes/recibir_hoja_traslado/$1';
$route['crear-hoja-traslado/(:num)']        = 'reportes/crear_hoja_traslado/$1';

//Pizarrón.
$route['lavado'] = 'pizarron/lavado';
$route['get_autos_lavado'] = 'pizarron/get_autos_lavado';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
