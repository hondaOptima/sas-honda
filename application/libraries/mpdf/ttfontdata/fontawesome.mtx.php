<?php
$name='FontAwesome';
$type='TTF';
$desc=array (
  'Ascent' => 857,
  'Descent' => -143,
  'CapHeight' => 0,
  'Flags' => 4,
  'FontBBox' => '[-1 -143 1286 857]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 500,
);
$up=0;
$ut=0;
$ttffile='application//libraries/mpdf/ttfonts/fontawesome-webfont.ttf';
$TTCfontID='0';
$originalsize=150920;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='fontawesome';
$panose=' 0 0 0 0 0 0 0 0 0 0 0 0';
$haskerninfo=false;
$unAGlyphs=false;
?>