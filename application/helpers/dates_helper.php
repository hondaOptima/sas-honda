<?php

    //Regresa una fecha larga cuando se le manda un parámetro de fecha.
    //Parámetros: DATE fecha, BOOL $meses_cortos.
    //Return (example): Viernes 09 de Diciembre del 2016.
    function fecha_completa_larga($fecha,$meses_cortos = null) {
        $meses = get_meses();
        $dias= get_dias();
        $date = DateTime::createFromFormat("Y-m-d", $fecha);
        return $dias[(int)$date->format("w")]." ".$date->format("d")." de ".(($meses_cortos) ? substr ($meses[(int)$date->format("m")-1],0,3) : $meses[(int)$date->format("m")-1])." del ".$date->format("Y");
    }

    //Regresa una fecha corta cuando se le manda un parámetro de fecha.
    //Parámetros: DATE fecha.
    //Return (example): 09 Dic 2016.
    function fecha_completa_corta($fecha) {
        $meses = get_meses();
        $dias= get_dias();
        $date = DateTime::createFromFormat("Y-m-d", $fecha);
        return $date->format("d")." ".substr($meses[(int)$date->format("m")-1],0,3)." ".$date->format("Y");
    }


    function get_meses() {
        return array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    }

    function get_dias() {
        return array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    }
?>
